"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iots_history_type_5 extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  iots_history_type_5.init(
    {
      node_id: DataTypes.INTEGER,
      devEui: DataTypes.STRING,
      battrey_level: DataTypes.DOUBLE,
      preassure: DataTypes.DOUBLE,
      tenant_id: DataTypes.INTEGER,
      internal_id: DataTypes.INTEGER,
      update_time: DataTypes.STRING,
      reportTimeFlag: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "iots_history_type_5",
    }
  );
  return iots_history_type_5;
};

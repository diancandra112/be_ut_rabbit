"use strict";
module.exports = (sequelize, DataTypes) => {
  const iot_nodes = sequelize.define(
    "iot_nodes",
    {
      areaId: DataTypes.INTEGER,
      interval: DataTypes.INTEGER,
      typeId: DataTypes.INTEGER,
      device_type_id: DataTypes.INTEGER,
      tenantId: DataTypes.INTEGER,
      internalId: DataTypes.INTEGER,
      image_url: DataTypes.STRING,
      devEui: DataTypes.STRING,
      description: DataTypes.STRING,
      installationDate: DataTypes.DATE,
      last_update: DataTypes.STRING,
      previous_update: DataTypes.STRING,
      live_last_meter: DataTypes.STRING,
      live_previous_meter: DataTypes.STRING,
      live_interval: DataTypes.INTEGER,
      live_battery: DataTypes.STRING,
      start_anomali_date: DataTypes.DATE,
      start_anomali_meter: DataTypes.STRING,
      is_anomali: DataTypes.BOOLEAN,
      last_cut_date: DataTypes.STRING,
      live_valve: DataTypes.STRING,
      setting_valve: DataTypes.STRING,
      setting_interval: DataTypes.STRING,
      rtu_pricing_id: DataTypes.INTEGER,
      rtu_pricing_name: DataTypes.STRING,
      latitude: DataTypes.STRING,
      longitude: DataTypes.STRING,
      altitude: DataTypes.STRING,
      address: DataTypes.TEXT,
      merk: DataTypes.STRING,
      meter_id: DataTypes.STRING,
      prepayment: DataTypes.STRING,
      live_prepayment: DataTypes.STRING,
      live_prepayment_date: DataTypes.DATE,
      field_billing_rtu: DataTypes.STRING,
      line_anomali: DataTypes.DATE,
      line_offline: DataTypes.DATE,
      line_low_battery: DataTypes.DATE,
      line_prepaid: DataTypes.DATE,

      email_anomali: DataTypes.DATE,
      email_offline: DataTypes.DATE,
      email_low_battery: DataTypes.DATE,
      email_prepaid: DataTypes.DATE,

      anomali_usage_start_date: DataTypes.STRING,
      anomali_usage_end_date: DataTypes.STRING,
      max_usage: DataTypes.STRING,

      minim_balance_gas: DataTypes.DOUBLE,
      minim_balance_water: DataTypes.DOUBLE,
      alarm_pressure: DataTypes.DOUBLE,
      interval_alarm_pressure: DataTypes.DOUBLE,
      last_alarm_pressure: DataTypes.DOUBLE,

      is_unsigned: DataTypes.BOOLEAN,
      node_link_id: DataTypes.INTEGER,

      sensor_to_botom: DataTypes.DOUBLE,
      high_alarm: DataTypes.DOUBLE,
      low_alarm: DataTypes.DOUBLE,

      child_app_name: DataTypes.STRING,
      child_sn: DataTypes.STRING,

      iaq: DataTypes.DOUBLE,
      iaq_quality: DataTypes.STRING,

      is_display: DataTypes.BOOLEAN,
      sensor_level_offset: DataTypes.DOUBLE,

      setting_pulse: DataTypes.DOUBLE,
      live_pulse: DataTypes.DOUBLE,
      start_totalizer: DataTypes.DOUBLE,
      flag_install: DataTypes.BOOLEAN,

      max_totalizer: DataTypes.DOUBLE,

      carrunt_latitude: DataTypes.STRING,
      carrunt_longitude: DataTypes.STRING,

      last_rtu_log: DataTypes.TEXT,

      rssi: DataTypes.STRING,
      snr: DataTypes.STRING,
      gateway_eui: DataTypes.STRING,
      gateway_name: DataTypes.STRING,
      battery_voltage: DataTypes.DOUBLE,
    },
    {}
  );
  iot_nodes.associate = function (models) {
    iot_nodes.hasMany(models.iots_steal_alarm, {
      foreignKey: "node_id",
    });
    iot_nodes.hasMany(models.iots_log_downlink, {
      foreignKey: "node_id",
    });
    iot_nodes.hasMany(models.iots_charts_usage, {
      foreignKey: "nodeId",
    });
    iot_nodes.belongsTo(models.iot_nodes, {
      foreignKey: "node_link_id",
    });
    iot_nodes.hasMany(models.iots_anomali_history, {
      foreignKey: "deveui",
      sourceKey: "devEui",
    });
    iot_nodes.hasOne(models.iots_tiketing, {
      foreignKey: "dev_eui",
    });
    iot_nodes.hasOne(models.iot_pricing_postpaid_all, {
      foreignKey: "areaId",
      sourceKey: "areaId",
    });

    // associations can be defined here
    // iot_nodes.hasMany(models.iot_histories,{foreignKey:'device_id'})
    iot_nodes.belongsTo(models.iot_area, { foreignKey: "areaId" });
    iot_nodes.hasMany(models.iots_billing_history, {
      foreignKey: "nodeId",
      as: "last_billing_desc",
      sourceKey: "id",
    });
    iot_nodes.hasMany(models.iots_billing_prepaid, {
      foreignKey: "device_id",
    });
    iot_nodes.hasMany(models.iots_billing_history, {
      foreignKey: "nodeId",
      as: "bill",
    });
    iot_nodes.belongsTo(models.iots_billing_history, {
      foreignKey: "id",
      targetKey: "nodeId",
    });
    iot_nodes.belongsTo(models.iots_billing_history, {
      foreignKey: "id",
      targetKey: "nodeId",
      as: "last_billing",
    });
    iot_nodes.belongsTo(models.iot_tenant, { foreignKey: "tenantId" });
    iot_nodes.belongsTo(models.iot_internal, { foreignKey: "internalId" });

    iot_nodes.hasOne(models.iot_detail_pju, { foreignKey: "node_id" });
    iot_nodes.hasOne(models.iot_detail_water, { foreignKey: "node_id" });
    iot_nodes.hasOne(models.iot_detail_temperature, { foreignKey: "node_id" });
    iot_nodes.hasOne(models.iot_detail_rtu, { foreignKey: "node_id" });
    iot_nodes.hasOne(models.iot_detail_preassure, { foreignKey: "node_id" });
    iot_nodes.hasOne(models.iot_detail_gas, {
      foreignKey: "node_id",
      as: "iot_detail_gas",
    });
    iot_nodes.hasOne(models.iot_detail_electric, { foreignKey: "node_id" });
    iot_nodes.hasOne(models.iot_detail_electricct, { foreignKey: "node_id" });

    // iot_nodes.belongsTo(models.iot_histories,{foreignKey:'device_id'})

    iot_nodes.hasMany(models.iot_histories, {
      foreignKey: "device_id",
    });
    iot_nodes.hasMany(models.iot_histories, {
      foreignKey: "device_id",
      as: "history_end",
    });
    iot_nodes.hasMany(models.iot_histories, {
      foreignKey: "device_id",
      as: "history_start",
    });
    iot_nodes.belongsTo(models.iot_histories, {
      foreignKey: "id",
      targetKey: "device_id",
      as: "history_log",
    });

    iot_nodes.belongsTo(models.iot_device_type, {
      foreignKey: "device_type_id",
    });
    iot_nodes.belongsTo(models.iot_node_type, {
      foreignKey: "typeId",
      // targetKey: "type_id",
      // constraints: true
    });
  };
  return iot_nodes;
};

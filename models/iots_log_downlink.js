"use strict";
module.exports = (sequelize, DataTypes) => {
  const iots_log_downlink = sequelize.define(
    "iots_log_downlink",
    {
      time: DataTypes.STRING,
      action: DataTypes.STRING,
      email: DataTypes.STRING,
      node_id: DataTypes.INTEGER,
      areaId: DataTypes.INTEGER,
      value: DataTypes.DOUBLE,
      unit: DataTypes.STRING,
      deveui: DataTypes.STRING,
    },
    {}
  );
  iots_log_downlink.associate = function (models) {
    // associations can be defined here
    iots_log_downlink.belongsTo(models.iot_nodes, {
      foreignKey: "node_id",
    });
  };
  return iots_log_downlink;
};

"use strict";
module.exports = (sequelize, DataTypes) => {
  const iots_admin = sequelize.define(
    "iots_admin",
    {
      user_id: DataTypes.INTEGER,
      fullname: DataTypes.STRING,
      username: DataTypes.STRING,
      password: DataTypes.TEXT,
      email: DataTypes.STRING,
      phone: DataTypes.STRING,
      address: DataTypes.TEXT,
      area_id: DataTypes.INTEGER,
      images: DataTypes.TEXT,
      summary: DataTypes.TEXT,
    },
    {}
  );
  iots_admin.associate = function (models) {
    // associations can be defined here
  };
  return iots_admin;
};

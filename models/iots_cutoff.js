"use strict";
module.exports = (sequelize, DataTypes) => {
  const iots_cutoff = sequelize.define(
    "iots_cutoff",
    {
      areaId: DataTypes.INTEGER,
      tenantId: DataTypes.INTEGER,
      tanggal_cutoff: DataTypes.INTEGER,
      order: DataTypes.INTEGER,
      auto_bill_type: DataTypes.INTEGER,
      status: DataTypes.BOOLEAN,
      time: DataTypes.STRING,
    },
    {}
  );
  iots_cutoff.associate = function (models) {
    // associations can be defined here
    iots_cutoff.belongsTo(models.iot_area, { foreignKey: "areaId" });
    iots_cutoff.belongsTo(models.iot_area, {
      foreignKey: "areaId",
      as: "cut_off",
    });
  };
  return iots_cutoff;
};

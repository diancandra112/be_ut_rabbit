'use strict';
module.exports = (sequelize, DataTypes) => {
  const iots_auto_bill_options = sequelize.define('iots_auto_bill_options', {
    name: DataTypes.STRING
  }, {});
  iots_auto_bill_options.associate = function(models) {
    // associations can be defined here
  };
  return iots_auto_bill_options;
};
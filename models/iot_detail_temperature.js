'use strict';
module.exports = (sequelize, DataTypes) => {
  const iot_detail_temperature = sequelize.define('iot_detail_temperature', {
    last_update: DataTypes.DATE,
    devEui: DataTypes.STRING,
    interval: DataTypes.INTEGER,
    desc: DataTypes.STRING,
    node_id: DataTypes.INTEGER
  }, {});
  iot_detail_temperature.associate = function(models) {
    // associations can be defined here
    iot_detail_temperature.belongsTo(models.iot_nodes,{foreignKey:'node_id'})
  };
  return iot_detail_temperature;
};
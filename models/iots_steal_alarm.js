"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iots_steal_alarm extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      iots_steal_alarm.belongsTo(models.iot_area, { foreignKey: "areaId" });
      iots_steal_alarm.belongsTo(models.iot_nodes, {
        foreignKey: "node_id",
      });
    }
  }
  iots_steal_alarm.init(
    {
      node_id: DataTypes.INTEGER,
      devEui: DataTypes.STRING,
      type: DataTypes.STRING,
      report_time_start: DataTypes.STRING,
      report_time_end: DataTypes.STRING,
      date: DataTypes.STRING,
      notif: DataTypes.BOOLEAN,
      areaId: DataTypes.INTEGER,
      area_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "iots_steal_alarm",
    }
  );
  return iots_steal_alarm;
};

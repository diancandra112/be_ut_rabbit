"use strict";
module.exports = (sequelize, DataTypes) => {
  const iots_teknisi = sequelize.define(
    "iots_teknisi",
    {
      fullname: DataTypes.STRING,
      username: DataTypes.STRING,
      password: DataTypes.TEXT,
      email: DataTypes.STRING,
      phone: DataTypes.STRING,
      address: DataTypes.TEXT,
      area_id: DataTypes.INTEGER,
      images: DataTypes.TEXT,
      is_company: DataTypes.BOOLEAN,
      status: DataTypes.BOOLEAN,
      user_id: DataTypes.INTEGER,
      fcm_token: DataTypes.TEXT,
      summary: DataTypes.TEXT,
    },
    {}
  );
  iots_teknisi.associate = function (models) {
    iots_teknisi.hasMany(models.iots_node_type_teknisi, {
      foreignKey: "teknisi_id",
    });
    iots_teknisi.hasMany(models.iots_node_type_teknisi, {
      foreignKey: "teknisi_id",
      as: "node_type",
    });
    iots_teknisi.belongsTo(models.iot_users, {
      foreignKey: "username",
    });
  };
  return iots_teknisi;
};

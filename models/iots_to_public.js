'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class iots_to_public extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  iots_to_public.init({
    payload: DataTypes.TEXT,
    method: DataTypes.STRING,
    auth: DataTypes.TEXT,
    url: DataTypes.TEXT,
    status: DataTypes.TEXT,
    code: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'iots_to_public',
  });
  return iots_to_public;
};
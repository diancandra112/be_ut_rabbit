'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class iots_midtrans_callback extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  iots_midtrans_callback.init({
    invoice: DataTypes.STRING,
    status_code: DataTypes.STRING,
    payload: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'iots_midtrans_callback',
  });
  return iots_midtrans_callback;
};
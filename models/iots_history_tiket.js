"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iots_history_tiket extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  iots_history_tiket.init(
    {
      ticket_type: DataTypes.STRING,
      create_by: DataTypes.STRING,
      delegation_to: DataTypes.STRING,
      ticket_delegation_date: DataTypes.STRING,
      no_tiket: DataTypes.STRING,
      action: DataTypes.STRING,
      tiket_id: DataTypes.INTEGER,
      area_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "iots_history_tiket",
    }
  );
  return iots_history_tiket;
};

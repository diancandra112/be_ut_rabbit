'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class iots_billing_fine extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  iots_billing_fine.init({
    status: DataTypes.STRING,
    deskripsi: DataTypes.STRING,
    inv_id: DataTypes.INTEGER,
    jumlah_hari: DataTypes.INTEGER,
    jumlah_denda: DataTypes.DOUBLE,
    used: DataTypes.BOOLEAN,
    link_inv_denda: DataTypes.INTEGER,
    bln_inv: DataTypes.STRING,
    bln_denda: DataTypes.STRING,
    nodeId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'iots_billing_fine',
  });
  return iots_billing_fine;
};
"use strict";
module.exports = (sequelize, DataTypes) => {
  const allocation_type = sequelize.define(
    "allocation_type",
    {
      type_id: DataTypes.INTEGER,
      internal_id: DataTypes.INTEGER,
      tenant_id: DataTypes.INTEGER
    },
    {}
  );
  allocation_type.associate = function(models) {
    // associations can be defined here
    allocation_type.belongsTo(models.iot_node_type, {
      foreignKey: "type_id",
      targetKey: "type_id"
    });
  };
  return allocation_type;
};

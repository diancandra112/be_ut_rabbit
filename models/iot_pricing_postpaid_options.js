"use strict";
module.exports = (sequelize, DataTypes) => {
  const iot_pricing_postpaid_options = sequelize.define(
    "iot_pricing_postpaid_options",
    {
      postpaid_id: DataTypes.INTEGER,
      option_name: DataTypes.STRING
    },
    {}
  );
  iot_pricing_postpaid_options.associate = function(models) {
    // associations can be defined here
    iot_pricing_postpaid_options.belongsTo(models.iot_area, {
      foreignKey: "postpaid_id"
    });
  };
  return iot_pricing_postpaid_options;
};

'use strict';
module.exports = (sequelize, DataTypes) => {
  const iot_detail_gas = sequelize.define('iot_detail_gas', {
    last_update: DataTypes.DATE,
    meter_id: DataTypes.STRING,
    status_valve: DataTypes.INTEGER,
    battery_level: DataTypes.STRING,
    interval: DataTypes.INTEGER,
    desc: DataTypes.STRING,
    node_id: DataTypes.INTEGER,
    billing_type_id: DataTypes.INTEGER,
    start_totalizer: DataTypes.STRING,
    balance: DataTypes.STRING,
    installation_date: DataTypes.DATE
  }, {});
  iot_detail_gas.associate = function(models) {
    // associations can be defined here
    iot_detail_gas.belongsTo(models.iot_nodes,{foreignKey:'node_id'})
  };
  return iot_detail_gas;
};
"use strict";
module.exports = (sequelize, DataTypes) => {
  const iot_companies = sequelize.define(
    "iot_companies",
    {
      user_id: DataTypes.INTEGER,
      company_name: DataTypes.STRING,
      username: DataTypes.STRING,
      pic_name: DataTypes.STRING,
      phone: DataTypes.STRING,
      address: DataTypes.STRING,
      image: DataTypes.TEXT,
      handphone: DataTypes.STRING,
    },
    {}
  );
  iot_companies.associate = function (models) {
    // associations can be defined here
    iot_companies.hasMany(models.iot_area, {
      foreignKey: "company_id",
      as: "list_area",
    });
    iot_companies.belongsTo(models.iot_users, { foreignKey: "user_id" });
  };
  return iot_companies;
};

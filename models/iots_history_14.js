"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iots_history_14 extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      iots_history_14.hasMany(models.iots_history_14_detail, {
        foreignKey: "histori_id",
        as: "log",
      });
    }
  }
  iots_history_14.init(
    {
      device_id: DataTypes.INTEGER,
      reportTime: DataTypes.STRING,
      reportTimeFlag: DataTypes.STRING,
      tenantId: DataTypes.INTEGER,
      internalId: DataTypes.INTEGER,
      devEui: DataTypes.STRING,
      meter_id: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "iots_history_14",
    }
  );
  return iots_history_14;
};

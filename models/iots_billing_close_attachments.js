"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iots_billing_close_attachments extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      iots_billing_close_attachments.belongsTo(models.iots_billing_history, {
        foreignKey: "billing_id",
      });
    }
  }
  iots_billing_close_attachments.init(
    {
      billing_id: DataTypes.INTEGER,
      filename: DataTypes.STRING,
      mimetype: DataTypes.STRING,
      url: DataTypes.TEXT,
    },
    {
      sequelize,
      modelName: "iots_billing_close_attachments",
    }
  );
  return iots_billing_close_attachments;
};

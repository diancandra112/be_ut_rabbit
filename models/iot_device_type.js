'use strict';
module.exports = (sequelize, DataTypes) => {
  const iot_device_type = sequelize.define('iot_device_type', {
    name: DataTypes.STRING
  }, {});
  iot_device_type.associate = function(models) {
    // associations can be defined here
    iot_device_type.hasOne(models.iot_nodes,{foreignKey:'device_type_id'})

  };
  return iot_device_type;
};
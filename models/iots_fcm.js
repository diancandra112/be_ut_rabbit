"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iots_fcm extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  iots_fcm.init(
    {
      user_id: DataTypes.INTEGER,
      user_type: DataTypes.STRING,
      time: DataTypes.STRING,
      title: DataTypes.STRING,
      body: DataTypes.TEXT,
      areaId: DataTypes.INTEGER,
      is_read: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "iots_fcm",
    }
  );
  return iots_fcm;
};

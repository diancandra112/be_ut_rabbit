"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iot_histori_35 extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  iot_histori_35.init(
    {
      device_id: DataTypes.INTEGER,
      latitude: DataTypes.DOUBLE,
      longitude: DataTypes.DOUBLE,
      battery: DataTypes.STRING,
      report_time: DataTypes.STRING,
      report_time_flag: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "iot_histori_35",
    }
  );
  return iot_histori_35;
};

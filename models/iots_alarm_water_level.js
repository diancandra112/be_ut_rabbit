"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iots_alarm_water_level extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      iots_alarm_water_level.belongsTo(models.iot_area, {
        foreignKey: "areaId",
      });
    }
  }
  iots_alarm_water_level.init(
    {
      node_id: DataTypes.INTEGER,
      areaId: DataTypes.INTEGER,
      devEui: DataTypes.STRING,
      update_time: DataTypes.STRING,
      threshold: DataTypes.DOUBLE,
      threshold_type: DataTypes.STRING,
      meter_now: DataTypes.DOUBLE,
      location_name: DataTypes.STRING,
      location_type: DataTypes.ENUM("internal", "tenant"),
      grafik: DataTypes.TEXT,
    },
    {
      sequelize,
      modelName: "iots_alarm_water_level",
    }
  );
  return iots_alarm_water_level;
};

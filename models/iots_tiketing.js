"use strict";
module.exports = (sequelize, DataTypes) => {
  const iots_tiketing = sequelize.define(
    "iots_tiketing",
    {
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      phone: DataTypes.STRING,
      node_type: DataTypes.STRING,
      no_tiket: DataTypes.STRING,
      user_name: DataTypes.STRING,
      type_user: DataTypes.STRING,
      ticket_type: DataTypes.ENUM("installation", "maintenance", "unassigned"),
      technician_id: DataTypes.INTEGER,
      technician_name: DataTypes.STRING,
      type_user: DataTypes.ENUM("tenant", "internal"),
      user_id: DataTypes.INTEGER,
      area_id: DataTypes.INTEGER,
      note: DataTypes.TEXT,
      note_teknisi: DataTypes.TEXT,
      ticket_processing_date: DataTypes.DATE,
      ticket_close_date: DataTypes.DATE,
      ticket_delegation_date: DataTypes.DATE,
      status: DataTypes.ENUM("new", "onprogress", "close"),
      type_service: DataTypes.ENUM("gateway", "node"),
      installation_date: DataTypes.DATE,
      dev_eui: DataTypes.STRING,
      meter_id: DataTypes.STRING,
      billing_type: DataTypes.ENUM("PREPAID", "POSTPAID"),
      status_valve: DataTypes.STRING,
      battery_level: DataTypes.STRING,
      interval: DataTypes.STRING,
      gateway_name: DataTypes.STRING,
      unit_model: DataTypes.STRING,
      mac_address: DataTypes.STRING,
      merk: DataTypes.STRING,
      prepayment: DataTypes.DOUBLE,

      tenant_id: DataTypes.INTEGER,
      internal_id: DataTypes.INTEGER,
      node_id: DataTypes.INTEGER,
      summary: DataTypes.TEXT,

      create_by: DataTypes.STRING,
      create_by_type: DataTypes.STRING,
      create_by_id: DataTypes.INTEGER,

      start_totalizer: DataTypes.DOUBLE,
    },
    {}
  );
  iots_tiketing.associate = function (models) {
    // associations can be defined here
    iots_tiketing.belongsTo(models.iot_nodes, {
      foreignKey: "dev_eui",
    });
    iots_tiketing.hasMany(models.iots_tiketing_attachment, {
      foreignKey: "tiketing_id",
      as: "images",
    });
    iots_tiketing.hasMany(models.iots_tiketing_attachment, {
      foreignKey: "tiketing_id",
      as: "images_teknisi",
    });
    iots_tiketing.hasOne(models.iot_tenant, {
      foreignKey: "id",
      sourceKey: "user_id",
    });
    iots_tiketing.hasOne(models.iot_internal, {
      foreignKey: "id",
      sourceKey: "user_id",
    });
  };
  return iots_tiketing;
};

"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iots_histori_17 extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      iots_histori_17.hasMany(models.iots_type_17_details, {
        foreignKey: "histori_id",
      });
    }
  }
  iots_histori_17.init(
    {
      device_id: DataTypes.INTEGER,
      reportTime: DataTypes.STRING,
      reportTimeFlag: DataTypes.STRING,
      tenantId: DataTypes.INTEGER,
      internalId: DataTypes.INTEGER,
      devEui: DataTypes.STRING,
      meter_id: DataTypes.STRING,
      alarm: DataTypes.STRING,
      valve: DataTypes.STRING,
      battery: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "iots_histori_17",
    }
  );
  return iots_histori_17;
};

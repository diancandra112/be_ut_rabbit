"use strict";
module.exports = (sequelize, DataTypes) => {
  const iots_finance = sequelize.define(
    "iots_finance",
    {
      fullname: DataTypes.STRING,
      username: DataTypes.STRING,
      password: DataTypes.TEXT,
      email: DataTypes.STRING,
      phone: DataTypes.STRING,
      address: DataTypes.TEXT,
      area_id: DataTypes.INTEGER,
      images: DataTypes.TEXT,
      user_id: DataTypes.INTEGER,
      summary: DataTypes.TEXT,
    },
    {}
  );
  iots_finance.associate = function (models) {
    // associations can be defined here
    iots_finance.belongsTo(models.iot_users, { foreignKey: "user_id" });
  };
  return iots_finance;
};

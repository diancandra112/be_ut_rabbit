"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iot_detail_pju extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      iot_detail_pju.belongsTo(models.iot_nodes, { foreignKey: "node_id" });
    }
  }
  iot_detail_pju.init(
    {
      devEui: DataTypes.STRING,
      interval: DataTypes.INTEGER,
      desc: DataTypes.STRING,
      node_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "iot_detail_pju",
    }
  );
  return iot_detail_pju;
};

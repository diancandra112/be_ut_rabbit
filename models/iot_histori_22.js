'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class iot_histori_22 extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  iot_histori_22.init({
    reportTime: DataTypes.STRING,
    reportTimeFlag: DataTypes.STRING,
    device_id: DataTypes.INTEGER,
    tenantId: DataTypes.INTEGER,
    internalId: DataTypes.INTEGER,
    devEui: DataTypes.STRING,
    meter_id: DataTypes.STRING,
    status: DataTypes.STRING,
    battery: DataTypes.DOUBLE,
    temp: DataTypes.DOUBLE,
    lat: DataTypes.STRING,
    long: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'iot_histori_22',
  });
  return iot_histori_22;
};
"use strict";
module.exports = (sequelize, DataTypes) => {
  const iot_correction_history = sequelize.define(
    "iot_correction_history",
    {
      note: DataTypes.STRING,
      email: DataTypes.STRING,
      role: DataTypes.STRING,
      billing_id: DataTypes.INTEGER,
      edit_details: DataTypes.TEXT,
    },
    {}
  );
  iot_correction_history.associate = function (models) {
    // associations can be defined here
  };
  return iot_correction_history;
};

'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class iot_history_pressure_5 extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  iot_history_pressure_5.init({
    device_id: DataTypes.INTEGER,
    devEui: DataTypes.STRING,
    battrey_level: DataTypes.STRING,
    preassure: DataTypes.STRING,
    meter_id: DataTypes.STRING,
    tenantId: DataTypes.INTEGER,
    internalId: DataTypes.INTEGER,
    reportTime: DataTypes.STRING,
    reportTimeFlag: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'iot_history_pressure_5',
  });
  return iot_history_pressure_5;
};
"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iots_cutoff_saas extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      iots_cutoff_saas.belongsTo(models.iot_area, { foreignKey: "areaId" });
    }
  }
  iots_cutoff_saas.init(
    {
      areaId: DataTypes.INTEGER,
      tanggal_cutoff: DataTypes.INTEGER,
      order: DataTypes.INTEGER,
      time: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "iots_cutoff_saas",
    }
  );
  return iots_cutoff_saas;
};

"use strict";
module.exports = (sequelize, DataTypes) => {
  const iots_billing_history = sequelize.define(
    "iots_billing_history",
    {
      areaId: DataTypes.INTEGER,
      tenantId: DataTypes.INTEGER,
      nodeId: DataTypes.INTEGER,
      cut_date: DataTypes.STRING,
      start_meter: DataTypes.STRING,
      end_meter: DataTypes.STRING,
      start_date: DataTypes.STRING,
      end_date: DataTypes.STRING,
      totalizer: DataTypes.STRING,
      usage: DataTypes.STRING,
      invoice: DataTypes.STRING,
      ppn: DataTypes.INTEGER,
      harga_satuan: DataTypes.INTEGER,
      periode_cut: DataTypes.INTEGER,
      biaya_transaksi: DataTypes.INTEGER,
      biaya_penyesuaian: DataTypes.STRING,
      virtual_account: DataTypes.STRING,
      status: DataTypes.ENUM("CANCEL", "PAID", "UNPAID", "NEW"),
      correction_usage: DataTypes.STRING,
      billing_usage: DataTypes.STRING,
      denda: DataTypes.TEXT,
      periode_billing: DataTypes.STRING,
      due_date: DataTypes.STRING,
      deveui: DataTypes.STRING,
      node_type: DataTypes.STRING,
      unit: DataTypes.STRING,
      typeId: DataTypes.INTEGER,
      resultErp: DataTypes.TEXT,

      payment_method: DataTypes.STRING,
      payment_date: DataTypes.DATE,

      minimum_charge: DataTypes.DOUBLE,
      minimum_charge_total: DataTypes.DOUBLE,
      billing_type: DataTypes.ENUM("area", "tenant", "node"),

      log_area_id: DataTypes.INTEGER,
      log: DataTypes.TEXT,
      log_flag: DataTypes.BOOLEAN,
      no_meter: DataTypes.STRING,
      is_combine_billing: DataTypes.BOOLEAN,

      wa_rimender: DataTypes.TEXT,
      wa_raw: DataTypes.TEXT,
      wa_msgId: DataTypes.STRING,
      wa_status: DataTypes.STRING,
      wa_responseCode: DataTypes.STRING,

      erp_payload: DataTypes.TEXT,
      erp_id: DataTypes.INTEGER,

      materai: DataTypes.DOUBLE,

      finance_name: DataTypes.STRING,
      finance_id: DataTypes.INTEGER,
    },
    {}
  );
  iots_billing_history.associate = function (models) {
    // associations can be defined here
    iots_billing_history.hasMany(models.iots_wa_status, {
      foreignKey: "billing_id",
    });
    iots_billing_history.hasMany(models.iots_billing_close_attachments, {
      foreignKey: "billing_id",
    });
    iots_billing_history.hasMany(models.iots_billing_attachment, {
      foreignKey: "billingId",
      // as: "denda",
    });
    iots_billing_history.hasMany(models.iots_denda, {
      foreignKey: "history_id",
      // as: "denda",
    });
    iots_billing_history.belongsTo(models.iot_area, {
      foreignKey: "areaId",
      as: "history_billing",
    });
    iots_billing_history.belongsTo(models.iot_area, {
      foreignKey: "areaId",
    });
    iots_billing_history.hasOne(models.iot_nodes, {
      foreignKey: "id",
      sourceKey: "nodeId",
    });
    iots_billing_history.hasMany(models.iot_nodes, {
      foreignKey: "id",
      as: "last_billing",
      sourceKey: "nodeId",
    });
    iots_billing_history.belongsTo(models.iot_nodes, {
      foreignKey: "nodeId",
      as: "last_billing_desc",
      sourceKey: "id",
    });
    iots_billing_history.hasOne(models.iot_tenant, {
      foreignKey: "id",
      sourceKey: "tenantId",
    });
    iots_billing_history.belongsTo(models.iot_nodes, {
      foreignKey: "nodeId",
      as: "bill",
    });
  };
  return iots_billing_history;
};

"use strict";
module.exports = (sequelize, DataTypes) => {
  const iot_gateways = sequelize.define(
    "iot_gateways",
    {
      gateway_name: DataTypes.STRING,
      unit_model: DataTypes.STRING,
      mac_address: DataTypes.STRING,
      power_source: DataTypes.STRING,
      description: DataTypes.STRING,
      installation_date: DataTypes.DATE,
      latitude: DataTypes.STRING,
      longitude: DataTypes.STRING,
      area_id: DataTypes.INTEGER,
      last_update: DataTypes.DATE,
      status: DataTypes.BOOLEAN,
      line_status: DataTypes.BOOLEAN,
      notifikasi: DataTypes.STRING,
    },
    {}
  );
  iot_gateways.associate = function (models) {
    // associations can be defined here
    iot_gateways.belongsTo(models.iot_area, { foreignKey: "area_id" });
  };
  return iot_gateways;
};

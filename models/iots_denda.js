"use strict";
module.exports = (sequelize, DataTypes) => {
  const iots_denda = sequelize.define(
    "iots_denda",
    {
      history_id: DataTypes.INTEGER,
      periode: DataTypes.DATE,
      days: DataTypes.INTEGER,
      amount: DataTypes.STRING,
    },
    {}
  );
  iots_denda.associate = function (models) {
    // associations can be defined here
    iots_denda.belongsTo(models.iots_billing_history, {
      foreignKey: "history_id",
      // as: "denda",
    });
  };
  return iots_denda;
};

"use strict";
module.exports = (sequelize, DataTypes) => {
  const iot_member_level = sequelize.define(
    "iot_member_level",
    {
      area_id: DataTypes.INTEGER,
      name: DataTypes.STRING,
    },
    {}
  );
  iot_member_level.associate = function (models) {
    // associations can be defined here
    iot_member_level.belongsTo(models.iot_area, {
      foreignKey: "area_id",
      sourceKey: "area_id",
    });
    iot_member_level.hasMany(models.iot_tenant, {
      foreignKey: "member_level_id",
    });
    iot_member_level.hasMany(models.iot_pricing_postpaid, {
      foreignKey: "memberId",
      as: "child",
    });
  };
  return iot_member_level;
};

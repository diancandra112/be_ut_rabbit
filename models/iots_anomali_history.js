"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iots_anomali_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      iots_anomali_history.belongsTo(models.iot_tenant, {
        foreignKey: "tenantId",
      });
      iots_anomali_history.belongsTo(models.iot_internal, {
        foreignKey: "internalId",
      });
      iots_anomali_history.belongsTo(models.iot_node_type, {
        foreignKey: "typeid",
      });
      iots_anomali_history.belongsTo(models.iot_area, {
        foreignKey: "areaId",
      });
      iots_anomali_history.belongsTo(models.iot_nodes, {
        foreignKey: "deveui",
        targetKey: "devEui",
      });
    }
  }
  iots_anomali_history.init(
    {
      tenantId: DataTypes.INTEGER,
      internalId: DataTypes.INTEGER,
      last_update: DataTypes.STRING,
      usage: DataTypes.STRING,
      max_usage: DataTypes.STRING,
      deveui: DataTypes.STRING,
      status: DataTypes.BOOLEAN,
      valve: DataTypes.STRING,
      typeid: DataTypes.INTEGER,
      areaId: DataTypes.INTEGER,
      anomi_type: DataTypes.ENUM("USAGE INTERVAL", "USAGE TIME"),
    },
    {
      sequelize,
      modelName: "iots_anomali_history",
    }
  );
  return iots_anomali_history;
};

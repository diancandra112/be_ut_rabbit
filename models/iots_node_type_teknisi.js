"use strict";
module.exports = (sequelize, DataTypes) => {
  const iots_node_type_teknisi = sequelize.define(
    "iots_node_type_teknisi",
    {
      teknisi_id: DataTypes.INTEGER,
      type_id: DataTypes.INTEGER,
      type_name: DataTypes.STRING,
    },
    {}
  );
  iots_node_type_teknisi.associate = function (models) {
    // associations can be defined here
    iots_node_type_teknisi.belongsTo(models.iots_teknisi, {
      foreignKey: "teknisi_id",
    });
    iots_node_type_teknisi.belongsTo(models.iots_teknisi, {
      foreignKey: "teknisi_id",
      as: "node_type",
    });
  };
  return iots_node_type_teknisi;
};

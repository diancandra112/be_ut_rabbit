"use strict";
module.exports = (sequelize, DataTypes) => {
  const iot_users = sequelize.define(
    "iot_users",
    {
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      role_id: DataTypes.INTEGER,
      area_id: DataTypes.INTEGER,
    },
    {}
  );
  iot_users.associate = function (models) {
    // associations can be defined here
    iot_users.hasOne(models.iots_finance, { foreignKey: "user_id" });
    iot_users.belongsTo(models.iot_user_role, { foreignKey: "role_id" });
    iot_users.hasOne(models.iot_area, { foreignKey: "user_id" });
    iot_users.hasOne(models.iot_companies, { foreignKey: "user_id" });
    iot_users.hasOne(models.iot_tenant, { foreignKey: "user_id" });
    iot_users.hasOne(models.iots_teknisi, {
      foreignKey: "username",
      sourceKey: "email",
    });
  };
  return iot_users;
};

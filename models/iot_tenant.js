"use strict";
module.exports = (sequelize, DataTypes) => {
  const iot_tenant = sequelize.define(
    "iot_tenant",
    {
      user_id: DataTypes.INTEGER,
      area_id: DataTypes.INTEGER,
      email: DataTypes.STRING,
      username: DataTypes.STRING,
      password: DataTypes.TEXT,
      password_flag: DataTypes.TEXT,
      tenant_name: DataTypes.STRING,
      image: DataTypes.STRING,
      address: DataTypes.STRING,
      phone: DataTypes.STRING,
      handphone: DataTypes.STRING,
      member_id: DataTypes.STRING,
      member_level: DataTypes.STRING,
      member_level_id: DataTypes.INTEGER,
      erp_name: DataTypes.STRING,
      erp_id: DataTypes.INTEGER,
      erp_contract_name: DataTypes.STRING,
      erp_contract_id: DataTypes.INTEGER,

      anomali_usage_start_date: DataTypes.STRING,
      anomali_usage_end_date: DataTypes.STRING,

      minim_balance_gas: DataTypes.DOUBLE,
      minim_balance_water: DataTypes.DOUBLE,

      minimum_charge_gas: DataTypes.DOUBLE,
      minimum_charge_water: DataTypes.DOUBLE,
      minimum_charge_electricity_ct: DataTypes.DOUBLE,
      minimum_charge_electricity_non_ct: DataTypes.DOUBLE,

      is_ppn_value: DataTypes.INTEGER,
      is_ppn: DataTypes.BOOLEAN,

      company_name: DataTypes.STRING,
      billing_address: DataTypes.TEXT,

      tenant_close: DataTypes.BOOLEAN,

      id_deleted: DataTypes.BOOLEAN,
    },
    {}
  );
  iot_tenant.associate = function (models) {
    // associations can be defined here
    iot_tenant.belongsTo(models.iots_tiketing, {
      foreignKey: "id",
      targetKey: "user_id",
    });
    iot_tenant.belongsTo(models.iot_member_level, {
      foreignKey: "member_level_id",
    });
    iot_tenant.hasMany(models.iots_anomali_history, {
      foreignKey: "tenantId",
    });
    iot_tenant.hasMany(models.iots_billing_prepaid, {
      foreignKey: "tenant_id",
    });
    iot_tenant.hasMany(models.iot_nodes, {
      foreignKey: "tenantId",
    });
    iot_tenant.belongsTo(models.iots_billing_history, {
      foreignKey: "id",
      targetKey: "tenantId",
    });
    iot_tenant.belongsTo(models.iot_area, {
      foreignKey: "area_id",
      as: "list_tenant",
    });
    iot_tenant.belongsTo(models.iot_users, { foreignKey: "user_id" });
  };
  return iot_tenant;
};

'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class iots_histori_18 extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  iots_histori_18.init({
    device_id: DataTypes.INTEGER,
    reportTime: DataTypes.STRING,
    reportTimeFlag: DataTypes.STRING,
    tenantId: DataTypes.INTEGER,
    internalId: DataTypes.INTEGER,
    devEui: DataTypes.STRING,
    status: DataTypes.TEXT,
    battery: DataTypes.DOUBLE,
    board_temp: DataTypes.DOUBLE,
    rh: DataTypes.DOUBLE,
    iaq: DataTypes.DOUBLE,
    iaq_quality: DataTypes.STRING,
    environment_temp: DataTypes.DOUBLE
  }, {
    sequelize,
    modelName: 'iots_histori_18',
  });
  return iots_histori_18;
};
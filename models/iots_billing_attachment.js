"use strict";
module.exports = (sequelize, DataTypes) => {
  const iots_billing_attachment = sequelize.define(
    "iots_billing_attachment",
    {
      file_name: DataTypes.STRING,
      billingId: DataTypes.INTEGER,
      file_pdf: DataTypes.STRING,
      default: DataTypes.INTEGER,
      status_id: DataTypes.INTEGER,
      status_name: DataTypes.STRING,
    },
    {}
  );
  iots_billing_attachment.associate = function (models) {
    // associations can be defined here
    iots_billing_attachment.belongsTo(models.iots_billing_history, {
      foreignKey: "billingId",
      // as: "denda",
    });
  };
  return iots_billing_attachment;
};

"use strict";
module.exports = (sequelize, DataTypes) => {
  const iot_detail_rtu = sequelize.define(
    "iot_detail_rtu",
    {
      last_update: DataTypes.DATE,
      serial_number: DataTypes.STRING,
      interval: DataTypes.INTEGER,
      model: DataTypes.STRING,
      node_id: DataTypes.INTEGER,
      field_billing: DataTypes.STRING,
    },
    {}
  );
  iot_detail_rtu.associate = function (models) {
    // associations can be defined here
    iot_detail_rtu.belongsTo(models.iot_nodes, { foreignKey: "node_id" });
  };
  return iot_detail_rtu;
};

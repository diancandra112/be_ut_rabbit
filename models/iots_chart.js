"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iots_chart extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  iots_chart.init(
    {
      areaId: DataTypes.INTEGER,
      typeId: DataTypes.INTEGER,
      payload: DataTypes.TEXT,
      type: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "iots_chart",
    }
  );
  return iots_chart;
};

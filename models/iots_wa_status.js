"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iots_wa_status extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      iots_wa_status.belongsTo(models.iots_billing_history, {
        foreignKey: "billing_id",
      });
    }
  }
  iots_wa_status.init(
    {
      billing_id: DataTypes.INTEGER,
      wa_raw: DataTypes.TEXT,
      wa_msgId: DataTypes.STRING,
      wa_status: DataTypes.STRING,
      wa_responseCode: DataTypes.STRING,
      wa_number: DataTypes.STRING,
      note: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "iots_wa_status",
    }
  );
  return iots_wa_status;
};

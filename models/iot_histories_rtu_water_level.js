"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iot_histories_rtu_water_level extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  iot_histories_rtu_water_level.init(
    {
      sensor_to_botom: DataTypes.DOUBLE,
      water_surface_to_bottom: DataTypes.DOUBLE,
      water_level: DataTypes.DOUBLE,
      reportTime: DataTypes.STRING,
      reportTimeFlag: DataTypes.STRING,
      device_id: DataTypes.INTEGER,
      tenantId: DataTypes.INTEGER,
      internalId: DataTypes.INTEGER,
      devEui: DataTypes.STRING,
      meter_id: DataTypes.STRING,
      alarm_high: DataTypes.BOOLEAN,
      alarm_low: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "iot_histories_rtu_water_level",
    }
  );
  return iot_histories_rtu_water_level;
};

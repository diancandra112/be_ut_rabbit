"use strict";
module.exports = (sequelize, DataTypes) => {
  const iot_internal = sequelize.define(
    "iot_internal",
    {
      user_id: DataTypes.INTEGER,
      area_id: DataTypes.INTEGER,
      email: DataTypes.STRING,
      internal_name: DataTypes.STRING,
      location: DataTypes.STRING,
      phone: DataTypes.STRING,
      handphone: DataTypes.STRING,
      pic: DataTypes.STRING,
    },
    {}
  );
  iot_internal.associate = function (models) {
    // associations can be defined here
    iot_internal.hasMany(models.iots_anomali_history, {
      foreignKey: "internalId",
    });
    iot_internal.belongsTo(models.iots_tiketing, {
      foreignKey: "id",
      targetKey: "user_id",
    });
    iot_internal.belongsTo(models.iot_area, { foreignKey: "area_id" });
    iot_internal.hasMany(models.iot_nodes, { foreignKey: "internalId" });
  };
  return iot_internal;
};

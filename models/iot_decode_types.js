'use strict';
module.exports = (sequelize, DataTypes) => {
  const iot_decode_types = sequelize.define('iot_decode_types', {
    company_name: DataTypes.STRING
  }, {});
  iot_decode_types.associate = function(models) {
    // associations can be defined here
  };
  return iot_decode_types;
};
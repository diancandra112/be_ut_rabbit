"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iot_histori_11_detail extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      iot_histori_11_detail.belongsTo(models.iot_histori_11, {
        foreignKey: "log_id",
      });
    }
  }
  iot_histori_11_detail.init(
    {
      device_id: DataTypes.INTEGER,
      log_id: DataTypes.INTEGER,
      name: DataTypes.STRING,
      value: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "iot_histori_11_detail",
    }
  );
  return iot_histori_11_detail;
};

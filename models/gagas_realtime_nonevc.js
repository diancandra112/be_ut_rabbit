"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class gagas_realtime_nonevc extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  gagas_realtime_nonevc.init(
    {
      idrefcustomer: DataTypes.STRING,
      fdate: DataTypes.STRING,
      fyear: DataTypes.INTEGER,
      fmonth: DataTypes.INTEGER,
      fday: DataTypes.INTEGER,
      fhour: DataTypes.INTEGER,
      fminute: DataTypes.INTEGER,
      fsecond: DataTypes.INTEGER,
      fv: DataTypes.DOUBLE,
      fp: DataTypes.DOUBLE,
      fvcs: DataTypes.DOUBLE,
      attribute1: DataTypes.STRING,
      attirbute2: DataTypes.STRING,
      reffcreatedby: DataTypes.STRING,
      fvu: DataTypes.DOUBLE,
      pinlet: DataTypes.DOUBLE,

      status: DataTypes.STRING,
      status_msg: DataTypes.TEXT,
    },
    {
      sequelize,
      modelName: "gagas_realtime_nonevc",
    }
  );
  return gagas_realtime_nonevc;
};

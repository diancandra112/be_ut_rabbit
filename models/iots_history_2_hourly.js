"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iots_history_2_hourly extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  iots_history_2_hourly.init(
    {
      nodeId: DataTypes.INTEGER,
      areaId: DataTypes.INTEGER,
      tenantId: DataTypes.INTEGER,
      internalId: DataTypes.INTEGER,
      fdate: DataTypes.STRING,
      fyear: DataTypes.STRING,
      fmonth: DataTypes.STRING,
      fday: DataTypes.STRING,
      fhour: DataTypes.STRING,
      fminute: DataTypes.STRING,
      totalizer: DataTypes.DOUBLE,
    },
    {
      sequelize,
      modelName: "iots_history_2_hourly",
    }
  );
  return iots_history_2_hourly;
};

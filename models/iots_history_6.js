"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iots_history_6 extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      iots_history_6.hasMany(models.iots_history_6_ex, {
        foreignKey: "log_id",
      });
    }
  }
  iots_history_6.init(
    {
      device_id: DataTypes.INTEGER,
      reporttime: DataTypes.STRING,
      reporttimeflag: DataTypes.STRING,
      tenantid: DataTypes.INTEGER,
      internalid: DataTypes.INTEGER,
      deveui: DataTypes.STRING,
      meter_id: DataTypes.STRING,
      battrey_level: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "iots_history_6",
    }
  );
  return iots_history_6;
};

"use strict";
module.exports = (sequelize, DataTypes) => {
  const iot_pricing_postpaid = sequelize.define(
    "iot_pricing_postpaid",
    {
      typeId: DataTypes.INTEGER,
      memberId: DataTypes.INTEGER,
      pricing: DataTypes.STRING,
      item_id: DataTypes.INTEGER,
      unit_id: DataTypes.INTEGER,
      Item_name: DataTypes.STRING,
    },
    {}
  );
  iot_pricing_postpaid.associate = function (models) {
    // associations can be defined here
    iot_pricing_postpaid.belongsTo(models.iot_member_level, {
      foreignKey: "memberId",
      as: "child",
    });
    iot_pricing_postpaid.hasOne(models.iot_node_type, {
      foreignKey: "type_id",
      sourceKey: "typeId",
    });
  };
  return iot_pricing_postpaid;
};

"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iots_history_14_detail extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      iots_history_14_detail.belongsTo(models.iots_history_14, {
        foreignKey: "histori_id",
        as: "log",
      });
    }
  }
  iots_history_14_detail.init(
    {
      name: DataTypes.STRING,
      value: DataTypes.STRING,
      histori_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "iots_history_14_detail",
    }
  );
  return iots_history_14_detail;
};

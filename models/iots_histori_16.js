"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iots_histori_16 extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      iots_histori_16.hasMany(models.iots_type_16_detail, {
        foreignKey: "histori_id",
      });
    }
  }
  iots_histori_16.init(
    {
      device_id: DataTypes.INTEGER,
      reportTime: DataTypes.STRING,
      reportTimeFlag: DataTypes.STRING,
      tenantId: DataTypes.INTEGER,
      internalId: DataTypes.INTEGER,
      devEui: DataTypes.STRING,
      meter_id: DataTypes.STRING,
      child_app_name: DataTypes.STRING,
      child_sn: DataTypes.STRING,
      log: DataTypes.JSON,
    },
    {
      sequelize,
      modelName: "iots_histori_16",
    }
  );
  return iots_histori_16;
};

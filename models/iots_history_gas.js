"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iots_history_gas extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  iots_history_gas.init(
    {
      device_id: DataTypes.INTEGER,
      meterReading: DataTypes.DOUBLE,
      reportTime: DataTypes.STRING,
      reportTimeFlag: DataTypes.STRING,
      tenantId: DataTypes.INTEGER,
      internalId: DataTypes.INTEGER,
      devEui: DataTypes.STRING,
      meter_id: DataTypes.STRING,
      valve: DataTypes.STRING,
      battrey_level: DataTypes.STRING,
      max_totalizer: DataTypes.DOUBLE,
    },
    {
      sequelize,
      modelName: "iots_history_gas",
    }
  );
  return iots_history_gas;
};

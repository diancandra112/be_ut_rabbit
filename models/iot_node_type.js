"use strict";
module.exports = (sequelize, DataTypes) => {
  const iot_node_type = sequelize.define(
    "iot_node_type",
    {
      type_name: DataTypes.STRING,
      type_id: DataTypes.INTEGER,
      active: DataTypes.BOOLEAN,
      satuan: DataTypes.STRING,
    },
    {}
  );
  iot_node_type.associate = function (models) {
    // associations can be defined here
    iot_node_type.belongsTo(models.iot_pricing_billing_area, {
      foreignKey: "type_id",
      targetKey: "typeId",
    });
    iot_node_type.hasMany(models.iots_anomali_history, {
      foreignKey: "typeid",
    });
    iot_node_type.hasOne(models.allocation_type, { foreignKey: "type_id" });
    iot_node_type.hasOne(models.iot_nodes, {
      foreignKey: "typeId",
      sourceKey: "type_id",
      // constraints: true
    });
    iot_node_type.belongsTo(models.iot_pricing_postpaid_all, {
      foreignKey: "type_id",
    });

    iot_node_type.belongsTo(models.iot_pricing_postpaid, {
      foreignKey: "type_id",
      // targetKet: "type_id"
    });
  };
  return iot_node_type;
};

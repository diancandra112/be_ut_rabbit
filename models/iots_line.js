'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class iots_line extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  iots_line.init({
    area_id: DataTypes.INTEGER,
    line_id: DataTypes.STRING,
    username_line: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'iots_line',
  });
  return iots_line;
};
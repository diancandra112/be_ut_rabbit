"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iot_histori_11 extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      iot_histori_11.hasMany(models.iot_histori_11_detail, {
        foreignKey: "log_id",
      });
    }
  }
  iot_histori_11.init(
    {
      device_id: DataTypes.INTEGER,
      meterReading: DataTypes.DOUBLE,
      reportTime: DataTypes.STRING,
      reportTimeFlag: DataTypes.STRING,
      tenantId: DataTypes.INTEGER,
      internalId: DataTypes.INTEGER,
      devEui: DataTypes.STRING,
      meter_id: DataTypes.STRING,
      valve: DataTypes.STRING,
      battrey_level: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "iot_histori_11",
    }
  );
  return iot_histori_11;
};

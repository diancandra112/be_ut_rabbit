"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iots_type_17_details extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      iots_type_17_details.belongsTo(models.iots_histori_17, {
        foreignKey: "histori_id",
      });
    }
  }
  iots_type_17_details.init(
    {
      name: DataTypes.STRING,
      value: DataTypes.STRING,
      histori_id: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "iots_type_17_details",
    }
  );
  return iots_type_17_details;
};

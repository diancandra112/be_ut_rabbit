'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class iot_history_202010 extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  iot_history_202010.init({
    device_id: DataTypes.STRING,
    usage: DataTypes.STRING,
    reporttime: DataTypes.STRING,
    payload: DataTypes.TEXT,
    payload_antares: DataTypes.TEXT,
    reporttimeflag: DataTypes.STRING,
    to_company_response: DataTypes.TEXT,
    to_company_payload: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'iot_history_202010',
  });
  return iot_history_202010;
};
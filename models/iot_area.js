"use strict";
module.exports = (sequelize, DataTypes) => {
  const iot_area = sequelize.define(
    "iot_area",
    {
      user_id: DataTypes.INTEGER,
      company_id: DataTypes.INTEGER,
      pricing_option: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: "1",
      },
      automatic_bill: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: "1",
      },
      email: DataTypes.STRING,
      area_name: DataTypes.STRING,
      username: DataTypes.STRING,
      stamp_number: DataTypes.STRING,
      stamp_date: DataTypes.STRING,
      pic_name: DataTypes.TEXT,
      email_pic: DataTypes.TEXT,
      phone: DataTypes.TEXT,
      address: DataTypes.STRING,
      image: DataTypes.TEXT,
      handphone: DataTypes.STRING,
      url_to_company: DataTypes.STRING,
      decode_id: DataTypes.INTEGER,
      company_token: DataTypes.TEXT,
      url_erp: DataTypes.TEXT,
      email_erp: DataTypes.TEXT,
      pass_erp: DataTypes.TEXT,
      is_erp: DataTypes.BOOLEAN,
      is_line: DataTypes.BOOLEAN,
      auto_paid_prepaid: DataTypes.BOOLEAN,

      anomali_usage_start_date: DataTypes.STRING,
      anomali_usage_end_date: DataTypes.STRING,

      minim_balance_gas: DataTypes.DOUBLE,
      minim_balance_water: DataTypes.DOUBLE,

      pressure: DataTypes.DOUBLE,
      water_pressure: DataTypes.DOUBLE,
      pressure_pt: DataTypes.DOUBLE,
      low_pressure: DataTypes.DOUBLE,

      interval_pressure: DataTypes.DOUBLE,
      interval_water_pressure: DataTypes.DOUBLE,
      interval_pressure_pt: DataTypes.DOUBLE,
      interval_low_pressure: DataTypes.DOUBLE,

      minimum_charge_gas: DataTypes.DOUBLE,
      minimum_charge_water: DataTypes.DOUBLE,
      minimum_charge_electricity_ct: DataTypes.DOUBLE,
      minimum_charge_electricity_non_ct: DataTypes.DOUBLE,
      billing_charge_type: DataTypes.ENUM("area", "tenant"),

      erp_name: DataTypes.STRING,
      erp_id: DataTypes.INTEGER,
      erp_contract_name: DataTypes.STRING,
      erp_contract_id: DataTypes.INTEGER,

      is_ppn_value: DataTypes.INTEGER,
      is_ppn: DataTypes.BOOLEAN,
      nomor_pelanggan: DataTypes.TEXT,

      saas: DataTypes.BOOLEAN,
      saas_expired: DataTypes.STRING,
      saas_cut_date: DataTypes.STRING,
      saas_price: DataTypes.DOUBLE,

      saas_item_id: DataTypes.INTEGER,
      saas_unit_id: DataTypes.INTEGER,
      saas_Item_name: DataTypes.STRING,

      saas_30: DataTypes.STRING,
      saas_7: DataTypes.STRING,
      saas_3: DataTypes.STRING,

      company_name: DataTypes.STRING,
      billing_address: DataTypes.TEXT,

      admin_pic_email: DataTypes.TEXT,
    },
    {}
  );
  iot_area.associate = function (models) {
    // associations can be defined here
    iot_area.hasMany(models.iots_steal_alarm, { foreignKey: "areaId" });

    iot_area.hasMany(models.iots_cutoff_saas, { foreignKey: "areaId" });
    iot_area.hasMany(models.iots_alarm_water_level, { foreignKey: "areaId" });
    iot_area.hasMany(models.iot_pricing_billing_area, { foreignKey: "areaId" });
    iot_area.hasMany(models.iots_kerusakan_gas, { foreignKey: "areaId" });

    iot_area.hasOne(models.iots_erp_item, { foreignKey: "areaId" });
    iot_area.hasMany(models.iot_gateways, { foreignKey: "area_id" });
    iot_area.hasMany(models.iots_anomali_history, { foreignKey: "areaId" });
    iot_area.hasMany(models.iot_nodes, { foreignKey: "areaId" });
    iot_area.hasMany(models.iot_member_level, { foreignKey: "area_id" });
    iot_area.hasMany(models.iot_pricing_postpaid_all, { foreignKey: "areaId" });
    iot_area.hasMany(models.iots_billing_history, { foreignKey: "areaId" });
    iot_area.hasOne(models.iots_billing_history, {
      foreignKey: "areaId",
      as: "history_billing",
    });
    iot_area.hasMany(models.iots_cutoff, { foreignKey: "areaId" });
    iot_area.hasOne(models.iots_cutoff, {
      foreignKey: "areaId",
      as: "cut_off",
    });
    iot_area.hasOne(models.iot_pricing_postpaid_options, {
      foreignKey: "postpaid_id",
      sourceKey: "pricing_option",
    });
    iot_area.belongsTo(models.iot_companies, {
      foreignKey: "company_id",
      as: "list_area",
    });
    iot_area.belongsTo(models.iot_users, { foreignKey: "user_id" });
    iot_area.hasMany(models.iot_tenant, {
      foreignKey: "area_id",
      as: "list_tenant",
    });
    iot_area.hasMany(models.iot_internal, {
      foreignKey: "area_id",
      as: "list_internal",
    });
  };
  return iot_area;
};

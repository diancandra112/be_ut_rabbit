'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class iot_history_23 extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  iot_history_23.init({
    sensor_to_botom: DataTypes.DOUBLE,
    water_depth: DataTypes.DOUBLE,
    reportTime: DataTypes.STRING,
    reportTimeFlag: DataTypes.STRING,
    device_id: DataTypes.INTEGER,
    tenantId: DataTypes.INTEGER,
    internalId: DataTypes.INTEGER,
    devEui: DataTypes.STRING,
    meter_id: DataTypes.STRING,
    alarm_high: DataTypes.BOOLEAN,
    alarm_low: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'iot_history_23',
  });
  return iot_history_23;
};
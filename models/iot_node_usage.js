'use strict';
module.exports = (sequelize, DataTypes) => {
  const iot_node_usage = sequelize.define('iot_node_usage', {
    bulan: DataTypes.STRING,
    tahun: DataTypes.STRING,
    devEui: DataTypes.STRING,
    startDate: DataTypes.DATE,
    endDate: DataTypes.DATE,
    startMeter: DataTypes.STRING,
    endMeter: DataTypes.STRING,
    usage: DataTypes.STRING
  }, {});
  iot_node_usage.associate = function(models) {
    // associations can be defined here
  };
  return iot_node_usage;
};
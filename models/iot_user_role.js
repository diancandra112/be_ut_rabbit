'use strict';
module.exports = (sequelize, DataTypes) => {
  const iot_user_role = sequelize.define('iot_user_role', {
    role: DataTypes.STRING
  }, {});
  iot_user_role.associate = function(models) {
    // associations can be defined here
    iot_user_role.hasMany(models.iot_users,{foreignKey:'role_id'})
  };
  return iot_user_role;
};
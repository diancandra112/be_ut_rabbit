"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iots_billing_prepaid extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      iots_billing_prepaid.belongsTo(models.iot_tenant, {
        foreignKey: "tenant_id",
      });
      iots_billing_prepaid.belongsTo(models.iot_nodes, {
        foreignKey: "device_id",
      });
    }
  }
  iots_billing_prepaid.init(
    {
      order_date: DataTypes.DATE,
      tenant_id: DataTypes.INTEGER,
      tenant_name: DataTypes.STRING,
      device_id: DataTypes.INTEGER,
      device_deveui: DataTypes.STRING,
      area_id: DataTypes.INTEGER,
      product_id: DataTypes.INTEGER,
      product_name: DataTypes.STRING,
      product_value: DataTypes.STRING,
      product_price: DataTypes.INTEGER,
      status: DataTypes.ENUM("CANCEL", "PAID", "UNPAID"),
      no_order: DataTypes.STRING,
      va_bca: DataTypes.STRING,
      va_permata: DataTypes.STRING,
      satuan_vc: DataTypes.STRING,
      type_vc: DataTypes.STRING,
      payment_date: DataTypes.DATE,

      payment_qris: DataTypes.BOOLEAN,
      qris_string: DataTypes.TEXT,
      qris_timeout: DataTypes.STRING,
      qris_order_id: DataTypes.STRING,
      qris_nominal: DataTypes.DOUBLE,
    },
    {
      sequelize,
      modelName: "iots_billing_prepaid",
    }
  );
  return iots_billing_prepaid;
};

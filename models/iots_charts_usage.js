"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iots_charts_usage extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      iots_charts_usage.belongsTo(models.iot_nodes, {
        foreignKey: "nodeId",
      });
    }
  }
  iots_charts_usage.init(
    {
      nodeId: DataTypes.INTEGER,
      typeId: DataTypes.INTEGER,
      areaId: DataTypes.INTEGER,
      fdate: DataTypes.STRING,
      fhour: DataTypes.STRING,
      startMeter: DataTypes.DOUBLE,
      endMeter: DataTypes.DOUBLE,
      usage: DataTypes.DOUBLE,
      type: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "iots_charts_usage",
    }
  );
  return iots_charts_usage;
};

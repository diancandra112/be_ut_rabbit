"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iots_kerusakan_gas extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      iots_kerusakan_gas.belongsTo(models.iot_area, { foreignKey: "areaId" });
    }
  }
  iots_kerusakan_gas.init(
    {
      areaId: DataTypes.INTEGER,
      node_gas_id: DataTypes.INTEGER,
      node_gas_devEui: DataTypes.STRING,
      node_gas_last_update: DataTypes.STRING,
      node_pressure_id: DataTypes.INTEGER,
      node_pressure_devEui: DataTypes.STRING,
      node_pressure_date_1: DataTypes.STRING,
      node_pressure_date_2: DataTypes.STRING,
      node_pressure_meter_1: DataTypes.DOUBLE,
      node_pressure_meter_2: DataTypes.DOUBLE,
      node_gas_totalizer: DataTypes.DOUBLE,
      is_notif: DataTypes.BOOLEAN,
      location_type: DataTypes.STRING,
      location: DataTypes.STRING,
      chart_press: DataTypes.TEXT,
    },
    {
      sequelize,
      modelName: "iots_kerusakan_gas",
    }
  );
  return iots_kerusakan_gas;
};

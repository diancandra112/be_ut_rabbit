"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iots_history_water extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  iots_history_water.init(
    {
      device_id: DataTypes.INTEGER,
      meterreading: DataTypes.DOUBLE,
      reporttime: DataTypes.STRING,
      reporttimeflag: DataTypes.STRING,
      tenantid: DataTypes.INTEGER,
      internalid: DataTypes.INTEGER,
      deveui: DataTypes.STRING,
      meter_id: DataTypes.STRING,
      valve: DataTypes.STRING,
      battrey_level: DataTypes.STRING,
      max_totalizer: DataTypes.BOOLEAN,
      timereport: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "iots_history_water",
    }
  );
  return iots_history_water;
};

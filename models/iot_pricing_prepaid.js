"use strict";
module.exports = (sequelize, DataTypes) => {
  const iot_pricing_prepaid = sequelize.define(
    "iot_pricing_prepaid",
    {
      product_name: DataTypes.STRING,
      price: DataTypes.INTEGER,
      value: DataTypes.STRING,
      status: DataTypes.BOOLEAN,
      areaId: DataTypes.INTEGER,
      type: DataTypes.STRING,
      satuan: DataTypes.STRING,
    },
    {}
  );
  iot_pricing_prepaid.associate = function (models) {
    // associations can be defined here
  };
  return iot_pricing_prepaid;
};

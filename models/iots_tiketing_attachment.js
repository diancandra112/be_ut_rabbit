"use strict";
module.exports = (sequelize, DataTypes) => {
  const iots_tiketing_attachment = sequelize.define(
    "iots_tiketing_attachment",
    {
      tiketing_id: DataTypes.INTEGER,
      image_url: DataTypes.TEXT,
      is_teknisi: DataTypes.BOOLEAN,
    },
    {}
  );
  iots_tiketing_attachment.associate = function (models) {
    // associations can be defined here
    iots_tiketing_attachment.belongsTo(models.iots_tiketing, {
      foreignKey: "tiketing_id",
      as: "images",
    });
    iots_tiketing_attachment.belongsTo(models.iots_tiketing, {
      foreignKey: "tiketing_id",
      as: "images_teknisi",
    });
  };
  return iots_tiketing_attachment;
};

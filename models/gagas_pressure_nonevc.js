"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class gagas_pressure_nonevc extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  gagas_pressure_nonevc.init(
    {
      id_meter: DataTypes.STRING,
      pressure: DataTypes.STRING,
      timestamp: DataTypes.STRING,

      status: DataTypes.STRING,
      status_msg: DataTypes.TEXT,
    },
    {
      sequelize,
      modelName: "gagas_pressure_nonevc",
    }
  );
  return gagas_pressure_nonevc;
};

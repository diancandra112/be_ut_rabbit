"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iot_pricing_billing_area extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      iot_pricing_billing_area.belongsTo(models.iot_area, {
        foreignKey: "areaId",
      });
      iot_pricing_billing_area.hasOne(models.iot_node_type, {
        foreignKey: "type_id",
        sourceKey: "typeId",
      });
    }
  }
  iot_pricing_billing_area.init(
    {
      areaId: DataTypes.INTEGER,
      typeId: DataTypes.INTEGER,
      pricing: DataTypes.DOUBLE,
      item_id: DataTypes.INTEGER,
      unit_id: DataTypes.INTEGER,
      Item_name: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "iot_pricing_billing_area",
    }
  );
  return iot_pricing_billing_area;
};

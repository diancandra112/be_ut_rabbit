"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iots_erp_item extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      iots_erp_item.belongsTo(models.iot_area, { foreignKey: "areaId" });
    }
  }
  iots_erp_item.init(
    {
      materai_name: DataTypes.STRING,
      materai_item_id: DataTypes.STRING,
      materai_unit_id: DataTypes.STRING,
      denda_name: DataTypes.STRING,
      denda_item_id: DataTypes.STRING,
      denda_unit_id: DataTypes.STRING,
      admin_name: DataTypes.STRING,
      admin_item_id: DataTypes.STRING,
      admin_unit_id: DataTypes.STRING,
      piutang_coa_id: DataTypes.STRING,
      piutang_coa_no: DataTypes.STRING,
      piutang_coa_name: DataTypes.STRING,
      penjualan_coa_id: DataTypes.STRING,
      penjualan_coa_no: DataTypes.STRING,
      penjualan_coa_name: DataTypes.STRING,
      ppn_coa_id: DataTypes.STRING,
      ppn_coa_no: DataTypes.STRING,
      ppn_coa_name: DataTypes.STRING,
      materai_coa_id: DataTypes.STRING,
      materai_coa_no: DataTypes.STRING,
      materai_coa_name: DataTypes.STRING,
      admin_coa_id: DataTypes.STRING,
      admin_coa_no: DataTypes.STRING,
      admin_coa_name: DataTypes.STRING,
      denda_coa_id: DataTypes.STRING,
      denda_coa_no: DataTypes.STRING,
      denda_coa_name: DataTypes.STRING,
      areaId: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "iots_erp_item",
    }
  );
  return iots_erp_item;
};

'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class iots_to_anaheim extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  iots_to_anaheim.init({
    payload: DataTypes.TEXT,
    method: DataTypes.STRING,
    auth: DataTypes.STRING,
    url: DataTypes.STRING,
    response: DataTypes.TEXT,
    code: DataTypes.STRING,
    date: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'iots_to_anaheim',
  });
  return iots_to_anaheim;
};
"use strict";
module.exports = (sequelize, DataTypes) => {
  const iot_pricing_postpaid_all = sequelize.define(
    "iot_pricing_postpaid_all",
    {
      areaId: DataTypes.INTEGER,
      typeId: DataTypes.INTEGER,
      pricing: DataTypes.STRING,
      item_id: DataTypes.INTEGER,
      unit_id: DataTypes.INTEGER,
      Item_name: DataTypes.STRING,
    },
    {}
  );
  iot_pricing_postpaid_all.associate = function (models) {
    iot_pricing_postpaid_all.belongsTo(models.iot_area, {
      foreignKey: "areaId",
    });
    iot_pricing_postpaid_all.hasOne(models.iot_node_type, {
      foreignKey: "type_id",
      sourceKey: "typeId",
    });

    iot_pricing_postpaid_all.belongsTo(models.iot_nodes, {
      foreignKey: "areaId",
      targetKey: "areaId",
    });

    // associations can be defined here
  };
  return iot_pricing_postpaid_all;
};

"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class iots_history_6_ex extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      iots_history_6_ex.belongsTo(models.iots_history_6, {
        foreignKey: "log_id",
      });
    }
  }
  iots_history_6_ex.init(
    {
      device_id: DataTypes.INTEGER,
      log_id: DataTypes.INTEGER,
      name: DataTypes.STRING,
      value: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "iots_history_6_ex",
    }
  );
  return iots_history_6_ex;
};

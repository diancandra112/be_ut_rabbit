"use strict";
module.exports = (sequelize, DataTypes) => {
  const iots_billing_email_attachments = sequelize.define(
    "iots_billing_email_attachments",
    {
      areaId: DataTypes.INTEGER,
      attachment_name: DataTypes.STRING,
    },
    {}
  );
  iots_billing_email_attachments.associate = function (models) {
    // associations can be defined here
  };
  return iots_billing_email_attachments;
};

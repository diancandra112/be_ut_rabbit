'use strict';
module.exports = (sequelize, DataTypes) => {
  const iot_detail_preassure = sequelize.define('iot_detail_preassure', {
    last_update: DataTypes.DATE,
    devEui: DataTypes.STRING,
    battery_level: DataTypes.STRING,
    interval: DataTypes.INTEGER,
    desc: DataTypes.STRING,
    node_id: DataTypes.INTEGER
  }, {});
  iot_detail_preassure.associate = function(models) {
    // associations can be defined here
    iot_detail_preassure.belongsTo(models.iot_nodes,{foreignKey:'node_id'})

  };
  return iot_detail_preassure;
};
"use strict";
module.exports = (sequelize, DataTypes) => {
  const iot_histories = sequelize.define(
    "iot_histories",
    {
      device_id: DataTypes.STRING,
      usage: DataTypes.STRING,
      meterReading: DataTypes.STRING,
      reportTime: DataTypes.STRING,
      payload: DataTypes.TEXT,
      payload_antares: DataTypes.TEXT,
      reportTimeFlag: DataTypes.STRING,
      to_company_response: DataTypes.TEXT,
      to_company_payload: DataTypes.TEXT,
      tenantId: DataTypes.INTEGER,
      internalId: DataTypes.INTEGER,
      devEui: DataTypes.STRING,
      meter_id: DataTypes.STRING,
      balance: DataTypes.FLOAT,
    },
    {}
  );
  iot_histories.associate = function (models) {
    // associations can be defined here
    iot_histories.belongsTo(models.iot_nodes, { foreignKey: "device_id" });
    iot_histories.belongsTo(models.iot_nodes, {
      foreignKey: "device_id",
      as: "history_end",
    });
    iot_histories.belongsTo(models.iot_nodes, {
      foreignKey: "device_id",
      as: "history_start",
    });
    iot_histories.belongsTo(models.iot_histories, {
      foreignKey: "device_id",
      as: "m2",
    });
    iot_histories.hasOne(models.iot_nodes, {
      foreignKey: "id",
      sourceKey: "device_id",
      as: "history_log",
    });
    // iot_histories.hasOne(models.iot_nodes,{foreignKey:'id'})
  };
  return iot_histories;
};

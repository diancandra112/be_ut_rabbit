const { SvcSetValve } = require("../../services/actions/serviceValve");
const { SvcLogActions } = require("../../services/actions/serviceLog");

module.exports = {
  CtrlSetValve2: (req, res) => {
    const { user_id, role } = req;
    return console.log(user_id, role, role + "-" + user_id);
    SvcLogActions("valve", res.locals.decoded, req.params.devEUI)
      .then((response) => res.json(response))
      .catch((error) => res.status(500).json(error));
  },
  CtrlSetValve: (req, res) => {
    const data = req.params;
    const { user_id, role } = req;
    SvcLogActions(
      "valve",
      req.params.valve,
      role + "-" + user_id,
      req.params.devEUI,
      null,
      user_id
    )
      .then((response) => {
        SvcSetValve(data)
          .then((response) => res.json(response))
          .catch((error) => res.status(500).json(error));
      })
      .catch((error) => res.status(500).json(error));
  },
};

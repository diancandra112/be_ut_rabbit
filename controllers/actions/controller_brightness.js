const {
  SvcSetBrightness,
} = require("../../services/actions/serviceBrightness");
const { SvcLogActions } = require("../../services/actions/serviceLog");

module.exports = {
  CtrlSetBrightness: (req, res) => {
    const data = req.params;
    const { user_id, role } = req;
    SvcLogActions(
      "Brightness",
      req.params.value,
      role + "-" + user_id,
      req.params.devEUI,
      null,
      user_id
    )
      .then((response) => {
        SvcSetBrightness(data)
          .then((response) => res.json(response))
          .catch((error) => res.status(500).json(error));
      })
      .catch((error) => res.status(500).json(error));
  },
};

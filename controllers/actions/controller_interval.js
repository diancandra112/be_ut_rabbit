const { SvcSetInterval } = require("../../services/actions/serviceInterval");
const { SvcLogActions } = require("../../services/actions/serviceLog");

module.exports = {
  CtrlSetInterval: (req, res) => {
    let data = req.params;
    const { user_id, role } = req;
    if (req.query.is_web == null) {
      data.interval = parseFloat(data.interval) * 60;
    }
    if (parseFloat(data.interval) < 300) {
      res.status(403).json({
        responseCode: 403,
        message: "device interval at least 5 minutes",
      });
    } else {
      SvcLogActions(
        "interval",
        req.params.interval / 60,
        role + "-" + user_id,
        req.params.devEUI,
        "minute",
        user_id
      )
        .then((response) => {
          SvcSetInterval(data)
            .then((response) => res.json(response))
            .catch((error) => res.status(500).json(error));
        })
        .catch((error) => res.status(500).json(error));
    }
  },
};

const { SvcEditMeter } = require("../../services/actions/serviceEditMeter");

module.exports = {
  CtrlEditMeter: (req, res) => {
    const data = req.params;
    const payload = req.body;
    const type = payload.typeId;
    delete payload.typeId;
    SvcEditMeter(data, payload, type)
      .then((response) => res.json(response))
      .catch((error) => res.status(500).json(error));
  },
};

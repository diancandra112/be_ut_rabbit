const { SvcGetLogActions } = require("../../services/actions/serviceLog");

module.exports = {
  CtrlGetLog: (req, res) => {
    const data = req.query;
    let areaId = {};
    if (res.locals.areaId != null) {
      areaId = { areaId: res.locals.areaId };
    }
    SvcGetLogActions(
      data,
      data.search,
      data.action,
      data.date_start,
      data.date_end,
      data.page,
      data.size,
      areaId
    )
      .then((response) => res.status(200).json(response))
      .catch((error) => res.status(500).json(error));
  },
};

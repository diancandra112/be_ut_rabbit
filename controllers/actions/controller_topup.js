const { SvcSetTopup } = require("../../services/actions/serviceTopup");
const { SvcLogActions } = require("../../services/actions/serviceLog");
const models = require("../../models/index");

module.exports = {
  CtrlSetTopup: (req, res) => {
    const data = req.params;
    models.iot_nodes
      .findOne({ attributes: ["typeId"], where: { devEui: req.params.devEUI } })
      .then((res_node) => {
        let unit = "";
        if ([1, 2].includes(res_node.typeId)) {
          unit = "m3";
        } else if ([3, 7].includes(res_node.typeId)) {
          unit = "kwh";
        }
        const { user_id, role } = req;
        SvcLogActions(
          "topup",
          req.params.topup,
          role + "-" + user_id,
          req.params.devEUI,
          unit,
          user_id
        )
          .then((response) => {
            SvcSetTopup(data)
              .then((response) => res.json(response))
              .catch((error) => res.status(500).json(error));
          })
          .catch((error) => res.status(500).json(error));
      })
      .catch((error) => res.status(500).json(error));
  },
};

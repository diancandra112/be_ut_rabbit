const { SvcSetTopup } = require("../../services/actions/serviceMeter");
const { SvcLogActions } = require("../../services/actions/serviceLog");
const { SvcSetUtc } = require("../../services/actions/serviceUtc");
const { SvcSetPulse } = require("../../services/actions/servicePulse");
const models = require("../../models/index");

module.exports = {
  CtrlSetMeter: (req, res) => {
    let data = req.params;
    models.iot_nodes
      .findOne({ attributes: ["typeId"], where: { devEui: req.params.devEUI } })
      .then((res_node) => {
        let unit = "";
        if ([1, 2].includes(res_node.typeId)) {
          unit = "m3";
        } else if ([3, 7].includes(res_node.typeId)) {
          unit = "kwh";
        }
        const { user_id, role } = req;
        SvcLogActions(
          "meter",
          req.params.set_meter,
          role + "-" + user_id,
          req.params.devEUI,
          unit,
          user_id
        )
          .then((response) => {
            data.set_meter = parseFloat(data.set_meter) * 1000;
            SvcSetTopup(data)
              .then((response) => res.json(response))
              .catch((error) => res.status(500).json(error));
          })
          .catch((error) => res.status(500).json(error));
      })
      .catch((error) => res.status(500).json(error));
  },
  CtrlSetUtc: (req, res) => {
    const data = req.params;
    const { user_id, role } = req;
    role + "-" + user_id,
      SvcLogActions(
        "utc",
        0,
        role + "-" + user_id,
        req.params.devEUI,
        "utc",
        user_id
      )
        .then((response) => {
          SvcSetUtc(data)
            .then((response) => res.json(response))
            .catch((error) => res.status(500).json(error));
        })
        .catch((error) => res.status(500).json(error));
  },
  CtrlSetPulse: (req, res) => {
    const data = req.params;
    const { user_id, role } = req;
    SvcLogActions(
      "pulse",
      data.pulse,
      role,
      req.params.devEUI,
      data.pulse_count,
      user_id
    )
      .then((response) => {
        SvcSetPulse(data)
          .then((response) => res.json(response))
          .catch((error) => res.status(500).json(error));
      })
      .catch((error) => res.status(500).json(error));
  },
};

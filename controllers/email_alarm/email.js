const { SvcAlarm } = require("../../services/email_alarm/email");

module.exports = {
  CtrlAlarm: (req, res) => {
    SvcAlarm()
      .then((s) => res.json(s))
      .catch((err) => res.json(err));
  },
};

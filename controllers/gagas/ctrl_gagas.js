const { SvcGagasHourly } = require("../../services/gagas/svc_gagas_hourly");
const { SvcGagasDaily } = require("../../services/gagas/svc_gagas_daily");
const {
  svcCreateBillingAllV2,
  svcMerk,
} = require("../../services/billing/serviceBillingCreateV2Manualv2");
module.exports = {
  CtrlMerk: (req, res) => {
    svcMerk()
      .then((result) => res.json(result))
      .catch((err) => res.json(err));
  },
  CtrlBillingCustom: (req, res) => {
    svcCreateBillingAllV2()
      .then((result) => res.json(result))
      .catch((err) => res.json(err));
  },
  CtrlGagasHourly: (req, res) => {
    SvcGagasHourly()
      .then((result) => res.json(result))
      .catch((err) => res.json(err));
  },
  CtrlGagasDaily: (req, res) => {
    SvcGagasDaily()
      .then((result) => res.json(result))
      .catch((err) => res.json(err));
  },
};

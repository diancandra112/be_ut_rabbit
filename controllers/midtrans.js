const { servicemidtransCallback } = require("../services/midtrans");
module.exports = {
  controllerMidtransCallback: (req, res) => {
    try {
      const data = req.body;
      let cancel_va =
        data.order_id.split("-BCA").length > 1
          ? data.order_id.split("-BCA")[0] + "-PERMATA"
          : data.order_id.split("-PERMATA")[0] + "-BCA";
      let inv =
        data.order_id.split("-BCA").length > 1
          ? data.order_id.split("-BCA")[0]
          : data.order_id.split("-PERMATA")[0];
      let update = {};
      switch (parseInt(data.status_code)) {
        case 201:
          update = { status: "UNPAID", invoice: inv };
          break;
        case 200:
          update = { status: "PAID", invoice: inv };
          break;
        case 202:
          update = { status: "CANCEL", invoice: inv };
          break;

        default:
          break;
      }
      let history = {
        invoice: inv,
        status_code: data.status_code,
        payload: JSON.stringify(data),
      };
      console.log(update, "\r\n", cancel_va);
      if (parseInt(data.status_code) == 201) {
        res.status(200).json("sukses");
      } else {
        servicemidtransCallback(update, cancel_va, history)
          .then((result) => res.json(result))
          .catch((error) => res.status(400).json(error));
      }
    } catch (error) {
      console.log("error", error);
    }
  },

  controllerMidtransCharge: (req, res) => {
    const data = req.body;
    servicemidtransCallback(data.data)
      .then((result) => res.json(result))
      .catch((error) => res.json(error));
  },

  controllerMidtransCancel: (req, res) => {
    const data = req.body;
    servicemidtransCallback(data.data)
      .then((result) => res.json(result))
      .catch((error) => res.json(error));
  },
};

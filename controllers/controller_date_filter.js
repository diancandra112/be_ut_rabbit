const {serviceHistoryFilter,serviceHistoryRangeFilter,serviceHistoryNodev2} = require('../services/dateFilter')
module.exports={
    controllerHistoryFilter:(req, res)=>{
        let end = req.body.date.split('-')
        end[2] = ('00'+(parseInt(end[2])+1).toString()).slice(-2)
        end = end.join('-')
        const device_id = req.body.device_id
        const start = `${req.body.date}T00:00:00.000Z`
        const final = `${end}T00:00:00.000Z`
        const data = req.body
        const typeId = req.body.typeId
        serviceHistoryNodev2(data, start, final, device_id,typeId)
        // serviceHistoryFilter(data, start, final, device_id,typeId)
            .then((result)=>res.status(result.responseCode).json(result))
            .catch((error)=>res.status(error.responseCode).json(error))
    },
    controllerHistoryRangeFilter:(req, res)=>{
        let end = req.body.end.split('-')
        end[2] = ('00'+(parseInt(end[2])+1).toString()).slice(-2)
        end = end.join('-')
        const device_id = req.body.device_id
        const start = `${req.body.start}T00:00:00.000Z`
        const final = `${end}T00:00:00.000Z`
        const data = req.body
        const typeId = req.body.typeId
        serviceHistoryNodev2(data, start, final, device_id,typeId)
        // serviceHistoryRangeFilter(start, final, device_id)
            .then((result)=>res.status(result.responseCode).json(result))
            .catch((error)=>res.status(error.responseCode).json(error))
    },
}
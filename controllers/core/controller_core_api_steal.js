const models = require("../../models/index");

module.exports = {
  ctrlStealNode: (input, devEui, report_time) => {
    return new Promise(async (resolve, reject) => {
      try {
        input = Buffer.from(input, "base64");
        input = input.toString("hex");
        input = input.match(/.{1,2}/g);
        let flag = true;
        let output = [];
        for (let index = 0; index < input.length; index++) {
          let binary = (
            "00000000" + parseInt(input[index], 16).toString(2)
          ).slice(-8);
          let dataType = parseInt(binary.slice(0, 6), 2);
          let dataLength = Math.pow(2, parseInt(binary.slice(-2), 2));
          let dataRaw = [];
          for (let idx = index + 1; idx <= dataLength + index; idx++) {
            dataRaw.push(input[idx]);
          }
          if (dataType == 2) flag = false;
          output.push({
            type: dataType,
            binary: binary,
            hex: input[index],
            data: dataRaw,
          });
          index += dataLength;
        }
        if (flag) return resolve(output);
        let steal = false;
        output.map((val, idx) => {
          let binary = "";
          switch (parseInt(val.type)) {
            case 2:
              let binary2 = (
                "00000000" + parseInt(val.data[0], 16).toString(2)
              ).slice(-8);
              let alarm = [];
              let alarmFlag = [];
              for (let index = 0; index < binary2.length; index++) {
                switch (index) {
                  case 0:
                    if (binary2[index] == 1) {
                      alarm.push("Reboot");
                      alarmFlag.push("11");
                    }
                    break;
                  case 1:
                    if (binary2[index] == 1) {
                      alarm.push("Battery low");
                      alarmFlag.push("1");
                    }
                    break;
                  case 2:
                    if (binary2[index] == 1) {
                      alarm.push("No prepayment");
                    }
                    break;
                  case 3:
                    if (binary2[index] == 1) {
                      alarm.push("Steal");
                      steal = true;
                      alarmFlag.push("5");
                    }
                    break;
                  case 4:
                    if (binary2[index] == 1) {
                      alarm.push("Valve closed");
                      alarmFlag.push("4");
                    }
                    break;
                  case 5:
                    if (binary2[index] == 1) {
                      alarm.push("No Signal");
                      alarmFlag.push("10");
                    }
                    break;
                  case 6:
                    if (binary2[index] == 1) {
                      alarm.push("Valve fault");
                      alarmFlag.push("12");
                    }
                    break;
                  case 7:
                    if (binary2[index] == 1) {
                      alarm.push("Reserve");
                    }
                    break;

                  default:
                    break;
                }
              }

            // if (alarmFlag.length > 0) {
            //   alarmFlag = alarmFlag.join(",");
            //   tamplate.alarm_flag = alarmFlag;
            // }
            // if (alarm.length > 0) {
            //   alarm = alarm.join(",");
            // } else {
            //   alarm = "N/A";
            // }
            // tamplate.meter.alarm = alarm;
            // break;
          }
        });
        if (steal == false) return resolve(output);
        let node = await models.iot_nodes.findOne({
          where: devEui,
          include: [{ model: models.iot_node_type }],
        });
        node = JSON.parse(JSON.stringify(node));
        if (node == null) return resolve(output);
        const moment = require("moment");
        let update = [
          {
            type: node.iot_node_type.type_name,
            report_time_start: report_time,
            report_time_end: report_time,
            node_id: node.id,
            areaId: node.areaId,
            area_id: node.areaId,
            devEui: node.devEui,
            date: moment(report_time).utcOffset("+0700").format("YYYY-MM-DD"),
          },
        ];
        let steal_db = await models.iots_steal_alarm.bulkCreate(update, {
          updateOnDuplicate: ["report_time_end"],
        });
        console.log(steal_db);
        return resolve(steal_db);
      } catch (error) {
        reject(error.message);
      }
    });
  },
};

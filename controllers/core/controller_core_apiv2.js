const {
  serviceCoreNodeHistoryApi,
  serviceCoreNodeHistoryNonMeterApi,
} = require("../../services/core.js/services_core_api");
const {
  serviceCoreNodeHistoryApi1237,
} = require("../../services/core.js/services_core_api_1237");
const {
  serviceCoreNodeHistoryApi13,
} = require("../../services/core.js/services_core_api_13");
const {
  serviceCoreNodeHistoryApi23,
} = require("../../services/core.js/services_core_api_23");
const {
  serviceCoreNodeHistoryApi6,
} = require("../../services/core.js/services_core_api_6");
const {
  serviceCoreNodeHistoryApi16,
} = require("../../services/core.js/services_core_api_16");
const {
  serviceCoreNodeHistoryApi5,
} = require("../../services/core.js/services_core_api_5");
const {
  serviceCoreNodeHistoryApi9,
} = require("../../services/core.js/services_core_api_9");
const { ctrlStealNode } = require("./controller_core_api_steal");
const {
  serviceCoreNodeHistoryApi17,
} = require("../../services/core.js/services_core_api_17");
const {
  serviceCoreNodeHistoryApi18,
} = require("../../services/core.js/services_core_api_18");
const {
  serviceCoreNodeHistoryApi19,
} = require("../../services/core.js/services_core_api_19");
const {
  serviceCoreNodeHistoryApi20,
} = require("../../services/core.js/services_core_api_20");
const {
  serviceCoreNodeHistoryApi21,
} = require("../../services/core.js/services_core_api_21");
const {
  serviceCoreNodeHistoryApi22,
} = require("../../services/core.js/services_core_api_22");
const models = require("../../models/index");
const { GetNodeLog } = require("../../middlewares/chache");
const { ctrlHistoriDetail } = require("./controller_core_api_history_detail");
const { CtrlToAnaheim } = require("./controller_anaheim");
const {
  serviceCoreNodeHistoryApi35,
} = require("../../services/core.js/services_core_api_35");
const {
  serviceCoreNodeHistoryApi14,
} = require("../../services/core.js/services_core_api_14");
const {
  serviceCoreNodeHistoryApi2,
} = require("../../services/core.js/services_core_api_2");
const { ctrlRxInfo } = require("./controller_rxinfo");
module.exports = {
  ctrlCoreNodeHistoryApi: async (req, res) => {
    const amqp = require("amqplib/callback_api");
    amqp.connect(
      "amqp://admin:zAnypet15@ec2-54-251-60-140.ap-southeast-1.compute.amazonaws.com",
      function (error0, connection) {
        if (error0) {
          throw error0;
        }
        connection.createChannel(function (error1, channel) {
          if (error1) {
            throw error1;
          }
          var queue = "49";

          channel.assertQueue(queue, {
            durable: true,
          });
          channel.prefetch(1);

          console.log("Waiting for messages in %s", queue);
          channel.consume(queue, async function (msg) {
            let data = JSON.parse(msg.content.toString());
            console.log(data);
            try {
              if (data.payload == null) {
                setTimeout(function () {
                  channel.ack(msg);
                }, 1000);
                return console.log({
                  responseCode: 200,
                  messages: "payload kosong",
                });
              }
            } catch (error) {}
            let max_totalizer = 0;
            try {
              if (data.payload == null) {
                setTimeout(function () {
                  channel.ack(msg);
                }, 1000);
                return console.log({
                  responseCode: 200,
                  messages: "payload kosong",
                });
              }
            } catch (error) {}
            try {
              let node_find = await GetNodeLog(data.devEUI);
              if (
                node_find == null ||
                (node_find.tenantId == null && node_find.internalId == null)
              ) {
                setTimeout(function () {
                  channel.ack(msg);
                }, 1000);
                return console.log("ok");
              }
              try {
                let temp_rx = data.payload;
                temp_rx = JSON.parse(temp_rx);
                ctrlRxInfo(node_find.id, temp_rx)
                  .then((r) => console.log("ok"))
                  .catch((r) => console.log("ok"));
              } catch (error) {}
              if (data.typeId == 14) {
                serviceCoreNodeHistoryApi14(data)
                  .then((result) => console.log(result))
                  .catch((error) => console.log(error));
              }
              if (data.typeId == 22) {
                serviceCoreNodeHistoryApi22(data, node_find)
                  .then((result) => console.log(result))
                  .catch((error) => console.log(error));
              } else if (data.typeId == 35) {
                serviceCoreNodeHistoryApi35(data)
                  .then((result) => console.log(result))
                  .catch((error) => console.log(error));
              } else if (data.typeId == 23) {
                serviceCoreNodeHistoryApi23(data)
                  .then((result) => console.log(result))
                  .catch((error) => console.log(error));
              } else if (data.typeId == 21) {
                serviceCoreNodeHistoryApi21(data, node_find)
                  .then((result) => console.log(result))
                  .catch((error) => console.log(error));
              } else if (data.typeId == 20) {
                serviceCoreNodeHistoryApi20(data, node_find)
                  .then((result) => console.log(result))
                  .catch((error) => console.log(error));
              } else if (data.typeId == 19) {
                serviceCoreNodeHistoryApi19(data, node_find)
                  .then((result) => console.log(result))
                  .catch((error) => console.log(error));
              } else if (data.typeId == 18) {
                serviceCoreNodeHistoryApi18(data, node_find)
                  .then((result) => console.log(result))
                  .catch((error) => console.log(error));
              } else if (data.typeId == 17) {
                serviceCoreNodeHistoryApi17(data, node_find)
                  .then((result) => console.log(result))
                  .catch((error) => console.log(error));
              } else if (
                data.typeId == 1 ||
                data.typeId == 3 ||
                data.typeId == 7
              ) {
                serviceCoreNodeHistoryApi1237(data, max_totalizer)
                  .then((result) => console.log(result))
                  .catch((error) => console.log(error));
              } else if (data.typeId == 2) {
                serviceCoreNodeHistoryApi2(data, max_totalizer)
                  .then((result) => console.log(result))
                  .catch((error) => console.log(error));
              } else if (data.typeId == 16) {
                data.child_app_name = JSON.parse(data.payload).applicationName;
                data.child_sn = JSON.parse(data.payload).serialNumber;
                serviceCoreNodeHistoryApi16(data)
                  .then((result) => console.log(result))
                  .catch((error) => console.log(error));
              } else if (data.typeId == 13) {
                serviceCoreNodeHistoryApi13(data)
                  .then((result) => console.log(result))
                  .catch((error) => console.log(error));
              } else if (data.typeId == 6) {
                serviceCoreNodeHistoryApi6(data)
                  .then((result) => console.log(result))
                  .catch((error) => console.log(error));
              } else {
                if (data.typeId == 5) {
                  serviceCoreNodeHistoryApi5(data)
                    .then((result) => console.log(result))
                    .catch((error) => console.log(error));
                } else if (data.typeId == 9) {
                  serviceCoreNodeHistoryApi9(data)
                    .then((result) => console.log(result))
                    .catch((error) => console.log(error));
                }
                serviceCoreNodeHistoryApi(data)
                  .then((result) => console.log(result))
                  .catch((error) => console.log(error));
              }
              setTimeout(function () {
                channel.ack(msg);
              }, 1000);
            } catch (error) {
              setTimeout(function () {
                channel.ack(msg);
              }, 1000);
            }
          });
        });
      }
    );
  },
};

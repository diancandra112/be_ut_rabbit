const models = require("../../models/index");

module.exports = {
  ctrlRxInfo: (id, temp_rx) => {
    return new Promise(async (resolve, reject) => {
      try {
        let rx_info = temp_rx.rxInfo[0];
        let updateNode = {
          rssi: rx_info.rssi,
          snr: rx_info.loRaSNR,
          gateway_eui: rx_info.mac,
          gateway_name: rx_info.name,
        };
        try {
          updateNode.battery_voltage = temp_rx.meter.voltage;
          updateNode.battery_voltage = updateNode.battery_voltage
            .toLowerCase()
            .split("v")
            .join("");
          updateNode.battery_voltage = parseFloat(updateNode.battery_voltage);
        } catch (error) {
          console.log(error, "battery_voltage water");
          delete updateNode.battery_voltage;
        }
        if (isNaN(updateNode.battery_voltage)) {
          delete updateNode.battery_voltage;
        }
        console.log(updateNode, "updateNode");
        await models.iot_nodes.update(updateNode, { where: { id: id } });
        return resolve("ok");
      } catch (error) {
        resolve(error.message);
      }
    });
  },
};

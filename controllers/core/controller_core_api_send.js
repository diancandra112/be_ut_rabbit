const models = require("../../models/index");
const { GetNodeLog } = require("../../middlewares/chache");
const moment = require("moment");
const axios = require("axios");
module.exports = {
  ctrlLogEjip: (data, node) => {
    return new Promise(async (resolve, reject) => {
      try {
        if (node.areaId == 80) {
          let url = "https://api-ejip.blackeye.id/water/save-data";
          let data_db = {
            payload: data.payload,
            method: "POST",
            auth: "Basic ZWppcC1pa2k6ZWohcDFraUAybzIy",
            url: url,
          };
          try {
            let send = await axios({
              data: JSON.parse(data.payload),
              method: "POST",
              baseURL: url,
              headers: {
                authorization: "Basic ZWppcC1pa2k6ZWohcDFraUAybzIy",
              },
            });
            data_db.code = send.status;
            data_db.status = JSON.stringify(send.data);
            await models.iots_to_public.create(data_db);
            return resolve("ok ");
          } catch (error) {
            if (error.response != null) {
              data_db.code = error.response.status;
              data_db.status = JSON.stringify(error.response.data);
            } else {
              data_db.code = 500;
              data_db.status = error.message;
            }
            await models.iots_to_public.create(data_db);
            console.log("error send ", error.message);
            return resolve(error.message);
          }
        } else {
          console.log("no ejib");
          return resolve("no ejib");
        }
      } catch (error) {
        console.log(error.message);
        return resolve(error.message);
      }
    });
  },
};

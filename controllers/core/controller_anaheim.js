module.exports = {
  CtrlToAnaheim: (data) => {
    return new Promise(async (resolve, reject) => {
      const moment = require("moment");
      const axios = require("axios");
      const models = require("../../models/index");
      try {
        let update = {
          payload: data.payload,
          method: "POST",
          auth: "no auth",
          url: "https://payload.anaheim.co.id/api/payload",
          date: moment().format("YYYY-MM-DD HH:mm:ss"),
        };
        let ax = await axios({
          method: "POST",
          baseURL: "https://payload.anaheim.co.id/api/payload",
          data: JSON.parse(data.payload),
        });
        update.code = ax.status;
        update.response = JSON.stringify(ax.data);
        await models.iots_to_anaheim.create(update);
        resolve("ok");
      } catch (error) {
        let update_error = {
          payload: data.payload,
          method: "POST",
          auth: "no auth",
          url: "https://payload.anaheim.co.id/api/payload",
          response: "time out",
          code: 500,
          date: moment().format("YYYY-MM-DD HH:mm:ss"),
        };
        try {
          update_error.code = error.response.status;
          update_error.response = JSON.stringify(error.response.data);
        } catch (error) {}
        await models.iots_to_anaheim.create(update_error);
        reject(error.message);
      }
    });
  },
};

const models = require("../../models/index");
const { GetNodeLog } = require("../../middlewares/chache");
const { ctrlLogEjip } = require("./controller_core_api_send");
const moment = require("moment");
module.exports = {
  ctrlHistoriDetail: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        let node = await GetNodeLog(data.devEUI);
        node = JSON.parse(JSON.stringify(node));
        let update = [];
        if (node.typeId == 2) {
          try {
            ctrlLogEjip(data, node)
              .then((a) => console.log(a))
              .catch((b) => console.log(b));
          } catch (error) {
            console.log(error, "milik ctrlLogEjip");
          }
          update.push({
            nodeId: node.id,
            areaId: node.areaId,
            tenantId: node.tenantId || 0,
            internalId: node.internalId || 0,
            fdate: moment(data.report_time)
              .utcOffset("+0700")
              .format("YYYY-MM-DD"),
            fyear: moment(data.report_time).utcOffset("+0700").format("YYYY"),
            fmonth: moment(data.report_time).utcOffset("+0700").format("MM"),
            fday: moment(data.report_time).utcOffset("+0700").format("DD"),
            fhour: moment(data.report_time).utcOffset("+0700").format("HH"),
            fminute: moment(data.report_time).utcOffset("+0700").format("mm"),
            totalizer: JSON.parse(data.payload).meter.meterReading,
          });
          await models.iots_history_2_dayly.bulkCreate(update, {
            updateOnDuplicate: [
              "nodeId",
              "areaId",
              "tenantId",
              "internalId",
              "fdate",
              "fyear",
              "fmonth",
              "fday",
              "fhour",
              "fminute",
              "totalizer",
            ],
          });
          await models.iots_history_2_hourly.bulkCreate(update, {
            updateOnDuplicate: [
              "nodeId",
              "areaId",
              "tenantId",
              "internalId",
              "fdate",
              "fyear",
              "fmonth",
              "fday",
              "fhour",
              "fminute",
              "totalizer",
            ],
          });
          await models.iots_history_2_monthly.bulkCreate(update, {
            updateOnDuplicate: [
              "nodeId",
              "areaId",
              "tenantId",
              "internalId",
              "fdate",
              "fyear",
              "fmonth",
              "fday",
              "fhour",
              "fminute",
              "totalizer",
            ],
          });
        } else {
        }
        return resolve("ok");
      } catch (error) {
        console.log(error.message);
        resolve(error.message);
      }
    });
  },
};

const {
  serviceCoreNodeHistoryApi,
  serviceCoreNodeHistoryNonMeterApi,
} = require("../../services/core.js/services_core_api");
const {
  serviceCoreNodeHistoryApi1237,
} = require("../../services/core.js/services_core_api_1237");
const {
  serviceCoreNodeHistoryApi13,
} = require("../../services/core.js/services_core_api_13");
const {
  serviceCoreNodeHistoryApi6,
} = require("../../services/core.js/services_core_api_6");
const {
  serviceCoreNodeHistoryApi16,
} = require("../../services/core.js/services_core_api_16");
const {
  serviceCoreNodeHistoryApi5,
} = require("../../services/core.js/services_core_api_5");
const {
  serviceCoreNodeHistoryApi9,
} = require("../../services/core.js/services_core_api_9");
const { ctrlStealNode } = require("./controller_core_api_steal");
const {
  serviceCoreNodeHistoryApi17,
} = require("../../services/core.js/services_core_api_17");
const {
  serviceCoreNodeHistoryApi18,
} = require("../../services/core.js/services_core_api_18");
const {
  serviceCoreNodeHistoryApi19,
} = require("../../services/core.js/services_core_api_19");
const {
  serviceCoreNodeHistoryApi20,
} = require("../../services/core.js/services_core_api_20");
const {
  serviceCoreNodeHistoryApi21,
} = require("../../services/core.js/services_core_api_21");
const {
  serviceCoreNodeHistoryApi22,
} = require("../../services/core.js/services_core_api_22");
const models = require("../../models/index");
const { GetNodeLog } = require("../../middlewares/chache");
const { ctrlHistoriDetail } = require("./controller_core_api_history_detail");
const { CtrlToAnaheim } = require("./controller_anaheim");
module.exports = {
  ctrlCoreNodeHistoryApi: async (req, res) => {
    const data = req.body;
    let max_totalizer = 0;
    if ([1, 2].includes(data.typeId)) {
      let node = await models.iot_nodes.findOne({
        where: { devEui: data.devEUI },
        attributes: [
          "id",
          "typeId",
          "setting_pulse",
          "live_pulse",
          "start_totalizer",
          "flag_install",
          "live_last_meter",
          "max_totalizer",
          "max_totalizer_flag",
          "interval",
          "setting_interval",
        ],
      });
      node = JSON.parse(JSON.stringify(node));
      if (node == null) {
        return res.status(200).json("null");
      }
      max_totalizer = node.max_totalizer;

      let tem = data.payload;
      tem = JSON.parse(tem);
      const { weiots } = require("../../config/axios");
      if (node.flag_install == true) {
        const ilang = (a) => {
          a = a.toString().split(".");
          if (a.length > 1) {
            a = a[0] + "." + a[1].slice(0, 1);
            a = parseFloat(a);
          } else {
            a = a.join("");
            a = parseFloat(a);
          }
          return a;
        };
        if (node.setting_pulse != node.live_pulse) {
          weiots({
            method: "post",
            url: `/action/pulse/${data.devEUI}/${node.setting_pulse}/${node.start_totalizer}`,
          });
          return res.status(200).json({
            responseCode: 200,
            messages: "ok",
          });
        } else if (
          node.setting_pulse >= 1000 &&
          parseInt(parseFloat(node.start_totalizer) / 1000) !=
            parseInt(parseFloat(tem.meter.meterReading) / 1000)
        ) {
          weiots({
            method: "post",
            url: `/action/meter/${data.devEUI}/${node.start_totalizer}`,
          });
          return res.status(200).json({
            responseCode: 200,
            messages: "ok",
          });
        } else if (
          node.setting_pulse < 1000 &&
          ilang(parseFloat(node.start_totalizer) / 1000) !=
            ilang(parseFloat(tem.meter.meterReading) / 1000)
        ) {
          weiots({
            method: "post",
            url: `/action/meter/${data.devEUI}/${node.start_totalizer}`,
          });
          return res.status(200).json({
            responseCode: 200,
            messages: "ok",
          });
        }

        weiots({
          method: "post",
          url: `/action/interval/${data.devEUI}/${60 * 60}`,
        });
        models.iot_nodes.update(
          { flag_install: false },
          {
            where: { devEui: data.devEUI },
          }
        );
      }

      //if max totalizer
      data.payload = JSON.parse(data.payload);
      if (
        parseFloat(data.payload.meter.meterReading) >= node.max_totalizer &&
        node.max_totalizer > 0
      ) {
        if (parseFloat(node.live_last_meter) != node.max_totalizer) {
          data.payload.meter.meterReading = node.max_totalizer;
          weiots({
            method: "post",
            url: `/action/interval/${data.devEUI}/${120}`,
          });
          models.iot_nodes.update(
            {
              interval: 2,
              max_totalizer_flag: true,
            },
            {
              where: { id: node.id },
            }
          );
        } else {
          let mtr =
            parseFloat(data.payload.meter.meterReading) - node.max_totalizer;
          weiots({
            method: "post",
            url: `/action/meter/${data.devEUI}/${mtr}`,
          });
          return res.status(200).json({
            responseCode: 200,
            messages: "ok",
          });
        }
      } else {
        console.log("tidak max");
      }

      if (node.setting_interval != node.interval) {
        weiots({
          method: "post",
          url: `/action/interval/${data.devEUI}/${
            parseFloat(node.setting_interval) * 60
          }`,
        });
      }
      data.payload = JSON.stringify(data.payload);
      //if max totalizer
    }
    console.log(
      "amannnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn"
    );
    try {
      ctrlHistoriDetail(data)
        .then((a) => console.log(a))
        .catch((b) => console.log(b));
    } catch (error) {
      console.log(error, "MILIK ctrlHistoriDetail");
    }
    try {
      let pa = JSON.parse(data.payload).data;
      ctrlStealNode(pa, { devEui: data.devEUI }, data.report_time)
        .then((a) => console.log(a))
        .catch((b) => console.log(b));
    } catch (error) {}
    let node_find = await GetNodeLog(data.devEUI);
    try {
      if (node_find.areaId == 85) {
        CtrlToAnaheim(data)
          .then((a) => console.log(a))
          .catch((b) => console.log(b));
      }
    } catch (error) {}
    if (
      node_find == null ||
      (node_find.tenantId == null && node_find.internalId == null)
    ) {
      return res.status(200).json("ok");
    }
    if (data.typeId == 22) {
      serviceCoreNodeHistoryApi22(data, node_find)
        .then((result) => res.status(200).json(result))
        .catch((error) => res.status(500).json(error));
    } else if (data.typeId == 21) {
      serviceCoreNodeHistoryApi21(data, node_find)
        .then((result) => res.status(200).json(result))
        .catch((error) => res.status(500).json(error));
    } else if (data.typeId == 20) {
      serviceCoreNodeHistoryApi20(data, node_find)
        .then((result) => res.status(200).json(result))
        .catch((error) => res.status(500).json(error));
    } else if (data.typeId == 19) {
      serviceCoreNodeHistoryApi19(data, node_find)
        .then((result) => res.status(200).json(result))
        .catch((error) => res.status(500).json(error));
    } else if (data.typeId == 18) {
      serviceCoreNodeHistoryApi18(data, node_find)
        .then((result) => res.status(200).json(result))
        .catch((error) => res.status(500).json(error));
    } else if (data.typeId == 17) {
      serviceCoreNodeHistoryApi17(data, node_find)
        .then((result) => res.status(200).json(result))
        .catch((error) => res.status(500).json(error));
    } else if (
      data.typeId == 1 ||
      data.typeId == 2 ||
      data.typeId == 3 ||
      data.typeId == 7
    ) {
      serviceCoreNodeHistoryApi1237(data, max_totalizer)
        .then((result) => res.status(200).json(result))
        .catch((error) => res.status(500).json(error));
    } else if (data.typeId == 16) {
      data.child_app_name = JSON.parse(data.payload).applicationName;
      data.child_sn = JSON.parse(data.payload).serialNumber;
      serviceCoreNodeHistoryApi16(data)
        .then((result) => res.status(200).json(result))
        .catch((error) => res.status(500).json(error));
    } else if (data.typeId == 13) {
      serviceCoreNodeHistoryApi13(data)
        .then((result) => res.status(200).json(result))
        .catch((error) => res.status(500).json(error));
    } else if (data.typeId == 6) {
      serviceCoreNodeHistoryApi6(data)
        .then((result) => res.status(200).json(result))
        .catch((error) => res.status(500).json(error));
    } else {
      serviceCoreNodeHistoryApi(data)
        .then((result) => {
          if (data.typeId == 5) {
            serviceCoreNodeHistoryApi5(data)
              .then((result) => res.status(200).json(result))
              .catch((error) => res.status(500).json(error));
          } else if (data.typeId == 9) {
            serviceCoreNodeHistoryApi9(data)
              .then((result) => res.status(200).json(result))
              .catch((error) => res.status(500).json(error));
          } else {
            res.status(200).json(result);
          }
        })
        .catch((error) => res.status(500).json(error));
    }
  },
  ctrlCoreNodeHistoryNonMeterApi: (req, res) => {
    const data = req.body;
    serviceCoreNodeHistoryNonMeterApi(data)
      .then((result) => res.status(200).json(result))
      .catch((error) => res.status(500).json(error));
  },
};

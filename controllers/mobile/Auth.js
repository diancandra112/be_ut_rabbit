const {
  svcMobileLogin,
  svcMobileTeknisiLogin,
  svcMobileTeknisiCompanyLogin,
  svcMobileTeknisiCompanyLoginGet,
  svcMobileTeknisiUpdateFcmToken,
  svcTeknisiLogOut,
  svcMobileTenantNodeList,
} = require("../../services/mobile/service_auth");
const {
  svcGetPrepaid,
} = require("../../services/pricingPrepaid/pricing_prepaid");
const hash = (data) => {
  const crypto = require("crypto-js");
  return crypto.SHA256(data).toString();
};
module.exports = {
  controllerListMobileVoucher: (req, res) => {
    const data = {
      ...req.params,
      ...req.query,
    };
    data.areaId = res.locals.decoded.area_id;
    svcGetPrepaid(data)
      .then((result) => res.status(200).json(result))
      .catch((error) => res.status(400).json(error));
  },
  controllerTeknisiLogOut: (req, res) => {
    const id = res.locals.decoded.id;
    svcTeknisiLogOut(id)
      .then((result) => res.status(200).json(result))
      .catch((error) => res.json(error));
  },
  controllerTenantLogOut: (req, res) => {
    const data = req.body;
    data.password = hash(data.password);
    console.log(data);
    return res.json({ responseCode: 200, message: "logout successful" });
  },
  controllerUserMobileLogin: (req, res) => {
    const data = req.body;
    data.password = hash(data.password);
    console.log(data);
    svcMobileLogin(data)
      .then((result) => res.json(result))
      .catch((error) => res.json(error));
  },
  controllerTeknisiMobileLogin: (req, res) => {
    try {
      const data = req.body;
      data.password = hash(data.password);
      console.log(data);
      svcMobileTeknisiLogin(data)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    } catch (error) {
      console.log(error);
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
  controllerTeknisiFcmToken: (req, res) => {
    try {
      console.log(res.locals.decoded.id);
      svcMobileTeknisiUpdateFcmToken(req.params, { id: res.locals.decoded.id })
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    } catch (error) {
      console.log(error);
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
  controllerTeknisiCompanyMobileLogin: (req, res) => {
    try {
      const data = req.body;
      data.password = hash(data.password);
      svcMobileTeknisiCompanyLogin(data)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    } catch (error) {
      console.log(error);
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
  controllerTeknisiCompanyMobileLoginGet: (req, res) => {
    try {
      const data = req.query;
      svcMobileTeknisiCompanyLoginGet(data)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    } catch (error) {
      console.log(error);
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
  controllerMobileTenantNodeList: (req, res) => {
    try {
      const data = { ...req.params, ...req.query };
      svcMobileTenantNodeList(data)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    } catch (error) {
      console.log(error);
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
};

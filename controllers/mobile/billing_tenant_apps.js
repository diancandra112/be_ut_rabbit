const {
  svcListBilling,
  svcListBillingDetails,
} = require("../../services/mobile/billing_tenant_apps");
const hash = (data) => {
  const crypto = require("crypto-js");
  return crypto.SHA256(data).toString();
};
module.exports = {
  controllerListBilling: (req, res) => {
    const data = { tenantId: res.locals.decoded.id };
    svcListBilling(data)
      .then((result) => res.json(result))
      .catch((error) => res.json(error));
  },
  controllerListBillingDetail: (req, res) => {
    const data = req.params;
    svcListBillingDetails(data)
      .then((result) => res.json(result))
      .catch((error) => res.json(error));
  },
};

const {
  SvcGetTeknisiNodeList,
} = require("../../../services/mobile/teknisi/service_node_list");
const {
  SvcGetTeknisiNodeDetails,
} = require("../../../services/mobile/teknisi/service_node_details");

module.exports = {
  CtrlGetTeknisiNodeDetails: (req, res) => {
    try {
      const data = {
        ...req.params,
      };
      SvcGetTeknisiNodeDetails(data)
        .then((response) => res.status(response.responseCode).json(response))
        .catch((err) => res.status(err.responseCode).json(err));
    } catch (error) {
      console.log(error);
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
  CtrlGetTeknisiNodeList: (req, res) => {
    try {
      const { role } = res.locals.decoded;
      const data = {
        ...req.params,
      };
      const condition = req.query;
      if (role == "TEKNISI COMPANY") {
        if (isNaN(req.query.areaId)) {
          delete data.areaId;
          delete data.area_id;
          // throw new Error("teknisi company must contain areaId");
        } else {
          data.areaId = req.query.areaId;
        }
      } else {
        data.areaId = res.locals.decoded.area_id;
      }
      SvcGetTeknisiNodeList(data, condition)
        .then((response) => res.status(response.responseCode).json(response))
        .catch((err) => res.status(err.responseCode).json(err));
    } catch (error) {
      console.log(error);
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
};

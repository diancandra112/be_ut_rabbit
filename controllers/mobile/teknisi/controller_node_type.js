const {
  SvcGetTeknisiNodeType,
} = require("../../../services/mobile/teknisi/service_node_type");

module.exports = {
  CtrlGetTeknisiNodeType: (req, res) => {
    try {
      const data = { ...req.query, id: res.locals.decoded.id };
      SvcGetTeknisiNodeType(data)
        .then((response) => res.status(response.responseCode).json(response))
        .catch((err) => res.status(err.responseCode).json(err));
    } catch (error) {
      console.log(error);
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
};

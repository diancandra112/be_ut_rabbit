const {
  SvcGetTeknisiTiketList,
  SvcEditTeknisiTiketStatus,
  SvcGetTeknisiTiketDetail,
  SvcTeknisiTiketMaintenanceSave,
  SvcTeknisiTiketInstallationSave,
} = require("../../../services/mobile/teknisi/service_tiket_list");
const moment = require("moment");
require("dotenv").config();
module.exports = {
  CtrlGetTeknisiTiketDetail: (req, res) => {
    try {
      const id = req.params;
      const data = {
        area_id: res.locals.decoded.area_id,
        // technician_id: res.locals.decoded.id,
        ...req.query,
      };
      delete data.id;
      if (res.locals.decoded.role == "TEKNISI COMPANY") {
        delete data.area_id;
      }
      SvcGetTeknisiTiketDetail(data, id)
        .then((response) => res.status(response.responseCode).json(response))
        .catch((err) => res.status(err.responseCode).json(err));
    } catch (error) {
      console.log(error);
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
  CtrlGetTeknisiTiketList: (req, res) => {
    try {
      const type = req.params;
      const { role } = res.locals.decoded;
      let data = {
        technician_id: res.locals.decoded.id,
        ...req.query,
      };
      let page = isNaN(parseInt(data.page)) ? 1 : parseInt(data.page);
      let size = isNaN(parseInt(data.size)) ? 10 : parseInt(data.size);
      delete data.page;
      delete data.size;
      if (role == "TEKNISI COMPANY") {
        // delete data.area_id;
        delete data.technician_id;
      } else {
        data.area_id = res.locals.decoded.area_id;
      }
      let type_flag = type.ticket_type.toLocaleLowerCase().trim();
      if (
        type_flag != "all" &&
        type_flag != "installation" &&
        type_flag != "maintenance"
      ) {
        throw new Error("type must contain installation or maintenance");
      }
      SvcGetTeknisiTiketList(data, type, page, size)
        .then((response) => res.status(response.responseCode).json(response))
        .catch((err) => res.status(err.responseCode).json(err));
    } catch (error) {
      console.log(error);
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
  CtrlEditTeknisiTiketStatus: (req, res) => {
    try {
      const data = req.params;
      switch (data.status.toLocaleLowerCase().trim()) {
        case "onprogress":
          data.ticket_processing_date = moment().utcOffset("+0700").format();
          break;
        case "close":
          data.ticket_close_date = moment().utcOffset("+0700").format();
          break;

        default:
          break;
      }
      SvcEditTeknisiTiketStatus(data, res.locals.decoded.id)
        .then((response) => res.status(response.responseCode).json(response))
        .catch((err) => res.status(err.responseCode).json(err));
    } catch (error) {
      console.log(error);
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
  CtrlTeknisiTiketMaintenanceSave: (req, res) => {
    try {
      const data = {
        ...req.params,
        ...req.body,
        ticket_close_date: moment().utcOffset("+0700").format(),
        status: "close",
      };
      let images = [];
      req.files.map((val) => {
        images.push({
          tiketing_id: parseInt(data.id),
          is_teknisi: true,
          image_url: process.env.IMG_URL + "tiketing/" + val.filename,
        });
      });

      SvcTeknisiTiketMaintenanceSave(data, images)
        .then((response) => res.status(response.responseCode).json(response))
        .catch((err) => res.status(err.responseCode).json(err));
    } catch (error) {
      console.log(error);
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
  CtrlTeknisiTiketInstallationSave: async (req, res) => {
    try {
      const data = {
        ...req.params,
        ...req.body,
      };
      if (!["open", "close"].includes(data.status_valve.toLocaleLowerCase())) {
        data.status_valve = "open";
      }
      let images = [];
      let dataInstallation = null;
      let dataInstallation_detail = null;
      let dataGateway = null;
      const { role, area_id } = res.locals.decoded;
      const models = require("../../../models/index");
      let tiket = await models.iots_tiketing.findOne({
        where: { id: data.id },
        attributes: ["id", "area_id", "type_user", "ticket_type", "user_id"],
      });
      let user = {};
      tiket = JSON.parse(JSON.stringify(tiket));
      if (tiket.type_user == "tenant" && tiket.ticket_type == "installation") {
        user = { tenantId: tiket.user_id };
      } else if (
        tiket.type_user == "internal" &&
        tiket.ticket_type == "installation"
      ) {
        user = { internalId: tiket.user_id };
      }
      data.areaId = tiket.area_id;
      data.area_id = tiket.area_id;
      if (role == "TEKNISI COMPANY") {
        if (isNaN(data.areaId)) {
          throw new Error("teknisi company must contain areaId");
        }
      }
      if (
        parseInt(data.save) == 1 &&
        data.installation_type.toLocaleLowerCase().trim() == "node"
      ) {
        data.ticket_close_date = moment().utcOffset("+0700").format();
        data.status = "close";
        if (role == "TEKNISI COMPANY") {
          if (isNaN(data.areaId)) {
            throw new Error("teknisi company must contain areaId");
          }
        } else {
          dataInstallation = { areaId: area_id };
        }
        let meter = {
          start_totalizer: 0,
          setting_pulse: 0,
          flag_install: false,
        };
        if (data.start_totalizer > 0) {
          meter.start_totalizer = data.start_totalizer * 1000;
          meter.flag_install = true;
        }
        if (data.interval > 0) {
          meter.setting_pulse = data.interval;
        }
        dataInstallation_detail = {
          ...dataInstallation_detail,
          ...user,
          ...meter,
          meter_id: data.meter_id,
          serial_number: data.meter_id,
          interval: 60,
          billing_type_id: data.billing_type == "POSTPAID" ? 1 : 2,
          installation_date: moment().format(),
          desc: data.description,
          devEui: data.dev_eui,
        };
        dataInstallation = {
          ...dataInstallation,
          ...user,
          ...meter,
          merk: data.merk,
          meter_id: data.meter_id,
          interval: 60,
          device_type_id: data.billing_type == "POSTPAID" ? 1 : 2,
          devEui: data.dev_eui,
          description: data.description,
          installationDate: data.installation_date,
          is_anomali: false,
          setting_valve:
            data.status_valve.toLocaleLowerCase() == "close" ||
            data.status_valve.toLocaleLowerCase() == "open"
              ? data.status_valve.toLocaleLowerCase()
              : "N/A",
          live_valve:
            data.status_valve.toLocaleLowerCase() == "close" ||
            data.status_valve.toLocaleLowerCase() == "open"
              ? data.status_valve.toLocaleLowerCase()
              : "N/A",
          live_interval: 60,
          setting_interval: 60,
        };
        ["field_billing", "model", "prepayment"].map((val) => {
          console.log(data[val]);
          if (data[val] != undefined) {
            dataInstallation_detail[val] = data[val];
            if (val == "field_billing") {
              dataInstallation.field_billing_rtu = data[val];
            }
            if (val == "prepayment") {
              dataInstallation.prepayment = parseFloat(data[val]) * 1000;
            }
          }
        });
        const { Op } = models.Sequelize;
        let type = await models.iot_node_type.findOne({
          where: { type_name: { [Op.substring]: data.node_type } },
          attributes: ["type_id", "type_name"],
        });
        dataInstallation.typeId = type.type_id;
        if (![1, 2].includes(type.type_id)) {
          dataInstallation.flag_install = false;
        }
        if (req.file != undefined) {
          console.log(req.file);
          dataInstallation.image_url =
            process.env.IMG_URL + "tiketing/" + req.file.filename;
          images.push({
            tiketing_id: parseInt(data.id),
            is_teknisi: true,
            image_url: process.env.IMG_URL + "tiketing/" + req.file.filename,
          });
        }
      } else if (
        parseInt(data.save) == 1 &&
        data.installation_type.toLocaleLowerCase().trim() == "gateway"
      ) {
        dataGateway = {
          gateway_name: data.gateway_name,
          unit_model: data.unit_model,
          mac_address: data.mac_address,
          description: data.description,
          installation_date: data.installation_date,
        };
      }
      SvcTeknisiTiketInstallationSave(
        data,
        images,
        dataInstallation,
        dataGateway,
        dataInstallation_detail
      )
        .then((response) => res.status(response.responseCode).json(response))
        .catch((err) => res.status(err.responseCode).json(err));
    } catch (error) {
      console.log(error);
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
};

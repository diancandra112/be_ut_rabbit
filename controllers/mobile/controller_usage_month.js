const {
  SvcUsageMonth,
  SvcUsageActivity,
} = require("../../services/mobile/service_usage_month");

module.exports = {
  CtrlUsageMonth: (req, res) => {
    const data = req.params;
    SvcUsageMonth(data)
      .then((response) => res.status(200).json(response))
      .catch((err) => res.status(err.responseCode).json(err));
  },
  CtrlUsageActivity: (req, res) => {
    const data = req.params;
    SvcUsageActivity(data)
      .then((response) => res.status(200).json(response))
      .catch((err) => res.status(err.responseCode).json(err));
  },
};

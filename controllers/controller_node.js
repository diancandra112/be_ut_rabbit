const {
  serviceCheckNodeRtuNew,
  serviceAddNode,
  serviceHistoryNode,
  serviceListNode,
  serviceListNodeV2,
  serviceDeviceCheck,
  serviceDeviceLog,
  serviceCheckNode,
  servicePaginationHistory,
  servicePaginationHistoryNopage,
  servicePaginationHistorySum,
  servicePaginationHistorySumV2,
  serviceRegisterNode,
  serviceListUnassignedNode,
  serviceAssignedNode,
  serviceHistoryNodev2,
  serviceHistoryFilterSum,
  serviceUpdateIstallation,
  serviceDeleteIstallation,
  serviceUnassignedNode,
  serviceAdjustBalace,
  servicePaginationHistoryMobile,
  servicePaginationHistorySumV2Mobile,
  serviceListPrepaidNode,
  serviceListNodeLink,
  serviceSaveNodeLink,
  serviceNodeUnlink,
  serviceUnassignedNodev2,
} = require("../services/service_node");
const { SvcSetInterval } = require("../services/actions/serviceInterval");
const { serviceNodeSumUsage } = require("../services/service_node_sum_usage");
const {
  serviceType5,
  serviceType13,
  serviceType16,
  serviceType17,
  serviceType18,
  serviceType19,
  serviceType6,
} = require("../services/service_log_type");
const {
  serviceType20,
  serviceType21,
  serviceType22,
  serviceType2,
  serviceType3,
  serviceType11,
  serviceType1,
} = require("../services/service_log_type2");
const { SvcSetTopup } = require("../services/actions/serviceTopup");
const hash = (data) => {
  const crypto = require("crypto-js");
  return crypto.SHA256(data).toString();
};

getDateArray = (first, end) => {
  // eslint-disable-next-line
  let arr = new Array();
  let dt = new Date(first);
  while (dt <= end) {
    arr.push(moment(new Date(dt)).format("YYYY-MM-DD"));
    dt.setDate(dt.getDate() + 1);
  }
  console.log(arr);
  return arr;
};

const moment = require("moment");
module.exports = {
  ControllerUnassignedNode: (req, res) => {
    const data = req.params;
    let flag = req.params.devEui.split("_");
    if (flag[0] == "unsigned" || flag[0] == "deleted") {
      res.status(400).json({
        responseCode: 400,
        messages: "node telah ter unsigned sebelumnya",
      });
    } else {
      serviceUnassignedNodev2(data)
        .then(async (result) => {
          const { DeleteCache } = require("../middlewares/chache");
          await DeleteCache(data.devEui);
          res.status(result.responseCode).json(result);
        })
        .catch((error) => res.status(error.responseCode).json(error));
    }
  },
  ControllerDeleteIstallation: (req, res) => {
    const data = req.body;
    serviceDeleteIstallation(data)
      .then(async (result) => {
        const { DeleteCache } = require("../middlewares/chache");
        await DeleteCache(data.devEUI);
        res.status(result.responseCode).json(result);
      })
      .catch((error) => res.status(error.responseCode).json(error));
  },
  ControllerUpdateIstallation: (req, res) => {
    const data = req.body;
    if (parseFloat(data.max_usage) > 0) {
      data.max_usage = parseFloat(data.max_usage) * 1000;
    }
    if (parseFloat(data.max_totalizer) > 0) {
      data.max_totalizer = parseFloat(data.max_totalizer) * 1000;
    }
    console.log(data, "data update ");
    serviceUpdateIstallation(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  ControllerAdjustBalace: (req, res) => {
    const data = req.params;
    serviceAdjustBalace(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  ControllerAddNode: (req, res) => {
    const data = req.body;
    serviceAddNode(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  ControllerRegisterNode: (req, res) => {
    const data = req.body;
    if (parseFloat(data.max_usage) > 0) {
      data.max_usage = parseFloat(data.max_usage) * 1000;
    }
    data.max_totalizer = parseFloat(data.max_totalizer) || 9999;
    if (data.max_totalizer > 0) {
      data.max_totalizer = data.max_totalizer * 1000;
    }
    data.areaId = res.locals.decoded.iot_area.id;
    const company_id = res.locals.decoded.iot_area.company_id;
    serviceRegisterNode(data, company_id)
      .then((result) => {
        if (data.device_type_id == 1) {
          let interval_update = {
            interval: parseInt(data.interval) * 60,
            devEUI: data.devEui,
          };
          SvcSetInterval(interval_update)
            .then((response) => console.log(result))
            .catch((error) => console.log(error));
          res.status(result.responseCode).json(result);
        } else {
          let prepayment_update = {
            topup: data.prepayment,
            devEUI: data.devEui,
          };
          SvcSetTopup(prepayment_update)
            .then((response) => console.log(result))
            .catch((error) => console.log(error));
          res.status(result.responseCode).json(result);
        }
      })
      .catch((error) => res.status(error.responseCode).json(error));
  },
  ControllerCheckNode: (req, res) => {
    const data = req.body;
    serviceCheckNode(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  ControllerListNodeV2: (req, res) => {
    const data = res.locals.data;
    serviceListNodeV2(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  ControllerListNodeLink: (req, res) => {
    const data = req.params;
    serviceListNodeLink(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  ControllerSaveNodeLink: (req, res) => {
    const data = req.params;
    let update = [
      { id: data.id, node_link_id: data.link_id },
      { id: data.link_id, node_link_id: data.id },
    ];
    serviceSaveNodeLink(update, data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  ControllerNodeUnlink: (req, res) => {
    const data = req.params;
    let update = [
      { id: data.id, node_link_id: null },
      { id: data.link_id, node_link_id: null },
    ];
    serviceNodeUnlink(update)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  ControllerListNode: (req, res) => {
    const data = res.locals.data;
    serviceListNode(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  ControllerListPrepaidNode: (req, res) => {
    const data = res.locals.decoded;
    let new_data = {
      tenantId: data.id,
      areaId: data.area_id,
      device_type_id: 2,
    };

    serviceListPrepaidNode(new_data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  ControllerListUnassignedNode: (req, res) => {
    const data = res.locals.data;

    serviceListUnassignedNode(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  ControllerAssignedNode: (req, res) => {
    const update = res.locals.update;
    const data = res.locals.data;
    serviceAssignedNode(data, update)
      .then(async (result) => {
        const { DeleteCache } = require("../middlewares/chache");
        await DeleteCache(data.devEui);
        res.status(result.responseCode).json(result);
      })
      .catch((error) => res.status(error.responseCode).json(error));
  },
  ControllerHistoryNode: (req, res) => {
    const data = res.locals.data;
    const page = res.locals.page;
    const page_size = res.locals.page_size;
    serviceHistoryNode(data, page, page_size)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  ControllerHistoryNodev2: (req, res) => {
    const data = res.locals.data;
    const page = res.locals.page;
    const page_size = res.locals.page_size;
    const typeId = res.locals.typeId;
    serviceHistoryNodev2(data, page, page_size, typeId)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  ControllerHistoryFilterSum: (req, res) => {
    const data = req.query;
    let pagination = {};
    pagination.offset = (parseInt(data.page) - 1) * parseInt(data.size) || 0;
    pagination.limit = parseInt(data.size) || 10;
    data.date_start = data.date_start.trim();
    data.date_end = data.date_end.trim();
    data.date_end = new Date(data.date_end);
    data.date_end.setDate(data.date_end.getDate() + 1);
    data.date_end = data.date_end;
    // console.log(pagination);
    serviceHistoryFilterSum(data, pagination)
      .then((result) => res.json(result))
      .catch((error) => res.json(error));
  },
  controllerPaginationHistorySumV2Mobile: (req, res) => {
    const { device_id, start_date, end_date } = req.params;
    const arr_date = getDateArray(moment(start_date), moment(end_date));
    const arr_date2 = getDateArray(
      moment(start_date).subtract(1, "day"),
      moment(end_date)
    );

    serviceNodeSumUsage(
      device_id,
      start_date,
      end_date,
      arr_date,
      arr_date2,
      true
    )
      .then((result) => res.json(result))
      .catch((error) => res.json(error));
    // servicePaginationHistorySumV2Mobile(
    //   device_id,
    //   start_date,
    //   end_date,
    //   arr_date,
    //   arr_date2
    // )
    //   .then((result) => res.json(result))
    //   .catch((error) => res.json(error));
  },
  controllerPaginationHistorySumV2: (req, res) => {
    const { device_id, start_date, end_date } = req.params;
    const arr_date = getDateArray(moment(start_date), moment(end_date));
    const arr_date2 = getDateArray(
      moment(start_date).subtract(1, "day"),
      moment(end_date)
    );
    serviceNodeSumUsage(device_id, start_date, end_date, arr_date, arr_date2)
      .then((result) => res.json(result))
      .catch((error) => res.json(error));
    // servicePaginationHistorySumV2(
    //   device_id,
    //   start_date,
    //   end_date,
    //   arr_date,
    //   arr_date2
    // )
    //   .then((result) => res.json(result))
    //   .catch((error) => res.json(error));
  },
  controllerPaginationHistorySum: (req, res) => {
    const data = req.query;
    let pagination = {};
    pagination.offset = (parseInt(data.page) - 1) * parseInt(data.size) || 0;
    pagination.limit = parseInt(data.size) || 10;
    data.date_start = moment(data.date_start).format("YYYY-MM-DD");
    data.date_end = moment(data.date_end).add(1, "d").format("YYYY-MM-DD");
    // console.log(data, pagination);
    servicePaginationHistorySum(data, pagination)
      .then((result) => res.json(result))
      .catch((error) => res.json(error));
  },
  controllerDeviceLog: (req, res) => {
    const data = req.query;
    const device_id = req.params.devEui;
    const userId = res.locals.decoded.id;
    serviceDeviceCheck({ areaId: userId, devEui: device_id })
      .then((result_check) => {
        result_check = JSON.parse(JSON.stringify(result_check));
        let page = data.page ? parseInt(data.page) : 1;
        let size = data.size ? parseInt(data.size) : 1;
        start = data.start_date
          ? moment(data.start_date).format("YYYY-MM-DD")
          : data.start_date;
        end = data.end_date
          ? moment(data.end_date).format("YYYY-MM-DD")
          : data.end_date;

        serviceDeviceLog(
          { device_id: result_check.data.device_id },
          page,
          size,
          result_check.data.typeId,
          result_check.data.child,
          result_check.data.sn,
          result_check.data.type_name,
          start,
          end,
          result_check.data.devEui,
          result_check.data.latitude,
          result_check.data.longitude,
          result_check.data.altitude
        )
          .then((result_log) => {
            res.status(result_log.responseCode).json(result_log);
          })
          .catch((error) => res.status(error.responseCode).json(error));
      })
      .catch((error) => res.status(error.responseCode).json(error));
  },
  controllerPaginationHistory: (req, res) => {
    const data = req.query;
    let pagination = {};
    let pagination_flag = false;
    if (data.page != undefined || data.size != undefined) {
      pagination.offset = (parseInt(data.page) - 1) * parseInt(data.size) || 0;
      pagination.limit = parseInt(data.size) || 10;
      pagination_flag = true;
    }

    if (data.date_start != undefined && data.date_end == undefined) {
      data.date_start = moment(data.date_start).format("YYYY-MM-DD");
      data.date_end = moment(data.date_start).add(1, "d").format("YYYY-MM-DD");
    } else if (data.date_start != undefined && data.date_end != undefined) {
      data.date_start = moment(data.date_start).format("YYYY-MM-DD");
      data.date_end = moment(data.date_end).add(1, "d").format("YYYY-MM-DD");
    }
    let typeId = parseInt(data.typeId);
    if (data.typeId == 1) {
      serviceType1(data)
        .then((result) => res.json(result))
        .catch((error) => res.json(error));
    } else if (data.typeId == 11) {
      serviceType11(data)
        .then((result) => res.json(result))
        .catch((error) => res.json(error));
    } else if (data.typeId == 2) {
      serviceType2(data)
        .then((result) => res.json(result))
        .catch((error) => res.json(error));
    } else if (data.typeId == 3) {
      serviceType3(data)
        .then((result) => res.json(result))
        .catch((error) => res.json(error));
    } else if (data.typeId == 22) {
      serviceType22(data)
        .then((result) => res.json(result))
        .catch((error) => res.json(error));
    } else if (data.typeId == 21) {
      serviceType21(data)
        .then((result) => res.json(result))
        .catch((error) => res.json(error));
    } else if (data.typeId == 20) {
      serviceType20(data)
        .then((result) => res.json(result))
        .catch((error) => res.json(error));
    } else if (data.typeId == 19) {
      serviceType19(data)
        .then((result) => res.json(result))
        .catch((error) => res.json(error));
    } else if (data.typeId == 18) {
      serviceType18(data)
        .then((result) => res.json(result))
        .catch((error) => res.json(error));
    } else if (data.typeId == 17) {
      serviceType17(data)
        .then((result) => res.json(result))
        .catch((error) => res.json(error));
    } else if (data.typeId == 16) {
      serviceType16(data)
        .then((result) => res.json(result))
        .catch((error) => res.json(error));
    } else if (data.typeId == 13) {
      serviceType13(data, pagination, pagination_flag)
        .then((result) => res.json(result))
        .catch((error) => res.json(error));
    } else if (data.typeId == 6) {
      serviceType6(data, pagination, pagination_flag)
        .then((result) => res.json(result))
        .catch((error) => res.json(error));
    } else {
      servicePaginationHistory(data, pagination, pagination_flag)
        .then(async (result) => {
          if (typeId == 14) {
            const models = require("../models/index");
            const { Op } = models.Sequelize;
            result.rows.map((val, idx) => {
              let exReading = JSON.parse(val.payload).meter.exReading;
              exReading.map((val, idx2) => {
                exReading[idx2].value = (parseFloat(val.value) / 1000).toFixed(
                  3
                );
                // .replace(/[.,]/g, function (x) {
                //   return x == "," ? "." : ",";
                // });
              });
              exReading.unshift({
                log_id: val.id,
                node_id: val.device_id,
                devEui: val.devEui,
                id: 0,
                name: "Update Time",
                value: moment(JSON.parse(val.payload).reportTime)
                  .utcOffset("+0700")
                  .format("YYYY-MM-DD HH:mm:ss"),
                reportTime: JSON.parse(val.payload).reportTime,
              });
              result.rows[idx] = exReading;
            });
            if (result.rows.length > 0) {
              try {
                let data_new = result.rows[result.rows.length - 1];
                let iot_detail_rtu = await models.iot_detail_rtu.findOne({
                  where: { node_id: data_new[0].node_id },
                });
                if (
                  iot_detail_rtu != null &&
                  iot_detail_rtu.field_billing != null
                ) {
                  let log = result.rows[result.rows.length - 1][0];
                  let log_1 = parseFloat(
                    result.rows[result.rows.length - 1].find((valP) => {
                      return (
                        valP.name.trim().toLocaleLowerCase() ==
                        iot_detail_rtu.field_billing.trim().toLocaleLowerCase()
                      );
                    }).value
                  );
                  let last_log = await models.iot_histories.findOne({
                    where: {
                      device_id: log.node_id,
                      devEui: log.devEui,
                      id: { [Op.lt]: log.log_id },
                      reportTime: { [Op.lt]: log.reportTime },
                    },
                    order: [["reportTime", "DESC"]],
                  });
                  if (last_log != null) {
                    last_log = JSON.parse(last_log.payload).meter.exReading;
                    last_log = parseFloat(
                      last_log.find((valP) => {
                        return (
                          valP.name.trim().toLocaleLowerCase() ==
                          iot_detail_rtu.field_billing
                            .trim()
                            .toLocaleLowerCase()
                        );
                      }).value
                    );
                  } else {
                    last_log = 0;
                  }
                  result.rows[result.rows.length - 1] = [
                    ...result.rows[result.rows.length - 1],
                    {
                      id: result.rows[result.rows.length - 1].length,
                      name: `Usage ${iot_detail_rtu.field_billing}`,
                      value: (log_1 - last_log / 1000)
                        .toFixed(3)
                        .replace(/[.,]/g, function (x) {
                          return x == "," ? "." : ",";
                        }),
                    },
                  ];

                  result.rows.map((val, idx) => {
                    let usages = 0;
                    if (idx < result.rows.length - 1) {
                      let usage_1 = parseFloat(
                        val.find((valP) => {
                          return (
                            valP.name.trim().toLocaleLowerCase() ==
                            iot_detail_rtu.field_billing
                              .trim()
                              .toLocaleLowerCase()
                          );
                        }).value
                      );
                      let usage_2 = parseFloat(
                        result.rows[idx + 1].find((valP) => {
                          return (
                            valP.name.trim().toLocaleLowerCase() ==
                            iot_detail_rtu.field_billing
                              .trim()
                              .toLocaleLowerCase()
                          );
                        }).value
                      );
                      usages = (usage_1 - usage_2)
                        .toFixed(3)
                        .replace(/[.,]/g, function (x) {
                          return x == "," ? "." : ",";
                        });
                      // console.log(usage_1, usage_2);
                      result.rows[idx] = [
                        ...val,
                        {
                          id: val.length,
                          name: `Usage ${iot_detail_rtu.field_billing}`,
                          value: usages,
                        },
                      ];
                    }
                  });
                }
              } catch (error) {
                console.log(error, "error type 14");
              }
            }
          } else if (typeId == 7) {
            result.rows.map((val, idx) => {
              val.raw.meter.exReading.map((val2, idx2) => {
                result.rows[idx].raw.meter.exReading[idx2].value = (
                  parseFloat(val2.value) / 1000
                )
                  .toFixed(3)
                  .replace(/[.,]/g, function (x) {
                    return x == "," ? "." : ",";
                  });
              });
            });
          } else if (typeId == 11 || typeId == 12) {
            result.rows.map((val, idx) => {
              result.rows[idx].payload = JSON.parse(result.rows[idx].payload);
              result.rows[idx].payload.meter.exReading.map((val2, idx2) => {
                if (val2.name.trim() == "Pressure") {
                  result.rows[idx].payload.meter.exReading[idx2].value = (
                    result.rows[idx].payload.meter.exReading[idx2].value /
                    100000
                  ).toFixed(3);
                }
              });
              result.rows[idx].payload = JSON.stringify(
                result.rows[idx].payload
              );
            });
          } else if (typeId == 1) {
            if (result.rows.length > 0) {
              try {
                let temp_res = result.rows[result.rows.length - 1];
                const models = require("../models/index");
                const { Op } = models.Sequelize;
                let last_log = await models.iot_histories.findOne({
                  where: {
                    device_id: temp_res.device_id,
                    devEui: temp_res.devEui,
                    id: { [Op.lt]: temp_res.id },
                    reportTime: { [Op.lt]: temp_res.reportTime },
                  },
                  order: [["reportTime", "DESC"]],
                });
                if (last_log != null) {
                  last_log = JSON.parse(JSON.stringify(last_log));
                  result.rows[result.rows.length - 1].usage =
                    (parseFloat(
                      result.rows[result.rows.length - 1].payload.totalizer
                    ) -
                      parseFloat(
                        JSON.parse(last_log.payload).meter.meterReading
                      )) /
                    1000;
                }
              } catch (error) {
                console.log(error);
              }
            }
          }
          res.json(result);
        })
        .catch((error) => res.json(error));
    }
  },
  controllerPaginationHistoryMobile: (req, res) => {
    let data = req.query;
    let pagination = {};
    let pagination_flag = false;
    if (data.page != undefined || data.size != undefined) {
      data.page = parseInt(data.page);
      data.size = parseInt(data.size);
      pagination.offset = (parseInt(data.page) - 1) * parseInt(data.size) || 0;
      pagination.limit = parseInt(data.size) || 10;
      pagination_flag = true;
    }

    if (data.date_start != undefined && data.date_end == undefined) {
      data.date_start = moment(data.date_start).format("YYYY-MM-DD");
      data.date_end = moment(data.date_start).add(1, "d").format("YYYY-MM-DD");
    } else if (data.date_start != undefined && data.date_end != undefined) {
      data.date_start = moment(data.date_start).format("YYYY-MM-DD");
      data.date_end = moment(data.date_end).add(1, "d").format("YYYY-MM-DD");
    } else {
      data.date_start = moment().format("YYYY-MM-DD");
      data.date_end = moment().add(1, "d").format("YYYY-MM-DD");
    }
    let typeId = parseInt(data.typeId);
    if (data.device_id == 138138986) {
      serviceType5(data, pagination, pagination_flag)
        .then((result) => {
          result.rows.map((val, idx) => {
            result.rows[idx].preassure = result.rows[idx].preassure.toFixed(2);
          });
          res.json(result);
        })
        .catch((error) => res.json(error));
    } else if (data.typeId == 13) {
      serviceType13(data, pagination, pagination_flag)
        .then((result) => res.json(result))
        .catch((error) => res.json(error));
    } else if (data.typeId == 2) {
      serviceType2(data, true)
        .then((result) => res.json(result))
        .catch((error) => res.json(error));
    } else {
      servicePaginationHistoryMobile(data, pagination, pagination_flag)
        .then((result) => res.json(result))
        .catch((error) => res.json(error));
    }
  },
  controllerPaginationHistorynopage: (req, res) => {
    const data = req.query;
    if (data.date_start != undefined && data.date_end == undefined) {
      data.date_start = moment(data.date_start).format("YYYY-MM-DD");
      data.date_end = moment(data.date_start).add(1, "d").format("YYYY-MM-DD");
    } else if (data.date_start != undefined && data.date_end != undefined) {
      data.date_start = moment(data.date_start).format("YYYY-MM-DD");
      data.date_end = moment(data.date_end).add(1, "d").format("YYYY-MM-DD");
    }
    servicePaginationHistoryNopage(data)
      .then((result) => res.json(result))
      .catch((error) => res.json(error));
  },
  ControllerCheckNodeRtuNew: (req, res) => {
    const data = req.body;
    serviceCheckNodeRtuNew(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
};

const {
  svcAssignedNodeList,
  svcAssignedNodeCount,
} = require("../../services/dashboard/service_assigned");

module.exports = {
  ctrlAssignedNodeCount: (req, res) => {
    const data = req.body;
    const { role } = res.locals.decoded;
    if (role == "COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else if (role == "TEKNISI") {
      data.areaId = res.locals.decoded.area_id;
    } else if (role == "TEKNISI COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else {
      data.areaId = res.locals.decoded.iot_area.id;
    }
    svcAssignedNodeCount(data)
      .then((_res) => res.status(_res.responseCode).json(_res))
      .catch((_err) => res.status(_err.responseCode).json(_err));
  },
  ctrlAssignedNodeList: (req, res) => {
    const data = req.query;
    const page = data.page;
    const size = data.size;
    let is_mobile = false;
    const search = data.search || "";
    const sort = data.sort || "id";
    const order = data.order || "DESC";
    const start_date = data.start_date;
    const end_date = data.end_date;

    delete data.search;
    delete data.sort;
    delete data.order;
    delete data.start_date;
    delete data.end_date;
    delete data.page;
    delete data.size;
    const { role } = res.locals.decoded;
    if (role == "COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else if (role == "TEKNISI") {
      is_mobile = true;
      data.areaId = res.locals.decoded.area_id;
    } else if (role == "TEKNISI COMPANY") {
      is_mobile = true;
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else {
      data.areaId = res.locals.decoded.iot_area.id;
    }
    svcAssignedNodeList(
      data,
      parseInt(page),
      parseInt(size),
      req.params.tenant,
      is_mobile,
      search,
      sort,
      order,
      start_date,
      end_date
    )
      .then((_res) => res.status(_res.responseCode).json(_res))
      .catch((_err) => res.status(_err.responseCode).json(_err));
  },
};

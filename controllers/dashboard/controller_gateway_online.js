const {
  svcGatewayOnlineCount,
  svcGatewayOnlineList,
  svcNodeList,
} = require("../../services/dashboard/service_gateway_online");

module.exports = {
  ctrlGatewayOnlineCount: (req, res) => {
    const data = req.body;
    const { role } = res.locals.decoded;
    if (role == "COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else if (role == "TEKNISI") {
      data.areaId = res.locals.decoded.area_id;
    } else if (role == "TEKNISI COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else {
      data.areaId = res.locals.decoded.iot_area.id;
    }
    if (data.areaId != undefined) {
      data.area_id = data.areaId;
      delete data.areaId;
    }
    svcGatewayOnlineCount(data)
      .then((_res) => res.status(_res.responseCode).json(_res))
      .catch((_err) => res.status(_err.responseCode).json(_err));
  },
  ctrlGatewayOnlineList: (req, res) => {
    const data = req.query;
    const page = data.page;
    const size = data.size;
    const search = data.search || "";
    const sort = data.sort || "id";
    const order = data.order || "DESC";
    const start_date = data.start_date;
    const end_date = data.end_date;

    delete data.search;
    delete data.sort;
    delete data.order;
    delete data.start_date;
    delete data.end_date;
    delete data.page;
    delete data.size;
    const { role } = res.locals.decoded;
    if (role == "COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else if (role == "TEKNISI") {
      data.areaId = res.locals.decoded.area_id;
    } else if (role == "TEKNISI COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else {
      data.areaId = res.locals.decoded.iot_area.id;
    }
    if (data.areaId != undefined) {
      data.area_id = data.areaId;
      delete data.areaId;
    }
    svcGatewayOnlineList(
      data,
      parseInt(page),
      parseInt(size),
      req.params.tenant,
      search,
      sort,
      order,
      start_date,
      end_date
    )
      .then((_res) => res.status(_res.responseCode).json(_res))
      .catch((_err) => res.status(_err.responseCode).json(_err));
  },
  ctrlNodeList: (req, res) => {
    const data = req.query;
    const { role } = res.locals.decoded;
    if (role == "COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else if (role == "TEKNISI") {
      data.areaId = res.locals.decoded.area_id;
    } else if (role == "TEKNISI COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else {
      data.areaId = res.locals.decoded.iot_area.id;
    }
    svcNodeList(data)
      .then((_res) => res.status(_res.responseCode).json(_res))
      .catch((_err) => res.status(_err.responseCode).json(_err));
  },
};

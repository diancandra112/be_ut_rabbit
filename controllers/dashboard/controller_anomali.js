const {
  svcNodeAnomaliCount,
  svcNodeAnomaliList,
  svcNodeAnomaliListTime,
  svcNodeAnomaliClose,
  svcNodeAnomaliCloseStatus,
  svcNodeAnomaliTimeCount,
  svcNodeAnomaliCloseStatusAll,
  svcNodeAnomaliUsageChart,
} = require("../../services/dashboard/service_node_anomali");

module.exports = {
  ctrlNodeAnomaliCount: (req, res) => {
    const data = req.body;
    const { role } = res.locals.decoded;
    if (role == "COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else if (role == "TEKNISI") {
      data.areaId = res.locals.decoded.area_id;
    } else if (role == "TEKNISI COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else {
      data.areaId = res.locals.decoded.iot_area.id;
    }
    svcNodeAnomaliCount(data)
      .then((_res) => res.status(_res.responseCode).json(_res))
      .catch((_err) => res.status(_err.responseCode).json(_err));
  },
  ctrlNodeAnomaliList: (req, res) => {
    const data = req.query;
    const page = data.page;
    const size = data.size;
    const search = data.search || "";
    const sort = data.sort || "id";
    const order = data.order || "DESC";
    const start_date = data.start_date;
    const end_date = data.end_date;

    delete data.search;
    delete data.sort;
    delete data.order;
    delete data.start_date;
    delete data.end_date;
    delete data.page;
    delete data.size;
    const { role } = res.locals.decoded;
    if (role == "COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else if (role == "TEKNISI") {
      data.areaId = res.locals.decoded.area_id;
    } else if (role == "TEKNISI COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else {
      data.areaId = res.locals.decoded.iot_area.id;
    }
    svcNodeAnomaliList(
      data,
      parseInt(page),
      parseInt(size),
      req.params.tenant,
      search,
      sort,
      order,
      start_date,
      end_date
    )
      .then((_res) => res.status(_res.responseCode).json(_res))
      .catch((_err) => res.status(_err.responseCode).json(_err));
  },
  ctrlNodeAnomaliListTime: (req, res) => {
    let data = req.query;
    const page = data.page;
    const size = data.size;
    const search = data.search || "";
    const sort = data.sort || "id";
    const order = data.order || "DESC";
    const start_date = data.start_date;
    const end_date = data.end_date;

    delete data.search;
    delete data.sort;
    delete data.order;
    delete data.start_date;
    delete data.end_date;
    delete data.page;
    delete data.size;
    const { role } = res.locals.decoded;
    if (role == "COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else if (role == "TEKNISI") {
      data.areaId = res.locals.decoded.area_id;
    } else if (role == "TEKNISI COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else {
      data.areaId = res.locals.decoded.iot_area.id;
    }
    if (parseInt(req.params.tenant) == 1) {
      data = { ...data, internalId: null };
    } else {
      data = { ...data, tenantId: null };
    }
    if (parseInt(req.params.tenant) == 1) {
      data = {
        ...data,
        anomi_type: "USAGE TIME",
        internalId: null,
        status: parseInt(req.params.status),
      };
    } else {
      data = {
        ...data,
        anomi_type: "USAGE TIME",
        tenantId: null,
        status: parseInt(req.params.status),
      };
    }
    console.log(data, parseInt(page), parseInt(size));
    svcNodeAnomaliListTime(
      data,
      parseInt(page),
      parseInt(size),
      search,
      sort,
      order,
      start_date,
      end_date
    )
      .then((_res) => res.status(_res.responseCode).json(_res))
      .catch((_err) => res.status(_err.responseCode).json(_err));
  },
  ctrlNodeAnomaliUsageCount: (req, res) => {
    let data = req.query;
    const { role } = res.locals.decoded;
    if (role == "COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else if (role == "TEKNISI") {
      data.areaId = res.locals.decoded.area_id;
    } else if (role == "TEKNISI COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else {
      data.areaId = res.locals.decoded.iot_area.id;
    }
    data.anomi_type = "USAGE INTERVAL";
    svcNodeAnomaliTimeCount(data)
      .then((_res) => res.status(_res.responseCode).json(_res))
      .catch((_err) => res.status(_err.responseCode).json(_err));
  },
  ctrlNodeAnomaliTimeCount: (req, res) => {
    let data = req.query;
    const { role } = res.locals.decoded;
    if (role == "COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else if (role == "TEKNISI") {
      data.areaId = res.locals.decoded.area_id;
    } else if (role == "TEKNISI COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else {
      data.areaId = res.locals.decoded.iot_area.id;
    }
    data.anomi_type = "USAGE TIME";
    svcNodeAnomaliTimeCount(data)
      .then((_res) => res.status(_res.responseCode).json(_res))
      .catch((_err) => res.status(_err.responseCode).json(_err));
  },
  ctrlNodeAnomaliListUsage: (req, res) => {
    let data = req.query;
    const page = data.page;
    const size = data.size;
    const search = data.search || "";
    const sort = data.sort || "id";
    const order = data.order || "DESC";
    const start_date = data.start_date;
    const end_date = data.end_date;

    delete data.search;
    delete data.sort;
    delete data.order;
    delete data.start_date;
    delete data.end_date;
    delete data.page;
    delete data.size;
    const { role } = res.locals.decoded;
    if (role == "COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else if (role == "TEKNISI") {
      data.areaId = res.locals.decoded.area_id;
    } else if (role == "TEKNISI COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else {
      data.areaId = res.locals.decoded.iot_area.id;
    }
    if (parseInt(req.params.tenant) == 1) {
      data = { ...data, internalId: null };
    } else {
      data = { ...data, tenantId: null };
    }
    if (parseInt(req.params.tenant) == 1) {
      data = {
        ...data,
        anomi_type: "USAGE INTERVAL",
        internalId: null,
        status: parseInt(req.params.status),
      };
    } else {
      data = {
        ...data,
        anomi_type: "USAGE INTERVAL",
        tenantId: null,
        status: parseInt(req.params.status),
      };
    }
    svcNodeAnomaliListTime(
      data,
      parseInt(page),
      parseInt(size),
      search,
      sort,
      order,
      start_date,
      end_date
    )
      .then((_res) => res.status(_res.responseCode).json(_res))
      .catch((_err) => res.status(_err.responseCode).json(_err));
  },
  ctrlNodeAnomaliListClose: (req, res) => {
    let data = { areaId: res.locals.decoded.iot_area.id, ...req.query };
    svcNodeAnomaliClose(data)
      .then((_res) => res.status(_res.responseCode).json(_res))
      .catch((_err) => res.status(_err.responseCode).json(_err));
  },
  ctrlNodeAnomaliListCloseStatus: (req, res) => {
    let data = req.params;
    svcNodeAnomaliCloseStatus(data)
      .then((_res) => res.status(_res.responseCode).json(_res))
      .catch((_err) => res.status(_err.responseCode).json(_err));
  },
  ctrlNodeAnomaliListCloseStatusAll: (req, res) => {
    let data = { areaId: res.locals.decoded.iot_area.id, ...req.query };
    svcNodeAnomaliCloseStatusAll(data)
      .then((_res) => res.status(_res.responseCode).json(_res))
      .catch((_err) => res.status(_err.responseCode).json(_err));
  },
  ctrlNodeAnomaliUsageChart: (req, res) => {
    let data = req.query;
    svcNodeAnomaliUsageChart(data)
      .then((_res) => res.status(_res.responseCode).json(_res))
      .catch((_err) => res.status(_err.responseCode).json(_err));
  },
};

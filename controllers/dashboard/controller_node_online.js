const {
  svcOnlineNode,
  svcOnlineNodeList,
  svcPressureOnlineNode,
  svcPressureOnlineNodeCount,
  svcNodePressure,
  svcNodeRusakCount,
  svcNodeRusakList,
} = require("../../services/dashboard/service_node_online");

module.exports = {
  ctrlPressureOnlineNodeCount: (req, res) => {
    const data = req.query;
    const { role } = res.locals.decoded;
    if (role == "COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else if (role == "TEKNISI") {
      data.areaId = res.locals.decoded.area_id;
    } else if (role == "TEKNISI COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else {
      data.areaId = res.locals.decoded.iot_area.id;
    }
    svcPressureOnlineNodeCount(data)
      .then((_res) => res.status(_res.responseCode).json(_res))
      .catch((_err) => res.status(_err.responseCode).json(_err));
  },
  ctrlPressureOnlineNode: (req, res) => {
    const data = req.query;
    const page = isNaN(data.page) == true ? 1 : data.page;
    const size = isNaN(data.size) == true ? 10 : data.size;
    const search = data.search || "";
    const sort = data.sort || "id";
    const order = data.order || "DESC";
    const start_date = data.start_date;
    const end_date = data.end_date;

    delete data.search;
    delete data.sort;
    delete data.order;
    delete data.start_date;
    delete data.end_date;
    delete data.page;
    delete data.size;
    const { role } = res.locals.decoded;
    if (role == "COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else if (role == "TEKNISI") {
      data.areaId = res.locals.decoded.area_id;
    } else if (role == "TEKNISI COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else {
      data.areaId = res.locals.decoded.iot_area.id;
    }
    svcPressureOnlineNode(
      data,
      parseInt(page),
      parseInt(size),
      req.params.tenant,
      search,
      sort,
      order,
      start_date,
      end_date
    )
      .then((_res) => res.status(_res.responseCode).json(_res))
      .catch((_err) => res.status(_err.responseCode).json(_err));
  },
  ctrlOnlineNode: (req, res) => {
    const data = req.body;
    const { role } = res.locals.decoded;
    if (role == "COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else if (role == "TEKNISI") {
      data.areaId = res.locals.decoded.area_id;
    } else if (role == "TEKNISI COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else {
      data.areaId = res.locals.decoded.iot_area.id;
    }
    svcOnlineNode(data)
      .then((_res) => res.status(_res.responseCode).json(_res))
      .catch((_err) => res.status(_err.responseCode).json(_err));
  },
  ctrlNodePressure: (req, res) => {
    const data = req.body;
    const { role } = res.locals.decoded;
    if (role == "COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else if (role == "TEKNISI") {
      data.areaId = res.locals.decoded.area_id;
    } else if (role == "TEKNISI COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else {
      data.areaId = res.locals.decoded.iot_area.id;
    }
    svcNodePressure(data)
      .then((_res) => res.status(_res.responseCode).json(_res))
      .catch((_err) => res.status(_err.responseCode).json(_err));
  },
  ctrlOnlineNodeList: (req, res) => {
    const data = req.query;
    const page = isNaN(data.page) == true ? 1 : data.page;
    const size = isNaN(data.size) == true ? 10 : data.size;
    const search = data.search || "";
    const sort = data.sort || "id";
    const order = data.order || "DESC";
    const start_date = data.start_date;
    const end_date = data.end_date;

    delete data.search;
    delete data.sort;
    delete data.order;
    delete data.start_date;
    delete data.end_date;
    delete data.page;
    delete data.size;
    const { role } = res.locals.decoded;
    if (role == "COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else if (role == "TEKNISI") {
      data.areaId = res.locals.decoded.area_id;
    } else if (role == "TEKNISI COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else {
      data.areaId = res.locals.decoded.iot_area.id;
    }
    svcOnlineNodeList(
      data,
      parseInt(page),
      parseInt(size),
      req.params.tenant,
      search,
      sort,
      order,
      start_date,
      end_date
    )
      .then((_res) => res.status(_res.responseCode).json(_res))
      .catch((_err) => res.status(_err.responseCode).json(_err));
  },
  ctrlNodeRusakCount: (req, res) => {
    const data = req.body;
    const { role } = res.locals.decoded;
    if (role == "COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else if (role == "TEKNISI") {
      data.areaId = res.locals.decoded.area_id;
    } else if (role == "TEKNISI COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else {
      data.areaId = res.locals.decoded.iot_area.id;
    }
    svcNodeRusakCount(data)
      .then((_res) => res.status(_res.responseCode).json(_res))
      .catch((_err) => res.status(_err.responseCode).json(_err));
  },
  ctrlNodeRusakList: (req, res) => {
    const data = req.query;
    const { role } = res.locals.decoded;
    if (role == "COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else if (role == "TEKNISI") {
      data.areaId = res.locals.decoded.area_id;
    } else if (role == "TEKNISI COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else {
      data.areaId = res.locals.decoded.iot_area.id;
    }
    let page = isNaN(data.page) ? 1 : parseInt(data.page);
    let size = isNaN(data.size) ? 10 : parseInt(data.size);
    delete data.page;
    delete data.size;
    svcNodeRusakList(data, page, size)
      .then((_res) => res.status(_res.responseCode).json(_res))
      .catch((_err) => res.status(_err.responseCode).json(_err));
  },
};

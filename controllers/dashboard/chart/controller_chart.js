const {
  svcCartNodeDays,
  svcCartNodeYesterday,
  svcChartLast7days,
  svcChartLast30days,
} = require("../../../services/dashboard/chart/index");

module.exports = {
  ctrlCartNodeDays: (req, res) => {
    const data = { ...req.params };
    let type =
      req.query.type_name != null ? { type: req.query.type_name } : null;
    const { role } = res.locals.decoded;
    if (role == "COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else if (role == "TEKNISI") {
      data.areaId = res.locals.decoded.area_id;
    } else if (role == "TEKNISI COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else {
      data.areaId = res.locals.decoded.iot_area.id;
    }
    svcCartNodeDays(data, type)
      .then((_res) => res.status(_res.responseCode).json(_res))
      .catch((_err) => res.status(_err.responseCode).json(_err));
  },
  ctrlCartNodeYesterday: (req, res) => {
    const data = { ...req.params };
    let type =
      req.query.type_name != null ? { type: req.query.type_name } : null;
    const { role } = res.locals.decoded;
    if (role == "COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else if (role == "TEKNISI") {
      data.areaId = res.locals.decoded.area_id;
    } else if (role == "TEKNISI COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else {
      data.areaId = res.locals.decoded.iot_area.id;
    }
    svcCartNodeYesterday(data, type)
      .then((_res) => res.status(_res.responseCode).json(_res))
      .catch((_err) => res.status(_err.responseCode).json(_err));
  },
  ctrlChartLast7days: (req, res) => {
    const data = { ...req.params };
    const { role } = res.locals.decoded;
    let type =
      req.query.type_name != null ? { type: req.query.type_name } : null;
    if (role == "COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else if (role == "TEKNISI") {
      data.areaId = res.locals.decoded.area_id;
    } else if (role == "TEKNISI COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else {
      data.areaId = res.locals.decoded.iot_area.id;
    }
    svcChartLast7days(data, type)
      .then((_res) => res.status(_res.responseCode).json(_res))
      .catch((_err) => res.status(_err.responseCode).json(_err));
  },
  ctrlChartLast30days: (req, res) => {
    const data = { ...req.params };
    let type =
      req.query.type_name != null ? { type: req.query.type_name } : null;
    const { role } = res.locals.decoded;
    if (role == "COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else if (role == "TEKNISI") {
      data.areaId = res.locals.decoded.area_id;
    } else if (role == "TEKNISI COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else {
      data.areaId = res.locals.decoded.iot_area.id;
    }
    svcChartLast30days(data, type)
      .then((_res) => res.status(_res.responseCode).json(_res))
      .catch((_err) => res.status(_err.responseCode).json(_err));
  },
};

const {
  serviceGetType,
  serviceGetTypeCount,
  serviceGetTypeGas,
  serviceGetTypeWaterLevel,
} = require("../../../services/dashboard/chart/service_type");
module.exports = {
  controllerGetType: (req, res) => {
    const data = req.query;
    const { role } = res.locals.decoded;
    if (role == "COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else if (role == "TEKNISI") {
      data.areaId = res.locals.decoded.area_id;
    } else if (role == "TEKNISI COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else {
      data.areaId = res.locals.decoded.iot_area.id;
    }
    serviceGetType(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  controllerGetTypeCount: (req, res) => {
    const data = req.body;
    let q = req.query;
    let id = [9, 12];
    if (q.type != null && q.type == "alert") {
      id = [5, 9, 11, 12];
    }
    delete data.type;
    const { role } = res.locals.decoded;
    if (role == "COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else if (role == "TEKNISI") {
      data.areaId = res.locals.decoded.area_id;
    } else if (role == "TEKNISI COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else {
      data.areaId = res.locals.decoded.iot_area.id;
    }
    serviceGetTypeCount(data, id)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  controllerGetTypeGas: (req, res) => {
    const data = req.body;
    const { role } = res.locals.decoded;
    if (role == "COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else if (role == "TEKNISI") {
      data.areaId = res.locals.decoded.area_id;
    } else if (role == "TEKNISI COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else {
      data.areaId = res.locals.decoded.iot_area.id;
    }
    serviceGetTypeGas(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  controllerGetTypeWaterLevel: (req, res) => {
    const data = req.body;
    const { role } = res.locals.decoded;
    if (role == "COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else if (role == "TEKNISI") {
      data.areaId = res.locals.decoded.area_id;
    } else if (role == "TEKNISI COMPANY") {
      if (req.query.areaId != undefined) {
        data.areaId = req.query.areaId;
      }
    } else {
      data.areaId = res.locals.decoded.iot_area.id;
    }
    serviceGetTypeWaterLevel(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
};

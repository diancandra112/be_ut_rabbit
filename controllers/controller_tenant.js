const {
  serviceTenantEdit,
  serviceTenantGet,
  serviceTenantRegister,
} = require("../services/services_tenant");
const hash = (data) => {
  const crypto = require("crypto-js");
  return crypto.SHA256(data).toString();
};
require("dotenv").config();
module.exports = {
  controllerTenantRegister: (req, res) => {
    const nodeType = res.locals.nodeType;
    const dataUser = {
      ...res.locals.dataUser,
      email: req.body.username,
      password: hash(req.body.password),
      area_id: res.locals.decoded.iot_area.id,
    };
    const dataProfile = res.locals.dataProfile;
    dataProfile.password_flag = dataProfile.password;
    dataProfile.password = hash(dataProfile.password);
    if (dataProfile.image != undefined) {
      dataProfile.image = dataProfile.image.split("/");
      dataProfile.image =
        process.env.IMG_URL + dataProfile.image[dataProfile.image.length - 1];
    }
    serviceTenantRegister(dataUser, dataProfile, nodeType)
      .then((result) => res.json(result))
      .catch((error) => res.json(error));
  },
  controllerTenantGet: (req, res) => {
    try {
      const data = req.query;
      const { role } = res.locals.decoded;
      if (data.areaId) {
        data.area_id = data.areaId;
      }
      if (role == "COMPANY") {
        if (data.area_id == undefined) {
          return res.status(400).json({
            responseCode: 400,
            messages: `field area_id harus di deklarasikan`,
          });
        }
      } else if (role == "TEKNISI") {
        data.area_id = res.locals.decoded.area_id;
      } else if (role == "TEKNISI COMPANY") {
        // if (req.query.areaId == undefined) {
        //   return res.status(400).json({
        //     responseCode: 400,
        //     messages: `field areaId harus di deklarasikan`,
        //   });
        // } else {
        //   data.area_id = req.query.areaId;
        // }
      } else {
        data.area_id = res.locals.decoded.iot_area.id;
      }
      delete data.areaId;
      serviceTenantGet(data)
        .then((result) => res.json(result))
        .catch((error) => res.json(error));
    } catch (error) {
      console.log(error);
    }
  },
  controllerTenantEdit: (req, res) => {
    const data = req.body;
    if (data.image != undefined) {
      data.image = data.image.split("/");
      data.image = process.env.IMG_URL + data.image[data.image.length - 1];
    }
    serviceTenantEdit(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.json(error));
  },
};

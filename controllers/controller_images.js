const { serviceUploadImages } = require("../services/service_images");

module.exports = {
  ControllerUploadImages: (req, res) => {
    const data = req.body;
    const UrlHost = req.protocol + "://" + req.get("host");
    serviceUploadImages(data, UrlHost)
      .then(result => res.status(result.responseCode).json(result))
      .catch(error => res.status(error.responseCode).json(error));
  }
};

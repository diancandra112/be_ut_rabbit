const {
  serviceGatewayRegister,
  serviceRtuEditBilling,
  serviceBillingDenda,
} = require("../services/service_rtu_appname");

module.exports = {
  controllerGetAppName: (req, res) => {
    const data = req.query.devEui;
    if (req.query.devEui == undefined) {
      res.status(400).json({
        responseCode: 400,
        messages: "field devEui harus di deklarasikan",
      });
    } else {
      serviceGatewayRegister(data)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    }
  },
  controllerRtuEditBilling: (req, res) => {
    const data = req.body;
    let update_details = {
      node_id: data.id,
      field_billing: data.field_billing_rtu,
    };

    serviceRtuEditBilling(data, update_details)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  controllerBillingDenda: (req, res) => {
    let data = !req.query.used
      ? { ...req.query, ...req.params, used: false }
      : { ...req.query, ...req.params };
    serviceBillingDenda(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
};

const {
  serviceAreaRegister,
  serviceAreaGet,
  serviceAreaProfile,
  serviceAreaProfileErp,
} = require("../services");
const {
  serviceEditAreaProfile,
  serviceEditAlarmPressure,
  serviceAlarmPressure,
  serviceSaveErp,
  serviceGetErp,
  serviceTenantPassword,
  serviceTenantParameters,
  serviceGetTenantParameters,
} = require("../services/services_area");
const hash = (data) => {
  const crypto = require("crypto-js");
  return crypto.SHA256(data).toString();
};
module.exports = {
  controllerGetTenantParameters: (req, res) => {
    let tenant = req.params.tenantId;
    serviceGetTenantParameters(tenant)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.json(error));
  },
  controllerTenantParameters: (req, res) => {
    let tenant = req.params.tenantId;
    let data = { ...req.body };
    if (isNaN(data.minim_balance_gas) == false) {
      data.minim_balance_gas = parseFloat(data.minim_balance_gas) * 1000;
    }
    if (isNaN(data.minim_balance_water) == false) {
      data.minim_balance_water = parseFloat(data.minim_balance_water) * 1000;
    }
    serviceTenantParameters(data, tenant)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.json(error));
  },
  controllerTenantPassword: (req, res) => {
    let data = {
      id: req.params.tenantId,
      password: hash(req.params.password),
      password_flag: req.params.password,
      username: req.params.username,
    };
    serviceTenantPassword(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  controllerAreaRegister: (req, res) => {
    const dataUser = res.locals.dataUser;
    const dataProfile = res.locals.dataProfile;
    dataUser.password = hash(dataUser.password);
    serviceAreaRegister(dataUser, dataProfile)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  controllerAreaGet: (req, res) => {
    // const company_id = res.locals.decoded.id;
    serviceAreaGet()
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.json(error));
  },
  controllerAreaProfiles: (req, res) => {
    const data = res.locals.data;
    serviceAreaProfile(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.json(error));
  },
  controllerAreaProfilesErp: (req, res) => {
    let data = req.body;
    const { role } = res.locals.decoded;
    if (role === "AREA") {
      data.id = res.locals.decoded.iot_area.id;
    } else {
      if (req.query.area_id == null) throw new Error("request area_id");
      data.id = req.query.area_id;
    }
    serviceAreaProfileErp(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.json(error));
  },
  controllerEditAlarmPressure: (req, res) => {
    let data = { ...req.params, ...req.body };
    const { role } = res.locals.decoded;
    if (role === "AREA") {
      data.id = res.locals.decoded.iot_area.id;
    } else {
      if (req.query.area_id == null) throw new Error("request area_id");
      data.id = req.query.area_id;
    }
    serviceEditAlarmPressure(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.json(error));
  },
  controllerAlarmPressure: (req, res) => {
    let data = {};
    const { role } = res.locals.decoded;
    if (role === "AREA") {
      data = { id: res.locals.decoded.iot_area.id };
    } else {
      if (req.query.area_id == null) throw new Error("request area_id");
      data = { id: req.query.area_id };
    }
    serviceAlarmPressure(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.json(error));
  },
  controllerEditAreaProfiles: (req, res) => {
    let data = res.locals.data;
    let body = req.body;
    serviceEditAreaProfile(data, body)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.json(error));
  },
  controllerSaveErp: (req, res) => {
    let data = req.body;
    const { role } = res.locals.decoded;
    if (role === "AREA") {
      data.areaId = res.locals.decoded.iot_area.id;
    } else {
      if (req.query.area_id == null) throw new Error("request area_id");
      data.areaId = req.query.area_id;
    }
    serviceSaveErp(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.json(error));
  },
  controllerGetErp: (req, res) => {
    let data = req.body;
    const { role } = res.locals.decoded;
    if (role === "AREA") {
      data.areaId = res.locals.decoded.iot_area.id;
    } else {
      if (req.query.area_id == null) throw new Error("request area_id");
      data.areaId = req.query.area_id;
    }
    serviceGetErp(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.json(error));
  },
};

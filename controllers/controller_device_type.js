const {serviceGetDeviceType} = require('../services/services_device_type')
module.exports={
    controllerGetDeviceType:(req, res)=>{
        const data = req.query
        serviceGetDeviceType(data)
            .then((result)=>res.json(result))
            .catch((error)=>res.json(error))
    },
}
const { serviceFasapayCallback } = require("../services/service_callback");
const moment = require("moment");

module.exports = {
  controllerCallbackFasapay: (req, res) => {
    try {
      const data = req.body;
      if (parseInt(data.payment_status_code) == 2) {
        console.log(data);
        let history = {
          invoice: data.bill_no,
          status_code: data.payment_status_code,
          payload: JSON.stringify(data),
        };
        let update = {
          status: "PAID",
          invoice: data.bill_no,
          payment_method: "faspay-" + data.payment_channel,
          payment_date: moment().utcOffset("+0700").format(),
        };
        serviceFasapayCallback(update)
          .then((result) => res.json(result))
          .catch((error) => res.status(400).json(error));
      } else {
        console.log(data);
        res.status(200).json("sukses");
      }
    } catch (error) {
      console.log("error", error);
    }
  },
};

const {
  serviceGatewayEdit,
  serviceGatewayGet,
  serviceGatewayRegister,
  serviceCheckGateway,
  serviceGatewayDelete,
} = require("../services/service_gateway");
const hash = (data) => {
  const crypto = require("crypto-js");
  return crypto.SHA256(data).toString();
};
module.exports = {
  controllerGatewayDelete: (req, res) => {
    let data = req.params;
    serviceGatewayDelete(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  controllerGatewayRegister: (req, res) => {
    let data = req.body;
    data.area_id = res.locals.area_id;
    serviceGatewayRegister(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  controllerGatewayGet: (req, res) => {
    const data = req.query;
    const { role } = res.locals.decoded;
    if (role == "COMPANY") {
      if (req.query.areaId != undefined) {
        data.area_id = req.query.areaId;
      }
      delete data.areaId;
    } else if (role == "TEKNISI") {
      data.area_id = res.locals.decoded.area_id;
    } else if (role == "TEKNISI COMPANY") {
      if (req.query.areaId != undefined) {
        data.area_id = req.query.areaId;
      }
    } else {
      data.area_id = res.locals.decoded.iot_area.id;
    }
    serviceGatewayGet(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.json(error));
  },
  controllerGatewayEdit: (req, res) => {
    const data = res.locals.data;
    console.log(data);
    serviceGatewayEdit(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.json(error));
  },
  ControllerCheckGateway: (req, res) => {
    const data = req.body;
    serviceCheckGateway(data)
      .then((result) => res.json(result))
      .catch((error) => res.json(error));
  },
};

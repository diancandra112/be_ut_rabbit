const {
  SvcRegisterLine,
  SvcEditLine,
  SvcDeleteLine,
  SvcGetLine,
} = require("../services/service_line");

module.exports = {
  CtrlRegisterLine: (req, res) => {
    try {
      const data = req.body;
      SvcRegisterLine(data)
        .then((result) => {
          res.status(200).json(result);
        })
        .catch((error) => {
          res.status(400).json(error);
        });
    } catch (error) {
      res.status(500).json({ responseCode: 500, message: error.message });
    }
  },
  CtrlEditLine: (req, res) => {
    try {
      const data = req.body;
      const id = req.params;
      SvcEditLine(data, id)
        .then((result) => {
          res.status(200).json(result);
        })
        .catch((error) => {
          res.status(400).json(error);
        });
    } catch (error) {
      res.status(500).json({ responseCode: 500, message: error.message });
    }
  },
  CtrlDeleteLine: (req, res) => {
    try {
      const id = req.params;
      SvcDeleteLine(id)
        .then((result) => {
          res.status(200).json(result);
        })
        .catch((error) => {
          res.status(400).json(error);
        });
    } catch (error) {
      res.status(500).json({ responseCode: 500, message: error.message });
    }
  },
  CtrlGetLine: (req, res) => {
    try {
      const data = req.query;
      const { role } = res.locals.decoded;
      let areaId = 0;
      if (role === "AREA") {
        areaId = res.locals.decoded.iot_area.id;
      } else {
        if (req.query.area_id == null) throw new Error("request area_id");
        areaId = req.query.area_id;
        delete data.area_id;
      }
      SvcGetLine(data, { area_id: areaId })
        .then((result) => {
          res.status(200).json(result);
        })
        .catch((error) => {
          res.status(400).json(error);
        });
    } catch (error) {
      res.status(500).json({ responseCode: 500, message: error.message });
    }
  },
};

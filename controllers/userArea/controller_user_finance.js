const {
  serviceRegisterFinance,
  serviceViewFinance,
  serviceEditFinance,
  serviceDeleteFinance,
  serviceRegisterFinanceV2,
  serviceEditFinanceV2,
  servicePwdFinanceV2,
} = require("../../services/areaUser/services_user_finance");

const hash = (data) => {
  const crypto = require("crypto-js");
  return crypto.SHA256(data).toString();
};
const { env_website } = require("../../config/env");
module.exports = {
  controllerPwdFinance: (req, res) => {
    const data = { ...req.params, ...req.body };
    data.password = hash(data.password);
    servicePwdFinanceV2(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  controllerRegisterFinance: (req, res) => {
    const data = {
      ...req.body,
      area_id: res.locals.decoded.iot_area.id,
      role_id: 5,
    };
    data.password = hash(data.password);
    if (req.file) {
      data.images = process.env.IMG_URL + req.file.filename;
    }
    if (data.username.length < 4) {
      return res.status(403).json({
        responseCode: 403,
        response: "username minimum length 4",
      });
    }
    if (["poc", "iki"].includes(env_website.toLocaleLowerCase())) {
      serviceRegisterFinanceV2(data)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    } else {
      serviceRegisterFinance(data)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    }
  },
  controllerEditFinance: (req, res) => {
    const data = { ...req.body, area_id: res.locals.decoded.iot_area.id };
    if (req.file) {
      data.images = process.env.IMG_URL + req.file.filename;
    }
    delete data.password;
    if (["poc", "iki"].includes(env_website.toLocaleLowerCase())) {
      serviceEditFinanceV2(data)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.json(error));
    } else {
      serviceEditFinance(data)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.json(error));
    }
  },
  controllerDeleteFinance: (req, res) => {
    const data = req.params;
    serviceDeleteFinance(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.json(error));
  },
  controllerViewFinance: (req, res) => {
    const area_id = { area_id: res.locals.decoded.iot_area.id };
    const data = { ...req.query };
    serviceViewFinance(data, area_id)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.json(error));
  },
};

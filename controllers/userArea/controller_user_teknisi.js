const {
  serviceRegisterTeknisi,
  serviceRegisterTeknisiCompany,
  serviceViewTeknisi,
  serviceEditTeknisi,
  serviceDeleteTeknisi,
  serviceEditTeknisiCompany,
  serviceEditStatusTeknisiCompany,
  servicePwdTeknisi,
  serviceEditTeknisiV2,
  serviceRegisterTeknisiV2,
  servicePwdTeknisiCompany,
} = require("../../services/areaUser/services_user_teknisi");

const hash = (data) => {
  const crypto = require("crypto-js");
  return crypto.SHA256(data).toString();
};
const { env_website } = require("../../config/env");
module.exports = {
  controllerPwdTeknisiCompany: (req, res) => {
    const data = { ...req.params, ...req.body };
    data.password = hash(data.password);
    servicePwdTeknisiCompany(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  controllerEditStatusTeknisiCompany: (req, res) => {
    let data = { ...req.params };
    data.status =
      data.status.toLocaleLowerCase() == true ||
      data.status.toLocaleLowerCase() == "true";
    serviceEditStatusTeknisiCompany(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.json(error));
  },
  controllerEditTeknisiCompany: (req, res) => {
    const data = { ...req.body };
    data.nodeType.map((val, index) => {
      data.nodeType[index] = JSON.parse(data.nodeType[index]);
    });
    if (req.file) {
      data.images = process.env.IMG_URL + req.file.filename;
    }
    data.password = hash(data.password);
    serviceEditTeknisiCompany(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.json(error));
  },
  controllerRegisterTeknisiCompany: (req, res) => {
    const data = { ...req.body, area_id: 0, is_company: true };
    data.password = hash(data.password);
    data.nodeType.map((val, index) => {
      data.nodeType[index] = JSON.parse(data.nodeType[index]);
    });
    if (req.file) {
      data.images = process.env.IMG_URL + req.file.filename;
    }
    serviceRegisterTeknisiCompany(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  controllerRegisterTeknisi: (req, res) => {
    const data = {
      ...req.body,
      area_id: res.locals.decoded.iot_area.id,
      role_id: 6,
    };
    data.password = hash(data.password);
    data.nodeType.map((val, index) => {
      data.nodeType[index] = JSON.parse(data.nodeType[index]);
    });
    if (req.file) {
      data.images = process.env.IMG_URL + req.file.filename;
    }
    if (["poc", "iki"].includes(env_website.toLocaleLowerCase())) {
      serviceRegisterTeknisiV2(data)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    } else {
      serviceRegisterTeknisi(data)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    }
  },
  controllerEditTeknisi: (req, res) => {
    const data = { ...req.body, area_id: res.locals.decoded.iot_area.id };
    data.nodeType.map((val, index) => {
      data.nodeType[index] = JSON.parse(data.nodeType[index]);
    });
    if (req.file) {
      data.images = process.env.IMG_URL + req.file.filename;
    }
    delete data.password;

    if (["poc", "iki"].includes(env_website.toLocaleLowerCase())) {
      serviceEditTeknisiV2(data)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.json(error));
    } else {
      serviceEditTeknisi(data)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.json(error));
    }
  },
  controllerDeleteTeknisi: (req, res) => {
    const data = req.params;
    serviceDeleteTeknisi(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.json(error));
  },
  controllerViewTeknisi: (req, res) => {
    const area_id = { area_id: res.locals.decoded.iot_area.id };
    const data = { ...req.query };
    serviceViewTeknisi(data, area_id)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.json(error));
  },

  controllerPwdTeknisi: (req, res) => {
    const data = { ...req.params, ...req.body };
    data.password = hash(data.password);
    servicePwdTeknisi(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.json(error));
  },
};

const {
  serviceRegisterAdmin,
  serviceViewAdmin,
  serviceEditAdmin,
  serviceDeleteAdmin,
  serviceRegisterAdminV2,
  serviceEditAdminV2,
  servicePwdAminV2,
} = require("../../services/areaUser/services_user_admin");

require("dotenv").config();
const hash = (data) => {
  const crypto = require("crypto-js");
  return crypto.SHA256(data).toString();
};
const { env_website } = require("../../config/env");
module.exports = {
  controllerPwdAmin: (req, res) => {
    const data = { ...req.params, ...req.body };
    data.password = hash(data.password);
    servicePwdAminV2(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  controllerRegisterAdmin: (req, res) => {
    const data = { ...req.body, area_id: res.locals.decoded.iot_area.id };
    data.password = hash(data.password);
    if (req.file) {
      data.images = process.env.IMG_URL + req.file.filename;
    }
    data.role_id = 7;
    if (["poc", "iki"].includes(env_website.toLocaleLowerCase())) {
      serviceRegisterAdminV2(data)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    } else {
      serviceRegisterAdmin(data)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    }
  },
  controllerEditAdmin: (req, res) => {
    const data = { ...req.body, area_id: res.locals.decoded.iot_area.id };
    if (req.file) {
      data.images = process.env.IMG_URL + req.file.filename;
    }
    delete data.password;
    if (["poc", "iki"].includes(env_website.toLocaleLowerCase())) {
      serviceEditAdminV2(data)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    } else {
      serviceEditAdmin(data)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    }
  },
  controllerDeleteAdmin: (req, res) => {
    const data = req.params;
    serviceDeleteAdmin(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.json(error));
  },
  controllerViewAdmin: (req, res) => {
    const area_id = { area_id: res.locals.decoded.iot_area.id };
    const data = { ...req.query };
    serviceViewAdmin(data, area_id)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.json(error));
  },
};

const { controllerUserLogin } = require("./controller_users");
const {
  controllerAreaRegister,
  controllerAreaGet,
  controllerAreaProfiles,
  controllerAreaProfilesErp,
  controllerTenantPassword,
  controllerTenantParameters,
  controllerGetTenantParameters,
} = require("./controller_area");
const { controllerCompaniesRegister } = require("./controller_company");
const {
  controllerTenantRegister,
  controllerTenantGet,
  controllerTenantEdit,
} = require("./controller_tenant");

module.exports = {
  controllerTenantGet,
  controllerAreaProfiles,
  controllerAreaGet,
  controllerTenantRegister,
  controllerAreaRegister,
  controllerCompaniesRegister,
  controllerUserLogin,
  controllerTenantEdit,
  controllerAreaProfilesErp,
  controllerTenantPassword,
  controllerTenantParameters,
  controllerGetTenantParameters,
};

const moment = require("moment");
const {
  svcCreatePrepaid,
  svcListType,
  svcGetPrepaid,
  svcEditPrepaid,
  svcDeletePrepaid,
  svcTransactionPrepaidAdd,
  svcTransactionPrepaidGet,
  svcTransactionPrepaidGetArea,
  svcTransactionPrepaidPostWeiots,
} = require("../../services/pricingPrepaid/pricing_prepaid");

module.exports = {
  ctrlEditPrepaid: (req, res) => {
    let data = req.body;
    const { role } = res.locals.decoded;
    if (role === "AREA") {
      data.areaId = res.locals.decoded.iot_area.id;
    } else {
      if (req.query.area_id == null) throw new Error("request area_id");
      data.areaId = req.query.area_id;
    }
    svcEditPrepaid(data)
      .then((result) => res.status(200).json(result))
      .catch((error) => res.status(400).json(error));
  },
  ctrlDeletePrepaid: (req, res) => {
    const data = {
      ...req.params,
    };
    const { role } = res.locals.decoded;
    if (role === "AREA") {
      data.areaId = res.locals.decoded.iot_area.id;
    } else {
      if (req.query.area_id == null) throw new Error("request area_id");
      data.areaId = req.query.area_id;
    }
    svcDeletePrepaid(data)
      .then((result) => res.status(200).json(result))
      .catch((error) => res.status(400).json(error));
  },
  ctrlGetPrepaid: (req, res) => {
    const data = {
      ...req.params,
      ...req.query,
    };
    const { role } = res.locals.decoded;
    if (role === "AREA") {
      data.areaId = res.locals.decoded.iot_area.id;
    } else {
      if (req.query.area_id == null) throw new Error("request area_id");
      data.areaId = req.query.area_id;
      delete data.area_id;
    }
    svcGetPrepaid(data)
      .then((result) => res.status(200).json(result))
      .catch((error) => res.status(400).json(error));
  },
  ctrlCreatePrepaid: (req, res) => {
    let data = { ...req.body, status: true };
    const { role } = res.locals.decoded;
    if (role === "AREA") {
      data.areaId = res.locals.decoded.iot_area.id;
    } else {
      if (req.query.area_id == null) throw new Error("request area_id");
      data.areaId = req.query.area_id;
    }
    svcCreatePrepaid(data)
      .then((result) => res.status(200).json(result))
      .catch((error) => res.status(400).json(error));
  },
  ctrlListType: (req, res) => {
    const data = req.query;
    svcListType(data)
      .then((result) => res.status(200).json(result))
      .catch((error) => res.status(400).json(error));
  },
  ctrlTransactionPrepaidAdd: (req, res) => {
    let data = req.body;
    data.area_id = res.locals.decoded.area_id;
    data.order_date = moment().format();
    if (res.locals.decoded.role == "TENANT") {
      data.tenant_id = res.locals.decoded.id;
      data.tenant_name = res.locals.decoded.tenant_name;
      data.status = "UNPAID";
    }

    svcTransactionPrepaidGetArea(data)
      .then((res_area) => {
        if (res_area.data.auto_paid_prepaid) {
          console.log("auto");
          data.status = "PAID";
          svcTransactionPrepaidAdd(data)
            .then((result_post) => {
              svcTransactionPrepaidPostWeiots(result_post)
                .then((result) => res.status(200).json(result))
                .catch((error) => res.status(400).json(error));
            })
            .catch((error) => res.status(400).json(error));
        } else {
          console.log("manual");
          data.status = "UNPAID";
          svcTransactionPrepaidAdd(data)
            .then((result) => res.status(200).json(result))
            .catch((error) => res.status(400).json(error));
        }
      })
      .catch((error) => res.status(400).json(error));
  },
  ctrlTransactionPrepaidGet: (req, res) => {
    let data = req.query;
    data.area_id = res.locals.decoded.area_id;
    let page = data.page;
    let size = data.size;
    delete data.page;
    delete data.size;
    if (res.locals.decoded.role == "TENANT") {
      data.tenant_id = res.locals.decoded.id;
    }

    svcTransactionPrepaidGet(data, page, size)
      .then((result) => res.status(200).json(result))
      .catch((error) => res.status(400).json(error));
  },
};

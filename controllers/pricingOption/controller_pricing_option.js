const {
  svcListMember,
} = require("../../services/pricingOptions/pricing_pricing_options");

module.exports = {
  ctrlListMember: (req, res) => {
    const data = req.query;
    data.area_id = res.locals.decoded.iot_area.id;
    svcListMember(data)
      .then((result) => res.status(200).json(result))
      .catch((error) => res.status(400).json(error));
  },
};

const { serviceWaSend } = require("../services/service_wa");

module.exports = {
  controllerWaSend: (req, res) => {
    const data = req.body;
    serviceWaSend(data)
      .then((result) => res.json(result))
      .catch((error) => res.json(error));
  },
};

const { svcSendMail } = require("../../services/billing/serviceSendMail");

module.exports = {
  ctrlSendMail: (req, res) => {
    const data = req.body;
    let attachment = [];
    data.attachment.map((val) => {
      attachment.push({ name: val.name, base64: val.base64 });
    });
    svcSendMail(data, attachment)
      .then((result) => res.status(200).json(result))
      .catch((error) => res.status(400).json(error));
  },
};

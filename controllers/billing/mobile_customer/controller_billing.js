const { svcGetBilling } = require("../../../services/billing/serviceBilling");

const {
  SvcGetArea,
} = require("../../../services/tiketing/mobile_customer/service_tiket");
const moment = require("moment");
require("dotenv").config();
module.exports = {
  ctrlGetBilling: (req, res) => {
    try {
      const data = {
        ...req.query,
        areaId: res.locals.decoded.area_id,
        tenantId: res.locals.decoded.id,
      };
      svcGetBilling(data)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    } catch (error) {
      console.log(error);
      res.status(400).json({
        responseCode: 400,
        response: JSON.stringify(error),
      });
    }
  },
};

const {
  svcBillingCombine,
} = require("../../services/billing/serviceBillingCombine");
const moment = require("moment");
require("dotenv").config();
module.exports = {
  ctrlBillingCombine: (req, res) => {
    try {
      const data = { ...req.params, areaId: res.locals.decoded.iot_area.id };
      if (
        data.periode_billing == undefined ||
        data.periode_billing == null ||
        moment(data.periode_billing).isValid() == false
      ) {
        data.periode_billing = moment().utcOffset("+0700").format("YYYY-MM");
      }
      svcBillingCombine(data)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    } catch (error) {
      console.log(error);
      res.status(400).json({
        responseCode: 400,
        response: JSON.stringify(error),
      });
    }
  },
};

const moment = require("moment");
moment.locale("id");
require("moment/locale/id");
const { serviceWaSend } = require("../../services/service_wa");
const {
  svcSendEmailBilling,
  svcMidtrans,
  svcFasapay,
  svcUpdateBilling,
  svcCheckBilling,
  svcSendEmailTest,
  svcUpdateBilling2,
} = require("../../services/billing/emails");
const {
  SvcBillingToErp,
} = require("../../services/billing/serviceBillingToErp");

function delay(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

function getRandomInt(n) {
  return Math.floor(Math.random() * n);
}
function shuffle(s) {
  var arr = s.split(""); // Convert String to array
  var n = arr.length; // Length of the array

  for (var i = 0; i < n - 1; ++i) {
    var j = getRandomInt(n); // Get random of [0, n-1]

    var temp = arr[i]; // Swap arr[i] and arr[j]
    arr[i] = arr[j];
    arr[j] = temp;
  }

  s = arr.join(""); // Convert Array to string
  return s; // Return shuffled string
}

module.exports = {
  CtrlSendEmailFasapay: (req, res) => {
    try {
      const data = req.body;
      let dataMD = moment().utcOffset("+0700").format("MMDD");
      let dataHMS = moment().utcOffset("+0700").format("HHmmssSS");
      let combine = dataMD + shuffle(dataHMS);
      let id = ("0000" + data.billing_id).toString().slice(-5);
      // let permata = "337360" + dataMD + id; dev
      // let bca = "337361" + dataMD + id; dev

      //penambahan mandiri dan cimb 2021/08/12 start
      // Mandiri VA: 33736124
      // CIMB VA: 337369
      let mandiri = "88308013" + id + dataMD;
      let cimb = "422911" + id + dataMD;
      cimb = (cimb + combine).toString().slice(0, 16);
      mandiri = (mandiri + combine).toString().slice(0, 16);
      //penambahan mandiri dan cimb 2021/08/12 end

      let permata = "654321" + id + dataMD;
      let bca = "123456" + id + dataMD;
      permata = (permata + combine).toString().slice(0, 16);
      bca = (bca + combine).toString().slice(0, 16);

      //penambahan mandiri dan cimb 2021/08/12 start di bagian virtual_account
      const updateBilling = {
        id: data.billing_id,
        status: "UNPAID",
        virtual_account: `VA. PERMATA : ${permata} atau VA. BCA : ${bca} atau VA. CIMB : ${cimb} atau VA. MANDIRI : ${mandiri}`,
        due_date: data.date,
      };
      res.json({
        responseCode: 200,
        virtual_account: updateBilling.virtual_account,
      });

      //penambahan mandiri dan cimb 2021/08/12 end di bagian virtual_account
    } catch (error) {
      console.log(error);
      res.status(400).json(error);
    }
  },
  CtrlSendEmailMidtrans: (req, res) => {
    try {
      const data = req.body;
      const dataMidBca = {
        amount: parseInt(data.grand_total),
        invoice: data.no_faktur + "-BCA",
        email: data.email_to[0],
        first_name: data.nama_pelanggan,
        last_name: "",
        phone: data.no_kontak,
        bank: "bca",
        // bank: "permata",
      };
      const dataMidPermata = {
        amount: parseInt(data.grand_total),
        invoice: data.no_faktur + "-PERMATA",
        email: data.email_to[0],
        first_name: data.nama_pelanggan,
        last_name: "",
        phone: data.no_kontak,
        // bank: "bca",
        bank: "permata",
      };

      svcMidtrans(dataMidPermata)
        .then((resultPmt) => {
          let va = [];
          resultPmt = JSON.parse(JSON.stringify(resultPmt));
          console.log(resultPmt);
          if (resultPmt.is_error == undefined) {
            va.push("VA. PERMATA : " + resultPmt.permata_va_number);
          }
          delay(3000).then(() => {
            svcMidtrans(dataMidBca)
              .then((resultBca) => {
                resultBca = JSON.parse(JSON.stringify(resultBca));
                if (resultBca.is_error == undefined) {
                  va.push("VA. BCA : " + resultBca.va_numbers[0].va_number);
                }
                const updateBilling = {
                  id: data.billing_id,
                  status: "UNPAID",
                  virtual_account: va.join(" atau ").toString(),
                  due_date: data.date,
                };
                svcUpdateBilling2(updateBilling)
                  .then((result) => {
                    res.json({
                      responseCode: 200,
                      virtual_account: updateBilling.virtual_account,
                    });
                  })
                  .catch((error) => res.status(400).json(error));
              })
              .catch((error) => res.status(400).json(error));
          });
        })
        .catch((error) => res.status(400).json(error));
    } catch (error) {
      console.log(error);
      res.status(400).json(error);
    }
  },
  CtrlSendEmailTest: (req, res) => {
    svcSendEmailTest()
      .then((result) => res.json({ responseCode: 200, message: result }))
      .catch((error) => res.status(400).json(error));
  },
  CtrlSendEmailTestMul: (req, res) => {
    let filePdf = [];
    let filePdfDefault = [];

    if (req.files.attachment != undefined) {
      req.files.attachment.map((val, index) => {
        filePdf.push({
          filename: val.filename,
          mimetype: "./attachment/",
        });
      });
    }
    if (req.files.default != undefined) {
      req.files.default.map((val, index) => {
        filePdfDefault.push({
          filename: val.filename,
          mimetype: "./attachment/",
        });
      });
    }
    let adata = [...filePdf, ...filePdfDefault];
    console.log(adata);
    // svcSendEmailTest()
    //   .then((result) => res.json({ responseCode: 200, message: result }))
    //   .catch((error) => res.status(400).json(error));
  },

  svcBilling: (req, res) => {
    try {
      const { role, real_role, finance_name, finance_id } = res.locals.decoded;
      let data = req.body;
      let subject = new Date(data.cut_date);
      subject = "E-billing periode " + moment(subject).format("MMMM YYYY");
      delete data.cut_date;
      let filePdf = [];
      let attachment_log = [];
      let attachment_inv = [];
      let att_arr = [];
      let att_arr1 = "";
      let att_arr2 = "";
      if (req.files.attachment_inv != undefined) {
        req.files.attachment_inv.map((val, index) => {
          att_arr1 = process.env.PDF_URL + val.filename;
          attachment_inv.push({
            filename: val.filename,
            mimetype: "./attachment/",
          });
        });
      }
      if (req.files.attachment != undefined) {
        req.files.attachment.map((val, index) => {
          att_arr.push(process.env.PDF_URL + val.filename);
          filePdf.push({
            filename: val.filename,
            mimetype: "./attachment/",
          });
        });
      }
      if (req.files.attachment_log != undefined) {
        req.files.attachment_log.map((val, index) => {
          att_arr2 = process.env.PDF_URL + val.filename;
          attachment_log.push({
            filename: val.filename,
            mimetype: "./attachment/",
          });
        });
      }
      data.attachments = filePdf;
      data.attachmentsInv = attachment_inv;
      data.attachmentsLog = attachment_log;
      data.areaId = res.locals.decoded.iot_area.id;
      data.grand_total = parseInt(data.grand_total);
      data.vat = parseFloat(data.vat).toFixed(2);
      data.jumlah = parseFloat(data.jumlah).toFixed(2);

      // .then((result) => {
      const updateBilling = {
        id: data.billing_id,
        virtual_account: data.virtual_account,
        status: "UNPAID",
        due_date: data.date,
        finance_name: finance_name,
        finance_id: finance_id,
      };

      let due_date = moment(data.date).format("YYYY-MM-DD");
      let si_date = moment(data.tgl_faktur).format("YYYY-MM-DD");
      let dataToErp = {
        si_date: si_date,
        due_date: due_date,
        term_payment: moment(due_date).diff(moment(si_date), "day"),
        no_ref: data.no_faktur,
        unit_price: parseFloat(data.harga),
        qty: data.volume,
        total_price: parseInt(data.grand_total),
      };
      dataToErp.sub_total =
        parseFloat(dataToErp.qty) * parseFloat(dataToErp.unit_price);

      svcCheckBilling(data, dataToErp)
        .then((result) => {
          try {
            let materai = result.data.materai;
            //data company_name dan billing_address
            if (
              result.data.is_combine_billing ||
              result.data.billing_type == "node"
            ) {
              data.nama_pelanggan =
                result.data.iot_area.company_name != null &&
                result.data.iot_area.company_name.length > 0
                  ? result.data.iot_area.company_name
                  : data.nama_pelanggan;
              data.alamat =
                result.data.iot_area.billing_address != null &&
                result.data.iot_area.billing_address.length > 0
                  ? result.data.iot_area.billing_address
                  : data.alamat;
            } else {
              data.nama_pelanggan =
                result.data.iot_tenant.company_name != null &&
                result.data.iot_tenant.company_name.length > 0
                  ? result.data.iot_tenant.company_name
                  : data.nama_pelanggan;
              data.alamat =
                result.data.iot_tenant.billing_address != null &&
                result.data.iot_tenant.billing_address.length > 0
                  ? result.data.iot_tenant.billing_address
                  : data.alamat;
            }
            //data company_name dan billing_address
            data.image = result.data.iot_area.image;
            if (result.data.billing_type == "node") {
              data.unit_meter = "pcs";
              data.keterangan = `Monthly System Subscription(Periode ${moment(
                "2021-01"
              ).format("MMM YYYY")})`;
            }
            if (
              result.data.iot_area.is_erp == false ||
              parseFloat(data.volume) == 0
            ) {
              if (
                result.data.is_combine_billing ||
                result.data.billing_type == "node"
              ) {
                data.no_pelanggan = result.data.iot_area.id.toString();
              } else {
                data.no_pelanggan = result.data.iot_tenant.id.toString();
              }
              svcUpdateBilling(
                updateBilling,
                data.attachments,
                data.attachmentsInv,
                data.attachmentsLog
              )
                .then((result) => {
                  data.attachmentsAll = [
                    ...data.attachments,
                    ...data.attachmentsInv,
                    ...data.attachmentsLog,
                  ];
                  svcSendEmailBilling(data, updateBilling, subject, materai)
                    .then((result) => {
                      // serviceWaSend(
                      //   [
                      //     data.nama_pelanggan,
                      //     data.no_pelanggan,
                      //     data.nama_pelanggan,
                      //     data.no_faktur,
                      //     data.tgl_faktur,
                      //     data.keterangan,
                      //     data.date,
                      //   ],
                      //   data.wa_number.split(","),
                      //   att_arr,
                      //   data.billing_id,
                      //   att_arr1,
                      //   att_arr2
                      // );
                      res.json({ responseCode: 200, message: result });
                    })
                    .catch((error) => res.status(400).json(error));
                })
                .catch((error) => res.status(400).json(error));
            } else {
              if (
                result.data.is_combine_billing ||
                result.data.billing_type == "node"
              ) {
                data.no_pelanggan = result.data.iot_area.erp_contract_name
                  .split(" - ")[0]
                  .toString();
              } else {
                data.no_pelanggan = result.data.iot_tenant.erp_contract_name
                  .split(" - ")[0]
                  .toString();
              }
              let baseUrl = result.data.iot_area.url_erp;
              baseUrl = baseUrl.split("api")[0] + "api";
              let meter_start = 0;
              let meter_end = 0;
              if (result.data.node_type == "RTU") {
                meter_start = parseFloat(result.data.start_meter);
                meter_end = parseFloat(result.data.end_meter);
              } else {
                meter_start = parseFloat(result.data.start_meter) / 1000;
                meter_end = parseFloat(result.data.end_meter) / 1000;
              }
              let period_to = moment(result.data.end_date)
                .utcOffset("+0700")
                .format("YYYY-MM-DD");
              let period_from = moment(result.data.start_date)
                .utcOffset("+0700")
                .add(1, "day")
                .format("YYYY-MM-DD");
              let noted =
                `Billing From Weiots: Pemakaian ${result.data.node_type.toLocaleLowerCase()} tgl ` +
                moment(result.data.start_date).format("DD-MM-YYYY") +
                " sampai " +
                moment(result.data.end_date).format("DD-MM-YYYY");
              SvcBillingToErp(
                noted,
                materai,
                result.data.ppn,
                result.dataToErp,
                data.denda,
                result.data.biaya_transaksi,
                baseUrl,
                result.data.iot_area.email_erp,
                result.data.iot_area.pass_erp,
                meter_start,
                meter_end,
                period_to,
                period_from,
                result.data.iot_area.iots_erp_item,
                result.data.is_combine_billing == true ||
                  result.data.billing_type == "node"
                  ? result.data.iot_area.erp_contract_id
                  : result.data.iot_tenant.erp_contract_id,
                data.biaya_admin
              )
                .then((resultErp) => {
                  resultErp = JSON.parse(JSON.stringify(resultErp));
                  updateBilling.resultErp = resultErp.decode;
                  updateBilling.erp_payload = JSON.stringify(resultErp);
                  updateBilling.erp_id = resultErp.id;
                  svcUpdateBilling(
                    updateBilling,
                    data.attachments,
                    data.attachmentsInv,
                    data.attachmentsLog
                  )
                    .then((result) => {
                      data.attachmentsAll = [
                        ...data.attachments,
                        ...data.attachmentsInv,
                        ...data.attachmentsLog,
                      ];
                      svcSendEmailBilling(data, updateBilling, subject, materai)
                        .then((result) => {
                          // serviceWaSend(
                          //   [
                          //     data.nama_pelanggan,
                          //     data.no_pelanggan,
                          //     data.nama_pelanggan,
                          //     data.no_faktur,
                          //     data.tgl_faktur,
                          //     data.keterangan,
                          //     data.date,
                          //   ],
                          //   data.wa_number.split(","),
                          //   att_arr,
                          //   data.billing_id,
                          //   att_arr1,
                          //   att_arr2
                          // );
                          res.json({ responseCode: 200, message: result });
                        })
                        .catch((error) => {
                          console.log(error);
                          res.status(400).json(error);
                        });
                    })
                    .catch((error) => {
                      console.log(error);
                      res.status(400).json(error);
                    });
                })
                .catch((error) => {
                  console.log(error);
                  res.status(400).json(error);
                });
            }
          } catch (error) {
            console.log(error);
          }
        })
        .catch((error) => res.status(400).json(error));
    } catch (error) {
      console.log(error);
      res.status(400).json(error);
    }
  },
};

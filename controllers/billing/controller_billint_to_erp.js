const {
  SvcBillingToErp,
} = require("../../services/billing/serviceBillingToErp");

module.exports = {
  CtrlBillingToErp: (req, res) => {
    try {
      const data = req.body;
      SvcBillingToErp(data)
        .then((result) => res.status(200).json(result))
        .catch((error) => res.status(400).json(error));
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
  CtrlBillingToErpInput: (req, res) => {
    try {
      console.log(req.body);
    } catch (error) {
      console.log(error);
      res.status(400).json({ responseCode: 400, message: error.message });
    }
  },
};

const {
  svcCreateBillingAll,
} = require("../../services/billing/serviceBillingCreateV2");
const {
  svcCreateBillingAllRtu,
} = require("../../services/billing/serviceBillingCreateV2Rtu");
const {
  svcCreateBillingAllManual,
} = require("../../services/billing/serviceBillingCreateV2Manual");
const {
  svcCreateBillingCustom,
} = require("../../services/billing/serviceBillingCreateV2Custom");
const moment = require("moment");
module.exports = {
  ctrlCreateBillingAll: (req, res) => {
    let data = req.body;
    svcCreateBillingAll(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  ctrlCreateBillingAllManual: (req, res) => {
    let data = req.body;
    // let area = res.locals.decoded.iot_area;
    let area = {};
    svcCreateBillingAllManual(data, area)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  ctrlCreateBillingCustom: (req, res) => {
    let data = req.query;
    svcCreateBillingCustom(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
};

const {
  svcGetUsage,
  svcGetUsageV2,
} = require("../../services/billing/serviceUsage");
const {
  svcGetNodeInternal,
  svcGetHistoryNode,
  svcGetNodeTenant,
} = require("../../services/billing/serviceUsageDateInternal");

module.exports = {
  ctrlGetUsage: (req, res) => {
    try {
      const data = req.query;
      data.areaId = res.locals.decoded.iot_area.id;
      svcGetUsage(data)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    } catch (error) {
      console.log(error);
      res.status(400).json({
        responseCode: 400,
        response: JSON.stringify(error),
      });
    }
  },
  ctrlGetUsageV2: (req, res) => {
    try {
      const data = req.query;
      const { type } = req.params;
      data.areaId = res.locals.decoded.iot_area.id;
      delete data.status_user;
      svcGetUsageV2(data, type)
        .then((result) => {
          result.result.map((val, idx) => {
            if (val.node_type != "RTU") {
              result.result[idx].start_meter =
                parseFloat(val.start_meter) / 1000;
              result.result[idx].end_meter = parseFloat(val.end_meter) / 1000;
              result.result[idx].usage = parseFloat(val.billing_usage) / 1000;
              result.result[idx].billing_usage =
                parseFloat(val.billing_usage) / 1000;
            } else {
              let sat = val.iot_node.field_billing_rtu.match(/\(([^)]+)\)/)
                ? val.iot_node.field_billing_rtu.match(/\(([^)]+)\)/)[1]
                : null;
              result.result[idx].satuan = sat;
            }
          });
          res.status(200).json(result);
        })
        .catch((error) => res.status(400).json(error));
    } catch (error) {
      console.log(error);
      res.status(400).json({
        responseCode: 400,
        response: error.message,
      });
    }
  },
  ctrlGetUsageDate: (req, res) => {
    try {
      const data = req.query;
      data.areaId = res.locals.decoded.iot_area.id;
      svcGetNodeTenant({ id: data.tenant_id })
        .then((result) => {
          let data_promise = [];
          result.node.map((val) => {
            let typeId = result.result.iot_nodes.filter((valP) => {
              return valP.id == val;
            });
            let rtu_model = null;
            if (typeId[0].iot_detail_rtu != null) {
              rtu_model = typeId[0].iot_detail_rtu.model;
            }
            data_promise.push(
              svcGetHistoryNode(
                val,
                data.start_date,
                data.end_date,
                typeId[0].iot_node_type.satuan,
                typeId[0].devEui,
                typeId[0].iot_node_type.type_name,
                result.result.tenant_name,
                typeId[0].merk,
                typeId[0].field_billing_rtu,
                rtu_model
              )
            );
          });
          Promise.all(data_promise)
            .then((result_promise) => {
              let new_data = [];
              result_promise.map((val) => {
                new_data.push(...val);
              });
              function compare(a, b) {
                console.log(a, b);
                if (a.date < b.date) {
                  return -1;
                }
                if (a.date > b.last_nom) {
                  return 1;
                }
                return 0;
              }
              let output = [];
              new_data.map((val, idx) => {
                new_data[idx].start_date = val.start;
                new_data[idx].end_date = val.end;
              });
              const moment = require("moment");
              function getDateArray(first, end) {
                // eslint-disable-next-line
                let arr = new Array();
                let dt = new Date(first);
                while (dt <= end) {
                  arr.push(moment(new Date(dt)).format("YYYY-MM-DD"));
                  dt.setDate(dt.getDate() + 1);
                }
                return arr;
              }
              let date = getDateArray(
                moment(data.start_date),
                moment(data.end_date)
              );
              new_data = new_data.sort(compare);
              let out_date = [];
              date.map((val) => {
                let temp = new_data.find((valp) => {
                  return val == valp.date;
                });
                if (temp != undefined) {
                  out_date.push(temp);
                }
              });
              res.json({ responseCode: 200, result: out_date });
            })
            .catch((error) => res.status(400).json(error));
        })
        .catch((error) => res.status(error.responseCode).json(error));
    } catch (error) {
      console.log(error);
      res.status(400).json({
        responseCode: 400,
        response: error.message,
      });
    }
  },
};

const moment = require("moment");
const {
  svcCreateCutoff,
  svcCreateCutoffSaas,
} = require("../../services/billing/serviceCutoff");

module.exports = {
  ctrlCreateCutOff: (req, res) => {
    try {
      const data = req.body;
      const { role } = res.locals.decoded;
      let areaId = 0;
      if (role === "AREA") {
        areaId = res.locals.decoded.iot_area.id;
      } else {
        if (req.query.area_id == null) throw new Error("request area_id");
        areaId = req.query.area_id;
      }
      data.data.map((val, idx) => {
        data.data[idx].areaId = areaId;
        let time = val.time.split(":");
        time.map((val, idx) => {
          if (isNaN(parseInt(val))) time[idx] = "00";
        });
        if (time.length <= 1) {
          data.data[idx].time = "00:00:00";
        } else if (time.length == 2) {
          data.data[idx].time = time[0] + ":" + time[1] + ":00";
        } else if (time.length > 3) {
          data.data[idx].time = time[0] + ":" + time[1] + ":" + time[2];
        }
      });
      svcCreateCutoff(data.data, areaId)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    } catch (error) {
      console.log(error);
      res.status(400).json({
        responseCode: 400,
        response: JSON.stringify(error),
      });
    }
  },
  ctrlCreateCutOffSaas: (req, res) => {
    try {
      const data = req.body;
      const { role } = res.locals.decoded;
      let areaId = 0;
      if (role === "AREA") {
        areaId = res.locals.decoded.iot_area.id;
      } else {
        if (req.query.area_id == null) throw new Error("request area_id");
        areaId = req.query.area_id;
      }
      data.map((val, idx) => {
        data[idx].areaId = areaId;
        let time = "00:00:00".split(":");
        if (moment(val.time).isValid()) {
          time = moment(val.time)
            .utcOffset("+0700")
            .format("HH:mm:ss")
            .split(":");
        } else {
          time = val.time == null ? "00:00:00".split(":") : val.time.split(":");
        }
        time.map((val, idx) => {
          if (isNaN(parseInt(val))) time[idx] = "00";
        });
        if (time.length <= 1) {
          data[idx].time = "00:00:00";
        } else if (time.length >= 2) {
          data[idx].time = time[0] + ":" + time[1] + ":00";
        }
      });
      svcCreateCutoffSaas(data, areaId)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    } catch (error) {
      console.log(error);
      res.status(400).json({
        responseCode: 400,
        response: JSON.stringify(error),
      });
    }
  },
};

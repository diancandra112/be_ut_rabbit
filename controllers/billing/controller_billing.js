const {
  svcGetBilling,
  svcUpdateBilling,
  svcGetUsageBilling,
  svcBillingCorrection,
  svcGetBillingCorrection,
  svcGetCorrection,
  svcUpdateBillingCancel,
  svcBillingCorrection2,
  svcGetCountBilling,
  svcGetUsageBillingCount0,
  svcUpdateBillingClose,
  svcGetBillingClose,
  svcBillingFine,
  svcGetBillingLogArea,
  svcGetBillingAttachCombine,
} = require("../../services/billing/serviceBilling");
const moment = require("moment");
getDateArray = (first, end) => {
  // eslint-disable-next-line
  let arr = new Array();
  let dt = new Date(first);
  while (dt <= end) {
    arr.push(moment(new Date(dt)).format("DD/MM/YYYY"));
    dt.setDate(dt.getDate() + 1);
  }
  return arr;
};
require("dotenv").config();
module.exports = {
  ctrlGetBillingClose: (req, res) => {
    try {
      const data = { ...req.params };
      svcGetBillingClose(data)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    } catch (error) {
      console.log(error);
      res.status(400).json({
        responseCode: 400,
        response: JSON.stringify(error),
      });
    }
  },
  ctrlUpdateBillingClose: (req, res) => {
    try {
      const data = { status: "PAID", ...req.params, ...req.body, files: [] };
      if (req.files.attachment != undefined) {
        req.files.attachment.map((val, index) => {
          data.files.push({
            billing_id: req.params.id,
            filename: val.filename,
            mimetype: val.mimetype,
            url:
              process.env.URL_IMAGES +
              `billing/attachment/${val.filename}/${val.mimetype.replace(
                "/",
                "garing"
              )}?destination=${val.destination}`,
          });
        });
      }
      svcUpdateBillingClose(data)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    } catch (error) {
      console.log(error);
      res.status(400).json({
        responseCode: 400,
        response: JSON.stringify(error),
      });
    }
  },
  ctrlUpdateBilling: (req, res) => {
    try {
      const data = req.body;
      data.areaId = res.locals.decoded.iot_area.id;
      if (data.status == "CANCEL") {
        svcUpdateBillingCancel(data)
          .then((result) => res.status(result.responseCode).json(result))
          .catch((error) => res.status(error.responseCode).json(error));
      } else {
        svcUpdateBilling(data)
          .then((result) => res.status(result.responseCode).json(result))
          .catch((error) => res.status(error.responseCode).json(error));
      }
    } catch (error) {
      console.log(error);
      res.status(400).json({
        responseCode: 400,
        response: JSON.stringify(error),
      });
    }
  },
  ctrlGetBilling: (req, res) => {
    try {
      let data = req.query;
      data.areaId = res.locals.decoded.iot_area.id;
      let sort_key = data.sort_key;
      let sort_idx = data.sort_idx;
      let page = data.page;
      let page_size = data.page_size;
      let search_inv = data.search_inv;
      delete data.sort_key;
      delete data.sort_idx;
      delete data.page;
      delete data.page_size;
      delete data.search_inv;
      svcGetBilling(data, sort_key, sort_idx, page, page_size, search_inv)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    } catch (error) {
      console.log(error);
      res.status(400).json({
        responseCode: 400,
        response: JSON.stringify(error),
      });
    }
  },
  ctrlGetUsageBillingV2: (req, res) => {
    let data = req.query;
    data.startDate = data.startDate.split(" ").join("+");
    data.endDate = data.endDate.split(" ").join("+");
    data.areaId = req.params.areaId;
    let date_range = getDateArray(moment(data.startDate), moment(data.endDate));
    svcGetCountBilling(data, date_range)
      .then((result_count) => {
        if (result_count > 0) {
          svcGetUsageBilling(data, date_range)
            .then((result) => res.status(result.responseCode).json(result))
            .catch((error) => res.status(error.responseCode).json(error));
        } else {
          svcGetUsageBillingCount0(data, date_range)
            .then((result) => res.status(result.responseCode).json(result))
            .catch((error) => res.status(error.responseCode).json(error));
        }
      })
      .catch((error) => res.status(error.responseCode).json(error));
  },
  ctrlGetUsageBilling: (req, res) => {
    let data = req.query;
    data.startDate = data.startDate.split(" ").join("+");
    data.endDate = data.endDate.split(" ").join("+");
    data.areaId = res.locals.decoded.iot_area.id;
    let date_range = getDateArray(moment(data.startDate), moment(data.endDate));
    svcGetCountBilling(data, date_range)
      .then((result_count) => {
        if (result_count > 0) {
          svcGetUsageBilling(data, date_range)
            .then((result) => res.status(result.responseCode).json(result))
            .catch((error) => res.status(error.responseCode).json(error));
        } else {
          svcGetUsageBillingCount0(data, date_range)
            .then((result) => res.status(result.responseCode).json(result))
            .catch((error) => res.status(error.responseCode).json(error));
        }
      })
      .catch((error) => res.status(error.responseCode).json(error));
  },
  ctrlBillingCorrection: (req, res) => {
    let Rawdata = req.body;
    let data = Rawdata.data;
    let pesan = Rawdata.pesan;
    let decoded = res.locals.decoded;
    let denda = Rawdata.denda;
    pesan = {
      ...pesan,
      email: decoded.email,
      role: decoded.role,
      billing_id: data.id,
    };
    data.denda.map((val, index) => {
      data.denda[index].used = true;
    });
    svcBillingFine(data)
      .then((result) => {
        let update_denda = [];
        if (Array.isArray(JSON.parse(result.response1.denda))) {
          let flag_denda = JSON.parse(result.response1.denda);
          data.denda.map((val, index) => {
            let idx = flag_denda.findIndex((valP) => {
              return val.id == valP.id;
            });
            flag_denda.splice(idx, 1);
          });
          flag_denda.map((val, index) => {
            flag_denda[index].used = false;
          });
          update_denda = data.denda.concat(flag_denda);
        }
        // svcBillingCorrection(data, pesan, update_denda)
        //   .then((result) => {
        let note = "";
        result = JSON.parse(JSON.stringify(result));
        Object.keys(data).map((val, index) => {
          if (result.response1[val] != undefined) {
            if (result.response1[val] != data[val]) {
              note +=
                "Edit " +
                val.split("_").join(" ") +
                " From " +
                result.response1[val] +
                " To " +
                data[val] +
                "\r\n";
            }
          }
        });
        if (data.billing_usage != undefined && result.response1.typeId != 4) {
          data.billing_usage = data.billing_usage * 1000;
          data.minimum_charge = data.minimum_charge * 1000;
          data.minimum_charge_total = data.minimum_charge_total * 1000;
        }
        pesan.edit_details = note;
        svcBillingCorrection2(data, pesan, update_denda)
          .then((result) => res.status(result.responseCode).json(result))
          .catch((error) => res.status(error.responseCode).json(error));
        // })
        // .catch((error) => res.status(error.responseCode).json(error));
      })
      .catch((error) => res.status(error.responseCode).json(error));
  },
  ctrlGetCorrection: (req, res) => {
    let data = req.body;
    svcGetCorrection(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  ctrlcBillingCorrection: (req, res) => {
    let data = req.query;
    svcGetBillingCorrection(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  ctrlcBillingLogArea: (req, res) => {
    let data = { areaId: res.locals.decoded.iot_area.id, ...req.params };
    svcGetBillingLogArea(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  ctrlcBillingAttachCombine: (req, res) => {
    let data = { areaId: res.locals.decoded.iot_area.id, ...req.params };
    svcGetBillingAttachCombine(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
};

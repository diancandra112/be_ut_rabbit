const { svcGetAutoBill } = require("../../services/billing/serviceAutoBill");

module.exports = {
  ctrlGetAutoBill: (req, res) => {
    try {
      const data = req.body;
      svcGetAutoBill(data)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    } catch (error) {
      console.log(error);
      res.status(400).json({
        responseCode: 400,
        response: JSON.stringify(error),
      });
    }
  },
};

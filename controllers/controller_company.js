const {
  serviceCompaniesRegister,
  serviceCompanyProfile,
  serviceCompanyProfileEdit,
  serviceCompanyPasswordEdit,
  serviceAreaPasswordEdit,
} = require("../services/services_company");
const hash = (data) => {
  const crypto = require("crypto-js");
  return crypto.SHA256(data).toString();
};
module.exports = {
  controllerCompaniesRegister: (req, res) => {
    const dataUser = res.locals.dataUser;
    const dataProfile = res.locals.dataProfile;
    dataUser.password = hash(dataUser.password);
    serviceCompaniesRegister(dataUser, dataProfile)
      .then((result) => res.json(result))
      .catch((error) => res.json(error));
  },
  ControllerCompanyProfile: (req, res) => {
    const data = res.locals.data;
    serviceCompanyProfile(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  ControllerCompanyProfileEdit: (req, res) => {
    const data = req.body;
    serviceCompanyProfileEdit(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  ControllerCompanyPasswordEdit: (req, res) => {
    let { id, user_id } = res.locals.decoded.iot_company;
    let data_where = { id: id, user_id: user_id };
    let data = { password: hash(req.params.password) };
    serviceCompanyPasswordEdit(data, data_where)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  ControllerAreaPasswordEdit: (req, res) => {
    let { id, user_id } = res.locals.decoded.iot_area;
    let data_where = { id: id, user_id: user_id };
    let data = { password: hash(req.params.password) };
    serviceAreaPasswordEdit(data, data_where)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
};

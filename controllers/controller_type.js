const {
  serviceAddType,
  serviceEditType,
  serviceGetType,
  serviceGetTenantInternal,
  serviceGetTypePricing,
} = require("../services/service_type");
const hash = (data) => {
  const crypto = require("crypto-js");
  return crypto.SHA256(data).toString();
};
module.exports = {
  controllerAddType: (req, res) => {
    const data = req.body;
    serviceAddType(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  controllerEditType: (req, res) => {
    const dataUser = res.locals.dataUser;
    const dataProfile = res.locals.dataProfile;
    dataUser.password = hash(dataUser.password);
    serviceEditType(dataUser, dataProfile)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  controllerGetType: (req, res) => {
    const data = req.query;
    serviceGetType(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  controllerGetTypeTenantInternal: (req, res) => {
    const data = req.query;
    serviceGetTenantInternal(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  controllerGetTypePricing: (req, res) => {
    const data = req.body;
    const { role } = res.locals.decoded;
    let areaId = 0;
    if (role === "AREA") {
      areaId = res.locals.decoded.iot_area.id;
    } else {
      if (req.query.area_id == null) throw new Error("request area_id");
      areaId = req.query.area_id;
    }
    serviceGetTypePricing(areaId)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
};

const {
  serviceGetMember,
  serviceSetMember,
  serviceUpdateMember,
  serviceDeleteMember,
} = require("../services/service_member_level");

module.exports = {
  controllerGetMember: (req, res) => {
    let data = req.body;
    const { role } = res.locals.decoded;
    if (role === "AREA") {
      data.area_id = res.locals.decoded.iot_area.id;
    } else {
      if (req.query.area_id == null) throw new Error("request area_id");
      data.area_id = req.query.area_id;
    }
    serviceGetMember(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  controllerSetMember: (req, res) => {
    let data = req.body;
    const { role } = res.locals.decoded;
    if (role === "AREA") {
      data.area_id = res.locals.decoded.iot_area.id;
    } else {
      if (req.query.area_id == null) throw new Error("request area_id");
      data.area_id = req.query.area_id;
    }
    serviceSetMember(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  controllerUpdateMember: (req, res) => {
    let data = req.body;
    const { role } = res.locals.decoded;
    if (role === "AREA") {
      data.area_id = res.locals.decoded.iot_area.id;
    } else {
      if (req.query.area_id == null) throw new Error("request area_id");
      data.area_id = req.query.area_id;
    }
    serviceUpdateMember(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  controllerDeleteMember: (req, res) => {
    let data = req.body;
    const { role } = res.locals.decoded;
    if (role === "AREA") {
      data.area_id = res.locals.decoded.iot_area.id;
    } else {
      if (req.query.area_id == null) throw new Error("request area_id");
      data.area_id = req.query.area_id;
    }
    serviceDeleteMember(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
};

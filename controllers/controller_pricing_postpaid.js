const {
  servicePricingMemberLevel,
  serviceOnePriceForAll,
  serviceGetAreaProfilePricing,
  serviceGetOnePriceForAll,
  servicePricingBillingArea,
  serviceGetPricingBillingArea,
} = require("../services/service_pricing_postpaid");

module.exports = {
  controllerPricingMemberLevel: (req, res) => {
    const data = req.body;
    servicePricingMemberLevel(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  controllerGetAreaProfilePricing: (req, res) => {
    let data = req.body;
    const { role } = res.locals.decoded;
    if (role === "AREA") {
      data.area_id = res.locals.decoded.iot_area.id;
    } else {
      if (req.query.area_id == null) throw new Error("request area_id");
      data.area_id = req.query.area_id;
    }
    serviceGetAreaProfilePricing(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  controllerGetOnePriceForAll: (req, res) => {
    let data = req.body;
    const { role } = res.locals.decoded;
    if (role === "AREA") {
      data.area_id = res.locals.decoded.iot_area.id;
    } else {
      if (req.query.area_id == null) throw new Error("request area_id");
      data.area_id = req.query.area_id;
    }
    serviceGetOnePriceForAll(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  controllerOnePriceForAll: (req, res) => {
    let data = req.body;
    const { role } = res.locals.decoded;
    let area_id = 0;
    if (role === "AREA") {
      area_id = res.locals.decoded.iot_area.id;
    } else {
      if (req.query.area_id == null) throw new Error("request area_id");
      area_id = req.query.area_id;
    }
    data.map((val, idx) => {
      data[idx].areaId = area_id;
    });
    serviceOnePriceForAll(data, area_id)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  controllerPricingBillingArea: (req, res) => {
    let data = req.body;
    const { role } = res.locals.decoded;
    let area_id = 0;
    if (role === "AREA") {
      area_id = res.locals.decoded.iot_area.id;
    } else {
      if (req.query.area_id == null) throw new Error("request area_id");
      area_id = req.query.area_id;
    }
    data.map((val, idx) => {
      data[idx].areaId = area_id;
    });
    servicePricingBillingArea(data, area_id)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
  controllerGetPricingBillingArea: (req, res) => {
    let data = req.query;
    const { role } = res.locals.decoded;
    if (role === "AREA") {
      data.areaId = res.locals.decoded.iot_area.id;
    } else {
      if (req.query.area_id == null) throw new Error("request area_id");
      data.areaId = req.query.area_id;
      delete data.area_id;
    }
    serviceGetPricingBillingArea(data)
      .then((result) => res.status(result.responseCode).json(result))
      .catch((error) => res.status(error.responseCode).json(error));
  },
};

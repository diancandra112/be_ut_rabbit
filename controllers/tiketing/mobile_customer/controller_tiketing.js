const {
  SvcAddTiketing,
  SvcGetTiketing,
  SvcDeleterTiketing,
  SvcUpdateTiketing,
} = require("../../../services/tiketing/service_tiketing");
const { url_website } = require("../../../config/env");

const {
  SvcGetArea,
} = require("../../../services/tiketing/mobile_customer/service_tiket");
const moment = require("moment");
require("dotenv").config();
module.exports = {
  CtrlGetTiketing: (req, res) => {
    try {
      const data = {
        ...req.query,
        user_id: res.locals.decoded.id,
        area_id: res.locals.decoded.area_id,
        type_user: "tenant",
      };
      SvcGetTiketing(data)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    } catch (error) {
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
  CtrlAddTiketing: (req, res) => {
    try {
      let data = {
        ...req.body,
        area_id: res.locals.decoded.area_id,
        user_id: res.locals.decoded.id,
        user_name: res.locals.decoded.tenant_name,
        type_user: "tenant",
        ticket_type: "maintenance",
      };
      data.images = [];
      if (req.files) {
        req.files.map((val) => {
          data.images.push(process.env.IMG_URL + "tiketing/" + val.filename);
        });
      }
      SvcGetArea({ id: data.area_id })
        .then((result) => {
          result = JSON.parse(JSON.stringify(result));
          data = { ...data, no_tiket: result.response.username };
          let email = {
            area: result.response.area_name,
            nama_pelanggan: res.locals.decoded.tenant_name,
            no_tiket: 1,
            link_page: `${url_website}?t=maintenance&f=email&i=${data.area_id}`,
          };
          let flag = false;
          if (result.response.admin_pic_email != null) {
            let a = result.response.admin_pic_email.split(",");
            if (a.length > 0) {
              email.to = a;
              flag = true;
            }
          }
          SvcAddTiketing(data, email, flag)
            .then((result) => res.status(result.responseCode).json(result))
            .catch((error) => res.status(error.responseCode).json(error));
        })
        .catch((error) => res.status(error.responseCode).json(error));
    } catch (error) {
      console.log(error);
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
  CtrlEditTiketing: (req, res) => {
    try {
      const data = req.body;
      // SvcUpdateTiketing(data)
      //   .then((result) => res.status(result.responseCode).json(result))
      //   .catch((error) => res.status(error.responseCode).json(error));
    } catch (error) {
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
  CtrlDeleteTiketing: (req, res) => {
    try {
      const data = req.params;
      const tiket = { tiketing_id: data.id };
      // SvcDeleterTiketing(data, tiket)
      //   .then((result) => res.status(result.responseCode).json(result))
      //   .catch((error) => res.status(error.responseCode).json(error));
    } catch (error) {
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
};

const {
  SvcAddTiketing,
  SvcGetTiketing,
  SvcDeleterTiketing,
  SvcUpdateTiketing,
  SvcGetTiketingTeknisi,
  SvcGetTeknisiTiket,
  SvcReadTeknisiTiket,
  SvcAddHistoryTiket,
  SvcGetTiketingHistory,
  SvcGetTeknisiTiketCompany,
  SvcTiketingUnsigned,
} = require("../../services/tiketing/service_tiketing");
const moment = require("moment");
require("dotenv").config();
module.exports = {
  CtrlListTeknisi: (req, res) => {
    try {
      const data = { ...req.query, area_id: res.locals.decoded.iot_area.id };
      let area_id = data.area_id;
      delete data.area_id;
      let type = {};
      if (data.node_type == undefined || data.node_type.trim().length < 3) {
        delete data.node_type;
      } else {
        type.type_name = data.node_type;
        delete data.node_type;
      }
      SvcGetTiketingTeknisi(data, type, area_id)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    } catch (error) {
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
  CtrlGetTiketing: (req, res) => {
    try {
      const data = { ...req.query, area_id: res.locals.decoded.id };
      SvcGetTiketing(data)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    } catch (error) {
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
  CtrlGetTiketingHistory: (req, res) => {
    try {
      const data = { ...req.query, area_id: res.locals.decoded.id };
      SvcGetTiketingHistory(data)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    } catch (error) {
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
  CtrlAddTiketing: (req, res) => {
    try {
      const data = {
        ...req.body,
        area_id: res.locals.decoded.iot_area.id,
        no_tiket: res.locals.decoded.iot_area.area_name,
        ticket_delegation_date: moment().utcOffset("+0700").format(),
        status: "new",
        create_by: req.email,
        create_by_type: req.role,
        create_by_id: req.user_id,
      };
      data.images = [];
      req.files.map((val) => {
        data.images.push(process.env.IMG_URL + "tiketing/" + val.filename);
      });
      SvcAddTiketing(data)
        .then(async (result) => {
          const models = require("../../models/index");
          const { admin } = require("../../config/fcm_teknisi");
          let teknisi = await models.iots_teknisi.findOne({
            where: { id: data.technician_id },
          });
          teknisi = JSON.parse(JSON.stringify(teknisi));
          if (teknisi != null && teknisi.fcm_token != null) {
            const notification_options = {
              priority: "high",
              timeToLive: 60 * 60 * 24,
            };
            const registrationToken = teknisi.fcm_token;
            const message = {
              notification: {
                title: `Notifikasi Tiket ${data.ticket_type}`,
                body: `You Get New Tiket From ${data.type_user} ${data.user_name}`,
              },
            };
            const options = notification_options;
            await admin
              .messaging()
              .sendToDevice(registrationToken, message, options);
          }

          const { notif_tiket2 } = require("../../emailPayload/notif_tiket2");
          const { transporter } = require("../../config/email");
          let email_body = await notif_tiket2({
            area: res.locals.decoded.iot_area.area_name,
            nama_pelanggan: data.user_name,
            no_tiket: " ",
          });
          var mailOptions = {
            from: process.env.EMAIL_USER,
            to: teknisi.email,
            subject: "Notification Tiket",
            html: email_body,
            headers: {
              "x-priority": "1",
              "x-msmail-priority": "High",
              importance: "high",
            },
          };
          transporter.sendMail(mailOptions);
          let histo = {
            ticket_type: result.response.ticket_type,
            create_by:
              "User Area= " +
              res.locals.decoded.iot_area.area_name +
              " username= " +
              res.locals.decoded.iot_area.username,
            delegation_to: result.response.technician_name,
            ticket_delegation_date: moment(
              result.response.ticket_delegation_date
            ).format(),
            no_tiket: result.response.no_tiket,
            tiket_id: result.response.id,
            area_id: result.response.area_id,
            action: "create",
          };
          SvcAddHistoryTiket(histo)
            .then((his) => res.status(his.responseCode).json(his))
            .catch((error) => res.status(error.responseCode).json(error));
        })
        .catch((error) => res.status(error.responseCode).json(error));
    } catch (error) {
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
  CtrlEditTiketing: (req, res) => {
    try {
      const data = req.body;
      SvcUpdateTiketing(data, res.locals.decoded.iot_area)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    } catch (error) {
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
  CtrlDeleteTiketing: (req, res) => {
    try {
      const data = req.params;
      const tiket = { tiketing_id: data.id };
      SvcDeleterTiketing(data, tiket, res.locals.decoded.iot_area)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    } catch (error) {
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
  CtrlGetTeknisiTiket: (req, res) => {
    try {
      const data = { ...req.query };
      let page = isNaN(parseInt(data.page)) ? 1 : parseInt(data.page);
      let size = isNaN(parseInt(data.size)) ? 10 : parseInt(data.size);
      delete data.page;
      delete data.size;
      if (res.locals.decoded.role == "TEKNISI") {
        SvcGetTeknisiTiket(
          data,
          res.locals.decoded.id,
          res.locals.decoded.area_id,
          page,
          size
        )
          .then((result) => res.status(result.responseCode).json(result))
          .catch((error) => res.status(error.responseCode).json(error));
      } else {
        SvcGetTeknisiTiketCompany(data, res.locals.decoded.id, page, size)
          .then((result) => res.status(result.responseCode).json(result))
          .catch((error) => res.status(error.responseCode).json(error));
      }
    } catch (error) {
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
  CtrlReadTeknisiTiket: (req, res) => {
    try {
      const data = { ...req.query, user_id: res.locals.decoded.id };
      SvcReadTeknisiTiket(data)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    } catch (error) {
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
  CtrlTiketingUnsigned: (req, res) => {
    try {
      const data = {
        ...req.body,
        area_id: res.locals.decoded.id,
        ticket_type: "unassigned",
        status: "new",
      };
      SvcTiketingUnsigned(data)
        .then((result) => res.status(result.responseCode).json(result))
        .catch((error) => res.status(error.responseCode).json(error));
    } catch (error) {
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
};

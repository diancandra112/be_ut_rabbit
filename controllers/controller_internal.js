const {
  serviceInternalGet,
  serviceInternalRegister,
  serviceInternalEdit,
} = require("../services/services_internal");
const hash = (data) => {
  const crypto = require("crypto-js");
  return crypto.SHA256(data).toString();
};
module.exports = {
  controllerInternalRegister: (req, res) => {
    const nodeType = res.locals.nodeType;
    const dataProfile = res.locals.dataProfile;
    serviceInternalRegister(dataProfile, nodeType)
      .then((result) => res.json(result))
      .catch((error) => res.json(error));
  },
  controllerInternalGet: (req, res) => {
    const data = res.locals.data;
    serviceInternalGet(data)
      .then((result) => res.json(result))
      .catch((error) => res.json(error));
  },
  controllerInternalEdit: (req, res) => {
    const data = req.body;
    serviceInternalEdit(data)
      .then((result) => res.json(result))
      .catch((error) => res.json(error));
  },
};

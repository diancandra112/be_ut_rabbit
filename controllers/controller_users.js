const {
  serviceUserLogin,
  serviceUserLoginRole1,
  serviceUserLoginRole2,
  serviceUserLoginRole5,
  serviceUserLoginRole6,
  serviceUserLoginRole7,
  serviceUserLoginRole8,
} = require("../services/services_users");
const hash = (data) => {
  const crypto = require("crypto-js");
  return crypto.SHA256(data).toString();
};
module.exports = {
  controllerUserLogin: async (req, res) => {
    try {
      const data = req.body;
      data.password = hash(data.password);
      const models = require("../models/index");
      let user = await models.iot_users.findOne({
        where: {
          email: data.email,
          password: data.password,
        },
        attributes: {
          exclude: ["password", "createdAt", "updatedAt"],
        },
        include: [
          { model: models.iot_user_role },
          { model: models.iot_companies },
          { model: models.iot_tenant },
          { model: models.iot_area },
          { model: models.iots_finance },
        ],
      });
      if (user == null)
        return res.status(401).json({
          responseCode: 401,
          messages: "Wrong Username Or Password",
        });
      if (user.role_id == 1) {
        //company
        serviceUserLoginRole1(user)
          .then((result) => res.json(result))
          .catch((error) => res.json(error));
      } else if (user.role_id == 2) {
        //area
        serviceUserLoginRole2(user)
          .then((result) => res.json(result))
          .catch((error) => res.json(error));
      } else if (user.role_id == 5) {
        //finance
        serviceUserLoginRole5(user)
          .then((result) => res.json(result))
          .catch((error) => res.json(error));
      } else if (user.role_id == 6) {
        //TEKNISI
        serviceUserLoginRole6(user)
          .then((result) => res.json(result))
          .catch((error) => res.json(error));
      } else if (user.role_id == 7) {
        //finance
        serviceUserLoginRole7(user)
          .then((result) => res.json(result))
          .catch((error) => res.json(error));
      } else if (user.role_id == 8) {
        //TEKNISI COMPANY
        serviceUserLoginRole8(user)
          .then((result) => res.json(result))
          .catch((error) => res.json(error));
      } else {
        console.log(user);
        return res.status(401).json({
          responseCode: 401,
          messages: "Roles Are Not Allowed",
        });
      }
      // serviceUserLogin(data)
      //   .then((result) => res.json(result))
      //   .catch((error) => res.json(error));
    } catch (error) {
      console.log(error);
      res.status(500).json({
        responseCode: 500,
        messages: error.message,
      });
    }
  },
};

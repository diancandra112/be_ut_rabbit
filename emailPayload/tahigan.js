const moment = require("moment");
module.exports = {
  Billing: (data, data2, materai) => {
    return new Promise((resolve) => {
      resolve(`<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
      <html style="width:100%;font-family:arial, 'helvetica neue', helvetica, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;">
       <head> 
        <meta charset="UTF-8"> 
        <meta content="width=device-width, initial-scale=1" name="viewport"> 
        <meta name="x-apple-disable-message-reformatting"> 
        <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
        <meta content="telephone=no" name="format-detection"> 
        <title>New email 2</title> 
        <!--[if (mso 16)]>
          <style type="text/css">
          a {text-decoration: none;}
          </style>
          <![endif]--> 
        <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--> 
        <style type="text/css">
      @media only screen and (max-width:600px) {p, ul li, ol li, a { font-size:16px!important; line-height:150%!important } h1 { font-size:30px!important; text-align:center; line-height:120%!important } h2 { font-size:26px!important; text-align:center; line-height:120%!important } h3 { font-size:20px!important; text-align:center; line-height:120%!important } h1 a { font-size:30px!important } h2 a { font-size:26px!important } h3 a { font-size:20px!important } .es-menu td a { font-size:16px!important } .es-header-body p, .es-header-body ul li, .es-header-body ol li, .es-header-body a { font-size:16px!important } .es-footer-body p, .es-footer-body ul li, .es-footer-body ol li, .es-footer-body a { font-size:16px!important } .es-infoblock p, .es-infoblock ul li, .es-infoblock ol li, .es-infoblock a { font-size:12px!important } *[class="gmail-fix"] { display:none!important } .es-m-txt-c, .es-m-txt-c h1, .es-m-txt-c h2, .es-m-txt-c h3 { text-align:center!important } .es-m-txt-r, .es-m-txt-r h1, .es-m-txt-r h2, .es-m-txt-r h3 { text-align:right!important } .es-m-txt-l, .es-m-txt-l h1, .es-m-txt-l h2, .es-m-txt-l h3 { text-align:left!important } .es-m-txt-r img, .es-m-txt-c img, .es-m-txt-l img { display:inline!important } .es-button-border { display:block!important } a.es-button { font-size:20px!important; display:block!important; border-width:10px 0px 10px 0px!important } .es-btn-fw { border-width:10px 0px!important; text-align:center!important } .es-adaptive table, .es-btn-fw, .es-btn-fw-brdr, .es-left, .es-right { width:100%!important } .es-content table, .es-header table, .es-footer table, .es-content, .es-footer, .es-header { width:100%!important; max-width:600px!important } .es-adapt-td { display:block!important; width:100%!important } .adapt-img { width:100%!important; height:auto!important } .es-m-p0 { padding:0px!important } .es-m-p0r { padding-right:0px!important } .es-m-p0l { padding-left:0px!important } .es-m-p0t { padding-top:0px!important } .es-m-p0b { padding-bottom:0!important } .es-m-p20b { padding-bottom:20px!important } .es-mobile-hidden, .es-hidden { display:none!important } .es-desk-hidden { display:table-row!important; width:auto!important; overflow:visible!important; float:none!important; max-height:inherit!important; line-height:inherit!important } .es-desk-menu-hidden { display:table-cell!important } table.es-table-not-adapt, .esd-block-html table { width:auto!important } table.es-social { display:inline-block!important } table.es-social td { display:inline-block!important } }
      #outlook a {
          padding:0;
      }
      .ExternalClass {
          width:100%;
      }
      .ExternalClass,
      .ExternalClass p,
      .ExternalClass span,
      .ExternalClass font,
      .ExternalClass td,
      .ExternalClass div {
          line-height:100%;
      }
      .es-button {
          mso-style-priority:100!important;
          text-decoration:none!important;
      }
      a[x-apple-data-detectors] {
          color:inherit!important;
          text-decoration:none!important;
          font-size:inherit!important;
          font-family:inherit!important;
          font-weight:inherit!important;
          line-height:inherit!important;
      }
      .es-desk-hidden {
          display:none;
          float:left;
          overflow:hidden;
          width:0;
          max-height:0;
          line-height:0;
          mso-hide:all;
      }
      table td, table td * {
      vertical-align: top;
      }
      </style> 
       </head> 
       <body style="width:100%;font-family:arial, 'helvetica neue', helvetica, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;"> 
        <div class="es-wrapper-color" style="background-color:#F6F6F6;"> 
         <!--[if gte mso 9]>
                  <v:background xmlns:v="urn:schemas-microsoft-com:vml" fill="t">
                      <v:fill type="tile" color="#f6f6f6"></v:fill>
                  </v:background>
              <![endif]--> 
         <table class="es-wrapper" width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;padding:0;Margin:0;width:100%;height:100%;background-repeat:repeat;background-position:center top;"> 
           <tr style="border-collapse:collapse;"> 
            <td valign="top" style="padding:0;Margin:0;"> 
             <table class="es-content" cellspacing="0" cellpadding="0" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;"> 
               <tr style="border-collapse:collapse;"> 
                <td align="center" style="padding:0;Margin:0;"> 
                 <table class="es-content-body" width="600" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:#FFFFFF;"> 
                   <tr style="border-collapse:collapse;"> 
                    <td align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;"> 
                     <table width="100%" cellspacing="0" cellpadding="0" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                       <tr style="border-collapse:collapse;"> 
                        <td width="560" valign="top" align="center" style="padding:0;Margin:0;"> 
							<div style="margin: 0 auto; text-align: center;">
							<img src="${data.image}" style="height: 100px; position: relative;" />
						   </div>
                         <table width="100%" cellspacing="0" cellpadding="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                           <tr style="border-collapse:collapse;"> 
                            <td align="center" style="padding:20px;Margin:0;font-size:0;"> 
                             <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                               <tr style="border-collapse:collapse;"> 
                                <td style="padding:0;Margin:0px 0px 0px 0px;border-bottom:1px solid #CCCCCC;background:none;height:1px;width:100%;margin:0px;"></td> 
                               </tr> 
                             </table></td> 
                           </tr> 
                           <tr style="border-collapse:collapse;"> 
                            <td align="center" style="padding:0;Margin:0;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:20px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:30px;color:#333333;"><strong>Your Billing Notification</strong><br><br><span style="font-size:14px;">Dear <strong>${
                              data.nama_pelanggan
                            }, </strong><br>Below is your billing detail:</span></p></td> 
                           </tr> 
                           <tr style="border-collapse:collapse;"> 
                            <td align="center" style="padding:20px;Margin:0;font-size:0;"> 
                             <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                               <tr style="border-collapse:collapse;"> 
                                <td style="padding:0;Margin:0px 0px 0px 0px;border-bottom:1px solid #CCCCCC;background:none;height:1px;width:100%;margin:0px;"></td> 
                               </tr> 
                             </table></td> 
                           </tr> 
                         </table></td> 
                       </tr> 
                     </table></td> 
                   </tr> 
                   <tr style="border-collapse:collapse;"> 
                    <td align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;"> 
                     <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                       <tr style="border-collapse:collapse;"> 
                        <td width="560" align="center" valign="top" style="padding:0;Margin:0;"> 
                         <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                           <tr style="border-collapse:collapse;"> 
                            <td style="padding:0;Margin:0;"> 
                             <div style="float:left;width:100%;"> 
                              <table style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;width:96%;" cellspacing="0" cellpadding="5" role="presentation"> 
                                <tr style="border-collapse:collapse;"> 
                                 <td style="padding:0;Margin:0;width:35%;">No. Pelanggan<br></td> 
                                 <td style="padding:0;Margin:0;text-align:center;width:5%;">:<br></td> 
                                 <td style="padding:0;Margin:0;width:60%;">${
                                   data.no_pelanggan
                                 }<br></td> 
                                </tr> 
                                <tr style="border-collapse:collapse;"> 
                                 <td style="padding:0;Margin:0;">Nama Pelanggan<br></td> 
                                 <td style="padding:0;Margin:0;text-align:center;">:<br></td> 
                                 <td style="padding:0;Margin:0;">${
                                   data.nama_pelanggan
                                 }<br></td> 
                                </tr> 
                                <tr style="border-collapse:collapse;"> 
                                 <td style="padding:0;Margin:0;">No. Kontak<br></td> 
                                 <td style="padding:0;Margin:0;text-align:center;">:<br></td> 
                                 <td style="padding:0;Margin:0;">${
                                   data.no_kontak
                                 }<br></td> 
                                </tr> 
                                <tr style="border-collapse:collapse;"> 
                                 <td style="padding:0;Margin:0;">Alamat<br></td> 
                                 <td style="padding:0;Margin:0;text-align:center;">:<br></td> 
                                 <td style="padding:0;Margin:0;">${
                                   data.alamat
                                 }<br></td> 
                                </tr> 
                                <tr style="border-collapse:collapse;"> 
                                 <td style="padding:0;Margin:0;width:35%;">No. Faktur<br></td> 
                                 <td style="padding:0;Margin:0;text-align:center;width:5%;">:<br></td> 
                                 <td style="padding:0;Margin:0;width:60%;">${
                                   data.no_faktur
                                 }<br></td> 
                                </tr> 
                                <tr style="border-collapse:collapse;"> 
                                 <td style="padding:0;Margin:0;">Tgl Faktur<br></td> 
                                 <td style="padding:0;Margin:0;text-align:center;">:<br></td> 
                                 <td style="padding:0;Margin:0;">${
                                   data.tgl_faktur
                                 }<br></td> 
                                </tr> 
                                <tr style="border-collapse:collapse;"> 
                                 <td style="padding:0;Margin:0;">VA Number<br></td> 
                                 <td style="padding:0;Margin:0;text-align:center;">:<br></td> 
                                 <td style="padding:0;Margin:0;">${
                                   data2.virtual_account
                                 }</td> 
                                </tr> 
                                <tr style="border-collapse:collapse;"> 
                                 <td style="padding:0;Margin:0;">Keterangan<br></td> 
                                 <td style="padding:0;Margin:0;text-align:center;">:<br></td> 
                                 <td style="padding:0;Margin:0;">${
                                   data.keterangan
                                 }<br></td> 
                                </tr> 
                                <tr style="border-collapse:collapse;"> 
                                 <td style="padding:0;Margin:0;"><b>Catatan</b><br></td> 
                                 <td style="padding:0;Margin:0;text-align:center;"><b>:</b><br></td> 
                                 <td style="padding:0;Margin:0;"><b>${
                                   data.catatan_email ? data.catatan_email : ""
                                 }</b><br></td> 
                                </tr> 
                                <tr style="border-collapse:collapse;"> 
                                 <td style="padding:0;Margin:0;">Unit Meter<br></td> 
                                 <td style="padding:0;Margin:0;text-align:center;">:<br></td> 
                                 <td style="padding:0;Margin:0;text-align:right;">${
                                   data.unit_meter
                                 }<br></td> 
                                </tr> 
                                <tr style="border-collapse:collapse;"> 
                                 <td style="padding:0;Margin:0;">Volume<br></td> 
                                 <td style="padding:0;Margin:0;text-align:center;">:<br></td> 
                                 <td style="padding:0;Margin:0;text-align:right;">${
                                   data.volume
                                 }<br></td> 
                                </tr> 
                                <tr style="border-collapse:collapse;"> 
                                 <td style="padding:0;Margin:0;">Harga (/unit)<br></td> 
                                 <td style="padding:0;Margin:0;text-align:center;">:<br></td> 
                                 <td style="padding:0;Margin:0;text-align:right;">${new Intl.NumberFormat(
                                   "id-ID",
                                   {
                                     style: "currency",
                                     currency: "IDR",
                                   }
                                 )
                                   .format(parseInt(data.harga))
                                   .replace(/[.,]/g, function (x) {
                                     return x == "," ? "." : ",";
                                   })}<br></td> 
                                </tr> 
                                <tr style="border-collapse:collapse;"> 
                                 <td style="padding:0;Margin:0;">Jumlah (IDR)<br></td> 
                                 <td style="padding:0;Margin:0;text-align:center;">:<br></td> 
                                 <td style="padding:0;Margin:0;text-align:right;">${new Intl.NumberFormat(
                                   "id-ID",
                                   {
                                     style: "currency",
                                     currency: "IDR",
                                   }
                                 )
                                   .format(parseInt(data.jumlah))
                                   .replace(/[.,]/g, function (x) {
                                     return x == "," ? "." : ",";
                                   })}<br></td> 
                                </tr> 
                                <tr style="border-collapse:collapse;"> 
                                 <td style="padding:0;Margin:0;">Discount (0%)<br></td> 
                                 <td style="padding:0;Margin:0;text-align:center;">:<br></td> 
                                 <td style="padding:0;Margin:0;text-align:right;">${new Intl.NumberFormat(
                                   "id-ID",
                                   {
                                     style: "currency",
                                     currency: "IDR",
                                   }
                                 )
                                   .format(parseInt(data.discount))
                                   .replace(/[.,]/g, function (x) {
                                     return x == "," ? "." : ",";
                                   })}</br></td> 
                                </tr> 
                                <tr style="border-collapse:collapse;"> 
                                 <td style="padding:0;Margin:0;">VAT (excluded)<br></td> 
                                 <td style="padding:0;Margin:0;text-align:center;">:<br></td> 
                                 <td style="padding:0;Margin:0;text-align:right;">${new Intl.NumberFormat(
                                   "id-ID",
                                   {
                                     style: "currency",
                                     currency: "IDR",
                                   }
                                 )
                                   .format(parseInt(data.vat))
                                   .replace(/[.,]/g, function (x) {
                                     return x == "," ? "." : ",";
                                   })}<br></td> 
                                </tr> 
                                <tr style="border-collapse:collapse;"> 
                                 <td style="padding:0;Margin:0;">Admin<br></td> 
                                 <td style="padding:0;Margin:0;text-align:center;">:<br></td> 
                                 <td style="padding:0;Margin:0;text-align:right;">${new Intl.NumberFormat(
                                   "id-ID",
                                   {
                                     style: "currency",
                                     currency: "IDR",
                                   }
                                 )
                                   .format(parseInt(data.biaya_admin))
                                   .replace(/[.,]/g, function (x) {
                                     return x == "," ? "." : ",";
                                   })}<br></td> 
                                </tr> 
                                <tr style="border-collapse:collapse;"> 
                                 <td style="padding:0;Margin:0;">Materai<br></td> 
                                 <td style="padding:0;Margin:0;text-align:center;">:<br></td> 
                                 <td style="padding:0;Margin:0;text-align:right;">${new Intl.NumberFormat(
                                   "id-ID",
                                   {
                                     style: "currency",
                                     currency: "IDR",
                                   }
                                 )
                                   .format(parseInt(materai))
                                   .replace(/[.,]/g, function (x) {
                                     return x == "," ? "." : ",";
                                   })}<br></td> 
                                </tr> 
                                <tr style="border-collapse:collapse;"> 
                                 <td style="padding:0;Margin:0;"><b>Grand Total</b><br></td> 
                                 <td style="padding:0;Margin:0;text-align:center;">:<br></td> 
                                 <td style="padding:0;Margin:0;text-align:right;"><b>${new Intl.NumberFormat(
                                   "id-ID",
                                   {
                                     style: "currency",
                                     currency: "IDR",
                                   }
                                 )
                                   .format(parseInt(data.grand_total))
                                   .replace(/[.,]/g, function (x) {
                                     return x == "," ? "." : ",";
                                   })}</b><br></td> 
                                </tr> 
                              </table> 
                              <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;">&nbsp;<br></p> 
                             </div></td> 
                           </tr> 
                           <tr style="border-collapse:collapse;"> 
                            <td align="center" style="padding:20px;Margin:0;font-size:0;"> 
                             <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                               <tr style="border-collapse:collapse;"> 
                                <td style="padding:0;Margin:0px 0px 0px 0px;border-bottom:1px solid #CCCCCC;background:none;height:1px;width:100%;margin:0px;"></td> 
                               </tr> 
                             </table></td> 
                           </tr> 
                         </table></td> 
                       </tr> 
                     </table></td> 
                   </tr> 
                   <tr style="border-collapse:collapse;"> 
                    <td align="left" style="padding:0;Margin:0;padding-top:20px;padding-left:20px;padding-right:20px;"> 
                     <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                       <tr style="border-collapse:collapse;"> 
                        <td width="560" align="center" valign="top" style="padding:0;Margin:0;"> 
                         <table cellpadding="0" cellspacing="0" width="100%" role="presentation" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;"> 
                           <tr style="border-collapse:collapse;"> 
                            <td style="padding:0;Margin:0;"> 
                             <div style="float:left;width:100%;text-align:center;"> 
                              <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;">The due date of payment is on ${moment(
                                data.date
                              ).format(
                                "DD/MM/YYYY"
                              )}<br> Please contact we@wiraenergi.co.id or WhatsApp to +6281120200088 for more info.<br> You can find your invoice and usage detail attached in this e-mail.</p> 
                              <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;">&nbsp;<br></p> 
                             </div></td> 
                           </tr> 
                           <tr style="border-collapse:collapse;"> 
                            <td style="padding:0;Margin:0;"> 
                             <div style="float:left;width:100%;text-align:center;"> 
                              <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;">&nbsp;<br></p> 
                              <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;"><i>This is a systems generated message. Please do not reply to this email.</i><br></p> 
                              <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:21px;color:#333333;">&nbsp;<br></p> 
                             </div></td> 
                           </tr> 
                         </table></td> 
                       </tr> 
                     </table></td> 
                   </tr> 
                 </table></td> 
               </tr> 
             </table></td> 
           </tr> 
         </table> 
        </div>  
       </body>
      </html>`);
    });
  },
};

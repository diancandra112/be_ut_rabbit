const express = require("express");
const router = express.Router();
var multer = require("multer");

const { AuthArea, AuthCompany } = require("../middlewares/jwt_auth");

const {
  controllerDeleteTeknisi,
  controllerEditTeknisi,
  controllerRegisterTeknisi,
  controllerRegisterTeknisiCompany,
  controllerViewTeknisi,
  controllerEditTeknisiCompany,
  controllerEditStatusTeknisiCompany,
  controllerPwdTeknisi,
} = require("../controllers/userArea/controller_user_teknisi");

const {
  controllerDeleteFinance,
  controllerEditFinance,
  controllerRegisterFinance,
  controllerViewFinance,
  controllerPwdFinance,
} = require("../controllers/userArea/controller_user_finance");

const {
  controllerDeleteAdmin,
  controllerEditAdmin,
  controllerRegisterAdmin,
  controllerViewAdmin,
  controllerPwdAmin,
} = require("../controllers/userArea/controller_user_admin");

var storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, "./images");
  },
  filename: function (req, file, callback) {
    callback(null, Date.now() + "-" + file.originalname);
  },
});
const {
  controllerTeknisiCompanyMobileLoginGet,
} = require("../controllers/mobile/Auth");

var upload = multer({ storage: storage });

router
  .get("/teknisi", AuthArea, controllerViewTeknisi)
  .patch("/teknisi", AuthArea, upload.single("image"), controllerEditTeknisi)
  .post("/teknisi", AuthArea, upload.single("image"), controllerRegisterTeknisi)
  .post(
    "/teknisi/company",
    AuthCompany,
    upload.single("image"),
    controllerRegisterTeknisiCompany
  )
  .patch(
    "/teknisi/company",
    AuthCompany,
    upload.single("image"),
    controllerEditTeknisiCompany
  )
  .put("/teknisi/company/:id", AuthCompany, controllerEditTeknisiCompany)
  .patch("/teknisi/company/:status/:id", controllerEditStatusTeknisiCompany)
  .get("/teknisi/company", controllerTeknisiCompanyMobileLoginGet)
  .delete("/teknisi/:id", AuthArea, controllerDeleteTeknisi)
  .put("/teknisi/:id", AuthArea, controllerPwdTeknisi)

  .get("/finance", AuthArea, controllerViewFinance)
  .patch("/finance", AuthArea, upload.single("image"), controllerEditFinance)
  .post("/finance", AuthArea, upload.single("image"), controllerRegisterFinance)
  .put("/finance/:id", AuthArea, controllerPwdFinance)
  .delete("/finance/:id", AuthArea, controllerDeleteFinance)

  .get("/admin", AuthArea, controllerViewAdmin)
  .patch("/admin", AuthArea, upload.single("image"), controllerEditAdmin)
  .post("/admin", AuthArea, upload.single("image"), controllerRegisterAdmin)
  .put("/admin/:id", AuthArea, controllerPwdAmin)
  .delete("/admin/:id", AuthArea, controllerDeleteAdmin);

module.exports = router;

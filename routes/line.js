const express = require("express");
const router = express.Router();

const {
  CtrlRegisterLine,
  CtrlEditLine,
  CtrlDeleteLine,
  CtrlGetLine,
} = require("../controllers/controller_line");
const { AuthArea, AuthCompanyArea } = require("../middlewares/jwt_auth");
/* GET users listing. */
router
  .post("/register", AuthCompanyArea, CtrlRegisterLine)
  .get("/register", AuthCompanyArea, CtrlGetLine)
  .delete("/register/:id", AuthCompanyArea, CtrlDeleteLine)
  .patch("/register/:id", AuthCompanyArea, CtrlEditLine);

module.exports = router;

const express = require("express");
const router = express.Router();

const models = require("../models/index");
router.patch("/offset/:id/:sensor_level_offset", async (req, res) => {
  try {
    const data = req.params;
    let id = parseFloat(data.id);
    let sensor_level_offset = parseFloat(data.sensor_level_offset);
    await models.iot_nodes.update(
      { sensor_level_offset: sensor_level_offset },
      { where: { id: id } }
    );
    return res.status(200).json({
      status: 200,
      message: "sukses Edit Offset",
      data: sensor_level_offset,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({ status: 500, message: error.message });
  }
});
module.exports = router;

const express = require("express");
const router = express.Router();
const multer = require("multer");
var storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, "./images");
  },
  filename: function (req, file, callback) {
    callback(null, Date.now() + "-" + file.originalname);
  },
});

var upload = multer({ storage: storage });
const {
  CtrlBillingToErp,
  CtrlBillingToErpInput,
} = require("../controllers/billing/controller_billint_to_erp");
const {
  CtrlGagasDaily,
  CtrlGagasHourly,
  CtrlBillingCustom,
  CtrlMerk,
} = require("../controllers/gagas/ctrl_gagas");
const { CtrlAlarm } = require("../controllers/email_alarm/email");
const { SvcGetArea } = require("../services/cronjob_billing/service_get_area");
const {
  SvcGetAreaRtu,
} = require("../services/cronjob_billing/service_get_area_rtu");
router
  .get("/area", (req, res) => {
    SvcGetArea()
      .then((result) => res.json(result))
      .catch((error) => res.json(error));
  })
  .get("/area/rtu", (req, res) => {
    SvcGetAreaRtu()
      .then((result) => res.json(result))
      .catch((error) => res.json(error));
  })
  .get("/update/merk", CtrlMerk)
  .post("/billing/custom", CtrlBillingCustom)
  .post("/gagas/hour", CtrlGagasHourly)
  .post("/gagas/daily", CtrlGagasDaily)
  .post("/alarm/email", CtrlAlarm)
  .post("/erp", CtrlBillingToErp)
  .post("/v1/sales_invoices", upload.array("image"), CtrlBillingToErpInput);

module.exports = router;

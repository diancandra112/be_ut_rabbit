const express = require("express");
const router = express.Router();
/* GET users listing. */
router
  .get("/me", async (req, res) => {
    res.status(200).json({ status: 200, message: "health check" });
  })
  .get("/db", async (req, res) => {
    try {
      const models = require("../models/index");
      let node = await models.iot_device_type.count();
      res.status(200).json({ status: 200, message: node });
    } catch (error) {
      console.log(error);
      res.status(500).json({ status: 500, message: error.message });
    }
  });
module.exports = router;

const express = require("express");
const router = express.Router();
var multer = require("multer");

const {
  AuthArea,
  AuthTeknisiAndCom,
  AuthGetDetail,
} = require("../middlewares/jwt_auth");

const {
  CtrlAddTiketing,
  CtrlDeleteTiketing,
  CtrlEditTiketing,
  CtrlGetTiketing,
  CtrlListTeknisi,
  CtrlGetTeknisiTiket,
  CtrlReadTeknisiTiket,
  CtrlGetTiketingHistory,
  CtrlTiketingUnsigned,
} = require("../controllers/tiketing/controller_tiketing");

var storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, "./images/tiketing");
  },
  filename: function (req, file, callback) {
    callback(null, Date.now() + "-" + file.originalname);
  },
});

var upload = multer({ storage: storage });

router
  .get("/teknisi", AuthArea, CtrlListTeknisi)
  .get("/view", AuthArea, CtrlGetTiketing)
  .get("/history", AuthArea, CtrlGetTiketingHistory)
  .patch("/edit", AuthArea, CtrlEditTiketing)
  .post(
    "/create",
    upload.array("image"),
    AuthGetDetail,
    AuthArea,
    CtrlAddTiketing
  )
  .delete("/delete/:id", AuthArea, CtrlDeleteTiketing)
  .patch("/teknisi/notif/:id", AuthTeknisiAndCom, CtrlReadTeknisiTiket)
  .get("/teknisi/notif", AuthTeknisiAndCom, CtrlGetTeknisiTiket)

  .post("/unassigned/create", AuthArea, CtrlTiketingUnsigned);

module.exports = router;

var express = require("express");
var { fields_gas_water, data_gas_water } = require("./gas_water");
var { data_ct_nonct, fields_ct_nonct } = require("./ct_nonct");
var { Parser } = require("json2csv");
var router = express.Router();
const models = require("../../models/index");
const moment = require("moment");
const downloadResource = (res, fileName, fields, data) => {
  const json2csv = new Parser({ fields });
  const csv = json2csv.parse(data);
  res.header("Content-Type", "text/csv");
  res.attachment(fileName);
  return res.send(csv);
};
/* GET home page. */
router
  .get("/daily", async function (req, res, next) {
    try {
      const data = req.query;
      if (
        data.node_id == null ||
        data.start_date == null ||
        data.end_date == null
      ) {
        return res.status(400).json({
          responseCode: 400,
          message: "node_id,start_date,end_date is requered",
        });
      } else {
        let node = await models.iot_nodes.findOne({
          where: { id: data.node_id },
          attributes: ["id", "devEui", "typeId"],
        });
        node = JSON.parse(JSON.stringify(node));
        if (node == null || [1, 2, 3, 7].includes(node.typeId) == false) {
          return res.status(400).json({
            responseCode: 400,
            message: "node type not allowed",
          });
        } else {
          let log = await models.sequelize.query(`SELECT
          * 
        FROM
          iot_histories 
        WHERE
          DATE( reportTimeFlag )>= '${data.start_date}' 
          AND DATE( reportTimeFlag )<= '${data.end_date}' 
          AND device_id = ${data.node_id} 
          AND reportTimeFlag IN (
          SELECT
            MAX( reportTimeFlag ) 
          FROM
            iot_histories 
          WHERE
            DATE( reportTimeFlag )>= '${data.start_date}' 
            AND DATE( reportTimeFlag )<= '${data.end_date}' 
            AND device_id = ${data.node_id} 
          GROUP BY
          DATE( reportTimeFlag ) 
          ) ORDER BY reportTime DESC`);
          log = JSON.parse(JSON.stringify(log));
          log = log[0];
          let output = [];
          log.map((val, idx) => {
            if (log.length - 1 == idx) {
              output.push({
                update_time: moment(log[idx].reportTimeFlag).format(
                  "YYYY-MM-DD HH:mm:ss+0700"
                ),
                totalizer:
                  parseFloat(JSON.parse(val.payload).meter.meterReading) / 1000,
                usage: "0.000",
              });
            } else {
              output.push({
                update_time: moment(log[idx].reportTimeFlag).format(
                  "YYYY-MM-DD HH:mm:ss+0700"
                ),
                totalizer:
                  parseFloat(JSON.parse(val.payload).meter.meterReading) / 1000,
                usage: (
                  (parseFloat(JSON.parse(val.payload).meter.meterReading) -
                    parseFloat(
                      JSON.parse(log[idx + 1].payload).meter.meterReading
                    )) /
                  1000
                ).toFixed(3),
              });
            }
          });

          let last_history = await models.iot_histories.findOne({
            where: {
              device_id: data.node_id,
              reportTimeFlagStart: models.sequelize.where(
                models.sequelize.fn(
                  "date",
                  models.sequelize.col("iot_histories.reportTimeFlag")
                ),
                "=",
                moment(data.start_date).subtract(1, "day").format("YYYY-MM-DD")
              ),
            },
            order: [["reportTimeFlag", "DESC"]],
          });
          last_history = JSON.parse(JSON.stringify(last_history));
          if (last_history == null) {
            last_history = await models.iot_histories.findOne({
              where: {
                device_id: data.node_id,
                reportTimeFlagStart: models.sequelize.where(
                  models.sequelize.fn(
                    "date",
                    models.sequelize.col("iot_histories.reportTimeFlag")
                  ),
                  "=",
                  moment(output[output.length - 1].update_time).format(
                    "YYYY-MM-DD"
                  )
                ),
              },
              order: [["reportTimeFlag", "ASC"]],
            });
            output[output.length - 1].usage = (
              output[output.length - 1].totalizer -
              parseFloat(JSON.parse(last_history.payload).meter.meterReading) /
                1000
            ).toFixed(3);
          } else {
            output[output.length - 1].usage = (
              output[output.length - 1].totalizer -
              parseFloat(JSON.parse(last_history.payload).meter.meterReading) /
                1000
            ).toFixed(3);
          }
          const field = [
            {
              label: "Update Time",
              value: "update_time",
            },
            {
              label: [1, 2].includes(node.typeId)
                ? "Totalizer/m3"
                : "Totalizer/kwh",
              value: "totalizer",
            },
            {
              label: [1, 2].includes(node.typeId) ? "Usage/m3" : "Usage/kwh",
              value: "usage",
            },
          ];

          function compare(a, b) {
            if (a.update_time < b.update_time) {
              return -1;
            }
            if (a.update_time > b.update_time) {
              return 1;
            }
            return 0;
          }

          output = output.sort(compare);
          return downloadResource(
            res,
            `Log_Daily_${node.devEui}_${data.start_date}-${data.end_date}.csv`,
            field,
            output
          );
        }
      }
    } catch (error) {
      return res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  })
  .get("/date", async function (req, res, next) {
    const data = req.query;
    if (
      data.node_id == null ||
      data.start_date == null ||
      data.end_date == null
    ) {
      return res.status(400).json({
        responseCode: 400,
        message: "node_id,start_date,end_date is requered",
      });
    } else {
      let node = await models.iot_nodes.findOne({
        where: { id: data.node_id },
        attributes: ["id", "devEui", "typeId"],
      });
      node = JSON.parse(JSON.stringify(node));
      let data_csv = [];
      let field = [];
      if ([1, 2].includes(node.typeId)) {
        data_csv = await data_gas_water(data);
        field = fields_gas_water;
      } else if ([3, 7].includes(node.typeId)) {
        data_csv = await data_ct_nonct(data);
        field = fields_ct_nonct;
      }
      console.log(data_csv);
      function compare(a, b) {
        if (a.update_time < b.update_time) {
          return -1;
        }
        if (a.update_time > b.update_time) {
          return 1;
        }
        return 0;
      }

      data_csv = data_csv.sort(compare);
      return downloadResource(
        res,
        `Log_Range_${node.devEui}_${data.start_date}-${data.end_date}.csv`,
        field,
        data_csv
      );
    }
  });

module.exports = router;

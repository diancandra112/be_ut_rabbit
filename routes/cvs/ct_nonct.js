const fields_ct_nonct = [
  {
    label: "Update Time",
    value: "update_time",
  },
  {
    label: "Totalizer/kwh",
    value: "totalizer",
  },
  {
    label: "Valve",
    value: "valve",
  },
  {
    label: "Usage/kwh",
    value: "usage",
  },
  {
    label: "Battery Level",
    value: "battery",
  },
];
const data_ct_nonct = async (data) => {
  const models = require("../../models/index");
  const { Op } = models.Sequelize;
  const moment = require("moment");
  let history = await models.iot_histories.findAll({
    where: {
      device_id: data.node_id,
      reportTimeFlagStart: models.sequelize.where(
        models.sequelize.fn(
          "date",
          models.sequelize.col("iot_histories.reportTimeFlag")
        ),
        ">=",
        moment(data.start_date).format("YYYY-MM-DD")
      ),
      reportTimeFlagEnd: models.sequelize.where(
        models.sequelize.fn(
          "date",
          models.sequelize.col("iot_histories.reportTimeFlag")
        ),
        "<=",
        moment(data.end_date).format("YYYY-MM-DD")
      ),
    },
    order: [["reportTimeFlag", "DESC"]],
  });

  history = JSON.parse(JSON.stringify(history));
  console.log(history);
  let last_history = 0;
  if (history.length > 0) {
    last_history = await models.iot_histories.findOne({
      where: {
        id: { [Op.lt]: history[history.length - 1].id },
        device_id: data.node_id,
      },
      order: [["reportTimeFlag", "DESC"]],
    });
    last_history = JSON.parse(JSON.stringify(last_history));
    last_history =
      last_history == null
        ? 0
        : JSON.parse(last_history.payload).meter.meterReading / 1000;
  }

  let data_csv = [];
  history.map((val, idx) => {
    let usage = 0;
    if (idx != history.length - 1) {
      usage =
        parseFloat(JSON.parse(val.payload).meter.meterReading) / 1000 -
        parseFloat(JSON.parse(history[idx + 1].payload).meter.meterReading) /
          1000;
    } else {
      usage =
        parseFloat(JSON.parse(val.payload).meter.meterReading) / 1000 -
        last_history;
    }
    data_csv.push({
      update_time: moment(val.reportTimeFlag).format(
        "YYYY-MM-DD HH:mm:ss+0700"
      ),
      totalizer: JSON.parse(val.payload).meter.meterReading / 1000,
      valve: JSON.parse(val.payload).meter.valve,
      usage: usage.toFixed(3),
      battery: "100 %",
    });
  });

  return data_csv;
};
module.exports = { fields_ct_nonct, data_ct_nonct };

const express = require("express");
const router = express.Router();

const {
  ctrlOnlineNode,
  ctrlOnlineNodeList,
  ctrlPressureOnlineNode,
  ctrlPressureOnlineNodeCount,
  ctrlNodePressure,
  ctrlNodeRusakCount,
  ctrlNodeRusakList,
} = require("../controllers/dashboard/controller_node_online");
const {
  ctrlOfflineNode,
  ctrlOfflineNodeList,
} = require("../controllers/dashboard/controller_node_offline");
const {
  ctrlUnsignedNodeList,
  ctrlUnsignedNodeCount,
  ctrlWaterLevel,
} = require("../controllers/dashboard/controller_unsigned");
const {
  ctrlStealCount,
  ctrlStealNodeList,
} = require("../controllers/dashboard/controller_steal");
const {
  ctrlAssignedNodeCount,
  ctrlAssignedNodeList,
} = require("../controllers/dashboard/controller_assigned");
const {
  ctrlCartNodeYesterday,
  ctrlCartNodeDays,
  ctrlChartLast7days,
  ctrlChartLast30days,
} = require("../controllers/dashboard/chart/controller_chart");
const {
  ctrlNodeBatteryCount,
  ctrlNodeBatteryList,
} = require("../controllers/dashboard/controller_battery");
const {
  ctrlNodeAnomaliList,
  ctrlNodeAnomaliCount,
  ctrlNodeAnomaliListTime,
  ctrlNodeAnomaliListUsage,
  ctrlNodeAnomaliListClose,
  ctrlNodeAnomaliListCloseStatus,
  ctrlNodeAnomaliUsageCount,
  ctrlNodeAnomaliTimeCount,
  ctrlNodeAnomaliListCloseStatusAll,
  ctrlNodeAnomaliUsageChart,
} = require("../controllers/dashboard/controller_anomali");
const {
  ctrlGatewayOfflineCount,
  ctrlGatewayOfflineList,
  ctrlGatewaySum,
} = require("../controllers/dashboard/controller_gateway_offline");
const {
  ctrlGatewayOnlineCount,
  ctrlGatewayOnlineList,
  ctrlNodeList,
} = require("../controllers/dashboard/controller_gateway_online");
const {
  AuthArea,
  AuthCompanyArea,
  AuthCompanyAreaTeknisi,
} = require("../middlewares/jwt_auth");
const {
  controllerGetType,
  controllerGetTypeCount,
  controllerGetTypeGas,
  controllerGetTypeWaterLevel,
} = require("../controllers/dashboard/chart/controller_type");

router
  .get("/node/list", AuthCompanyAreaTeknisi, ctrlNodeList)
  .get("/gateway/sum", AuthCompanyAreaTeknisi, ctrlGatewaySum)
  .get("/gateway/offline", AuthCompanyAreaTeknisi, ctrlGatewayOfflineCount)
  .get("/gateway/offline/list", AuthCompanyAreaTeknisi, ctrlGatewayOfflineList)
  .get("/gateway/online", AuthCompanyAreaTeknisi, ctrlGatewayOnlineCount)
  .get("/gateway/online/list", AuthCompanyAreaTeknisi, ctrlGatewayOnlineList)
  .get("/node/anomali", AuthCompanyAreaTeknisi, ctrlNodeAnomaliCount)
  .get("/node/anomali/:tenant", AuthCompanyAreaTeknisi, ctrlNodeAnomaliList)
  .get(
    "/node/anomali/time/:tenant/:status",
    AuthCompanyAreaTeknisi,
    ctrlNodeAnomaliListTime
  )
  .get(
    "/node/anomali/usage/:tenant/:status",
    AuthCompanyAreaTeknisi,
    ctrlNodeAnomaliListUsage
  )
  .get(
    "/node/anomali/usage/chart",
    AuthCompanyAreaTeknisi,
    ctrlNodeAnomaliUsageChart
  )
  .get(
    "/node/anomali/usage/count",
    AuthCompanyAreaTeknisi,
    ctrlNodeAnomaliUsageCount
  )
  .get(
    "/node/anomali/time/count",
    AuthCompanyAreaTeknisi,
    ctrlNodeAnomaliTimeCount
  )
  .delete(
    "/node/anomali/delete/:id",
    AuthCompanyAreaTeknisi,
    ctrlNodeAnomaliListClose
  )
  .delete("/node/anomali/delete", AuthArea, ctrlNodeAnomaliListClose)
  .put(
    "/node/anomali/close/:id",
    AuthCompanyAreaTeknisi,
    ctrlNodeAnomaliListCloseStatus
  )
  .patch("/node/anomali/close/all", AuthArea, ctrlNodeAnomaliListCloseStatusAll)
  .get("/node/battery", AuthCompanyAreaTeknisi, ctrlNodeBatteryCount)
  .get("/node/battery/:tenant", AuthCompanyAreaTeknisi, ctrlNodeBatteryList)
  .get("/node/online", AuthCompanyAreaTeknisi, ctrlOnlineNode)
  .get("/node/pressure", AuthCompanyAreaTeknisi, ctrlNodePressure)
  .get("/node/rusak/count", AuthCompanyAreaTeknisi, ctrlNodeRusakCount)
  .get("/node/rusak/list", AuthCompanyAreaTeknisi, ctrlNodeRusakList)
  .get(
    "/node/pressure/online/:tenant",
    AuthCompanyAreaTeknisi,
    ctrlPressureOnlineNode
  )
  .get(
    "/node/pressure/online",
    AuthCompanyAreaTeknisi,
    ctrlPressureOnlineNodeCount
  )
  .get("/node/online/:tenant", AuthCompanyAreaTeknisi, ctrlOnlineNodeList)
  .get("/node/offline", AuthCompanyAreaTeknisi, ctrlOfflineNode)
  .get("/node/offline/:tenant", AuthCompanyAreaTeknisi, ctrlOfflineNodeList)
  .get("/unssigned/node/list", AuthCompanyAreaTeknisi, ctrlUnsignedNodeList)
  .get("/unssigned/node/count", AuthCompanyAreaTeknisi, ctrlUnsignedNodeCount)
  .get(
    "/assigned/node/list/:tenant",
    AuthCompanyAreaTeknisi,
    ctrlAssignedNodeList
  )
  .get("/assigned/node/count", AuthCompanyAreaTeknisi, ctrlAssignedNodeCount)

  .get(
    "/chart/node/yesterday/:typeId",
    AuthCompanyAreaTeknisi,
    ctrlCartNodeYesterday
  )
  .get("/chart/node/days/:typeId", AuthCompanyAreaTeknisi, ctrlCartNodeDays)
  .get("/chart/node/7days/:typeId", AuthCompanyAreaTeknisi, ctrlChartLast7days)
  .get(
    "/chart/node/30days/:typeId",
    AuthCompanyAreaTeknisi,
    ctrlChartLast30days
  )
  .get("/chart/node/type", AuthCompanyAreaTeknisi, controllerGetType)
  .get(
    "/check/water/level",
    AuthCompanyAreaTeknisi,
    controllerGetTypeWaterLevel
  )
  .get("/check/gas", AuthCompanyAreaTeknisi, controllerGetTypeGas)
  .get("/check/pressure", AuthCompanyAreaTeknisi, controllerGetTypeCount)

  .get("/alarm/water/level", AuthCompanyAreaTeknisi, ctrlWaterLevel)

  .get("/steal/node/list", AuthCompanyAreaTeknisi, ctrlStealNodeList)
  .get("/steal/node/count", AuthCompanyAreaTeknisi, ctrlStealCount);

module.exports = router;

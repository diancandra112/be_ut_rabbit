const express = require("express");
const router = express.Router();
const fs = require("fs");
/* GET users listing. */
router
  .get("/billing/attachment/:file/:type", function (req, res, next) {
    req.params.type = req.params.type.replace("garing", "/");
    fs.readFile(
      `${req.query.destination}/` + req.params.file,
      function (err, content) {
        if (err) {
          res.writeHead(400, { "Content-type": "text/html" });
          console.log(err);
          res.end("No such image");
        } else {
          res.writeHead(200, { "Content-type": req.params.type });
          res.end(content);
        }
      }
    );
  })
  .get("/pdf/:images", function (req, res, next) {
    fs.readFile("./attachment/" + req.params.images, function (err, content) {
      if (err) {
        res.writeHead(400, { "Content-type": "text/html" });
        console.log(err);
        res.end("No such image");
      } else {
        //specify the content type in the response will be an image
        res.writeHead(200, { "Content-type": "application/pdf" });
        // res.contentType("application/pdf");
        // console.log(fs.createReadStream("./pdf/Dashboard.pdf"));
        res.end(content);
      }
    });
  })
  .get("/:images", function (req, res, next) {
    fs.readFile("./images/" + req.params.images, function (err, content) {
      if (err) {
        res.writeHead(400, { "Content-type": "text/html" });
        console.log(err);
        res.end("No such image");
      } else {
        //specify the content type in the response will be an image
        res.writeHead(200, { "Content-type": "image/jpg" });
        // res.contentType("application/pdf");
        // console.log(fs.createReadStream("./pdf/Dashboard.pdf"));
        res.end(content);
      }
    });
  })
  .get("/tiketing/:images", function (req, res, next) {
    fs.readFile(
      "./images/tiketing/" + req.params.images,
      function (err, content) {
        if (err) {
          res.writeHead(400, { "Content-type": "text/html" });
          console.log(err);
          res.end("No such image");
        } else {
          //specify the content type in the response will be an image
          res.writeHead(200, { "Content-type": "image/jpg" });
          // res.contentType("application/pdf");
          // console.log(fs.createReadStream("./pdf/Dashboard.pdf"));
          res.end(content);
        }
      }
    );
  })
  .post("/pdf", (req, res, next) => {
    console.log(req.body.data.split("/")[1].split(";")[0]);
  })
  .get("/pdf/:pdf", (req, res, next) => {
    console.log(req);
  });
module.exports = router;

const express = require("express");
const moment = require("moment");
const router = express.Router();

const { yukk_qris } = require("../config/axios_qris");
const models = require("../models/index");
const { AuthArea, AuthCompanyArea } = require("../middlewares/jwt_auth");
/* GET users listing. */
let inquire = (nominal) => {
  return new Promise(async (resolve, reject) => {
    try {
      let qris = await yukk_qris({
        method: "GET",
        url: `/api/request/qris/${nominal}`,
      });
      if (qris.data.status_code == 6000 && qris.data.result != null) {
        let update = {
          qris_string: qris.data.result.qris_string,
          qris_timeout: qris.data.result.timeout_datetime,
          qris_order_id: qris.data.result.order_id,
          qris_nominal: qris.data.result.nominal,
        };
        return resolve(update);
      } else {
        return reject(qris.data.status_message);
      }
    } catch (error) {
      return reject(error);
    }
  });
};
router
  .post("/request/:id", async (req, res) => {
    try {
      transaction = await models.sequelize.transaction();
      let voucher = await models.iots_billing_prepaid.findOne({
        where: { id: req.params.id },
        transaction,
      });
      voucher = JSON.parse(JSON.stringify(voucher));
      if (voucher == null) throw new Error("Voucher Not Found");
      if (voucher.qris_order_id != null) {
        let qris = await yukk_qris({
          method: "POST",
          url: `/api/Inquiry/qris/${voucher.qris_order_id}`,
        });
        if (voucher.status == "UNPAID" && qris.data.result.status == "PAID") {
          await models.iots_billing_prepaid.update(
            {
              payment_qris: true,
              status: "PAID",
              payment_date: moment().utcOffset("+0700").format(),
            },
            {
              where: { qris_order_id: voucher.qris_order_id },
              transaction,
            }
          );
          await transaction.commit();
          let update = {
            qris_string: voucher.qris_string,
            qris_timeout: voucher.qris_timeout,
            qris_order_id: voucher.qris_order_id,
            qris_nominal: voucher.qris_nominal,
          };
          return res.status(200).json({ responseCode: 200, data: update });
        } else if (voucher.status == "PAID") {
          let update = {
            qris_string: voucher.qris_string,
            qris_timeout: voucher.qris_timeout,
            qris_order_id: voucher.qris_order_id,
            qris_nominal: voucher.qris_nominal,
          };
          return res.status(200).json({ responseCode: 200, data: update });
        } else if (
          (qris.data.result != null && qris.data.result.status == "PAID") ||
          (qris.data.result != null &&
            qris.data.result.status == "NOT_PAID" &&
            qris.data.result.is_timeout == false)
        ) {
          let update = {
            qris_string: voucher.qris_string,
            qris_timeout: voucher.qris_timeout,
            qris_order_id: voucher.qris_order_id,
            qris_nominal: voucher.qris_nominal,
          };
          return res.status(200).json({ responseCode: 200, data: update });
        } else if (
          qris.data.result == null ||
          (qris.data.result.status == "NOT_PAID" &&
            qris.data.result.is_timeout == true)
        ) {
          let update = await inquire(voucher.product_price);
          await models.iots_billing_prepaid.update(update, {
            where: { id: req.params.id },
            transaction,
          });
          await transaction.commit();
          return res.status(200).json({ responseCode: 200, data: update });
        }
      } else {
        let update = await inquire(voucher.product_price);
        await models.iots_billing_prepaid.update(update, {
          where: { id: req.params.id },
          transaction,
        });
        await transaction.commit();
        return res.status(200).json({ responseCode: 200, data: update });
      }
    } catch (error) {
      console.log(error);
      if (transaction) await transaction.rollback();
      return res
        .status(500)
        .json({ responseCode: 500, message: error.message });
    }
  })
  .post("/callback", async (req, res) => {
    try {
      transaction = await models.sequelize.transaction();
      let vouher = await models.iots_billing_prepaid.findOne({
        where: { qris_order_id: req.body.order_id },
        include: [{ model: models.iot_nodes }],
        transaction,
      });
      vouher = JSON.parse(JSON.stringify(vouher));
      let data_log = {
        time: moment().utcOffset("+0700").format(),
        action: "topup " + vouher.product_value + " " + vouher.satuan_vc,
        deveui: vouher.device_deveui,
        email: "QRIS",
      };
      let nominal =
        parseFloat(vouher.product_value) * 1000 +
        parseFloat(vouher.iot_node.prepayment);
      let data_down = `/action/topup/${vouher.device_deveui}/${nominal}`;
      const { weiots } = require("../config/axios");
      weiots({
        method: "post",
        url: data_down,
      });
      await models.iots_log_downlink.create(data_log, { transaction });
      await models.iot_nodes.update(
        {
          prepayment: nominal.toString(),
          live_prepayment_date: moment().utcOffset("+0700").format(),
        },
        { where: { devEui: vouher.device_deveui } }
      );
      await models.iots_billing_prepaid.update(
        {
          payment_qris: true,
          status: "PAID",
          payment_date: moment().utcOffset("+0700").format(),
        },
        {
          where: { qris_order_id: req.body.order_id },
          transaction,
        }
      );
      await transaction.commit();
      return res.status(200).json({ responseCode: 200, data: "ok" });
    } catch (error) {
      console.log(error);
      if (transaction) await transaction.rollback();
      return res
        .status(500)
        .json({ responseCode: 500, message: error.message });
    }
  });

module.exports = router;

const express = require("express");
const router = express.Router();
const fs = require("fs");
var multer = require("multer");
const {
  svcBilling,
  CtrlSendEmailTest,
  CtrlSendEmailTestMul,
  CtrlSendEmailMidtrans,
  CtrlSendEmailFasapay,
} = require("../controllers/billing/controller_email");
const { AuthArea } = require("../middlewares/jwt_auth");
/* GET users listing. */

var storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, "./attachment");
  },
  filename: function (req, file, callback) {
    callback(null, Date.now() + "1-2" + file.originalname);
  },
});
var limits = {
  fileSize: 50024 * 50024, // 1 MB (max file size)
};

var upload = multer({ storage: storage, limits: limits });

router
  .post(
    "/send/multer/test",
    upload.fields([
      {
        name: "attachment",
      },
      {
        name: "default",
      },
    ]),
    CtrlSendEmailTestMul
  )
  // .post("/send", AuthArea, upload.array("attachment"), svcBilling)
  .post(
    "/send",
    AuthArea,
    upload.fields([
      {
        name: "attachment",
      },
      {
        name: "attachment_log",
      },
      {
        name: "attachment_inv",
      },
    ]),
    svcBilling
  )
  // .post(
  //   "/midtrans",
  //   upload.fields([
  //     {
  //       name: "attachment",
  //     },
  //     {
  //       name: "attachment_log",
  //     },
  //     {
  //       name: "attachment_inv",
  //     },
  //   ]),
  //   CtrlSendEmailMidtrans
  // )
  .post(
    "/midtrans",
    upload.fields([
      {
        name: "attachment",
      },
      {
        name: "attachment_log",
      },
      {
        name: "attachment_inv",
      },
    ]),
    CtrlSendEmailFasapay
  )
  .post("/send/test", CtrlSendEmailTest);
module.exports = router;

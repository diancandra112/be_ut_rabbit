const express = require("express");
const router = express.Router();
const fs = require("fs");
const {
  ctrlCreateBillingAll,
  ctrlCreateBillingCustom,
  ctrlCreateBillingAllManual,
} = require("../controllers/billing/controller_billing_create_v2");
const { AuthArea } = require("../middlewares/jwt_auth");

/* GET users listing. */
router
  .get("/create/all", ctrlCreateBillingAll)
  .get("/create/custom", ctrlCreateBillingCustom)
  // .post("/create/manual/oneprice", AuthArea, ctrlCreateBillingAllManual);
  .get("/create/manual/oneprice", ctrlCreateBillingAllManual);
module.exports = router;

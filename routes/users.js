var express = require("express");
var router = express.Router();
const { wa_token, wa_damp } = require("../config/wa");
var multer = require("multer");
const {
  ctrlBillingCombine,
} = require("../controllers/billing/controller_billing_combine");
const {
  controllerPricingMemberLevel,
  controllerOnePriceForAll,
  controllerGetAreaProfilePricing,
  controllerGetOnePriceForAll,
  controllerPricingBillingArea,
  controllerGetPricingBillingArea,
} = require("../controllers/controller_pricing_postpaid");
const {
  ctrlGetAutoBill,
} = require("../controllers/billing/controller_auto_bill");
const {
  ctrlGetUsage,
  ctrlGetUsageV2,
  ctrlGetUsageDate,
} = require("../controllers/billing/controller_usage");
const {
  ctrlGetBilling,
  ctrlUpdateBilling,
  ctrlGetUsageBilling,
  ctrlBillingCorrection,
  ctrlcBillingCorrection,
  ctrlGetCorrection,
  ctrlUpdateBillingClose,
  ctrlGetBillingClose,
  ctrlGetUsageBillingV2,
  ctrlcBillingLogArea,
  ctrlcBillingAttachCombine,
} = require("../controllers/billing/controller_billing");

const {
  ctrlCreateCutOff,
  ctrlCreateCutOffSaas,
} = require("../controllers/billing/controller_cutoff");
const {
  AuthCompany,
  AuthCompanyArea,
  AuthArea,
  AuthTenant,
  AuthCompanyAreaTeknisi,
} = require("../middlewares/jwt_auth");
const {
  controllerAreaRegister,
  controllerUserLogin,
  controllerTenantRegister,
  controllerTenantEdit,
  controllerAreaGet,
  controllerAreaProfiles,
  controllerTenantGet,
  controllerAreaProfilesErp,
  controllerTenantPassword,
  controllerTenantParameters,
  controllerGetTenantParameters,
} = require("../controllers");
const {
  controllerEditAreaProfiles,
  controllerEditAlarmPressure,
  controllerAlarmPressure,
  controllerSaveErp,
  controllerGetErp,
} = require("../controllers/controller_area");
const {
  ctrlListMember,
} = require("../controllers/pricingOption/controller_pricing_option");
const {
  controllerAddType,
  controllerEditType,
  controllerGetType,
  controllerGetTypeTenantInternal,
  controllerGetTypePricing,
} = require("../controllers/controller_type");
const {
  ControllerCheckNodeRtuNew,
  controllerPaginationHistory,
  controllerDeviceLog,
  controllerPaginationHistorynopage,
  controllerPaginationHistorySum,
  controllerPaginationHistorySumV2,
  ControllerAddNode,
  ControllerHistoryNode,
  ControllerHistoryNodev2,
  ControllerHistoryFilterSum,
  ControllerListNode,
  ControllerListNodeV2,
  ControllerCheckNode,
  ControllerRegisterNode,
  ControllerListUnassignedNode,
  ControllerAssignedNode,
  ControllerUpdateIstallation,
  ControllerDeleteIstallation,
  ControllerUnassignedNode,
  ControllerAdjustBalace,
  controllerPaginationHistoryMobile,
  controllerPaginationHistorySumV2Mobile,
  ControllerListPrepaidNode,
  ControllerListNodeLink,
  ControllerSaveNodeLink,
  ControllerNodeUnlink,
} = require("../controllers/controller_node");
const {
  controllerInternalGet,
  controllerInternalRegister,
  controllerInternalEdit,
} = require("../controllers/controller_internal");

const {
  validasiRegisterArea,
  validasiRegisterCompany,
  validasiRegisterTenant,
} = require("../middlewares/users_validasi");
const { validasiProfileCompany } = require("../middlewares/company_validasi");
const { validasiProfileArea } = require("../middlewares/area_validasi");
const { validasiGetTenant } = require("../middlewares/tenant_validasi");
const {
  validasiGetHistoryNode,
  validasiGetNode,
  validasiAssignedNode,
  validasiUnsignedNode,
  validasiGetUnsignedNode,
} = require("../middlewares/node_validaasi");
const { validasiAddGateway } = require("../middlewares/add_gateways");
const {
  controllerGetMember,
  controllerSetMember,
  controllerUpdateMember,
  controllerDeleteMember,
} = require("../controllers/controller_member_level");

const {
  ControllerCompanyProfile,
  ControllerCompanyProfileEdit,
  controllerCompaniesRegister,
  ControllerCompanyPasswordEdit,
  ControllerAreaPasswordEdit,
} = require("../controllers/controller_company");
const { ControllerUploadImages } = require("../controllers/controller_images");
const {
  controllerGatewayRegister,
  controllerGatewayEdit,
  controllerGatewayGet,
  ControllerCheckGateway,
  controllerGatewayDelete,
} = require("../controllers/controller_gateways");
const {
  controllerGetDeviceType,
} = require("../controllers/controller_device_type");
const {
  ctrlCoreNodeHistoryApi,
  ctrlCoreNodeHistoryNonMeterApi,
} = require("../controllers/core/controller_core_api");
const {
  controllerGetAppName,
  controllerRtuEditBilling,
  controllerBillingDenda,
} = require("../controllers/controller_rtu_appname");
const {
  ctrlSendMail,
} = require("../controllers/billing/controller_send_email");

const {
  controllerHistoryFilter,
  controllerHistoryRangeFilter,
} = require("../controllers/controller_date_filter");

const {
  validasiRegisterInternal,
  validasiGetInternal,
} = require("../middlewares/internal_validasi");
/* GET users listing. */

const models = require("../models/index");
const Op = models.Sequelize.Op;
const moment = require("moment");
const fs = require("fs");

var storage = multer.diskStorage({
  destination: function (req, file, callback) {
    let path = `./attachment/${req.params.id}`;
    fs.mkdirSync(path, { recursive: true });
    callback(null, path);
  },
  filename: function (req, file, callback) {
    callback(null, req.params.id.toString() + file.originalname);
  },
});
var limits = {
  fileSize: 50024 * 50024, // 1 MB (max file size)
};

var upload = multer({ storage: storage, limits: limits });

router
  .post("/wa/rimender/:id", (req, res) => {
    try {
      models.iots_billing_history
        .findOne({ where: req.params, attributes: ["id", "wa_rimender"] })
        .then((result) => {
          result = JSON.parse(JSON.stringify(result));
          let billing_id = result.id;
          result = JSON.parse(result.wa_rimender);
          return wa_damp({
            method: "POST",
            url: "billing_reminder_rev",
            data: {
              to: result.to,
              token: wa_token,
              param: result.params,
            },
          })
            .then(async (wa) => {
              let data_wa = [];
              wa.data.data.map((val_wa) => {
                data_wa.push({
                  billing_id: billing_id,
                  wa_raw: JSON.stringify(val_wa),
                  wa_msgId: val_wa.msgId,
                  wa_status: val_wa.status,
                  wa_responseCode: wa.data.responseCode,
                  wa_number: val_wa.to,
                  note: "reminder",
                });
              });

              await models.iots_wa_status.bulkCreate(data_wa);
              res.json(wa);
            })
            .catch((error) => {
              console.log(error);
              res.json(error);
            });
        })
        .catch((error) => {
          console.log(error);
          res.json("ok");
        });
    } catch (error) {
      console.log(error);
    }
  })
  .post("/callback/wa", (req, res) => {
    try {
      console.log(req.body);
      models.iots_wa_status
        .update(
          {
            wa_raw: JSON.stringify(req.body),
            wa_status: req.body.statuses[0].status,
          },
          { where: { wa_msgId: req.body.statuses[0].id } }
        )
        .then((result) => {
          console.log(result);
          res.json("ok");
        })
        .catch((error) => {
          console.log(error);
          res.json("ok");
        });
    } catch (error) {
      console.log(error);
    }
  })
  .get("/testing/billing", (req, res) => {
    models.iots_billing_history
      .findAll({ include: [{ model: models.iot_nodes }] })
      .then((resa) => {
        let data = [];
        resa = JSON.parse(JSON.stringify(resa));
        let update = [];
        resa.map((value, key) => {
          update.push({
            id: value.id,
            deveui: value.iot_node.devEui,
            typeId: parseInt(value.iot_node.typeId),
            node_type:
              parseInt(value.iot_node.typeId) == 1
                ? "GAS"
                : parseInt(value.iot_node.typeId) == 2
                ? "WATER"
                : parseInt(value.iot_node.typeId) == 4
                ? "ELECTRICITY NON CT"
                : parseInt(value.iot_node.typeId) == 5
                ? "PRESSURE"
                : parseInt(value.iot_node.typeId) == 6
                ? "TEMPERATURE"
                : "ELECTRICITY CT",
            unit:
              parseInt(value.iot_node.typeId) == 3 ||
              parseInt(value.iot_node.typeId) == 7
                ? "kwh"
                : "m3",
          });
          resa[key].deveui = value.iot_node.devEui;
          resa[key].node_type =
            parseInt(value.iot_node.typeId) == 1
              ? "GAS"
              : parseInt(value.iot_node.typeId) == 2
              ? "WATER"
              : parseInt(value.iot_node.typeId) == 3
              ? "ELECTRICITY NON CT"
              : parseInt(value.iot_node.typeId) == 4
              ? "RTU"
              : parseInt(value.iot_node.typeId) == 5
              ? "PRESSURE"
              : parseInt(value.iot_node.typeId) == 6
              ? "TEMPERATURE"
              : "ELECTRICITY CT";
          resa[key].unit =
            parseInt(value.iot_node.typeId) == 3 ||
            parseInt(value.iot_node.typeId) == 7
              ? "kwh"
              : "m3";
        });
        // resa.map((value, key) => {
        //   if (key == 0) {
        //     data.push({
        //       id: value.id,
        //       meterReading: parseFloat(
        //         JSON.parse(value.payload).meter.meterReading
        //       ),
        //       usage: 0,
        //     });
        //   } else {
        //     data.push({
        //       id: value.id,
        //       meterReading: parseFloat(
        //         JSON.parse(value.payload).meter.meterReading
        //       ),
        //       usage:
        //         (parseFloat(JSON.parse(value.payload).meter.meterReading) -
        //           data[key - 1].meterReading) /
        //         1000,
        //     });
        //   }
        // });
        // res.json(resa);
        models.iots_billing_history
          .bulkCreate(update, {
            updateOnDuplicate: ["deveui", "node_type", "unit", "typeId"],
          })
          .then(() => {
            res.json(update);
          });
        // models.iot_histories
        //   .bulkCreate(data, {
        //     updateOnDuplicate: ["usage"],
        //   })
        //   .then((a) => {
        //     res.json(a);
        //   });
      });
  })
  .get("/testing/usage", (req, res) => {
    models.iot_histories
      .findAll({ where: { device_id: 3 }, order: [["reportTimeFlag", "ASC"]] })
      .then((resa) => {
        let data = [];
        resa = JSON.parse(JSON.stringify(resa));
        resa.map((value, key) => {
          if (key == 0) {
            data.push({
              id: value.id,
              meterReading: parseFloat(
                JSON.parse(value.payload).meter.meterReading
              ),
              usage: 0,
            });
          } else {
            data.push({
              id: value.id,
              meterReading: parseFloat(
                JSON.parse(value.payload).meter.meterReading
              ),
              usage:
                (parseFloat(JSON.parse(value.payload).meter.meterReading) -
                  data[key - 1].meterReading) /
                1000,
            });
          }
        });
        models.iot_histories
          .bulkCreate(data, {
            updateOnDuplicate: ["usage"],
          })
          .then((a) => {
            res.json(a);
          });
      });
  })
  .get("/testing", (req, res) => {
    models.iot_histories
      .findAll({
        where: {
          reportTime: models.sequelize.where(
            models.sequelize.fn(
              "date",
              models.sequelize.col("iot_histories.reportTime")
            ),
            ">=",
            moment("2020-08-17").utcOffset("+0700").format("YYYY-MM-DD")
          ),
          reportTime2: models.sequelize.where(
            models.sequelize.fn(
              "date",
              models.sequelize.col("iot_histories.reportTime")
            ),
            "<=",
            moment("2020-08-31").utcOffset("+0700").format("YYYY-MM-DD")
          ),
        },
      })
      .then((resa) => {
        let data = [];
        resa = JSON.parse(JSON.stringify(resa));
        resa.map((value, key) => {
          data.push({
            id: value.id,
            reportTimeFlag: moment(value.reportTime)
              .utcOffset("+0700")
              .format("YYYY-MM-DD HH:mm:ss"),
          });
        });
        models.iot_histories
          .bulkCreate(data, {
            updateOnDuplicate: ["reportTimeFlag"],
          })
          .then((a) => {
            res.json(a);
          });
      });
  })
  .get("/check/erp/v1/:id", async (req, res) => {
    try {
      let area = await models.iot_area.findOne({
        where: { id: req.params.id },
        attributes: ["id", "url_erp", "email_erp", "pass_erp", "is_erp"],
      });
      res
        .status(200)
        .json({ responseCode: 200, message: "Sukses Get Data", data: area });
    } catch (error) {
      res.status(500).json({ responseCode: 500, message: error.message });
    }
  })
  .post("/login", controllerUserLogin)
  .post(
    "/register/area",
    AuthCompany,
    validasiRegisterArea,
    controllerAreaRegister
  )
  .get("/list/area", controllerAreaGet)
  .get(
    "/profile/area",
    AuthCompanyArea,
    validasiProfileArea,
    controllerAreaProfiles
  )
  .put("/profile/area", AuthCompanyArea, controllerAreaProfilesErp)
  .put(
    "/password/tenant/:tenantId/:password/:username",
    AuthCompanyArea,
    controllerTenantPassword
  )
  .put(
    "/parameter/tenant/:tenantId",
    AuthCompanyArea,
    controllerTenantParameters
  )
  .get(
    "/parameter/tenant/:tenantId",
    AuthCompanyArea,
    controllerGetTenantParameters
  )
  .patch(
    "/profile/area",
    AuthCompanyArea,
    validasiProfileArea,
    controllerEditAreaProfiles
  )
  .get("/get/erp", AuthCompanyArea, controllerGetErp)
  .post("/save/erp", AuthCompanyArea, controllerSaveErp)
  .patch(
    "/profile/v2/area/:password",
    AuthCompanyArea,
    ControllerAreaPasswordEdit
  )
  .patch(
    "/alrm/pressure/:pressure/:water_pressure/:pressure_pt/:low_pressure",
    AuthCompanyArea,
    controllerEditAlarmPressure
  )
  .get("/alrm/pressure", AuthCompanyArea, controllerAlarmPressure)

  .post("/filter/single", controllerHistoryFilter)
  .post("/filter/range", controllerHistoryRangeFilter)

  .post(
    "/register/company",
    validasiRegisterCompany,
    controllerCompaniesRegister
  )
  .get(
    "/profile/company",
    AuthCompany,
    validasiProfileCompany,
    ControllerCompanyProfile
  )

  .patch("/profile/company", AuthCompany, ControllerCompanyProfileEdit)
  .patch(
    "/profile/company/:password",
    AuthCompany,
    ControllerCompanyPasswordEdit
  )

  .patch("/tenant", AuthArea, controllerTenantEdit)

  .post(
    "/register/tenant",
    AuthArea,
    validasiRegisterTenant,
    controllerTenantRegister
  )
  .get("/list/tenant", AuthCompanyAreaTeknisi, controllerTenantGet)

  .delete("/gateway/:id", AuthArea, controllerGatewayDelete)
  .post("/gateway", AuthArea, validasiAddGateway, controllerGatewayRegister)
  .get("/gateway", AuthCompanyAreaTeknisi, controllerGatewayGet)
  .post("/check/gateway", ControllerCheckGateway)

  .post(
    "/register/internal",
    AuthArea,
    validasiRegisterInternal,
    controllerInternalRegister
  )
  .patch("/internal", AuthArea, controllerInternalEdit)
  .get(
    "/list/internal",
    AuthCompanyArea,
    validasiGetInternal,
    controllerInternalGet
  )

  .get("/device/log/:devEui", AuthArea, controllerDeviceLog)
  .get("/pagination/node/non/mobile", controllerPaginationHistory)
  .get("/pagination/node", controllerPaginationHistoryMobile)
  .get("/pagination/node/nopage", controllerPaginationHistorynopage)
  .get("/pagination/node/sum", controllerPaginationHistorySum)
  .get(
    "/pagination/node/sum/:device_id/:start_date/:end_date",

    controllerPaginationHistorySumV2
  )
  .get(
    "/pagination/node/sum/mobile/:device_id/:start_date/:end_date",
    controllerPaginationHistorySumV2Mobile
  )
  .post("/adjust/balance/:id/:prepayment", ControllerAdjustBalace)
  .patch("/installation/node", ControllerUpdateIstallation)
  // .post("/unassigned/node", validasiUnsignedNode, ControllerUnassignedNode)
  .post("/unassigned/node/:devEui", ControllerUnassignedNode)
  .delete("/delete/node", ControllerDeleteIstallation)
  .post("/add/node", ControllerAddNode)
  .post("/register/node", AuthArea, ControllerRegisterNode)
  .get("/list/node", AuthCompanyArea, validasiGetNode, ControllerListNode)
  .get("/list/node/v2", AuthCompanyArea, validasiGetNode, ControllerListNodeV2)
  .get("/list/node/link/:id", AuthCompanyArea, ControllerListNodeLink)
  .post("/list/node/link/:id/:link_id", AuthCompanyArea, ControllerSaveNodeLink)
  .post("/list/node/unlink/:id/:link_id", AuthCompanyArea, ControllerNodeUnlink)
  .get("/list/node/prepaid", AuthTenant, ControllerListPrepaidNode)
  .get(
    "/unassigned/node",
    AuthCompanyArea,
    validasiGetUnsignedNode,
    ControllerListUnassignedNode
  )
  .post(
    "/assigned/node",
    AuthArea,
    validasiAssignedNode,
    ControllerAssignedNode
  )
  .post("/check/node", ControllerCheckNode)
  .post("/check/node/rtu/new", ControllerCheckNodeRtuNew)
  .get("/history/node", validasiGetHistoryNode, ControllerHistoryNode)

  .get("/v2/history/node", validasiGetHistoryNode, ControllerHistoryNodev2)
  .get("/histori/range/sum", ControllerHistoryFilterSum)

  .get("/device/type", controllerGetDeviceType)

  .get("/billing/denda/:link_inv_denda", controllerBillingDenda)
  .get("/rtu/appname", controllerGetAppName)
  .patch("/rtu/billing", controllerRtuEditBilling)

  .post("/add/type", controllerAddType)
  .get("/list/type", controllerGetType)
  .get("/list/type/pricing", AuthCompanyArea, controllerGetTypePricing)
  .get("/type", controllerGetTypeTenantInternal)
  .patch("/update/type", controllerEditType)

  .post("/upload/images", ControllerUploadImages)

  .get("/member", AuthCompanyArea, controllerGetMember)
  .post("/member", AuthCompanyArea, controllerSetMember)
  .patch("/member", AuthCompanyArea, controllerUpdateMember)
  .delete("/member", AuthCompanyArea, controllerDeleteMember)

  .post("/pricing/postpaid", AuthCompanyArea, controllerPricingMemberLevel)
  .get("/area/profile/v2", AuthCompanyArea, controllerGetAreaProfilePricing)
  .get("/pricing/postpaid/all", AuthCompanyArea, controllerGetOnePriceForAll)
  .post("/pricing/postpaid/all", AuthCompanyArea, controllerOnePriceForAll)
  .post("/pricing/billing/area", AuthCompanyArea, controllerPricingBillingArea)
  .get(
    "/pricing/billing/area",
    AuthCompanyArea,
    controllerGetPricingBillingArea
  )

  .post("/cutoff", AuthCompanyArea, ctrlCreateCutOff)
  .post("/cutoff/saas", AuthCompanyArea, ctrlCreateCutOffSaas)

  .get("/bill/list", AuthCompanyArea, ctrlGetAutoBill)

  .get("/usage", AuthArea, ctrlGetUsage)
  .get("/usage/:type", AuthArea, ctrlGetUsageV2)
  .get("/usage_date", AuthArea, ctrlGetUsageDate)

  .get(
    "/billing/combine/attachment/:log_area_id",
    AuthArea,
    ctrlcBillingAttachCombine
  )
  .get("/billing", AuthArea, ctrlGetBilling)
  .get("/billing/log/area/:log_area_id", AuthArea, ctrlcBillingLogArea)
  .patch("/billing", AuthArea, ctrlUpdateBilling)
  .patch(
    "/billing/close/manual/:id",
    AuthArea,
    upload.fields([
      {
        name: "attachment",
      },
    ]),
    ctrlUpdateBillingClose
  )
  .get("/billing/close/manual/:id", AuthArea, ctrlGetBillingClose)
  .get("/billing/usage", AuthArea, ctrlGetUsageBilling)
  .get("/billing/usage/:areaId", ctrlGetUsageBillingV2)
  .get("/billing/correction", AuthArea, ctrlcBillingCorrection)
  .get("/correction", AuthArea, ctrlGetCorrection)
  .patch("/billing/correction", AuthArea, ctrlBillingCorrection)
  .post("/billing/combine/:periode_billing", AuthArea, ctrlBillingCombine)

  .post("/send/customer", AuthArea, ctrlSendMail)

  .post("/push/node/history", ctrlCoreNodeHistoryApi)
  .post("/push/node/history/non/meter", ctrlCoreNodeHistoryNonMeterApi)
  .get("/member/list", AuthArea, ctrlListMember)
  .post("/saas/check", AuthCompanyAreaTeknisi, async (req, res) => {
    try {
      const data = req.query;
      const { role, real_role, finance_name, finance_id } = res.locals.decoded;
      if (
        real_role == "FINANCE" &&
        (finance_name == null || finance_id == null)
      ) {
        return res.json({ responseCode: 400, message: false });
      } else if (role == "AREA") {
        let {
          iot_area: { id },
        } = res.locals.decoded;
        let area = await models.iot_area.findOne({ where: { id: id } });
        if (
          area.saas === true &&
          (area.saas_expired == null ||
            area.saas_expired <
              moment().utcOffset("+0700").format("YYYY-MM-DD"))
        ) {
          return res.json({ responseCode: 400, message: false });
        } else {
          return res.json({ responseCode: 200, message: true });
        }
      } else {
        return res.json({ responseCode: 200, message: true });
      }
      return console.log(role);
      if (role == "COMPANY") {
        if (req.query.areaId != undefined) {
          data.area_id = req.query.areaId;
        }
        delete data.areaId;
      } else if (role == "TEKNISI") {
        data.area_id = res.locals.decoded.area_id;
      } else if (role == "TEKNISI COMPANY") {
        if (req.query.areaId != undefined) {
          data.area_id = req.query.areaId;
        }
      } else {
        data.area_id = res.locals.decoded.iot_area.id;
      }
    } catch (error) {
      console.log(error);
      res.status(500).json({ responseCode: 500, message: error.message });
    }
  });

module.exports = router;

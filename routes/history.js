var express = require("express");
var router = express.Router();
const { wa_token, wa_damp } = require("../config/wa");
var multer = require("multer");
const {
  ctrlBillingCombine,
} = require("../controllers/billing/controller_billing_combine");
const {
  controllerPricingMemberLevel,
  controllerOnePriceForAll,
  controllerGetAreaProfilePricing,
  controllerGetOnePriceForAll,
  controllerPricingBillingArea,
  controllerGetPricingBillingArea,
} = require("../controllers/controller_pricing_postpaid");
const {
  ctrlGetAutoBill,
} = require("../controllers/billing/controller_auto_bill");
const {
  ctrlGetUsage,
  ctrlGetUsageV2,
  ctrlGetUsageDate,
} = require("../controllers/billing/controller_usage");
const {
  ctrlGetBilling,
  ctrlUpdateBilling,
  ctrlGetUsageBilling,
  ctrlBillingCorrection,
  ctrlcBillingCorrection,
  ctrlGetCorrection,
  ctrlUpdateBillingClose,
  ctrlGetBillingClose,
  ctrlGetUsageBillingV2,
  ctrlcBillingLogArea,
  ctrlcBillingAttachCombine,
} = require("../controllers/billing/controller_billing");

const {
  ctrlCreateCutOff,
  ctrlCreateCutOffSaas,
} = require("../controllers/billing/controller_cutoff");
const {
  AuthCompany,
  AuthCompanyArea,
  AuthArea,
  AuthTenant,
  AuthCompanyAreaTeknisi,
} = require("../middlewares/jwt_auth");
const {
  controllerAreaRegister,
  controllerUserLogin,
  controllerTenantRegister,
  controllerTenantEdit,
  controllerAreaGet,
  controllerAreaProfiles,
  controllerTenantGet,
  controllerAreaProfilesErp,
  controllerTenantPassword,
  controllerTenantParameters,
  controllerGetTenantParameters,
} = require("../controllers");
const {
  controllerEditAreaProfiles,
  controllerEditAlarmPressure,
  controllerAlarmPressure,
  controllerSaveErp,
  controllerGetErp,
} = require("../controllers/controller_area");
const {
  ctrlListMember,
} = require("../controllers/pricingOption/controller_pricing_option");
const {
  controllerAddType,
  controllerEditType,
  controllerGetType,
  controllerGetTypeTenantInternal,
  controllerGetTypePricing,
} = require("../controllers/controller_type");
const {
  ControllerCheckNodeRtuNew,
  controllerPaginationHistory,
  controllerDeviceLog,
  controllerPaginationHistorynopage,
  controllerPaginationHistorySum,
  controllerPaginationHistorySumV2,
  ControllerAddNode,
  ControllerHistoryNode,
  ControllerHistoryNodev2,
  ControllerHistoryFilterSum,
  ControllerListNode,
  ControllerListNodeV2,
  ControllerCheckNode,
  ControllerRegisterNode,
  ControllerListUnassignedNode,
  ControllerAssignedNode,
  ControllerUpdateIstallation,
  ControllerDeleteIstallation,
  ControllerUnassignedNode,
  ControllerAdjustBalace,
  controllerPaginationHistoryMobile,
  controllerPaginationHistorySumV2Mobile,
  ControllerListPrepaidNode,
  ControllerListNodeLink,
  ControllerSaveNodeLink,
  ControllerNodeUnlink,
} = require("../controllers/controller_node");
const {
  controllerInternalGet,
  controllerInternalRegister,
  controllerInternalEdit,
} = require("../controllers/controller_internal");

const {
  validasiRegisterArea,
  validasiRegisterCompany,
  validasiRegisterTenant,
} = require("../middlewares/users_validasi");
const { validasiProfileCompany } = require("../middlewares/company_validasi");
const { validasiProfileArea } = require("../middlewares/area_validasi");
const { validasiGetTenant } = require("../middlewares/tenant_validasi");
const {
  validasiGetHistoryNode,
  validasiGetNode,
  validasiAssignedNode,
  validasiUnsignedNode,
  validasiGetUnsignedNode,
} = require("../middlewares/node_validaasi");
const { validasiAddGateway } = require("../middlewares/add_gateways");
const {
  controllerGetMember,
  controllerSetMember,
  controllerUpdateMember,
  controllerDeleteMember,
} = require("../controllers/controller_member_level");

const {
  ControllerCompanyProfile,
  ControllerCompanyProfileEdit,
  controllerCompaniesRegister,
  ControllerCompanyPasswordEdit,
  ControllerAreaPasswordEdit,
} = require("../controllers/controller_company");
const { ControllerUploadImages } = require("../controllers/controller_images");
const {
  controllerGatewayRegister,
  controllerGatewayEdit,
  controllerGatewayGet,
  ControllerCheckGateway,
  controllerGatewayDelete,
} = require("../controllers/controller_gateways");
const {
  controllerGetDeviceType,
} = require("../controllers/controller_device_type");
const {
  ctrlCoreNodeHistoryApi,
  ctrlCoreNodeHistoryNonMeterApi,
} = require("../controllers/core/controller_core_api");
const {
  controllerGetAppName,
  controllerRtuEditBilling,
  controllerBillingDenda,
} = require("../controllers/controller_rtu_appname");
const {
  ctrlSendMail,
} = require("../controllers/billing/controller_send_email");

const {
  controllerHistoryFilter,
  controllerHistoryRangeFilter,
} = require("../controllers/controller_date_filter");

const {
  validasiRegisterInternal,
  validasiGetInternal,
} = require("../middlewares/internal_validasi");
/* GET users listing. */

const models = require("../models/index");
const Op = models.Sequelize.Op;
const moment = require("moment");
const fs = require("fs");

var storage = multer.diskStorage({
  destination: function (req, file, callback) {
    let path = `./attachment/${req.params.id}`;
    fs.mkdirSync(path, { recursive: true });
    callback(null, path);
  },
  filename: function (req, file, callback) {
    callback(null, req.params.id.toString() + file.originalname);
  },
});
var limits = {
  fileSize: 50024 * 50024, // 1 MB (max file size)
};

var upload = multer({ storage: storage, limits: limits });

router
  .post("/push/node/history", ctrlCoreNodeHistoryApi)
  .post("/push/node/history/non/meter", ctrlCoreNodeHistoryNonMeterApi);

module.exports = router;

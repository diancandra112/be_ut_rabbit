const express = require("express");
const moment = require("moment");
const router = express.Router();
const models = require("../models/index");
const { Op } = models.Sequelize;
const { AuthArea, AuthCompanyArea } = require("../middlewares/jwt_auth");
/* GET users listing. */

router.get("/last/meter/:devEui", async (req, res) => {
  try {
    transaction = await models.sequelize.transaction();
    let token = req.headers.authorization;
    if (
      token !=
      "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6ImtpayIsImlhdCI6MTUxNjIzOTAyMn0.ltnjv3CMR51jjepftdSiYKmPuq7YnwMo3cmYrjmFj5E"
    ) {
      return res
        .status(401)
        .json({ responseCode: 401, message: "Invalid Token" });
    }
    let list = await models.iot_nodes.findOne({
      include: [
        {
          model: models.iot_node_type,
          attributes: ["type_name", "type_id", "satuan"],
        },
      ],
      attributes: ["devEui", "last_update", "live_last_meter", "typeId"],
      where: { devEui: req.params.devEui, areaId: 63, typeId: 2 },
      transaction,
    });
    let output = [];
    list = JSON.parse(JSON.stringify(list));
    if (list == null) {
      return res
        .status(400)
        .json({ responseCode: 400, message: "DevEui Not Found" });
    } else {
      if (list.last_update != null || list.live_last_meter != null) {
        list.last_update = moment(list.last_update)
          .utcOffset("+0700")
          .format("YYYY-MM-DD HH:mm:ss");
        list.live_last_meter = parseFloat(
          parseFloat(list.live_last_meter) / 1000
        );
      }
      return res
        .status(200)
        .json({ responseCode: 200, message: "Success Get Data", data: list });
    }
    return console.log(list);
    list.map((val) => {
      console.log(val);
      if ([1, 2, 3, 7].includes(val.typeId)) {
        if (val.iots_charts_usages.length > 0) {
          output.push({
            name: val.iot_tenant.tenant_name,
            address: val.iot_tenant.address,
            date: val.iots_charts_usages[0].fdate,
            // date_time: moment(val.iots_charts_usages[0].fdate)
            //   .utcOffset("+0700")
            //   .format("YYYY-MM-DD HH:mm:ss"),
            totalizer: parseFloat(val.iots_charts_usages[0].endMeter) / 1000,
            devui: val.devEui,
            node_type: val.iot_node_type.type_name,
          });
        }
      } else {
        output.push({
          name: val.iot_tenant.tenant_name,
          address: val.iot_tenant.address,
          date: val.last_update,
          // date_time: moment(val.last_update)
          //   .utcOffset("+0700")
          //   .format("YYYY-MM-DD HH:mm:ss"),
          totalizer: val.live_last_meter,
          devui: val.devEui,
          node_type: val.iot_node_type.type_name,
        });
      }
    });
    await transaction.commit();
    return res.status(200).json({
      responseCode: 200,
      message: "Sukses Get Log List",
      data: output,
    });
  } catch (error) {
    console.log(error);
    if (transaction) await transaction.rollback();
    return res.status(500).json({ responseCode: 500, message: error.message });
  }
});
// .get("/last/meter/:devEui", async (req, res) => {
//   try {
//     transaction = await models.sequelize.transaction();
//     let token = req.headers.authorization;
//     if (
//       token !=
//       "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6ImtpayIsImlhdCI6MTUxNjIzOTAyMn0.ltnjv3CMR51jjepftdSiYKmPuq7YnwMo3cmYrjmFj5E"
//     ) {
//       return res
//         .status(401)
//         .json({ responseCode: 401, message: "Invalid Token" });
//     }
//     return console.log(req.params);
//     // if (req.query.date == null) {
//     //   return res
//     //     .status(403)
//     //     .json({ responseCode: 403, message: "date Requered" });
//     // }
//     // let date = null;
//     // try {
//     //   date = moment(req.query.date).format("YYYY-MM-DD");
//     // } catch (error) {
//     //   return res
//     //     .status(403)
//     //     .json({ responseCode: 403, message: "Invalid Date" });
//     // }
//     let filter = {};
//     if (req.query.tenantId) {
//       filter = { tenantId: req.query.tenantId };
//     }
//     if (req.query.devEui) {
//       filter = { devEui: req.query.devEui };
//     }
//     let list = await models.iot_nodes.findAll({
//       attributes: ["devEui", "last_update", "live_last_meter", "typeId"],
//       where: [
//         filter,
//         {
//           areaId: 70,
//           is_unsigned: false,
//           tenantId: { [Op.not]: null },
//           // live_last_meter: { [Op.not]: null },
//           // last_update: {
//           //   [Op.and]: [
//           //     { [Op.not]: null },
//           //     models.sequelize.where(
//           //       models.sequelize.fn(
//           //         "date",
//           //         models.sequelize.col("iot_nodes.last_update")
//           //       ),
//           //       "=",
//           //       date
//           //     ),
//           //   ],
//           // },
//         },
//       ],
//       include: [
//         {
//           model: models.iots_charts_usage,
//           limit: 1,
//           where: { fdate: date },
//           order: [["fhour", "DESC"]],
//         },
//         { model: models.iot_tenant, attributes: ["tenant_name", "address"] },
//         { model: models.iot_node_type, attributes: ["type_name"] },
//       ],
//       order: [["iot_tenant", "tenant_name", "ASC"]],
//       transaction,
//     });
//     let output = [];
//     list = JSON.parse(JSON.stringify(list));
//     list.map((val) => {
//       console.log(val);
//       if ([1, 2, 3, 7].includes(val.typeId)) {
//         if (val.iots_charts_usages.length > 0) {
//           output.push({
//             name: val.iot_tenant.tenant_name,
//             address: val.iot_tenant.address,
//             date: val.iots_charts_usages[0].fdate,
//             // date_time: moment(val.iots_charts_usages[0].fdate)
//             //   .utcOffset("+0700")
//             //   .format("YYYY-MM-DD HH:mm:ss"),
//             totalizer: parseFloat(val.iots_charts_usages[0].endMeter) / 1000,
//             devui: val.devEui,
//             node_type: val.iot_node_type.type_name,
//           });
//         }
//       } else {
//         output.push({
//           name: val.iot_tenant.tenant_name,
//           address: val.iot_tenant.address,
//           date: val.last_update,
//           // date_time: moment(val.last_update)
//           //   .utcOffset("+0700")
//           //   .format("YYYY-MM-DD HH:mm:ss"),
//           totalizer: val.live_last_meter,
//           devui: val.devEui,
//           node_type: val.iot_node_type.type_name,
//         });
//       }
//     });
//     await transaction.commit();
//     return res.status(200).json({
//       responseCode: 200,
//       message: "Sukses Get Log List",
//       data: output,
//     });
//   } catch (error) {
//     console.log(error);
//     if (transaction) await transaction.rollback();
//     return res
//       .status(500)
//       .json({ responseCode: 500, message: error.message });
//   }
// });

module.exports = router;

var express = require("express");
var router = express.Router();
const { controllerMidtransCallback } = require("../controllers/midtrans");
/* GET home page. */
router
  .get("/flag", function (req, res, next) {
    res.send("11/12/2021");
  })
  .post("/callback/payment", controllerMidtransCallback)
  .post("/charge/payment", controllerMidtransCallback)
  .post("/cancel/payment", controllerMidtransCallback);

module.exports = router;

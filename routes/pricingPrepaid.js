const express = require("express");
const router = express.Router();
const fs = require("fs");
const {
  ctrlCreatePrepaid,
  ctrlListType,
  ctrlGetPrepaid,
  ctrlEditPrepaid,
  ctrlDeletePrepaid,
  ctrlTransactionPrepaidAdd,
  ctrlTransactionPrepaidGet,
} = require("../controllers/pricingPrepaid/controller_prepaid");
const {
  AuthArea,
  AuthAreaTenant,
  AuthCompanyArea,
} = require("../middlewares/jwt_auth");
/* GET users listing. */

router
  .post("/transaction", AuthAreaTenant, ctrlTransactionPrepaidAdd)
  .get("/transaction", AuthAreaTenant, ctrlTransactionPrepaidGet)

  .post("/", AuthCompanyArea, ctrlCreatePrepaid)
  .get("/:type", AuthCompanyArea, ctrlGetPrepaid)
  .patch("/", AuthCompanyArea, ctrlEditPrepaid)
  .delete("/:id", AuthCompanyArea, ctrlDeletePrepaid)
  .get("/list/type", AuthCompanyArea, ctrlListType);
module.exports = router;

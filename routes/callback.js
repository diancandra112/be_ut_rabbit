const express = require("express");
const router = express.Router();
const fs = require("fs");
const {
  controllerCallbackFasapay,
} = require("../controllers/controller_callback");
const { AuthArea } = require("../middlewares/jwt_auth");
/* GET users listing. */
const models = require("../models/index");
const { Op } = models.Sequelize;
router
  .post("/fasapay", controllerCallbackFasapay)
  .post("/fasapay/static", (req, res) => {
    console.log(req.body);
    models.iots_billing_prepaid
      .findOne({
        where: {
          [Op.or]: {
            va_bca: req.body.va_static,
            va_permata: req.body.va_static,
          },
          status: "UNPAID",
        },
        order: [["id", "DESC"]],
      })
      .then((resultPrepaid) => {
        resultPrepaid = JSON.parse(JSON.stringify(resultPrepaid));
        if (resultPrepaid == null) {
          models.iots_billing_history
            .findOne({
              where: {
                virtual_account: { [Op.substring]: req.body.va_static },
              },
              include: [
                { model: models.iot_tenant },
                { model: models.iot_area },
              ],
            })
            .then((response) => {
              response = JSON.parse(JSON.stringify(response));
              console.log(response);
              if (response == null) {
                res.json({
                  response: "VA Static Response",
                  va_number: req.body.va_static,
                  amount: "-",
                  cust_name: "-",
                  response_code: "14",
                });
              } else if (response.status == "PAID") {
                res.json({
                  response: "VA Static Response",
                  va_number: req.body.va_static,
                  amount: "-",
                  cust_name: "-",
                  response_code: "81",
                });
              } else if (response.status == "CANCEL") {
                res.json({
                  response: "VA Static Response",
                  va_number: req.body.va_static,
                  amount: "-",
                  cust_name: "-",
                  response_code: "17",
                });
              } else if (response.status == "UNPAID") {
                let biaya_penyesuaian = parseFloat(response.biaya_penyesuaian);
                let billing_usage = 0;
                if (response.node_type == "RTU") {
                  billing_usage = parseFloat(response.billing_usage);
                } else {
                  billing_usage = parseFloat(response.billing_usage) / 1000;
                }
                let amount = parseInt(
                  (
                    parseFloat(billing_usage) *
                    parseFloat(response.harga_satuan)
                  ).toFixed(4)
                );
                let _denda = Array.isArray(JSON.parse(response.denda))
                  ? JSON.parse(response.denda)
                  : [];

                let total_denda = 0;
                _denda.map((valDenda, idx2) => {
                  total_denda += parseInt(valDenda.jumlah_denda);
                });
                let denda = total_denda;
                let materai = response.materai;
                let amount_pajak = parseInt(
                  amount * (parseInt(response.ppn) / 100)
                );
                let sub_total = amount + amount_pajak;
                let total = sub_total + denda + materai;
                let grandtotal =
                  total + biaya_penyesuaian + response.biaya_transaksi;
                grandtotal = parseInt(grandtotal);
                if (req.body.type == "payment") {
                  if (req.body.amount == grandtotal) {
                    res.json({
                      response: "VA Static Response",
                      va_number: req.body.va_static,
                      amount: grandtotal.toString(),
                      cust_name:
                        response.billing_type == "area"
                          ? response.iot_area.area_name
                          : response.iot_tenant.tenant_name,
                      response_code: "00",
                    });
                  } else {
                    res.json({
                      response: "VA Static Response",
                      va_number: req.body.va_static,
                      amount: grandtotal,
                      cust_name:
                        response.billing_type == "area"
                          ? response.iot_area.area_name
                          : response.iot_tenant.tenant_name,
                      response_code: "13",
                    });
                  }
                } else {
                  res.json({
                    response: "VA Static Response",
                    va_number: req.body.va_static,
                    amount: grandtotal.toString(),
                    cust_name:
                      response.billing_type == "area"
                        ? response.iot_area.area_name
                        : response.iot_tenant.tenant_name,
                    response_code: "00",
                  });
                }
              } else {
                res.json({
                  response: "VA Static Response",
                  va_number: req.body.va_static,
                  amount: "-",
                  cust_name: "-",
                  response_code: "14",
                });
              }
              // console.log(response);
            })
            .catch((error) => {
              console.log(error);
            });
        } else {
          res.json({
            response: "VA Static Response",
            va_number: req.body.va_static,
            amount: resultPrepaid.product_price.toString(),
            cust_name: resultPrepaid.tenant_name,
            response_code: "00",
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  });
module.exports = router;

const express = require("express");
const router = express.Router();

const {
  CtrlRegisterLine,
  CtrlEditLine,
  CtrlDeleteLine,
  CtrlGetLine,
} = require("../controllers/controller_line");
const { AuthArea, AuthCompanyArea } = require("../middlewares/jwt_auth");
const axios = require("axios");
const models = require("../models/index");
/* GET users listing. */
router
  .post("/area/check", AuthArea, (req, res) => {
    try {
      let url =
        req.query.url_erp +
        `/auth/login?email=${req.query.email_erp}&password=${req.query.pass_erp}`;

      return axios
        .post(url)
        .then((response) => {
          res.status(200).json({ status: 200, message: "ok" });
        })
        .catch((error) => {
          console.log(error);
          res.status(500).json({ status: 500, message: error.message });
        });
    } catch (error) {
      console.log(error);
      res.status(500).json({ status: 500, message: error.message });
    }
  })
  .get("/email/check/:billingId/:tenantId", AuthArea, (req, res) => {
    try {
      let data = { areaId: res.locals.decoded.id };
      models.iot_area
        .findOne({
          where: { id: data.areaId },
          attributes: { exclude: ["image"] },
          include: [
            {
              model: models.iots_erp_item,
              attributes: { exclude: ["createdAt", "updatedAt"] },
            },
            { model: models.iot_pricing_postpaid_all },
            { model: models.iot_pricing_billing_area },
            {
              model: models.iots_billing_history,
              as: "history_billing",
              where: { id: req.params.billingId },
              include: [{ model: models.iot_nodes }],
            },
          ],
        })
        .then((response_area) => {
          response_area = JSON.parse(JSON.stringify(response_area));
          if (response_area.is_erp == false) {
            return res.status(200).json({ status: 200, message: "ok" });
          } else {
            if (
              response_area.url_erp == null ||
              response_area.email_erp == null ||
              response_area.pass_erp == null
            ) {
              return res.status(500).json({
                status: 500,
                message: "Lengkapi Area Setup Untuk Integrasi Ke Erp",
              });
            } else if (
              (response_area.history_billing.is_combine_billing == true ||
                response_area.history_billing.billing_type == "node") &&
              (response_area.erp_name == null ||
                response_area.erp_id == null ||
                response_area.erp_contract_name == null ||
                response_area.erp_contract_id == null)
            ) {
              return res.status(500).json({
                status: 500,
                message:
                  "Lengkapi Area Setup Erp Customer Dan Contract Untuk Integrasi Ke Erp",
              });
            } else if (response_area.iots_erp_item == null) {
              return res.status(500).json({
                status: 500,
                message: "Lengkapi Area Setup Erp Item Untuk Integrasi Ke Erp",
              });
            } else {
              let ok = true;
              Object.keys(response_area.iots_erp_item).map((val) => {
                if (response_area.iots_erp_item[val] == null) {
                  ok = false;
                }
              });
              if (ok) {
                if (response_area.history_billing.billing_type == "node") {
                  if (
                    response_area.saas_item_id == null ||
                    response_area.saas_unit_id == null
                  )
                    throw new Error("Lengkapi Item di Pricing Area");

                  return res.status(200).json({ status: 200, message: "ok" });
                } else if (
                  response_area.history_billing.is_combine_billing == true
                ) {
                  let temp_post =
                    response_area.iot_pricing_billing_areas.filter((valP) => {
                      return (
                        valP.typeId == response_area.history_billing.typeId
                      );
                    });
                  if (
                    temp_post[0] == undefined ||
                    temp_post[0].item_id == null ||
                    temp_post[0].unit_id == null
                  )
                    throw new Error("Lengkapi Item di Pricing Area");

                  return res.status(200).json({ status: 200, message: "ok" });
                } else {
                  models.iot_tenant
                    .findOne({
                      where: { id: req.params.tenantId },
                      include: [
                        {
                          model: models.iot_member_level,
                          include: [
                            { model: models.iot_pricing_postpaid, as: "child" },
                          ],
                        },
                      ],
                    })
                    .then((response_tenat) => {
                      response_tenat = JSON.parse(
                        JSON.stringify(response_tenat)
                      );
                      if (
                        response_tenat.erp_name == null ||
                        response_tenat.erp_id == null ||
                        response_tenat.erp_contract_name == null ||
                        response_tenat.erp_contract_id == null
                      ) {
                        return res.status(500).json({
                          status: 500,
                          message:
                            "Lengkapi Customer Di Tenant Untuk Integrasi Ke Erp",
                        });
                      } else {
                        if (response_area.pricing_option == 1) {
                          let temp_post = [];
                          if (response_area.history_billing.typeId == 4) {
                            temp_post =
                              response_area.iot_pricing_postpaid_alls.filter(
                                (valP) => {
                                  return (
                                    valP.typeId ==
                                    response_area.history_billing.iot_node
                                      .rtu_pricing_id
                                  );
                                }
                              );
                          } else {
                            temp_post =
                              response_area.iot_pricing_postpaid_alls.filter(
                                (valP) => {
                                  return (
                                    valP.typeId ==
                                    response_area.history_billing.typeId
                                  );
                                }
                              );
                          }
                          if (
                            temp_post[0] == undefined ||
                            temp_post[0].item_id == null ||
                            temp_post[0].unit_id == null
                          )
                            throw new Error("Lengkapi Item di Pricing Area");
                        } else {
                          let temp_post2 = [];
                          if (response_area.history_billing.typeId == 4) {
                            temp_post2 =
                              response_tenat.iot_member_level.child.filter(
                                (valP) => {
                                  return (
                                    valP.typeId ==
                                    response_area.history_billing.iot_node
                                      .rtu_pricing_id
                                  );
                                }
                              );
                          } else {
                            temp_post2 =
                              response_tenat.iot_member_level.child.filter(
                                (valP) => {
                                  return (
                                    valP.typeId ==
                                    response_area.history_billing.typeId
                                  );
                                }
                              );
                          }
                          if (
                            temp_post2[0] == undefined ||
                            temp_post2[0].item_id == null ||
                            temp_post2[0].unit_id == null
                          )
                            throw new Error("Lengkapi Item di Pricing Area");
                        }
                        return res
                          .status(200)
                          .json({ status: 200, message: "ok" });
                      }
                    })
                    .catch((error) =>
                      res
                        .status(500)
                        .json({ status: 500, message: error.message })
                    );
                }
              } else {
                return res.status(500).json({
                  status: 500,
                  message:
                    "Lengkapi Area Setup Erp Item Untuk Integrasi Ke Erp",
                });
              }
            }
          }
        })
        .catch((error) => {
          console.log(error);
          res.status(500).json({ status: 500, message: error.message });
        });
    } catch (error) {
      console.log(error);
      res.status(500).json({ responseCode: 500, message: error.message });
    }
  })
  .post("/login", AuthCompanyArea, (req, res) => {
    try {
      const { role } = res.locals.decoded;
      let id = {};
      if (role === "AREA") {
        id = { id: res.locals.decoded.iot_area.id };
      } else {
        if (req.query.area_id == null) throw new Error("request area_id");
        id = { id: req.query.area_id };
      }
      models.iot_area
        .findOne({
          where: id,
          attributes: ["url_erp", "email_erp", "pass_erp"],
        })
        .then((_result) => {
          _result = JSON.parse(JSON.stringify(_result));
          console.log(_result);
          let url = _result.url_erp;
          url =
            url.split("api")[0] +
            `api/auth/login?email=${_result.email_erp}&password=${_result.pass_erp}`;
          return axios
            .post(url)
            .then((response) => res.status(200).json(response.data))
            .catch((error) => res.status(500).json(error.response.data));
        });
    } catch (error) {
      console.log(error);
      res.status(500).json({ responseCode: 500, message: error.message });
    }
  })
  .post("/master/customer", AuthCompanyArea, (req, res) => {
    try {
      let data = req.body;
      data.url = data.url.split("api")[0] + "api/v1/master/customer";
      return axios({
        method: "GET",
        url: data.url,
        params: {
          active: 1,
          keyword: data.searchValue,
        },
        headers: {
          Authorization: data.token,
        },
      })
        .then((response) => res.status(200).json(response.data))
        .catch((error) => res.status(500).json(error.response.data));
    } catch (error) {
      console.log(error);
      res.status(500).json({ responseCode: 500, message: error.message });
    }
  })
  .get("/master/bank", AuthArea, (req, res) => {
    try {
      let data = req.query;
      models.iot_area
        .findOne({
          attributes: ["url_erp", "email_erp", "pass_erp"],
          where: { id: res.locals.decoded.iot_area.id },
        })
        .then((response) => {
          response = JSON.parse(JSON.stringify(response));
          let url =
            response.url_erp.split("api")[0] +
            `api/auth/login?email=${response.email_erp}&password=${response.pass_erp}`;
          return axios
            .post(url)
            .then((response_login) => {
              return axios({
                method: "GET",
                url: response.url_erp.split("api")[0] + "api/v1/master/banks",
                params: {
                  active: 1,
                  keyword: data.searchValue,
                },
                headers: {
                  Authorization: "Bearer " + response_login.data.access_token,
                },
              })
                .then((response_final) =>
                  res.status(200).json(response_final.data)
                )
                .catch((error) => res.status(500).json(error.response.data));
            })
            .catch((error) => res.status(500).json(error.response.data));
        })
        .catch((error) => res.status(500).json(error));
    } catch (error) {
      console.log(error);
      res.status(500).json({ responseCode: 500, message: error.message });
    }
  })
  .get("/master/contract", AuthCompanyArea, (req, res) => {
    try {
      const { role } = res.locals.decoded;
      let id = {};
      if (role === "AREA") {
        id = { id: res.locals.decoded.iot_area.id };
      } else {
        if (req.query.area_id == null) throw new Error("request area_id");
        id = { id: req.query.area_id };
      }
      let data = req.query;
      let customer_id = {};
      if (req.query.customer_id != undefined) {
        customer_id = { customer_id: req.query.customer_id };
      }
      models.iot_area
        .findOne({
          attributes: ["url_erp", "email_erp", "pass_erp"],
          where: id,
        })
        .then((response) => {
          response = JSON.parse(JSON.stringify(response));
          let url =
            response.url_erp.split("api")[0] +
            `api/auth/login?email=${response.email_erp}&password=${response.pass_erp}`;
          return axios
            .post(url)
            .then((response_login) => {
              return axios({
                method: "GET",
                url: response.url_erp.split("api")[0] + "api/v1/pi/contracts",
                params: {
                  active: 1,
                  keyword: data.searchValue,
                  ...customer_id,
                },
                headers: {
                  Authorization: "Bearer " + response_login.data.access_token,
                },
              })
                .then((response_final) =>
                  res.status(200).json(response_final.data)
                )
                .catch((error) => {
                  console.log(error);
                  res.status(500).json(error.response.data);
                });
            })
            .catch((error) => {
              console.log(error);
              res.status(500).json(error.response.data);
            });
        })
        .catch((error) => res.status(500).json(error));
    } catch (error) {
      console.log(error);
      res.status(500).json({ responseCode: 500, message: error.message });
    }
  })
  .post("/master/items", AuthCompanyArea, (req, res) => {
    try {
      let data = req.body;
      data.url = data.url.split("api")[0] + "api/v1/master/items";
      return axios({
        method: "GET",
        url: data.url,
        params: {
          active: 1,
          keyword: data.searchValue,
        },
        headers: {
          Authorization: data.token,
        },
      })
        .then((response) => res.status(200).json(response.data))
        .catch((error) => res.status(500).json(error.response.data));
    } catch (error) {
      console.log(error);
      res.status(500).json({ responseCode: 500, message: error.message });
    }
  })
  .get("/master/items", AuthCompanyArea, (req, res) => {
    try {
      let data = req.query;
      const { role } = res.locals.decoded;
      let id = {};
      if (role === "AREA") {
        id = { id: res.locals.decoded.iot_area.id };
      } else {
        if (req.query.area_id == null) throw new Error("request area_id");
        id = { id: req.query.area_id };
      }
      models.iot_area
        .findOne({
          attributes: ["url_erp", "email_erp", "pass_erp"],
          where: id,
        })
        .then((response) => {
          response = JSON.parse(JSON.stringify(response));
          let url =
            response.url_erp.split("api")[0] +
            `api/auth/login?email=${response.email_erp}&password=${response.pass_erp}`;
          return axios
            .post(url)
            .then((response_login) => {
              return axios({
                method: "GET",
                url: response.url_erp.split("api")[0] + "api/v1/master/items",
                params: {
                  active: 1,
                  keyword: data.searchValue,
                },
                headers: {
                  Authorization: "Bearer " + response_login.data.access_token,
                },
              })
                .then((response_final) =>
                  res.status(200).json(response_final.data)
                )
                .catch((error) => res.status(500).json(error.response.data));
            })
            .catch((error) => res.status(500).json(error.response.data));
        })
        .catch((error) => res.status(500).json(error));
    } catch (error) {
      console.log(error);
      res.status(500).json({ responseCode: 500, message: error.message });
    }
  })
  .get("/master/coa", AuthCompanyArea, (req, res) => {
    try {
      let data = req.query;
      const { role } = res.locals.decoded;
      let id = {};
      if (role === "AREA") {
        id = { id: res.locals.decoded.iot_area.id };
      } else {
        if (req.query.area_id == null) throw new Error("request area_id");
        id = { id: req.query.area_id };
      }
      models.iot_area
        .findOne({
          attributes: ["url_erp", "email_erp", "pass_erp"],
          where: id,
        })
        .then((response) => {
          response = JSON.parse(JSON.stringify(response));
          let url =
            response.url_erp.split("api")[0] +
            `api/auth/login?email=${response.email_erp}&password=${response.pass_erp}`;
          return axios
            .post(url)
            .then((response_login) => {
              return axios({
                method: "GET",
                url: response.url_erp.split("api")[0] + "api/v1/master/coas",
                params: {
                  active: 1,
                  keyword: data.searchValue,
                  bermutasi: 1,
                },
                headers: {
                  Authorization: "Bearer " + response_login.data.access_token,
                },
              })
                .then((response_final) =>
                  res.status(200).json(response_final.data)
                )
                .catch((error) => res.status(500).json(error.response.data));
            })
            .catch((error) => res.status(500).json(error.response.data));
        })
        .catch((error) => res.status(500).json(error));
    } catch (error) {
      console.log(error);
      res.status(500).json({ responseCode: 500, message: error.message });
    }
  });
module.exports = router;

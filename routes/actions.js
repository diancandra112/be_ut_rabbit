const express = require("express");
const router = express.Router();

const {
  AuthArea,
  AuthNotRequered,
  AuthCompanyAreaTeknisi,
  AuthActionRequered,
} = require("../middlewares/jwt_auth");
const models = require("../models/index");
const { Op } = models.Sequelize;
const {
  CtrlSetInterval,
} = require("../controllers/actions/controller_interval");
const {
  CtrlEditMeter,
} = require("../controllers/actions/controller_Edit_meter");
const {
  CtrlSetValve,
  CtrlSetValve2,
} = require("../controllers/actions/controller_valve");
const {
  CtrlSetMeter,
  CtrlSetUtc,
  CtrlSetPulse,
} = require("../controllers/actions/controller_meter");
const { CtrlSetTopup } = require("../controllers/actions/controller_topup");
const { CtrlGetLog } = require("../controllers/actions/controller_log_action");
const {
  CtrlSetBrightness,
} = require("../controllers/actions/controller_brightness");

router
  .post("/coba/auth", AuthActionRequered, CtrlSetValve2)
  .patch("/edit/meter/:node_id", AuthActionRequered, CtrlEditMeter)
  .post("/node/interval/:devEUI/:interval", AuthActionRequered, CtrlSetInterval)
  .post("/node/valve/:devEUI/:valve", AuthActionRequered, CtrlSetValve)
  .post(
    "/node/brightness/:devEUI/:value",
    AuthActionRequered,
    CtrlSetBrightness
  )
  .post("/node/topup/:devEUI/:topup", AuthActionRequered, CtrlSetTopup)
  .post("/node/meter/:devEUI/:set_meter", AuthActionRequered, CtrlSetMeter)
  .post("/node/utc/:devEUI", AuthActionRequered, CtrlSetUtc)
  .post(
    "/node/pulse/:devEUI/:pulse/:pulse_count",
    AuthActionRequered,
    CtrlSetPulse
  )

  .get("/logs", AuthCompanyAreaTeknisi, CtrlGetLog)
  .get("/change/tenant", (req, res, next) => {
    models.iot_area
      .findAll({
        where: { image: { [Op.substring]: "http://103.65.237.194:3005/" } },
      })
      .then((resa) => {
        resa = JSON.parse(JSON.stringify(resa));
        resa.map((val, idx) => {
          val.image = val.image.split("http://103.65.237.194:3005/");
          resa[idx].image = "https://com.be.wiraenergi.co.id/" + val.image[1];
        });
        // res.json(resa);
        models.iot_area
          .bulkCreate(resa, {
            updateOnDuplicate: ["image"],
          })
          .then((a) => {
            res.json(a);
          });
      });
  });
module.exports = router;

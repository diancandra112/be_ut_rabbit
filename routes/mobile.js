const express = require("express");
const router = express.Router();
var multer = require("multer");

const {
  controllerUserMobileLogin,
  controllerTeknisiMobileLogin,
  controllerTeknisiCompanyMobileLogin,
  controllerTeknisiCompanyMobileLoginGet,
  controllerListMobileVoucher,
  controllerTeknisiFcmToken,
  controllerTeknisiLogOut,
  controllerTenantLogOut,
  controllerMobileTenantNodeList,
} = require("../controllers/mobile/Auth");
const {
  controllerListBilling,
  controllerListBillingDetail,
} = require("../controllers/mobile/billing_tenant_apps");
const {
  ctrlGetBilling,
} = require("../controllers/billing/mobile_customer/controller_billing");
const {
  AuthTenant,
  AuthTeknisi,
  AuthTeknisiAndCom,
} = require("../middlewares/jwt_auth");

var storage = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, "./images/tiketing");
  },
  filename: function (req, file, callback) {
    callback(null, Date.now() + "-" + file.originalname);
  },
});

var upload = multer({ storage: storage });

const {
  CtrlUsageMonth,
  CtrlUsageActivity,
} = require("../controllers/mobile/controller_usage_month");
const {
  CtrlAddTiketing,
  CtrlGetTiketing,
} = require("../controllers/tiketing/mobile_customer/controller_tiketing");
const {
  CtrlGetTeknisiNodeType,
} = require("../controllers/mobile/teknisi/controller_node_type");
const {
  CtrlGetTeknisiNodeList,
  CtrlGetTeknisiNodeDetails,
} = require("../controllers/mobile/teknisi/controller_node_list");
const {
  CtrlGetTeknisiTiketList,
  CtrlEditTeknisiTiketStatus,
  CtrlGetTeknisiTiketDetail,
  CtrlTeknisiTiketMaintenanceSave,
  CtrlTeknisiTiketInstallationSave,
} = require("../controllers/mobile/teknisi/controller_teknisi_tiket");
/* GET users listing. */
router
  .post("/teknisi/login", controllerTeknisiMobileLogin)
  .post("/teknisi/company/login", controllerTeknisiCompanyMobileLogin)
  .post("/teknisi/fcm/:fcm_token", AuthTeknisiAndCom, controllerTeknisiFcmToken)
  .get("/teknisi/node/type", AuthTeknisiAndCom, CtrlGetTeknisiNodeType)
  .get("/teknisi/node/list/:typeId", AuthTeknisiAndCom, CtrlGetTeknisiNodeList)
  .get("/teknisi/node/details/:devEui", CtrlGetTeknisiNodeDetails)
  .get(
    "/teknisi/tiket/list/:ticket_type",
    AuthTeknisiAndCom,
    CtrlGetTeknisiTiketList
  )
  .get(
    "/teknisi/tiket/detail/:id",
    AuthTeknisiAndCom,
    CtrlGetTeknisiTiketDetail
  )
  .patch(
    "/teknisi/tiket/:id/:status",
    AuthTeknisiAndCom,
    CtrlEditTeknisiTiketStatus
  )
  .post(
    "/teknisi/tiket/maintenance/:id",
    AuthTeknisiAndCom,
    upload.array("image"),
    CtrlTeknisiTiketMaintenanceSave
  )
  .post(
    "/teknisi/tiket/installation/:id/:save",
    AuthTeknisiAndCom,
    upload.single("image"),
    CtrlTeknisiTiketInstallationSave
  )

  .get("/tenant/billing", AuthTenant, controllerListBilling)
  .get("/tenant/billing/:id", AuthTenant, controllerListBillingDetail)

  .post("/tenant/logout", AuthTenant, controllerTenantLogOut)
  .post("/teknisi/logout", AuthTeknisiAndCom, controllerTeknisiLogOut)
  .post("/tenant/login", controllerUserMobileLogin)
  .get("/mobile/list/voucher/:type", AuthTenant, controllerListMobileVoucher)
  .get(
    "/mobile/node/list/:tenantId",
    AuthTeknisiAndCom,
    controllerMobileTenantNodeList
  )
  .post("/tenant/tiket", AuthTenant, upload.array("image"), CtrlAddTiketing)
  .get("/tenant/tiket", AuthTenant, CtrlGetTiketing)
  .get("/tenant/billing", AuthTenant, ctrlGetBilling)
  .get("/usage/month/:device_id/:typeId", CtrlUsageMonth)
  .get("/usage/activity/:device_id/:typeId/:from/:to", CtrlUsageActivity);

module.exports = router;

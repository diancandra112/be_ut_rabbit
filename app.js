var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const cors = require("cors");
var bodyParser = require("body-parser");
var callbackRouter = require("./routes/callback");
var indexRouter = require("./routes/index");
var indexYukkQris = require("./routes/yukk_qris");
var usersRouter = require("./routes/users");
var historyRouter = require("./routes/history");
var lineRouter = require("./routes/line");
var usersImages = require("./routes/images");
var usersMobile = require("./routes/mobile");
var apiNode = require("./routes/apiNode");
var emailRouter = require("./routes/email");
var billingRouter = require("./routes/billing");
var actionRouter = require("./routes/actions");
var pricingPrepaidRouter = require("./routes/pricingPrepaid");
var dashboardRouter = require("./routes/dashboard");
var areaUserRouter = require("./routes/areaUsers");
var tiketingRouter = require("./routes/tiketing");
var testingRouter = require("./routes/testing");
var erpRouter = require("./routes/erp");
var erpCsv = require("./routes/cvs/index");
var lloydRouter = require("./routes/lloyd");
var kikRouter = require("./routes/kik");
var health_checkRouter = require("./routes/health_check");

var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "jade");

// app.use(cors());
// app.use(logger("dev"));
// app.use(express.json());
// app.use(express.urlencoded({ extended: false }));
// app.use(express());
// app.use(express.static(path.join(__dirname, "public")));

app.use(logger("dev"));
app.use(bodyParser.json({ limit: "1mb" }));
app.use(bodyParser.urlencoded({ limit: "1mb", extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(cors());

// app.use("/api", historyRouter);
app.use("/api/csv", erpCsv);
app.use("/api/iki/qris", indexYukkQris);
app.use("/midtrans", indexRouter);
app.use("/callback", callbackRouter);
app.use("/api", usersRouter);
app.use("/api/line", lineRouter);
app.use("/api/pricing/prepaid", pricingPrepaidRouter);
app.use("/api/dashboard", dashboardRouter);
app.use("/api/action", actionRouter);
app.use("/api", usersMobile);
app.use("/api/water/level", apiNode);
app.use("/", usersImages);
app.use("/api/email", emailRouter);
app.use("/billing", billingRouter);
app.use("/api/user/area", areaUserRouter);
app.use("/api/tiketing", tiketingRouter);
app.use("/testing/api", testingRouter);
app.use("/api/erp", erpRouter);
app.use("/api/lloyd/v1", lloydRouter);
app.use("/api/kik/v1", kikRouter);
app.use("/api/v2/health/check", health_checkRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

//e
// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;

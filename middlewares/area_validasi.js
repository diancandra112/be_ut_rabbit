module.exports={
    validasiProfileArea:(req, res, next)=>{
        try {
            const decoded_token = res.locals.decoded
            if (decoded_token.role=='COMPANY') {
                const data = req.query
                if (data.area_id==undefined) {
                    res.status(400).json({responseCode:400,messages:`field area_id harus di deklarasikan`})
                }else{
                    const result = {}
                    result.id = data.area_id
                    res.locals.data = result
                    next()
                }
            } else {
                const result = {}
                result.id = decoded_token.id
                res.locals.data = result
                next()
            }
        } catch (error) {
            console.log(error)
        }
    },
}
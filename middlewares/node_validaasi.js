module.exports = {
  validasiUnsignedNode: (req, res, next) => {
    try {
      const params = ["devEui", "node_id", "typeId"];
      const data = req.body;
      params.map((val) => {
        if (data[val] == undefined) {
          res.status(400).json({
            responseCode: 400,
            messages: `${val} undefined`,
          });
          throw new Error(`${val} undefined`);
        }
      });
      next();
    } catch (error) {
      console.log(error);
    }
  },
  validasiGetUnsignedNode: (req, res, next) => {
    try {
      const decoded_token = res.locals.decoded;
      if (decoded_token.role == "COMPANY") {
        const data = req.query;
        if (data.areaId == undefined) {
          res.status(400).json({
            responseCode: 400,
            messages: `field areaId harus di deklarasikan`,
          });
        } else {
          res.locals.data = data;
          next();
        }
      } else {
        const result = {};
        const data = req.query;
        result.areaId = decoded_token.id;
        result.tenantId = null;
        result.internalId = null;
        res.locals.data = { ...result, ...data };
        next();
      }
    } catch (error) {
      console.log(error);
    }
  },
  validasiGetNode: (req, res, next) => {
    try {
      const decoded_token = res.locals.decoded;
      if (decoded_token.role == "COMPANY") {
        const data = req.query;
        if (data.areaId == undefined) {
          res.status(400).json({
            responseCode: 400,
            messages: `field areaId harus di deklarasikan`,
          });
        } else {
          res.locals.data = data;
          next();
        }
      } else if (decoded_token.role == "TENANT") {
        const data = req.query;
        data.tenantId = decoded_token.id;
        // console.log(decoded_token);
        // if (data.tenantId == undefined) {
        //   res.status(400).json({
        //     responseCode: 400,
        //     messages: `field tenantId harus di deklarasikan`,
        //   });
        // } else {
        res.locals.data = data;
        next();
        // }?
      } else {
        const result = {};
        const data = req.query;
        result.areaId = decoded_token.id;
        res.locals.data = { ...result, ...data };
        next();
      }
    } catch (error) {
      console.log(error);
    }
  },

  validasiAssignedNode: (req, res, next) => {
    try {
      const data = req.body;
      const detail = {};
      const update = {};
      if (data.devEui == undefined) {
        res.status(400).json({
          responseCode: 400,
          messages: `field devEui harus di deklarasikan`,
        });
        throw new Error(
          `responseCode:400,messages:field devEui harus di deklarasikan`
        );
      } else {
        if (data.tenantId == undefined) {
          if (data.internalId == undefined) {
            res.status(400).json({
              responseCode: 400,
              messages: `pilih slah satu antara tenantId atau internalId`,
            });
            throw new Error(
              `responseCode:400,messages:pilih slah satu antara tenantId atau internalId`
            );
          }
          update.internalId = data.internalId;
          delete data.internalId;
        } else if (data.internalId == undefined) {
          if (data.tenantId == undefined) {
            res.status(400).json({
              responseCode: 400,
              messages: `pilih slah satu antara tenantId atau internalId`,
            });
            throw new Error(
              `responseCode:400,messages:pilih slah satu antara tenantId atau internalId`
            );
          }
          update.meter_id = data.meter_id;
          update.tenantId = data.tenantId;
          update.rtu_pricing_id = data.rtu_pricing_id;
          update.rtu_pricing_name = data.rtu_pricing_name;
          update.field_billing_rtu = data.field_billing;
          delete data.tenantId;
          delete data.rtu_pricing_id;
          delete data.rtu_pricing_name;
        }
      }
      res.locals.update = update;
      res.locals.data = data;
      next();
    } catch (error) {
      console.log(error);
    }
  },
  validasiGetHistoryNode: (req, res, next) => {
    try {
      const data = req.query;
      const field = ["device_id", "page", "page_size", "typeId"];
      field.map((val) => {
        if (data[val] == undefined) {
          res.status(400).json({
            messages: `field ${val} harus di isi`,
          });
          throw new Error();
        }
      });
      const page = (data.page - 1) * (data.page_size + 1);
      const pageSize = data.page_size * data.page;
      delete data.page;
      delete data.page_size;
      res.locals.typeId = data.typeId;
      delete data.typeId;
      res.locals.data = data;
      res.locals.page = page;
      res.locals.page_size = pageSize;
      next();
    } catch (error) {
      console.log(error);
    }
  },
};

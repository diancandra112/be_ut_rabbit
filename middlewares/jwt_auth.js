const jwt = require("jsonwebtoken");
require("dotenv").config();
module.exports = {
  AuthTeknisiAndCom: (req, res, next) => {
    try {
      let token = req.headers["authorization"];
      console.log(token);
      token = token.split(" ");
      token = token[1];
      jwt.verify(token, process.env.SECRET, (err, result) => {
        if (err) {
          res.status(401).json({
            responseCode: 401,
            messages: "token tidak sesuai",
          });
        } else {
          if (result.role != "TEKNISI" && result.role != "TEKNISI COMPANY") {
            throw new Error();
          }
          res.locals.decoded = result;
          next();
        }
      });
    } catch (error) {
      console.log(error);
      res.status(401).json({
        responseCode: 401,
        messages: "token tidak sesuai",
      });
    }
  },
  AuthTeknisi: (req, res, next) => {
    try {
      let token = req.headers["authorization"];
      token = token.split(" ");
      token = token[1];
      jwt.verify(token, process.env.SECRET, (err, result) => {
        if (err) {
          res.status(401).json({
            responseCode: 401,
            messages: "token tidak sesuai",
          });
        } else {
          if (result.role != "TEKNISI") {
            throw new Error();
          }
          res.locals.decoded = result;
          next();
        }
      });
    } catch (error) {
      console.log(error);
      res.status(401).json({
        responseCode: 401,
        messages: "token tidak sesuai",
      });
    }
  },
  AuthTenant: (req, res, next) => {
    try {
      let token = req.headers["authorization"];
      token = token.split(" ");
      token = token[1];
      jwt.verify(token, process.env.SECRET, (err, result) => {
        if (err) {
          res.status(401).json({
            responseCode: 401,
            messages: "token tidak sesuai",
          });
        } else {
          if (result.role != "TENANT") {
            throw new Error();
          }
          res.locals.decoded = result;
          next();
        }
      });
    } catch (error) {
      console.log(error);
      res.status(401).json({
        responseCode: 401,
        messages: "token tidak sesuai",
      });
    }
  },

  AuthActionRequered: (req, res, next) => {
    try {
      let token = req.headers["authorization"];
      token = token.split(" ");
      token = token[1];
      console.log(token);
      let output = "";
      jwt.verify(token, process.env.SECRET, (err, result) => {
        if (err) {
          res.status(401).json({
            responseCode: 401,
            messages: "token invalid",
          });
        } else {
          if (result.iat < 1634205201) {
            return res.status(401).json({
              responseCode: 401,
              messages: "token force log out",
            });
          }
          if (
            (result.role == "TEKNISI" && result.real_role == null) ||
            (result.role == "TEKNISI COMPANY" && result.real_role == null)
          ) {
            req.user_id = result.user_id;
            req.role = result.role;
          } else if (result.role != result.real_role) {
            req.user_id = result.user_id;
            req.role = result.real_role;
          } else if (result.role == "AREA" && result.real_role == "AREA") {
            req.user_id = result.iot_area.user_id;
            req.role = result.role;
          } else if (
            result.role == "COMPANY" &&
            result.real_role == "COMPANY"
          ) {
            req.user_id = result.iot_company.user_id;
            req.role = result.role;
          } else {
            return res.status(401).json({
              responseCode: 401,
              messages: "token invalid",
            });
          }
          next();
        }
      });
    } catch (error) {
      res.status(401).json({
        responseCode: 401,
        messages: "token invalid",
      });
    }
  },
  AuthNotRequered: (req, res, next) => {
    try {
      let token = req.headers["authorization"];
      token = token.split(" ");
      token = token[1];
      console.log(token);
      let output = "";
      jwt.verify(token, process.env.SECRET, (err, result) => {
        if (err) {
          next();
        } else {
          if (result.role == "TENANT" || result.role == "TEKNISI") {
            output = result.role + "-" + result.username;
            res.locals.areaId = result.area_id;
          } else {
            output = result.role + "-" + result.email;
            res.locals.areaId = result.iot_area.id;
          }
          res.locals.decoded = output;
          res.locals.role = result.role;
          next();
        }
      });
    } catch (error) {
      next();
    }
  },
  AuthAreaTenant: (req, res, next) => {
    try {
      let token = req.headers["authorization"];
      token = token.split(" ");
      token = token[1];
      jwt.verify(token, process.env.SECRET, (err, result) => {
        if (err) {
          res.status(401).json({
            responseCode: 401,
            messages: "token tidak sesuai",
          });
        } else {
          if (result.role == "AREA") {
            result.area_id = result.iot_area.id;
          } else if (result.role == "TENANT") {
            result.area_id = result.area_id;
          } else {
            throw new Error();
          }
          res.locals.decoded = result;
          next();
        }
      });
    } catch (error) {
      console.log(error);
      res.status(401).json({
        responseCode: 401,
        messages: "token tidak sesuai",
      });
    }
  },
  AuthArea: (req, res, next) => {
    try {
      let token = req.headers["authorization"];
      token = token.split(" ");
      token = token[1];
      jwt.verify(token, process.env.SECRET, (err, result) => {
        if (err) {
          res.status(401).json({
            responseCode: 401,
            messages: "token tidak sesuai",
          });
        } else {
          if (result.role != "AREA") {
            throw new Error();
          }
          res.locals.decoded = result;
          next();
        }
      });
    } catch (error) {
      console.log(error);
      res.status(401).json({
        responseCode: 401,
        messages: "token tidak sesuai",
      });
    }
  },
  AuthCompany: (req, res, next) => {
    try {
      let token = req.headers["authorization"];
      token = token.split(" ");
      token = token[1];
      jwt.verify(token, process.env.SECRET, (err, result) => {
        if (err) {
          res.status(401).json({
            responseCode: 401,
            messages: "token tidak sesuai",
          });
        } else {
          if (result.role != "COMPANY") {
            throw new Error();
          }
          res.locals.decoded = result;
          next();
        }
      });
    } catch (error) {
      console.log(error);
      res.status(401).json({
        responseCode: 401,
        messages: "token tidak sesuai",
      });
    }
  },
  AuthCompanyArea: (req, res, next) => {
    try {
      let token = req.headers["authorization"];
      token = token.split(" ");
      token = token[1];
      jwt.verify(token, process.env.SECRET, (err, result) => {
        if (err) {
          res.status(401).json({
            responseCode: 401,
            messages: "token tidak sesuai",
          });
        } else {
          if (
            result.role == "COMPANY" ||
            result.role == "AREA" ||
            result.role == "TENANT"
          ) {
            res.locals.decoded = result;
            next();
          } else {
            throw new Error();
          }
        }
      });
    } catch (error) {
      console.log(error);
      res.status(401).json({
        responseCode: 401,
        messages: "token tidak sesuai",
      });
    }
  },
  AuthCompanyAreaTeknisi: (req, res, next) => {
    try {
      let token = req.headers["authorization"];
      token = token.split(" ");
      token = token[1];
      jwt.verify(token, process.env.SECRET, (err, result) => {
        if (err) {
          res.status(401).json({
            responseCode: 401,
            messages: "token tidak sesuai",
          });
        } else {
          if (result.iat < 1634205201) {
            res.json({
              responseCode: 400,
              message: false,
            });
          } else {
            if (
              result.role == "TEKNISI" ||
              result.role == "TEKNISI COMPANY" ||
              result.role == "COMPANY" ||
              result.role == "AREA" ||
              result.role == "TENANT"
            ) {
              res.locals.decoded = result;
              next();
            } else {
              throw new Error();
            }
          }
        }
      });
    } catch (error) {
      console.log(error);
      res.status(401).json({
        responseCode: 401,
        messages: "token tidak sesuai",
      });
    }
  },

  AuthGetDetail: (req, res, next) => {
    try {
      let token = req.headers["authorization"];
      token = token.split(" ");
      token = token[1];
      console.log(token);
      let output = "";
      jwt.verify(token, process.env.SECRET, async (err, result) => {
        if (err) {
          req.user_id = 0;
          req.role = "gagal";
        } else {
          // return console.log(result);
          if (result.iat < 1634205201) {
            return res.status(401).json({
              responseCode: 401,
              messages: "token force log out",
            });
          }
          if (
            (result.role == "TEKNISI" && result.real_role == null) ||
            (result.role == "TEKNISI COMPANY" && result.real_role == null)
          ) {
            req.user_id = result.user_id;
            req.role = result.role;
          } else if (
            (result.role == "TEKNISI" && result.real_role == null) ||
            (result.role == "TEKNISI COMPANY" && result.real_role == null)
          ) {
            req.user_id = result.user_id;
            req.role = result.role;
          } else if (result.role != result.real_role) {
            req.user_id = result.user_id;
            req.role = result.real_role;
          } else if (result.role == "AREA" && result.real_role == "AREA") {
            req.user_id = result.iot_area.user_id;
            req.role = result.role;
          } else if (
            result.role == "COMPANY" &&
            result.real_role == "COMPANY"
          ) {
            req.user_id = result.iot_company.user_id;
            req.role = result.role;
          } else {
            req.user_id = 0;
            req.role = "token invalid";
          }
          if (req.user_id != 0) {
            const models = require("../models/index");
            let name = await models.iot_users.findOne({
              where: { id: req.user_id },
            });
            name = JSON.parse(JSON.stringify(name));
            req.email = name.email;
          } else {
            req.email = "email";
          }
          next();
        }
      });
    } catch (error) {
      res.status(401).json({
        responseCode: 401,
        messages: "token invalid",
      });
    }
  },

  UnderMaintenace: (req, res, next) => {
    res.status(403).json({
      responseCode: 403,
      messages: "Under Maintenace",
    });
  },
};

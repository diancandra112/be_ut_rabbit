const models = require("../models/index");
const NodeCache = require("node-cache");
const myCache = new NodeCache();
module.exports = {
  GetNodeLog: async (devEUI) => {
    try {
      // let data_cache = await myCache.get(devEUI);
      let data_cache = null;
      if (data_cache == null) {
        let node_find = await models.iot_nodes.findOne({
          where: { devEui: devEUI },
          include: [
            {
              model: models.iot_nodes,
              attributes: [
                "id",
                "areaId",
                "typeId",
                "devEui",
                "last_update",
                "previous_update",
                "live_last_meter",
                "live_previous_meter",
                "is_unsigned",
                "node_link_id",
                "setting_interval",
              ],
            },
            { model: models.iot_internal },
            { model: models.iot_tenant },
            { model: models.iot_area, attributes: { exclude: ["image"] } },
            { model: models.iot_node_type },
            { model: models.iot_detail_gas, as: "iot_detail_gas" },
            { model: models.iot_detail_water },
            { model: models.iot_detail_electric },
            { model: models.iot_detail_electricct },
            { model: models.iot_detail_rtu },
          ],
        });
        node_find = JSON.parse(JSON.stringify(node_find));
        if (node_find != null) {
          await myCache.set(devEUI, node_find, 7200);
          return node_find;
        } else {
          return node_find;
        }
      } else {
        return data_cache;
      }
    } catch (error) {
      console.log(error);
    }
  },
  DeleteCache: async (devEUI) => {
    try {
      await myCache.del(devEUI);
      return "ok";
    } catch (error) {
      console.log(error);
    }
  },
};

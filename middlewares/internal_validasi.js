module.exports = {
  validasiGetInternal: (req, res, next) => {
    try {
      const decoded_token = res.locals.decoded;
      if (decoded_token.role == "COMPANY") {
        const data = req.query;
        if (data.area_id == undefined) {
          res.status(400).json({
            responseCode: 400,
            messages: `field area_id harus di deklarasikan`,
          });
        } else {
          const result = {};
          result.area_id = data.area_id;
          res.locals.data = result;
          // console.log(res.locals.data)
          next();
        }
      } else {
        const result = {};
        const data = req.query;
        result.area_id = decoded_token.id;
        res.locals.data = { ...result, ...data };
        next();
      }
    } catch (error) {
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
  validasiRegisterInternal: (req, res, next) => {
    try {
      const data = req.body;
      const form2 = [
        "email",
        "internal_name",
        "location",
        "phone",
        "handphone",
        "pic",
      ];
      const dataUser = {};
      const dataProfile = {};
      form2.map((val) => {
        if (data[val] == undefined) {
          res.status(400).json({
            responseCode: 400,
            messages: `field ${val} harus di deklarasikan`,
          });
          throw new Error();
        } else {
          dataProfile[val] = data[val];
          delete data[val];
        }
      });

      res.locals.nodeType = data.nodeType;
      res.locals.dataProfile = dataProfile;
      res.locals.dataProfile.area_id = res.locals.decoded.id;
      next();
    } catch (error) {
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
};

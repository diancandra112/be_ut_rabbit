module.exports={
    validasiGetTenant:(req, res, next)=>{
        try {
            const decoded_token = res.locals.decoded
            if (decoded_token.role=='COMPANY') {
                const data = req.query
                if (data.area_id==undefined) {
                    res.status(400).json({responseCode:400,messages:`field area_id harus di deklarasikan`})
                }else{
                    res.locals.data = data
                    next()
                }
            } else{
                const result = {}
                const data = req.query
                result.area_id = decoded_token.id
                res.locals.data = {...result,...data}
                next()
            }
        } catch (error) {
            console.log(error)
        }
    },
}
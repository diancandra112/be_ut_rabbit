module.exports={
    validasiProfileCompany:(req, res, next)=>{
        try {
            const decoded_token = res.locals.decoded
            const result = {}
            result.id = decoded_token.id
            res.locals.data = result
            next()
        } catch (error) {
            console.log(error)
        }
    },
}
module.exports = {
  validasiRegisterCompany: (req, res, next) => {
    try {
      const data = req.body;
      const form1 = ["email", "password", "role_id"];
      const form2 = [
        "company_name",
        "email",
        "username",
        "pic_name",
        "phone",
        "address",
        "handphone",
      ];
      const dataUser = {};
      const dataProfile = {};
      if (data["company_name"] == undefined) {
        res.status(400).json({
          responseCode: 400,
          messages: `field company_name harus di deklarasikan`,
        });
        throw new Error();
      }
      form1.map((val) => {
        if (data[val] == undefined) {
          res.status(400).json({
            responseCode: 400,
            messages: `field ${val} harus di deklarasikan`,
          });
          throw new Error();
        } else {
          dataUser[val] = data[val];
        }
      });
      form2.map((val) => {
        if (data[val] != undefined) {
          dataProfile[val] = data[val];
        }
      });
      res.locals.dataUser = dataUser;
      res.locals.dataProfile = dataProfile;
      next();
    } catch (error) {
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
  validasiRegisterArea: (req, res, next) => {
    try {
      const data = req.body;
      data.company_id = res.locals.decoded.id;
      const form1 = ["email", "password", "role_id"];
      const form2 = [
        "company_id",
        "area_name",
        "email",
        "username",
        "pic_name",
        "phone",
        "address",
        "handphone",
        "image",
        "company_name",
        "billing_address",
      ];
      const dataUser = {};
      const dataProfile = {};
      if (data["area_name"] == undefined) {
        res.status(400).json({
          responseCode: 400,
          messages: `field area_name harus di deklarasikan`,
        });
        throw new Error();
      }
      form1.map((val) => {
        if (data[val] == undefined) {
          res.status(400).json({
            responseCode: 400,
            messages: `field ${val} harus di deklarasikan`,
          });
          throw new Error();
        } else {
          dataUser[val] = data[val];
        }
      });
      form2.map((val) => {
        if (data[val] != undefined) {
          dataProfile[val] = data[val];
        }
      });
      res.locals.dataUser = dataUser;
      res.locals.dataProfile = dataProfile;
      next();
    } catch (error) {
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
  validasiRegisterTenant: (req, res, next) => {
    try {
      const data = req.body;
      const form1 = ["email", "password", "role_id", "username"];
      const form2 = [
        "email",
        "tenant_name",
        "image",
        "address",
        "phone",
        "handphone",
        "username",
        "password",
        "member_level",
        "member_level_id",
        "erp_name",
        "erp_id",
        "company_name",
        "billing_address",
      ];
      const dataUser = {};
      const dataProfile = {};
      if (data["tenant_name"] == undefined) {
        res.status(400).json({
          responseCode: 400,
          messages: `field tenant_name harus di deklarasikan`,
        });
        throw new Error(
          `responseCode:400,messages:field tenant_name harus di deklarasikan`
        );
      }
      form1.map((val) => {
        if (data[val] == undefined) {
          res.status(400).json({
            responseCode: 400,
            messages: `field ${val} harus di deklarasikan`,
          });
          throw new Error(
            `responseCode:400,messages:field ${val} harus di deklarasikan`
          );
        } else {
          dataUser[val] = data[val];
        }
      });
      form2.map((val) => {
        if (data[val] != undefined) {
          dataProfile[val] = data[val];
        }
      });
      res.locals.nodeType = data.nodeType;
      res.locals.dataUser = dataUser;
      res.locals.dataProfile = dataProfile;
      res.locals.dataProfile.area_id = res.locals.decoded.id;
      next();
    } catch (error) {
      res.status(500).json({
        responseCode: 500,
        message: error.message,
      });
    }
  },
};

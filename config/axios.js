const axios = require("axios");
require("dotenv").config();

const weiots = axios.create({
  baseURL: "http://18.140.193.173:3000",
  // baseURL: "https://901a5deae643c6e431856367460474f4.m.pipedream.net",
  timeout: 180000,
});
const midtrans = axios.create({
  // baseURL: "http://127.0.0.1:3010",
  // baseURL: "http://18.141.5.41:3010",
  baseURL: "http://midtrans-sm-dev.trimagnus.com",
  timeout: 30000,
});
const fasapay = axios.create({
  // baseURL: "http://127.0.0.1:3010",
  // baseURL: "http://18.141.5.41:3010",
  baseURL: "http://18.140.193.173:3023",
  timeout: 30000,
});
const axios_erp = axios.create({
  baseURL: "https://erp.be.wiraenergi.co.id/api",
  // baseURL: "http://localhost:3005/testing/api",
  timeout: 30000,
});
const axios_erp2 = axios.create({
  // baseURL: "https://erp.be.wiraenergi.co.id/api",
  baseURL: "http://localhost:3005/testing/api",
  timeout: 30000,
});

module.exports = { weiots, midtrans, axios_erp, axios_erp2, fasapay };

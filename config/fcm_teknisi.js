var admin = require("firebase-admin");

var serviceAccount = require("./iki-teknisi-iot-firebase-adminsdk-ffc27-3bdefc8b43.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});

module.exports.admin = admin;

const axios = require("axios");
require("dotenv").config();

const wa_damp = axios.create({
  baseURL: "https://waba.damcorp.id/whatsapp/sendHsm/",
  //   baseURL: "http://localhost:3005/testing/api",
  timeout: 30000,
});
const wa_token =
  "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiIiwiYWRkcmVzcyI6IiIsInBpYyI6IiIsImlzX2RlbGV0ZSI6ZmFsc2UsImRhdGVfY3JlYXRlZCI6IjIwMjEtMDYtMjJUMDM6MTg6MjcuMTE3WiIsIl9pZCI6IjYwZDE1NjgzM2VmNWZjMjAzNzUwNjdkYyJ9.UnrorRQ2V_kYnzM5qQDJ9YFl6Uicq3Q9kqArkxQP65k";
module.exports = { wa_damp, wa_token };

const axios = require("axios");
require("dotenv").config();

const yukk_qris = axios.create({
  // baseURL: "http://127.0.0.1:3025/",
  baseURL: "https://wh.yukk.weiots.io/",
  timeout: 180000,
});

module.exports = { yukk_qris };

let admin_area = [
  {
    name: "Dashboard",
    link: "/dashboard",
    icon: "icon-dashboard",
  },
  {
    name: "Area Setup",
    link: "/pricing",
    // img: AREA,
    icon: "icon-setupqueue",
    tabs: [
      {
        name: "General",
      },
      {
        name: "Pricing Postpaid",
      },
      {
        name: "Pricing Prepaid",
      },
      {
        name: "Member Level",
      },
      {
        name: "LINE Bot",
      },
      {
        name: "Alarm Pressure",
      },
      {
        name: "ERP Settings",
      },
      {
        name: "Area Pricing",
      },
    ],
  },
  {
    name: "Internal",
    link: "/customer-internal",
    // img: INTERNAL,
    icon: "icon-internalworkexperience",
  },
  {
    name: "Tenant",
    link: "/customer-tenant",
    // img: TENANT,
    icon: "icon-location",
  },
  {
    name: "User",
    link: "/list-user",
    // img: USER,
    icon: "icon-usersetting",
  },
  {
    name: "Instalation",
    link: "/list-instalation",
    // img: INSTALATION,
    icon: "icon-ReservationInstallation",
  },
  {
    name: "Ticket",
    link: "/request",
    // img: Ticket,
    icon: "icon-ask",
  },
  {
    name: "Transaction",
    link: "/billing",
    // img: Billing,
    icon: "icon-iconfinder__",
    child: [
      {
        name: "Usage",
        url: "/transaction/usage",
        create: false,
        read: true,
        update: false,
        delete: false,
      },
      {
        name: "Billing",
        url: "/transaction/billing",
        create: false,
        read: true,
        update: false,
        delete: false,
      },
      {
        url: "/transaction/voucher",
        name: "Voucher",
        create: false,
        read: true,
        update: false,
        delete: false,
      },
    ],
  },
  {
    name: "Downlink Log",
    link: "/downlink-log",
    // img: Report,
    icon: "icon-Organization-Data-report-32",
  },
];

let teknisi_area = [
  {
    name: "Dashboard",
    link: "/dashboard",
    icon: "icon-dashboard",
  },
  {
    name: "Internal",
    link: "/customer-internal",
    // img: INTERNAL,
    icon: "icon-internalworkexperience",
  },
  {
    name: "Tenant",
    link: "/customer-tenant",
    // img: TENANT,
    icon: "icon-location",
  },
  {
    name: "Instalation",
    link: "/list-instalation",
    // img: INSTALATION,
    icon: "icon-ReservationInstallation",
  },
  {
    name: "Ticket",
    link: "/request",
    // img: Ticket,
    icon: "icon-ask",
  },
  {
    name: "Transaction",
    link: "/billing",
    // img: Billing,
    icon: "icon-iconfinder__",
    child: [
      {
        name: "Usage",
        url: "/transaction/usage",
        create: false,
        read: true,
        update: false,
        delete: false,
      },
      {
        url: "/transaction/voucher",
        name: "Voucher",
        create: false,
        read: true,
        update: false,
        delete: false,
      },
    ],
  },
  {
    name: "Downlink Log",
    link: "/downlink-log",
    // img: Report,
    icon: "icon-Organization-Data-report-32",
  },
];

let finance_area = [
  {
    name: "Dashboard",
    link: "/dashboard",
    icon: "icon-dashboard",
  },
  {
    name: "Area Setup",
    link: "/pricing",
    // img: AREA,
    icon: "icon-setupqueue",
    tabs: [
      {
        name: "General",
      },
      {
        name: "Pricing Postpaid",
      },
      {
        name: "Pricing Prepaid",
      },
      {
        name: "Member Level",
      },
      {
        name: "ERP Settings",
      },
      {
        name: "Area Pricing",
      },
    ],
  },
  {
    name: "Tenant",
    link: "/customer-tenant",
    // img: TENANT,
    icon: "icon-location",
  },
  {
    name: "Transaction",
    link: "/billing",
    // img: Billing,
    icon: "icon-iconfinder__",
    child: [
      {
        name: "Usage",
        url: "/transaction/usage",
        create: true,
        read: true,
        update: true,
        delete: true,
      },
      {
        name: "Billing",
        url: "/transaction/billing",
        create: true,
        read: true,
        update: true,
        delete: true,
      },
      {
        url: "/transaction/voucher",
        name: "Voucher",
        create: true,
        read: true,
        update: true,
        delete: true,
      },
    ],
  },
];
let admin_company = [
  {
    name: "Dashboard",
    link: "/company/dashboard",
    img: "Dashboard",
    icon: "icon-dashboard",
  },
  {
    name: "List Area",
    link: "/company/list-area",
    // img: ListAreaIcon,
    icon: "icon-area",
  },
  {
    name: "User Teknisi",
    link: "/company/user-teknisi",
    // img: ListUserIcon,
    icon: "icon-usersetting",
  },
  {
    name: "Downlink Log Company",
    link: "/company/downlink-log",
    // img: Report,
    icon: "icon-Organization-Data-report-32",
  },
];
let teknisi_company = [
  {
    name: "Dashboard",
    link: "/company/dashboard",
    img: "Dashboard",
    icon: "icon-dashboard",
  },
  {
    name: "List Area",
    link: "/company/list-area",
    // img: ListAreaIcon,
    icon: "icon-area",
  },
  {
    name: "Downlink Log Company",
    link: "/company/downlink-log",
    // img: Report,
    icon: "icon-Organization-Data-report-32",
  },
];
module.exports = {
  admin_area,
  teknisi_area,
  finance_area,
  admin_company,
  teknisi_company,
};

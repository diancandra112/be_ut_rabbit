const corn = require("node-cron");
const moment = require("moment");
const axios = require("axios");
const models = require("../../models/index");
const { Op } = models.Sequelize;
const { SvcGetArea } = require("./service_get_area");

const GenerateBillingV2 = () => {
  //   const job = corn.schedule("*/30 */2 * * *", () => {
  const job = corn.schedule("*/30 * * * *", () => {
    job.stop();
    console.log("running create billing");
    SvcGetArea()
      .then((area) => {
        job.start();
      })
      .catch((e) => {
        console.log("error");
        job.start();
      });
  });
};

module.exports = {
  allNode: () => {
    GenerateBillingV2();
  },
};

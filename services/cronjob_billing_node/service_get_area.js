const moment = require("moment");
const models = require("../../models/index");
const { Op } = models.Sequelize;

module.exports = {
  SvcGetArea: (data) => {
    return new Promise((resolve, reject) => {
      return models.iot_area
        .findAll({
          where: {
            saas: true,
            saas_cut_date: {
              [Op.or]: [
                null,
                models.sequelize.where(
                  models.sequelize.fn(
                    "date",
                    models.sequelize.col("iot_area.saas_cut_date")
                  ),
                  "<",
                  moment().utcOffset("+0700").format("YYYY-MM-DD")
                ),
              ],
            },
          },
          attributes: { exclude: ["image"] },
          include: [
            {
              model: models.iots_cutoff_saas,
              attributes: ["id", "areaId", "tanggal_cutoff", "time", "order"],
              required: true,
              where: {
                tanggal_cutoff: moment().utcOffset("+0700").format("DD"),
                time: {
                  [Op.lte]: moment().utcOffset("+0700").format("HH:mm:ss"),
                },
              },
            },
            {
              model: models.iot_nodes,
              where: {
                is_unsigned: { [Op.not]: true },
                [Op.or]: [
                  {
                    tenantId: { [Op.not]: null },
                  },
                  {
                    internalId: { [Op.not]: null },
                  },
                ],
                [Op.and]: [
                  {
                    last_update: { [Op.not]: null },
                  },
                  {
                    last_update: models.sequelize.where(
                      models.sequelize.fn(
                        "date",
                        models.sequelize.col("iot_nodes.last_update")
                      ),
                      "<=",
                      moment().utcOffset("+0700").format("YYYY-MM-DD")
                    ),
                  },
                  {
                    last_update: models.sequelize.where(
                      models.sequelize.fn(
                        "date",
                        models.sequelize.col("iot_nodes.last_update")
                      ),
                      ">=",
                      moment()
                        .subtract(1, "month")
                        .utcOffset("+0700")
                        .format("YYYY-MM-DD")
                    ),
                  },
                ],
                last_cut_date: {
                  [Op.or]: [
                    null,
                    models.sequelize.where(
                      models.sequelize.fn(
                        "date",
                        models.sequelize.col("iot_nodes.last_cut_date")
                      ),
                      "<",
                      moment().utcOffset("+0700").format("YYYY-MM-DD")
                    ),
                  ],
                },
              },
              include: [
                {
                  model: models.iot_tenant,
                },
                {
                  model: models.iot_internal,
                },
                {
                  model: models.iot_node_type,
                },
              ],
            },
          ],
        })
        .then((area) => {
          area = JSON.parse(JSON.stringify(area));
          // return console.log(area);
          if (area.length > 0) {
            let device = [];
            let update = [];
            area.map((value) => {
              update.push({
                id: value.id,
                saas_cut_date: moment().utcOffset("+0700").format("YYYY-MM-DD"),
              });
              let ppn = value.is_ppn ? value.is_ppn_value : 0;
              let pricing = value.saas_price;
              let invoice =
                "We" +
                "." +
                value.area_name +
                "." +
                value.id +
                "-INV-" +
                moment().subtract(1, "day").format("DDMMYY") +
                "-" +
                value.id +
                "G";

              let billing = {
                log: [],
                periode_cut: value.iots_cutoff_saas[0].order,
                minimum_charge: 0,
                minimum_charge_total: 0,
                areaId: value.id,
                cut_date: moment()
                  .utcOffset("+0700")
                  .subtract(1, "day")
                  .format("YYYY-MM-DD"),
                periode_billing: moment()
                  .utcOffset("+0700")
                  .subtract(3, "day")
                  .format("YYYY-MM"),
                invoice: invoice,
                harga_satuan: pricing,
                ppn: ppn,
                biaya_transaksi: 5000,
                biaya_penyesuaian: 0,
                status: "NEW",
                usage: 0,
                billing_usage: 0,
                correction_usage: 0,
                denda: 0,
                log_flag: true,
                billing_type: "node",
              };
              value.iot_nodes.map((value2) => {
                billing.usage++;
                billing.billing_usage++;
                let node = {
                  type: value2.iot_node_type.type_name,
                  localtion:
                    value2.tenantId != null
                      ? value2.iot_tenant.tenant_name
                      : value2.iot_internal.internal_name,
                  location_type:
                    value2.tenantId != null ? "tenant" : "internal",
                  devEui: value2.devEui,
                  periode: moment()
                    .utcOffset("+0700")
                    .subtract(3, "day")
                    .format("YYYY-MM"),
                  start_date: moment().utcOffset("+0700").format("YYYY-MM-DD"),
                  end_date: moment()
                    .subtract(1, "month")
                    .utcOffset("+0700")
                    .format("YYYY-MM-DD"),
                };
                billing.log.push(node);
              });
              billing.log = JSON.stringify(billing.log);
              device.push(billing);
            });
            // return console.log(device);
            return models.iots_billing_history
              .bulkCreate(device)
              .then((result) => {
                return models.iot_area
                  .bulkCreate(update, {
                    updateOnDuplicate: ["saas_cut_date"],
                  })
                  .then((a) => resolve(device));
              })
              .catch((err) => {
                console.log(err, "eror");
                reject(err.message);
              });
          } else {
            resolve(area);
          }
        })
        .catch((error) => {
          console.log(error, "eror");
          reject(error);
        });
    });
  },
};

const moment = require("moment");
const { sequelize, Sequelize } = require("../../models/index");
const models = require("../../models/index");
const { weiots } = require("../../config/axios");
const { Op } = Sequelize;
module.exports = {
  svcTransactionPrepaidGet: (data, page = 1, size = 10) => {
    return new Promise((resolve, reject) => {
      return sequelize.transaction((t) => {
        return models.iots_billing_prepaid
          .findAndCountAll({
            limit: parseInt(size),
            offset: (parseInt(page) - 1) * parseInt(size),
            where: data,
            order: [["id", "DESC"]],
            include: [{ model: models.iot_tenant }],
            transaction: t,
          })
          .then((res) => {
            resolve({
              responseCode: 200,
              message: "sukses post prepaid billing",
              result: res.rows,
              page: page,
              size: size,
              total: res.count,
            });
          })
          .catch((err) => {
            reject({
              responseCode: 400,
              message: "failed post prepaid billing",
              error: err.message,
            });
          });
      });
    });
  },
  svcTransactionPrepaidGetArea: (data) => {
    return new Promise((resolve, reject) => {
      return sequelize
        .transaction((t) => {
          return models.iot_area.findOne({
            where: { id: data.area_id },
            attributes: ["id", "auto_paid_prepaid"],
            transaction: t,
          });
        })
        .then((res) => {
          res = JSON.parse(JSON.stringify(res));
          resolve({
            responseCode: 200,
            data: res,
          });
        })
        .catch((err) => {
          reject({
            responseCode: 400,
            message: "failed post prepaid billing",
            error: err.message,
          });
        });
    });
  },
  svcTransactionPrepaidAdd: (data) => {
    return new Promise((resolve, reject) => {
      return sequelize
        .transaction((t) => {
          return models.iots_billing_prepaid
            .create(data, {
              transaction: t,
            })
            .then((res) => {
              res = JSON.parse(JSON.stringify(res));
              return models.iots_billing_prepaid
                .findOne({
                  where: { id: { [Op.lt]: res.id } },
                  order: [["id", "DESC"]],
                  transaction: t,
                })
                .then((res2) => {
                  res2 = JSON.parse(JSON.stringify(res2));
                  let data_update = {};
                  let permata = "654321";
                  let bca = "123456";
                  let tgl = moment().utcOffset("+0700").format("YYYYMM");
                  let urutan = ("0000" + 1).toString().slice(-4);
                  if (res2 == null) {
                    data_update.no_order = "VC-" + tgl + "-" + urutan;
                    data_update.va_bca = bca + tgl + urutan;
                    data_update.va_permata = permata + tgl + urutan;
                  } else {
                    if (res2.no_order == null) {
                      data_update.no_order = "VC-" + tgl + "-" + urutan;
                      data_update.va_bca = bca + tgl + urutan;
                      data_update.va_permata = permata + tgl + urutan;
                    } else {
                      let no1 = res2.no_order.split("-");
                      if (no1[1] == tgl) {
                        urutan = ("0000" + (parseInt(no1[2]) + 1))
                          .toString()
                          .slice(-4);
                      }
                      data_update.no_order = "VC-" + tgl + "-" + urutan;
                      data_update.va_bca = bca + tgl + urutan;
                      data_update.va_permata = permata + tgl + urutan;
                    }
                  }
                  return models.iots_billing_prepaid
                    .update(data_update, {
                      where: { id: res.id },
                      transaction: t,
                    })
                    .then(() => {
                      return res;
                    });
                });
            });
        })
        .then((res) => {
          resolve({
            responseCode: 200,
            message: "sukses post prepaid billing",
            data: res,
          });
        })
        .catch((err) => {
          reject({
            responseCode: 400,
            message: "failed post prepaid billing",
            error: err.message,
          });
        });
    });
  },
  svcListType: (data) => {
    return new Promise((resolve, reject) => {
      return sequelize.transaction((t) => {
        return models.iot_node_type
          .findAll({
            where: { id: { [Op.in]: [1, 2, 3, 4, 7] } },
            transaction: t,
          })
          .then((res) => {
            resolve({
              responseCode: 200,
              message: "sukses get prepaid type",
              payload: res,
            });
          })
          .catch((err) => {
            reject({
              responseCode: 400,
              message: "failed get prepaid type",
              error: err.message,
            });
          });
      });
    });
  },
  svcCreatePrepaid: (data) => {
    return new Promise((resolve, reject) => {
      return sequelize.transaction((t) => {
        return models.iot_pricing_prepaid
          .create(data, { transaction: t })
          .then((res) => {
            resolve({
              responseCode: 200,
              message: "sukses add prepaid setting",
              payload: res,
            });
          })
          .catch((err) => {
            reject({
              responseCode: 400,
              message: "failed add prepaid setting",
              error: err.message,
            });
          });
      });
    });
  },
  svcGetPrepaid: (data) => {
    return new Promise((resolve, reject) => {
      return sequelize.transaction((t) => {
        return models.iot_pricing_prepaid
          .findAll({ where: data, transaction: t })
          .then((res) => {
            resolve({
              responseCode: 200,
              message: "sukses get prepaid setting",
              payload: res,
            });
          })
          .catch((err) => {
            reject({
              responseCode: 400,
              message: "failed get prepaid setting",
              error: err.message,
            });
          });
      });
    });
  },

  svcEditPrepaid: (data) => {
    return new Promise((resolve, reject) => {
      return sequelize.transaction((t) => {
        return models.iot_pricing_prepaid
          .update(data, {
            where: { id: data.id, areaId: data.areaId },
            transaction: t,
          })
          .then((res) => {
            resolve({
              responseCode: 200,
              message: "sukses edit prepaid setting",
            });
          })
          .catch((err) => {
            reject({
              responseCode: 400,
              message: "failed edit prepaid setting",
              error: err.message,
            });
          });
      });
    });
  },
  svcDeletePrepaid: (data) => {
    return new Promise((resolve, reject) => {
      return sequelize.transaction((t) => {
        return models.iot_pricing_prepaid
          .destroy({ where: data, transaction: t })
          .then((res) => {
            resolve({
              responseCode: 200,
              message: "sukses delete prepaid setting",
              payload: res,
            });
          })
          .catch((err) => {
            reject({
              responseCode: 400,
              message: "failed delete prepaid setting",
              error: err.message,
            });
          });
      });
    });
  },
  svcTransactionPrepaidPostWeiots: (data) => {
    return new Promise((resolve, reject) => {
      return sequelize
        .transaction((t) => {
          return models.iot_nodes
            .findOne({
              where: { devEui: data.data.device_deveui },
              attributes: ["id", "devEui", "prepayment"],
              transaction: t,
            })
            .then((node) => {
              node = JSON.parse(JSON.stringify(node));
              let data_log = {
                time: moment().utcOffset("+0700").format(),
                action:
                  "topup " +
                  data.data.product_value +
                  " " +
                  data.data.satuan_vc,
                deveui: data.data.device_deveui,
                email: "faspay",
              };
              let nominal =
                parseFloat(data.data.product_value) * 1000 +
                parseFloat(node.prepayment);
              let data_down = `/action/topup/${data.data.device_deveui}/${nominal}`;
              console.log(data_down, "post", data_log);
              weiots({
                method: "post",
                url: data_down,
              });
              models.iots_log_downlink.create(data_log);
              models.iot_nodes.update(
                {
                  prepayment: nominal.toString(),
                  live_prepayment_date: moment().utcOffset("+0700").format(),
                },
                { where: { devEui: data.data.device_deveui } }
              );

              return data;
            });
        })
        .then((res) => {
          res = JSON.parse(JSON.stringify(res));
          resolve({
            responseCode: 200,
            data: res,
          });
        })
        .catch((err) => {
          reject({
            responseCode: 400,
            message: "failed post prepaid billing",
            error: err.message,
          });
        });
    });
  },
};

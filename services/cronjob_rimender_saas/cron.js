const corn = require("node-cron");
const moment = require("moment");
const axios = require("axios");
const models = require("../../models/index");
const { Op } = models.Sequelize;
const { RimaenderMail } = require("../../emailPayload/rimender");
require("dotenv").config();

const { transporter } = require("../../config/email");
const email_send = (data, id, update, to) => {
  return new Promise(async (resolve, reject) => {
    try {
      let email_body = await RimaenderMail(data);
      var mailOptions = {
        from: process.env.EMAIL_USER,
        to: to,
        cc: ["diancandra@trimagnus.com", "genin@weiots.io", "alvin@weiots.io"],
        subject: "Reminder Before Subscription Ends",
        html: email_body,
        headers: {
          "x-priority": "1",
          "x-msmail-priority": "High",
          importance: "high",
        },
      };
      let email = await transporter.sendMail(mailOptions);
      await models.iot_area.update(update, { where: id });
      return resolve(email);
    } catch (error) {
      reject(error);
    }
  });
};
const get_data = (flag, data) => {
  return new Promise(async (resolve, reject) => {
    try {
      let area = await models.iot_area.findOne({
        attributes: [
          "area_name",
          "image",
          "address",
          "id",
          "email",
          "email_pic",
          "saas_expired",
        ],
        include: [
          {
            model: models.iot_companies,
            as: "list_area",
            attributes: ["image", "id"],
          },
        ],
        where: {
          saas: true,
          date: models.sequelize.where(
            models.sequelize.fn(
              "datediff",
              models.sequelize.fn(
                "date",
                models.sequelize.col("iot_area.saas_expired")
              ),
              models.sequelize.fn("NOW")
            ),
            { [Op.between]: [0, flag] }
          ),
          ...data,
        },
      });
      area = JSON.parse(JSON.stringify(area));
      if (area == null) return resolve(null);
      let to = [area.email, ...area.email_pic.split(",")];
      let data_email = {
        expired: moment(area.saas_expired).format("MMMM DD"),
        image: area.list_area.image,
        nama_pelanggan: area.area_name,
        alamat: area.address,
        keterangan: `reminder system ${flag} days before subscription ends`,
      };
      let flag_name = flag == 30 ? "saas_30" : flag == 7 ? "saas_7" : "saas_3";

      let email = await email_send(
        data_email,
        { id: area.id },
        { [flag_name]: moment().format("YYYY-MM") },
        to
      );
      return resolve(email);
    } catch (error) {
      reject(error);
    }
  });
};
const RimenderSaas = () => {
  //   const job = corn.schedule("*/30 */2 * * *", () => {
  const job = corn.schedule("*/20 * * * *", async () => {
    try {
      job.stop();
      let data_30 = {
        saas_30: {
          [Op.or]: [null, { [Op.not]: moment().format("YYYY-MM") }],
        },
      };
      let data_7 = {
        saas_7: {
          [Op.or]: [null, { [Op.not]: moment().format("YYYY-MM") }],
        },
      };
      let data_3 = {
        saas_3: {
          [Op.or]: [null, { [Op.not]: moment().format("YYYY-MM") }],
        },
      };
      console.log("sek");

      let day30 = await get_data(30, data_30);
      let day7 = await get_data(7, data_7);
      let day3 = await get_data(3, data_3);
      console.log("wes", day30);
      console.log("wes", day7);
      console.log("wes", day3);
      // job.start();
    } catch (error) {
      console.log(error);
      // job.start();
    }
  });
};

module.exports = {
  Rimender: () => {
    RimenderSaas();
  },
};

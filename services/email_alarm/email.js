const { transporter_alarm } = require("../../config/email");

module.exports = {
  SvcAlarm: (data) => {
    return new Promise((resolve, reject) => {
      var mailOptions = {
        from: "diancandra112@gmail.com",
        to: "teknisi.iki@gmail.com",
        subject: "Password Reset",
        text: "test",
      };

      transporter_alarm.sendMail(mailOptions, function (error, info) {
        if (error) {
          console.log(error);
          reject(error);
        } else {
          resolve("Email sent: " + info.response);
        }
      });
    });
  },
};

const models = require("../models/index");
const fs = require("fs");
function base64MimeType(encoded) {
  var result = null;

  if (typeof encoded !== "string") {
    return result;
  }

  var mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);

  if (mime && mime.length) {
    result = mime[1];
  }
  result = result.split("/");
  return result[1];
}

require("dotenv").config();

module.exports = {
  serviceUploadImages: (data, UrlHost) => {
    return new Promise((resolve, reject) => {
      const imgdata = data.base64;
      const ext = base64MimeType(imgdata);
      const name = Date.now() + `.${ext}`;
      const path = "./images/" + name;
      const base64Data = imgdata.replace(/^data:([A-Za-z-+/]+);base64,/, "");
      fs.writeFile(path, base64Data, { encoding: "base64" }, err => {
        if (err) {
          console.log(err);
          reject({
            responseCode: 500,
            messages: err
          });
        } else {
          resolve({
            responseCode: 200,
            messages: `${UrlHost}/${name}`
          });
        }
      });
    });
  }
};

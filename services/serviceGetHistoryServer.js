const models = require('../models/index')
const axios = require('axios')
const { Op } = require('sequelize')
require('dotenv').config()
module.exports={
    findAllNode:()=>{
        return new Promise((resolve, reject)=>{            
        models.iot_nodes.findAll({
            where:{
                [Op.or]:[
                    {
                        tenantId:{
                            [Op.not]:null
                        }
                    },
                    {
                        internalId:{
                            [Op.not]:null
                        }
                    }
                ]
            }
        }).then((result)=>resolve(JSON.parse(JSON.stringify(result)))).catch((err)=>console.log(err))
        })
    },
    nodeReport:(x,data)=>{
        return new Promise((resolve)=>setTimeout(()=>{
            data = JSON.parse(JSON.stringify(data))
            console.log('=========================================================')
            axios.get(process.env.HOST_SERVER+`/get/history?devEUI=${data.devEui}`)
            .then((s)=>{
                console.log(data.devEui)
                reportTime = JSON.parse(s.data.node_history.payload).reportTime
                console.log('+++++++++++++++++++++++++++++++++++++++++++++++++++++++') 
                resolve('ok')
                return models.sequelize.transaction(t=>{
                    return models.iot_histories.findOrCreate({
                        where:{
                            device_id:data.id,
                            reportTime: reportTime
                        },
                        defaults:{
                            device_id:data.id,
                            payload:s.data.node_history.payload,
                            reportTime: reportTime
                        },   
                        transaction:t
                    })
                }).then(()=>resolve('ok')).catch((err)=>resolve("err"))
            }).catch((err)=>{
                console.log('errrrrrrr',err)
                resolve("err")
            })
        },Math.random()*5000))
    }
}
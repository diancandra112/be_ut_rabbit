const moment = require("moment");
const models = require("../../models/index");
const { type_1 } = require("./billing_type_1");
const { type_2 } = require("./billing_type_2");
const { type_3 } = require("./billing_type_3");
const { type_7 } = require("./billing_type_7");
const { type_4 } = require("./billing_type_4");
const { SvcGetFine } = require("./service_billing_fine");
const { Op } = models.Sequelize;

module.exports = {
  Svchandle: (data) => {
    return new Promise((resolve, reject) => {
      if (data.typeId == 1) {
        type_1(data)
          .then((result_type) => {
            SvcGetFine(data, result_type)
              .then(() => resolve("OK"))
              .catch(() => resolve("error"));
          })
          .catch(() => resolve("error"));
      } else if (data.typeId == 2) {
        type_2(data)
          .then((result_type) => {
            SvcGetFine(data, result_type)
              .then(() => resolve("OK"))
              .catch(() => resolve("error"));
          })
          .catch(() => resolve("error"));
      } else if (data.typeId == 3) {
        type_3(data)
          .then((result_type) => {
            SvcGetFine(data, result_type)
              .then(() => resolve("OK"))
              .catch(() => resolve("error"));
          })
          .catch(() => resolve("error"));
      } else if (data.typeId == 4) {
        type_4(data)
          .then((result_type) => {
            SvcGetFine(data, result_type)
              .then(() => resolve("OK"))
              .catch(() => resolve("error"));
          })
          .catch(() => resolve("error"));
      } else if (data.typeId == 7) {
        type_7(data)
          .then((result_type) => {
            SvcGetFine(data, result_type)
              .then(() => resolve("OK"))
              .catch(() => resolve("error"));
          })
          .catch(() => resolve("error"));
      } else {
        resolve("skip");
      }
    });
  },
};

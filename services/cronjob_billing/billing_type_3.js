const moment = require("moment");
const models = require("../../models/index");
const { Op } = models.Sequelize;

module.exports = {
  type_3: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        console.log(data);
        let update = {
          id: data.id,
          last_cut_date: moment().format("YYYY-MM-DD"),
        };
        let billing = {
          periode_cut: data.area.iots_cutoffs[0].order,
          minimum_charge: 0,
          no_meter: data.meter_id,
          minimum_charge_total:
            data.iot_tenant.minimum_charge_electricity_non_ct <= 0
              ? parseFloat(data.area.minimum_charge_electricity_non_ct) * 1000
              : parseFloat(data.iot_tenant.minimum_charge_electricity_non_ct) *
                1000,
          //billing_type: data.area.billing_charge_type,
          areaId: data.areaId,
          tenantId: data.tenantId,
          nodeId: data.id,
          cut_date: moment()
            .utcOffset("+0700")
            .subtract(1, "day")
            .format("YYYY-MM-DD"),
          periode_billing: moment()
            .utcOffset("+0700")
            .subtract(1, "day")
            .format("YYYY-MM"),
          invoice: data.invoice,
          harga_satuan: data.price,
          ppn: data.ppn,
          biaya_transaksi: 5000,
          biaya_penyesuaian: 0,
          status: "NEW",
          correction_usage: 0,
          denda: 0,
          deveui: data.devEui,
          node_type: "ELECTRICITY NON CT",
          unit: "kwh",
          typeId: 3,
        };
        if (data.bill.length > 0) {
          billing.start_meter = data.bill[0].end_meter;
          billing.start_date = data.bill[0].end_date;
        } else {
          let his_first = await models.iots_history_3.findOne({
            attributes: ["reporttime", "meterreading", "device_id", "deveui"],
            where: { device_id: data.id, tenantId: data.tenantId },
            order: [["reportTime", "ASC"]],
          });
          billing.start_meter = his_first.meterreading;
          billing.start_date = his_first.reporttime;
        }
        let his_last = await models.iots_history_3.findOne({
          attributes: ["reporttime", "meterreading", "device_id", "deveui"],
          where: {
            device_id: data.id,
            tenantId: data.tenantId,
            reportTimeFlag: models.sequelize.where(
              models.sequelize.col("iots_history_3.reportTimeFlag"),
              "<=",
              data.time
            ),
          },
          order: [["reportTimeFlag", "DESC"]],
        });
        his_last = JSON.parse(JSON.stringify(his_last));
        billing.end_meter = his_last.meterreading;
        billing.end_date = his_last.reporttime;
        billing.totalizer = his_last.meterreading;
        billing.usage =
          parseInt(billing.end_meter) - parseInt(billing.start_meter);

        if (billing.usage < billing.minimum_charge_total) {
          billing.minimum_charge = billing.minimum_charge_total - billing.usage;
        }
        billing.billing_usage =
          parseFloat(billing.usage) + billing.minimum_charge;
        await models.iot_nodes.update(update, {
          where: { id: update.id },
        });
        let result = await models.iots_billing_history.create(billing);
        result = JSON.parse(JSON.stringify(result));
        return resolve(result);
      } catch (error) {
        console.log(error.message);
        return reject(error.message);
      }
    });
  },
};

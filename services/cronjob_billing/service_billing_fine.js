const moment = require("moment");
const models = require("../../models/index");
const { type_1 } = require("./billing_type_1");
const { Op } = models.Sequelize;

module.exports = {
  SvcGetFine: (data, inv = { id: 12, cut_date: "2021-04-31" }) => {
    return new Promise((resolve, reject) => {
      return models.iots_billing_history
        .findAll({
          where: {
            areaId: data.areaId,
            nodeId: data.id,
            cut_date: models.sequelize.where(
              models.sequelize.fn("date", models.sequelize.col("cut_date")),
              "<",
              moment().utcOffset("+0700").format("YYYY-MM-DD")
            ),
            [Op.or]: [
              { status: "UNPAID" },
              {
                status: "PAID",
                due_date: models.sequelize.where(
                  models.sequelize.fn("date", models.sequelize.col("due_date")),
                  "<",
                  models.sequelize.fn(
                    "date",
                    models.sequelize.col("payment_date")
                  )
                ),
              },
            ],
          },
          order: [["cut_date", "DESC"]],
          attributes: [
            "id",
            "billing_usage",
            "harga_satuan",
            "cut_date",
            "status",
            "due_date",
            "billing_usage",
            "payment_date",
            "nodeId",
          ],
        })
        .then((res) => {
          res = JSON.parse(JSON.stringify(res));
          let output = [];
          if (res.length > 0) {
            res.map((value, idx) => {
              if (idx == 0 && value.status == "UNPAID") {
                // kondisi inv sebelum create status unpaid
                let hari =
                  parseInt(moment(value.cut_date).endOf("month").format("DD")) -
                  parseInt(moment(value.due_date).format("DD"));
                let rp_usage =
                  ((parseFloat(value.billing_usage) / 1000) *
                    value.harga_satuan) /
                  1000;
                output.push({
                  status: value.status,
                  deskripsi: "denda invoice periode " + value.cut_date,
                  inv_id: value.id,
                  jumlah_hari: hari,
                  jumlah_denda: hari * rp_usage,
                  used: false,
                  link_inv_denda: inv.id,
                  bln_inv: inv.cut_date,
                  bln_denda: value.cut_date,
                  nodeId: value.nodeId,
                });
              } else if (idx == 0 && value.status == "PAID") {
                let hari =
                  parseInt(moment(value.payment_date).format("DD")) -
                  parseInt(moment(value.due_date).format("DD"));
                let rp_usage =
                  ((parseFloat(value.billing_usage) / 1000) *
                    value.harga_satuan) /
                  1000;
                output.push({
                  status: value.status,
                  deskripsi: "denda invoice periode " + value.cut_date,
                  inv_id: value.id,
                  jumlah_hari: hari,
                  jumlah_denda: hari * rp_usage,
                  used: false,
                  link_inv_denda: inv.id,
                  bln_inv: inv.cut_date,
                  bln_denda: value.cut_date,
                  nodeId: value.nodeId,
                });
              } else if (idx > 0 && value.status == "UNPAID") {
                let hari = parseInt(
                  moment(res[0].cut_date).endOf("month").format("DD")
                );
                let rp_usage =
                  ((parseFloat(value.billing_usage) / 1000) *
                    value.harga_satuan) /
                  1000;
                output.push({
                  status: value.status,
                  deskripsi: "denda invoice periode " + value.cut_date,
                  inv_id: value.id,
                  jumlah_hari: hari,
                  jumlah_denda: hari * rp_usage,
                  used: false,
                  link_inv_denda: inv.id,
                  bln_inv: inv.cut_date,
                  bln_denda: value.cut_date,
                  nodeId: value.nodeId,
                });
              } else if (idx > 0 && value.status == "PAID") {
                let hari =
                  parseInt(moment(value.payment_date).format("DD")) - 1;
                let rp_usage =
                  ((parseFloat(value.billing_usage) / 1000) *
                    value.harga_satuan) /
                  1000;
                output.push({
                  status: value.status,
                  deskripsi: "denda invoice periode " + value.cut_date,
                  inv_id: value.id,
                  jumlah_hari: hari,
                  jumlah_denda: hari * rp_usage,
                  used: false,
                  link_inv_denda: inv.id,
                  bln_inv: inv.cut_date,
                  bln_denda: value.cut_date,
                  nodeId: value.nodeId,
                });
              }
            });
            return models.iots_billing_fine
              .bulkCreate(output)
              .then(() => resolve("OK"))
              .catch((err) => resolve("error"));
          } else {
            resolve("Ok");
          }
        })
        .catch((err) => {
          console.log(err);
          resolve("error");
        });
    });
  },
};

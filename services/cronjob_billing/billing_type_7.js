const moment = require("moment");
const models = require("../../models/index");
const { Op } = models.Sequelize;

module.exports = {
  type_7: (data) => {
    return new Promise((resolve, reject) => {
      return models.iot_histories
        .findOne({
          attributes: ["payload"],
          where: { device_id: data.id, tenantId: data.tenantId },
          order: [["reportTime", "ASC"]],
        })
        .then((his_first) => {
          try {
            if (his_first == null) {
              console.log("his_first null", data.id);
              resolve("ok");
              return "null";
            }
            his_first = JSON.parse(his_first.payload);
            let update = {
              id: data.id,
              last_cut_date: moment().format("YYYY-MM-DD"),
            };
            let billing = {
              periode_cut: data.area.iots_cutoffs[0].order,
              minimum_charge: 0,
              no_meter: data.meter_id,
              minimum_charge_total:
                data.iot_tenant.minimum_charge_electricity_ct <= 0
                  ? parseFloat(data.area.minimum_charge_electricity_ct) * 1000
                  : parseFloat(data.iot_tenant.minimum_charge_electricity_ct) *
                    1000,
              //billing_type: data.area.billing_charge_type,
              areaId: data.areaId,
              tenantId: data.tenantId,
              nodeId: data.id,
              cut_date: moment()
                .utcOffset("+0700")
                .subtract(1, "day")
                .format("YYYY-MM-DD"),
              periode_billing: moment()
                .utcOffset("+0700")
                .subtract(1, "day")
                .format("YYYY-MM"),
              invoice: data.invoice,
              harga_satuan: data.price,
              ppn: data.ppn,
              biaya_transaksi: 5000,
              biaya_penyesuaian: 0,
              status: "NEW",
              correction_usage: 0,
              denda: 0,
              deveui: data.devEui,
              node_type: "ELECTRICITY CT",
              unit: "kwh",
              typeId: 7,
            };

            if (data.bill.length > 0) {
              billing.start_meter = data.bill[0].end_meter;
              billing.start_date = data.bill[0].end_date;
            } else {
              billing.start_meter = his_first.meter.meterReading;
              billing.start_date = his_first.reportTime;
            }
            return models.iot_histories
              .findOne({
                attributes: ["payload"],
                where: {
                  device_id: data.id,
                  tenantId: data.tenantId,
                  reportTimeFlag: models.sequelize.where(
                    models.sequelize.col("iot_histories.reportTimeFlag"),
                    "<=",
                    data.time
                  ),
                },
                order: [["reportTimeFlag", "DESC"]],
              })
              .then((his_last) => {
                his_last = JSON.parse(JSON.stringify(his_last));
                his_last = JSON.parse(his_last.payload);
                billing.end_meter = his_last.meter.meterReading;
                billing.end_date = his_last.reportTime;
                billing.totalizer = his_last.meter.meterReading;
                billing.usage =
                  parseInt(billing.end_meter) - parseInt(billing.start_meter);

                if (billing.usage < billing.minimum_charge_total) {
                  billing.minimum_charge =
                    billing.minimum_charge_total - billing.usage;
                }
                billing.billing_usage =
                  parseFloat(billing.usage) + billing.minimum_charge;
                return models.iot_nodes
                  .update(update, {
                    where: { id: update.id },
                  })
                  .then((result) => {
                    console.log(billing);
                    return models.iots_billing_history
                      .create(billing)
                      .then((result) => {
                        result = JSON.parse(JSON.stringify(result));
                        resolve(result);
                      })
                      .catch((err) => reject(err.message));
                  })
                  .catch((err) => reject(err.message));
              })
              .catch((err) => reject(err.message));
          } catch (error) {
            console.log(error);
            reject(error.message);
          }
        })
        .catch((err) => {
          console.log(err);
          reject(err.message);
        });
      // .then((result) => {})
      // .catch((err) => reject(err.message));
    });
  },
};

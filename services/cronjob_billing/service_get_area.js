const moment = require("moment");
const models = require("../../models/index");
const { Svchandle } = require("./service_handle_type");
const { SvcGetPricing1, SvcGetPricing2 } = require("./service_get_pricing");
const { Op } = models.Sequelize;

module.exports = {
  SvcGetArea1: (data) => {
    return new Promise((resolve, reject) => {
      return models.iot_area
        .findAll({
          attributes: ["id"],
          include: [
            {
              model: models.iots_cutoff,
              attributes: ["id", "areaId", "tanggal_cutoff"],
              required: true,
              where: {
                tanggal_cutoff: moment().utcOffset("+0700").format("DD"),
              },
            },
          ],
        })
        .then((res) => resolve(JSON.parse(JSON.stringify(res))))
        .catch(() => resolve("error"));
    });
  },
  SvcGetArea2: (data) => {
    return new Promise((resolve, reject) => {
      if (data.pricing_option == 1) {
        SvcGetPricing1(data)
          .then((price) => {
            if (price == null) {
              resolve("OK");
            } else {
              data.price = price.pricing;
              Svchandle(data)
                .then(() => resolve("OK"))
                .catch((error) => {
                  console.log(error);
                  resolve(error);
                });
            }
          })
          .catch((error) => {
            console.log(error);
            resolve(error);
          });
      } else {
        SvcGetPricing2(data)
          .then((price) => {
            if (price == null) {
              resolve("OK");
            } else {
              data.price = price.pricing;
              Svchandle(data)
                .then(() => resolve("OK"))
                .catch((error) => {
                  console.log(error);
                  resolve(error);
                });
            }
          })
          .catch((error) => {
            console.log(error);
            resolve(error);
          });
      }
    });
  },
  SvcGetArea3: (data) => {
    return new Promise((resolve, reject) => {
      console.log(data);
      //   resolve("ok");
    });
  },
  SvcGetArea: (data) => {
    return new Promise((resolve, reject) => {
      return models.iot_area
        .findAll({
          // where: { billing_charge_type: "tenant" },
          attributes: [
            "id",
            "pricing_option",
            "area_name",
            "minimum_charge_gas",
            "minimum_charge_water",
            "minimum_charge_electricity_ct",
            "minimum_charge_electricity_non_ct",
            "billing_charge_type",
            "is_ppn_value",
            "is_ppn",
          ],
          include: [
            {
              model: models.iots_cutoff,
              attributes: ["id", "areaId", "tanggal_cutoff", "time", "order"],
              required: true,
              where: {
                tanggal_cutoff: moment().utcOffset("+0700").format("DD"),
                time: models.sequelize.where(
                  models.sequelize.fn(
                    "TIME",
                    models.sequelize.col("iots_cutoffs.time")
                  ),
                  "<=",
                  moment()
                    .subtract(2, "hour")
                    .utcOffset("+0700")
                    .format("HH:mm:ss")
                ),
                // time: {
                //   [Op.lte]: moment()
                //     .subtract(2, "hour")
                //     .utcOffset("+0700")
                //     .format("HH:mm:ss"),
                // },
              },
            },
            {
              model: models.iot_nodes,
              //   limit: 3,
              where: {
                is_unsigned: false,
                tenantId: { [Op.not]: null },
                last_update: { [Op.not]: null },
                device_type_id: 1,
                typeId: { [Op.in]: [1, 2, 3, 7] },
                last_cut_date: {
                  [Op.or]: [
                    null,
                    models.sequelize.where(
                      models.sequelize.fn(
                        "date",
                        models.sequelize.col("iot_nodes.last_cut_date")
                      ),
                      "<",
                      moment().utcOffset("+0700").format("YYYY-MM-DD")
                    ),
                  ],
                },
              },
              include: [
                {
                  model: models.iot_tenant,
                },
                {
                  model: models.iots_billing_history,
                  attributes: {
                    exclude: ["log", "createdAt", "updatedAt"],
                  },
                  order: [["id", "DESC"]],
                  as: "bill",
                  limit: 1,
                },
              ],
            },
          ],
        })
        .then((area) => {
          area = JSON.parse(JSON.stringify(area));

          area = area.filter((valP) => {
            let cut = valP.iots_cutoffs.find(
              (valF) =>
                valF.tanggal_cutoff == moment().utcOffset("+0700").format("DD")
            );
            // return cut != null;
            if (cut != null) {
              let ed = [];
              let jam = moment()
                .subtract(2, "hour")
                .utcOffset("+0700")
                .format("HHmmss");
              cut.time = cut.time.split(":");
              for (let idx = 0; idx < 3; idx++) {
                ed.push(cut.time[idx] || "00");
              }
              cut.time = ed.join(":");
              ed = ed.join("");
              return parseInt(ed) <= parseInt(jam);
            }
          });
          if (area.length > 0) {
            let device = [];
            area.map((value) => {
              value.iot_nodes.map((value2) => {
                let invoice =
                  "We" +
                  "." +
                  value.area_name +
                  "." +
                  value2.iot_tenant.id +
                  "-INV-" +
                  moment().subtract(1, "day").format("DDMMYY") +
                  "-" +
                  value2.iot_tenant.id +
                  "G";
                // if (value2.bill.length == 0) {
                //   invoice += "-1";
                // } else {
                //   let temp_bill = value2.bill[0].invoice.split("-");
                //   if (
                //     temp_bill[2] !== undefined &&
                //     temp_bill[2].slice(4, 6) ==
                //       moment().utcOffset("+0700").format("YY")
                //   ) {
                //     invoice += "-" + (parseInt(temp_bill[3]) + 1).toString();
                //   } else {
                //     invoice += "-1";
                //   }
                // }
                let ppn = 0;
                if (value2.iot_tenant.is_ppn == true) {
                  ppn = value.is_ppn_value;
                }
                device.push({
                  ppn: ppn,
                  time:
                    value.iots_cutoffs.length > 0
                      ? moment().utcOffset("+0700").format("YYYY-MM-DD") +
                        " " +
                        value.iots_cutoffs[0].time
                      : moment().utcOffset("+0700").format("YYYY-MM-DD") +
                        " 00:00:00",
                  area: value,
                  invoice: invoice,
                  id: value2.id,
                  pricing_option: value.pricing_option,
                  ...value2,
                });
              });
            });
            // return console.log(device);
            resolve(device);
          } else {
            resolve(area);
          }
        })
        .catch((error) => {
          console.log(error, "eror");
          reject(error);
        });
    });
  },
};

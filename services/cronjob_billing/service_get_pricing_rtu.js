const moment = require("moment");
const models = require("../../models/index");
const { type_1 } = require("./billing_type_1");
const { Op } = models.Sequelize;

module.exports = {
  SvcGetPricing1: (data) => {
    return new Promise((resolve, reject) => {
      return models.iot_pricing_postpaid_all
        .findOne({
          where: {
            areaId: data.areaId,
            typeId: data.rtu_pricing_id,
          },
        })
        .then((res) => resolve(JSON.parse(JSON.stringify(res))))
        .catch(() => resolve("error"));
    });
  },
  SvcGetPricing2: (data) => {
    return new Promise((resolve, reject) => {
      return models.iot_pricing_postpaid
        .findOne({
          where: {
            typeId: data.rtu_pricing_id,
            memberId: data.iot_tenant.member_level_id,
          },
        })
        .then((res) => resolve(JSON.parse(JSON.stringify(res))))
        .catch(() => resolve("error"));
    });
  },
};

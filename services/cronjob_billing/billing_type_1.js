const moment = require("moment");
const models = require("../../models/index");
const { Op } = models.Sequelize;

module.exports = {
  type_1: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        let update = {
          id: data.id,
          last_cut_date: moment().format("YYYY-MM-DD"),
        };
        let billing = {
          periode_cut: data.area.iots_cutoffs[0].order,
          minimum_charge: 0,
          no_meter: data.meter_id,
          minimum_charge_total:
            data.iot_tenant.minimum_charge_gas <= 0
              ? parseFloat(data.area.minimum_charge_gas) * 1000
              : parseFloat(data.iot_tenant.minimum_charge_gas) * 1000,
          //billing_type: data.area.billing_charge_type,
          areaId: data.areaId,
          tenantId: data.tenantId,
          nodeId: data.id,
          cut_date: moment()
            .utcOffset("+0700")
            .subtract(1, "day")
            .format("YYYY-MM-DD"),
          periode_billing: moment()
            .utcOffset("+0700")
            .subtract(1, "day")
            .format("YYYY-MM"),
          invoice: data.invoice,
          harga_satuan: data.price,
          ppn: data.ppn,
          biaya_transaksi: 5000,
          biaya_penyesuaian: 0,
          status: "NEW",
          correction_usage: 0,
          denda: 0,
          deveui: data.devEui,
          node_type: "GAS",
          unit: "m3",
          typeId: 1,
        };
        let filter = {};

        if (data.bill.length > 0) {
          filter = {
            reportTimeFlag1: models.sequelize.where(
              models.sequelize.col("iots_history_gas.reportTimeFlag"),
              "<=",
              data.time
            ),
            reportTimeFlag2: models.sequelize.where(
              models.sequelize.col("iots_history_gas.reportTimeFlag"),
              ">=",
              moment(billing.end_date)
                .utcOffset("+0700")
                .format("YYYY-MM-DD HH:mm:ss")
            ),
          };
        } else {
          filter = {
            reportTimeFlag: models.sequelize.where(
              models.sequelize.col("iots_history_gas.reportTimeFlag"),
              "<=",
              data.time
            ),
          };
        }

        let his_last = await models.iots_history_gas.findAll({
          attributes: [
            "reportTime",
            "meterReading",
            "reportTimeFlag",
            "max_totalizer",
          ],
          where: [
            {
              device_id: data.id,
            },
            filter,
          ],
          order: [["reportTimeFlag", "ASC"]],
        });

        his_last = JSON.parse(JSON.stringify(his_last));

        billing.start_meter = his_last[0].meterReading;
        billing.start_date = his_last[0].reportTime;
        billing.end_meter = his_last[his_last.length - 1].meterReading;
        billing.end_date = his_last[his_last.length - 1].reportTime;
        billing.totalizer = billing.end_meter;
        let usage = 0;
        let t_date = null;
        let fl = [];
        let t_usage = 0;
        his_last.map((val, idx) => {
          if (idx == 0) {
            val.usage = 0;
          } else {
            if (
              parseFloat(his_last[idx - 1].max_totalizer) ==
              parseFloat(his_last[idx - 1].meterReading)
            ) {
              val.usage = parseFloat(val.meterReading) - 0;
            } else {
              val.usage =
                parseFloat(val.meterReading) -
                parseFloat(his_last[idx - 1].meterReading);
            }
          }
          usage += val.usage;
          let date_t = moment(val.reportTimeFlag).format("YYYY-MM-DD");
          if (date_t != t_date) {
            t_date = date_t;
            t_usage = val.usage;
            if (fl.length < 1) {
              fl.push({
                start_date: val.reportTimeFlag,
                start_meter: val.meterReading,
                end_meter: val.meterReading,
                end_date: val.reportTimeFlag,
                usage: val.usage,
              });
            } else {
              fl.push({
                start_date: fl[fl.length - 1].end_date,
                start_meter: fl[fl.length - 1].end_meter,
                end_meter: fl[fl.length - 1].end_meter,
                end_date: fl[fl.length - 1].end_date,
                usage: val.usage,
              });
            }
          } else {
            fl[fl.length - 1].end_meter = val.meterReading;
            fl[fl.length - 1].end_date = val.reportTimeFlag;
            fl[fl.length - 1].usage += val.usage;
          }
        });
        let msg = JSON.stringify({
          responseCode: 200,
          response: {
            log: fl,
            total: usage,
          },
        });
        billing.usage = usage;
        if (billing.usage < billing.minimum_charge_total) {
          billing.minimum_charge = billing.minimum_charge_total - billing.usage;
        }
        billing.billing_usage =
          parseFloat(billing.usage) + billing.minimum_charge;
        billing.log = msg;
        billing.log_flag = true;
        await models.iot_nodes.update(update, {
          where: { id: update.id },
        });
        let result = await models.iots_billing_history.create(billing);
        result = JSON.parse(JSON.stringify(result));
        resolve(result);
      } catch (error) {
        console.log(error);
        reject(error.message);
      }
    });
  },
};

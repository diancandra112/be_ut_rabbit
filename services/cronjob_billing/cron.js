const corn = require("node-cron");
const moment = require("moment");
const axios = require("axios");
const models = require("../../models/index");
const { Op } = models.Sequelize;
const {
  SvcGetArea2,
  SvcGetArea,
  SvcGetArea1,
  SvcGetArea3,
} = require("./service_get_area");
const { SvcGetAreaRtu, SvcGetAreaRtu2 } = require("./service_get_area_rtu");

const GenerateBillingV2 = () => {
  //   const job = corn.schedule("*/30 */2 * * *", () => {
  const job = corn.schedule("*/30 * * * *", () => {
    job.stop();
    console.log("running create billing");
    SvcGetArea()
      .then((area) => {
        console.log(area, "area");
        area
          .reduce(
            (chain, item) => chain.then(() => SvcGetArea2(item)),
            Promise.resolve()
          )
          .then((s) => {
            console.log(s, "ok");
            job.start();
          })
          .catch((e) => {
            console.log(e, "awdawd");
            job.start();
          });
      })
      .catch((e) => {
        console.log("error");
        job.start();
      });
  });
};

const GenerateBillingRtuV2 = () => {
  //   const job = corn.schedule("*/30 */2 * * *", () => {
  const job = corn.schedule("*/30 * * * *", () => {
    job.stop();
    console.log("running create billing");
    SvcGetAreaRtu()
      .then((area) => {
        console.log(area, "area");
        area
          .reduce(
            (chain, item) => chain.then(() => SvcGetAreaRtu2(item)),
            Promise.resolve()
          )
          .then((s) => {
            console.log(s, "ok");
            job.start();
          })
          .catch((e) => {
            console.log(e, "awdawd");
            job.start();
          });
      })
      .catch((e) => {
        console.log("error");
        job.start();
      });
  });
};

const DeleteChart = () => {
  // const job = corn.schedule("* * * * * *", () => {
  const job = corn.schedule("10 5 * * *", () => {
    job.stop();
    console.log("running create billing");
    models.iots_charts_usage
      .destroy({
        where: {
          fdate: {
            [Op.lt]: moment()
              .utcOffset("+0700")
              .subtract(6, "month")
              .format("YYYY-MM-DD"),
          },
        },
      })
      .then((area) => {
        job.start();
      })
      .catch((e) => {
        console.log("error");
        job.start();
      });
  });
};
const DeleteAnomali = () => {
  // const job = corn.schedule("* * * * * *", () => {
  const job = corn.schedule("10 5 * * *", () => {
    job.stop();
    console.log("running create billing");
    models.iots_anomali_history
      .destroy({
        where: {
          last_update: models.sequelize.where(
            models.sequelize.fn(
              "date",
              models.sequelize.col("iots_anomali_history.last_update")
            ),
            "<",
            moment()
              .utcOffset("+0700")
              .subtract(31, "days")
              .format("YYYY-MM-DD")
          ),
        },
      })
      .then((area) => {
        job.start();
      })
      .catch((e) => {
        console.log("error");
        job.start();
      });
  });
};

const InsetLog = () => {
  // const job = corn.schedule("* * * * * *", () => {
  const job = corn.schedule("*/5 * * * *", () => {
    job.stop();
    console.log("running log insert");
    models.iots_billing_history
      .findOne({
        where: {
          log_flag: false,
        },
        order: [["id", "DESC"]],
      })
      .then((log) => {
        log = JSON.parse(JSON.stringify(log));
        if (log == null) {
          job.start();
          return "ok";
        } else {
          console.log(log.id);
          axios
            .get(
              `http://127.0.0.1:3005/api/billing/usage/${log.areaId}?nodeId=${log.nodeId}&startDate=${log.start_date}&endDate=${log.end_date}`
            )

            .then((log_push) => {
              models.iots_billing_history.update(
                {
                  log: JSON.stringify(log_push.data),
                  log_flag: true,
                },
                {
                  where: {
                    id: log.id,
                  },
                }
              );
              job.start();
            })
            .catch((e) => {
              console.log(e, "error axios");
              models.iots_billing_history.update(
                {
                  log_flag: true,
                },
                {
                  where: {
                    id: log.id,
                  },
                }
              );
              job.start();
            });
        }
      })
      .catch((e) => {
        console.log(e, "error");
        job.start();
      });
  });
};

const areaNomor = () => {
  // const job = corn.schedule("* * * * * *", () => {
  const job = corn.schedule("1 */1 * * *", () => {
    job.stop();
    models.iot_area
      .findAll({
        where: {
          nomor_pelanggan: null,
        },
        attributes: ["id", "nomor_pelanggan", "area_name"],
      })
      .then((log) => {
        log = JSON.parse(JSON.stringify(log));
        let update = [];
        if (log.length == 0) {
          console.log("running nomor pelanggan null");
          return job.start();
        }
        console.log("running nomor pelanggan Insert");
        log.map((value) => {
          update.push({
            id: value.id,
            nomor_pelanggan: `POC.${value.area_name}.${value.id}`,
          });
        });
        models.iot_area
          .bulkCreate(update, {
            updateOnDuplicate: ["nomor_pelanggan"],
          })
          .then((log_push) => {
            return job.start();
          })
          .catch((e) => {
            console.log(e, "error axios");
            return job.start();
          });
      })
      .catch((e) => {
        console.log(e, "error");
        job.start();
      });
  });
};
module.exports = {
  all2: () => {
    GenerateBillingV2();
    GenerateBillingRtuV2();
    DeleteChart();
    DeleteAnomali();
    InsetLog();
    areaNomor();
  },
};

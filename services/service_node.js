const models = require("../models/index");
const axios = require("axios");
const { Op } = require("sequelize");
const { sequelize } = require("../models/index");
const moment = require("moment");
require("dotenv").config();

function romanize(num) {
  if (isNaN(num)) return NaN;
  var digits = String(+num).split(""),
    key = [
      "",
      "C",
      "CC",
      "CCC",
      "CD",
      "D",
      "DC",
      "DCC",
      "DCCC",
      "CM",
      "",
      "X",
      "XX",
      "XXX",
      "XL",
      "L",
      "LX",
      "LXX",
      "LXXX",
      "XC",
      "",
      "I",
      "II",
      "III",
      "IV",
      "V",
      "VI",
      "VII",
      "VIII",
      "IX",
    ],
    roman = "",
    i = 3;
  while (i--) roman = (key[+digits.pop() + i * 10] || "") + roman;
  return Array(+digits.join("") + 1).join("M") + roman;
}

module.exports = {
  serviceAddNode: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_nodes.bulkCreate(data, {
            updateOnDuplicate: ["devEui"],
          });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: "Berhasil terdaftar",
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error,
          });
        });
    });
  },
  serviceDeleteIstallation: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        transaction = await models.sequelize.transaction();
        let node_find = await models.iot_nodes.findOne({
          where: {
            devEui: data.devEUI,
          },
          attributes: ["id", "devEUI", "tenantId", "internalId"],
          transaction,
        });
        node_find = JSON.parse(JSON.stringify(node_find));
        console.log(node_find);
        if (node_find.tenantId != null || node_find.internalId != null) {
          return reject({
            responseCode: 400,
            messages: "node masih terpasang",
          });
        } else {
          await models.iot_nodes.destroy({
            where: {
              id: node_find.id,
            },
            transaction,
          });
          axios.post(process.env.HOST_SERVER + "/ussigned/node", data);
          await transaction.commit();
          return resolve({
            responseCode: 200,
            messages: "Berhasil delete nodes",
          });
        }
      } catch (error) {
        console.log(error);
        try {
          if (transaction) await transaction.rollback();
        } catch (error) {}
        reject({
          responseCode: 400,
          messages: "gagal update installation",
          err: error.message,
        });
      }
    });
  },
  serviceAdjustBalace: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_nodes.update(data, { where: { id: data.id } });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: "Berhasil update prepayment",
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: "Gagal update prepayment",
            err: error,
          });
        });
    });
  },
  serviceUpdateIstallation: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_nodes.update(data, { where: { id: data.id } });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: "Berhasil update installation",
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: "Gagal update installation",
            err: error,
          });
        });
    });
  },
  serviceRegisterNode: (data, company_id) => {
    return new Promise((resolve, reject) => {
      data.installationDate = new Date();
      return models.sequelize
        .transaction((t) => {
          return models.iot_companies
            .findOne({
              where: {
                id: company_id,
              },
              include: [
                { model: models.iot_users },
                {
                  model: models.iot_area,
                  as: "list_area",
                  where: { id: data.areaId },
                  attributes: [
                    "id",
                    "pressure",
                    "water_pressure",
                    "pressure_pt",
                    "low_pressure",
                    "interval_pressure",
                    "interval_water_pressure",
                    "interval_pressure_pt",
                    "interval_low_pressure",
                  ],
                },
              ],
              transaction: t,
            })
            .then((resUsers) => {
              resUsers = JSON.parse(JSON.stringify(resUsers));
              resUsers.list_area.map((val, index) => {
                if (data.typeId == 5) {
                  data.alarm_pressure = val.pressure;
                  data.interval_alarm_pressure = val.interval_pressure;
                } else if (data.typeId == 8) {
                  data.alarm_pressure = val.water_pressure;
                  data.interval_alarm_pressure = val.interval_water_pressure;
                } else if (data.typeId == 9) {
                  data.alarm_pressure = val.pressure_pt;
                  data.interval_alarm_pressure = val.interval_pressure_pt;
                } else if (data.typeId == 11) {
                  data.alarm_pressure = val.low_pressure;
                  data.interval_alarm_pressure = val.interval_low_pressure;
                }
              });

              let ns_data = {
                child_app_name: data.child_app_name,
                child_sn: data.child_sn,
                email: resUsers.iot_user.email,
                devEUI: data.devEui,
              };
              axios
                .patch(process.env.HOST_SERVER + "/node", ns_data)
                .then((result) => {
                  data.setting_interval = data.interval;
                  data.live_interval = data.interval;
                  return models.iot_nodes
                    .create(data)
                    .then((result2) => {
                      console.log(result2);
                      resolve({
                        responseCode: 200,
                        messages: "Berhasil terdaftar",
                      });
                    })
                    .catch((error) => {
                      console.log(error);
                      reject({
                        responseCode: 400,
                        messages: error.response.data,
                      });
                    });
                })
                .catch((error) => {
                  // console.log(error);
                  reject({
                    responseCode: 400,
                    messages: error.response.data,
                  });
                });
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: "Berhasil terdaftar",
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error,
          });
        });
    });
  },
  serviceAssignedNode: (data, update) => {
    return new Promise((resolve, reject) => {
      data.model = data.model || null;
      data.serial_number = data.serial_number || null;
      return models.sequelize
        .transaction((t) => {
          return models.iot_nodes
            .findOne({
              where: {
                devEui: data.devEui,
                child_app_name: data.model,
                child_sn: data.serial_number,
              },
              include: [
                { model: models.iot_device_type },
                { model: models.iot_node_type },
              ],
              transaction: t,
            })
            .then(async (result) => {
              result = JSON.parse(JSON.stringify(result));
              if (result == null) {
                result = await models.iot_nodes.findOne({
                  where: {
                    devEui: data.devEui,
                  },
                  include: [
                    { model: models.iot_device_type },
                    { model: models.iot_node_type },
                  ],
                  transaction: t,
                });
              }
              result = JSON.parse(JSON.stringify(result));
              data.node_id = result.id;
              if (result.iot_node_type.type_id == 16) {
                data.serial_number = result.child_sn;
                data.model = result.child_app_name;
              }
              if ([4, 13, 14, 16].includes(result.iot_node_type.type_id)) {
                update.meter_id = data.serial_number;
              }
              return models.iot_nodes
                .update(update, {
                  where: { id: result.id },
                })
                .then(() => {
                  models.iot_histories.destroy({
                    where: { device_id: result.id },
                  });
                  switch (result.iot_node_type.type_id) {
                    case 16:
                      console.log("RTU NEW");
                      data.interval = result.interval;
                      return models.iot_detail_rtu.create(data, {
                        transaction: t,
                      });
                      break;
                    case 14:
                      console.log("RTU DLMS");
                      data.interval = result.interval;
                      return models.iot_detail_rtu.create(data, {
                        transaction: t,
                      });
                      break;
                    case 1:
                      console.log("GAS");
                      data.interval = result.interval;
                      return models.iot_detail_gas.create(data, {
                        transaction: t,
                      });
                      break;
                    case 2:
                      console.log("WATER");
                      data.interval = result.interval;
                      return models.iot_detail_water.create(data, {
                        transaction: t,
                      });
                      break;
                    case 3:
                      console.log("ELECTRIC NON CT");
                      data.interval = result.interval;
                      return models.iot_detail_electric.create(data, {
                        transaction: t,
                      });
                      break;
                    case 4:
                      console.log("RTU");
                      data.interval = result.interval;
                      return models.iot_detail_rtu.create(data, {
                        transaction: t,
                      });
                      break;
                    case 13:
                      console.log("RTU");
                      data.interval = result.interval;
                      return models.iot_detail_rtu.create(data, {
                        transaction: t,
                      });
                      break;
                    case 5:
                      console.log("PREASURE");
                      data.interval = result.interval;
                      return models.iot_detail_preassure.create(data, {
                        transaction: t,
                      });
                      break;
                    case 11:
                      console.log("PREASURE SINDCON");
                      data.interval = result.interval;
                      return models.iot_detail_preassure.create(data, {
                        transaction: t,
                      });
                      break;
                    case 12:
                      console.log("PREASURE HIGH SINDCON");
                      data.interval = result.interval;
                      return models.iot_detail_preassure.create(data, {
                        transaction: t,
                      });
                      break;
                    case 9:
                      console.log("PREASURE PT");
                      data.interval = result.interval;
                      return models.iot_detail_preassure.create(data, {
                        transaction: t,
                      });
                      break;
                    case 10:
                      console.log("PJU");
                      data.interval = result.interval;
                      return models.iot_detail_pju.create(data, {
                        transaction: t,
                      });
                      break;
                    case 8:
                      console.log("WATER PREASURE");
                      data.interval = result.interval;
                      return models.iot_detail_preassure.create(data, {
                        transaction: t,
                      });
                      break;
                    case 6:
                      console.log("TEMPERATURE");
                      data.interval = result.interval;
                      return models.iot_detail_temperature.create(data, {
                        transaction: t,
                      });
                      break;
                    case 7:
                      console.log("ELECTRIC CT");
                      data.interval = result.interval;
                      return models.iot_detail_electricct.create(data, {
                        transaction: t,
                      });
                      break;

                    default:
                      break;
                  }
                });
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: "Berhasil terdaftar",
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error,
          });
        });
    });
  },

  serviceUnassignedNodev2: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        transaction = await models.sequelize.transaction();
        let _res = await models.iot_nodes.findOne({
          where: data,
          include: [{ model: models.iot_tenant }],
        });
        _res = JSON.parse(JSON.stringify(_res));
        let new_node = {
          areaId: _res.areaId,
          interval: _res.interval,
          typeId: _res.typeId,
          device_type_id: _res.device_type_id,
          image_url: _res.image_url,
          devEui: _res.devEui,
          description: _res.description,
          installationDate: _res.installationDate,
          previous_update: _res.previous_update,
          live_interval: _res.live_interval,
          live_battery: _res.live_battery,
          start_anomali_date: _res.start_anomali_date,
          start_anomali_meter: _res.start_anomali_meter,
          is_anomali: _res.is_anomali,
          live_valve: _res.live_valve,
          setting_valve: _res.setting_valve,
          setting_interval: _res.setting_interval,
          rtu_pricing_id: _res.rtu_pricing_id,
          rtu_pricing_name: _res.rtu_pricing_name,
          latitude: _res.latitude,
          longitude: _res.longitude,
          altitude: _res.altitude,
          address: _res.address,
          merk: _res.merk,
          meter_id: _res.meter_id,
          prepayment: _res.prepayment,
          live_prepayment: _res.live_prepayment,
          live_prepayment_date: _res.live_prepayment_date,
          field_billing_rtu: _res.field_billing_rtu,
          line_anomali: _res.line_anomali,
          line_offline: _res.line_offline,
          line_low_battery: _res.line_low_battery,
          line_prepaid: _res.line_prepaid,
          email_anomali: _res.email_anomali,
          email_offline: _res.email_offline,
          email_low_battery: _res.email_low_battery,
          email_prepaid: _res.email_prepaid,
          anomali_usage_start_date: _res.anomali_usage_start_date,
          anomali_usage_end_date: _res.anomali_usage_end_date,
          max_usage: _res.max_usage,
          minim_balance_gas: _res.minim_balance_gas,
          minim_balance_water: _res.minim_balance_water,
          alarm_pressure: _res.alarm_pressure,
          interval_alarm_pressure: _res.interval_alarm_pressure,
          last_alarm_pressure: _res.last_alarm_pressure,
        };
        await models.iot_nodes.update(
          {
            is_unsigned: true,
            devEui: "unsigned_" + _res.devEui + "_id_" + _res.id,
          },
          { where: { id: _res.id }, transaction }
        );
        await models.iot_nodes.create(new_node, {
          transaction,
        });
        if (
          _res.tenantId == null ||
          _res.device_type_id == 2 ||
          [1, 2, 3, 7].includes(_res.typeId) == false ||
          _res.live_last_meter == null
        ) {
          await transaction.commit();
          return resolve({
            responseCode: 200,
            messages: `berhasil unsigned node`,
          });
        } else {
          let price = 0;
          let bill_his = await models.iots_billing_history.findOne({
            where: {
              nodeId: _res.id,
            },
            order: [["id", "DESC"]],
          });
          let hist_first = null;
          let area = await models.iot_area.findOne({
            where: { id: _res.areaId },
          });

          let billing = {
            periode_billing: moment().utcOffset("+0700").format("YYYY-MM"),
            areaId: _res.areaId,
            tenantId: _res.tenantId,
            nodeId: _res.id,
            ppn: area.is_ppn_value,
            biaya_transaksi: 5000,
            correction_usage: 0,
            denda: 0,
            biaya_penyesuaian: 0,
            cut_date: moment().utcOffset("+0700").format("YYYY-MM-DD"),
            status: "NEW",
            end_meter: _res.live_last_meter,
            end_date: _res.last_update,
          };

          if (bill_his == null) {
            hist_first = await models.iot_histories.findOne({
              where: {
                device_id: _res.id,
              },
            });
            billing.start_date = hist_first.reportTime;
            billing.start_meter = JSON.parse(
              hist_first.payload
            ).meter.meterReading;
            billing.invoice = `We.${area.area_name}.${
              _res.tenantId
            }-INV-${moment().format("DDMMYY")}-${_res.tenantId}G`;
          } else {
            billing.start_meter = bill_his.end_meter;
            billing.start_date = bill_his.end_date;
            billing.invoice = `We.${area.area_name}.${
              _res.tenantId
            }-INV-${moment().format("DDMMYY")}-${_res.tenantId}G`;
          }
          let bil_count = await models.iots_billing_history.count({
            where: {
              tenantId: _res.tenantId,
              cut_date: moment().utcOffset("+0700").format("YYYY-MM-DD"),
            },
          });
          billing.invoice = billing.invoice + "-" + romanize(bil_count + 1);
          billing.totalizer = parseFloat(billing.end_meter);
          billing.billing_usage =
            parseFloat(billing.end_meter) - parseFloat(billing.start_meter);
          billing.usage =
            parseFloat(billing.end_meter) - parseFloat(billing.start_meter);

          if (area.pricing_option == 1) {
            let price_all = await models.iot_pricing_postpaid_all.findOne({
              where: {
                areaId: _res.areaId,
                typeId: _res.typeId,
              },
            });
            if (price_all != null) {
              price = price_all.pricing;
            }
          } else {
            let price_member = await models.iot_pricing_postpaid.findOne({
              where: {
                typeId: _res.typeId,
                memberId: _res.iot_tenant.member_level_id,
              },
            });
            if (price_member != null) {
              price = price_member.pricing;
            }
          }

          billing.periode_cut = area.automatic_bill;
          billing.typeId = _res.typeId;
          if (_res.typeId == 1 || _res.typeId == 2) {
            billing.unit = "m3";
          } else if (_res.typeId == 3 || _res.typeId == 7) {
            billing.unit = "kwh";
          }
          billing.node_type =
            _res.typeId == 1
              ? "GAS"
              : _res.typeId == 2
              ? "WATER"
              : _res.typeId == 3
              ? "ELECTRICITY NON CT"
              : _res.typeId == 7
              ? "ELECTRICITY CT"
              : null;
          billing.deveui = _res.devEui;
          billing.harga_satuan = price;
          await models.iots_billing_history.create(billing, {
            transaction,
          });
          await transaction.commit();
          return resolve({
            responseCode: 200,
            messages: `berhasil unsigned node`,
          });
        }
      } catch (error) {
        console.log(error);
        if (transaction) await transaction.rollback();
        reject({ responseCode: 500, message: error.message });
      }
    });
  },
  serviceUnassignedNode: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_nodes
            .findOne({
              where: data,
              include: [
                {
                  model: models.iots_billing_history,
                  order: [["id", "DESC"]],
                  as: "last_billing",
                  // limit: 1,
                },
                { model: models.iot_histories, as: "history_start", limit: 1 },
                {
                  model: models.iot_tenant,
                  include: [
                    {
                      model: models.iot_member_level,
                      include: [
                        {
                          model: models.iot_pricing_postpaid,
                          as: "child",
                          on: {
                            col1: models.sequelize.where(
                              models.sequelize.col(
                                "iot_tenant->iot_member_level.id"
                              ),
                              "=",
                              models.sequelize.col(
                                "iot_tenant->iot_member_level->child.memberId"
                              )
                            ),
                            col2: models.sequelize.where(
                              models.sequelize.col("iot_nodes.typeId"),
                              "=",
                              models.sequelize.col(
                                "iot_tenant->iot_member_level->child.typeId"
                              )
                            ),
                          },
                        },
                      ],
                    },
                  ],
                },
                {
                  model: models.iot_area,
                  attributes: { exclude: ["createdAt", "updatedAt", "image"] },
                  include: [
                    {
                      model: models.iots_cutoff,
                    },
                    {
                      model: models.iot_pricing_postpaid_all,
                      on: {
                        col1: models.sequelize.where(
                          models.sequelize.col("iot_area.id"),
                          "=",
                          models.sequelize.col(
                            "iot_area->iot_pricing_postpaid_alls.areaId"
                          )
                        ),
                        col2: models.sequelize.where(
                          models.sequelize.col("iot_nodes.typeId"),
                          "=",
                          models.sequelize.col(
                            "iot_area->iot_pricing_postpaid_alls.typeId"
                          )
                        ),
                      },
                    },
                  ],
                },
                {
                  model: models.iot_histories,
                  as: "history_end",
                  limit: 1,
                  order: [["reportTime", "DESC"]],
                },
              ],
            })
            .then((_res) => {
              try {
                _res = JSON.parse(JSON.stringify(_res));
                if (_res.is_unsigned == true) {
                  throw new Error("node telah ter unsigned sebelumnya");
                }
                let new_node = {
                  areaId: _res.areaId,
                  interval: _res.interval,
                  typeId: _res.typeId,
                  device_type_id: _res.device_type_id,
                  image_url: _res.image_url,
                  devEui: _res.devEui,
                  description: _res.description,
                  installationDate: _res.installationDate,
                  previous_update: _res.previous_update,
                  live_interval: _res.live_interval,
                  live_battery: _res.live_battery,
                  start_anomali_date: _res.start_anomali_date,
                  start_anomali_meter: _res.start_anomali_meter,
                  is_anomali: _res.is_anomali,
                  live_valve: _res.live_valve,
                  setting_valve: _res.setting_valve,
                  setting_interval: _res.setting_interval,
                  rtu_pricing_id: _res.rtu_pricing_id,
                  rtu_pricing_name: _res.rtu_pricing_name,
                  latitude: _res.latitude,
                  longitude: _res.longitude,
                  altitude: _res.altitude,
                  address: _res.address,
                  merk: _res.merk,
                  meter_id: _res.meter_id,
                  prepayment: _res.prepayment,
                  live_prepayment: _res.live_prepayment,
                  live_prepayment_date: _res.live_prepayment_date,
                  field_billing_rtu: _res.field_billing_rtu,
                  line_anomali: _res.line_anomali,
                  line_offline: _res.line_offline,
                  line_low_battery: _res.line_low_battery,
                  line_prepaid: _res.line_prepaid,
                  email_anomali: _res.email_anomali,
                  email_offline: _res.email_offline,
                  email_low_battery: _res.email_low_battery,
                  email_prepaid: _res.email_prepaid,
                  anomali_usage_start_date: _res.anomali_usage_start_date,
                  anomali_usage_end_date: _res.anomali_usage_end_date,
                  max_usage: _res.max_usage,
                  minim_balance_gas: _res.minim_balance_gas,
                  minim_balance_water: _res.minim_balance_water,
                  alarm_pressure: _res.alarm_pressure,
                  interval_alarm_pressure: _res.interval_alarm_pressure,
                  last_alarm_pressure: _res.last_alarm_pressure,
                };
                if (_res == null) return _res;
                if (_res.history_start.length == 0) {
                  return models.iot_nodes
                    .update(
                      {
                        is_unsigned: true,
                        devEui: "unsigned_" + _res.devEui + "_id_" + _res.id,
                      },
                      { where: { id: _res.id }, transaction: t }
                    )
                    .then((res_update) => {
                      return models.iot_nodes.create(new_node, {
                        transaction: t,
                      });
                    });
                }

                if (
                  (_res.typeId == 1 &&
                    _res.device_type_id == 1 &&
                    _res.tenantId != null) ||
                  (_res.typeId == 2 &&
                    _res.device_type_id == 1 &&
                    _res.tenantId != null) ||
                  (_res.typeId == 3 &&
                    _res.device_type_id == 1 &&
                    _res.tenantId != null) ||
                  (_res.typeId == 7 &&
                    _res.device_type_id == 1 &&
                    _res.tenantId != null)
                ) {
                  let billing = {
                    periode_billing:
                      parseInt(
                        _res.iot_area.iots_cutoffs[
                          _res.iot_area.iots_cutoffs.length - 1
                        ].tanggal_cutoff
                      ) > parseInt(moment().format("DD"))
                        ? moment().utcOffset("+0700").format("YYYY-MM")
                        : moment()
                            .utcOffset("+0700")
                            .add(1, "month")
                            .format("YYYY-MM"),
                    areaId: _res.areaId,
                    tenantId: _res.tenantId,
                    nodeId: _res.id,
                    ppn: 10,
                    biaya_transaksi: 5000,
                    correction_usage: 0,
                    denda: 0,
                    biaya_penyesuaian: 0,
                    cut_date: moment().utcOffset("+0700").format("YYYY-MM-DD"),
                    status: "NEW",
                    end_meter: JSON.parse(_res.history_end[0].payload).meter
                      .meterReading,
                    end_date: _res.history_end[0].reportTime,
                  };
                  if (_res.last_billing == null) {
                    billing.start_date = _res.history_start[0].reportTime;
                    billing.start_meter = JSON.parse(
                      _res.history_start[0].payload
                    ).meter.meterReading;
                    billing.invoice = `${_res.iot_area.username}.${
                      _res.iot_tenant.username
                    }.${_res.iot_tenant.id}-INV-${moment().format("DDMMYY")}-1`;
                  } else {
                    billing.start_meter = _res.last_billing.end_meter;
                    billing.start_date = _res.last_billing.end_date;
                    billing.invoice = _res.last_billing.invoice;
                  }
                  billing.totalizer = parseFloat(billing.end_meter);
                  billing.billing_usage =
                    parseFloat(billing.end_meter) -
                    parseFloat(billing.start_meter);
                  billing.usage =
                    parseFloat(billing.end_meter) -
                    parseFloat(billing.start_meter);
                  if (_res.iot_area.pricing_option == 1) {
                    billing.harga_satuan =
                      _res.iot_area.iot_pricing_postpaid_alls[0].pricing;
                  } else {
                    billing.harga_satuan =
                      _res.iot_tenant.iot_member_level.child[0].pricing;
                  }
                  billing.periode_cut = _res.iot_area.automatic_bill;
                  billing.typeId = _res.typeId;
                  if (_res.typeId == 1 || _res.typeId == 2) {
                    billing.unit = "m3";
                  } else if (_res.typeId == 3 || _res.typeId == 7) {
                    billing.unit = "kwh";
                  }
                  billing.node_type =
                    _res.typeId == 1
                      ? "GAS"
                      : _res.typeId == 2
                      ? "WATER"
                      : _res.typeId == 3
                      ? "ELECTRICITY NON CT"
                      : _res.typeId == 7
                      ? "ELECTRICITY CT"
                      : null;
                  billing.deveui = _res.devEui;
                  return models.iots_billing_history
                    .create(billing, {
                      transaction: t,
                    })
                    .then(() => {
                      return models.iot_nodes
                        .update(
                          {
                            is_unsigned: true,
                            devEui:
                              "unsigned_" + _res.devEui + "_id_" + _res.id,
                          },
                          { where: { id: _res.id }, transaction: t }
                        )
                        .then((res_update) => {
                          return models.iot_nodes.create(new_node, {
                            transaction: t,
                          });
                        });
                    });
                } else {
                  return models.iot_nodes
                    .update(
                      {
                        is_unsigned: true,
                        devEui: "unsigned_" + _res.devEui + "_id_" + _res.id,
                      },
                      { where: { id: _res.id }, transaction: t }
                    )
                    .then((res_update) => {
                      return models.iot_nodes.create(new_node, {
                        transaction: t,
                      });
                    });
                }
              } catch (error) {
                console.log(error);
                throw new Error(error.message);
              }
            });
        })
        .then((res) =>
          resolve({
            responseCode: 200,
            messages: `berhasil unsigned node`,
            // messages: res,
          })
        )
        .catch((err) => {
          resolve({
            responseCode: 500,
            messages: err.message,
          });
        });
    });
  },
  serviceUnassignedNodeBK: (data, typeId) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_nodes
            .update(
              { tenantId: null, internalId: null },
              { where: { devEui: data.devEui }, transaction: t }
            )
            .then(([result]) => {
              if (result) {
                delete data.devEui;
                switch (parseInt(typeId)) {
                  case 1:
                    console.log("GAS");
                    return models.iot_detail_gas.destroy({
                      where: data,
                      transaction: t,
                    });
                    break;
                  case 2:
                    console.log("WATER");
                    return models.iot_detail_water.destroy({
                      where: data,
                      transaction: t,
                    });
                    break;
                  case 3:
                    console.log("ELECTRICITY NON CT");
                    return models.iot_detail_electric.destroy({
                      where: data,
                      transaction: t,
                    });
                    break;
                  case 4:
                    console.log("RTU");
                    return models.iot_detail_rtu.destroy({
                      where: data,
                      transaction: t,
                    });
                    break;
                  case 5:
                    console.log("PRESSURE");
                    return models.iot_detail_preassure.destroy({
                      where: data,
                      transaction: t,
                    });
                    break;
                  case 9:
                    console.log("PRESSURE");
                    return models.iot_detail_preassure.destroy({
                      where: data,
                      transaction: t,
                    });
                    break;
                  case 6:
                    console.log("TEMPERATURE");
                    return models.iot_detail_temperature.destroy({
                      where: data,
                      transaction: t,
                    });
                    break;

                  case 7:
                    console.log("ELECTRICITY CT");
                    return models.iot_detail_electricct.destroy({
                      where: data,
                      transaction: t,
                    });
                    break;

                  default:
                    break;
                }
              } else
                reject({
                  responseCode: 404,
                  messages: `devEui salah`,
                });
            })
            .catch((err) => console.log(err));
        })
        .then((res) =>
          resolve({
            responseCode: 200,
            messages: `berhasil unsigned node`,
          })
        )
        .catch((err) => {
          resolve({
            responseCode: 500,
            messages: `something error`,
            err: err,
          });
          console.log(err);
        });
    });
  },
  serviceListPrepaidNode: (data) => {
    return new Promise((resolve, reject) => {
      models.iot_nodes
        .findAll({
          where: data,
          include: [
            { model: models.iot_device_type },
            { model: models.iot_node_type },
          ],
        })
        .then((result) => {
          result = JSON.parse(JSON.stringify(result));
          console.log(result);
          resolve({
            responseCode: 200,
            list_node: result,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error,
          });
        });
    });
  },
  serviceListUnassignedNode: (data) => {
    return new Promise((resolve, reject) => {
      let type = {};
      if (data.typeId == 5) {
        type = { typeId: { [Op.in]: [5, 9, 11, 12] } };
        delete data.typeId;
      }
      models.iot_nodes
        .findAll({
          where: [data, type],
          include: [
            { model: models.iot_device_type },
            { model: models.iot_node_type },
          ],
        })
        .then((result) => {
          result = JSON.parse(JSON.stringify(result));
          console.log(result);
          resolve({
            responseCode: 200,
            list_node: result,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error,
          });
        });
    });
  },
  serviceListNodeV2: (data) => {
    return new Promise((resolve, reject) => {
      let type = {};
      if (data.typeId == 5) {
        type = { typeId: { [Op.in]: [5, 9, 11, 12] } };
        delete data.typeId;
      }
      return models.sequelize
        .transaction((t) => {
          return models.iot_nodes.findAll({
            order: [["is_unsigned", "ASC"]],
            where: [data, type, { is_display: true }],
            include: [
              {
                model: models.iot_nodes,
                attributes: ["id", "devEui", "typeId"],
                include: [{ model: models.iot_node_type }],
              },
              { model: models.iot_device_type },
              { model: models.iot_node_type },
              { model: models.iot_detail_pju },
              { model: models.iot_detail_water },
              { model: models.iot_detail_gas, as: "iot_detail_gas" },
              { model: models.iot_detail_electric },
              { model: models.iot_detail_electricct },
              { model: models.iot_detail_rtu },
              { model: models.iot_detail_temperature },
              { model: models.iot_detail_preassure },
            ],
            transaction: t,
          });
        })
        .then((result) => {
          result = JSON.parse(JSON.stringify(result));
          result.map((val, index) => {
            result[index].link_devEui =
              val.iot_node != null ? val.iot_node.devEui : null;
            result[index].link_type =
              val.iot_node != null
                ? val.iot_node.iot_node_type.type_name
                : null;
            result[index].link_type_id =
              val.iot_node != null ? val.iot_node.iot_node_type.id : null;
            if (val.typeId == 9) {
              result[index].live_last_meter =
                parseFloat(result[index].live_last_meter) * 2 + 10;
            }
            if (val.typeId == 13) {
              result[index].level_value =
                parseFloat(result[index].sensor_to_botom) +
                parseFloat(result[index].sensor_level_offset) -
                parseFloat(result[index].live_last_meter);
            }
            result[index].isOffline = false;
            if (
              [1, 2, 3, 7].includes(parseInt(val.typeId)) &&
              val.live_last_meter != null
            ) {
              result[index].live_last_meter =
                parseFloat(result[index].live_last_meter) / 1000;
            }
            if (val.last_update != null) {
              const endTime = moment();
              const startTime = moment(val.last_update);
              let duration = moment.duration(endTime.diff(startTime));
              duration = duration.asMinutes();
              if (duration >= val.interval * 4) {
                result[index].isOffline = true;
              }
            } else {
              result[index].isOffline = true;
            }
            result[index].interval = val.setting_interval;
            // if (parseInt(val.setting_interval) == parseInt(val.live_interval)) {
            //   result[index].interval = val.setting_interval;
            // } else {
            //   result[index].interval = "PROCESSING";
            // }
            result[index].live_valve = val.live_valve;
          });
          resolve({
            responseCode: 200,
            list_node: result,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error,
          });
        });
    });
  },
  serviceListNode: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_nodes.findAll({
            where: [data, { is_display: true }],
            include: [
              { model: models.iot_device_type },
              { model: models.iot_node_type },
              { model: models.iot_detail_water },
              { model: models.iot_detail_gas, as: "iot_detail_gas" },
              { model: models.iot_detail_electric },
              { model: models.iot_detail_electricct },
              { model: models.iot_detail_rtu },
              { model: models.iot_detail_temperature },
              { model: models.iot_detail_preassure },
              // {
              //   model: models.iot_histories,
              //   attributes: ["id", "updatedAt", "payload"],
              //   order: [["id", "DESC"]],
              //   limit: 1,
              // },
            ],
            transaction: t,
          });
        })
        .then((result) => {
          result = JSON.parse(JSON.stringify(result));
          result.map((val, idx) => {
            result[idx].max_usage = parseFloat(result[idx].max_usage) / 1000;
            result[idx].max_totalizer =
              parseFloat(result[idx].max_totalizer) / 1000;
            if (result[idx].iot_detail_rtu !== null) {
              result[idx].iot_detail_rtu.meter_id =
                result[idx].iot_detail_rtu.serial_number;
            }
          });
          resolve({
            responseCode: 200,
            list_node: result,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error,
          });
        });
    });
  },
  serviceCheckNode: (data) => {
    return new Promise((resolve, reject) => {
      console.log(data.devEUI);
      axios
        .get(process.env.HOST_SERVER + `/list/node?devEUI=${data.devEUI}`)
        .then((s) => {
          resolve({
            responseCode: 200,
            list_node: s.data,
          });
          // console.log(s.data)
          // resolve(s.data)
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            list_node: err.response.data,
          });
          // console.log(err.response.data)
          // reject(err.response)
        });
    });
  },
  servicePaginationHistorySumV2: (
    devEui,
    date_start,
    date_end,
    arr_date,
    arr_date2
  ) => {
    return new Promise((resolve, reject) => {
      // models.sequelize
      //   .query(
      //     `SELECT * FROM iot_histories WHERE DATE(reportTimeFlag) <= '${moment(
      //       date_end
      //     ).format("YYYY-MM-DD")}' AND DATE(reportTimeFlag) >= '${moment(
      //       date_start
      //     )
      //       .subtract(1, "days")
      //       .format(
      //         "YYYY-MM-DD"
      //       )}' AND device_id=${devEui} AND (reportTimeFlag IN(
      //   SELECT MAX(reportTimeFlag) as max
      //   FROM iot_histories
      //   GROUP BY DATE(reportTimeFlag)
      //  ) OR reportTimeFlag IN(
      //   SELECT MIN(reportTimeFlag) as min
      //   FROM iot_histories
      //   GROUP BY DATE(reportTimeFlag)
      //  ))
      //  ORDER BY reportTime ASC`
      //   )
      models.sequelize
        .query(
          `SELECT * FROM iot_histories WHERE DATE(reportTimeFlag) <= '${moment(
            date_end
          ).format("YYYY-MM-DD")}' AND DATE(reportTimeFlag) >= '${moment(
            date_start
          ).format("YYYY-MM-DD")}' AND device_id=${devEui} 
       ORDER BY reportTime ASC`
        )
        .then((_res) => {
          _res = _res[0];
          models.iot_histories
            .findOne({
              where: {
                device_id: devEui,
                reportTimeFlag: models.sequelize.where(
                  models.sequelize.fn(
                    "date",
                    models.sequelize.col("iot_histories.`reportTimeFlag")
                  ),
                  "<",
                  moment(date_start).format("YYYY-MM-DD")
                ),
              },
              order: [["reportTime", "DESC"]],
            })
            .then((_res_f) => {
              _res_f = JSON.parse(JSON.stringify(_res_f));
              if (_res_f != null) {
                arr_date.unshift(
                  moment(_res_f.reportTimeFlag).format("YYYY-MM-DD")
                );
                _res.unshift(_res_f);
              }
              let output = { data: [] };
              arr_date.map((val, idx) => {
                let data_1 = _res.filter((valP) => {
                  return (
                    moment(valP.reportTimeFlag).format("YYYY-MM-DD") ==
                    moment(val).format("YYYY-MM-DD")
                  );
                });
                if (data_1.length > 0 && idx > 0 && output.data.length > 0) {
                  let pyd1 = JSON.parse(data_1[0].payload);
                  let pyd2 = JSON.parse(data_1[data_1.length - 1].payload);
                  console.log(pyd1.reportTime, pyd2.reportTime, val);
                  output.data.push({
                    last_update: moment(pyd2.reportTime)
                      .utcOffset("+0700")
                      .format("YYYY-MM-DD HH:mm:ss"),
                    totalizer: pyd2.meter.meterReading,
                  });
                } else if (
                  (data_1.length > 0 && idx == 0 && output.data.length == 0) ||
                  (data_1.length > 0 && idx > 0 && output.data.length == 0)
                ) {
                  let pyd1 = JSON.parse(data_1[0].payload);
                  let pyd2 = JSON.parse(data_1[data_1.length - 1].payload);
                  output.data.push({
                    last_update: moment(pyd2.reportTime)
                      .utcOffset("+0700")
                      .format("YYYY-MM-DD HH:mm:ss"),
                    totalizer: pyd2.meter.meterReading,
                    usage:
                      parseFloat(pyd2.meter.meterReading) -
                      parseFloat(pyd1.meter.meterReading),
                  });
                }
              });
              output.data.map((val, index) => {
                if (index > 0) {
                  output.data[index].usage =
                    output.data[index].totalizer -
                    output.data[index - 1].totalizer;
                }
              });

              if (_res_f != null) {
                let idx_remove = output.data.findIndex((valP) => {
                  return (
                    moment(valP.last_update).format("YYYY-MM-DD") ==
                    moment(arr_date[0]).format("YYYY-MM-DD")
                  );
                });
                if (idx_remove >= 0) {
                  output.data.splice(idx_remove, 1);
                }
              }
              resolve(output);
              return _res;
            });
        })
        .catch((error) => console.log(error));
    });
  },
  servicePaginationHistorySum: (data, pagination) => {
    return new Promise((resolve, reject) => {
      const page = data.page;
      const size = data.size;
      const typeId = data.typeId;
      delete data.typeId;
      delete data.page;
      delete data.size;
      let child = "";
      if (data.child != undefined) {
        child = data.child;
        delete data.child;
      }
      models.iot_histories
        .findAndCountAll({
          where: {
            device_id: data.device_id,
            [Op.and]: [
              {
                reportTime: models.sequelize.where(
                  models.sequelize.fn(
                    "date",
                    models.sequelize.col("iot_histories.reportTimeFlag")
                  ),
                  ">=",
                  moment(data.date_start).format("YYYY-MM-DD")
                ),
              },
              {
                reportTime: models.sequelize.where(
                  models.sequelize.fn(
                    "date",
                    models.sequelize.col("iot_histories.reportTimeFlag")
                  ),
                  "<",
                  moment(data.date_end).format("YYYY-MM-DD")
                ),
              },
            ],
          },
          order: [["reportTime", "DESC"]],
          include: [
            {
              model: models.iot_nodes,
              include: [
                { model: models.iot_device_type },
                { model: models.iot_node_type },

                { model: models.iot_detail_water },
                { model: models.iot_detail_gas, as: "iot_detail_gas" },
                { model: models.iot_detail_electric },
                { model: models.iot_detail_rtu },
                { model: models.iot_detail_temperature },
                { model: models.iot_detail_preassure },
              ],
            },
          ],
        })
        .then((result) => {
          switch (parseInt(typeId)) {
            case 1:
              console.log("GAS");
              let tgl = "";
              let flag = 0;
              let data = {};
              let totalizer = [];
              result.rows.map((val, index) => {
                val.payload = JSON.parse(val.payload);
                if (tgl == val.reportTimeFlag.split(" ")[0]) {
                  console.log("awes");
                  data.usage = data.totalizer - val.payload.meter.meterReading;
                  totalizer[totalizer.length - 1].payload = data;
                } else {
                  console.log(flag);
                  tgl = val.reportTimeFlag.split(" ")[0];
                  if (flag == 0) {
                    data = {
                      devEui: val.payload.devEUI,
                      update_time: val.payload.reportTime,
                      totalizer: val.payload.meter.meterReading,
                      valve: val.payload.meter.valve,
                      usage:
                        val.payload.meter.meterReading -
                        JSON.parse(result.rows[index + 1].payload).meter
                          .meterReading,
                      battrey_level: val.payload.meter.battery,
                    };
                    totalizer.push(val.payload);
                    totalizer[0].payload = data;
                    flag++;
                  } else {
                    data = {
                      devEui: val.payload.devEUI,
                      update_time: val.payload.reportTime,
                      totalizer: val.payload.meter.meterReading,
                      valve: val.payload.meter.valve,
                      usage: 0,
                      battrey_level: val.payload.meter.battery,
                    };
                    totalizer.push(val.payload);
                    totalizer.payload = data;
                  }
                }
              });
              result = totalizer;
              break;
            case 2:
              console.log("WATER");
              result.rows.map((val, index) => {
                result.rows[index].payload = JSON.parse(val.payload);
                const data = {
                  devEui: val.payload.devEUI,
                  update_time: val.payload.reportTime,
                  totalizer: val.payload.meter.meterReading,
                  valve: val.payload.meter.valve,
                  usage: "",
                  balance:
                    parseInt(val.iot_node.iot_detail_water.balance) -
                    parseInt(val.payload.meter.meterReading),
                  battrey_level: val.payload.meter.battery,
                };
                result.rows[index].payload = data;
              });
              break;
            case 3:
              console.log("ELECTRICITY NON CT");
              result.rows.map((val, index) => {
                result.rows[index].payload = JSON.parse(val.payload);

                const data = {
                  devEui: val.payload.devEUI,
                  update_time: val.payload.reportTime,
                  totalizer: val.payload.meter.meterReading,
                  valve: val.payload.meter.valve,
                  usage: "",
                  balance:
                    parseInt(val.iot_node.iot_detail_electric.balance) -
                    parseInt(val.payload.meter.meterReading),
                  battrey_level: val.payload.meter.battery,
                };
                result.rows[index].payload = data;
              });
              break;
            case 4:
              console.log("RTU");
              result.rows.map((val, index) => {
                result.rows[index].payload = JSON.parse(val.payload);
                let NewData = "";
                val.payload.subMeter.map((val) => {
                  if (val.applicationName == child) {
                    NewData = val;
                  }
                });
                const data = {
                  devEui: val.payload.devEUI,
                  update_time: val.payload.reportTime,
                  raw: NewData.report,
                };
                result.rows[index] = data;
              });
              break;
            case 5:
              console.log("PRESSURE");
              result.rows.map((val, index) => {
                result.rows[index].payload = JSON.parse(val.payload);
                const data = {
                  devEui: val.payload.hardware_serial,
                  update_time: val.payload.reportTime,
                  preassure: val.payload.payload_fields.gasPressure.toFixed(2),
                  battrey_level: val.payload.payload_fields.batteryVoltage,
                };
                result.rows[index] = data;
              });
              break;
            case 9:
              console.log("PRESSURE PT");
              result.rows.map((val, index) => {
                result.rows[index].payload = JSON.parse(val.payload);
                const data = {
                  devEui: val.payload.hardware_serial,
                  update_time: val.payload.reportTime,
                  preassure:
                    parseFloat(
                      val.payload.payload_fields.gasPressure.toFixed(2)
                    ) *
                      2 +
                    10,
                  battrey_level: val.payload.payload_fields.batteryVoltage,
                };
                result.rows[index] = data;
              });
              break;
            case 6:
              console.log("TEMPERATURE");
              console.log(result.rows.length);
              result.rows.map((val, index) => {
                result.rows[index].payload = JSON.parse(val.payload);

                const data = {
                  devEui: val.payload.devEUI,
                  update_time: val.payload.reportTime,
                  Temperature: val.payload.meter.exReading[0].value,
                  Humidity: val.payload.meter.exReading[1].value,
                };
                console.log(index);
                result.rows[index] = data;
                console.log(result.rows[index]);
              });
              break;

            case 7:
              console.log("ELECTRICITY CT");
              result.rows.map((val, index) => {
                result.rows[index].payload = JSON.parse(val.payload);

                const data = {
                  devEui: val.payload.devEUI,
                  update_time: val.payload.reportTime,
                  raw: val.payload,
                };
                result.rows[index] = data;
              });
              break;

            default:
              break;
          }
          result.page = page;
          result.size = size;
          resolve(result);
        })
        .catch((error) => console.log(error));
    });
  },

  // servicePaginationHistory: (data, pagination) => {
  //   return new Promise((resolve, reject) => {
  //     const page = data.page;
  //     const size = data.size;
  //     const typeId = data.typeId;
  //     delete data.typeId;
  //     delete data.page;
  //     delete data.size;
  //     let child = "";
  //     if (data.child != undefined) {
  //       child = data.child;
  //       delete data.child;
  //     }
  //     data.date_start != undefined || data.date_end != undefined
  //       ? models.iot_histories
  //           .findAndCountAll({
  //             where: {
  //               device_id: data.device_id,
  //               reportTime: {
  //                 [Op.between]: [data.date_start, data.date_end]
  //               }
  //             },
  //             limit: pagination.limit,
  //             offset: pagination.offset,
  //             order: [["id", "DESC"]],
  //             include: [
  //               {
  //                 model: models.iot_nodes,
  //                 include: [
  //                   { model: models.iot_device_type },
  //                   { model: models.iot_node_type },

  //                   { model: models.iot_detail_water },
  //                   { model: models.iot_detail_gas, as: "iot_detail_gas" },
  //                   { model: models.iot_detail_electric },
  //                   { model: models.iot_detail_rtu },
  //                   { model: models.iot_detail_temperature },
  //                   { model: models.iot_detail_preassure }
  //                 ]
  //               }
  //             ]
  //           })
  //           .then(result => {
  //             switch (parseInt(typeId)) {
  //               case 1:
  //                 console.log("GAS");
  //                 result.rows.map((val, index) => {
  //                   result.rows[index].payload = JSON.parse(val.payload);
  //                   const data = {
  //                     devEui: val.payload.devEUI,
  //                     update_time: val.payload.reportTime,
  //                     totalizer: val.payload.meter.meterReading,
  //                     valve: val.payload.meter.valve,
  //                     usage:
  //                       result.rows.length - index == 1
  //                         ? 0
  //                         : parseFloat(
  //                             result.rows[index].payload.meter.meterReading -
  //                               result.rows[index + 1].payload.meter
  //                                 .meterReading
  //                           ) / 1000,
  //                     balance:
  //                       parseInt(val.iot_node.iot_detail_gas.balance) -
  //                       parseInt(val.payload.meter.meterReading),
  //                     battrey_level: val.payload.meter.battery
  //                   };
  //                   result.rows[index].payload = data;
  //                 });
  //                 break;
  //               case 2:
  //                 console.log("WATER");
  //                 result.rows.map((val, index) => {
  //                   result.rows[index].payload = JSON.parse(val.payload);
  //                   const data = {
  //                     devEui: val.payload.devEUI,
  //                     update_time: val.payload.reportTime,
  //                     totalizer: val.payload.meter.meterReading,
  //                     valve: val.payload.meter.valve,
  //                     usage: "",
  //                     balance:
  //                       parseInt(val.iot_node.iot_detail_water.balance) -
  //                       parseInt(val.payload.meter.meterReading),
  //                     battrey_level: val.payload.meter.battery
  //                   };
  //                   result.rows[index].payload = data;
  //                 });
  //                 break;
  //               case 3:
  //                 console.log("ELECTRICITY NON CT");
  //                 result.rows.map((val, index) => {
  //                   result.rows[index].payload = JSON.parse(val.payload);

  //                   const data = {
  //                     devEui: val.payload.devEUI,
  //                     update_time: val.payload.reportTime,
  //                     totalizer: val.payload.meter.meterReading,
  //                     valve: val.payload.meter.valve,
  //                     usage: "",
  //                     balance:
  //                       parseInt(val.iot_node.iot_detail_electric.balance) -
  //                       parseInt(val.payload.meter.meterReading),
  //                     battrey_level: val.payload.meter.battery
  //                   };
  //                   result.rows[index].payload = data;
  //                 });
  //                 break;
  //               case 4:
  //                 console.log("RTU");
  //                 result.rows.map((val, index) => {
  //                   result.rows[index].payload = JSON.parse(val.payload);
  //                   let NewData = "";
  //                   val.payload.subMeter.map(val => {
  //                     if (val.applicationName == child) {
  //                       NewData = val;
  //                     }
  //                   });
  //                   const data = {
  //                     devEui: val.payload.devEUI,
  //                     update_time: val.payload.reportTime,
  //                     raw: NewData.report
  //                   };
  //                   result.rows[index] = data;
  //                 });
  //                 break;
  //               case 5:
  //                 console.log("PRESSURE");
  //                 result.rows.map((val, index) => {
  //                   result.rows[index].payload = JSON.parse(val.payload);
  //                   const data = {
  //                     devEui: val.payload.hardware_serial,
  //                     update_time: val.payload.reportTime,
  //                     preassure: val.payload.payload_fields.gasPressure.toFixed(
  //                       2
  //                     ),
  //                     battrey_level: val.payload.payload_fields.batteryVoltage
  //                   };
  //                   result.rows[index] = data;
  //                 });
  //                 break;
  //               case 6:
  //                 console.log("TEMPERATURE");
  //                 console.log(result.rows.length);
  //                 result.rows.map((val, index) => {
  //                   result.rows[index].payload = JSON.parse(val.payload);

  //                   const data = {
  //                     devEui: val.payload.devEUI,
  //                     update_time: val.payload.reportTime,
  //                     Temperature: val.payload.meter.exReading[0].value,
  //                     Humidity: val.payload.meter.exReading[1].value
  //                   };
  //                   console.log(index);
  //                   result.rows[index] = data;
  //                   console.log(result.rows[index]);
  //                 });
  //                 break;

  //               case 7:
  //                 console.log("ELECTRICITY CT");
  //                 result.rows.map((val, index) => {
  //                   result.rows[index].payload = JSON.parse(val.payload);

  //                   const data = {
  //                     devEui: val.payload.devEUI,
  //                     update_time: val.payload.reportTime,
  //                     raw: val.payload
  //                   };
  //                   result.rows[index] = data;
  //                 });
  //                 break;

  //               default:
  //                 break;
  //             }
  //             result.page = page;
  //             result.size = size;
  //             resolve(result);
  //           })
  //           .catch(error => console.log(error))
  //       : models.iot_histories
  //           .findAndCountAll({
  //             where: data,
  //             // limit: pagination.limit,
  //             // offset: pagination.offset,
  //             order: [["id", "DESC"]],
  //             include: [
  //               {
  //                 model: models.iot_nodes,
  //                 include: [
  //                   { model: models.iot_device_type },
  //                   { model: models.iot_node_type },

  //                   { model: models.iot_detail_water },
  //                   { model: models.iot_detail_gas, as: "iot_detail_gas" },
  //                   { model: models.iot_detail_electric },
  //                   { model: models.iot_detail_rtu },
  //                   { model: models.iot_detail_temperature },
  //                   { model: models.iot_detail_preassure }
  //                 ]
  //               }
  //             ]
  //           })
  //           .then(result => {
  //             switch (parseInt(typeId)) {
  //               case 1:
  //                 console.log("GAS");
  //                 result.rows.map((val, index) => {
  //                   result.rows[index].payload = JSON.parse(val.payload);
  //                   const data = {
  //                     devEui: val.payload.devEUI,
  //                     update_time: val.payload.reportTime,
  //                     totalizer: val.payload.meter.meterReading,
  //                     valve: val.payload.meter.valve,
  //                     usage:
  //                       result.rows.length - index == 1
  //                         ? 0
  //                         : parseFloat(
  //                             val.payload.meter.meterReading -
  //                               JSON.parse(result.rows[index + 1].payload).meter
  //                                 .meterReading
  //                           ) / 1000,
  //                     battrey_level: val.payload.meter.battery
  //                   };

  //                   models.iot_histories.update(
  //                     {
  //                       usage:
  //                         result.rows.length - index == 1
  //                           ? 0
  //                           : parseFloat(
  //                               val.payload.meter.meterReading -
  //                                 JSON.parse(result.rows[index + 1].payload)
  //                                   .meter.meterReading
  //                             ) / 1000
  //                     },
  //                     {
  //                       where: { id: result.rows[index].id }
  //                     }
  //                   );
  //                   result.rows[index].payload = data;
  //                   console.log(result.rows[index].id);
  //                 });
  //                 break;
  //               case 2:
  //                 console.log("WATER");
  //                 result.rows.map((val, index) => {
  //                   result.rows[index].payload = JSON.parse(val.payload);
  //                   const data = {
  //                     devEui: val.payload.devEUI,
  //                     update_time: val.payload.reportTime,
  //                     totalizer: val.payload.meter.meterReading,
  //                     valve: val.payload.meter.valve,
  //                     usage: "",
  //                     balance:
  //                       parseInt(val.iot_node.iot_detail_water.balance) -
  //                       parseInt(val.payload.meter.meterReading),
  //                     battrey_level: val.payload.meter.battery
  //                   };
  //                   result.rows[index].payload = data;
  //                 });
  //                 break;
  //               case 3:
  //                 console.log("ELECTRICITY NON CT");
  //                 result.rows.map((val, index) => {
  //                   result.rows[index].payload = JSON.parse(val.payload);

  //                   const data = {
  //                     devEui: val.payload.devEUI,
  //                     update_time: val.payload.reportTime,
  //                     totalizer: val.payload.meter.meterReading,
  //                     valve: val.payload.meter.valve,
  //                     usage: "",
  //                     balance:
  //                       parseInt(val.iot_node.iot_detail_electric.balance) -
  //                       parseInt(val.payload.meter.meterReading),
  //                     battrey_level: val.payload.meter.battery
  //                   };
  //                   result.rows[index].payload = data;
  //                 });
  //                 break;
  //               case 4:
  //                 console.log("RTU");
  //                 result.rows.map((val, index) => {
  //                   result.rows[index].payload = JSON.parse(val.payload);
  //                   let NewData = "";
  //                   val.payload.subMeter.map(val => {
  //                     if (val.applicationName == child) {
  //                       NewData = val;
  //                     }
  //                   });
  //                   const data = {
  //                     devEui: val.payload.devEUI,
  //                     update_time: val.payload.reportTime,
  //                     raw: NewData.report
  //                   };
  //                   result.rows[index] = data;
  //                 });
  //                 break;
  //               case 5:
  //                 console.log("PRESSURE");
  //                 result.rows.map((val, index) => {
  //                   result.rows[index].payload = JSON.parse(val.payload);
  //                   const data = {
  //                     devEui: val.payload.hardware_serial,
  //                     update_time: val.payload.reportTime,
  //                     preassure: val.payload.payload_fields.gasPressure.toFixed(
  //                       2
  //                     ),
  //                     battrey_level: val.payload.payload_fields.batteryVoltage
  //                   };
  //                   result.rows[index] = data;
  //                 });
  //                 break;
  //               case 6:
  //                 console.log("TEMPERATURE");
  //                 result.rows.map((val, index) => {
  //                   result.rows[index].payload = JSON.parse(val.payload);

  //                   const data = {
  //                     devEui: val.payload.devEUI,
  //                     update_time: val.payload.reportTime,
  //                     Temperature: val.payload.meter.exReading[0].value,
  //                     Humidity: val.payload.meter.exReading[1].value
  //                   };
  //                   result.rows[index] = data;
  //                 });
  //                 break;

  //               case 7:
  //                 console.log("ELECTRICITY CT");
  //                 result.rows.map((val, index) => {
  //                   result.rows[index].payload = JSON.parse(val.payload);

  //                   const data = {
  //                     devEui: val.payload.devEUI,
  //                     update_time: val.payload.reportTime,
  //                     raw: val.payload
  //                   };
  //                   result.rows[index] = data;
  //                 });
  //                 break;

  //               default:
  //                 break;
  //             }
  //             result.page = page;
  //             result.size = size;
  //             resolve(result);
  //           })
  //           .catch(error => console.log(error));
  //   });
  // },
  servicePaginationHistory1: (data, pagination) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_histories.findAll({
            limit: pagination.limit,
            offset: pagination.offset,
            attributes: ["id", "reportTime", "reportTimeFlag", "device_id"],
            where: {
              // id: 1,
              // reportTimeWib: null,
              device_id: data.device_id,
              [Op.and]: [
                {
                  reportTime: models.sequelize.where(
                    models.sequelize.fn(
                      "date",
                      models.sequelize.col("iot_histories.reportTimeFlag")
                    ),
                    ">=",
                    moment(data.date_start).format("YYYY-MM-DD")
                  ),
                },
                {
                  reportTime: models.sequelize.where(
                    models.sequelize.fn(
                      "date",
                      models.sequelize.col("iot_histories.reportTimeFlag")
                    ),
                    "<",
                    moment(data.date_end).format("YYYY-MM-DD")
                  ),
                },
              ],
            },
            order: [["reportTime", "DESC"]],
            transaction: t,
          });
          // .then((res) => {
          //   res = JSON.parse(JSON.stringify(res));
          //   res.map((val, idx) => {
          //     // res[idx].reportTimeWib = moment(val.reportTime);
          //     res[idx].reportTimeFlag = moment(val.reportTime).format(
          //       "YYYY-MM-DD hh:mm:ss"
          //     );
          //   });
          //   return models.iot_histories.bulkCreate(res, {
          //     updateOnDuplicate: ["reportTimeFlag"],
          //     transaction: t,
          //   });
          // });
        })
        .then((res) => {
          // res = JSON.parse(JSON.stringify(res));
          // res.map((val, idx) => {
          //   res[idx].reportTimeWib = moment(val.reportTimeWib).format(
          //     "YYYY-MM-DD hh:mm:ss"
          //   );
          // });
          resolve(res);
        })
        .catch((err) => reject(err.message));
    });
  },

  serviceDeviceLog: (
    data,
    page,
    size,
    typeId,
    child,
    sn,
    type_name,
    start,
    end,
    devEui,
    latitude,
    longitude,
    altitude
  ) => {
    return new Promise((resolve, reject) => {
      let = filter_time = {};
      console.log(start, end);
      if (start != undefined && end != undefined) {
        filter_time = {
          [Op.and]: [
            {
              time: models.sequelize.where(
                models.sequelize.fn(
                  "date",
                  models.sequelize.col("iot_histories.reportTimeFlag")
                ),
                ">=",
                start
              ),
            },
            {
              time: models.sequelize.where(
                models.sequelize.fn(
                  "date",
                  models.sequelize.col("iot_histories.reportTimeFlag")
                ),
                "<=",
                end
              ),
            },
          ],
        };
      } else if (start != undefined) {
        filter_time = {
          start: models.sequelize.where(
            models.sequelize.fn(
              "date",
              models.sequelize.col("iot_histories.reportTimeFlag")
            ),
            ">=",
            start
          ),
        };
      }
      return models.sequelize
        .transaction((t) => {
          return models.iot_histories
            .findAndCountAll({
              limit: parseInt(size),
              offset: (parseInt(page) - 1) * parseInt(size),
              where: [data, filter_time],
              attributes: [
                "device_id",
                "reportTime",
                "payload",
                "reportTimeFlag",
              ],
              order: [["reportTime", "DESC"]],
              transaction: t,
            })
            .then((_result) => {
              _result = JSON.parse(JSON.stringify(_result));
              let res = _result.rows;
              let hasil = {
                page: page,
                size: size,
                total: _result.count,
                data: [],
              };
              switch (typeId) {
                case 7:
                  res.map((val, index) => {
                    hasil.data.push({
                      // device_id: val.device_id,
                      devEui: devEui,
                      reportTime: moment(val.reportTime)
                        .utcOffset("+0700")
                        .format("YYYY-MM-DD HH:mm:ss"),
                      meterReading: JSON.parse(val.payload).meter.meterReading,
                      device_type: type_name,
                      exReading: JSON.parse(val.payload).meter.exReading,
                    });
                  });
                  return hasil;
                case 6:
                  res.map((val, index) => {
                    hasil.data.push({
                      // device_id: val.device_id,
                      devEui: devEui,
                      reportTime: moment(val.reportTime)
                        .utcOffset("+0700")
                        .format("YYYY-MM-DD HH:mm:ss"),
                      device_type: type_name,
                      exReading: JSON.parse(val.payload).meter.exReading,
                    });
                  });
                  return hasil;
                case 5:
                  res.map((val, index) => {
                    hasil.data.push({
                      // device_id: val.device_id,
                      devEui: devEui,
                      reportTime: moment(val.reportTime)
                        .utcOffset("+0700")
                        .format("YYYY-MM-DD HH:mm:ss"),
                      ...JSON.parse(val.payload).payload_fields,
                      device_type: type_name,
                    });
                  });
                  return hasil;
                case 9:
                  res.map((val, index) => {
                    hasil.data.push({
                      // device_id: val.device_id,
                      devEui: devEui,
                      reportTime: moment(val.reportTime)
                        .utcOffset("+0700")
                        .format("YYYY-MM-DD HH:mm:ss"),
                      ...JSON.parse(val.payload).payload_fields,
                      device_type: type_name,
                    });
                  });
                  return hasil;
                case 4:
                  res.map((val, index) => {
                    hasil.data.push({
                      // device_id: val.device_id,
                      devEui: devEui,
                      reportTime: moment(val.reportTime)
                        .utcOffset("+0700")
                        .format("YYYY-MM-DD HH:mm:ss"),
                      meterReading: JSON.parse(val.payload).subMeter.filter(
                        (valP) => {
                          return (
                            valP.applicationName == child &&
                            valP.serialNumber == sn
                          );
                        }
                      )[0].report,
                      device_type: type_name,
                    });
                  });
                  return hasil;
                default:
                  res.map((val, index) => {
                    hasil.data.push({
                      // device_id: val.device_id,
                      devEui: devEui,
                      reportTime: moment(val.reportTime)
                        .utcOffset("+0700")
                        .format("YYYY-MM-DD HH:mm:ss"),
                      meterReading: JSON.parse(val.payload).meter.meterReading,
                      // device_type: type_name,
                      latitude: latitude,
                      longitude: longitude,
                      altitude: altitude,
                    });
                  });
                  return hasil;
              }
            });
        })
        .then((res) => resolve({ responseCode: 200, data: res }))
        .catch((err) => reject({ responseCode: 400, message: err.message }));
    });
  },
  serviceDeviceCheck: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_nodes
            .findOne({
              where: data,
              // where: { id: data.id },
              attributes: [
                "id",
                "areaId",
                "typeId",
                "device_type_id",
                "tenantId",
                "internalId",
                "devEui",
                "latitude",
                "longitude",
                "altitude",
              ],
              include: [
                { model: models.iot_node_type },
                { model: models.iot_detail_rtu },
              ],
              transaction: t,
            })
            .then((res) => {
              res = JSON.parse(JSON.stringify(res));
              console.log(res);
              if (res == null)
                return reject({
                  responseCode: 404,
                  message: "device id " + data.devEui + " not found",
                });
              switch (res.typeId) {
                case 4:
                  return {
                    responseCode: 200,
                    data: {
                      altitude: res.altitude,
                      latitude: res.latitude,
                      longitude: res.longitude,
                      typeId: res.typeId,
                      device_id: res.id,
                      devEui: res.devEui,
                      type_name: res.iot_node_type.type_name,
                      child: res.iot_detail_rtu.model,
                      sn: res.iot_detail_rtu.serial_number,
                    },
                  };

                default:
                  return {
                    responseCode: 200,
                    data: {
                      altitude: res.altitude,
                      latitude: res.latitude,
                      longitude: res.longitude,
                      typeId: res.typeId,
                      device_id: res.id,
                      devEui: res.devEui,
                      type_name: res.iot_node_type.type_name,
                    },
                  };
              }
            });
        })
        .then((res) => resolve(res))
        .catch((err) => reject({ responseCode: 400, message: err.message }));
    });
  },

  servicePaginationHistoryMobile: (
    data,
    pagination,
    pagination_flag = false
  ) => {
    return new Promise((resolve, reject) => {
      const page = data.page;
      const size = data.size;
      const typeId = data.typeId;
      delete data.typeId;
      delete data.page;
      delete data.size;
      let child = "";
      if (data.child != undefined) {
        child = data.child;
        delete data.child;
      }
      let pag = {};
      if (pagination_flag == true) {
        if (parseInt(typeId) == 4) {
          pagination.limit += 1;
        }
        pag = {
          limit: pagination.limit,
          offset: pagination.offset,
        };
      }
      data.date_start != undefined || data.date_end != undefined
        ? models.iot_histories
            .findAndCountAll({
              // attributes: { exclude: ["id"] },
              // group: "reportTime",
              where: {
                device_id: data.device_id,
                [Op.and]: [
                  {
                    reportTime: models.sequelize.where(
                      models.sequelize.fn(
                        "date",
                        models.sequelize.col("iot_histories.reportTimeFlag")
                      ),
                      ">=",
                      moment(data.date_start).format("YYYY-MM-DD")
                    ),
                  },
                  {
                    reportTime: models.sequelize.where(
                      models.sequelize.fn(
                        "date",
                        models.sequelize.col("iot_histories.reportTimeFlag")
                      ),
                      "<",
                      moment(data.date_end).format("YYYY-MM-DD")
                    ),
                  },
                ],
                // [Op.or]: [
                //   { reportTimeFlag: { [Op.gte]: "2020-06-29" } },
                //   { reportTimeFlag: { [Op.lt]: "2020-06-30" } },
                // ],

                // reportTime: {
                //   [Op.between]: [data.date_start, data.date_end],
                // },
              },
              ...pag,
              order: [["reportTimeFlag", "DESC"]],
              include: [
                {
                  model: models.iot_nodes,
                  include: [
                    { model: models.iot_device_type },
                    { model: models.iot_node_type },

                    { model: models.iot_detail_water },
                    { model: models.iot_detail_gas, as: "iot_detail_gas" },
                    { model: models.iot_detail_electric },
                    { model: models.iot_detail_rtu },
                    { model: models.iot_detail_temperature },
                    { model: models.iot_detail_preassure },
                  ],
                },
              ],
            })
            .then((result) => {
              result = JSON.parse(JSON.stringify(result));
              result.page = page;
              result.size = size;
              if (result.rows.length == 0) {
                resolve(result);
                return result;
              }

              return models.iot_histories
                .findOne({
                  where: {
                    device_id: data.device_id,
                    id: { [Op.lt]: result.rows[result.rows.length - 1].id },
                  },
                  order: [["reportTimeFlag", "DESC"]],
                })
                .then((res_before) => {
                  res_before = JSON.parse(JSON.stringify(res_before));
                  if (res_before == null) {
                    res_before = 0;
                  } else {
                    res_before = JSON.parse(res_before.payload).meter
                      .meterReading;
                  }

                  switch (parseInt(typeId)) {
                    case 1:
                      console.log("GAS MOBILE");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        const data = {
                          devEui: val.payload.devEUI,
                          update_time: val.payload.reportTime,
                          totalizer: val.payload.meter.meterReading,
                          valve: val.payload.meter.valve,
                          usage:
                            result.rows.length - index == 1
                              ? 0
                              : parseFloat(
                                  result.rows[index].payload.meter
                                    .meterReading -
                                    JSON.parse(result.rows[index + 1].payload)
                                      .meter.meterReading
                                ) / 1000,
                          balance: val.balance,
                          battrey_level: val.payload.meter.battery,
                        };
                        result.rows[index].usage = data.usage;
                        result.rows[index].payload = data;
                      });
                      break;
                    case 2:
                      console.log("WATER ? mobile");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        result.rows[index].usage =
                          result.rows[index].usage / 1000 <= 0
                            ? 0
                            : (result.rows[index].usage / 1000).toFixed(2);
                        const data = {
                          devEui: val.payload.devEUI,
                          update_time: val.payload.reportTime,
                          totalizer: val.payload.meter.meterReading,
                          valve: val.payload.meter.valve,
                          usage: "",
                          balance: val.balance,
                          battrey_level: val.payload.meter.battery,
                        };
                        result.rows[index].payload = data;
                      });
                      break;
                    case 3:
                      console.log("ELECTRICITY NON CT");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        result.rows[index].usage =
                          result.rows[index].usage / 1000 <= 0
                            ? 0
                            : (result.rows[index].usage / 1000).toFixed(2);

                        const data = {
                          devEui: val.payload.devEUI,
                          update_time: val.payload.reportTime,
                          totalizer: val.payload.meter.meterReading,
                          valve: val.payload.meter.valve,
                          usage: "",
                          balance: val.balance,
                          battrey_level: val.payload.meter.battery,
                        };
                        result.rows[index].payload = data;
                      });
                      break;
                    case 4:
                      let sn = JSON.parse(JSON.stringify(result.rows[0]))
                        .iot_node.iot_detail_rtu.serial_number;
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        let NewData = "";
                        val.payload.subMeter.map((val) => {
                          console.log("RTU", sn);
                          if (val.applicationName == child) {
                            NewData = val;
                          }
                        });
                        const data = {
                          devEui: val.payload.devEUI,
                          update_time: val.payload.reportTime,
                          raw: NewData.report,
                        };
                        result.rows[index] = data;
                      });
                      break;
                    case 5:
                      console.log("PRESSURE");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        const data = {
                          devEui: val.payload.hardware_serial,
                          update_time: val.payload.reportTime,
                          preassure:
                            val.payload.payload_fields.gasPressure.toFixed(2),
                          battrey_level:
                            ((parseFloat(
                              val.payload.payload_fields.batteryVoltage
                            ) -
                              1.8) *
                              100) /
                            (3.65 - 1.8),
                        };
                        data.battrey_level = parseFloat(
                          data.battrey_level.toFixed(2)
                        );
                        data.battrey_level =
                          data.battrey_level > 100
                            ? 100
                            : data.battrey_level < 0
                            ? 0
                            : data.battrey_level;
                        result.rows[index] = data;
                      });
                      break;
                    case 10:
                      console.log("PJU");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        const data = {
                          devEui: val.payload.devEUI,
                          update_time: val.payload.reportTime,
                          tegangan: val.payload.payload_fields.tegangan,
                          arus: val.payload.payload_fields.arus,
                          power: val.payload.payload_fields.power,
                          power_factor: val.payload.payload_fields.power_factor,
                          cahaya: val.payload.payload_fields.cahaya,
                          relay: val.payload.payload_fields.relay,
                          lampu: val.payload.payload_fields.lampu,
                        };
                        result.rows[index] = data;
                      });
                      break;
                    case 9:
                      console.log("PRESSURE");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        const data = {
                          devEui: val.payload.hardware_serial,
                          update_time: val.payload.reportTime,
                          preassure:
                            parseFloat(
                              val.payload.payload_fields.gasPressure.toFixed(2)
                            ) *
                              2 +
                            10,
                          battrey_level: (
                            ((parseFloat(
                              val.payload.payload_fields.batteryVoltage
                            ) -
                              1.8) *
                              100) /
                            (3.65 - 1.8)
                          ).toFixed(2),
                        };
                        data.battrey_level =
                          data.battrey_level > 100 ? 100 : data.battrey_level;
                        data.battrey_level =
                          data.battrey_level < 0 ? 0 : data.battrey_level;
                        result.rows[index] = data;
                      });
                      break;
                    case 8:
                      console.log("WATER PRESSURE");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        const data = {
                          devEui: val.payload.hardware_serial,
                          update_time: val.payload.reportTime,
                          pressureRaw:
                            val.payload.payload_fields.pressureRaw != undefined
                              ? val.payload.payload_fields.pressureRaw.toFixed(
                                  2
                                )
                              : 0,
                          pressure:
                            val.payload.payload_fields.pressure != undefined
                              ? val.payload.payload_fields.pressure.toFixed(2)
                              : 0,
                          batteryVoltage:
                            val.payload.payload_fields.batteryVoltage !=
                            undefined
                              ? val.payload.payload_fields.batteryVoltage.toFixed(
                                  2
                                )
                              : 0,
                          battrey_level: (
                            ((parseFloat(
                              val.payload.payload_fields.batteryVoltage
                            ) -
                              1.8) *
                              100) /
                            (3.65 - 1.8)
                          ).toFixed(2),
                          boardTemperature:
                            val.payload.payload_fields.boardTemperature !=
                            undefined
                              ? val.payload.payload_fields.boardTemperature.toFixed(
                                  2
                                )
                              : 0,
                          busVoltage:
                            val.payload.payload_fields.busVoltage != undefined
                              ? val.payload.payload_fields.busVoltage.toFixed(2)
                              : 0,
                        };
                        data.battrey_level =
                          data.battrey_level > 100 ? 100 : data.battrey_level;
                        data.battrey_level =
                          data.battrey_level < 0 ? 0 : data.battrey_level;
                        result.rows[index] = data;
                      });
                      break;
                    case 6:
                      console.log("TEMPERATURE");
                      console.log(result.rows.length);
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);

                        const data = {
                          devEui: val.payload.devEUI,
                          update_time: val.payload.reportTime,
                          Temperature: val.payload.meter.exReading[0].value,
                          Humidity: val.payload.meter.exReading[1].value,
                          battery: val.payload.meter.exReading[2].value,
                        };
                        console.log(index);
                        result.rows[index] = data;
                        console.log(result.rows[index]);
                      });
                      break;

                    case 7:
                      console.log("ELECTRICITY CT");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        result.rows[index].usage =
                          result.rows[index].usage / 1000 <= 0
                            ? 0
                            : (result.rows[index].usage / 1000).toFixed(2);

                        const data = {
                          devEui: val.payload.devEUI,
                          update_time: val.payload.reportTime,
                          totalizer: val.payload.meter.meterReading,
                          valve: val.payload.meter.valve,
                          usage: "",
                          balance: val.balance,
                          battrey_level: val.payload.meter.battery,
                        };
                        result.rows[index].payload = data;
                      });
                      break;

                    default:
                      break;
                  }
                  resolve(result);
                })
                .catch((error) => console.log(error));
            })
            .catch((error) => console.log(error))
        : models.iot_histories
            .findAndCountAll({
              where: data,
              ...pag,
              order: [["reportTimeFlag", "DESC"]],
              include: [
                {
                  model: models.iot_nodes,
                  include: [
                    { model: models.iot_device_type },
                    { model: models.iot_node_type },

                    { model: models.iot_detail_water },
                    { model: models.iot_detail_gas, as: "iot_detail_gas" },
                    { model: models.iot_detail_electric },
                    { model: models.iot_detail_rtu },
                    { model: models.iot_detail_temperature },
                    { model: models.iot_detail_preassure },
                  ],
                },
              ],
            })
            .then((result) => {
              result.page = page;
              result.size = size;
              if (result.rows.length == 0) {
                resolve(result);
                return result;
              }

              return models.iot_histories
                .findOne({
                  where: {
                    device_id: data.device_id,
                    id: { [Op.lt]: result.rows[result.rows.length - 1].id },
                  },
                  order: [["reportTimeFlag", "DESC"]],
                })
                .then((res_before) => {
                  res_before = JSON.parse(JSON.stringify(res_before));
                  if (res_before == null) {
                    res_before = 0;
                  } else {
                    res_before = JSON.parse(res_before.payload).meter
                      .meterReading;
                  }
                  switch (parseInt(typeId)) {
                    case 1:
                      console.log("GAS MOBILE");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        const data = {
                          devEui: val.payload.devEUI,
                          update_time: val.payload.reportTime,
                          totalizer: val.payload.meter.meterReading,
                          valve: val.payload.meter.valve,
                          usage:
                            result.rows.length - index == 1
                              ? 0
                              : parseFloat(
                                  result.rows[index].payload.meter
                                    .meterReading -
                                    JSON.parse(result.rows[index + 1].payload)
                                      .meter.meterReading
                                ) / 1000,
                          balance: val.balance,
                          battrey_level: val.payload.meter.battery,
                        };
                        result.rows[index].usage = data.usage;
                        result.rows[index].payload = data;
                      });
                      break;
                    case 2:
                      console.log("WATER :");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        result.rows[index].usage =
                          result.rows[index].usage / 1000 <= 0
                            ? 0
                            : (result.rows[index].usage / 1000).toFixed(2);
                        const data = {
                          devEui: val.payload.devEUI,
                          update_time: val.payload.reportTime,
                          totalizer: val.payload.meter.meterReading,
                          valve: val.payload.meter.valve,
                          usage: "",
                          balance: val.balance,
                          battrey_level: val.payload.meter.battery,
                        };
                        result.rows[index].payload = data;
                      });
                      break;
                    case 3:
                      console.log("ELECTRICITY NON CT");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        result.rows[index].usage =
                          result.rows[index].usage / 1000 <= 0
                            ? 0
                            : (result.rows[index].usage / 1000).toFixed(2);
                        const data = {
                          devEui: val.payload.devEUI,
                          update_time: val.payload.reportTime,
                          totalizer: val.payload.meter.meterReading,
                          valve: val.payload.meter.valve,
                          usage: "",
                          balance: val.balance,
                          battrey_level: val.payload.meter.battery,
                        };
                        result.rows[index].payload = data;
                      });
                      break;
                    case 4:
                      console.log("RTU");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        let NewData = "";
                        val.payload.subMeter.map((val) => {
                          if (val.applicationName == child) {
                            NewData = val;
                          }
                        });
                        const data = {
                          devEui: val.payload.devEUI,
                          update_time: val.payload.reportTime,
                          raw: NewData.report,
                        };
                        result.rows[index] = data;
                      });
                      break;
                    case 5:
                      console.log("PRESSURE");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        const data = {
                          devEui: val.payload.hardware_serial,
                          update_time: val.payload.reportTime,
                          preassure:
                            val.payload.payload_fields.gasPressure.toFixed(2),
                          battrey_level:
                            ((parseFloat(
                              val.payload.payload_fields.batteryVoltage
                            ) -
                              1.8) *
                              100) /
                            (3.65 - 1.8),
                        };
                        data.battrey_level = parseFloat(
                          data.battrey_level.toFixed(2)
                        );
                        data.battrey_level =
                          data.battrey_level > 100
                            ? 100
                            : data.battrey_level < 0
                            ? 0
                            : data.battrey_level;
                        result.rows[index] = data;
                      });
                      break;
                    case 10:
                      console.log("PJU");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        const data = {
                          devEui: val.payload.devEUI,
                          update_time: val.payload.reportTime,
                          tegangan: val.payload.payload_fields.tegangan,
                          arus: val.payload.payload_fields.arus,
                          power: val.payload.payload_fields.power,
                          power_factor: val.payload.payload_fields.power_factor,
                          cahaya: val.payload.payload_fields.cahaya,
                          relay: val.payload.payload_fields.relay,
                          lampu: val.payload.payload_fields.lampu,
                        };
                        result.rows[index] = data;
                      });
                      break;
                    case 8:
                      console.log("WATER PRESSURE");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        const data = {
                          devEui: val.payload.hardware_serial,
                          update_time: val.payload.reportTime,
                          pressureRaw:
                            val.payload.payload_fields.pressureRaw != undefined
                              ? val.payload.payload_fields.pressureRaw.toFixed(
                                  2
                                )
                              : 0,
                          pressure:
                            val.payload.payload_fields.pressure != undefined
                              ? val.payload.payload_fields.pressure.toFixed(2)
                              : 0,
                          batteryVoltage:
                            val.payload.payload_fields.batteryVoltage !=
                            undefined
                              ? val.payload.payload_fields.batteryVoltage.toFixed(
                                  2
                                )
                              : 0,
                          battrey_level: (
                            ((parseFloat(
                              val.payload.payload_fields.batteryVoltage
                            ) -
                              1.8) *
                              100) /
                            (3.65 - 1.8)
                          ).toFixed(2),
                          boardTemperature:
                            val.payload.payload_fields.boardTemperature !=
                            undefined
                              ? val.payload.payload_fields.boardTemperature.toFixed(
                                  2
                                )
                              : 0,
                          busVoltage:
                            val.payload.payload_fields.busVoltage != undefined
                              ? val.payload.payload_fields.busVoltage.toFixed(2)
                              : 0,
                        };
                        data.battrey_level =
                          data.battrey_level > 100 ? 100 : data.battrey_level;
                        data.battrey_level =
                          data.battrey_level < 0 ? 0 : data.battrey_level;
                        result.rows[index] = data;
                      });
                      break;
                    case 9:
                      console.log("PRESSURE PT");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        const data = {
                          devEui: val.payload.hardware_serial,
                          update_time: val.payload.reportTime,
                          preassure:
                            parseFloat(
                              val.payload.payload_fields.gasPressure.toFixed(2)
                            ) *
                              2 +
                            10,
                          battrey_level: (
                            ((parseFloat(
                              val.payload.payload_fields.batteryVoltage
                            ) -
                              1.8) *
                              100) /
                            (3.65 - 1.8)
                          ).toFixed(2),
                        };
                        data.battrey_level =
                          data.battrey_level > 100 ? 100 : data.battrey_level;
                        data.battrey_level =
                          data.battrey_level < 0 ? 0 : data.battrey_level;
                        result.rows[index] = data;
                      });
                      break;
                    case 6:
                      console.log("TEMPERATURE");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);

                        const data = {
                          devEui: val.payload.devEUI,
                          update_time: val.payload.reportTime,
                          Temperature: val.payload.meter.exReading[0].value,
                          Humidity: val.payload.meter.exReading[1].value,
                        };
                        result.rows[index] = data;
                      });
                      break;

                    case 7:
                      console.log("ELECTRICITY CT");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        result.rows[index].usage =
                          result.rows[index].usage / 1000 <= 0
                            ? 0
                            : (result.rows[index].usage / 1000).toFixed(2);
                        const data = {
                          devEui: val.payload.devEUI,
                          update_time: val.payload.reportTime,
                          totalizer: val.payload.meter.meterReading,
                          valve: val.payload.meter.valve,
                          usage: "",
                          balance: val.balance,
                          battrey_level: val.payload.meter.battery,
                        };
                        result.rows[index].payload = data;
                      });
                      break;

                    default:
                      break;
                  }
                  resolve(result);
                })
                .catch((error) => console.log(error));
            })
            .catch((error) => console.log(error));
    });
  },
  servicePaginationHistory: (data, pagination, pagination_flag = false) => {
    return new Promise((resolve, reject) => {
      const page = data.page;
      const size = data.size;
      const typeId = data.typeId;
      delete data.typeId;
      delete data.page;
      delete data.size;
      let child = "";
      if (data.child != undefined) {
        child = data.child;
        delete data.child;
      }
      let pag = {};
      if (pagination_flag == true) {
        if (parseInt(typeId) == 4) {
          pagination.limit += 1;
        }
        pag = {
          limit: pagination.limit,
          offset: pagination.offset,
        };
      }
      data.date_start != undefined || data.date_end != undefined
        ? models.iot_histories
            .findAndCountAll({
              // attributes: { exclude: ["id"] },
              // group: "reportTime",
              where: {
                device_id: data.device_id,
                [Op.and]: [
                  {
                    reportTime: models.sequelize.where(
                      models.sequelize.fn(
                        "date",
                        models.sequelize.col("iot_histories.reportTimeFlag")
                      ),
                      ">=",
                      moment(data.date_start).format("YYYY-MM-DD")
                    ),
                  },
                  {
                    reportTime: models.sequelize.where(
                      models.sequelize.fn(
                        "date",
                        models.sequelize.col("iot_histories.reportTimeFlag")
                      ),
                      "<",
                      moment(data.date_end).format("YYYY-MM-DD")
                    ),
                  },
                ],
                // [Op.or]: [
                //   { reportTimeFlag: { [Op.gte]: "2020-06-29" } },
                //   { reportTimeFlag: { [Op.lt]: "2020-06-30" } },
                // ],

                // reportTime: {
                //   [Op.between]: [data.date_start, data.date_end],
                // },
              },
              ...pag,
              order: [["reportTimeFlag", "DESC"]],
              include: [
                {
                  model: models.iot_nodes,
                  include: [
                    { model: models.iot_device_type },
                    { model: models.iot_node_type },

                    { model: models.iot_detail_water },
                    { model: models.iot_detail_gas, as: "iot_detail_gas" },
                    { model: models.iot_detail_electric },
                    { model: models.iot_detail_rtu },
                    { model: models.iot_detail_temperature },
                    { model: models.iot_detail_preassure },
                  ],
                },
              ],
            })
            .then((result) => {
              result = JSON.parse(JSON.stringify(result));
              result.page = page;
              result.size = size;
              if (result.rows.length == 0) {
                resolve(result);
                return result;
              }

              return models.iot_histories
                .findOne({
                  where: {
                    device_id: data.device_id,
                    id: { [Op.lt]: result.rows[result.rows.length - 1].id },
                  },
                  order: [["reportTimeFlag", "DESC"]],
                })
                .then((res_before) => {
                  res_before = JSON.parse(JSON.stringify(res_before));
                  if (res_before == null) {
                    res_before = 0;
                  } else {
                    res_before = JSON.parse(res_before.payload).meter
                      .meterReading;
                  }

                  switch (parseInt(typeId)) {
                    case 1:
                      console.log("GAS");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        const data = {
                          devEui: val.payload.devEUI,
                          update_time: val.payload.reportTime,
                          totalizer: val.payload.meter.meterReading,
                          valve: val.payload.meter.valve,
                          usage:
                            result.rows.length - index == 1
                              ? 0
                              : parseFloat(
                                  result.rows[index].payload.meter
                                    .meterReading -
                                    JSON.parse(result.rows[index + 1].payload)
                                      .meter.meterReading
                                ) / 1000,
                          balance: val.balance,
                          battrey_level: val.payload.meter.battery,
                        };
                        result.rows[index].usage =
                          result.rows.length - index == 1
                            ? 0
                            : parseFloat(
                                result.rows[index].payload.meter.meterReading -
                                  JSON.parse(result.rows[index + 1].payload)
                                    .meter.meterReading
                              ) / 1000;
                        result.rows[index].payload = data;
                      });
                      break;
                    case 2:
                      console.log("WATER ?");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        const data = {
                          devEui: val.payload.devEUI,
                          update_time: val.payload.reportTime,
                          totalizer: val.payload.meter.meterReading,
                          valve: val.payload.meter.valve,
                          usage:
                            result.rows.length - index == 1
                              ? 0
                              : parseFloat(
                                  result.rows[index].payload.meter
                                    .meterReading -
                                    JSON.parse(result.rows[index + 1].payload)
                                      .meter.meterReading
                                ) / 1000,
                          balance: val.balance,
                          battrey_level: val.payload.meter.battery,
                        };
                        result.rows[index].usage =
                          result.rows.length - index == 1
                            ? 0
                            : parseFloat(
                                result.rows[index].payload.meter.meterReading -
                                  JSON.parse(result.rows[index + 1].payload)
                                    .meter.meterReading
                              ) / 1000;
                        result.rows[index].payload = data;
                      });
                      break;
                    case 3:
                      console.log("ELECTRICITY NON CT");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);

                        const data = {
                          devEui: val.payload.devEUI,
                          update_time: val.payload.reportTime,
                          totalizer: val.payload.meter.meterReading,
                          valve: val.payload.meter.valve,
                          usage: "",
                          balance: val.balance,
                          battrey_level: val.payload.meter.battery,
                        };
                        result.rows[index].payload = data;
                      });
                      break;
                    case 4:
                      let sn = JSON.parse(JSON.stringify(result.rows[0]))
                        .iot_node.iot_detail_rtu.serial_number;
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        let NewData = "";
                        val.payload.subMeter.map((valP) => {
                          if (valP.applicationName == child) {
                            NewData = valP;
                            NewData.report.map((valN, idxN) => {
                              if (
                                valN.name.trim().toLocaleLowerCase() == "status"
                              ) {
                                if (
                                  parseInt(
                                    parseInt(valN.value).toString(2)[0]
                                  ) == 1
                                ) {
                                  NewData.report[idxN].value = "ON";
                                } else {
                                  NewData.report[idxN].value = "OFF";
                                }
                              }
                            });
                            NewData.report.unshift({
                              name: "Battery",
                              value: val.iot_node.live_battery + "%",
                            });
                          }
                        });
                        const data = {
                          devEui: val.payload.devEUI,
                          update_time: val.payload.reportTime,
                          raw: NewData.report,
                        };
                        result.rows[index] = data;
                      });
                      break;
                    case 5:
                      console.log("PRESSURE");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        const data = {
                          devEui: val.payload.hardware_serial,
                          update_time: val.payload.reportTime,
                          preassure:
                            val.payload.payload_fields.gasPressure.toFixed(2),
                          battrey_level:
                            val.payload.payload_fields.batteryVoltage,
                        };
                        result.rows[index] = data;
                      });
                      break;
                    case 10:
                      console.log("PJU");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        const data = {
                          devEui: val.payload.devEUI,
                          update_time: val.payload.reportTime,
                          tegangan: val.payload.payload_fields.tegangan,
                          arus: val.payload.payload_fields.arus,
                          power: val.payload.payload_fields.power,
                          power_factor: val.payload.payload_fields.power_factor,
                          cahaya: val.payload.payload_fields.cahaya,
                          relay: val.payload.payload_fields.relay,
                          lampu: val.payload.payload_fields.lampu,
                        };
                        result.rows[index] = data;
                      });
                      break;
                    case 9:
                      console.log("PRESSURE");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        const data = {
                          devEui: val.payload.hardware_serial,
                          update_time: val.payload.reportTime,
                          preassure:
                            parseFloat(
                              val.payload.payload_fields.gasPressure.toFixed(2)
                            ) *
                              2 +
                            10,
                          battrey_level: (
                            ((parseFloat(
                              val.payload.payload_fields.batteryVoltage
                            ) -
                              1.8) *
                              100) /
                            (3.65 - 1.8)
                          ).toFixed(2),
                        };
                        data.battrey_level =
                          data.battrey_level > 100 ? 100 : data.battrey_level;
                        data.battrey_level =
                          data.battrey_level < 0 ? 0 : data.battrey_level;
                        result.rows[index] = data;
                      });
                      break;
                    case 8:
                      console.log("WATER PRESSURE");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        const data = {
                          devEui: val.payload.hardware_serial,
                          update_time: val.payload.reportTime,
                          pressureRaw:
                            val.payload.payload_fields.pressureRaw != undefined
                              ? val.payload.payload_fields.pressureRaw.toFixed(
                                  2
                                )
                              : 0,
                          pressure:
                            val.payload.payload_fields.pressure != undefined
                              ? val.payload.payload_fields.pressure.toFixed(2)
                              : 0,
                          batteryVoltage:
                            val.payload.payload_fields.batteryVoltage !=
                            undefined
                              ? val.payload.payload_fields.batteryVoltage.toFixed(
                                  2
                                )
                              : 0,
                          battrey_level: (
                            ((parseFloat(
                              val.payload.payload_fields.batteryVoltage
                            ) -
                              1.8) *
                              100) /
                            (3.65 - 1.8)
                          ).toFixed(2),
                          boardTemperature:
                            val.payload.payload_fields.boardTemperature !=
                            undefined
                              ? val.payload.payload_fields.boardTemperature.toFixed(
                                  2
                                )
                              : 0,
                          busVoltage:
                            val.payload.payload_fields.busVoltage != undefined
                              ? val.payload.payload_fields.busVoltage.toFixed(2)
                              : 0,
                        };
                        data.battrey_level =
                          data.battrey_level > 100 ? 100 : data.battrey_level;
                        data.battrey_level =
                          data.battrey_level < 0 ? 0 : data.battrey_level;
                        result.rows[index] = data;
                      });
                      break;
                    case 6:
                      console.log("TEMPERATURE");
                      console.log(result.rows.length);
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);

                        const data = {
                          devEui: val.payload.devEUI,
                          update_time: val.payload.reportTime,
                          Temperature: val.payload.meter.exReading[0].value,
                          Humidity: val.payload.meter.exReading[1].value,
                          battery: val.payload.meter.exReading[2].value,
                        };
                        console.log(index);
                        result.rows[index] = data;
                        console.log(result.rows[index]);
                      });
                      break;

                    case 7:
                      console.log("ELECTRICITY CT");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        let usg = 0;
                        if (index != result.rows.length - 1) {
                          usg =
                            val.payload.meter.meterReading -
                            JSON.parse(result.rows[index + 1].payload).meter
                              .meterReading;
                        } else {
                          usg = val.payload.meter.meterReading - res_before;
                        }
                        const data = {
                          usage: usg,
                          devEui: val.payload.devEUI,
                          update_time: val.payload.reportTime,
                          raw: val.payload,
                        };
                        result.rows[index] = data;
                      });
                      break;

                    default:
                      break;
                  }
                  resolve(result);
                })
                .catch((error) => console.log(error));
            })
            .catch((error) => console.log(error))
        : models.iot_histories
            .findAndCountAll({
              where: data,
              ...pag,
              order: [["reportTimeFlag", "DESC"]],
              include: [
                {
                  model: models.iot_nodes,
                  include: [
                    { model: models.iot_device_type },
                    { model: models.iot_node_type },

                    { model: models.iot_detail_water },
                    { model: models.iot_detail_gas, as: "iot_detail_gas" },
                    { model: models.iot_detail_electric },
                    { model: models.iot_detail_rtu },
                    { model: models.iot_detail_temperature },
                    { model: models.iot_detail_preassure },
                  ],
                },
              ],
            })
            .then((result) => {
              result.page = page;
              result.size = size;
              if (result.rows.length == 0) {
                resolve(result);
                return result;
              }

              return models.iot_histories
                .findOne({
                  where: {
                    device_id: data.device_id,
                    id: { [Op.lt]: result.rows[result.rows.length - 1].id },
                  },
                  order: [["reportTimeFlag", "DESC"]],
                })
                .then((res_before) => {
                  res_before = JSON.parse(JSON.stringify(res_before));
                  if (res_before == null) {
                    res_before = 0;
                  } else {
                    res_before = JSON.parse(res_before.payload).meter
                      .meterReading;
                  }
                  switch (parseInt(typeId)) {
                    case 1:
                      console.log("GAS");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        const data = {
                          devEui: val.payload.devEUI,
                          update_time: val.payload.reportTime,
                          totalizer: val.payload.meter.meterReading,
                          valve: val.payload.meter.valve,
                          usage: val.usage,
                          balance: val.balance,
                          battrey_level: val.payload.meter.battery,
                        };
                        result.rows[index].payload = data;
                      });
                      break;
                    case 2:
                      console.log("WATER");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        const data = {
                          devEui: val.payload.devEUI,
                          update_time: val.payload.reportTime,
                          totalizer: val.payload.meter.meterReading,
                          valve: val.payload.meter.valve,
                          usage:
                            result.rows.length - index == 1
                              ? 0
                              : parseFloat(
                                  result.rows[index].payload.meter
                                    .meterReading -
                                    JSON.parse(result.rows[index + 1].payload)
                                      .meter.meterReading
                                ) / 1000,
                          balance: val.balance,
                          battrey_level: val.payload.meter.battery,
                        };
                        result.rows[index].usage =
                          result.rows.length - index == 1
                            ? 0
                            : parseFloat(
                                result.rows[index].payload.meter.meterReading -
                                  JSON.parse(result.rows[index + 1].payload)
                                    .meter.meterReading
                              ) / 1000;
                        result.rows[index].payload = data;
                      });
                      break;
                    case 3:
                      console.log("ELECTRICITY NON CT");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        const data = {
                          devEui: val.payload.devEUI,
                          update_time: val.payload.reportTime,
                          totalizer: val.payload.meter.meterReading,
                          valve: val.payload.meter.valve,
                          usage: "",
                          balance: val.balance,
                          battrey_level: val.payload.meter.battery,
                        };
                        result.rows[index].payload = data;
                      });
                      break;
                    case 4:
                      console.log("RTU_4_b");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        let NewData = "";
                        val.payload.subMeter.map((val) => {
                          if (val.applicationName == child) {
                            NewData = val;
                          }
                        });
                        const data = {
                          devEui: val.payload.devEUI,
                          update_time: val.payload.reportTime,
                          raw: NewData.report,
                        };
                        result.rows[index] = data;
                      });
                      break;
                    case 5:
                      console.log("PRESSURE");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        const data = {
                          devEui: val.payload.hardware_serial,
                          update_time: val.payload.reportTime,
                          preassure:
                            val.payload.payload_fields.gasPressure.toFixed(2),
                          battrey_level:
                            val.payload.payload_fields.batteryVoltage,
                        };
                        result.rows[index] = data;
                      });
                      break;
                    case 10:
                      console.log("PJU");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        const data = {
                          devEui: val.payload.devEUI,
                          update_time: val.payload.reportTime,
                          tegangan: val.payload.payload_fields.tegangan,
                          arus: val.payload.payload_fields.arus,
                          power: val.payload.payload_fields.power,
                          power_factor: val.payload.payload_fields.power_factor,
                          cahaya: val.payload.payload_fields.cahaya,
                          relay: val.payload.payload_fields.relay,
                          lampu: val.payload.payload_fields.lampu,
                        };
                        result.rows[index] = data;
                      });
                      break;
                    case 8:
                      console.log("WATER PRESSURE");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        const data = {
                          devEui: val.payload.hardware_serial,
                          update_time: val.payload.reportTime,
                          pressureRaw:
                            val.payload.payload_fields.pressureRaw != undefined
                              ? val.payload.payload_fields.pressureRaw.toFixed(
                                  2
                                )
                              : 0,
                          pressure:
                            val.payload.payload_fields.pressure != undefined
                              ? val.payload.payload_fields.pressure.toFixed(2)
                              : 0,
                          batteryVoltage:
                            val.payload.payload_fields.batteryVoltage !=
                            undefined
                              ? val.payload.payload_fields.batteryVoltage.toFixed(
                                  2
                                )
                              : 0,
                          battrey_level: (
                            ((parseFloat(
                              val.payload.payload_fields.batteryVoltage
                            ) -
                              1.8) *
                              100) /
                            (3.65 - 1.8)
                          ).toFixed(2),
                          boardTemperature:
                            val.payload.payload_fields.boardTemperature !=
                            undefined
                              ? val.payload.payload_fields.boardTemperature.toFixed(
                                  2
                                )
                              : 0,
                          busVoltage:
                            val.payload.payload_fields.busVoltage != undefined
                              ? val.payload.payload_fields.busVoltage.toFixed(2)
                              : 0,
                        };
                        data.battrey_level =
                          data.battrey_level > 100 ? 100 : data.battrey_level;
                        data.battrey_level =
                          data.battrey_level < 0 ? 0 : data.battrey_level;
                        result.rows[index] = data;
                      });
                      break;
                    case 9:
                      console.log("PRESSURE PT");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        const data = {
                          devEui: val.payload.hardware_serial,
                          update_time: val.payload.reportTime,
                          preassure:
                            parseFloat(
                              val.payload.payload_fields.gasPressure.toFixed(2)
                            ) *
                              2 +
                            10,
                          battrey_level: (
                            ((parseFloat(
                              val.payload.payload_fields.batteryVoltage
                            ) -
                              1.8) *
                              100) /
                            (3.65 - 1.8)
                          ).toFixed(2),
                        };
                        data.battrey_level =
                          data.battrey_level > 100 ? 100 : data.battrey_level;
                        data.battrey_level =
                          data.battrey_level < 0 ? 0 : data.battrey_level;
                        result.rows[index] = data;
                      });
                      break;
                    case 6:
                      console.log("TEMPERATURE");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);

                        const data = {
                          devEui: val.payload.devEUI,
                          update_time: val.payload.reportTime,
                          Temperature: val.payload.meter.exReading[0].value,
                          Humidity: val.payload.meter.exReading[1].value,
                        };
                        result.rows[index] = data;
                      });
                      break;

                    case 7:
                      console.log("ELECTRICITY CT");
                      result.rows.map((val, index) => {
                        result.rows[index].payload = JSON.parse(val.payload);
                        let usg = 0;
                        if (index != result.rows.length - 1) {
                          usg =
                            val.payload.meter.meterReading -
                            JSON.parse(result.rows[index + 1].payload).meter
                              .meterReading;
                        } else {
                          usg = val.payload.meter.meterReading - res_before;
                        }
                        const data = {
                          usage: usg,
                          devEui: val.payload.devEUI,
                          update_time: val.payload.reportTime,
                          raw: val.payload,
                        };
                        result.rows[index] = data;
                      });
                      break;

                    default:
                      break;
                  }
                  resolve(result);
                })
                .catch((error) => console.log(error));
            })
            .catch((error) => console.log(error));
    });
  },
  serviceHistoryFilterSum: (data, pagination) => {
    const page = data.page;
    const size = data.size;
    const typeId = data.typeId;
    delete data.typeId;
    delete data.page;
    delete data.size;
    let child = "";
    if (data.child != undefined) {
      child = data.child;
      delete data.child;
    }
    return new Promise((resolve, reject) => {
      models.iot_histories
        .findAndCountAll({
          where: {
            device_id: data.device_id,
            reportTime: {
              [Op.between]: [data.date_start, data.date_end],
            },
          },
          // limit: pagination.limit,
          // offset: pagination.offset,
          order: [["reportTimeFlag", "DESC"]],
        })
        .then((result) => {
          let dataResult = result.rows;
          let flag = 0;
          let final = [];
          dataResult.map((val, index) => {
            if (
              dataResult[index + 1] != undefined &&
              dataResult[index].reportTime.split("T")[0] ==
                dataResult[index + 1].reportTime.split("T")[0]
            ) {
              console.log(JSON.parse(dataResult[0].payload).meter.meterReading);
            } else {
              flag++;
            }
          });
          console.log(final);
          resolve({
            responseCode: 200,
            history_node: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            error: err,
          });
        });
    });
  },
  // serviceHistoryNodev2: (data, page, page_size, typeId) => {
  //   return new Promise((resolve, reject) => {
  //     let child = "";
  //     if (data.child != undefined) {
  //       child = data.child;
  //       delete data.child;
  //     }
  //     console.log(data);
  //     models.iot_histories
  //       .findAll({
  //         limit: page_size,
  //         offset: page,
  //         where: data,
  //         order: [["id", "DESC"]],
  //         include: [
  //           {
  //             model: models.iot_nodes,
  //             include: [
  //               { model: models.iot_device_type },
  //               { model: models.iot_node_type },

  //               { model: models.iot_detail_water },
  //               { model: models.iot_detail_gas, as: "iot_detail_gas" },
  //               { model: models.iot_detail_electric },
  //               { model: models.iot_detail_rtu },
  //               { model: models.iot_detail_temperature },
  //               { model: models.iot_detail_preassure }
  //             ]
  //           }
  //         ]
  //       })
  //       .then(result => {
  //         result = JSON.parse(JSON.stringify(result));
  //         console.log(result);
  //         switch (parseInt(typeId)) {
  //           case 1:
  //             console.log("GAS");
  //             result.map((val, index) => {
  //               result[index].payload = JSON.parse(val.payload);
  //               const data = {
  //                 devEui: val.payload.devEUI,
  //                 update_time: val.payload.reportTime,
  //                 totalizer: val.payload.meter.meterReading,
  //                 valve: val.payload.meter.valve,
  //                 usage: "",
  //                 balance:
  //                   parseInt(val.iot_node.iot_detail_gas.balance) -
  //                   parseInt(val.payload.meter.meterReading),
  //                 battrey_level: val.payload.meter.battery
  //               };
  //               result[index].payload = data;
  //             });
  //             break;
  //           case 2:
  //             console.log("WATER");
  //             result.map((val, index) => {
  //               result[index].payload = JSON.parse(val.payload);
  //               const data = {
  //                 devEui: val.payload.devEUI,
  //                 update_time: val.payload.reportTime,
  //                 totalizer: val.payload.meter.meterReading,
  //                 valve: val.payload.meter.valve,
  //                 usage: "",
  //                 balance:
  //                   parseInt(val.iot_node.iot_detail_water.balance) -
  //                   parseInt(val.payload.meter.meterReading),
  //                 battrey_level: val.payload.meter.battery
  //               };
  //               result[index].payload = data;
  //             });
  //             break;
  //           case 3:
  //             console.log("ELECTRICITY NON CT");
  //             result.map((val, index) => {
  //               result[index].payload = JSON.parse(val.payload);

  //               const data = {
  //                 devEui: val.payload.devEUI,
  //                 update_time: val.payload.reportTime,
  //                 totalizer: val.payload.meter.meterReading,
  //                 valve: val.payload.meter.valve,
  //                 usage: "",
  //                 balance:
  //                   parseInt(val.iot_node.iot_detail_electric.balance) -
  //                   parseInt(val.payload.meter.meterReading),
  //                 battrey_level: val.payload.meter.battery
  //               };
  //               result[index].payload = data;
  //             });
  //             break;
  //           case 4:
  //             console.log("RTU");
  //             result.map((val, index) => {
  //               result[index].payload = JSON.parse(val.payload);
  //               let NewData = "";
  //               val.payload.subMeter.map(val => {
  //                 if (val.applicationName == child) {
  //                   NewData = val;
  //                 }
  //               });
  //               const data = {
  //                 devEui: val.payload.devEUI,
  //                 update_time: val.payload.reportTime,
  //                 raw: NewData.report
  //               };
  //               result[index] = data;
  //             });
  //             break;
  //           case 5:
  //             console.log("PRESSURE");
  //             result.map((val, index) => {
  //               result[index].payload = JSON.parse(val.payload);
  //               const data = {
  //                 devEui: val.payload.hardware_serial,
  //                 update_time: val.payload.reportTime,
  //                 preassure: val.payload.payload_fields.gasPressure.toFixed(2),
  //                 battrey_level: val.payload.payload_fields.batteryVoltage
  //               };
  //               result[index] = data;
  //             });
  //             break;
  //           case 6:
  //             console.log("TEMPERATURE");
  //             result.map((val, index) => {
  //               result[index].payload = JSON.parse(val.payload);

  //               const data = {
  //                 devEui: val.payload.devEUI,
  //                 update_time: val.payload.reportTime,
  //                 Temperature: val.payload.meter.exReading[0].value,
  //                 Humidity: val.payload.meter.exReading[1].value
  //               };
  //               result[index] = data;
  //             });
  //             break;

  //           case 7:
  //             console.log("ELECTRICITY CT");
  //             result.map((val, index) => {
  //               result[index].payload = JSON.parse(val.payload);

  //               const data = {
  //                 devEui: val.payload.devEUI,
  //                 update_time: val.payload.reportTime,
  //                 raw: val.payload
  //               };
  //               result[index] = data;
  //             });
  //             break;

  //           default:
  //             break;
  //         }

  //         resolve({
  //           responseCode: 200,
  //           history_node: result
  //         });
  //       })
  //       .catch(error => {
  //         console.log(error);
  //         reject({
  //           responseCode: 400,
  //           messages: error
  //         });
  //       });
  //   });
  // },

  serviceHistoryNode: (data, page, page_size) => {
    return new Promise((resolve, reject) => {
      models.iot_histories
        .findAll({
          limit: page_size,
          offset: page,
          where: data,
          include: models.iot_nodes,
        })
        .then((result) => {
          result = JSON.parse(JSON.stringify(result));
          // if (result.length==0) {
          result.map((val, index) => {
            result[index].payload = JSON.parse(val.payload);
          });
          resolve({
            responseCode: 200,
            messages: result,
          });
          // } else {
          //     newData=[]
          //     console.log(result)
          //     result.map(val=>{
          //         const payload = JSON.parse(val.payload)
          //         payload.reportTime = val.reportTime
          //         newData.push(payload)
          //     })
          //     resolve({
          //         responseCode:200,
          //         device_id:result[0].iot_node.id,
          //         devEui:result[0].iot_node.devEui,
          //         histories:newData
          //     })
          // }
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error,
          });
        });
    });
  },
  servicePaginationHistoryNopage: (data) => {
    return new Promise((resolve, reject) => {
      const page = data.page;
      const size = data.size;
      const typeId = data.typeId;
      delete data.typeId;
      delete data.page;
      delete data.size;
      let child = "";
      if (data.child != undefined) {
        child = data.child;
        delete data.child;
      }
      data.date_start != undefined || data.date_end != undefined
        ? models.iot_histories
            .findAndCountAll({
              attributes: { exclude: ["id"] },
              // group: "reportTime",
              where: {
                device_id: data.device_id,
                [Op.and]: [
                  {
                    reportTime: models.sequelize.where(
                      models.sequelize.fn(
                        "date",
                        models.sequelize.col("iot_histories.reportTimeFlag")
                      ),
                      ">=",
                      moment(data.date_start).format("YYYY-MM-DD")
                    ),
                  },
                  {
                    reportTime: models.sequelize.where(
                      models.sequelize.fn(
                        "date",
                        models.sequelize.col("iot_histories.reportTimeFlag")
                      ),
                      "<",
                      moment(data.date_end).format("YYYY-MM-DD")
                    ),
                  },
                ],
                // [Op.or]: [
                //   { reportTimeFlag: { [Op.gte]: "2020-06-29" } },
                //   { reportTimeFlag: { [Op.lt]: "2020-06-30" } },
                // ],

                // reportTime: {
                //   [Op.between]: [data.date_start, data.date_end],
                // },
              },
              order: [["reportTimeFlag", "DESC"]],
              include: [
                {
                  model: models.iot_nodes,
                  include: [
                    { model: models.iot_device_type },
                    { model: models.iot_node_type },

                    { model: models.iot_detail_water },
                    { model: models.iot_detail_gas, as: "iot_detail_gas" },
                    { model: models.iot_detail_electric },
                    { model: models.iot_detail_rtu },
                    { model: models.iot_detail_temperature },
                    { model: models.iot_detail_preassure },
                  ],
                },
              ],
            })
            .then((result) => {
              switch (parseInt(typeId)) {
                case 1:
                  console.log("GAS");
                  result.rows.map((val, index) => {
                    result.rows[index].payload = JSON.parse(val.payload);
                    const data = {
                      devEui: val.payload.devEUI,
                      update_time: val.payload.reportTime,
                      totalizer: val.payload.meter.meterReading,
                      valve: val.payload.meter.valve,
                      usage:
                        result.rows.length - index == 1
                          ? 0
                          : parseFloat(
                              result.rows[index].payload.meter.meterReading -
                                JSON.parse(result.rows[index + 1].payload).meter
                                  .meterReading
                            ) / 1000,
                      balance:
                        parseInt(val.iot_node.iot_detail_gas.balance) -
                        parseInt(val.payload.meter.meterReading),
                      battrey_level: val.payload.meter.battery,
                    };
                    result.rows[index].payload = data;
                  });
                  break;
                case 2:
                  console.log("WATER");
                  result.rows.map((val, index) => {
                    result.rows[index].payload = JSON.parse(val.payload);
                    const data = {
                      devEui: val.payload.devEUI,
                      update_time: val.payload.reportTime,
                      totalizer: val.payload.meter.meterReading,
                      valve: val.payload.meter.valve,
                      usage: "",
                      balance:
                        parseInt(val.iot_node.iot_detail_water.balance) -
                        parseInt(val.payload.meter.meterReading),
                      battrey_level: val.payload.meter.battery,
                    };
                    result.rows[index].payload = data;
                  });
                  break;
                case 3:
                  console.log("ELECTRICITY NON CT");
                  result.rows.map((val, index) => {
                    result.rows[index].payload = JSON.parse(val.payload);

                    const data = {
                      devEui: val.payload.devEUI,
                      update_time: val.payload.reportTime,
                      totalizer: val.payload.meter.meterReading,
                      valve: val.payload.meter.valve,
                      usage: "",
                      balance:
                        parseInt(val.iot_node.iot_detail_electric.balance) -
                        parseInt(val.payload.meter.meterReading),
                      battrey_level: val.payload.meter.battery,
                    };
                    result.rows[index].payload = data;
                  });
                  break;
                case 4:
                  console.log("RTU");
                  result.rows.map((val, index) => {
                    result.rows[index].payload = JSON.parse(val.payload);
                    let NewData = "";
                    val.payload.subMeter.map((val) => {
                      if (val.applicationName == child) {
                        NewData = val;
                      }
                    });
                    const data = {
                      devEui: val.payload.devEUI,
                      update_time: val.payload.reportTime,
                      raw: NewData.report,
                    };
                    result.rows[index] = data;
                  });
                  break;
                case 5:
                  console.log("PRESSURE");
                  result.rows.map((val, index) => {
                    result.rows[index].payload = JSON.parse(val.payload);
                    const data = {
                      devEui: val.payload.hardware_serial,
                      update_time: val.payload.reportTime,
                      preassure:
                        val.payload.payload_fields.gasPressure.toFixed(2),
                      battrey_level: val.payload.payload_fields.batteryVoltage,
                    };
                    result.rows[index] = data;
                  });
                  break;
                case 9:
                  console.log("PRESSURE");
                  result.rows.map((val, index) => {
                    result.rows[index].payload = JSON.parse(val.payload);
                    const data = {
                      devEui: val.payload.hardware_serial,
                      update_time: val.payload.reportTime,
                      preassure:
                        parseFloat(
                          val.payload.payload_fields.gasPressure.toFixed(2)
                        ) *
                          2 +
                        10,
                      battrey_level: val.payload.payload_fields.batteryVoltage,
                    };
                    result.rows[index] = data;
                  });
                  break;
                case 6:
                  console.log("TEMPERATURE");
                  console.log(result.rows.length);
                  result.rows.map((val, index) => {
                    result.rows[index].payload = JSON.parse(val.payload);

                    const data = {
                      devEui: val.payload.devEUI,
                      update_time: val.payload.reportTime,
                      Temperature: val.payload.meter.exReading[0].value,
                      Humidity: val.payload.meter.exReading[1].value,
                      battery: val.payload.meter.exReading[2].value,
                    };
                    console.log(index);
                    result.rows[index] = data;
                    console.log(result.rows[index]);
                  });
                  break;

                case 7:
                  console.log("ELECTRICITY CT");
                  result.rows.map((val, index) => {
                    result.rows[index].payload = JSON.parse(val.payload);

                    const data = {
                      devEui: val.payload.devEUI,
                      update_time: val.payload.reportTime,
                      raw: val.payload,
                    };
                    result.rows[index] = data;
                  });
                  break;

                default:
                  break;
              }
              result.page = page;
              result.size = size;
              resolve(result);
            })
            .catch((error) => console.log(error))
        : models.iot_histories
            .findAndCountAll({
              where: data,
              order: [["reportTimeFlag", "DESC"]],
              include: [
                {
                  model: models.iot_nodes,
                  include: [
                    { model: models.iot_device_type },
                    { model: models.iot_node_type },

                    { model: models.iot_detail_water },
                    { model: models.iot_detail_gas, as: "iot_detail_gas" },
                    { model: models.iot_detail_electric },
                    { model: models.iot_detail_rtu },
                    { model: models.iot_detail_temperature },
                    { model: models.iot_detail_preassure },
                  ],
                },
              ],
            })
            .then((result) => {
              switch (parseInt(typeId)) {
                case 1:
                  console.log("GAS");
                  result.rows.map((val, index) => {
                    result.rows[index].payload = JSON.parse(val.payload);
                    const data = {
                      devEui: val.payload.devEUI,
                      update_time: val.payload.reportTime,
                      totalizer: val.payload.meter.meterReading,
                      valve: val.payload.meter.valve,
                      usage: val.usage,
                      balance:
                        parseInt(val.iot_node.iot_detail_gas.balance) -
                        parseInt(val.payload.meter.meterReading),
                      battrey_level: val.payload.meter.battery,
                    };
                    result.rows[index].payload = data;
                  });
                  break;
                case 2:
                  console.log("WATER");
                  result.rows.map((val, index) => {
                    result.rows[index].payload = JSON.parse(val.payload);
                    const data = {
                      devEui: val.payload.devEUI,
                      update_time: val.payload.reportTime,
                      totalizer: val.payload.meter.meterReading,
                      valve: val.payload.meter.valve,
                      usage: "",
                      balance:
                        parseInt(val.iot_node.iot_detail_water.balance) -
                        parseInt(val.payload.meter.meterReading),
                      battrey_level: val.payload.meter.battery,
                    };
                    result.rows[index].payload = data;
                  });
                  break;
                case 3:
                  console.log("ELECTRICITY NON CT");
                  result.rows.map((val, index) => {
                    result.rows[index].payload = JSON.parse(val.payload);

                    const data = {
                      devEui: val.payload.devEUI,
                      update_time: val.payload.reportTime,
                      totalizer: val.payload.meter.meterReading,
                      valve: val.payload.meter.valve,
                      usage: "",
                      balance:
                        parseInt(val.iot_node.iot_detail_electric.balance) -
                        parseInt(val.payload.meter.meterReading),
                      battrey_level: val.payload.meter.battery,
                    };
                    result.rows[index].payload = data;
                  });
                  break;
                case 4:
                  console.log("RTU");
                  result.rows.map((val, index) => {
                    result.rows[index].payload = JSON.parse(val.payload);
                    let NewData = "";
                    val.payload.subMeter.map((val) => {
                      if (val.applicationName == child) {
                        NewData = val;
                      }
                    });
                    const data = {
                      devEui: val.payload.devEUI,
                      update_time: val.payload.reportTime,
                      raw: NewData.report,
                    };
                    result.rows[index] = data;
                  });
                  break;
                case 5:
                  console.log("PRESSURE");
                  result.rows.map((val, index) => {
                    result.rows[index].payload = JSON.parse(val.payload);
                    const data = {
                      devEui: val.payload.hardware_serial,
                      update_time: val.payload.reportTime,
                      preassure:
                        val.payload.payload_fields.gasPressure.toFixed(2),
                      battrey_level: val.payload.payload_fields.batteryVoltage,
                    };
                    result.rows[index] = data;
                  });
                  break;
                case 9:
                  console.log("PRESSURE PT");
                  result.rows.map((val, index) => {
                    result.rows[index].payload = JSON.parse(val.payload);
                    const data = {
                      devEui: val.payload.hardware_serial,
                      update_time: val.payload.reportTime,
                      preassure:
                        parseFloat(
                          val.payload.payload_fields.gasPressure.toFixed(2)
                        ) *
                          2 +
                        10,
                      battrey_level: val.payload.payload_fields.batteryVoltage,
                    };
                    result.rows[index] = data;
                  });
                  break;
                case 6:
                  console.log("TEMPERATURE");
                  result.rows.map((val, index) => {
                    result.rows[index].payload = JSON.parse(val.payload);

                    const data = {
                      devEui: val.payload.devEUI,
                      update_time: val.payload.reportTime,
                      Temperature: val.payload.meter.exReading[0].value,
                      Humidity: val.payload.meter.exReading[1].value,
                    };
                    result.rows[index] = data;
                  });
                  break;

                case 7:
                  console.log("ELECTRICITY CT");
                  result.rows.map((val, index) => {
                    result.rows[index].payload = JSON.parse(val.payload);

                    const data = {
                      devEui: val.payload.devEUI,
                      update_time: val.payload.reportTime,
                      raw: val.payload,
                    };
                    result.rows[index] = data;
                  });
                  break;

                default:
                  break;
              }
              result.page = page;
              result.size = size;
              resolve(result);
            })
            .catch((error) => console.log(error));
    });
  },
  servicePaginationHistorySumV2Mobile: (
    devEui,
    date_start,
    date_end,
    arr_date,
    arr_date2
  ) => {
    return new Promise((resolve, reject) => {
      models.sequelize
        .query(
          `SELECT * FROM iot_histories WHERE DATE(reportTimeFlag) <= '${moment(
            date_end
          ).format("YYYY-MM-DD")}' AND DATE(reportTimeFlag) >= '${moment(
            date_start
          ).format("YYYY-MM-DD")}' AND device_id=${devEui} 
       ORDER BY reportTime ASC`
        )
        .then((_res) => {
          _res = _res[0];
          models.iot_histories
            .findOne({
              where: {
                device_id: devEui,
                reportTimeFlag: models.sequelize.where(
                  models.sequelize.fn(
                    "date",
                    models.sequelize.col("iot_histories.`reportTimeFlag")
                  ),
                  "<",
                  moment(date_start).format("YYYY-MM-DD")
                ),
              },
              order: [["reportTime", "DESC"]],
            })
            .then((_res_f) => {
              _res_f = JSON.parse(JSON.stringify(_res_f));
              if (_res_f != null) {
                arr_date.unshift(
                  moment(_res_f.reportTimeFlag).format("YYYY-MM-DD")
                );
                _res.unshift(_res_f);
              }
              let output = { data: [] };
              arr_date.map((val, idx) => {
                let data_1 = _res.filter((valP) => {
                  return (
                    moment(valP.reportTimeFlag).format("YYYY-MM-DD") ==
                    moment(val).format("YYYY-MM-DD")
                  );
                });
                if (data_1.length > 0 && idx > 0 && output.data.length > 0) {
                  let pyd1 = JSON.parse(data_1[0].payload);
                  let pyd2 = JSON.parse(data_1[data_1.length - 1].payload);
                  console.log(pyd1.reportTime, pyd2.reportTime, val);
                  output.data.push({
                    last_update: moment(pyd2.reportTime)
                      .utcOffset("+0700")
                      .format("YYYY-MM-DD HH:mm:ss"),
                    totalizer: pyd2.meter.meterReading,
                  });
                } else if (
                  (data_1.length > 0 && idx == 0 && output.data.length == 0) ||
                  (data_1.length > 0 && idx > 0 && output.data.length == 0)
                ) {
                  let pyd1 = JSON.parse(data_1[0].payload);
                  let pyd2 = JSON.parse(data_1[data_1.length - 1].payload);
                  output.data.push({
                    last_update: moment(pyd2.reportTime)
                      .utcOffset("+0700")
                      .format("YYYY-MM-DD HH:mm:ss"),
                    totalizer: pyd2.meter.meterReading,
                    usage:
                      parseFloat(pyd2.meter.meterReading) -
                      parseFloat(pyd1.meter.meterReading),
                  });
                }
              });
              output.data.map((val, index) => {
                if (index > 0) {
                  output.data[index].usage =
                    output.data[index].totalizer -
                    output.data[index - 1].totalizer;
                }
              });

              if (_res_f != null) {
                let idx_remove = output.data.findIndex((valP) => {
                  return (
                    moment(valP.last_update).format("YYYY-MM-DD") ==
                    moment(arr_date[0]).format("YYYY-MM-DD")
                  );
                });
                if (idx_remove >= 0) {
                  output.data.splice(idx_remove, 1);
                }
              }
              output.data.map((val, idx) => {
                output.data[idx].totalizer = parseFloat(
                  (output.data[idx].totalizer / 1000).toFixed(3)
                );
                output.data[idx].usage = parseFloat(
                  (output.data[idx].usage / 1000).toFixed(3)
                );
              });
              resolve(output);
              return _res;
            });
        })
        .catch((error) => console.log(error));
    });
  },
  serviceListNodeLink: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_nodes
            .findOne({
              where: data,
              transaction: t,
              attributes: ["tenantId", "internalId"],
            })
            .then((result) => {
              result = JSON.parse(JSON.stringify(result));
              return models.iot_nodes.findAll({
                attributes: ["devEui", "id"],
                where: [
                  result,
                  { node_link_id: null, typeId: { [Op.in]: [5, 9, 11, 12] } },
                ],
                include: [
                  {
                    model: models.iot_node_type,
                    attributes: ["type_name", "type_id"],
                  },
                ],
                transaction: t,
              });
            });
        })
        .then((result) => {
          result = JSON.parse(JSON.stringify(result));
          resolve({
            responseCode: 200,
            list_node: result,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error,
          });
        });
    });
  },
  serviceSaveNodeLink: (update, data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_nodes
            .findOne({
              where: { id: data.id },
              transaction: t,
              attributes: ["id", "node_link_id"],
            })
            .then((result) => {
              result = JSON.parse(JSON.stringify(result));
              let to_null = [
                { id: result.id, node_link_id: null },
                { id: result.node_link_id, node_link_id: null },
              ];
              if (result.node_link_id == null) {
                return models.iot_nodes.bulkCreate(update, {
                  updateOnDuplicate: ["node_link_id"],
                });
              } else {
                return models.iot_nodes
                  .bulkCreate(to_null, {
                    updateOnDuplicate: ["node_link_id"],
                  })
                  .then(() => {
                    return models.iot_nodes.bulkCreate(update, {
                      updateOnDuplicate: ["node_link_id"],
                    });
                  });
              }
            });
        })
        .then((result) => {
          result = JSON.parse(JSON.stringify(result));
          resolve({
            responseCode: 200,
            resul: result,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error,
          });
        });
    });
  },
  serviceNodeUnlink: (update) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_nodes.bulkCreate(update, {
            updateOnDuplicate: ["node_link_id"],
          });
        })
        .then((result) => {
          result = JSON.parse(JSON.stringify(result));
          resolve({
            responseCode: 200,
            resul: result,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error,
          });
        });
    });
  },
  serviceCheckNodeRtuNew: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        let node = await axios({
          method: "GET",
          baseURL: process.env.HOST_SERVER,
          url: "/list/node/rtu/new",
          params: data,
        });
        resolve(node.data);
      } catch (error) {
        console.log(error);
        if (error.response) {
          reject({
            responseCode: 400,
            message: error.response.data,
          });
        } else {
          reject({
            responseCode: 400,
            message: error.message,
          });
        }
      }
    });
  },
};

const models = require("../../models/index");
const { Op } = models.Sequelize;
const moment = require("moment");
const { admin } = require("../../config/fcm_teknisi");
module.exports = {
  SvcGetTiketingTeknisi: (data, type, area_id) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_teknisi.findAll({
            where: [data, { status: true, area_id: { [Op.or]: [0, area_id] } }],
            include: [{ model: models.iots_node_type_teknisi, where: type }],
            transaction: t,
          });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            message: "Successfully Get Tiketing",
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },
  SvcGetTiketing: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_tiketing.findAll({
            where: data,
            include: [
              {
                model: models.iots_tiketing_attachment,
                as: "images",
                where: { is_teknisi: false },
                required: false,
              },
            ],
            order: [["createdAt", "DESC"]],
          });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            message: "Successfully Get Tiketing",
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },
  SvcAddTiketing2: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_tiketing
            .findOne({
              where: {
                createdAt1: models.sequelize.where(
                  models.sequelize.fn(
                    "year",
                    models.sequelize.col("iots_tiketing.createdAt")
                  ),
                  "=",
                  moment().utcOffset("+0700").format("YYYY")
                ),
                createdAt2: models.sequelize.where(
                  models.sequelize.fn(
                    "month",
                    models.sequelize.col("iots_tiketing.createdAt")
                  ),
                  "=",
                  moment().utcOffset("+0700").format("MM")
                ),
              },
              order: [["id", "DESC"]],
              transaction: t,
            })
            .then((_tiket) => {
              _tiket = JSON.parse(JSON.stringify(_tiket));
              if (_tiket == null) {
                data.no_tiket =
                  moment().utcOffset("+0700").format("YYYY-MM") +
                  "-" +
                  data.no_tiket +
                  "-1";
              } else {
                _tiket.no_tiket = _tiket.no_tiket.split("-");
                _tiket.no_tiket[_tiket.no_tiket.length - 1] =
                  parseInt(_tiket.no_tiket[_tiket.no_tiket.length - 1]) + 1;
                data.no_tiket = _tiket.no_tiket.join("-");
              }
              data.status = "new";
              return models.iots_tiketing
                .create(data, { transaction: t })
                .then((_create) => {
                  _create = JSON.parse(JSON.stringify(_create));
                  let image_url = [];
                  data.images.map((image) => {
                    image_url.push({
                      tiketing_id: _create.id,
                      image_url: image,
                    });
                  });
                  return models.iots_tiketing_attachment
                    .bulkCreate(image_url, {
                      transaction: t,
                    })
                    .then(() => {
                      return _create;
                    });
                });
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            message: "Successfully Add Tiketing",
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },
  SvcUpdateTiketing: (data, area) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_teknisi
            .findOne({
              where: { id: data.technician_id },
              attributes: ["fcm_token", "email"],
            })
            .then((teknisi) => {
              return models.iots_tiketing
                .findOne({
                  where: { id: data.id },
                  attributes: [
                    "user_name",
                    "type_user",
                    "ticket_type",
                    "area_id",
                    "ticket_type",
                    "technician_name",
                    "ticket_delegation_date",
                    "no_tiket",
                    "id",
                    "area_id",
                    "technician_name",
                  ],
                })
                .then((tiket) => {
                  if (teknisi.fcm_token !== null) {
                    models.iots_fcm.create({
                      areaId: tiket.area_id,
                      user_id: data.technician_id,
                      user_type: "TEKNISI",
                      title: `Notifikasi Tiket ${tiket.ticket_type}`,
                      body: `You Get New Tiket From ${tiket.type_user} ${tiket.user_name}`,
                    });
                    const notification_options = {
                      priority: "high",
                      timeToLive: 60 * 60 * 24,
                    };
                    const registrationToken = teknisi.fcm_token;
                    const message = {
                      notification: {
                        title: `Notifikasi Tiket ${tiket.ticket_type}`,
                        body: `You Get New Tiket From ${tiket.type_user} ${tiket.user_name}`,
                      },
                    };
                    const options = notification_options;
                    admin
                      .messaging()
                      .sendToDevice(registrationToken, message, options)
                      .then((response) => console.log(response.results))
                      .catch((error) => console.log(error));
                  }
                  return models.iots_tiketing
                    .update(data, {
                      where: { id: data.id },
                    })
                    .then(async () => {
                      //edit sini
                      const {
                        notif_tiket2,
                      } = require("../../emailPayload/notif_tiket2");
                      const { transporter } = require("../../config/email");
                      let email_body = await notif_tiket2({
                        area: area.area_name,
                        nama_pelanggan: tiket.user_name,
                        no_tiket: tiket.no_tiket,
                      });
                      var mailOptions = {
                        from: process.env.EMAIL_USER,
                        to: teknisi.email,
                        subject: "Notification Tiket",
                        html: email_body,
                        headers: {
                          "x-priority": "1",
                          "x-msmail-priority": "High",
                          importance: "high",
                        },
                      };
                      transporter.sendMail(mailOptions);

                      let histo = {
                        ticket_type: tiket.ticket_type,
                        create_by:
                          "User Area= " +
                          area.area_name +
                          " username= " +
                          area.username,
                        delegation_to: tiket.technician_name,
                        ticket_delegation_date: moment(
                          tiket.ticket_delegation_date
                        ).format(),
                        no_tiket: tiket.no_tiket,
                        tiket_id: tiket.id,
                        area_id: tiket.area_id,
                        action:
                          "delegation to teknisi " + tiket.technician_name,
                      };
                      return models.iots_history_tiket.create(histo, {
                        transaction: t,
                      });
                    });
                });
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            message: "Successfully Get Tiketing",
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },
  SvcReadTeknisiTiket: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_fcm.update(
            { is_read: true },
            { where: data, transaction: t }
          );
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            message: "Successfully Get Tiketing",
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },
  SvcGetTeknisiTiketCompany: (data, user_id, page, size) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_fcm
            .count({
              where: [data, { areaId: 0 }],
              order: [["id", "DESC"]],
              transaction: t,
            })
            .then((count) => {
              count = JSON.parse(JSON.stringify(count));
              return models.iots_fcm
                .findAll({
                  limit: size,
                  offset: (page - 1) * size,
                  where: [data, { areaId: 0 }],
                  order: [["id", "DESC"]],
                  transaction: t,
                })
                .then((result) => {
                  result = JSON.parse(JSON.stringify(result));
                  return { result: result, count: count };
                });
            });
        })
        .then((result) => {
          resolve({
            page: page,
            size: size,
            total: result.count,
            responseCode: 200,
            message: "Successfully Get Tiketing",
            response: result.result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },
  SvcGetTeknisiTiket: (data, user_id, area_id, page, size) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_fcm
            .count({
              where: [data, { [Op.or]: { user_id: user_id, areaId: area_id } }],
              order: [["id", "DESC"]],
              transaction: t,
            })
            .then((count) => {
              count = JSON.parse(JSON.stringify(count));
              return models.iots_fcm
                .findAll({
                  limit: size,
                  offset: (page - 1) * size,
                  where: [
                    data,
                    { [Op.or]: { user_id: user_id, areaId: area_id } },
                  ],
                  order: [["id", "DESC"]],
                  transaction: t,
                })
                .then((result) => {
                  result = JSON.parse(JSON.stringify(result));
                  return { result: result, count: count };
                });
            });
        })
        .then((result) => {
          resolve({
            page: page,
            size: size,
            total: result.count,
            responseCode: 200,
            message: "Successfully Get Tiketing",
            response: result.result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },
  SvcDeleterTiketing: (data, tiket, area) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_tiketing
            .findOne({
              where: { id: data.id },
              attributes: [
                "user_name",
                "type_user",
                "ticket_type",
                "area_id",
                "ticket_type",
                "technician_name",
                "ticket_delegation_date",
                "no_tiket",
                "id",
                "area_id",
                "technician_name",
              ],
            })
            .then((tiket) => {
              let histo = {
                ticket_type: tiket.ticket_type,
                create_by:
                  "User Area= " +
                  area.area_name +
                  " username= " +
                  area.username,
                delegation_to: tiket.technician_name,
                ticket_delegation_date: moment(
                  tiket.ticket_delegation_date
                ).format(),
                no_tiket: tiket.no_tiket,
                tiket_id: tiket.id,
                area_id: tiket.area_id,
                action: "delete",
              };
              return models.iots_history_tiket
                .create(histo, {
                  transaction: t,
                })
                .then(() => {
                  return models.iots_tiketing
                    .destroy({
                      where: { id: data.id },
                      transaction: t,
                    })
                    .then(() => {
                      return models.iots_tiketing_attachment.destroy({
                        where: { tiketing_id: data.id },
                        transaction: t,
                      });
                    });
                });
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            message: "Successfully Delete Tiketing",
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },
  SvcAddHistoryTiket: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_history_tiket.create(data, { transaction: t });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            message: "Successfully Add Tiketing",
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },
  SvcGetTiketingHistory: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_history_tiket.findAll({
            where: data,
            order: [["createdAt", "DESC"]],
          });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            message: "Successfully Get Tiketing",
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },
  SvcAddTiketing: (data, data_email = {}, flag = false) => {
    return new Promise(async (resolve, reject) => {
      try {
        transaction = await models.sequelize.transaction();
        let create_tiket = await models.iots_tiketing.create(data, {
          transaction,
        });
        create_tiket = JSON.parse(JSON.stringify(create_tiket));
        let no =
          moment().utcOffset("+0700").format("YYYY-MM") +
          "-" +
          data.no_tiket +
          "-" +
          create_tiket.id;
        create_tiket.no_tiket = no;
        data_email.no_tiket = no;

        await models.iots_tiketing.update(
          {
            no_tiket: no,
          },
          {
            where: { id: create_tiket.id },
            transaction,
          }
        );
        let image_url = [];
        data.images.map((image) => {
          image_url.push({
            tiketing_id: create_tiket.id,
            image_url: image,
          });
        });
        await models.iots_tiketing_attachment.bulkCreate(image_url, {
          transaction,
        });
        await transaction.commit();
        if (flag) {
          const { notif_tiket } = require("../../emailPayload/notif_tiket");
          const { transporter } = require("../../config/email");
          let email_body = await notif_tiket(data_email);
          var mailOptions = {
            from: process.env.EMAIL_USER,
            to: data_email.to,
            subject: "Notification Tiket",
            html: email_body,
            headers: {
              "x-priority": "1",
              "x-msmail-priority": "High",
              importance: "high",
            },
          };
          let email = await transporter.sendMail(mailOptions);
          console.log(email);
        }
        resolve({
          responseCode: 200,
          message: "Successfully Add Tiketing",
          response: create_tiket,
        });
      } catch (error) {
        console.log(error);
        if (transaction) await transaction.rollback();
        reject({
          responseCode: 400,
          response: error.message,
        });
      }
    });
  },
  SvcTiketingUnsigned: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        transaction = await models.sequelize.transaction();
        let input = await models.iots_tiketing.create(data, { transaction });
        let area = await models.iot_area.findOne({
          where: { id: data.area_id },
          attributes: ["area_name"],
          transaction,
        });
        let no =
          moment().format("YYYY-MM") + "-" + area.area_name + "-" + input.id;
        await models.iots_tiketing.update(
          { no_tiket: no },
          {
            where: { id: input.id },
            transaction,
          }
        );
        await transaction.commit();
        resolve({
          responseCode: 200,
          message: "Sukses Create Tiket",
          data: input,
        });
      } catch (error) {
        console.log(error);
        if (transaction) await transaction.rollback();
        reject({ responseCode: 500, message: error.message });
      }
      return models.sequelize
        .transaction((t) => {
          return models.iots_fcm.update(
            { is_read: true },
            { where: data, transaction: t }
          );
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            message: "Successfully Get Tiketing",
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },
};

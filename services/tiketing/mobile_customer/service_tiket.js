const models = require("../../../models/index");
const { Op } = models.Sequelize;
const moment = require("moment");

module.exports = {
  SvcGetArea: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_area.findOne({
            where: data,
          });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            message: "Successfully Get Tiketing",
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },
};

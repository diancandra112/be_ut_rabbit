const models = require("../models/index");
const jwt = require("jsonwebtoken");
require("dotenv").config();

module.exports = {
  serviceAreaPasswordEdit: (data, data_where) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_users.update(data, {
            where: { id: data_where.user_id },
          });
        })
        .then((res) => {
          resolve({
            responseCode: 200,
            messages: "Berhasil Edit Company Password",
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: "Gagal Edit Company Profile",
            error: error,
          });
        });
    });
  },
  serviceCompanyPasswordEdit: (data, data_where) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_users.update(data, {
            where: { id: data_where.user_id },
          });
        })
        .then((res) => {
          resolve({
            responseCode: 200,
            messages: "Berhasil Edit Company Password",
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: "Gagal Edit Company Profile",
            error: error,
          });
        });
    });
  },
  serviceCompanyProfileEdit: (data) => {
    console.log(data);
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_companies
            .update(data, {
              where: { user_id: data.user_id },
              transaction: t,
            })
            .then(() => {
              return models.iot_companies
                .findOne({
                  where: { user_id: data.user_id },
                  attributes: ["username", "company_name"],
                  include: [
                    {
                      model: models.iot_area,
                      as: "list_area",
                      attributes: ["username", "area_name"],
                      include: [
                        {
                          model: models.iot_tenant,
                          as: "list_tenant",
                          attributes: ["id", "tenant_name"],
                        },
                      ],
                    },
                  ],
                })
                .then((result) => {
                  result = JSON.parse(JSON.stringify(result));
                  // result.list_tenant;
                  result.list_area.map((val, idx) => {
                    val.list_tenant.map((val2, idx2) => {
                      return models.iot_tenant.update(
                        {
                          member_id: `${data.username}.${val.username}.${
                            idx2 + 1
                          }`,
                        },
                        {
                          where: { id: val2.id },
                        }
                      );
                    });
                  });
                });
            });
        })
        .then((res) => {
          // res = JSON.parse(JSON.stringify(res));
          resolve({
            responseCode: 200,
            messages: "Berhasil Edit Company Profile",
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: "Gagal Edit Company Profile",
            error: error,
          });
        });
    });
  },
  serviceCompanyProfile: (data) => {
    return new Promise((resolve, reject) => {
      models.iot_companies
        .findOne({
          where: data,
        })
        .then((res) => {
          res = JSON.parse(JSON.stringify(res));
          resolve({
            responseCode: 200,
            list_company: res,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error,
          });
        });
    });
  },
  serviceCompaniesRegister: (dataUser, dataProfile) => {
    return new Promise((resolve, reject) => {
      console.log(dataUser, " ", dataProfile);
      return models.sequelize
        .transaction((t) => {
          return models.iot_users
            .findOrCreate({
              where: {
                email: dataUser.email,
              },
              defaults: dataUser,
              transaction: t,
            })
            .then(([res, status]) => {
              resolve(status);
              res = JSON.parse(JSON.stringify(res));
              if (status == false) {
                throw new Error();
              }
              dataProfile.user_id = res.id;
              return models.iot_companies.create(dataProfile);
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: "Berhasil terdaftar",
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: "Email telah digunakan",
          });
        });
    });
  },
};

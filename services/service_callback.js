const models = require("../models/index");
const axios = require("axios");
require("dotenv").config();
const { Op } = models.Sequelize;
const { weiots } = require("../config/axios");
const moment = require("moment");
module.exports = {
  serviceFasapayCallback: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_billing_history
            .update(
              {
                status: data.status,
                payment_method: data.payment_method,
                payment_date: data.payment_date,
              },
              {
                where: {
                  virtual_account: { [Op.substring]: data.invoice },
                  status: { [Op.not]: "PAID" },
                },
                transaction: t,
              }
            )
            .then((nonVc) => {
              nonVc = JSON.parse(JSON.stringify(nonVc));
              console.log(nonVc, "nonvc", nonVc == false);
              if (nonVc == false) {
                return models.iots_billing_prepaid
                  .findOne({
                    where: {
                      [Op.or]: {
                        va_bca: data.invoice,
                        va_permata: data.invoice,
                      },
                    },
                    include: [{ model: models.iot_nodes }],
                  })
                  .then((res_find) => {
                    res_find = JSON.parse(JSON.stringify(res_find));
                    let data_log = {
                      time: moment().utcOffset("+0700").format(),
                      action:
                        "topup " +
                        res_find.product_value +
                        " " +
                        res_find.satuan_vc,
                      deveui: res_find.device_deveui,
                      email: "faspay",
                    };
                    let nominal =
                      parseFloat(res_find.product_value) * 1000 +
                      parseFloat(res_find.iot_node.prepayment);
                    let data_down = `/action/topup/${res_find.device_deveui}/${nominal}`;
                    console.log(data_down);
                    weiots({
                      method: "post",
                      url: data_down,
                    });
                    models.iots_log_downlink.create(data_log);
                    models.iot_nodes.update(
                      {
                        prepayment: nominal.toString(),
                        live_prepayment_date: moment()
                          .utcOffset("+0700")
                          .format(),
                      },
                      { where: { devEui: res_find.device_deveui } }
                    );

                    return models.iots_billing_prepaid.update(
                      {
                        status: "PAID",
                        payment_date: moment().utcOffset("+0700").format(),
                      },
                      {
                        where: {
                          [Op.or]: {
                            va_bca: data.invoice,
                            va_permata: data.invoice,
                          },
                        },
                      }
                    );
                  });
              } else {
                return nonVc;
              }
            });
        })
        .then((result) => {
          console.log(result);
          resolve({
            responseCode: 200,
            messages: "Berhasil rubah status",
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error.message,
          });
        });
    });
  },
};

const models = require("../models/index");
const axios = require("axios");
const { Op } = require("sequelize");
const { sequelize } = require("../models/index");
const moment = require("moment");
require("dotenv").config();

module.exports = {
  serviceNodeSumUsage: (
    device_id,
    start_date,
    end_date,
    arr_date,
    arr_date2,
    is_mobile = false
  ) => {
    return new Promise(async (resolve, reject) => {
      try {
        console.log(device_id, start_date, end_date, arr_date, arr_date2);

        let usage = await models.iots_charts_usage.findAll({
          where: {
            nodeId: device_id,
            [Op.and]: [
              { fdate: { [Op.gte]: start_date } },
              { fdate: { [Op.lte]: end_date } },
            ],
          },
        });
        let out = [];
        usage = JSON.parse(JSON.stringify(usage));
        arr_date.map((val, idx) => {
          let temp = usage.filter(
            (valp) =>
              moment(val).format("YYYY-MM-DD") ==
              moment(valp.fdate).format("YYYY-MM-DD")
          );
          let usg = 0;
          let meter = 0;
          let last_update = null;
          temp.map((valT) => {
            usg += valT.usage;
            meter = valT.endMeter;
            last_update = valT.fdate;
          });
          if (is_mobile) {
            meter = parseFloat((meter / 1000).toFixed(3));
            usg = parseFloat((usg / 1000).toFixed(3));
          }
          if (last_update != null) {
            out.push({
              last_update: last_update,
              totalizer: meter,
              usage: usg,
            });
          }
        });
        resolve({ responseCode: 200, data: out });
        // console.log(usage);
      } catch (error) {
        console.log(error);
        reject({ responseCode: 500, message: error.message });
      }
    });
  },
};

const models = require("../models/index");
const axios = require("axios");
const { Op } = require("sequelize");
const { sequelize } = require("../models/index");
const moment = require("moment");
require("dotenv").config();

module.exports = {
  serviceType20: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        let page = 0;
        let size = 0;

        let node_count = await models.iot_histori_20.count({
          where: {
            device_id: data.device_id,
            date_start: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iot_histori_20.reportTimeFlag")
              ),
              ">=",
              data.date_start
            ),
            date_end: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iot_histori_20.reportTimeFlag")
              ),
              "<",
              data.date_end
            ),
          },
        });

        node_count = JSON.parse(JSON.stringify(node_count));

        if (isNaN(data.page)) {
          page = 1;
          size = node_count;
        } else {
          page = data.page;
          size = data.size;
        }
        page = parseInt(page);
        size = parseInt(size);
        let node = await models.iot_histori_20.findAll({
          limit: size,
          offset: (page - 1) * size,
          order: [["reportTime", "DESC"]],
          where: {
            device_id: data.device_id,
            date_start: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iot_histori_20.reportTimeFlag")
              ),
              ">=",
              data.date_start
            ),
            date_end: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iot_histori_20.reportTimeFlag")
              ),
              "<",
              data.date_end
            ),
          },
        });

        return resolve({
          page: page,
          size: size,
          count: node_count,
          rows: node,
          responseCode: 200,
          messages: "Berhasil Get Data",
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          messages: error.message,
        });
      }
    });
  },
  serviceType21: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        let page = 0;
        let size = 0;

        let node_count = await models.iot_histori_21.count({
          where: {
            device_id: data.device_id,
            date_start: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iot_histori_21.reportTimeFlag")
              ),
              ">=",
              data.date_start
            ),
            date_end: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iot_histori_21.reportTimeFlag")
              ),
              "<",
              data.date_end
            ),
          },
        });

        node_count = JSON.parse(JSON.stringify(node_count));

        if (isNaN(data.page)) {
          page = 1;
          size = node_count;
        } else {
          page = data.page;
          size = data.size;
        }
        page = parseInt(page);
        size = parseInt(size);
        let node = await models.iot_histori_21.findAll({
          limit: size,
          offset: (page - 1) * size,
          order: [["reportTime", "DESC"]],
          where: {
            device_id: data.device_id,
            date_start: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iot_histori_21.reportTimeFlag")
              ),
              ">=",
              data.date_start
            ),
            date_end: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iot_histori_21.reportTimeFlag")
              ),
              "<",
              data.date_end
            ),
          },
        });

        return resolve({
          page: page,
          size: size,
          count: node_count,
          rows: node,
          responseCode: 200,
          messages: "Berhasil Get Data",
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          messages: error.message,
        });
      }
    });
  },
  serviceType22: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        let page = 0;
        let size = 0;

        let node_count = await models.iot_histori_22.count({
          where: {
            device_id: data.device_id,
            date_start: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iot_histori_22.reportTimeFlag")
              ),
              ">=",
              data.date_start
            ),
            date_end: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iot_histori_22.reportTimeFlag")
              ),
              "<",
              data.date_end
            ),
          },
        });

        node_count = JSON.parse(JSON.stringify(node_count));

        if (isNaN(data.page)) {
          page = 1;
          size = node_count;
        } else {
          page = data.page;
          size = data.size;
        }
        page = parseInt(page);
        size = parseInt(size);
        let node = await models.iot_histori_22.findAll({
          limit: size,
          offset: (page - 1) * size,
          order: [["reportTime", "DESC"]],
          where: {
            device_id: data.device_id,
            date_start: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iot_histori_22.reportTimeFlag")
              ),
              ">=",
              data.date_start
            ),
            date_end: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iot_histori_22.reportTimeFlag")
              ),
              "<",
              data.date_end
            ),
          },
        });

        return resolve({
          page: page,
          size: size,
          count: node_count,
          rows: node,
          responseCode: 200,
          messages: "Berhasil Get Data",
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          messages: error.message,
        });
      }
    });
  },
  serviceType1: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        let page = 0;
        let size = 0;
        let nodes = await models.iot_nodes.findOne({
          where: { id: data.device_id },
          include: [
            { model: models.iot_device_type },
            { model: models.iot_node_type },
            { model: models.iot_detail_gas, as: "iot_detail_gas" },
          ],
        });
        let node_count = await models.iots_history_gas.count({
          where: {
            device_id: data.device_id,
            date_start: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_history_gas.reportTimeFlag")
              ),
              ">=",
              data.date_start
            ),
            date_end: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_history_gas.reportTimeFlag")
              ),
              "<",
              data.date_end
            ),
          },
        });

        node_count = JSON.parse(JSON.stringify(node_count));
        data.page = parseInt(data.page);
        data.size = parseInt(data.size);
        if (isNaN(data.page)) {
          page = 1;
          size = node_count;
        } else {
          page = data.page;
          size = data.size;
        }
        page = parseInt(page);
        size = parseInt(size);
        let node = await models.iots_history_gas.findAll({
          limit: size,
          offset: (page - 1) * size,
          order: [["reportTime", "DESC"]],
          where: {
            device_id: data.device_id,
            date_start: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_history_gas.reportTimeFlag")
              ),
              ">=",
              data.date_start
            ),
            date_end: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_history_gas.reportTimeFlag")
              ),
              "<",
              data.date_end
            ),
          },
        });
        node = JSON.parse(JSON.stringify(node));
        nodes = JSON.parse(JSON.stringify(nodes));
        // return console.log(node[node.length - 1]);
        node.map((val, idx) => {
          if (node.length - 1 != idx) {
            val.usage = parseFloat(
              (val.meterReading - node[idx + 1].meterReading).toFixed(3)
            );
            val.usage = val.usage / 1000;
            val.payload = {
              devEui: val.devEui,
              update_time: val.reportTime,
              totalizer: val.meterReading,
              valve: val.valve,
              balance: 0,
              usage: val.usage,
              battrey_level:
                val.battrey_level == null
                  ? "99%"
                  : val.battrey_level.toString(),
            };
          } else {
            val.usage = 0;
            val.payload = {
              devEui: val.devEui,
              update_time: val.reportTime,
              totalizer: val.meterReading,
              valve: val.valve,
              balance: 0,
              usage: 0,
              battrey_level:
                val.battrey_level == null
                  ? "99%"
                  : val.battrey_level.toString(),
            };
          }
          val.balance = 0;

          val.iot_node = nodes;
        });

        if (node.length > 0) {
          let node2 = await models.iots_history_gas.findOne({
            order: [["reportTime", "desc"]],
            where: {
              device_id: data.device_id,
              id: { [Op.lt]: node[node.length - 1].id },
            },
          });
          if (node2 != null) {
            node[node.length - 1].usage = parseFloat(
              (node[node.length - 1].meterReading - node2.meterReading).toFixed(
                3
              )
            );
            node[node.length - 1].usage = node[node.length - 1].usage / 1000;
          }
        }
        return resolve({
          page: page,
          size: size,
          count: node_count,
          rows: node,
          responseCode: 200,
          messages: "Berhasil Get Data",
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          messages: error.message,
        });
      }
    });
  },
  serviceType2: (data, is_mobile = false) => {
    return new Promise(async (resolve, reject) => {
      try {
        let page = 0;
        let size = 0;
        let nodes = await models.iot_nodes.findOne({
          where: { id: data.device_id },
          include: [
            { model: models.iot_device_type },
            { model: models.iot_node_type },
            { model: models.iot_detail_water },
          ],
        });
        let node_count = await models.iots_history_water.count({
          where: {
            device_id: data.device_id,
            date_start: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_history_water.reporttimeflag")
              ),
              ">=",
              data.date_start
            ),
            date_end: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_history_water.reporttimeflag")
              ),
              "<",
              data.date_end
            ),
          },
        });

        node_count = JSON.parse(JSON.stringify(node_count));
        data.page = parseInt(data.page);
        data.size = parseInt(data.size);
        if (isNaN(data.page)) {
          page = 1;
          size = node_count;
        } else {
          page = data.page;
          size = data.size;
        }
        page = parseInt(page);
        size = parseInt(size);
        let node = await models.iots_history_water.findAll({
          attributes: [
            "id",
            "device_id",
            "meterreading",
            ["reporttime", "reportTime"],
            ["reporttimeflag", "reportTimeFlag"],
            "tenantid",
            "internalid",
            "deveui",
            "meter_id",
            "valve",
            "battrey_level",
            "max_totalizer",
          ],
          limit: size,
          offset: (page - 1) * size,
          order: [["reportTime", "DESC"]],
          where: {
            device_id: data.device_id,
            date_start: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_history_water.reporttimeflag")
              ),
              ">=",
              data.date_start
            ),
            date_end: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_history_water.reporttimeflag")
              ),
              "<",
              data.date_end
            ),
          },
        });
        node = JSON.parse(JSON.stringify(node));
        nodes = JSON.parse(JSON.stringify(nodes));
        // return console.log(node[node.length - 1]);
        node.map((val, idx) => {
          if (node.length - 1 != idx) {
            if (is_mobile) {
              if (
                parseFloat(node[idx + 1].meterreading) ==
                parseFloat(node[idx + 1].max_totalizer)
              ) {
                val.usage = parseFloat(
                  ((val.meterreading - 0) / 1000).toFixed(3)
                );
              } else {
                val.usage = parseFloat(
                  (
                    (val.meterreading - node[idx + 1].meterreading) /
                    1000
                  ).toFixed(3)
                );
              }
            } else {
              if (
                parseFloat(node[idx + 1].meterreading) ==
                parseFloat(node[idx + 1].max_totalizer)
              ) {
                val.usage = parseFloat((val.meterreading - 0) / 1000).toFixed(
                  3
                );
              } else {
                val.usage = parseFloat(
                  (val.meterreading - node[idx + 1].meterreading) / 1000
                ).toFixed(3);
              }
            }
            val.usage = val.usage;
            val.payload = {
              devEui: val.deveui,
              update_time: val.reporttime,
              totalizer: val.meterreading,
              valve: val.valve,
              balance: 0,
              usage: val.usage,
              battrey_level:
                val.battrey_level == null
                  ? "99%"
                  : val.battrey_level.toString(),
            };
          } else {
            val.usage = 0;
            val.payload = {
              devEui: val.deveui,
              update_time: val.reporttime,
              totalizer: val.meterreading,
              valve: val.valve,
              balance: 0,
              usage: 0,
              battrey_level:
                val.battrey_level == null
                  ? "99%"
                  : val.battrey_level.toString(),
            };
          }
          val.balance = 0;

          val.iot_node = nodes;
        });

        if (node.length > 0) {
          let node2 = await models.iots_history_water.findOne({
            order: [["reportTime", "desc"]],
            where: {
              device_id: data.device_id,
              id: { [Op.lt]: node[node.length - 1].id },
            },
          });
          if (node2 != null) {
            if (is_mobile) {
              if (node2.meterreading == node2.max_totalizer) {
                node[node.length - 1].usage = parseFloat(
                  ((node[node.length - 1].meterreading - 0) / 1000).toFixed(3)
                );
              } else {
                node[node.length - 1].usage = parseFloat(
                  (
                    (node[node.length - 1].meterreading - node2.meterreading) /
                    1000
                  ).toFixed(3)
                );
              }
            } else {
              if (node2.meterreading == node2.max_totalizer) {
                node[node.length - 1].usage = parseFloat(
                  (node[node.length - 1].meterreading - 0) / 1000
                ).toFixed(3);
              } else {
                node[node.length - 1].usage = parseFloat(
                  (node[node.length - 1].meterreading - node2.meterreading) /
                    1000
                ).toFixed(3);
              }
            }
            node[node.length - 1].payload.usage = node[node.length - 1].usage;
          }
        }
        return resolve({
          page: page,
          size: size,
          count: node_count,
          rows: node,
          responseCode: 200,
          messages: "Berhasil Get Data",
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          messages: error.message,
        });
      }
    });
  },
  serviceType3: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        let page = 0;
        let size = 0;
        let nodes = await models.iot_nodes.findOne({
          where: { id: data.device_id },
          include: [
            { model: models.iot_device_type },
            { model: models.iot_node_type },
            { model: models.iot_detail_water },
          ],
        });
        let node_count = await models.iots_history_3.count({
          where: {
            device_id: data.device_id,
            date_start: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_history_3.reporttimeflag")
              ),
              ">=",
              data.date_start
            ),
            date_end: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_history_3.reporttimeflag")
              ),
              "<",
              data.date_end
            ),
          },
        });

        node_count = JSON.parse(JSON.stringify(node_count));
        data.page = parseInt(data.page);
        data.size = parseInt(data.size);
        if (isNaN(data.page)) {
          page = 1;
          size = node_count;
        } else {
          page = data.page;
          size = data.size;
        }
        page = parseInt(page);
        size = parseInt(size);
        let node = await models.iots_history_3.findAll({
          limit: size,
          offset: (page - 1) * size,
          order: [["reportTime", "DESC"]],
          where: {
            device_id: data.device_id,
            date_start: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_history_3.reporttimeflag")
              ),
              ">=",
              data.date_start
            ),
            date_end: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_history_3.reporttimeflag")
              ),
              "<",
              data.date_end
            ),
          },
        });
        node = JSON.parse(JSON.stringify(node));
        nodes = JSON.parse(JSON.stringify(nodes));
        // return console.log(node[node.length - 1]);
        node.map((val, idx) => {
          if (node.length - 1 != idx) {
            val.usage = parseFloat(
              ((val.meterreading - node[idx + 1].meterreading) / 1000).toFixed(
                3
              )
            );
            val.usage = val.usage;
            val.payload = {
              devEui: val.deveui,
              update_time: val.reporttime,
              totalizer: val.meterreading,
              valve: val.valve,
              balance: 0,
              usage: val.usage,
              battrey_level:
                val.battrey_level == null
                  ? "99%"
                  : val.battrey_level.toString(),
            };
          } else {
            val.usage = 0;
            val.payload = {
              devEui: val.deveui,
              update_time: val.reporttime,
              totalizer: val.meterreading,
              valve: val.valve,
              balance: 0,
              usage: 0,
              battrey_level:
                val.battrey_level == null
                  ? "99%"
                  : val.battrey_level.toString(),
            };
          }
          val.balance = 0;

          val.iot_node = nodes;
        });

        if (node.length > 0) {
          let node2 = await models.iots_history_3.findOne({
            order: [["reportTime", "desc"]],
            where: {
              device_id: data.device_id,
              id: { [Op.lt]: node[node.length - 1].id },
            },
          });
          if (node2 != null) {
            node[node.length - 1].usage = parseFloat(
              (
                (node[node.length - 1].meterreading - node2.meterreading) /
                1000
              ).toFixed(3)
            );
            node[node.length - 1].usage = node[node.length - 1].usage;
          }
        }
        return resolve({
          page: page,
          size: size,
          count: node_count,
          rows: node,
          responseCode: 200,
          messages: "Berhasil Get Data",
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          messages: error.message,
        });
      }
    });
  },

  serviceType11: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        let page = 0;
        let size = 0;
        let nodes = await models.iot_nodes.findOne({
          where: { id: data.device_id },
          include: [
            { model: models.iot_device_type },
            { model: models.iot_node_type },
            { model: models.iot_detail_preassure },
          ],
        });
        let node_count = await models.iot_histori_11.count({
          where: {
            device_id: data.device_id,
            date_start: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iot_histori_11.reporttimeflag")
              ),
              ">=",
              data.date_start
            ),
            date_end: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iot_histori_11.reporttimeflag")
              ),
              "<",
              data.date_end
            ),
          },
        });

        node_count = JSON.parse(JSON.stringify(node_count));
        data.page = parseInt(data.page);
        data.size = parseInt(data.size);
        if (isNaN(data.page)) {
          page = 1;
          size = node_count;
        } else {
          page = data.page;
          size = data.size;
        }
        page = parseInt(page);
        size = parseInt(size);
        let node = await models.iot_histori_11.findAll({
          include: [
            {
              model: models.iot_histori_11_detail,
              attributes: ["device_id", "log_id", "name", "value"],
            },
          ],
          limit: size,
          offset: (page - 1) * size,
          order: [["reportTime", "DESC"]],
          where: {
            device_id: data.device_id,
            date_start: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iot_histori_11.reporttimeflag")
              ),
              ">=",
              data.date_start
            ),
            date_end: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iot_histori_11.reporttimeflag")
              ),
              "<",
              data.date_end
            ),
          },
        });
        node = JSON.parse(JSON.stringify(node));
        nodes = JSON.parse(JSON.stringify(nodes));
        // return console.log(node[node.length - 1]);
        node.map((val, idx) => {
          val.iot_histori_11_details.map((val2, idx2) => {
            if (val2.name.toLocaleLowerCase().search("pressure") >= 0) {
              val2.fix = val2.value;
              val2.value = parseFloat(val2.value) / 100000;
            }
          });
          val.payload = JSON.stringify({
            devEui: val.deveui,
            update_time: val.reporttime,
            totalizer: val.meterreading,
            valve: val.valve,
            balance: 0,
            usage: val.usage,
            battrey_level:
              val.battrey_level == null ? "99%" : val.battrey_level.toString(),
            meter: { exReading: val.iot_histori_11_details },
          });
          val.balance = 0;

          val.iot_node = nodes;
        });

        return resolve({
          page: page,
          size: size,
          count: node_count,
          rows: node,
          responseCode: 200,
          messages: "Berhasil Get Data",
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          messages: error.message,
        });
      }
    });
  },
};

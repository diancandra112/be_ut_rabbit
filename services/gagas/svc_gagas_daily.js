const models = require("../../models/index");
const moment = require("moment");
const axios = require("axios");

const get_log = (page, areaId, time, url_to_company, company_token) => {
  return new Promise((resolve, reject) => {
    let date = moment(time).utcOffset("+0700").format("YYYY-MM-DD");
    return models.sequelize
      .transaction((t) => {
        return models.iot_nodes
          .findAll({
            limit: 5,
            offset: (parseInt(page) - 1) * 5,
            attributes: ["id", "devEui", "merk"],
            where: { areaId: areaId, typeId: 1 },
            include: [
              {
                model: models.iot_tenant,
                attributes: ["tenant_name", "handphone"],
              },
              {
                model: models.iot_detail_gas,
                attributes: ["meter_id"],
                as: "iot_detail_gas",
              },
              {
                model: models.iot_histories,
                limit: 1,
                order: [["reportTimeFlag", "DESC"]],
                attributes: [
                  "id",
                  "device_id",
                  "reportTime",
                  "payload",
                  "reportTimeFlag",
                ],
                where: {
                  col2: models.sequelize.where(
                    models.sequelize.fn(
                      "date",
                      models.sequelize.col("iot_histories.reportTimeFlag")
                    ),
                    "=",
                    date
                  ),
                },
              },
            ],
          })
          .then((res) => {
            res = JSON.parse(JSON.stringify(res));
            res.map((val) => {
              console.log(val, "device id");
            });
            res = res.filter((value) => {
              return value.iot_histories.length > 0;
            });
            let data_send = [];
            if (res.length == 0) {
              return "done";
            }
            res.map((val) => {
              data_send.push({
                id: val.iot_histories[0].id,
                idrefcustomer: val.iot_tenant.handphone,
                fdate: moment(val.iot_histories[0].reportTime)
                  .utcOffset("+0700")
                  .format("YYYY-MM-DD HH:mm:ss"),
                fyear: moment(val.iot_histories[0].reportTime)
                  .utcOffset("+0700")
                  .format("YYYY"),
                fmonth: moment(val.iot_histories[0].reportTime)
                  .utcOffset("+0700")
                  .format("M"),
                fday: moment(val.iot_histories[0].reportTime)
                  .utcOffset("+0700")
                  .format("D"),
                fhour: moment(val.iot_histories[0].reportTime)
                  .utcOffset("+0700")
                  .format("H"),
                fminute: moment(val.iot_histories[0].reportTime)
                  .utcOffset("+0700")
                  .format("m"),
                fsecond: moment(val.iot_histories[0].reportTime)
                  .utcOffset("+0700")
                  .format("s"),
                fv:
                  JSON.parse(val.iot_histories[0].payload).meter.meterReading /
                  1000,
                fp: 0,
                fvcs: 1,
                // attribute1: JSON.parse(val.iot_histories[0].payload).nodeName,
                attribute1: val.merk,
                attirbute2: val.iot_detail_gas.meter_id,
                reffcreatedby: "WE",
                fvu: 0,
                pinlet: 0,
              });
            });
            return axios.post(url_to_company + "/daily/nonevc", data_send, {
              headers: {
                authorization: "Bearer " + company_token,
              },
            });
          });
      })
      .then((response) => {
        resolve(response);
      })
      .catch((err) => {
        reject(err.message);
      });
  });
};
module.exports = {
  SvcGagasDaily: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_area
            .findOne({
              where: { decode_id: 3 },
              attributes: [
                "id",
                "url_to_company",
                "decode_id",
                "company_token",
              ],
              transaction: t,
            })
            .then((res_area) => {
              res_area = JSON.parse(JSON.stringify(res_area));
              return models.iot_nodes
                .count({
                  where: { areaId: res_area.id, typeId: 1 },
                  transaction: t,
                })
                .then((res_node_count) => {
                  res_node_count = JSON.parse(JSON.stringify(res_node_count));
                  let count = [];
                  let time = moment()
                    .utcOffset("+0700")
                    .subtract(1, "day")
                    .format("YYYY-MM-DD HH:mm:ss");

                  for (
                    let index = 1;
                    index <= Math.ceil(res_node_count / 5);
                    index++
                  ) {
                    count.push({ page: index, areaId: res_area.id });
                  }
                  if (count.length == 0) {
                    return "done";
                  }
                  return count
                    .reduce(
                      (chain, item) =>
                        chain.then(() =>
                          get_log(
                            item.page,
                            item.areaId,
                            time,
                            res_area.url_to_company,
                            res_area.company_token
                          ).then((a) => console.log(a))
                        ),
                      Promise.resolve()
                    )
                    .then((s) => {
                      console.log(s, "s");
                    })
                    .catch((e) => console.log(e));
                  return count;
                });
            });
        })
        .then((res) =>
          resolve({ responseCode: 200, message: "done", res: res })
        )
        .catch((error) =>
          reject({ responseCode: 400, message: error.message })
        );
    });
  },
};

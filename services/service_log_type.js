const models = require("../models/index");
const axios = require("axios");
const { Op } = require("sequelize");
const { sequelize } = require("../models/index");
const moment = require("moment");
require("dotenv").config();

module.exports = {
  serviceType6: (data, pagination, pagination_flag) => {
    return new Promise(async (resolve, reject) => {
      try {
        let page = 0;
        let size = 0;
        let transaction = await models.sequelize.transaction();
        let count = await models.iots_history_6.count({
          where: {
            device_id: data.device_id,
            date_start: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_history_6.reporttimeflag")
              ),
              ">=",
              data.date_start
            ),
            date_end: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_history_6.reporttimeflag")
              ),
              "<",
              data.date_end
            ),
          },
          transaction,
        });
        count = JSON.parse(JSON.stringify(count));
        if (isNaN(data.page)) {
          page = 1;
          size = count;
        } else {
          page = data.page;
          size = data.size;
        }
        page = parseInt(page);
        size = parseInt(size);
        let node = await models.iot_nodes.findOne({
          where: { id: data.device_id },
          attributes: ["id", "sensor_level_offset"],
        });
        node = JSON.parse(JSON.stringify(node));
        let raw = await models.iots_history_6.findAll({
          limit: size,
          offset: (page - 1) * size,
          include: [{ model: models.iots_history_6_ex }],
          where: {
            device_id: data.device_id,
            date_start: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_history_6.reporttimeflag")
              ),
              ">=",
              data.date_start
            ),
            date_end: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_history_6.reporttimeflag")
              ),
              "<",
              data.date_end
            ),
          },
          order: [["reporttimeflag", "DESC"]],
          transaction,
        });
        raw = JSON.parse(JSON.stringify(raw));
        let output = [];
        raw.map((val) => {
          console.log(val);
          let humidity = val.iots_history_6_exes.find(
            (valf) => valf.name.toLowerCase() == "humidity"
          );
          let temperature = val.iots_history_6_exes.find(
            (valf) => valf.name.toLowerCase() == "temperature"
          );
          let battery = val.iots_history_6_exes.find(
            (valf) => valf.name.toLowerCase() == "battery"
          );
          output.push({
            Humidity: humidity ? humidity.value : "0",
            Temperature: temperature ? temperature.value : "0",
            battery: battery ? battery.value : "1",
            devEui: val.deveui,
            update_time: val.reporttime,
          });
        });

        await transaction.commit();
        return resolve({
          page: page,
          size: size,
          count: count,
          rows: output,
          responseCode: 200,
          messages: "Berhasil terdaftar",
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          messages: error.message,
        });
      }
    });
  },
  serviceType13: (data, pagination, pagination_flag) => {
    return new Promise(async (resolve, reject) => {
      try {
        let page = 0;
        let size = 0;
        let transaction = await models.sequelize.transaction();
        let count = await models.iot_histories_rtu_water_level.count({
          where: {
            device_id: data.device_id,
            date_start: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col(
                  "iot_histories_rtu_water_level.reportTimeFlag"
                )
              ),
              ">=",
              data.date_start
            ),
            date_end: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col(
                  "iot_histories_rtu_water_level.reportTimeFlag"
                )
              ),
              "<",
              data.date_end
            ),
          },
          transaction,
        });
        count = JSON.parse(JSON.stringify(count));
        if (isNaN(data.page)) {
          page = 1;
          size = count;
        } else {
          page = data.page;
          size = data.size;
        }
        page = parseInt(page);
        size = parseInt(size);
        let node = await models.iot_nodes.findOne({
          where: { id: data.device_id },
          attributes: ["id", "sensor_level_offset"],
        });
        node = JSON.parse(JSON.stringify(node));
        let raw = await models.iot_histories_rtu_water_level.findAll({
          limit: size,
          offset: (page - 1) * size,
          where: {
            device_id: data.device_id,
            date_start: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col(
                  "iot_histories_rtu_water_level.reportTimeFlag"
                )
              ),
              ">=",
              data.date_start
            ),
            date_end: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col(
                  "iot_histories_rtu_water_level.reportTimeFlag"
                )
              ),
              "<",
              data.date_end
            ),
          },
          order: [["reportTimeFlag", "DESC"]],
          transaction,
        });
        raw = JSON.parse(JSON.stringify(raw));
        let output = [];
        raw.map((val) => {
          output.push({
            devEui: val.devEui,
            update_time: val.reportTime,
            raw: [
              // { name: "Water Level (m)", value: val.water_level },
              { name: "Sensor Level (m)", value: val.sensor_to_botom },
              {
                name: "Water Level (m)",
                value: val.water_surface_to_bottom + node.sensor_level_offset,
              },
              { name: "Alarm High", value: val.alarm_high ? "ON" : "OFF" },
              { name: "Alarm Low", value: val.alarm_low ? "ON" : "OFF" },
            ],
          });
        });
        await transaction.commit();
        return resolve({
          page: page,
          size: size,
          count: count,
          rows: output,
          responseCode: 200,
          messages: "Berhasil terdaftar",
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          messages: error.message,
        });
      }
    });
  },
  serviceType5: (data, pagination, pagination_flag) => {
    return new Promise((resolve, reject) => {
      console.log(data, pagination, pagination_flag);
      let page = 0;
      let size = 0;
      return models.sequelize
        .transaction((t) => {
          return models.iots_history_type_5
            .count({
              where: {
                node_id: data.device_id,
                date_start: models.sequelize.where(
                  models.sequelize.fn(
                    "date",
                    models.sequelize.col("iots_history_type_5.reportTimeFlag")
                  ),
                  ">=",
                  data.date_start
                ),
                date_end: models.sequelize.where(
                  models.sequelize.fn(
                    "date",
                    models.sequelize.col("iots_history_type_5.reportTimeFlag")
                  ),
                  "<=",
                  data.date_end
                ),
              },
              transaction: t,
            })
            .then((count) => {
              count = JSON.parse(JSON.stringify(count));
              if (isNaN(data.page)) {
                page = 1;
                size = count;
              } else {
                page = data.page;
                size = data.size;
              }
              page = parseInt(page);
              size = parseInt(size);
              return models.iots_history_type_5
                .findAll({
                  limit: size,
                  offset: (page - 1) * size,
                  where: {
                    node_id: data.device_id,
                    date_start: models.sequelize.where(
                      models.sequelize.fn(
                        "date",
                        models.sequelize.col(
                          "iots_history_type_5.reportTimeFlag"
                        )
                      ),
                      ">=",
                      data.date_start
                    ),
                    date_end: models.sequelize.where(
                      models.sequelize.fn(
                        "date",
                        models.sequelize.col(
                          "iots_history_type_5.reportTimeFlag"
                        )
                      ),
                      "<=",
                      data.date_end
                    ),
                  },
                  transaction: t,
                })
                .then((raw) => {
                  raw = JSON.parse(JSON.stringify(raw));
                  return { raw: raw, count: count };
                });
            });
        })
        .then((result) => {
          resolve({
            page: page,
            size: size,
            count: result.count,
            rows: result.raw,
            responseCode: 200,
            messages: "Berhasil terdaftar",
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error,
          });
        });
    });
  },
  serviceType16: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        let page = 0;
        let size = 0;

        let node_count = await models.iots_histori_16.count({
          where: {
            device_id: data.device_id,
            date_start: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_histori_16.reportTimeFlag")
              ),
              ">=",
              data.date_start
            ),
            date_end: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_histori_16.reportTimeFlag")
              ),
              "<",
              data.date_end
            ),
          },
        });

        node_count = JSON.parse(JSON.stringify(node_count));

        if (isNaN(data.page)) {
          page = 1;
          size = node_count;
        } else {
          page = data.page;
          size = data.size;
        }
        page = parseInt(page);
        size = parseInt(size);
        let node = await models.iots_histori_16.findAll({
          limit: size,
          offset: (page - 1) * size,
          include: [{ model: models.iots_type_16_detail }],
          where: {
            device_id: data.device_id,
            date_start: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_histori_16.reportTimeFlag")
              ),
              ">=",
              data.date_start
            ),
            date_end: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_histori_16.reportTimeFlag")
              ),
              "<",
              data.date_end
            ),
          },
        });

        return resolve({
          page: page,
          size: size,
          count: node_count,
          rows: node,
          responseCode: 200,
          messages: "Berhasil Get Data",
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          messages: error.message,
        });
      }
    });
  },
  serviceType17: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        let page = 0;
        let size = 0;

        let node_count = await models.iots_histori_17.count({
          where: {
            device_id: data.device_id,
            date_start: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_histori_17.reportTimeFlag")
              ),
              ">=",
              data.date_start
            ),
            date_end: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_histori_17.reportTimeFlag")
              ),
              "<",
              data.date_end
            ),
          },
        });

        node_count = JSON.parse(JSON.stringify(node_count));

        if (isNaN(data.page)) {
          page = 1;
          size = node_count;
        } else {
          page = data.page;
          size = data.size;
        }
        page = parseInt(page);
        size = parseInt(size);
        let node = await models.iots_histori_17.findAll({
          limit: size,
          offset: (page - 1) * size,
          order: [["reportTime", "DESC"]],
          include: [
            {
              model: models.iots_type_17_details,
              attributes: ["name", "value"],
            },
          ],
          where: {
            device_id: data.device_id,
            date_start: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_histori_17.reportTimeFlag")
              ),
              ">=",
              data.date_start
            ),
            date_end: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_histori_17.reportTimeFlag")
              ),
              "<",
              data.date_end
            ),
          },
        });

        return resolve({
          page: page,
          size: size,
          count: node_count,
          rows: node,
          responseCode: 200,
          messages: "Berhasil Get Data",
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          messages: error.message,
        });
      }
    });
  },
  serviceType18: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        let page = 0;
        let size = 0;

        let node_count = await models.iots_histori_18.count({
          where: {
            device_id: data.device_id,
            date_start: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_histori_18.reportTimeFlag")
              ),
              ">=",
              data.date_start
            ),
            date_end: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_histori_18.reportTimeFlag")
              ),
              "<",
              data.date_end
            ),
          },
        });

        node_count = JSON.parse(JSON.stringify(node_count));

        if (isNaN(data.page)) {
          page = 1;
          size = node_count;
        } else {
          page = data.page;
          size = data.size;
        }
        page = parseInt(page);
        size = parseInt(size);
        let node = await models.iots_histori_18.findAll({
          limit: size,
          offset: (page - 1) * size,
          order: [["reportTime", "DESC"]],
          where: {
            device_id: data.device_id,
            date_start: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_histori_18.reportTimeFlag")
              ),
              ">=",
              data.date_start
            ),
            date_end: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_histori_18.reportTimeFlag")
              ),
              "<",
              data.date_end
            ),
          },
        });

        return resolve({
          page: page,
          size: size,
          count: node_count,
          rows: node,
          responseCode: 200,
          messages: "Berhasil Get Data",
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          messages: error.message,
        });
      }
    });
  },
  serviceType19: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        let page = 0;
        let size = 0;

        let node_count = await models.iots_histori_19.count({
          where: {
            device_id: data.device_id,
            date_start: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_histori_19.reportTimeFlag")
              ),
              ">=",
              data.date_start
            ),
            date_end: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_histori_19.reportTimeFlag")
              ),
              "<",
              data.date_end
            ),
          },
        });

        node_count = JSON.parse(JSON.stringify(node_count));

        if (isNaN(data.page)) {
          page = 1;
          size = node_count;
        } else {
          page = data.page;
          size = data.size;
        }
        page = parseInt(page);
        size = parseInt(size);
        let node = await models.iots_histori_19.findAll({
          limit: size,
          offset: (page - 1) * size,
          order: [["reportTime", "DESC"]],
          where: {
            device_id: data.device_id,
            date_start: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_histori_19.reportTimeFlag")
              ),
              ">=",
              data.date_start
            ),
            date_end: models.sequelize.where(
              models.sequelize.fn(
                "date",
                models.sequelize.col("iots_histori_19.reportTimeFlag")
              ),
              "<",
              data.date_end
            ),
          },
        });

        return resolve({
          page: page,
          size: size,
          count: node_count,
          rows: node,
          responseCode: 200,
          messages: "Berhasil Get Data",
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          messages: error.message,
        });
      }
    });
  },
};

const models = require("../models/index");
const jwt = require("jsonwebtoken");
require("dotenv").config();

module.exports = {
  serviceTenantEdit: (data) => {
    return new Promise((resolve, reject) => {
      if (data.id == undefined) {
        reject({
          responseCode: 400,
          messages: "id harus di declarasikan",
        });
        throw new Error();
      }
      return models.sequelize
        .transaction((t) => {
          return models.iot_tenant
            .findOne({
              where: { id: data.id },
              transaction: t,
            })
            .then((tenant_res) => {
              return models.iot_tenant
                .update(data, {
                  where: { id: data.id },
                  transaction: t,
                })
                .then(() => {
                  return models.allocation_type
                    .destroy({
                      where: { tenant_id: data.tenant_id },
                      transaction: t,
                    })
                    .then((result) => {
                      data.nodeType.map((val, index) => {
                        data.nodeType[index].tenant_id = data.tenant_id;
                      });
                      return models.allocation_type.bulkCreate(data.nodeType, {
                        transaction: t,
                      });
                    });
                });
            });
        })

        .then((res) => {
          console.log(res);
          resolve({
            responseCode: 200,
            messages: "Berhasil Update tenent",
            payload: res,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 500,
            messages: "internal server error",
            payload: error.message,
          });
        });
      // models.iot_tenant
      //   .update(data, {
      //     where: { id: data.id }
      //   })
      //   .then(result => {
      //     models.iot_tenant
      //       .destroy({ where: { tenant_id: data.tenant_id } })
      //       .then(result => {
      //         data.nodeType.map((val, index) => {
      //           data.nodeType[index].tenant_id = data.tenant_id;
      //         });
      //         models.iot_tenant.bulkCreate(data.nodeType).then(result => {
      //           resolve({
      //             responseCode: 200,
      //             messages: "Berhasil Update Tenant"
      //           });
      //         });
      //       });
      //   })
      //   .catch(error => {
      //     console.log(error);
      //     reject({
      //       responseCode: 500,
      //       messages: "internal server error",
      //       payload: error
      //     });
      //   });
    });
  },
  serviceTenantGet: (data) => {
    return new Promise((resolve, reject) => {
      models.iot_tenant
        .findAll({
          where: [data, { id_deleted: false }],
          // attributes:{ exclude:['user_id','createdAt','updatedAt']},
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            list_tenant: JSON.parse(JSON.stringify(result)),
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 500,
            messages: "internal server error",
            payload: error,
          });
        });
    });
  },
  serviceTenantRegister: (dataUser, dataProfile, nodeType) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_tenant
            .findOrCreate({
              where: {
                username: dataProfile.username,
                area_id: dataProfile.area_id,
              },
              defaults: dataProfile,
              transaction: t,
            })
            .then(([res, status]) => {
              if (status == false) {
                throw new Error("duplicate username");
              } else {
                nodeType.map((val, index) => {
                  nodeType[index].tenant_id = res.id;
                });
                return models.allocation_type
                  .bulkCreate(nodeType, { transaction: t })
                  .then(() => {
                    return models.iot_users
                      .create(dataUser, { transaction: t })
                      .then((user) => {
                        return models.iot_area
                          .findOne({
                            where: { id: dataProfile.area_id },
                            attributes: ["area_name", "username"],
                            transaction: t,
                            include: [
                              {
                                model: models.iot_companies,
                                as: "list_area",
                                attributes: ["username"],
                              },
                            ],
                          })
                          .then((resultArea) => {
                            return models.iot_tenant
                              .findOne({
                                where: { area_id: dataProfile.area_id },
                                transaction: t,
                                order: [["id", "DESC"]],
                              })
                              .then((resultTenant) => {
                                resultTenant = JSON.parse(
                                  JSON.stringify(resultTenant)
                                );
                                if (resultTenant.member_id == null) {
                                  return models.iot_tenant.update(
                                    {
                                      user_id: user.id,
                                      member_id: `${resultArea.list_area.username}.${resultArea.username}.1`,
                                    },
                                    { where: { id: res.id }, transaction: t }
                                  );
                                } else {
                                  let memberId = resultTenant.member_id;
                                  memberId = memberId.split(".");
                                  memberId[2] = parseInt(memberId[2]) + 1;
                                  memberId = memberId.join(".");
                                  return models.iot_tenant.update(
                                    {
                                      user_id: user.id,
                                      member_id: memberId,
                                    },
                                    { where: { id: res.id }, transaction: t }
                                  );
                                }
                              });
                          });
                      });
                  });
              }
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: "Berhasil terdaftar",
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error.message,
          });
        });
    });
  },

  // serviceTenantRegister: (dataUser, dataProfile, nodeType) => {
  //   return new Promise((resolve, reject) => {
  //     return models.sequelize
  //       .transaction(t => {
  //         return models.iot_users
  //           .findOrCreate({
  //             where: {
  //               email: dataUser.email
  //             },
  //             defaults: dataUser,
  //             transaction: t
  //           })
  //           .then(([res, status]) => {
  //             res = JSON.parse(JSON.stringify(res));
  //             console.log(res, " ", status, " ", dataUser);
  //             if (status == false) {
  //               throw new Error();
  //             }
  //             nodeType.map((val, index) => {
  //               nodeType[index].tenant_id = res.id;
  //             });
  //             dataProfile.user_id = res.id;
  //             return models.iot_tenant.create(dataProfile).then(() => {
  //               return models.allocation_type.bulkCreate(nodeType);
  //             });
  //           });
  //       })
  //       .then(result => {
  //         resolve({
  //           responseCode: 200,
  //           messages: "Berhasil terdaftar"
  //         });
  //       })
  //       .catch(error => {
  //         console.log(error);
  //         reject({
  //           responseCode: 400,
  //           messages: "Email telah digunakan"
  //         });
  //       });
  //   });
  // }
};

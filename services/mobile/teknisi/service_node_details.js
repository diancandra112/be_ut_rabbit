const models = require("../../../models/index");
const { Op } = models.Sequelize;
const moment = require("moment");
module.exports = {
  SvcGetTeknisiNodeDetails: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_nodes.findOne({
            where: data,
            include: [
              { model: models.iot_detail_rtu },
              { model: models.iot_device_type },
              {
                model: models.iot_tenant,
                required: false,
                on: {
                  tenantId: models.sequelize.where(
                    models.sequelize.col("iot_nodes.tenantId"),
                    "=",
                    models.sequelize.col("iot_tenant.id")
                  ),
                  areaId: models.sequelize.where(
                    models.sequelize.col("iot_nodes.areaId"),
                    "=",
                    models.sequelize.col("iot_tenant.area_id")
                  ),
                },
              },
              {
                model: models.iot_internal,
                required: false,
                on: {
                  tenantId: models.sequelize.where(
                    models.sequelize.col("iot_nodes.internalId"),
                    "=",
                    models.sequelize.col("iot_internal.id")
                  ),
                  areaId: models.sequelize.where(
                    models.sequelize.col("iot_nodes.areaId"),
                    "=",
                    models.sequelize.col("iot_internal.area_id")
                  ),
                },
              },
            ],
            transaction: t,
          });
        })
        .then((result) => {
          if (result == null) {
            reject({
              responseCode: 404,
              message: "node not found",
            });
            return "node not found";
          }
          let output = {
            pulse: result.live_pulse,
            node_id: result.id,
            last_update: moment(result.last_update)
              .utcOffset("+0700")
              .format("YYYY-MM-DD HH:mm:ss"),
            devEui: result.devEui,
            meter_id: result.meter_id,
            valve: result.live_valve,
            battery: result.live_battery + "%",
            interval: parseInt(result.setting_interval),
            device_type: result.iot_device_type.name,
            meter: isNaN(parseFloat(result.live_last_meter))
              ? 0
              : parseFloat(result.live_last_meter) / 1000,
          };
          if (result.typeId === 4) {
            output.child = result.iot_detail_rtu.model;
          } else {
            output.child = null;
          }
          output.isOffline = true;
          if (result.last_update != null) {
            const endTime = moment();
            const startTime = moment(output.last_update);
            let duration = moment.duration(endTime.diff(startTime));
            duration = duration.asMinutes();
            if (duration < result.setting_interval * 4) {
              output.isOffline = false;
            }
          } else {
            output.isOffline = true;
          }
          if (result.iot_tenant != null) {
            output = {
              ...output,
              name: result.iot_tenant.tenant_name,
              pic: result.iot_tenant.email,
              phone: result.iot_tenant.phone,
              address: result.iot_tenant.address,
            };
          } else if (result.iot_internal != null) {
            output = {
              ...output,
              name: result.iot_internal.internal_name,
              pic: result.iot_internal.pic,
              phone: result.iot_internal.phone,
              address: result.iot_internal.location,
            };
          }
          resolve({
            responseCode: 200,
            result: output,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            message: err.message,
          });
        });
    });
  },
};

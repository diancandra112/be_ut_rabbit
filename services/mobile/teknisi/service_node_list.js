const models = require("../../../models/index");
const { Op } = models.Sequelize;
const moment = require("moment");

module.exports = {
  SvcGetTeknisiNodeList: (data, condition) => {
    return new Promise((resolve, reject) => {
      let assigned = {};
      let search = {};
      let searchintenal = {};
      let searchtenant = {};
      condition.search_by =
        condition.search_by == undefined ? "" : condition.search_by;
      condition.filter = condition.filter == undefined ? "" : condition.filter;
      switch (condition.search_by.toLocaleLowerCase().trim()) {
        case "deveui":
          search = { devEui: { [Op.substring]: condition.search_value } };
          break;
        case "name":
          searchintenal = {
            internal_name: { [Op.substring]: condition.search_value },
          };
          searchtenant = {
            tenant_name: { [Op.substring]: condition.search_value },
          };
          break;
        case "email":
          searchintenal = { email: { [Op.substring]: condition.search_value } };
          searchtenant = { email: { [Op.substring]: condition.search_value } };
          break;

        default:
          search = {};
          break;
      }
      switch (condition.filter.toLocaleLowerCase().trim()) {
        case "tenant":
          assigned = {
            tenantId: { [Op.not]: null },
          };
          break;
        case "internal":
          assigned = {
            internalId: { [Op.not]: null },
          };
          break;
        default:
          assigned = {
            [Op.or]: {
              tenantId: { [Op.not]: null },
              internalId: { [Op.not]: null },
            },
          };
          break;
      }
      let inc = [];
      if ([4, 11, 12].includes(parseInt(data.typeId))) {
        inc = [
          {
            model: models.iot_detail_rtu,
          },
          // {
          //   model: models.iot_histories,
          //   limit: 1,
          //   order: [["reportTime", "DESC"]],
          // },
          {
            model: models.iots_charts_usage,
            required: false,
            where: {
              [Op.and]: [
                {
                  fdate: {
                    [Op.lte]: moment().utcOffset("+0700").format("YYYY-MM-DD"),
                  },
                },
                {
                  fdate: {
                    [Op.gte]: moment()
                      .utcOffset("+0700")
                      .subtract(1, "day")
                      .format("YYYY-MM-DD"),
                  },
                },
              ],
            },
          },
          {
            model: models.iot_tenant,
            where: searchtenant,
            required: false,
            on: {
              tenantId: models.sequelize.where(
                models.sequelize.col("iot_nodes.tenantId"),
                "=",
                models.sequelize.col("iot_tenant.id")
              ),
              areaId: models.sequelize.where(
                models.sequelize.col("iot_nodes.areaId"),
                "=",
                models.sequelize.col("iot_tenant.area_id")
              ),
            },
          },
          {
            model: models.iot_internal,
            where: searchintenal,
            required: false,
            on: {
              tenantId: models.sequelize.where(
                models.sequelize.col("iot_nodes.internalId"),
                "=",
                models.sequelize.col("iot_internal.id")
              ),
              areaId: models.sequelize.where(
                models.sequelize.col("iot_nodes.areaId"),
                "=",
                models.sequelize.col("iot_internal.area_id")
              ),
            },
          },
        ];
      } else {
        inc = [
          {
            model: models.iot_detail_rtu,
          },
          {
            model: models.iots_charts_usage,
            required: false,
            where: {
              [Op.and]: [
                {
                  fdate: {
                    [Op.lte]: moment().utcOffset("+0700").format("YYYY-MM-DD"),
                  },
                },
                {
                  fdate: {
                    [Op.gte]: moment()
                      .utcOffset("+0700")
                      .subtract(1, "day")
                      .format("YYYY-MM-DD"),
                  },
                },
              ],
            },
          },
          {
            model: models.iot_tenant,
            where: searchtenant,
            required: false,
            on: {
              tenantId: models.sequelize.where(
                models.sequelize.col("iot_nodes.tenantId"),
                "=",
                models.sequelize.col("iot_tenant.id")
              ),
              areaId: models.sequelize.where(
                models.sequelize.col("iot_nodes.areaId"),
                "=",
                models.sequelize.col("iot_tenant.area_id")
              ),
            },
          },
          {
            model: models.iot_internal,
            where: searchintenal,
            required: false,
            on: {
              tenantId: models.sequelize.where(
                models.sequelize.col("iot_nodes.internalId"),
                "=",
                models.sequelize.col("iot_internal.id")
              ),
              areaId: models.sequelize.where(
                models.sequelize.col("iot_nodes.areaId"),
                "=",
                models.sequelize.col("iot_internal.area_id")
              ),
            },
          },
        ];
      }
      return models.sequelize
        .transaction((t) => {
          return models.iot_nodes.findAll({
            where: [
              assigned,
              data,
              {
                is_display: true,
                [Op.or]: {
                  devEui: { [Op.substring]: condition.search_value },
                  tenant_name: models.sequelize.where(
                    models.sequelize.col("iot_tenant.tenant_name"),
                    "LIKE",
                    `%${condition.search_value}%`
                  ),
                },
              },
            ],
            order: [["last_update", "DESC"]],
            include: inc,
            transaction: t,
          });
        })
        .then((_result) => {
          _result = JSON.parse(JSON.stringify(_result));
          let output = [];
          _result.map((val) => {
            let temp = {
              areaId: val.areaId,
              battery: [3, 7].includes(val.typeId)
                ? 100
                : parseFloat(val.live_battery),
              last_update: val.last_update,
              is_online: false,
              raw: [],
            };
            if (val.typeId == 13) {
              temp.raw.push(
                { name: "interval", value: val.setting_interval },
                { name: "model", value: val.iot_detail_rtu.model },
                { name: "description", value: val.description },
                {
                  name: "low_alarm",
                  value: val.low_alarm,
                },
                {
                  name: "high_alarm",
                  value: val.high_alarm,
                },
                {
                  name: "sensor_level",
                  value: val.sensor_to_botom,
                },
                {
                  name: "water_level",
                  value:
                    val.live_last_meter == null
                      ? 0
                      : val.sensor_to_botom -
                        val.live_last_meter +
                        val.sensor_level_offset,
                }
              );
            }
            if ([5, 9].includes(val.typeId)) {
              temp.raw.push({
                name: "Pressure",
                value:
                  val.typeId == 9
                    ? (parseFloat(val.live_last_meter) * 2 + 10).toFixed(3) +
                      " /Bar"
                    : parseFloat(val.live_last_meter).toFixed(3) + " /Bar",
              });
            }
            // if ([11, 12].includes(val.typeId)) {
            //   if (val.iot_histories.length > 0) {
            //     JSON.parse(val.iot_histories[0].payload).meter.exReading.filter(
            //       (valF) => {
            //         if (valF.name.toLocaleLowerCase().trim() == "pressure") {
            //           temp.raw.push({
            //             name: valF.name,
            //             value: (parseFloat(valF.value) / 100000).toFixed(3),
            //           });
            //         } else if (
            //           valF.name.toLocaleLowerCase().trim() == "temperature"
            //         ) {
            //           temp.raw.push({ name: valF.name, value: valF.value });
            //         }
            //       }
            //     );
            //   }
            // }
            // if (val.typeId == 4) {
            //   if (val.iot_histories.length > 0) {
            //     temp.raw = JSON.parse(
            //       val.iot_histories[0].payload
            //     ).subMeter.find((valF) => {
            //       return valF.applicationName == val.iot_detail_rtu.model;
            //     }).report;
            //   }
            // }
            if ([1, 2, 3, 7].includes(val.typeId)) {
              temp.raw.push({
                name: "Totalizer",
                value:
                  (parseFloat(val.live_last_meter) / 1000).toFixed(3) +
                  ` ${[1, 2].includes(val.typeId) ? "/m3" : "/kwh"}`,
              });
              let yesterday_usage = val.iots_charts_usages.filter((valP) => {
                return (
                  valP.fdate ==
                  moment()
                    .utcOffset("+0700")
                    .subtract(1, "day")
                    .format("YYYY-MM-DD")
                );
              });
              if (yesterday_usage.length > 0) {
                temp.yesterday_usage = 0;
                yesterday_usage.map((valT) => {
                  temp.yesterday_usage += parseFloat(valT.usage) / 1000;
                });
                temp.raw.push({
                  name: "Yesterday Usage",
                  value:
                    temp.yesterday_usage.toFixed(3) +
                    ` ${[1, 2].includes(val.typeId) ? "m3" : "kwh"}`,
                });
                delete temp.yesterday_usage;
              } else {
                temp.raw.push({
                  name: "Yesterday Usage",
                  value: 0 + ` ${[1, 2].includes(val.typeId) ? "m3" : "kwh"}`,
                });
              }
              let today_usage = val.iots_charts_usages.filter((valP) => {
                return (
                  valP.fdate == moment().utcOffset("+0700").format("YYYY-MM-DD")
                );
              });
              if (today_usage.length > 0) {
                temp.today_usage = 0;
                today_usage.map((valT) => {
                  temp.today_usage += parseFloat(valT.usage) / 1000;
                });
                temp.raw.push({
                  name: "Today Usage",
                  value:
                    temp.today_usage.toFixed(3) +
                    ` ${[1, 2].includes(val.typeId) ? "m3" : "kwh"}`,
                });
                delete temp.today_usage;
              } else {
                temp.raw.push({
                  name: "Today Usage",
                  value: 0 + ` ${[1, 2].includes(val.typeId) ? "m3" : "kwh"}`,
                });
              }
            }
            if (val.last_update != null) {
              const endTime = moment();
              const startTime = moment(val.last_update);
              let duration = moment.duration(endTime.diff(startTime));
              duration = duration.asMinutes();
              if (duration < val.setting_interval * 4) {
                temp.is_online = true;
              }
            }
            if (val.iot_tenant != null) {
              output.push({
                node_id: val.id,
                type: "TENANT",
                devEui: val.devEui,
                name: val.iot_tenant.tenant_name,
                email: val.iot_tenant.email,
                ...temp,
              });
            } else if (val.iot_internal != null) {
              output.push({
                node_id: val.id,
                type: "INTERNAL",
                devEui: val.devEui,
                name: val.iot_internal.internal_name,
                email: val.iot_internal.email,
                ...temp,
              });
            }
          });
          resolve({
            responseCode: 200,
            result: output,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            message: err.message,
          });
        });
    });
  },
};

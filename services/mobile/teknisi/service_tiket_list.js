const models = require("../../../models/index");
const { Op } = models.Sequelize;
const moment = require("moment");
const { weiots } = require("../../../config/axios");
const axios = require("axios");

module.exports = {
  SvcTeknisiTiketInstallationSave: (
    data,
    images,
    dataInstallation,
    dataGateway,
    dataInstallation_detail
  ) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_tiketing
            .findOne({
              where: { id: data.id },
              transaction: t,
            })
            .then((tiket_get) => {
              tiket_get = JSON.parse(JSON.stringify(tiket_get));
              return models.iots_tiketing
                .update(data, {
                  where: { id: data.id, status: { [Op.not]: "close" } },
                  transaction: t,
                })
                .then((_resUpTiket) => {
                  _resUpTiket = JSON.parse(JSON.stringify(_resUpTiket));
                  if (_resUpTiket == 0) {
                    throw new Error("Tiket Already Close Or Not Found");
                  } else {
                    if (dataInstallation != null) {
                      dataInstallation.areaId = tiket_get.area_id;
                      if (tiket_get.type_user == "tenant") {
                        dataInstallation.tenantId = tiket_get.user_id;
                      } else {
                        dataInstallation.internalId = tiket_get.user_id;
                      }
                      return models.iot_nodes
                        .findOrCreate({
                          where: {
                            devEui: dataInstallation.devEui,
                          },
                          defaults: dataInstallation,
                          transaction: t,
                        })
                        .then(([node_create, status]) => {
                          if (status == false) {
                            return reject({
                              responseCode: 400,
                              messages: "devEui telah digunakan",
                            });
                          } else {
                            node_create = JSON.parse(
                              JSON.stringify(node_create)
                            );
                            dataInstallation_detail.node_id = node_create.id;
                            let dtl = dataInstallation_detail;
                            return models.iots_tiketing_attachment
                              .bulkCreate(images, {
                                transaction: t,
                              })
                              .then(() => {
                                console.log("topup");
                                axios.patch(process.env.HOST_SERVER + "/node", {
                                  email: process.env.HOST_SERVER_EMAIL,
                                  devEUI: dataInstallation.devEui,
                                });
                                if (dataInstallation.setting_pulse > 0) {
                                  let pulseCount = parseFloat(
                                    dataInstallation.start_totalizer
                                  );
                                  weiots({
                                    method: "post",
                                    url: `/action/pulse/${dataInstallation.devEui}/${dataInstallation.setting_pulse}/${pulseCount}`,
                                  });
                                } else if (
                                  dataInstallation.start_totalizer > 0
                                ) {
                                  let meter = parseFloat(
                                    dataInstallation.start_totalizer
                                  );
                                  weiots({
                                    method: "post",
                                    url: `/action/meter/${dataInstallation.devEui}/${meter}`,
                                  });
                                }
                                // if (dataInstallation.device_type_id == 1) {
                                //   weiots({
                                //     method: "post",
                                //     url: `/action/interval/${
                                //       dataInstallation.devEui
                                //     }/${
                                //       parseInt(dataInstallation.interval) * 60
                                //     }`,
                                //   });
                                // } else {
                                //   weiots({
                                //     method: "post",
                                //     url: `/action/topup/${dataInstallation.devEui}/${dataInstallation.prepayment}`,
                                //   });
                                // }
                                switch (parseInt(dataInstallation.typeId)) {
                                  case 1:
                                    console.log("GAS");
                                    return models.iot_detail_gas.create(dtl, {
                                      transaction: t,
                                    });
                                    break;
                                  case 2:
                                    console.log("WATER");
                                    return models.iot_detail_water.create(dtl, {
                                      transaction: t,
                                    });
                                    break;
                                  case 3:
                                    console.log("ELECTRIC NON CT");
                                    return models.iot_detail_electric.create(
                                      dtl,
                                      {
                                        transaction: t,
                                      }
                                    );
                                    break;
                                  case 4:
                                    console.log("RTU");
                                    return models.iot_detail_rtu.create(dtl, {
                                      transaction: t,
                                    });
                                    break;
                                  case 5:
                                    console.log("PREASURE");
                                    return models.iot_detail_preassure.create(
                                      dtl,
                                      {
                                        transaction: t,
                                      }
                                    );
                                    break;
                                  case 11:
                                    console.log("PREASURE SINDCON");
                                    return models.iot_detail_preassure.create(
                                      dtl,
                                      {
                                        transaction: t,
                                      }
                                    );
                                    break;
                                  case 9:
                                    console.log("PREASURE PT");
                                    return models.iot_detail_preassure.create(
                                      dtl,
                                      {
                                        transaction: t,
                                      }
                                    );
                                    break;
                                  case 10:
                                    console.log("PJU");
                                    return models.iot_detail_pju.create(dtl, {
                                      transaction: t,
                                    });
                                    break;
                                  case 8:
                                    console.log("WATER PREASURE");
                                    return models.iot_detail_preassure.create(
                                      dtl,
                                      {
                                        transaction: t,
                                      }
                                    );
                                    break;
                                  case 6:
                                    console.log("TEMPERATURE");
                                    return models.iot_detail_temperature.create(
                                      dtl,
                                      {
                                        transaction: t,
                                      }
                                    );
                                    break;
                                  case 7:
                                    console.log("ELECTRIC CT");
                                    return models.iot_detail_electricct.create(
                                      dtl,
                                      {
                                        transaction: t,
                                      }
                                    );
                                    break;

                                  default:
                                    return "ok";
                                    break;
                                }
                              });
                          }
                        });
                    } else if (dataGateway != null) {
                      dataGateway.area_id = tiket_get.area_id;
                      return models.iot_gateways
                        .create(dataGateway, {
                          transaction: t,
                        })
                        .then(() => {
                          return models.iots_tiketing_attachment.bulkCreate(
                            images,
                            {
                              transaction: t,
                            }
                          );
                        });
                    }
                  }
                });
            });
        })
        .then((_result) => {
          resolve({
            responseCode: 200,
            message: "successfully save installation",
            _result: _result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            message: err.message,
          });
        });
    });
  },
  SvcTeknisiTiketMaintenanceSave: (data, images) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_tiketing
            .update(data, {
              where: { id: data.id },
              transaction: t,
            })
            .then((response) => {
              if (response[0] == 0) {
                reject({
                  responseCode: 404,
                  message: "tiket not found",
                });
                return "null";
              } else {
                return models.iots_tiketing_attachment.bulkCreate(images, {
                  transaction: t,
                });
              }
            });
        })
        .then((_result) => {
          resolve({
            responseCode: 200,
            message: "successfully save maintenance",
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            message: err.message,
          });
        });
    });
  },
  SvcEditTeknisiTiketStatus: (data, technician_id) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_tiketing
            .findOne({
              where: { id: data.id },
              transaction: t,
            })
            .then((response) => {
              response = JSON.parse(JSON.stringify(response));
              if (response.technician_id != technician_id) {
                reject({
                  responseCode: 401,
                  message: "User Does Not Match",
                });
                return "null";
              }
              if (response == null) {
                reject({
                  responseCode: 404,
                  message: "Tiket Not Found",
                });
                return "null";
              }
              if (
                (data.status == "onprogress" &&
                  response.status == "onprogress") ||
                (data.status == "onprogress" && response.status == "close")
              ) {
                throw new Error(
                  `can't change the status to ${data.status} because it's ${response.status}`
                );
              } else if (
                (data.status == "close" && response.status == "new") ||
                (data.status == "close" && response.status == "close")
              ) {
                throw new Error(
                  `can't change the status to ${data.status} because it's ${response.status}`
                );
              }
              return models.iots_tiketing.update(data, {
                where: { id: data.id },
                transaction: t,
              });
            });
        })
        .then((_result) => {
          resolve({
            responseCode: 200,
            message: "successfully changed status",
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            message: err.message,
          });
        });
    });
  },
  SvcGetTeknisiTiketList: (data, type, page, size) => {
    return new Promise(async (resolve, reject) => {
      try {
        let filter = {};
        let type_flag = type.ticket_type.toLocaleLowerCase().trim();
        if (type_flag == "all") {
          type = {};
        }
        if (data.filter != undefined) {
          let tempData = [];
          data.filter = data.filter.split(",");
          data.filter.map((val) => {
            if (
              val.toLocaleLowerCase().trim() == "new" ||
              val.toLocaleLowerCase().trim() == "onprogress" ||
              val.toLocaleLowerCase().trim() == "close"
            ) {
              tempData.push({ status: val });
            }
          });
          if (tempData.length > 0) {
            filter = { [Op.or]: tempData };
          }
          delete data.filter;
        }
        let count = await models.iots_tiketing.count({
          where: [filter, data, type],
        });
        count = JSON.parse(JSON.stringify(count));
        let _result = await models.iots_tiketing.findAll({
          limit: size,
          offset: (page - 1) * size,
          where: [filter, data, type],
          include: [
            { model: models.iot_tenant },
            { model: models.iot_internal },
          ],
          order: [
            [models.iot_tenant, "tenant_name", "ASC"],
            [models.iot_internal, "internal_name", "ASC"],
          ],
        });
        _result = JSON.parse(JSON.stringify(_result));
        let output = [];
        _result.map((val, idx) => {
          output.push({
            id: val.id,
            area_id: val.area_id,
            user_type: val.type_user,
            user_name: "",
            no_tiket: val.no_tiket,
            type_service: val.type_service,
            status: val.status,
            create_date: moment(val.createdAt)
              .utcOffset("+0700")
              .format("YYYY-MM-DD HH:mm:ss"),
            on_progress_date: moment(val.ticket_processing_date).isValid()
              ? moment(val.ticket_processing_date)
                  .utcOffset("+0700")
                  .format("YYYY-MM-DD HH:mm:ss")
              : null,
            close_date: moment(val.ticket_close_date).isValid()
              ? moment(val.ticket_close_date)
                  .utcOffset("+0700")
                  .format("YYYY-MM-DD HH:mm:ss")
              : null,
            note: val.note,
            ticket_type: val.ticket_type,
          });
          if (val.iot_tenant != null && val.type_user == "tenant") {
            output[idx].user_name = val.iot_tenant.tenant_name;
          } else if (val.iot_internal != null && val.type_user == "internal") {
            output[idx].user_name = val.iot_internal.internal_name;
          }
        });
        resolve({
          responseCode: 200,
          result: output,
          page: page,
          size: size,
          total: count,
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          message: error.message,
        });
      }
    });
  },
  SvcGetTeknisiTiketDetail: (data, id) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_tiketing.findOne({
            where: [data, id],
            include: [
              { model: models.iot_tenant },
              { model: models.iot_internal },
              {
                model: models.iots_tiketing_attachment,
                as: "images",
                where: { is_teknisi: false },
                required: false,
              },
              {
                model: models.iots_tiketing_attachment,
                as: "images_teknisi",
                where: { is_teknisi: true },
                required: false,
              },
            ],
            transaction: t,
          });
        })
        .then((_result) => {
          _result = JSON.parse(JSON.stringify(_result));
          if (_result == null) {
            reject({
              responseCode: 404,
              message: "tiket not found",
            });
            return "not found";
          }
          let output = {
            ..._result,
            id: _result.id,
            user_type: _result.type_user,
            user_name: "",
            no_tiket: _result.no_tiket,
            node_type: _result.node_type,
            type_service: _result.type_service,
            status: _result.status,
            create_date: moment(_result.createdAt)
              .utcOffset("+0700")
              .format("YYYY-MM-DD HH:mm:ss"),
            on_progress_date: moment(_result.ticket_processing_date).isValid()
              ? moment(_result.ticket_processing_date)
                  .utcOffset("+0700")
                  .format("YYYY-MM-DD HH:mm:ss")
              : null,
            close_date: moment(_result.ticket_close_date).isValid()
              ? moment(_result.ticket_close_date)
                  .utcOffset("+0700")
                  .format("YYYY-MM-DD HH:mm:ss")
              : null,
            note: _result.note,
            note_teknisi: _result.note_teknisi,
            ticket_type: _result.ticket_type,
            images_customer: _result.images,
            images_teknisi: _result.images_teknisi,
          };
          if (_result.iot_tenant != null && _result.type_user == "tenant") {
            output.user_name = _result.iot_tenant.tenant_name;
            output.phone = _result.iot_tenant.handphone;
          } else if (
            _result.iot_internal != null &&
            _result.type_user == "internal"
          ) {
            output.user_name = _result.iot_internal.internal_name;
            output.phone = _result.iot_internal.handphone;
          }
          resolve({
            responseCode: 200,
            result: output,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            message: err.message,
          });
        });
    });
  },
};

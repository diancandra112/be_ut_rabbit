const models = require("../../../models/index");
const { Op } = models.Sequelize;

module.exports = {
  SvcGetTeknisiNodeType: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_teknisi.findOne({
            where: data,
            include: [
              { model: models.iots_node_type_teknisi, as: "node_type" },
            ],
            transaction: t,
          });
        })
        .then((_result) => {
          resolve({
            responseCode: 200,
            result: _result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            message: err.message,
          });
        });
    });
  },
};

const models = require("../../models/index");
const moment = require("moment");
const { Op } = models.Sequelize;
module.exports = {
  SvcUsageActivity: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_histories
            .findAll({
              where: {
                device_id: data.device_id,
                reportTimeFlag1: models.sequelize.where(
                  models.sequelize.fn(
                    "date",
                    models.sequelize.col("iot_histories.reportTimeFlag")
                  ),
                  ">=",
                  moment(data.from).utcOffset("+0700").format("YYYY-MM-DD")
                ),
                reportTimeFlag2: models.sequelize.where(
                  models.sequelize.fn(
                    "date",
                    models.sequelize.col("iot_histories.reportTimeFlag")
                  ),
                  "<=",
                  moment(data.to).utcOffset("+0700").format("YYYY-MM-DD")
                ),
              },
              order: [["reportTimeFlag", "ASC"]],
              transaction: t,
            })
            .then((one) => {
              one = JSON.parse(JSON.stringify(one));
              let outputData = [];
              let unit = "";
              const type = parseInt(data.typeId);
              switch (type) {
                case 1:
                  unit = "M3";
                  break;
                case 2:
                  unit = "M3";
                  break;
                case 3:
                  unit = "KWH";
                  break;
                case 7:
                  unit = "KWH";
                  break;

                default:
                  break;
              }
              if (one.length > 0) {
                one.map((val, index) => {
                  let meter = 0;
                  if (type == 1 || type == 2 || type == 3 || type == 7) {
                    meter = JSON.parse(val.payload).meter.meterReading;
                    meter = isNaN(meter) == true ? 0 : meter / 1000;
                  }
                  outputData.push({
                    device_id: val.device_id,
                    reportTime: val.reportTimeFlag,
                    unit: unit,
                    meter: meter,
                  });
                });
              }
              return {
                responseCode: 200,
                messages: "retrieve data successfully",
                data: outputData,
              };
            });
        })
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: "failed to retrieve data",
            error: error.message,
          });
        });
    });
  },
  SvcUsageMonth: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_histories
            .findOne({
              where: {
                device_id: data.device_id,
                reportTimeFlag1: models.sequelize.where(
                  models.sequelize.fn(
                    "year",
                    models.sequelize.col("iot_histories.reportTimeFlag")
                  ),
                  "=",
                  moment().utcOffset("+0700").format("YYYY")
                ),
                reportTimeFlag2: models.sequelize.where(
                  models.sequelize.fn(
                    "month",
                    models.sequelize.col("iot_histories.reportTimeFlag")
                  ),
                  "<",
                  moment().utcOffset("+0700").format("MM")
                ),
              },
              order: [["reportTimeFlag", "DESC"]],
              transaction: t,
            })
            .then((one) => {
              let last_meter_before = JSON.parse(one.payload).meter
                .meterReading;
              return models.iot_histories
                .findAll({
                  where: {
                    device_id: data.device_id,
                    reportTimeFlag1: models.sequelize.where(
                      models.sequelize.fn(
                        "year",
                        models.sequelize.col("iot_histories.reportTimeFlag")
                      ),
                      "=",
                      moment().utcOffset("+0700").format("YYYY")
                    ),
                    reportTimeFlag2: models.sequelize.where(
                      models.sequelize.fn(
                        "month",
                        models.sequelize.col("iot_histories.reportTimeFlag")
                      ),
                      "=",
                      moment().utcOffset("+0700").format("MM")
                    ),
                  },
                  order: [["reportTime", "ASC"]],
                  transaction: t,
                })
                .then((response) => {
                  response = JSON.parse(JSON.stringify(response));
                  let usage = 0;
                  let unit = "";
                  const type = parseInt(data.typeId);
                  if (type == 1 || type == 2 || type == 3 || type == 7) {
                    usage =
                      JSON.parse(response[response.length - 1].payload).meter
                        .meterReading - parseFloat(last_meter_before);
                  }
                  switch (type) {
                    case 1:
                      unit = "M3";
                      break;
                    case 2:
                      unit = "M3";
                      break;
                    case 3:
                      unit = "KWH";
                      break;
                    case 7:
                      unit = "KWH";
                      break;

                    default:
                      break;
                  }
                  return {
                    responseCode: 200,
                    messages: "retrieve data successfully",
                    usage: (usage / 1000).toFixed(3),
                    unit: unit,
                  };
                });
            });
        })
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: "failed to retrieve data",
            error: error.message,
          });
        });
    });
  },
};

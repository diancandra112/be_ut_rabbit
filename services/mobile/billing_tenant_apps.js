const models = require("../../models/index");
const { Op } = models.Sequelize;
const jwt = require("jsonwebtoken");
require("dotenv").config();

module.exports = {
  svcListBilling: (data) => {
    return new Promise(async (resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_billing_history.findAll({
            where: [
              { status: { [Op.in]: ["CANCEL", "PAID", "UNPAID"] } },
              data,
            ],
            attributes: ["due_date", "status", "invoice", "id"],
          });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            payload: result,
          });
        })
        .catch((err) => {
          resolve({
            responseCode: 400,
            payload: err.message,
          });
        });
    });
  },
  svcListBillingDetails: (data) => {
    return new Promise(async (resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_billing_history.findOne({
            where: [data, {}],
            include: [
              { model: models.iot_tenant },
              {
                model: models.iots_billing_attachment,
                on: {
                  col1: models.sequelize.where(
                    models.sequelize.col("iots_billing_history.id"),
                    "=",
                    models.sequelize.col("iots_billing_attachments.billingId")
                  ),
                  col2: models.sequelize.where(
                    models.sequelize.col("iots_billing_attachments.default"),
                    "=",
                    true
                  ),
                },
              },
            ],
          });
        })
        .then((result) => {
          result = JSON.parse(JSON.stringify(result));
          if (result == null) {
            throw new Error("Invalid id");
          }
          result.iots_billing_attachments.map((val, idx) => {
            result.iots_billing_attachments[idx].file_pdf =
              process.env.PDF_URL + val.file_pdf;
          });
          let output = {
            tenant_name: result.iot_tenant.tenant_name,
            invoice: result.invoice,
            due_date: result.due_date,
            va: result.virtual_account.split("atau"),
            total_usage: parseFloat(
              (result.node_type == "RTU"
                ? parseFloat(result.billing_usage)
                : parseFloat(result.billing_usage) / 1000
              ).toFixed(3)
            ),
            price: parseFloat(result.harga_satuan),
          };
          output.va.map((val, idx) => {
            output.va[idx] = val.trim();
          });
          output.total = parseInt(
            output.total_usage * parseFloat(result.harga_satuan)
          );
          output.tax = 0;
          if (parseFloat(result.ppn) > 0) {
            output.tax = parseInt(
              parseFloat(output.total) / parseFloat(result.ppn)
            );
          }
          output.Subtotal = output.total + output.tax;

          let _denda = Array.isArray(JSON.parse(result.denda))
            ? JSON.parse(result.denda)
            : [];
          let total_denda = 0;
          _denda.map((valDenda, idx2) => {
            total_denda += parseInt(valDenda.jumlah_denda);
          });
          result.denda = total_denda;
          output.payment_pinalty = parseFloat(result.denda);
          output.Materai = result.materai;
          output.total_cost =
            output.Subtotal + output.payment_pinalty + output.Materai;
          output.fee = parseFloat(result.biaya_transaksi);
          output.payment_adjusment = parseFloat(result.biaya_penyesuaian);
          output.amount =
            output.total_cost + output.fee + output.payment_adjusment;
          output.iots_billing_attachments = result.iots_billing_attachments;
          [
            "price",
            "total",
            "tax",
            "Subtotal",
            "payment_pinalty",
            "total_cost",
            "fee",
            "payment_adjusment",
            "amount",
          ].map((val) => {
            if (parseInt(output[val]) > 0) {
              output[val] = parseInt(output[val]) + 0.00001;
            }
          });
          resolve({
            responseCode: 200,
            payload: output,
            // payload2: result,
          });
        })
        .catch((err) => {
          resolve({
            responseCode: 400,
            payload: err.message,
          });
        });
    });
  },
};

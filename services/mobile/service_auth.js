const models = require("../../models/index");
const { Op } = models.Sequelize;
const jwt = require("jsonwebtoken");
const moment = require("moment");
require("dotenv").config();

module.exports = {
  svcTeknisiLogOut: (data) => {
    return new Promise(async (resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_teknisi
            .findOne({
              where: {
                id: data,
              },
              transaction: t,
            })
            .then((out) => {
              out = JSON.parse(JSON.stringify(out));
              if (out != null) {
                return models.iots_teknisi.update(
                  { fcm_token: null },
                  {
                    where: {
                      [Op.or]: {
                        id: data,
                        fcm_token: out.fcm_token,
                      },
                    },
                    transaction: t,
                  }
                );
              } else {
                return "ok";
              }
            });
        })
        .then((user) => {
          resolve({
            responseCode: 200,
            message: "logout successful",
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            message: err.message,
          });
        });
    });
  },
  svcMobileLogin: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        switch (parseInt(data.roleId)) {
          case 3:
            models.iot_tenant
              .findOne({
                where: {
                  area_id: data.area_id,
                  username: data.username,
                  password: data.password,
                },
                attributes: { exclude: ["password", "createdAt", "updatedAt"] },
              })
              .then((user) => {
                if (user != null) {
                  user = JSON.parse(JSON.stringify(user));
                  user.role = "TENANT";
                  const token = jwt.sign(user, process.env.SECRET);
                  resolve({
                    responseCode: 200,
                    token: `Bearer ${token}`,
                    profile: user,
                  });
                } else {
                  reject({
                    responseCode: 401,
                    messages: "Email atau password salah",
                  });
                }
              })
              .catch((err) => {
                console.log(err);
                reject({
                  responseCode: 400,
                  response1: JSON.stringify(err),
                  response2: "Gagal Update Billing",
                });
              });
            break;

          default:
            break;
        }
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 401,
          messages: "Email atau password salah",
          err: JSON.stringify(error),
        });
      }
    });
  },
  svcMobileTeknisiLogin: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_users.findOne({
            where: {
              area_id: data.area_id,
              email: data.username,
              password: data.password,
              role_id: 6,
            },
            include: [
              {
                model: models.iots_teknisi,
                attributes: { exclude: ["password", "createdAt", "updatedAt"] },
              },
            ],
            attributes: { exclude: ["password", "createdAt", "updatedAt"] },
            transaction: t,
          });
        })
        .then((user) => {
          if (user != null) {
            user = JSON.parse(JSON.stringify(user));
            user.role = "TEKNISI";
            let token = { ...user.iots_teknisi, role: "TEKNISI" };
            token = jwt.sign(token, process.env.SECRET);
            resolve({
              responseCode: 200,
              token: `Bearer ${token}`,
              profile: user,
            });
          } else {
            reject({
              responseCode: 401,
              messages: "Email atau password salah",
            });
          }
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            message: err.message,
          });
        });
    });
  },
  svcMobileTeknisiCompanyLoginGet: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_teknisi.findAll({
            where: { ...data, is_company: true },
            include: [{ model: models.iots_node_type_teknisi }],
            attributes: { exclude: ["password", "createdAt", "updatedAt"] },
            transaction: t,
          });
        })
        .then((user) => {
          resolve({
            responseCode: 200,
            data: user,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            message: err.message,
          });
        });
    });
  },
  svcMobileTeknisiCompanyLogin: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_users.findOne({
            where: {
              email: data.username,
              password: data.password,
              role_id: 8,
            },
            include: [
              {
                model: models.iots_teknisi,
                on: models.sequelize.where(
                  models.sequelize.col(`iot_users.id`),
                  "=",
                  models.sequelize.col(`iots_teknisi.user_id`)
                ),
                attributes: { exclude: ["password", "createdAt", "updatedAt"] },
              },
            ],
            attributes: { exclude: ["password", "createdAt", "updatedAt"] },
            transaction: t,
          });
        })
        .then((user) => {
          if (user != null) {
            user = JSON.parse(JSON.stringify(user));
            if (user.iots_teknisi.status != true) {
              reject({
                responseCode: 401,
                messages: "user not active",
              });
              return "reject";
            }
            user.role = "TEKNISI COMPANY";
            let token = { ...user.iots_teknisi, role: "TEKNISI COMPANY" };
            token = jwt.sign(token, process.env.SECRET);
            resolve({
              responseCode: 200,
              token: `Bearer ${token}`,
              profile: user,
            });
          } else {
            reject({
              responseCode: 401,
              messages: "Email atau password salah",
            });
          }
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            message: err.message,
          });
        });
    });
  },
  svcMobileTeknisiUpdateFcmToken: (data, condition) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_teknisi.update(data, {
            where: condition,
            transaction: t,
          });
        })
        .then((user) => {
          resolve({
            responseCode: 200,
            messages: `Sukses Update Fcm Token`,
            token: data.fcm_token,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            message: err.message,
          });
        });
    });
  },
  svcMobileTenantNodeList: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_nodes.findAll({
            where: { ...data },
            include: [
              { model: models.iot_node_type },
              {
                model: models.iots_charts_usage,
                required: false,
                where: {
                  fdate: {
                    [Op.gte]: moment()
                      .utcOffset("+0700")
                      .subtract(1, "day")
                      .format("YYYY-MM-DD"),
                  },
                },
              },
            ],
            transaction: t,
          });
        })
        .then((user) => {
          user = JSON.parse(JSON.stringify(user));
          let output = [];
          user.map((value) => {
            let temp = {
              type: value.iot_node_type.type_name,
              totalizer: [1, 2, 3, 7].includes(value.typeId)
                ? value.live_last_meter / 1000
                : value.typeId == 9
                ? value.live_last_meter * 2 + 10
                : value.live_last_meter,
              devEui: value.devEui,
              today_usage: null,
              yesterday_usage: null,
              battery: [3, 7].includes(value.typeId)
                ? 100
                : parseFloat(value.live_battery),
              last_update: value.last_update,
              is_online: false,
            };
            if (value.typeId == 13) {
              temp.low_alarm = value.low_alarm;
              temp.high_alarm = value.high_alarm;
              temp.sensor_level = value.sensor_to_botom;
              temp.water_level = value.sensor_to_botom - value.live_last_meter;
            }
            if ([1, 2, 3, 7].includes(value.typeId)) {
              let yesterday_usage = value.iots_charts_usages.filter((valP) => {
                return (
                  valP.fdate ==
                  moment()
                    .utcOffset("+0700")
                    .subtract(1, "day")
                    .format("YYYY-MM-DD")
                );
              });
              if (yesterday_usage.length > 0) {
                temp.yesterday_usage = 0;
                yesterday_usage.map((valT) => {
                  temp.yesterday_usage += parseFloat(valT.usage) / 1000;
                });
              }
              let today_usage = value.iots_charts_usages.filter((valP) => {
                return (
                  valP.fdate == moment().utcOffset("+0700").format("YYYY-MM-DD")
                );
              });
              if (today_usage.length > 0) {
                temp.today_usage = 0;
                today_usage.map((valT) => {
                  temp.today_usage += parseFloat(valT.usage) / 1000;
                });
              }
            }
            if (value.last_update != null) {
              const endTime = moment();
              const startTime = moment(value.last_update);
              let duration = moment.duration(endTime.diff(startTime));
              duration = duration.asMinutes();
              if (duration < value.setting_interval * 4) {
                temp.is_online = true;
              }
            }
            output.push(temp);
          });
          resolve({
            responseCode: 200,
            data: output,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            message: err.message,
          });
        });
    });
  },
};

const models = require("../../models/index");
const { Op } = models.Sequelize;
const moment = require("moment");
module.exports = {
  svcOnlineNodeList: (
    data,
    page,
    size,
    tenant,
    search,
    sort,
    order,
    start_date,
    end_date
  ) => {
    return new Promise((resolve, reject) => {
      let condition = {};
      let date = {};
      if (start_date != null || end_date != null) {
        date = {
          [Op.and]: [
            {
              last_update1: models.sequelize.where(
                models.sequelize.fn(
                  "date",
                  models.sequelize.col("iot_nodes.last_update")
                ),
                ">=",
                moment(start_date).format("YYYY-MM-DD")
              ),
            },
            {
              last_update2: models.sequelize.where(
                models.sequelize.fn(
                  "date",
                  models.sequelize.col("iot_nodes.last_update")
                ),
                "<=",
                moment(end_date).format("YYYY-MM-DD")
              ),
            },
          ],
        };
      }
      if (parseInt(tenant) == 1) {
        condition = { tenantId: { [Op.not]: null } };
      } else {
        condition = { internalId: { [Op.not]: null } };
      }
      return models.sequelize
        .transaction((t) => {
          return models.iot_nodes
            .count({
              where: [
                {
                  devEui: { [Op.substring]: search },
                  is_unsigned: false,
                  [Op.or]: [condition],
                  col1: models.sequelize.where(
                    models.sequelize.fn(
                      "TIMESTAMPDIFF",
                      models.sequelize.literal("minute"),
                      models.sequelize.col("iot_nodes.last_update"),
                      moment().utcOffset("+0800").format()
                    ),
                    "<",
                    models.sequelize.literal("`setting_interval`*4")
                  ),
                },
                data,
                date,
              ],
              transaction: t,
            })
            .then((count) => {
              return models.iot_nodes
                .findAll({
                  order: [[sort, order]],
                  limit: size,
                  offset: (page - 1) * size,
                  where: [
                    {
                      devEui: { [Op.substring]: search },
                      is_unsigned: false,
                      [Op.or]: [condition],
                      col1: models.sequelize.where(
                        models.sequelize.fn(
                          "TIMESTAMPDIFF",
                          models.sequelize.literal("minute"),
                          models.sequelize.col("iot_nodes.last_update"),
                          moment().utcOffset("+0800").format()
                        ),
                        "<",
                        models.sequelize.literal("`setting_interval`*4")
                      ),
                    },
                    data,
                    date,
                  ],
                  include: [
                    { model: models.iot_tenant, attributes: ["tenant_name"] },
                    {
                      model: models.iot_internal,
                      attributes: ["internal_name"],
                    },
                    { model: models.iot_detail_water },
                    { model: models.iot_detail_gas, as: "iot_detail_gas" },
                    { model: models.iot_detail_electric },
                    { model: models.iot_detail_electricct },
                    { model: models.iot_node_type },
                    {
                      model: models.iot_area,
                      attributes: { exclude: ["image"] },
                    },
                  ],
                  transaction: t,
                })
                .then((result) => {
                  result = JSON.parse(JSON.stringify(result));
                  count = JSON.parse(JSON.stringify(count));
                  return { result: result, count: count };
                });
            });
        })
        .then((_result_total) => {
          _result_total = JSON.parse(JSON.stringify(_result_total));
          let _result = _result_total.result;
          let count = _result_total.count;
          return resolve({
            responseCode: 200,
            total_online: _result,
            page: page,
            size: size,
            total: count,
          });
          let online = 0;
          let dataOnline = [];
          _result.rows.map((val, idx) => {
            if (val.last_update != null) {
              const endTime = moment();
              const startTime = moment(val.last_update);
              let duration = moment.duration(endTime.diff(startTime));
              duration = duration.asMinutes();
              if (duration < val.interval * 4) {
                dataOnline.push(val);
                online++;
              }
            }
          });
          dataOnline.map((val, idx) => {
            switch (parseInt(val.typeId)) {
              case 1:
                dataOnline[idx].meter_id =
                  dataOnline[idx].iot_detail_gas.meter_id;
                delete dataOnline[idx].iot_detail_gas;
                delete dataOnline[idx].iot_detail_water;
                delete dataOnline[idx].iot_detail_electric;
                delete dataOnline[idx].iot_detail_electricct;
                break;
              case 2:
                dataOnline[idx].meter_id =
                  dataOnline[idx].iot_detail_water.meter_id;

                delete dataOnline[idx].iot_detail_gas;
                delete dataOnline[idx].iot_detail_water;
                delete dataOnline[idx].iot_detail_electric;
                delete dataOnline[idx].iot_detail_electricct;
                break;
              case 3:
                dataOnline[idx].meter_id =
                  dataOnline[idx].iot_detail_electric.meter_id;

                delete dataOnline[idx].iot_detail_gas;
                delete dataOnline[idx].iot_detail_water;
                delete dataOnline[idx].iot_detail_electric;
                delete dataOnline[idx].iot_detail_electricct;
                break;
              case 7:
                dataOnline[idx].meter_id =
                  dataOnline[idx].iot_detail_electricct.meter_id;

                delete dataOnline[idx].iot_detail_gas;
                delete dataOnline[idx].iot_detail_water;
                delete dataOnline[idx].iot_detail_electric;
                delete dataOnline[idx].iot_detail_electricct;
                break;

              default:
                break;
            }
          });
          resolve({
            responseCode: 200,
            total_online: dataOnline,
            page: page,
            size: size,
            total: count,
          });
        })
        .catch((_error) => {
          console.log(_error);
          reject({
            responseCode: 400,
            message: _error.message,
          });
        });
    });
  },
  svcOnlineNode: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_nodes.count({
            where: [
              {
                is_unsigned: false,
                [Op.or]: [
                  { tenantId: { [Op.not]: null } },
                  { internalId: { [Op.not]: null } },
                ],
                col1: models.sequelize.where(
                  models.sequelize.fn(
                    "TIMESTAMPDIFF",
                    models.sequelize.literal("minute"),
                    models.sequelize.col("iot_nodes.last_update"),
                    moment().utcOffset("+0800").format()
                  ),
                  "<",
                  models.sequelize.literal("`setting_interval`*4")
                ),
              },
              data,
            ],
            transaction: t,
            // attributes: [["id", "device_id"], "interval"],
          });
        })
        .then((_result) => {
          resolve({
            responseCode: 200,
            data_online: _result,
          });
        })
        .catch((_error) => {
          reject({
            responseCode: 400,
            message: _error.message,
          });
        });
    });
  },
  svcNodePressure: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_nodes.findAll({
            where: [
              data,
              {
                // typeId: { [Op.in]: [5, 8, 9, 11, 12] },
                typeId: { [Op.in]: [5, 9, 12] },
                [Op.or]: {
                  tenantId: { [Op.not]: null },
                  internalId: { [Op.not]: null },
                },
                is_unsigned: false,
              },
            ],
            include: [
              { model: models.iot_detail_preassure },
              { model: models.iot_tenant },
              { model: models.iot_internal },
            ],
            transaction: t,
          });
        })
        .then((_result) => {
          let output = [];
          _result = JSON.parse(JSON.stringify(_result));
          _result.map((val, idx) => {
            output.push({
              desc: val.iot_detail_preassure.desc,
              last_update: val.last_update,
              devEui: val.devEui,
              typeId: val.typeId,
              type_name: val.tenantId != null ? "TENANT" : "INTERNAL",
              name:
                val.tenantId != null
                  ? val.iot_tenant.tenant_name
                  : val.iot_internal.internal_name,
              meter:
                val.live_last_meter == null
                  ? 0
                  : val.typeId == 9
                  ? (parseFloat(val.live_last_meter) * 2 + 10).toFixed(3)
                  : parseFloat(val.live_last_meter).toFixed(3),
            });
          });

          function compare(a, b) {
            if (parseFloat(a.meter) < parseFloat(b.meter)) {
              return -1;
            }
            if (parseFloat(a.meter) > parseFloat(b.meter)) {
              return 1;
            }
            return 0;
          }
          output = output.sort(compare);
          resolve({
            responseCode: 200,
            _result: output,
          });
        })
        .catch((_error) => {
          reject({
            responseCode: 400,
            message: _error.message,
          });
        });
    });
  },
  svcPressureOnlineNode: (
    data,
    page,
    size,
    tenant,
    search,
    sort,
    order,
    start_date,
    end_date
  ) => {
    return new Promise((resolve, reject) => {
      let condition = {};
      let date = {};
      if (start_date != null || end_date != null) {
        date = {
          [Op.and]: [
            {
              last_update1: models.sequelize.where(
                models.sequelize.fn(
                  "date",
                  models.sequelize.col("iot_nodes.last_update")
                ),
                ">=",
                moment(start_date).format("YYYY-MM-DD")
              ),
            },
            {
              last_update2: models.sequelize.where(
                models.sequelize.fn(
                  "date",
                  models.sequelize.col("iot_nodes.last_update")
                ),
                "<=",
                moment(end_date).format("YYYY-MM-DD")
              ),
            },
          ],
        };
      }
      if (parseInt(tenant) == 1) {
        condition = { tenantId: { [Op.not]: null } };
      } else {
        condition = { internalId: { [Op.not]: null } };
      }
      return models.sequelize
        .transaction((t) => {
          return models.iot_nodes.findAndCountAll({
            attributes: [
              "last_update",
              "typeId",
              "devEui",
              "live_battery",
              "areaId",
              "live_last_meter",
              "alarm_pressure",
            ],
            where: [
              {
                devEui: { [Op.substring]: search },
                alarm_pressure: { [Op.gt]: 0 },
                live_last_meter: { [Op.not]: null },
                typeId: { [Op.in]: [5, 8, 9, 11] },
                alarm_pressure: models.sequelize.where(
                  models.sequelize.col("iot_nodes.live_last_meter"),
                  "<",
                  models.sequelize.col("iot_nodes.alarm_pressure")
                ),
              },
              condition,
              data,
              date,
            ],
            include: [
              { model: models.iot_area, attributes: ["id", "area_name"] },
              { model: models.iot_tenant, attributes: ["tenant_name"] },
              { model: models.iot_internal, attributes: ["internal_name"] },
            ],
            order: [[sort, order]],
            transaction: t,
          });
        })
        .then((_result) => {
          _result = JSON.parse(JSON.stringify(_result));
          let type = {
            5: "PRESSURE",
            8: "WATER PRESSURE",
            9: "PRESSURE PT",
            11: "LOW PRESSURE",
          };
          _result.rows.map((val, idx) => {
            if (val.iot_internal != null) {
              _result.rows[idx].internal = val.iot_internal.internal_name;
            } else {
              _result.rows[idx].tenant = val.iot_tenant.tenant_name;
            }
            _result.rows[idx].typeId = type[val.typeId];
            delete _result.rows[idx].iot_internal;
            delete _result.rows[idx].iot_tenant;
          });
          resolve({
            page: page,
            size: size,
            total: _result.count,
            responseCode: 200,
            data: _result.rows,
          });
        })
        .catch((_error) => {
          reject({
            responseCode: 400,
            message: _error.message,
          });
        });
    });
  },
  svcPressureOnlineNodeCount: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_nodes.count({
            where: [
              {
                alarm_pressure: { [Op.gt]: 0 },
                live_last_meter: { [Op.not]: null },
                typeId: { [Op.in]: [5, 8, 9, 11] },
                [Op.or]: [
                  { tenantId: { [Op.not]: null } },
                  { internalId: { [Op.not]: null } },
                ],
                alarm_pressure: models.sequelize.where(
                  models.sequelize.col("iot_nodes.live_last_meter"),
                  "<",
                  models.sequelize.col("iot_nodes.alarm_pressure")
                ),
              },
              data,
            ],
            transaction: t,
            // attributes: [["id", "device_id"], "interval"],
          });
        })
        .then((_result) => {
          resolve({
            responseCode: 200,
            data: _result,
          });
        })
        .catch((_error) => {
          reject({
            responseCode: 400,
            message: _error.message,
          });
        });
    });
  },
  svcNodeRusakCount: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_kerusakan_gas.count({
            where: [data],
            transaction: t,
            // attributes: [["id", "device_id"], "interval"],
          });
        })
        .then((_result) => {
          resolve({
            responseCode: 200,
            data: _result,
          });
        })
        .catch((_error) => {
          reject({
            responseCode: 400,
            message: _error.message,
          });
        });
    });
  },
  svcNodeRusakList: (data, page, size) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_kerusakan_gas
            .count({
              where: [data],
              transaction: t,
            })
            .then((count) => {
              count = JSON.parse(JSON.stringify(count));
              return models.iots_kerusakan_gas
                .findAll({
                  limit: parseInt(size),
                  offset: (parseInt(page) - 1) * parseInt(size),
                  where: [data],
                  include: [
                    { model: models.iot_area, attributes: ["id", "area_name"] },
                  ],

                  transaction: t,
                })
                .then((result) => {
                  result = JSON.parse(JSON.stringify(result));
                  return { result: result, count: count };
                });
            });
        })
        .then((_result) => {
          resolve({
            responseCode: 200,
            total: _result.count,
            page: page,
            size: size,
            data: _result.result,
          });
        })
        .catch((_error) => {
          reject({
            responseCode: 400,
            message: _error.message,
          });
        });
    });
  },
};

const models = require("../../models/index");
module.exports = {
  svcStealNodeCount: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        let node = await models.iots_steal_alarm.count({
          where: data,
        });
        resolve({
          responseCode: 200,
          message: "sukses get data node Steal",
          payload: node,
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          message: error.message,
        });
      }
    });
  },
  svcStealNodeList: (data, page, size) => {
    return new Promise(async (resolve, reject) => {
      try {
        let count = await models.iots_steal_alarm.count({
          where: data,
        });
        let node = await models.iots_steal_alarm.findAll({
          include: [
            { model: models.iot_area, attributes: ["id", "area_name"] },
            {
              model: models.iot_nodes,
              attributes: ["id", "areaId", "typeId", "tenantId", "internalId"],
              include: [
                {
                  model: models.iot_tenant,
                  attributes: [["tenant_name", "name"]],
                },
                {
                  model: models.iot_internal,
                  attributes: [["internal_name", "name"]],
                },
              ],
            },
          ],
          limit: size,
          offset: (page - 1) * size,
          where: data,
        });
        node = JSON.parse(JSON.stringify(node));
        node.map((val, idx) => {
          console.log(val);
          node[idx].location =
            val.iot_node.tenantId == null && val.iot_node.internalId == null
              ? null
              : val.iot_node.iot_internal == null
              ? val.iot_node.iot_tenant.name
              : val.iot_node.iot_internal.name;
        });
        resolve({
          responseCode: 200,
          message: "sukses get data node steal",
          page: page,
          size: size,
          total: count,
          payload: node,
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          message: error.message,
        });
      }
    });
  },
};

const models = require("../../../models/index");
const { Op } = models.Sequelize;
const moment = require("moment");
module.exports = {
  svcChartLast7days: (data, type) => {
    return new Promise((resolve, reject) => {
      return models.sequelize.transaction((t) => {
        return models.iots_charts_usage
          .findAll({
            where: [
              [1, 2, 3, 7].includes(parseInt(data.typeId)) == false
                ? type
                : data,
              {
                [Op.and]: [
                  {
                    fdate: {
                      [Op.lte]: moment()
                        .utcOffset("+0700")
                        .format("YYYY-MM-DD"),
                    },
                  },
                  {
                    fdate: {
                      [Op.gt]: moment()
                        .utcOffset("+0700")
                        .subtract(7, "day")
                        .format("YYYY-MM-DD"),
                    },
                  },
                ],
              },
            ],
            transaction: t,
          })
          .then((nodes) => {
            nodes = JSON.parse(JSON.stringify(nodes));
            let tgl_end = moment().utcOffset("+0700");
            let tgl_start = moment().utcOffset("+0700").subtract(6, "day");

            const getDateArray = (first, end) => {
              let arr = [];
              let dt = new Date(first);
              while (dt <= end) {
                arr.push(moment(new Date(dt)).format("YYYY-MM-DD"));
                dt.setDate(dt.getDate() + 1);
              }
              return arr;
            };
            let tgl = getDateArray(tgl_start, tgl_end);
            let template = [];
            tgl.map((val) => {
              template.push({ date: val, usage: 0 });
            });
            console.log(template);
            if (nodes != null) {
              nodes.map((val, idx) => {
                let index = template.findIndex((valP) => {
                  return valP.date == val.fdate;
                });
                if ([1, 2, 3, 7].includes(parseInt(data.typeId)) == false) {
                  template[index].usage += parseFloat(val.usage);
                } else {
                  template[index].usage += parseFloat(val.usage) / 1000;
                }
              });
              return template;
            } else {
              return template;
            }
          })
          .then((_result) => {
            resolve({
              responseCode: 200,
              payload: _result,
            });
          })
          .catch((_error) => {
            console.log(_error);
            reject({
              responseCode: 400,
              message: _error.message,
            });
          });
      });
    });
  },
};

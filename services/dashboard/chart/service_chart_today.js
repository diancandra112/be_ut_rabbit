const models = require("../../../models/index");
const { Op } = models.Sequelize;
const moment = require("moment");

const queryLastReported = async (data) => {
  await models.iot_histories
    .findOne({
      where: {
        device_id: data.device_id,
        reportTime: models.sequelize.where(
          models.sequelize.fn(
            "date",
            models.sequelize.col("iot_histories.reportTimeFlag")
          ),
          "<",
          moment(data.reportTimeFlag).utcOffset("+0700").format("YYYY-MM-DD")
        ),
      },
      order: [["reportTimeFlag", "DESC"]],
    })
    .then((response) => {
      response = JSON.parse(JSON.stringify(response));
      return response;
    });
};
module.exports = {
  svcCartNodeDays: (data, type) => {
    return new Promise((resolve, reject) => {
      return models.sequelize.transaction((t) => {
        return models.iots_charts_usage
          .findAll({
            where: [
              [1, 2, 3, 7].includes(parseInt(data.typeId)) == false
                ? type
                : data,
              { fdate: moment().utcOffset("+0700").format("YYYY-MM-DD") },
            ],
            transaction: t,
          })
          .then((nodes) => {
            nodes = JSON.parse(JSON.stringify(nodes));
            let final = [];
            if (nodes != null) {
              let template = [
                { hours: "00", usage: 0 },
                { hours: "01", usage: 0 },
                { hours: "02", usage: 0 },
                { hours: "03", usage: 0 },
                { hours: "04", usage: 0 },
                { hours: "05", usage: 0 },
                { hours: "06", usage: 0 },
                { hours: "07", usage: 0 },
                { hours: "08", usage: 0 },
                { hours: "09", usage: 0 },
                { hours: "10", usage: 0 },
                { hours: "11", usage: 0 },
                { hours: "12", usage: 0 },
                { hours: "13", usage: 0 },
                { hours: "14", usage: 0 },
                { hours: "15", usage: 0 },
                { hours: "16", usage: 0 },
                { hours: "17", usage: 0 },
                { hours: "18", usage: 0 },
                { hours: "19", usage: 0 },
                { hours: "20", usage: 0 },
                { hours: "21", usage: 0 },
                { hours: "22", usage: 0 },
                { hours: "23", usage: 0 },
              ];
              if ([1, 2, 3, 7].includes(parseInt(data.typeId)) == false) {
                nodes.map((val, idx) => {
                  template[parseInt(val.fhour)].usage += parseFloat(val.usage);
                });
              } else {
                nodes.map((val, idx) => {
                  template[parseInt(val.fhour)].usage +=
                    parseFloat(val.usage) / 1000;
                });
              }
              return template;
            } else {
              return [
                { hours: "00", usage: 0 },
                { hours: "01", usage: 0 },
                { hours: "02", usage: 0 },
                { hours: "03", usage: 0 },
                { hours: "04", usage: 0 },
                { hours: "05", usage: 0 },
                { hours: "06", usage: 0 },
                { hours: "07", usage: 0 },
                { hours: "08", usage: 0 },
                { hours: "09", usage: 0 },
                { hours: "10", usage: 0 },
                { hours: "11", usage: 0 },
                { hours: "12", usage: 0 },
                { hours: "13", usage: 0 },
                { hours: "14", usage: 0 },
                { hours: "15", usage: 0 },
                { hours: "16", usage: 0 },
                { hours: "17", usage: 0 },
                { hours: "18", usage: 0 },
                { hours: "19", usage: 0 },
                { hours: "20", usage: 0 },
                { hours: "21", usage: 0 },
                { hours: "22", usage: 0 },
                { hours: "23", usage: 0 },
              ];
            }
          })
          .then((_result) => {
            resolve({
              responseCode: 200,
              payload: _result,
            });
          })
          .catch((_error) => {
            console.log(_error);
            reject({
              responseCode: 400,
              message: _error.message,
            });
          });
      });
    });
  },
};

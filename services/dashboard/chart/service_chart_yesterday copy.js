const models = require("../../../models/index");
const { Op } = models.Sequelize;
const moment = require("moment");
module.exports = {
  svcCartNodeYesterday: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize.transaction((t) => {
        return models.iot_nodes
          .findAll({
            where: data,
            attributes: [["id", "device_id"]],
            transaction: t,
          })
          .then((nodes) => {
            nodes = JSON.parse(JSON.stringify(nodes));
            if (nodes.length == 0) {
              return null;
            } else {
              let ArrayNode = [];
              nodes.map((val) => {
                ArrayNode.push(val.device_id);
              });
              return models.iot_histories
                .findAll({
                  where: {
                    device_id: { [Op.in]: ArrayNode },
                    reportTime: models.sequelize.where(
                      models.sequelize.fn(
                        "date",
                        models.sequelize.col("iot_histories.reportTimeFlag")
                      ),
                      "=",
                      moment()
                        .utcOffset("+0700")
                        .subtract(1, "day")
                        .format("YYYY-MM-DD")
                    ),
                  },
                  order: [["reportTimeFlag", "ASC"]],
                  transaction: t,
                })
                .then((final) => {
                  final = JSON.parse(JSON.stringify(final));
                  let type = data.typeId;
                  let device = ArrayNode;
                  let date = [];
                  for (let index = 0; index < 24; index++) {
                    date.push({
                      hours: ("00" + index.toString()).slice(-2),
                      usage: 0,
                    });
                  }
                  return models.iot_histories
                    .findAll({
                      LIMIT: 1,
                      where: {
                        device_id: { [Op.in]: ArrayNode },
                        reportTime: models.sequelize.where(
                          models.sequelize.fn(
                            "date",
                            models.sequelize.col("iot_histories.reportTimeFlag")
                          ),
                          "<",
                          moment()
                            .utcOffset("+0700")
                            .subtract(1, "day")
                            .format("YYYY-MM-DD")
                        ),
                      },
                      order: [["reportTimeFlag", "DESC"]],
                    })
                    .then((_last) => {
                      _last = JSON.parse(JSON.stringify(_last));
                      device.map((val1, idx1) => {
                        let newData = final.filter((valP) => {
                          return valP.device_id == val1;
                        });
                        let last_report = _last.filter((valP) => {
                          return valP.device_id == val1;
                        });
                        last_report = last_report[0];
                        newData.map((valNewData, idxNewData) => {
                          if (idxNewData == 0) {
                            if (
                              last_report == null ||
                              last_report == undefined
                            ) {
                              newData[0].usages =
                                parseFloat(
                                  JSON.parse(newData[0].payload).meter
                                    .meterReading
                                ) -
                                parseFloat(
                                  JSON.parse(newData[0].payload).meter
                                    .meterReading
                                );
                            } else {
                              newData[0].usages =
                                parseFloat(
                                  JSON.parse(newData[0].payload).meter
                                    .meterReading
                                ) -
                                parseFloat(
                                  JSON.parse(last_report.payload).meter
                                    .meterReading
                                );
                            }
                          } else {
                            newData[idxNewData].usages =
                              parseFloat(
                                JSON.parse(newData[idxNewData].payload).meter
                                  .meterReading
                              ) -
                              parseFloat(
                                JSON.parse(newData[idxNewData - 1].payload)
                                  .meter.meterReading
                              );
                          }
                        });
                        for (let index = 0; index < 24; index++) {
                          let newDatatgl = newData.filter((valP) => {
                            return (
                              parseInt(
                                moment(valP.reportTimeFlag).format("HH")
                              ) == index
                            );
                          });
                          if (newDatatgl.length > 0) {
                            let usageTemp = 0;
                            newDatatgl.map((value) => {
                              usageTemp += isNaN(parseFloat(value.usages))
                                ? 0
                                : parseFloat(value.usages);
                            });
                            date[index].usage += usageTemp / 1000;
                          }
                        }
                      });

                      return date;
                    });
                });
            }
          })
          .then((_result) => {
            resolve({
              responseCode: 200,
              payload: _result,
            });
          })
          .catch((_error) => {
            console.log(_error);
            reject({
              responseCode: 400,
              message: _error.message,
            });
          });
      });
    });
  },
};

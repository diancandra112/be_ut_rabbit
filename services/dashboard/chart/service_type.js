const models = require("../../../models/index");
const { Op } = models.Sequelize;
const jwt = require("jsonwebtoken");
require("dotenv").config();

module.exports = {
  serviceGetType: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        let node = await models.iot_nodes.findAll({
          where: [data, { typeId: { [Op.in]: [1, 2, 3, 4, 7] } }],
        });
        node = JSON.parse(JSON.stringify(node));
        let type = [];
        let rtu = [];
        node.map((val, idx) => {
          if (val.typeId == 4 && val.field_billing_rtu != null) {
            let billing = val.field_billing_rtu.match(/\(([^)]+)\)/);
            if (billing != null) {
              let index = rtu.findIndex((Valp) => Valp.satuan == billing[1]);
              console.log(rtu, index);
              if (index < 0) {
                rtu.push({
                  id: 100,
                  type_name: `${val.rtu_pricing_name}-${billing[1]}`,
                  type_id: 100,
                  active: true,
                  satuan: billing[1],
                  createdAt: "2020-03-11T12:33:43.000Z",
                  updatedAt: "2020-03-11T12:33:43.000Z",
                });
              }
            }
          } else {
            type.push(val.typeId);
          }
        });

        let list_node = await models.iot_node_type.findAll({
          where: [{ type_id: { [Op.in]: type } }],
          order: [["type_id"]],
        });
        list_node = [...list_node, ...rtu];
        list_node = list_node.filter((valP) => valP.type_id != 4);
        resolve({
          responseCode: 200,
          list_type: list_node,
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          messages: error,
        });
      }
    });
  },
  serviceGetTypeCount: (data, id) => {
    return new Promise((resolve, reject) => {
      models.iot_nodes
        .count({
          where: [{ typeId: { [Op.in]: id } }, data],
        })
        .then((result) => {
          result = JSON.parse(JSON.stringify(result));
          let msg = false;
          if (result > 0) {
            msg = true;
          }
          resolve({
            responseCode: 200,
            messages: msg,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error,
          });
        });
    });
  },
  serviceGetTypeGas: (data) => {
    return new Promise((resolve, reject) => {
      models.iot_nodes
        .count({
          where: [{ typeId: { [Op.in]: [1] } }, data],
        })
        .then((result) => {
          result = JSON.parse(JSON.stringify(result));
          let msg = false;
          if (result > 0) {
            msg = true;
          }
          resolve({
            responseCode: 200,
            messages: msg,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error,
          });
        });
    });
  },
  serviceGetTypeWaterLevel: (data) => {
    return new Promise((resolve, reject) => {
      models.iot_nodes
        .count({
          where: [{ typeId: { [Op.in]: [13] } }, data],
        })
        .then((result) => {
          result = JSON.parse(JSON.stringify(result));
          let msg = false;
          if (result > 0) {
            msg = true;
          }
          resolve({
            responseCode: 200,
            messages: msg,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error,
          });
        });
    });
  },
};

const models = require("../../models/index");
const moment = require("moment");
const { Op } = models.Sequelize;
module.exports = {
  svcUnsignedNodeCount: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize.transaction((t) => {
        return models.iot_nodes
          .count({ where: [data, { is_unsigned: false }] })
          .then((_result) => {
            resolve({
              responseCode: 200,
              message: "sukses get data node unsigned",
              payload: _result,
            });
          })
          .catch((_error) => {
            reject({
              responseCode: 400,
              message: _error.message,
            });
          });
      });
    });
  },
  svcUnsignedNodeList: (data, page, size) => {
    return new Promise((resolve, reject) => {
      return models.sequelize.transaction((t) => {
        return models.iot_nodes
          .findAndCountAll({
            limit: size,
            offset: (page - 1) * size,
            where: [data, { is_unsigned: false }],
            include: [{ model: models.iot_node_type }],
          })
          .then((_result) => {
            resolve({
              responseCode: 200,
              message: "sukses get data node unsigned",
              page: page,
              size: size,
              total: _result.count,
              payload: _result.rows,
            });
          })
          .catch((_error) => {
            reject({
              responseCode: 400,
              message: _error.message,
            });
          });
      });
    });
  },

  svcWaterLevel: (
    data,
    page,
    size,
    search,
    sort,
    order,
    start_date,
    end_date
  ) => {
    return new Promise((resolve, reject) => {
      let date = {};
      if (start_date != null || end_date != null) {
        date = {
          [Op.and]: [
            {
              update_time: {
                [Op.gte]: moment(start_date).format("YYYY-MM-DD"),
              },
            },
            {
              update_time: {
                [Op.lte]: moment(end_date).format("YYYY-MM-DD"),
              },
            },
          ],
        };
      }
      return models.sequelize
        .transaction((t) => {
          return models.iots_alarm_water_level
            .count({
              where: [
                { [Op.or]: { devEui: { [Op.substring]: search } } },
                data,
                date,
              ],
            })
            .then((_result_count) => {
              _result_count = JSON.parse(JSON.stringify(_result_count));
              return models.iots_alarm_water_level
                .findAll({
                  limit: size,
                  offset: (page - 1) * size,
                  where: [
                    { [Op.or]: { devEui: { [Op.substring]: search } } },
                    data,
                    date,
                  ],
                  order: [[sort, order]],
                  include: [
                    {
                      model: models.iot_area,
                      attributes: ["area_name"],
                    },
                  ],
                })
                .then((_result_find) => {
                  _result_find = JSON.parse(JSON.stringify(_result_find));
                  _result_find.map((val, idx) => {
                    _result_find[idx].grafik = JSON.parse(
                      _result_find[idx].grafik
                    );
                  });
                  return { count: _result_count, rows: _result_find };
                });
            });
        })
        .then((_result) => {
          resolve({
            responseCode: 200,
            message: "sukses get data node unsigned",
            page: page,
            size: size,
            total: _result.count,
            payload: _result.rows,
          });
        })
        .catch((_error) => {
          reject({
            responseCode: 400,
            message: _error.message,
          });
        });
    });
  },
};

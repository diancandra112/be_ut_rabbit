const models = require("../../models/index");
const { Op } = models.Sequelize;
const moment = require("moment");
module.exports = {
  svcAssignedNodeCount: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize.transaction((t) => {
        return models.iot_nodes
          .count({
            where: [
              {
                is_unsigned: false,
                [Op.or]: [
                  { tenantId: { [Op.not]: null } },
                  { internalId: { [Op.not]: null } },
                ],
              },
              data,
            ],
          })
          .then((_result) => {
            resolve({
              responseCode: 200,
              message: "sukses get data node assigned",
              payload: _result,
            });
          })
          .catch((_error) => {
            reject({
              responseCode: 400,
              message: _error.message,
            });
          });
      });
    });
  },
  svcAssignedNodeList: (
    data,
    page,
    size,
    tenant,
    is_mobile,
    search,
    sort,
    order,
    start_date,
    end_date
  ) => {
    return new Promise((resolve, reject) => {
      let condition = {};

      let date = {};
      if (start_date != null || end_date != null) {
        date = {
          [Op.and]: [
            {
              last_update1: models.sequelize.where(
                models.sequelize.fn(
                  "date",
                  models.sequelize.col("iot_nodes.last_update")
                ),
                ">=",
                moment(start_date).format("YYYY-MM-DD")
              ),
            },
            {
              last_update2: models.sequelize.where(
                models.sequelize.fn(
                  "date",
                  models.sequelize.col("iot_nodes.last_update")
                ),
                "<=",
                moment(end_date).format("YYYY-MM-DD")
              ),
            },
          ],
        };
      }
      if (!is_mobile) {
        if (parseInt(tenant) == 1) {
          condition = { tenantId: { [Op.not]: null } };
        } else {
          condition = { internalId: { [Op.not]: null } };
        }
      } else {
        condition = {
          tenantId: { [Op.not]: null },
          internalId: { [Op.not]: null },
        };
      }

      return models.sequelize.transaction((t) => {
        return models.iot_nodes
          .findAndCountAll({
            order: [[sort, order]],
            limit: size,
            offset: (page - 1) * size,
            where: [
              {
                is_unsigned: false,
                [Op.or]: condition,
                devEui: { [Op.substring]: search },
              },
              data,
              date,
            ],
            include: [
              { model: models.iot_tenant, attributes: ["tenant_name"] },
              { model: models.iot_internal, attributes: ["internal_name"] },
              { model: models.iot_detail_water },
              { model: models.iot_detail_gas, as: "iot_detail_gas" },
              { model: models.iot_detail_electric },
              { model: models.iot_detail_electricct },
              { model: models.iot_node_type },
              { model: models.iot_area, attributes: ["id", "area_name"] },
            ],
          })
          .then((_result) => {
            _result = JSON.parse(JSON.stringify(_result));
            _result.rows.map((val, idx) => {
              if (is_mobile && val.iot_tenant == null) {
                _result.rows[idx].iot_tenant = {
                  tenant_name: val.iot_internal.internal_name,
                };
              }
              switch (parseInt(val.typeId)) {
                case 1:
                  _result.rows[idx].meter_id =
                    _result.rows[idx].iot_detail_gas.meter_id;
                  delete _result.rows[idx].iot_detail_gas;
                  delete _result.rows[idx].iot_detail_water;
                  delete _result.rows[idx].iot_detail_electric;
                  delete _result.rows[idx].iot_detail_electricct;
                  break;
                case 2:
                  _result.rows[idx].meter_id =
                    _result.rows[idx].iot_detail_water.meter_id;

                  delete _result.rows[idx].iot_detail_gas;
                  delete _result.rows[idx].iot_detail_water;
                  delete _result.rows[idx].iot_detail_electric;
                  delete _result.rows[idx].iot_detail_electricct;
                  break;
                case 3:
                  _result.rows[idx].meter_id =
                    _result.rows[idx].iot_detail_electric.meter_id;

                  delete _result.rows[idx].iot_detail_gas;
                  delete _result.rows[idx].iot_detail_water;
                  delete _result.rows[idx].iot_detail_electric;
                  delete _result.rows[idx].iot_detail_electricct;
                  break;
                case 7:
                  _result.rows[idx].meter_id =
                    _result.rows[idx].iot_detail_electricct.meter_id;

                  delete _result.rows[idx].iot_detail_gas;
                  delete _result.rows[idx].iot_detail_water;
                  delete _result.rows[idx].iot_detail_electric;
                  delete _result.rows[idx].iot_detail_electricct;
                  break;

                default:
                  break;
              }
            });
            resolve({
              responseCode: 200,
              message: "sukses get data node assigned",
              page: page,
              size: size,
              total: _result.count,
              payload: _result.rows,
            });
          })
          .catch((_error) => {
            console.log(_error);
            reject({
              responseCode: 400,
              message: _error.message,
            });
          });
      });
    });
  },
};

const models = require("../../models/index");
const { Op } = models.Sequelize;
const moment = require("moment");
module.exports = {
  svcOfflineNodeList: (
    data,
    page,
    size,
    tenant,
    search,
    sort,
    order,
    start_date,
    end_date
  ) => {
    return new Promise(async (resolve, reject) => {
      try {
        let condition = {};
        let date = {};
        if (start_date != null || end_date != null) {
          date = {
            [Op.and]: [
              {
                last_update1: models.sequelize.where(
                  models.sequelize.fn(
                    "date",
                    models.sequelize.col("iot_nodes.last_update")
                  ),
                  ">=",
                  moment(start_date).format("YYYY-MM-DD")
                ),
              },
              {
                last_update2: models.sequelize.where(
                  models.sequelize.fn(
                    "date",
                    models.sequelize.col("iot_nodes.last_update")
                  ),
                  "<=",
                  moment(end_date).format("YYYY-MM-DD")
                ),
              },
            ],
          };
        }
        if (parseInt(tenant) == 1) {
          condition = { tenantId: { [Op.not]: null } };
        } else {
          condition = { internalId: { [Op.not]: null } };
        }
        let count = await models.iot_nodes.count({
          where: [
            data,
            condition,
            {
              devEui: { [Op.substring]: search },
              is_unsigned: false,
              [Op.and]: [
                {
                  [Op.or]: [
                    { tenantId: { [Op.not]: null } },
                    { internalId: { [Op.not]: null } },
                  ],
                },
                {
                  [Op.or]: [
                    { last_update: null },
                    {
                      col1: models.sequelize.where(
                        models.sequelize.fn(
                          "TIMESTAMPDIFF",
                          models.sequelize.literal("minute"),
                          models.sequelize.col("iot_nodes.last_update"),
                          moment().utcOffset("+0800").format()
                        ),
                        ">=",
                        models.sequelize.literal("`setting_interval`*4")
                      ),
                    },
                  ],
                },
              ],
            },
            date,
          ],
        });
        let dataOffline = await models.iot_nodes.findAll({
          order: [[sort, order]],
          limit: size,
          offset: (page - 1) * size,
          where: [
            data,
            condition,
            {
              devEui: { [Op.substring]: search },
              is_unsigned: false,
              [Op.and]: [
                {
                  [Op.or]: [
                    { tenantId: { [Op.not]: null } },
                    { internalId: { [Op.not]: null } },
                  ],
                },
                {
                  [Op.or]: [
                    { last_update: null },
                    {
                      col1: models.sequelize.where(
                        models.sequelize.fn(
                          "TIMESTAMPDIFF",
                          models.sequelize.literal("minute"),
                          models.sequelize.col("iot_nodes.last_update"),
                          moment().utcOffset("+0800").format()
                        ),
                        ">=",
                        models.sequelize.literal("`setting_interval`*4")
                      ),
                    },
                  ],
                },
              ],
            },
            date,
          ],
          include: [
            { model: models.iot_tenant, attributes: ["tenant_name"] },
            { model: models.iot_internal, attributes: ["internal_name"] },
            { model: models.iot_detail_water },
            { model: models.iot_detail_gas, as: "iot_detail_gas" },
            { model: models.iot_detail_electric },
            { model: models.iot_detail_electricct },
            { model: models.iot_node_type },
            { model: models.iot_area },
          ],
        });
        dataOffline.map((val, idx) => {
          switch (parseInt(val.typeId)) {
            case 1:
              dataOffline[idx].meter_id =
                dataOffline[idx].iot_detail_gas.meter_id;
              delete dataOffline[idx].iot_detail_gas;
              delete dataOffline[idx].iot_detail_water;
              delete dataOffline[idx].iot_detail_electric;
              delete dataOffline[idx].iot_detail_electricct;
              break;
            case 2:
              dataOffline[idx].meter_id =
                dataOffline[idx].iot_detail_water.meter_id;

              delete dataOffline[idx].iot_detail_gas;
              delete dataOffline[idx].iot_detail_water;
              delete dataOffline[idx].iot_detail_electric;
              delete dataOffline[idx].iot_detail_electricct;
              break;
            case 3:
              dataOffline[idx].meter_id =
                dataOffline[idx].iot_detail_electric.meter_id;

              delete dataOffline[idx].iot_detail_gas;
              delete dataOffline[idx].iot_detail_water;
              delete dataOffline[idx].iot_detail_electric;
              delete dataOffline[idx].iot_detail_electricct;
              break;
            case 7:
              dataOffline[idx].meter_id =
                dataOffline[idx].iot_detail_electricct.meter_id;

              delete dataOffline[idx].iot_detail_gas;
              delete dataOffline[idx].iot_detail_water;
              delete dataOffline[idx].iot_detail_electric;
              delete dataOffline[idx].iot_detail_electricct;
              break;

            default:
              break;
          }
        });
        resolve({
          responseCode: 200,
          data_offline: dataOffline,
          page: page,
          size: size,
          total: count,
        });
      } catch (error) {
        reject({
          responseCode: 400,
          message: error.message,
        });
      }
    });
  },
  svcOfflineNode: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_nodes.count({
            where: [
              data,
              {
                is_unsigned: false,
                [Op.and]: [
                  {
                    [Op.or]: [
                      { tenantId: { [Op.not]: null } },
                      { internalId: { [Op.not]: null } },
                    ],
                  },
                  {
                    [Op.or]: [
                      { last_update: null },
                      {
                        col1: models.sequelize.where(
                          models.sequelize.fn(
                            "TIMESTAMPDIFF",
                            models.sequelize.literal("minute"),
                            models.sequelize.col("iot_nodes.last_update"),
                            moment().utcOffset("+0800").format()
                          ),
                          ">=",
                          models.sequelize.literal("`setting_interval`*4")
                        ),
                      },
                    ],
                  },
                ],
              },
            ],
            transaction: t,
          });
        })
        .then((_result) => {
          resolve({
            responseCode: 200,
            total_offline: _result,
          });
        })
        .catch((_error) => {
          reject({
            responseCode: 400,
            message: _error.message,
          });
        });
    });
  },
};

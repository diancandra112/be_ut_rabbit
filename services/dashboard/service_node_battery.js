const moment = require("moment");
const models = require("../../models/index");
const { Op } = models.Sequelize;
module.exports = {
  svcNodeBatteryCount: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize.transaction((t) => {
        return models.iot_nodes
          .count({
            where: [
              {
                is_unsigned: false,
                typeId: { [Op.notIn]: [3, 7] },
                [Op.or]: [
                  {
                    tenantId: { [Op.not]: null },
                    live_battery: { [Op.not]: null },
                  },
                  {
                    internalId: { [Op.not]: null },
                    live_battery: { [Op.not]: null },
                  },
                ],
                live_battery: { [Op.lt]: 50 },
              },
              data,
            ],
          })
          .then((_result) => {
            resolve({
              responseCode: 200,
              message: "sukses get data node low battery",
              payload: _result,
            });
          })
          .catch((_error) => {
            console.log(_error);
            reject({
              responseCode: 400,
              message: _error.message,
            });
          });
      });
    });
  },
  svcNodeBatteryList: (
    data,
    page,
    size,
    tenant,
    search,
    sort,
    order,
    start_date,
    end_date
  ) => {
    return new Promise((resolve, reject) => {
      let condition = {};

      let date = {};
      if (start_date != null || end_date != null) {
        date = {
          [Op.and]: [
            {
              last_update1: models.sequelize.where(
                models.sequelize.fn(
                  "date",
                  models.sequelize.col("iot_nodes.last_update")
                ),
                ">=",
                moment(start_date).format("YYYY-MM-DD")
              ),
            },
            {
              last_update2: models.sequelize.where(
                models.sequelize.fn(
                  "date",
                  models.sequelize.col("iot_nodes.last_update")
                ),
                "<=",
                moment(end_date).format("YYYY-MM-DD")
              ),
            },
          ],
        };
      }
      if (parseInt(tenant) == 1) {
        condition = {
          tenantId: { [Op.not]: null },
          live_battery: { [Op.not]: null },
        };
      } else {
        condition = {
          internalId: { [Op.not]: null },
          live_battery: { [Op.not]: null },
        };
      }

      return models.sequelize.transaction((t) => {
        return models.iot_nodes
          .findAndCountAll({
            order: [[sort, order]],
            limit: size,
            offset: (page - 1) * size,
            include: [
              { model: models.iot_area },
              { model: models.iot_node_type },
              { model: models.iot_tenant },
              { model: models.iot_internal },
            ],
            where: [
              {
                devEui: { [Op.substring]: search },
                is_unsigned: false,
                typeId: { [Op.notIn]: [3, 7] },
                [Op.or]: [condition],
                live_battery: { [Op.lt]: 50 },
              },
              data,
              date,
            ],
          })
          .then((_result) => {
            resolve({
              responseCode: 200,
              message: "sukses get data node assigned",
              page: page,
              size: size,
              total: _result.count,
              payload: _result.rows,
            });
          })
          .catch((_error) => {
            reject({
              responseCode: 400,
              message: _error.message,
            });
          });
      });
    });
  },
};

const models = require("../../models/index");
const { Op } = models.Sequelize;
const moment = require("moment");
module.exports = {
  svcNodeAnomaliUsageChart: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_anomali_history
            .findOne({
              where: data,
              transaction: t,
              include: [
                { model: models.iot_nodes, attributes: ["id", "typeId"] },
              ],
            })
            .then((_result) => {
              _result = JSON.parse(JSON.stringify(_result));
              if (_result == null) {
                return [];
              }
              return models.iots_charts_usage
                .findAll({
                  attributes: ["fdate", "fhour", "usage"],
                  limit: 5,
                  order: [["id", "DESC"]],
                  where: {
                    nodeId: _result.iot_node.id,
                    fdate: {
                      [Op.lte]: moment(_result.last_update)
                        .utcOffset("+0700")
                        .format("YYYY-MM-DD"),
                    },
                    fhour: {
                      [Op.lt]: moment(_result.last_update)
                        .utcOffset("+0700")
                        .format("HH"),
                    },
                  },
                  transaction: t,
                })
                .then((_result_usage) => {
                  _result_usage = JSON.parse(JSON.stringify(_result_usage));
                  _result_usage.map((val, idx) => {
                    _result_usage[idx].usage = val.usage / 1000;
                  });

                  function compare(a, b) {
                    if (a.fhour < b.fhour) {
                      return -1;
                    }
                    if (a.fhour > b.fhour) {
                      return 1;
                    }
                    return 0;
                  }
                  _result_usage = _result_usage.sort(compare);
                  _result_usage.push({
                    fdate: moment(_result.last_update)
                      .utcOffset("+0700")
                      .format("YYYY-MM-DD"),
                    fhour: moment(_result.last_update)
                      .utcOffset("+0700")
                      .format("HH"),
                    usage: parseFloat(_result.usage),
                  });
                  return _result_usage;
                });
            });
        })
        .then((_result) => {
          resolve({
            responseCode: 200,
            message: "Sukses Update Status",
            _result: _result,
          });
        })
        .catch((_error) => {
          reject({
            responseCode: 400,
            message: _error.message,
          });
        });
    });
  },
  svcNodeAnomaliCloseStatusAll: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_anomali_history.update(
            { status: false },
            {
              where: data,
              transaction: t,
            }
          );
        })
        .then((_result) => {
          resolve({
            responseCode: 200,
            message: "Sukses Update Status",
          });
        })
        .catch((_error) => {
          reject({
            responseCode: 400,
            message: _error.message,
          });
        });
    });
  },
  svcNodeAnomaliCloseStatus: (data, page, size, tenant) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_anomali_history.update(
            { status: false },
            {
              where: data,
              transaction: t,
            }
          );
        })
        .then((_result) => {
          resolve({
            responseCode: 200,
            message: "Sukses Update Status",
          });
        })
        .catch((_error) => {
          reject({
            responseCode: 400,
            message: _error.message,
          });
        });
    });
  },
  svcNodeAnomaliClose: (data, page, size, tenant) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_anomali_history.destroy({
            where: data,
            transaction: t,
          });
        })
        .then((_result) => {
          resolve({
            responseCode: 200,
            message: "Sukses Delete Data",
          });
        })
        .catch((_error) => {
          reject({
            responseCode: 400,
            message: _error.message,
          });
        });
    });
  },
  svcNodeAnomaliListTime: (
    data,
    page,
    size,
    search,
    sort,
    order,
    start_date,
    end_date
  ) => {
    return new Promise((resolve, reject) => {
      let date = {};
      if (start_date != null || end_date != null) {
        date = {
          [Op.and]: [
            {
              last_update1: models.sequelize.where(
                models.sequelize.fn(
                  "date",
                  models.sequelize.col("iots_anomali_history.last_update")
                ),
                ">=",
                moment(start_date).format("YYYY-MM-DD")
              ),
            },
            {
              last_update2: models.sequelize.where(
                models.sequelize.fn(
                  "date",
                  models.sequelize.col("iots_anomali_history.last_update")
                ),
                "<=",
                moment(end_date).format("YYYY-MM-DD")
              ),
            },
          ],
        };
      }
      return models.sequelize
        .transaction((t) => {
          return models.iots_anomali_history
            .count({
              where: [data, date],
              transaction: t,
            })
            .then((count) => {
              count = JSON.parse(JSON.stringify(count));
              return models.iots_anomali_history
                .findAll({
                  where: [data, date],
                  order: [[sort, order]],
                  limit: size,
                  offset: (page - 1) * size,
                  transaction: t,
                  include: [
                    // {
                    //   model: models.iot_nodes,
                    //   attributes: [
                    //     "minim_balance_gas",
                    //     "minim_balance_water",
                    //   ],
                    // },
                    { model: models.iot_node_type },
                    { model: models.iot_tenant },
                    { model: models.iot_internal },
                    { model: models.iot_area },
                  ],
                })
                .then((result) => {
                  result = JSON.parse(JSON.stringify(result));
                  return { rows: result, count: count };
                });
            });
        })
        .then((_result) => {
          resolve({
            responseCode: 200,
            page: page,
            size: size,
            total: _result.count,
            payload: _result.rows,
          });
        })
        .catch((_error) => {
          reject({
            responseCode: 400,
            message: _error.message,
          });
        });
    });
  },
  svcNodeAnomaliTimeCount: (data) => {
    return new Promise((resolve, reject) => {
      let where = `anomi_type='${data.anomi_type}'`;
      if (data.areaId !== undefined) {
        where = `areaId=${data.areaId} AND anomi_type='${data.anomi_type}'`;
      }
      return models.sequelize
        .transaction((t) => {
          console.log(data);
          return models.sequelize.query(`SELECT  status,COUNT(*) AS count 
          FROM iots_anomali_histories WHERE ${where}
          GROUP BY status`);
        })
        .then((_result) => {
          _result = JSON.parse(JSON.stringify(_result));
          _result = _result[0];
          if (_result.length == 1) {
            _result.push({
              status: parseInt(_result[0].status) == 1 ? 0 : 1,
              count: 0,
            });
          } else if (_result.length == 0) {
            _result.push(
              {
                status: 1,
                count: 0,
              },
              {
                status: 0,
                count: 0,
              }
            );
          }
          let output = { true: 0, false: 0 };
          _result.map((val, idx) => {
            if (parseInt(val.status) == 1) {
              output.true = val.count;
            } else {
              output.false = val.count;
            }
          });
          resolve({
            responseCode: 200,
            payload: output,
          });
        })
        .catch((_error) => {
          reject({
            responseCode: 400,
            message: _error.message,
          });
        });
    });
  },
  svcNodeAnomaliList: (
    data,
    page,
    size,
    tenant,
    search,
    sort,
    order,
    start_date,
    end_date
  ) => {
    return new Promise((resolve, reject) => {
      let condition = {};
      let date = {};
      if (start_date != null || end_date != null) {
        date = {
          [Op.and]: [
            {
              last_update1: models.sequelize.where(
                models.sequelize.fn(
                  "date",
                  models.sequelize.col("iot_nodes.last_update")
                ),
                ">=",
                moment(start_date).format("YYYY-MM-DD")
              ),
            },
            {
              last_update2: models.sequelize.where(
                models.sequelize.fn(
                  "date",
                  models.sequelize.col("iot_nodes.last_update")
                ),
                "<=",
                moment(end_date).format("YYYY-MM-DD")
              ),
            },
          ],
        };
      }
      if (parseInt(tenant) == 1) {
        condition = { tenantId: { [Op.not]: null } };
      } else {
        condition = { internalId: { [Op.not]: null } };
      }
      return models.sequelize
        .transaction((t) => {
          return models.iot_nodes.findAndCountAll({
            include: [
              { model: models.iot_node_type },
              {
                model: models.iot_tenant,
                where: { tenant_close: false },
                required: true,
              },
              { model: models.iot_internal },
              { model: models.iot_area },
            ],
            limit: size,
            offset: (page - 1) * size,
            where: [
              {
                devEui: { [Op.substring]: search },
                is_unsigned: false,
                is_anomali: true,
                typeId: { [Op.in]: [1, 2, 3, 7] },
                [Op.or]: [condition],
              },
              data,
              date,
            ],
            order: [[sort, order]],
            transaction: t,
          });
        })
        .then((_result) => {
          resolve({
            responseCode: 200,
            page: page,
            size: size,
            total: _result.count,
            payload: _result.rows,
          });
        })
        .catch((_error) => {
          reject({
            responseCode: 400,
            message: _error.message,
          });
        });
    });
  },
  svcNodeAnomaliCount: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_nodes.count({
            where: [
              {
                is_unsigned: false,
                is_anomali: true,
                typeId: { [Op.in]: [1, 2, 3, 7] },
                [Op.or]: [
                  { tenantId: { [Op.not]: null } },
                  { internalId: { [Op.not]: null } },
                ],
              },
              data,
            ],
            transaction: t,
          });
        })
        .then((_result) => {
          resolve({
            responseCode: 200,
            data_anomali: _result,
          });
        })
        .catch((_error) => {
          reject({
            responseCode: 400,
            message: _error.message,
          });
        });
    });
  },
};

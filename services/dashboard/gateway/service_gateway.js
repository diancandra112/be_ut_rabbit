const models = require("../../../models/index");
const { Op } = models.Sequelize;
const moment = require("moment");
const { weiots } = require("../../../config/axios");
module.exports = {
  SvcGatewayCheck: (data, newDate) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_gateways
            .findAll({
              attributes: ["mac_address", "area_id"],
              transaction: t,
            })
            .then((res) => {
              res = JSON.parse(JSON.stringify(res));
              let output = [];
              res.map((val) => {
                if (val.mac_address != null) {
                  output.push(val.mac_address);
                }
              });
              return weiots({
                method: "post",
                url: "/sindcond/gateway/get",
                data: { data: output },
              }).then(async (response) => {
                let AxiosResponse = response.data.message;
                AxiosResponse.map((val, index) => {
                  if (val.last_update == null) {
                    AxiosResponse[index].status = 0;
                  } else {
                    const timeStart = moment(val.last_update)
                      .utcOffset("+0700")
                      .format("YYYY-MM-DD HH:mm:ss");
                    let duration = moment.duration(
                      moment().utcOffset("+0700").diff(timeStart)
                    );
                    duration = duration.asMinutes();
                    if (duration < 60) {
                      AxiosResponse[index].status = 1;
                      AxiosResponse[index].line_status = false;
                    } else {
                      AxiosResponse[index].status = 0;
                    }
                  }
                });
                let final = [];
                let update = [];
                res.map((val) => {
                  let mac = response.data.message.find((valP) => {
                    return valP.mac_address == val.mac_address;
                  });
                  if (mac != undefined) {
                    final.push({ ...mac, area_id: val.area_id });
                    update.push({
                      mac_address: val.mac_address,
                      last_update: mac.last_update,
                      offline: false,
                      status: 1,
                      line_status: true,
                    });
                  } else {
                    final.push({
                      mac_address: val.mac_address,
                      last_update: moment().utcOffset("+0700").format(),
                      offline: true,
                      status: 0,
                      line_status: false,
                      area_id: val.area_id,
                    });
                    update.push({
                      mac_address: val.mac_address,
                      offline: true,
                      status: 0,
                      line_status: false,
                    });
                  }
                });
                for (let i = 0; i < update.length; i++) {
                  await models.iot_gateways.update(update[i], {
                    where: { mac_address: update[i].mac_address },
                  });
                }
              });
            });
        })
        .then((res) => {
          resolve("ok");
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            message: "failed get prepaid type",
            error: err.message,
          });
        });
    });
  },
};

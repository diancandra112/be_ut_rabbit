const models = require("../../models/index");
const { Op } = models.Sequelize;
const moment = require("moment");
const { sequelize } = require("../../models/index");
module.exports = {
  svcNodeList: (data, page, size, tenant) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_nodes.findAll({
            where: [data],
            attributes: [
              "id",
              "areaId",
              "devEui",
              "description",
              "latitude",
              "longitude",
              "altitude",
              "merk",
            ],
            transaction: t,
          });
        })
        .then((_result) => {
          resolve({
            responseCode: 200,
            result: _result,
          });
        })
        .catch((_error) => {
          reject({
            responseCode: 400,
            message: _error.message,
          });
        });
    });
  },
  svcGatewayOnlineList: (
    data,
    page,
    size,
    tenant,
    search,
    sort,
    order,
    start_date,
    end_date
  ) => {
    return new Promise((resolve, reject) => {
      let date = {};
      if (start_date != null || end_date != null) {
        date = {
          [Op.and]: [
            {
              last_update1: models.sequelize.where(
                models.sequelize.fn(
                  "date",
                  models.sequelize.col("iot_gateways.last_update")
                ),
                ">=",
                moment(start_date).format("YYYY-MM-DD")
              ),
            },
            {
              last_update2: models.sequelize.where(
                models.sequelize.fn(
                  "date",
                  models.sequelize.col("iot_gateways.last_update")
                ),
                "<=",
                moment(end_date).format("YYYY-MM-DD")
              ),
            },
          ],
        };
      }
      return models.sequelize
        .transaction((t) => {
          return models.iot_gateways.findAndCountAll({
            order: [[sort, order]],
            limit: size,
            offset: (page - 1) * size,
            where: [
              {
                [Op.or]: {
                  gateway_name: { [Op.substring]: search },
                  mac_address: { [Op.substring]: search },
                  unit_model: { [Op.substring]: search },
                },
                status: true,
              },
              data,
              date,
            ],
            include: [
              { model: models.iot_area, attributes: ["id", "area_name"] },
            ],
            transaction: t,
          });
        })
        .then((_result) => {
          resolve({
            responseCode: 200,
            page: page,
            size: size,
            total: _result.count,
            payload: _result.rows,
          });
        })
        .catch((_error) => {
          reject({
            responseCode: 400,
            message: _error.message,
          });
        });
    });
  },
  svcGatewayOnlineCount: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_gateways.count({
            where: [
              {
                status: true,
              },
              data,
            ],
            transaction: t,
          });
        })
        .then((_result) => {
          resolve({
            responseCode: 200,
            data_anomali: _result,
          });
        })
        .catch((_error) => {
          console.log(_error);
          reject({
            responseCode: 400,
            message: _error.message,
          });
        });
    });
  },
};

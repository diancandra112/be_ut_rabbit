const { sequelize, Sequelize } = require("../../models/index");
const models = require("../../models/index");
const { Op } = Sequelize;
module.exports = {
  svcListMember: (data) => {
    return new Promise((resolve, reject) => {
      return sequelize.transaction((t) => {
        return models.iot_member_level
          .findAll({
            where: data,
            include: [
              {
                model: models.iot_area,
                include: [{ model: models.iot_pricing_postpaid_options }],
              },
            ],
            transaction: t,
          })
          .then((res) => {
            resolve({
              responseCode: 200,
              message: "sukses get member list",
              payload: res,
            });
          })
          .catch((err) => {
            reject({
              responseCode: 400,
              message: "failed get member list",
              error: err.message,
            });
          });
      });
    });
  },
};

const models = require("../models/index");
const axios = require("axios");
require("dotenv").config();

module.exports = {
  serviceGatewayDelete: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_gateways.destroy({
            where: data,
            transaction: t,
          });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: "Berhasil Delete gateway",
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error,
          });
        });
    });
  },
  serviceGatewayRegister: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_gateways
            .findOrCreate({
              where: {
                gateway_name: data.gateway_name,
                area_id: data.area_id,
              },
              defaults: data,
              transaction: t,
            })
            .then(([res, status]) => {
              console.log(status);
              if (status == false) {
                resolve({
                  responseCode: 400,
                  messages: "nama gateway telah digunakan",
                });
              }
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: "Berhasil mendaftar gateway",
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error,
          });
        });
    });
  },
  serviceGatewayGet: (data) => {
    return new Promise((resolve, reject) => {
      models.iot_gateways
        .findAll({
          where: data,
        })
        .then((result) => {
          result = JSON.parse(JSON.stringify(result));
          resolve({
            responseCode: 200,
            list_gateway: result,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error,
          });
        });
    });
  },
  serviceGatewayEdit: (data) => {
    return new Promise((resolve, reject) => {
      models.iot_gateways
        .update(data, {
          where: {
            id: data.id,
          },
        })
        .then((result) => {
          resolve({
            responseCode: 400,
            messages: "berhasil edit data",
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error,
          });
        });
    });
  },
  serviceCheckGateway: (data) => {
    return new Promise((resolve, reject) => {
      axios
        .get(process.env.HOST_SERVER + `/gateway?gateway_id=${data.gateway_id}`)
        .then((s) => {
          console.log(s);
          resolve({
            responseCode: 200,
            list_node: s.data,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            list_node: err.response.data,
          });
        });
    });
  },
};

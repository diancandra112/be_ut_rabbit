const models = require("../models/index");
require("dotenv").config();

module.exports = {
  serviceGetOnePriceForAll: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_pricing_postpaid_all.findAll({
            where: { areaId: data.area_id },
            include: [{ model: models.iot_node_type }],
          });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            response: result,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 401,
            messages: JSON.stringify(error),
          });
        });
    });
  },
  serviceGetAreaProfilePricing: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_area.findOne({
            where: { id: data.area_id },
            include: [{ model: models.iot_pricing_postpaid_options }],
          });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            response: result,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 401,
            messages: JSON.stringify(error),
          });
        });
    });
  },
  serviceOnePriceForAll: (data, area_id) => {
    return new Promise((resolve, reject) => {
      console.log(data);
      return models.sequelize
        .transaction((t) => {
          return models.iot_pricing_postpaid_all
            .destroy({
              where: { areaId: area_id },
            })
            .then((r) => {
              return models.iot_pricing_postpaid_all.bulkCreate(data);
            });
        })

        .then(() => {
          resolve({
            responseCode: 200,
            response: `berhasil update pricing`,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 401,
            messages: JSON.stringify(error),
          });
        });
    });
  },
  serviceOnePriceForAllBK: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_area
            .update(
              { pricing_option: data.pricing_option },
              { where: { id: data.area_id } }
            )
            .then(() => {
              delete data.pricing_option;
              return models.iot_pricing_postpaid_all
                .findOrCreate({
                  where: {
                    areaId: data.area_id,
                    typeId: data.typeId,
                  },
                  defaults: data,
                })
                .then(([result, status]) => {
                  console.log(data);
                  if (status == false) {
                    return models.iot_pricing_postpaid_all.update(data, {
                      where: {
                        areaId: data.area_id,
                        typeId: data.typeId,
                      },
                    });
                  }
                });
            });
        })
        .then(() => {
          resolve({
            responseCode: 200,
            response: `berhasil update pricing`,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 401,
            messages: JSON.stringify(error),
          });
        });
    });
  },
  servicePricingMemberLevel: (data) => {
    return new Promise((resolve, reject) => {
      models.iot_pricing_postpaid
        .findOrCreate({
          where: {
            typeId: data.typeId,
            memberId: data.memberId,
          },
          defaults: data,
        })
        .then(([result, status]) => {
          if (status == false) {
            models.iot_pricing_postpaid
              .update(data, {
                where: {
                  typeId: data.typeId,
                  memberId: data.memberId,
                },
              })
              .then(() => {
                resolve({
                  responseCode: 200,
                  response: `berhasil update pricing`,
                });
              });
          } else {
            resolve({
              responseCode: 200,
              response: `berhasil update pricing`,
            });
          }
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 401,
            messages: JSON.stringify(error),
          });
        });
    });
  },
  servicePricingBillingArea: (data, area_id) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_pricing_billing_area
            .destroy({
              where: { areaId: area_id },
            })
            .then((r) => {
              return models.iot_pricing_billing_area.bulkCreate(data);
            });
        })

        .then(() => {
          resolve({
            responseCode: 200,
            response: `berhasil update pricing`,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 401,
            messages: JSON.stringify(error),
          });
        });
    });
  },
  serviceGetPricingBillingArea: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_pricing_billing_area.findAll({
            where: data,
            include: [{ model: models.iot_node_type }],
          });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            response: result,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 401,
            messages: JSON.stringify(error),
          });
        });
    });
  },
};

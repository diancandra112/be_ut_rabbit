const models = require("../../models/index");
const { Op } = require("sequelize");
require("dotenv").config();
const moment = require("moment");
const axios = require("axios");
const axios_reatry = require("axios-retry");
const { admin } = require("../../config/fcm_teknisi");
const { serviceFcm } = require("./services_core_api_fcm");
const { SvcInsertChart } = require("./service_chart_insert");
const { GetNodeLog } = require("../../middlewares/chache");
module.exports = {
  serviceCoreNodeHistoryNonMeterApi: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        let update = {};
        if (data.type == "pulse") {
          update = { live_pulse: data.pulse };
        } else if (data.type == "interval") {
          update = { interval: data.pulse, setting_interval: data.pulse };
        }
        await models.iot_nodes.update(update, {
          where: { devEui: data.devEUI },
        });
        resolve({
          responseCode: 200,
          messages: "Success ",
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          messages: error.message,
        });
      }
    });
  },
  serviceCoreNodeHistoryApi: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction(async (t) => {
          let res = await GetNodeLog(data.devEUI);

          if (res == null) {
            return null;
          }
          try {
            if (data.typeId == 9) {
              let to_gagas = {
                id_meter: res.iot_tenant.handphone,
                pressure:
                  parseFloat(
                    JSON.parse(data.payload).payload_fields.gasPressure.toFixed(
                      2
                    )
                  ) *
                    2 +
                  10,
                timestamp: data.report_time,
              };
              await models.gagas_pressure_nonevc.create(to_gagas);
            } else if (data.typeId == 5) {
              let to_gagas_1 = {
                id_meter: res.iot_tenant.handphone,
                pressure: parseFloat(
                  JSON.parse(data.payload).payload_fields.gasPressure.toFixed(2)
                ),
                timestamp: data.report_time,
              };
              await models.gagas_pressure_nonevc.create(to_gagas_1);
            }
          } catch (error) {
            console.log(error);
          }

          if (res.iot_detail_rtu != null) {
            let met = JSON.parse(data.payload).subMeter.find(
              (valP) => valP.applicationName == res.iot_detail_rtu.model
            );
            if (res.field_billing_rtu != null) {
              let billing = res.field_billing_rtu.match(/\(([^)]+)\)/);
              if (billing != null && met != null) {
                met = met.report.find(
                  (valP) => valP.name == res.field_billing_rtu
                );
                if (met != null) {
                  let chart = {
                    nodeId: res.id,
                    typeId: res.typeId,
                    areaId: res.areaId,
                    fdate: moment(data.report_time)
                      .utcOffset("+0700")
                      .format("YYYY-MM-DD"),
                    fhour: moment(data.report_time)
                      .utcOffset("+0700")
                      .format("HH"),
                    meter: parseFloat(met.value),
                    type: `${res.rtu_pricing_name}-${billing[1]}`,
                  };
                  SvcInsertChart(chart)
                    .then((a) => console.log(a))
                    .catch((b) => console.log(b));
                }
              }
            }
          }

          let live_interval = 0;
          if (res.last_update != null) {
            const endTime = moment(data.report_time);
            const startTime = moment(res.last_update);
            var duration = moment.duration(endTime.diff(startTime));
            live_interval = duration.asMinutes();
          }
          let updateNode = {
            id: res.id,
            last_update: data.report_time,
            previous_update: res.last_update,
            live_interval: live_interval,
          };
          if (data.typeId == 4 || data.typeId == 14) {
            updateNode.live_battery = JSON.parse(data.payload).meter.battery;
            updateNode.live_battery = updateNode.live_battery
              .split("%")
              .join("");
            updateNode.live_valve = JSON.parse(data.payload).meter.valve;
          } else if (data.typeId == 10) {
            updateNode.live_valve = JSON.parse(
              data.payload
            ).payload_fields.relay;
          } else if (data.typeId == 8) {
            updateNode.live_last_meter = JSON.parse(
              data.payload
            ).payload_fields.pressure;
            updateNode.live_valve = "N/A";
            updateNode.setting_valve = "N/A";
          } else if (data.typeId == 5 || data.typeId == 9) {
            let battery5 = JSON.parse(data.payload).payload_fields
              .batteryVoltage;
            battery5 = (
              ((parseFloat(battery5) - 1.8) * 100) /
              (3.65 - 1.8)
            ).toFixed(2);
            updateNode.live_battery = battery5 > 100 ? 100 : battery5;
            updateNode.live_last_meter = JSON.parse(
              data.payload
            ).payload_fields.gasPressure;
            updateNode.live_valve = "N/A";
            updateNode.setting_valve = "N/A";
          } else if (data.typeId == 11 || data.typeId == 12) {
            let press_11 = JSON.parse(data.payload).meter.exReading.find(
              (val) => {
                return val.name.trim() == "Pressure";
              }
            );
            press_11 = press_11 ? parseFloat(press_11.value) / 100000 : 0;
            updateNode.live_last_meter = press_11;
            let battery11 = JSON.parse(data.payload).meter.exReading.find(
              (val) => {
                return val.name == "Battery";
              }
            );
            battery11 = battery11.value;
            battery11 = (
              ((parseFloat(battery11) - 1.8) * 100) /
              (3.65 - 1.8)
            ).toFixed(2);
            updateNode.live_battery = battery11 > 100 ? 100 : battery11;

            updateNode.live_valve = "N/A";
            updateNode.setting_valve = "N/A";
          } else if (data.typeId == 6) {
            let battery6 = JSON.parse(data.payload).meter.exReading.find(
              (val) => {
                return val.id == 3;
              }
            );
            battery6 = battery6.value;
            battery6 = (
              ((parseFloat(battery6) - 1.8) * 100) /
              (3.65 - 1.8)
            ).toFixed(2);
            updateNode.live_battery = battery6 > 100 ? 100 : battery6;
            updateNode.live_valve = "N/A";
            updateNode.setting_valve = "N/A";
          }
          updateNode.live_previous_meter = res.live_last_meter;

          //notif pressure
          if ([5, 8, 9, 11, 12].includes(parseInt(res.typeId))) {
            let notif = {
              devEui: res.devEui,
              title: "Notifikasi Pressure",
              area: res.iot_area.area_name,
              areaId: res.iot_area.id,
            };
            let flag_notif = false;
            let temp_update = [];
            let press_meter = updateNode.live_last_meter;
            if (res.typeId == 9) {
              press_meter = parseFloat(updateNode.live_last_meter) * 2 + 10;
            }
            if (press_meter < res.alarm_pressure) {
              if (
                moment(res.line_anomali).format("YYYY-MM-DD") !=
                  moment().format("YYYY-MM-DD") ||
                res.last_alarm_pressure <= 0
              ) {
                flag_notif = true;
                updateNode.last_alarm_pressure = res.alarm_pressure;
                updateNode.line_anomali = moment().format("YYYY-MM-DD");
                temp_update.push(
                  {
                    user_id: 0,
                    user_type: "TEKNISI",
                    title: "Alarm Node Pressure ",
                    body: ` ${
                      res.iot_tenant == null ? "" : res.iot_tenant.tenant_name
                    } - ${res.iot_area.area_name}\n devEui = ${
                      res.devEui
                    }\n Pressure Now = ${press_meter}\n Pressure Alert = ${
                      updateNode.last_alarm_pressure
                    }\n  `,
                    areaId: 0,
                    time: moment().format("YYYY-MM-DD HH:mm:ss"),
                  },
                  {
                    user_id: 0,
                    user_type: "TEKNISI",
                    title: "Alarm Node Pressure ",
                    body: ` ${
                      res.iot_tenant == null ? "" : res.iot_tenant.tenant_name
                    } - ${res.iot_area.area_name}\n devEui = ${
                      res.devEui
                    }\n Pressure Now = ${press_meter}\n Pressure Alert = ${
                      updateNode.last_alarm_pressure
                    }\n  `,
                    areaId: res.iot_area.id,
                    time: moment().format("YYYY-MM-DD HH:mm:ss"),
                  }
                );
              } else if (
                parseFloat(res.interval_alarm_pressure) > 0 &&
                parseFloat(res.alarm_pressure) -
                  parseFloat(res.interval_alarm_pressure) >
                  0 &&
                press_meter <=
                  parseFloat(res.alarm_pressure) -
                    parseFloat(res.interval_alarm_pressure) &&
                res.live_last_meter !=
                  parseFloat(res.alarm_pressure) -
                    parseFloat(res.interval_alarm_pressure)
              ) {
                flag_notif = true;
                updateNode.last_alarm_pressure =
                  parseFloat(res.alarm_pressure) -
                  parseFloat(res.interval_alarm_pressure);
                updateNode.line_anomali = moment().format("YYYY-MM-DD");
                temp_update.push(
                  {
                    user_id: 0,
                    user_type: "TEKNISI",
                    title: "Alarm Node Pressure ",
                    body: ` ${
                      res.iot_tenant == null ? "" : res.iot_tenant.tenant_name
                    } - ${res.iot_area.area_name}\n devEui = ${
                      res.devEui
                    }\n Pressure Now = ${press_meter}\n Pressure Alert = ${
                      updateNode.last_alarm_pressure
                    }\n  `,
                    areaId: 0,
                    time: moment().format("YYYY-MM-DD HH:mm:ss"),
                  },
                  {
                    user_id: 0,
                    user_type: "TEKNISI",
                    title: "Alarm Node Pressure ",
                    body: ` ${
                      res.iot_tenant == null ? "" : res.iot_tenant.tenant_name
                    } - ${res.iot_area.area_name}\n devEui = ${
                      res.devEui
                    }\n Pressure Now = ${press_meter}\n Pressure Alert = ${
                      updateNode.last_alarm_pressure
                    }\n  `,
                    areaId: res.iot_area.id,
                    time: moment().format("YYYY-MM-DD HH:mm:ss"),
                  }
                );
              }
            }

            if (flag_notif) {
              serviceFcm(
                "Alarm Node Pressure",
                ` ${
                  res.iot_tenant == null ? "" : res.iot_tenant.tenant_name
                } - ${res.iot_area.area_name} - ${moment().format(
                  "YYYY-MM-DD HH:mm:ss"
                )}\n devEui = ${
                  res.devEui
                }\n Pressure Now = ${press_meter}\n Pressure Alert = ${
                  updateNode.last_alarm_pressure
                }\n  `,
                temp_update
              );
            }
          }
          if (res.typeId == 11) {
            console.log("TYPE 11 ===services_core_api_1237.js");
            let update11 = {
              device_id: res.id,
              reportTime: data.report_time,
              reportTimeFlag: moment(data.report_time)
                .utcOffset("+0700")
                .format("YYYY-MM-DD HH:mm:ss"),
              tenantId: res.tenantId,
              internalId: res.internalId,
              devEui: res.devEui,
              meter_id: res.meter_id,
            };
            let n = await models.iot_histori_11.create(update11);
            let ex = [];
            JSON.parse(data.payload).meter.exReading.map((va) => {
              ex.push({
                device_id: res.id,
                log_id: n.id,
                name: va.name,
                value: va.value,
              });
            });
            await models.iot_histori_11_detail.bulkCreate(ex);
          }

          return models.iot_nodes
            .update(updateNode, {
              where: { id: updateNode.id },
            })
            .then(() => {
              let payload = {
                tenantId: res.tenantId,
                internalId: res.internalId,
                devEui: res.devEui,
                meter_id: res.meter_id,
                device_id: res.id,
                reportTime: data.report_time,
                payload: data.payload,
                payload_antares: data.payload_antares,
                reportTimeFlag: moment(data.report_time)
                  .utcOffset("+0700")
                  .format("YYYY-MM-DD HH:mm:ss"),
              };

              return models.iot_histories
                .create(payload)
                .then(([__responseFinal, status_flag]) => {
                  if (
                    res.iot_area != null &&
                    res.iot_area.decode_id != null &&
                    res.iot_area.url_to_company != null
                  ) {
                    switch (res.iot_area.decode_id) {
                      case 3:
                        if (data.typeId == 1) {
                          let to_gagas = {
                            id: __responseFinal.id,
                            idrefcustomer: res.iot_tenant.handphone,
                            fdate: moment(data.report_time)
                              .utcOffset("+0700")
                              .format("YYYY-MM-DD HH:mm:ss"),
                            fyear: moment(data.report_time)
                              .utcOffset("+0700")
                              .format("YYYY"),
                            fmonth: moment(data.report_time)
                              .utcOffset("+0700")
                              .format("M"),
                            fday: moment(data.report_time)
                              .utcOffset("+0700")
                              .format("D"),
                            fhour: moment(data.report_time)
                              .utcOffset("+0700")
                              .format("H"),
                            fminute: moment(data.report_time)
                              .utcOffset("+0700")
                              .format("m"),
                            fsecond: moment(data.report_time)
                              .utcOffset("+0700")
                              .format("s"),
                            fv:
                              JSON.parse(data.payload).meter.meterReading /
                              1000,
                            fp: 0,
                            fvcs: 1,
                            attribute1: res.merk,
                            attirbute2: res.iot_detail_gas.meter_id,
                            reffcreatedby: "WE",
                            fvu: 0,
                            pinlet: 0,
                          };
                          axios
                            .post(
                              res.iot_area.url_to_company + "/realtime/nonevc",
                              to_gagas,
                              {
                                headers: {
                                  authorization:
                                    "Bearer " + res.iot_area.company_token,
                                },
                              }
                            )
                            .then((response) => {
                              console.log(response.data, "axios");
                              return response.data;
                            })
                            .catch((error) => {
                              if (error.response != undefined) {
                                console.log(error.response.data, "axios error");
                                return error.response.data;
                              } else {
                                console.log(error.message, "axios error");
                                return error.message;
                              }
                            });
                        } else if (data.typeId == 5) {
                          let to_gagas_1 = {
                            id_meter: res.iot_tenant.handphone,
                            pressure: parseFloat(
                              JSON.parse(
                                data.payload
                              ).payload_fields.gasPressure.toFixed(2)
                            ),
                            timestamp: data.report_time,
                          };
                          axios
                            .post(
                              res.iot_area.url_to_company +
                                "/realtime/pressure/nonevc",
                              to_gagas_1,
                              {
                                headers: {
                                  authorization:
                                    "Bearer " + res.iot_area.company_token,
                                },
                              }
                            )
                            .then((response) => {
                              console.log(response.data, "axios");
                              return response.data;
                            })
                            .catch((error) => {
                              if (error.response != undefined) {
                                console.log(error.response.data, "axios error");
                                return error.response.data;
                              } else {
                                console.log(error.message, "axios error");
                                return error.message;
                              }
                            });
                        } else if (data.typeId == 9) {
                          let to_gagas_9 = {
                            id_meter: res.iot_tenant.handphone,
                            pressure:
                              parseFloat(
                                JSON.parse(
                                  data.payload
                                ).payload_fields.gasPressure.toFixed(2)
                              ) *
                                2 +
                              10,
                            timestamp: data.report_time,
                          };
                          axios
                            .post(
                              res.iot_area.url_to_company +
                                "/realtime/pressure/nonevc",
                              to_gagas_9,
                              {
                                headers: {
                                  authorization:
                                    "Bearer " + res.iot_area.company_token,
                                },
                              }
                            )
                            .then((response) => {
                              console.log(response.data, "axios");
                              return response.data;
                            })
                            .catch((error) => {
                              if (error.response != undefined) {
                                console.log(error.response.data, "axios error");
                                return error.response.data;
                              } else {
                                console.log(error.message, "axios error");
                                return error.message;
                              }
                            });
                        }
                        break;
                      case 1:
                        console.log(res.iot_area.url_to_company);
                        if (status_flag == true) {
                          console.log("to company");
                          // axios_reatry(axios, {
                          //   retryCondition: (condition) => {
                          //     return condition.status != 200;
                          //   },
                          //   shouldResetTimeout: true,
                          //   retries: Infinity,
                          //   retryDelay: (retryCount) => {
                          //     return retryCount * 1000;
                          //   },
                          // });
                          // axios_reatry(axios, {
                          //   retries: Infinity,
                          //   retryDelay: (retryCount) => {
                          //     return retryCount * 1000;
                          //   },
                          // });
                          if (payload.payload_antares != undefined) {
                            axios
                              .post(
                                res.iot_area.url_to_company,
                                JSON.parse(payload.payload_antares),
                                {
                                  headers: {
                                    authorization:
                                      "Bearer " + res.iot_area.company_token,
                                  },
                                }
                              )
                              .then((response) => {
                                console.log(response.data, "axios");
                                return response.data;
                              })
                              .catch((error) => {
                                console.log(error.response.data, "axios error");
                                return error.response.data;
                              });
                          } else {
                            break;
                          }
                        } else {
                          break;
                        }
                        break;

                      case 4:
                        console.log("to company gamatechno");
                        axios
                          .post(
                            res.iot_area.url_to_company,
                            JSON.parse(payload.payload),
                            {
                              headers: {
                                authorization:
                                  "Bearer " + res.iot_area.company_token,
                              },
                            }
                          )
                          .then((response) => {
                            console.log(response.data, "axios");
                            return response.data;
                          })
                          .catch((error) => {
                            console.log(error.response.data, "axios error");
                            return error.response.data;
                          });
                        break;

                      case 2:
                        // if (status_flag == true || status_flag == "true") {
                        console.log(
                          "to company id 2 = ",
                          res.iot_area.url_to_company
                        );
                        // axios_reatry(axios, {
                        //   retryCondition: (condition) => {
                        //     return condition.status != 200;
                        //   },
                        //   shouldResetTimeout: true,
                        //   retries: Infinity,
                        //   retryDelay: (retryCount) => {
                        //     return retryCount * 1000;
                        //   },
                        // });
                        // axios_reatry(axios, {
                        //   retries: Infinity,
                        //   retryDelay: (retryCount) => {
                        //     return retryCount * 1000;
                        //   },
                        // });
                        let temp_rtu = JSON.parse(
                          payload.payload
                        ).subMeter.filter((valP) => {
                          return (
                            valP.applicationName == res.iot_detail_rtu.model &&
                            valP.serialNumber ==
                              res.iot_detail_rtu.serial_number
                          );
                        })[0].report;

                        const filter_rtu = (rtu, str) => {
                          let data = rtu.filter((valP) => {
                            return valP.name == str;
                          })[0];
                          console.log(data, "adawd");
                          if (data != undefined) {
                            return data.value;
                          } else {
                            return 0;
                          }
                        };

                        let to_att = {
                          ctlr_id: res.id,
                          auth: "sample string 2",
                          kwh_id: res.id,
                          kwh_type: res.iot_detail_rtu.model,
                          // kwh_type: res.iot_node_type.type_name,
                          type_kwh_code: res.iot_node_type.type_id,
                          va: filter_rtu(temp_rtu, "va"),
                          vb: filter_rtu(temp_rtu, "vb"),
                          vc: filter_rtu(temp_rtu, "vc"),
                          ia: filter_rtu(temp_rtu, "ia"),
                          ib: filter_rtu(temp_rtu, "ib"),
                          ic: filter_rtu(temp_rtu, "ic"),
                          pf: filter_rtu(temp_rtu, "pf"),
                          f: filter_rtu(temp_rtu, "f"),
                          kwh: filter_rtu(temp_rtu, "kwh"),
                          ts: moment(payload.reportTime)
                            .utcOffset("+0700")
                            .format(),
                        };
                        console.log(
                          status_flag,
                          res.iot_area.url_to_company,
                          to_att,
                          "next axios"
                        );
                        // "to_company_payload",to_company_response
                        // return __responseFinal;
                        axios
                          .post(res.iot_area.url_to_company, to_att, {
                            timeout: 120000,
                            headers: {
                              authorization:
                                "Bearer " + res.iot_area.company_token,
                            },
                          })
                          .then((response) => {
                            console.log(response, "axios");
                            models.iot_histories
                              .update(
                                {
                                  to_company_response: JSON.stringify(
                                    response.data
                                  ),
                                  to_company_payload: JSON.stringify(to_att),
                                },
                                { where: { id: __responseFinal.id } }
                              )
                              .then(() => {
                                return response.data;
                              });
                          })
                          .catch((error) => {
                            console.log(error.response.data, "axios error");
                            models.iot_histories
                              .update(
                                {
                                  to_company_response: JSON.stringify(
                                    error.response.data
                                  ),
                                  to_company_payload: JSON.stringify(to_att),
                                },
                                { where: { id: __responseFinal.id } }
                              )
                              .then(() => {
                                return error.response.data;
                              });
                          });
                        // } else {
                        //   break;
                        // }
                        break;

                      default:
                        break;
                    }
                  } else {
                    console.log("awd");
                    return __responseFinal;
                  }
                });
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: result,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error.message,
          });
        });
    });
  },
};

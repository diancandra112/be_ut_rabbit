const models = require("../../models/index");
const { Op } = require("sequelize");
const { SvcInsertChart } = require("./service_chart_insert");
require("dotenv").config();
const moment = require("moment");
const axios = require("axios");
const axios_reatry = require("axios-retry");
const { GetNodeLog } = require("../../middlewares/chache");
module.exports = {
  serviceCoreNodeHistoryApi2: (data, max_totalizer) => {
    return new Promise(async (resolve, reject) => {
      try {
        let res = await GetNodeLog(data.devEUI);
        if (res == null) {
          return null;
        }
        let live_interval = 0;
        let updateNode = {
          id: res.id,
          last_update: data.report_time,
          previous_update: res.last_update,
          live_previous_meter: res.live_last_meter,
          live_interval: live_interval,
        };
        if (res.last_update != null) {
          const endTime = moment(data.report_time);
          const startTime = moment(res.last_update);
          var duration = moment.duration(endTime.diff(startTime));
          live_interval = duration.asMinutes();
        }

        updateNode.live_battery = JSON.parse(data.payload).meter.battery;
        updateNode.live_battery = updateNode.live_battery.split("%").join("");
        updateNode.live_valve = JSON.parse(data.payload).meter.valve;
        if (res.setting_valve != "close" && res.setting_valve != "open") {
          updateNode.setting_valve = JSON.parse(data.payload).meter.valve;
        }
        updateNode.live_last_meter = JSON.parse(
          data.payload
        ).meter.meterReading;
        if (res.start_anomali_meter == null) {
          updateNode.start_anomali_date = moment();
          updateNode.start_anomali_meter = JSON.parse(
            data.payload
          ).meter.meterReading;
          updateNode.is_anomali = false;
        } else {
          const usage =
            parseInt(updateNode.live_last_meter) -
            parseInt(res.start_anomali_meter);
          const endtime = moment();
          const starttime = moment(res.start_anomali_date);
          var durations = moment.duration(endtime.diff(starttime));
          const jam = durations.asHours();
          if (usage != 0) {
            updateNode.start_anomali_date = moment();
            updateNode.start_anomali_meter = JSON.parse(
              data.payload
            ).meter.meterReading;
            updateNode.is_anomali = false;
          } else if (jam >= 20 && usage == 0) {
            updateNode.is_anomali = true;
          }
        }
        let update2 = {
          max_totalizer: max_totalizer,
          device_id: res.id,
          meterreading: parseFloat(JSON.parse(data.payload).meter.meterReading),
          reporttime: data.report_time,
          reporttimeflag: moment(data.report_time)
            .utcOffset("+0700")
            .format("YYYY-MM-DD HH:mm:ss"),
          timereport: moment(data.report_time)
            .utcOffset("+0700")
            .format("YYYY-MM-DD HH:mm:ss"),
          tenantid: res.tenantId,
          internalid: res.internalId,
          deveui: res.devEui,
          meter_id: res.meter_id,
          valve: JSON.parse(data.payload).meter.valve,
          battrey_level: JSON.parse(data.payload).meter.battery,
        };
        await models.iots_history_water.create(update2);

        await models.iot_nodes.update(updateNode, {
          where: { id: updateNode.id },
        });
        resolve({
          responseCode: 200,
          messages: "ok",
        });
      } catch (error) {
        reject({
          responseCode: 400,
          messages: error.message,
        });
      }
    });
  },
};

const models = require("../../models/index");
const { Op } = require("sequelize");
require("dotenv").config();
const moment = require("moment");
const axios = require("axios");
const axios_reatry = require("axios-retry");
const { admin } = require("../../config/fcm_teknisi");
module.exports = {
  serviceFcm: (title, body, temp_update, area_id) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_teknisi
            .findAll({
              where: {
                area_id: { [Op.in]: [area_id, 0] },
                fcm_token: { [Op.not]: null },
              },
              attributes: ["id", "fcm_token"],
            })
            .then((res_teknisi) => {
              res_teknisi = JSON.parse(JSON.stringify(res_teknisi));
              if (res_teknisi.length == 0) {
                return "ok";
              }
              models.iots_fcm.bulkCreate(temp_update);
              let token = [];
              res_teknisi.map((val) => {
                token.push(val.fcm_token);
              });
              const notification_options = {
                priority: "high",
                timeToLive: 60 * 60 * 24,
              };
              const registrationToken = token;
              const message = {
                notification: {
                  title: title,
                  body: body,
                },
              };
              const options = notification_options;

              admin
                .messaging()
                .sendToDevice(registrationToken, message, options)
                .then((response) => {
                  return response.results;
                })
                .catch((error) => {
                  return error;
                });
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: result,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error.message,
          });
        });
    });
  },
};

const models = require("../../models/index");
const { Op } = require("sequelize");
require("dotenv").config();
const moment = require("moment");
const axios = require("axios");
const axios_reatry = require("axios-retry");
const { admin } = require("../../config/fcm_teknisi");
const { serviceFcm } = require("./services_core_api_fcm");
module.exports = {
  serviceCoreNodeHistoryApi14: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        let node_find = await models.iot_nodes.findOne({
          where: { devEui: data.devEUI },
          attributes: [
            "id",
            "last_update",
            "tenantId",
            "internalId",
            "devEui",
            "meter_id",
          ],
        });
        node_find = JSON.parse(JSON.stringify(node_find));
        let live_interval = 0;
        if (node_find.last_update != null) {
          const endTime = moment(data.report_time);
          const startTime = moment(node_find.last_update);
          var duration = moment.duration(endTime.diff(startTime));
          live_interval = duration.asMinutes();
        }
        let node_report = JSON.parse(data.payload);
        let updateNode = {
          id: node_find.id,
          last_update: data.report_time,
          previous_update: node_find.last_update,
          live_interval: live_interval,
          live_valve: node_report.meter.valve,
          setting_valve: node_report.meter.valve,
          live_battery: node_report.meter.battery,
        };
        try {
          updateNode.live_battery = updateNode.live_battery.split("%").join("");
        } catch (error) {}
        let iots_histori_14 = {
          reportTime: data.report_time,
          reportTimeFlag: moment(data.report_time)
            .utcOffset("+0700")
            .format("YYYY-MM-DD HH:mm:ss"),
          device_id: node_find.id,
          tenantId: node_find.tenantId,
          internalId: node_find.internalId,
          devEui: node_find.devEui,
          meter_id: node_find.meter_id,
        };

        let log_14 = await models.iots_history_14.create(iots_histori_14);
        let detail_14 = [];
        node_report.meter.exReading.map((val) => {
          detail_14.push({
            name: val.name,
            value: val.value,
            histori_id: log_14.id,
          });
        });
        await models.iots_history_14_detail.bulkCreate(detail_14);
        try {
          await models.iot_nodes.update(
            { last_rtu_log: JSON.stringify(detail_14) },
            {
              where: { id: node_find.id },
            }
          );
        } catch (error) {}
        resolve({
          responseCode: 200,
          messages: detail_14,
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          messages: error.message,
        });
      }
    });
  },
};

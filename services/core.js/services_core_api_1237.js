const models = require("../../models/index");
const { Op } = require("sequelize");
const { SvcInsertChart } = require("./service_chart_insert");
require("dotenv").config();
const moment = require("moment");
const axios = require("axios");
const axios_reatry = require("axios-retry");
const { GetNodeLog } = require("../../middlewares/chache");
module.exports = {
  serviceCoreNodeHistoryApi1237: (data, max_totalizer) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction(async (t) => {
          let res = await GetNodeLog(data.devEUI);
          if (res == null) {
            return null;
          }
          try {
            if ([1, 2].includes(data.typeId)) {
              let f_n = await models.iot_nodes.findOne({
                where: { devEui: data.devEUI, is_unsigned: false },
                include: [
                  {
                    model: models.iot_tenant,
                    required: true,
                    attributes: [
                      "id",
                      "anomali_usage_end_date",
                      "anomali_usage_start_date",
                    ],
                  },
                ],
                attributes: [
                  "id",
                  "devEui",
                  "live_last_meter",
                  "areaId",
                  "tenantId",
                  "internalId",
                  "typeId",
                ],
              });
              f_n = JSON.parse(JSON.stringify(f_n));
              if (f_n) {
                let usg =
                  parseFloat(JSON.parse(data.payload).meter.meterReading) -
                  parseFloat(res.live_last_meter);
                if (
                  f_n.iot_tenant.anomali_usage_start_date != null &&
                  f_n.iot_tenant.anomali_usage_end_date != null &&
                  usg > 0
                ) {
                  let time1 = f_n.iot_tenant.anomali_usage_start_date;
                  let time2 = f_n.iot_tenant.anomali_usage_end_date;
                  let now = moment()
                    .utcOffset("+0700")
                    .format("YYYY-MM-DD HH:mm:ss");
                  let now_tgl = moment(now)
                    .utcOffset("+0700")
                    .format("YYYY-MM-DD");
                  let now_tgl_1 = moment(now)
                    .utcOffset("+0700")
                    .subtract(1, "day")
                    .format("YYYY-MM-DD");
                  let now_tgl_ad = moment(now)
                    .utcOffset("+0700")
                    .add(1, "day")
                    .format("YYYY-MM-DD");
                  let now_time = moment(now)
                    .utcOffset("+0700")
                    .format("HH:mm:ss");
                  if (time1 <= time2) {
                    time1 = `${now_tgl} ${time1}`;
                    time2 = `${now_tgl} ${time2}`;
                  } else {
                    if (time1 > now_time) {
                      time1 = `${now_tgl_1} ${time1}`;
                      time2 = `${now_tgl} ${time2}`;
                    } else {
                      time1 = `${now_tgl} ${time1}`;
                      time2 = `${now_tgl_ad} ${time2}`;
                    }
                  }

                  if (time1 <= now && time2 >= now) {
                    let data_anom_time = {
                      areaId: f_n.areaId,
                      tenantId: f_n.tenantId,
                      internalId: f_n.internalId,
                      last_update: data.report_time,
                      usage: usg / 1000,
                      deveui: data.devEUI,
                      status: true,
                      valve: JSON.parse(data.payload).meter.valve,
                      typeid: f_n.typeId,
                      anomi_type: "USAGE TIME",
                    };

                    models.iots_anomali_history
                      .create(data_anom_time)
                      .then((cu) => console.log(cu))
                      .catch((ers) => console.log(ers));
                  }
                }
              }
            }
          } catch (error) {
            console.log(error);
          }
          try {
            if (data.typeId == 1) {
              console.log("TYPE 1 ===services_core_api_1237.js");
              let update1 = {
                max_totalizer: max_totalizer,
                device_id: res.id,
                meterReading: parseFloat(
                  JSON.parse(data.payload).meter.meterReading
                ),
                reportTime: data.report_time,
                reportTimeFlag: moment(data.report_time)
                  .utcOffset("+0700")
                  .format("YYYY-MM-DD HH:mm:ss"),
                tenantId: res.tenantId,
                internalId: res.internalId,
                devEui: res.devEui,
                meter_id: res.meter_id,
                valve: JSON.parse(data.payload).meter.valve,
                battrey_level: JSON.parse(data.payload).meter.battery,
              };
              await models.iots_history_gas.create(update1);
              try {
                if (res.areaId == 100) {
                  let insert_gagas = {
                    idrefcustomer: res.iot_tenant.handphone,
                    fdate: moment(data.report_time)
                      .utcOffset("+0700")
                      .format("YYYY-MM-DD HH:mm:ss"),
                    fyear: moment(data.report_time)
                      .utcOffset("+0700")
                      .format("YYYY"),
                    fmonth: moment(data.report_time)
                      .utcOffset("+0700")
                      .format("M"),
                    fday: moment(data.report_time)
                      .utcOffset("+0700")
                      .format("D"),
                    fhour: moment(data.report_time)
                      .utcOffset("+0700")
                      .format("H"),
                    fminute: moment(data.report_time)
                      .utcOffset("+0700")
                      .format("m"),
                    fsecond: moment(data.report_time)
                      .utcOffset("+0700")
                      .format("s"),
                    fv: JSON.parse(data.payload).meter.meterReading / 1000,
                    fp: 0,
                    fvcs: 1,
                    attribute1: res.merk,
                    attirbute2: res.iot_detail_gas.meter_id,
                    reffcreatedby: "WE",
                    fvu: 0,
                    pinlet: 0,
                  };
                  await models.gagas_realtime_nonevc.create(insert_gagas);
                }
              } catch (error) {
                console.log(error);
              }
            } else if (data.typeId == 2) {
              console.log("TYPE 2 ===services_core_api_1237.js");
              let update2 = {
                max_totalizer: max_totalizer,
                device_id: res.id,
                meterreading: parseFloat(
                  JSON.parse(data.payload).meter.meterReading
                ),
                reporttime: data.report_time,
                reporttimeflag: moment(data.report_time)
                  .utcOffset("+0700")
                  .format("YYYY-MM-DD HH:mm:ss"),
                tenantid: res.tenantId,
                internalid: res.internalId,
                deveui: res.devEui,
                meter_id: res.meter_id,
                valve: JSON.parse(data.payload).meter.valve,
                battrey_level: JSON.parse(data.payload).meter.battery,
              };
              // await models.iots_history_water.create(update2);
            } else if (data.typeId == 3) {
              console.log("TYPE 3 ===services_core_api_1237.js");
              let update3 = {
                device_id: res.id,
                meterreading: parseFloat(
                  JSON.parse(data.payload).meter.meterReading
                ),
                reporttime: data.report_time,
                reporttimeflag: moment(data.report_time)
                  .utcOffset("+0700")
                  .format("YYYY-MM-DD HH:mm:ss"),
                tenantid: res.tenantId,
                internalid: res.internalId,
                deveui: res.devEui,
                meter_id: res.meter_id,
                valve: JSON.parse(data.payload).meter.valve,
                battrey_level: JSON.parse(data.payload).meter.battery,
              };
              let n = await models.iots_history_3.create(update3);
              let ex = [];
              JSON.parse(data.payload).meter.exReading.map((va) => {
                ex.push({
                  device_id: res.id,
                  log_id: n.id,
                  name: va.name,
                  value: va.value,
                });
              });
              await models.grt_histroy_3_ex.bulkCreate(ex);
            }
            //  else if (data.typeId == 11) {
            //   console.log("TYPE 11 ===services_core_api_1237.js");
            //   let update11 = {
            //     device_id: res.id,
            //     reportTime: data.report_time,
            //     reportTimeFlag: moment(data.report_time)
            //       .utcOffset("+0700")
            //       .format("YYYY-MM-DD HH:mm:ss"),
            //     tenantId: res.tenantId,
            //     internalId: res.internalId,
            //     devEui: res.devEui,
            //     meter_id: res.meter_id,
            //   };
            //   let n = await models.iot_histori_11.create(update11);
            //   let ex = [];
            //   JSON.parse(val.payload).meter.exReading.map((va) => {
            //     ex.push({
            //       device_id: res.id,
            //       log_id: n.id,
            //       name: va.name,
            //       value: va.value,
            //     });
            //   });
            //   await models.iot_histori_11_detail.bulkCreate(ex);
            // }
          } catch (error) {}
          let live_interval = 0;
          if (res.last_update != null) {
            const endTime = moment(data.report_time);
            const startTime = moment(res.last_update);
            var duration = moment.duration(endTime.diff(startTime));
            live_interval = duration.asMinutes();
          }
          //cek anomali
          let anomali = [];
          if (
            [1, 2, 3, 7].includes(parseInt(res.typeId)) &&
            isNaN(parseFloat(res.live_last_meter)) == false
          ) {
            let useg_max =
              parseFloat(JSON.parse(data.payload).meter.meterReading) -
              parseFloat(res.live_last_meter);
            // if (
            //   res.anomali_usage_start_date != null &&
            //   res.anomali_usage_end_date != null &&
            //   useg_max > 0
            // ) {
            //   let last_update_anom = moment(res.last_update)
            //     .utcOffset("+0700")
            //     .format("YYYY-MM-DD HH:mm:ss");
            //   let anomali_start = moment(
            //     res.anomali_usage_start_date,
            //     "HH:mm:ss"
            //   );
            //   let anomali_end = moment(res.anomali_usage_end_date, "HH:mm:ss");
            //   let flag_start = parseInt(anomali_start.format("HHmmss"));
            //   let flag_end = parseInt(anomali_end.format("HHmmss"));
            //   if (flag_end == 0) {
            //     anomali_end = anomali_end
            //       .add(1, "day")
            //       .format("YYYY-MM-DD HH:mm:ss");
            //   } else {
            //     if (flag_start > flag_end) {
            //       anomali_end = anomali_end
            //         .add(1, "day")
            //         .format("YYYY-MM-DD HH:mm:ss");
            //     } else {
            //       anomali_end = anomali_end.format("YYYY-MM-DD HH:mm:ss");
            //     }
            //   }
            //   anomali_start = anomali_start.format("YYYY-MM-DD HH:mm:ss");

            //   if (
            //     moment(last_update_anom).isSameOrAfter(moment(anomali_start)) &&
            //     moment(last_update_anom).isSameOrBefore(moment(anomali_end))
            //   ) {
            //     anomali.push({
            //       areaId: res.areaId,
            //       tenantId: res.tenantId,
            //       internalId: res.internalId,
            //       last_update: data.report_time,
            //       usage: useg_max / 1000,
            //       deveui: data.devEUI,
            //       status: true,
            //       valve: JSON.parse(data.payload).meter.valve,
            //       typeid: res.typeId,
            //       anomi_type: "USAGE TIME",
            //     });
            //   }
            // }
            if (
              parseFloat(res.max_usage) > 0 &&
              isNaN(parseFloat(res.live_last_meter)) == false
            ) {
              if (
                useg_max > parseFloat(res.max_usage) &&
                parseInt(res.setting_interval) > 0 &&
                Math.abs(
                  parseInt(res.setting_interval) - parseInt(live_interval)
                ) <= 10
              ) {
                anomali.push({
                  areaId: res.areaId,
                  tenantId: res.tenantId,
                  internalId: res.internalId,
                  last_update: data.report_time,
                  usage: useg_max / 1000,
                  max_usage: parseFloat(res.max_usage) / 1000,
                  deveui: data.devEUI,
                  status: true,
                  valve: JSON.parse(data.payload).meter.valve,
                  typeid: res.typeId,
                  anomi_type: "USAGE INTERVAL",
                });
              }
            }
          }
          //cek anomali
          let updateNode = {
            id: res.id,
            last_update: data.report_time,
            previous_update: res.last_update,
            live_previous_meter: res.live_last_meter,
            live_interval: live_interval,
          };
          if (
            data.typeId == 1 ||
            data.typeId == 2 ||
            data.typeId == 3 ||
            data.typeId == 7
          ) {
            updateNode.live_battery = JSON.parse(data.payload).meter.battery;
            updateNode.live_battery = updateNode.live_battery
              .split("%")
              .join("");
            updateNode.live_valve = JSON.parse(data.payload).meter.valve;
            if (res.setting_valve != "close" && res.setting_valve != "open") {
              updateNode.setting_valve = JSON.parse(data.payload).meter.valve;
            }
            updateNode.live_last_meter = JSON.parse(
              data.payload
            ).meter.meterReading;
            if (res.start_anomali_meter == null) {
              updateNode.start_anomali_date = moment();
              updateNode.start_anomali_meter = JSON.parse(
                data.payload
              ).meter.meterReading;
              updateNode.is_anomali = false;
            } else {
              const usage =
                parseInt(updateNode.live_last_meter) -
                parseInt(res.start_anomali_meter);
              const endtime = moment();
              const starttime = moment(res.start_anomali_date);
              var durations = moment.duration(endtime.diff(starttime));
              const jam = durations.asHours();
              if (usage != 0) {
                updateNode.start_anomali_date = moment();
                updateNode.start_anomali_meter = JSON.parse(
                  data.payload
                ).meter.meterReading;
                updateNode.is_anomali = false;
              } else if (jam >= 20 && usage == 0) {
                updateNode.is_anomali = true;
              }
            }
          }
          let chart = {
            nodeId: res.id,
            typeId: res.typeId,
            areaId: res.areaId,
            fdate: moment(data.report_time)
              .utcOffset("+0700")
              .format("YYYY-MM-DD"),
            fhour: moment(data.report_time).utcOffset("+0700").format("HH"),
            meter: JSON.parse(data.payload).meter.meterReading,
          };

          if (
            parseFloat(updateNode.live_last_meter) -
              parseFloat(updateNode.live_previous_meter) ==
              0 &&
            res.iot_node != null &&
            isNaN(res.iot_node.live_previous_meter) == false &&
            isNaN(res.iot_node.live_last_meter) == false &&
            parseFloat(res.typeId) == 1 &&
            parseFloat(res.iot_node.live_last_meter) <=
              parseFloat(res.iot_node.live_previous_meter)
          ) {
            let limits =
              parseInt(res.setting_interval) /
              parseInt(res.iot_node.setting_interval);
            limits = parseInt(limits) < 3 ? 3 : parseInt(limits);
            limits = parseInt(limits) > 5 ? 5 : parseInt(limits);
            models.iot_histories
              .findAll({
                where: { device_id: res.iot_node.id },
                limit: limits,
                order: [["reportTime", "DESC"]],
              })
              .then((press) => {
                press = JSON.parse(JSON.stringify(press));
                if (press.length > 0) {
                  let chart_press = [];
                  press.map((val) => {
                    if (res.iot_node.typeId == 5) {
                      chart_press.push({
                        date: val.reportTime,
                        meter: JSON.parse(val.payload).payload_fields
                          .gasPressure,
                      });
                    } else if (res.iot_node.typeId == 9) {
                      chart_press.push({
                        date: val.reportTime,
                        meter:
                          parseFloat(
                            JSON.parse(val.payload).payload_fields.gasPressure
                          ) *
                            2 +
                          10,
                      });
                    } else if (
                      res.iot_node.typeId == 11 ||
                      res.iot_node.typeId == 12
                    ) {
                      let meter_read = JSON.parse(
                        val.payload
                      ).meter.exReading.find((valP) => {
                        return valP.name.trim() == "Pressure";
                      });
                      if (meter_read != undefined) {
                        chart_press.push({
                          date: val.reportTime,
                          meter: parseFloat(meter_read.value) / 100000,
                        });
                      }
                    }
                  });
                  let kerusakan = {
                    location_type:
                      res.iot_tenant != null ? "TENANT" : "INTERNAL",
                    location:
                      res.iot_tenant != null
                        ? res.iot_tenant.tenant_name
                        : res.iot_internal.internal_name,
                    areaId: res.areaId,
                    node_gas_id: res.id,
                    node_gas_devEui: res.devEui,
                    node_gas_last_update: updateNode.last_update,
                    node_pressure_id: res.iot_node.id,
                    node_pressure_devEui: res.iot_node.devEui,
                    node_pressure_date_1:
                      chart_press[chart_press.length - 1].date,
                    node_pressure_date_2: chart_press[0].date,
                    node_pressure_meter_1:
                      chart_press[chart_press.length - 1].meter,
                    node_pressure_meter_2: chart_press[0].meter,
                    node_gas_totalizer:
                      parseFloat(updateNode.live_last_meter) / 1000,
                    chart_press: JSON.stringify(chart_press),
                  };
                  console.log(kerusakan);
                  if (
                    kerusakan.node_pressure_meter_2 -
                      kerusakan.node_pressure_meter_1 <
                    0
                  ) {
                    models.iots_kerusakan_gas
                      .create(kerusakan)
                      .then(() => console.log("done,insert"))
                      .catch((e) => console.log(e));
                  }
                }
              });
          }

          SvcInsertChart(chart);
          if (data.typeId == 3 || data.typeId == 7) {
            try {
              updateNode.meter_id = JSON.parse(data.payload).serialNumber;
              if (data.typeId == 3) {
                models.iot_detail_electric.update(
                  { meter_id: updateNode.meter_id },
                  { where: { node_id: updateNode.id } }
                );
              } else {
                models.iot_detail_electricct.update(
                  { meter_id: updateNode.meter_id },
                  { where: { node_id: updateNode.id } }
                );
              }
            } catch (error) {
              console.log(error);
            }
          }
          return models.iot_nodes
            .update(updateNode, {
              where: { id: updateNode.id },
            })
            .then(() => {
              let payload = {
                tenantId: res.tenantId,
                internalId: res.internalId,
                devEui: res.devEui,
                meter_id: res.meter_id,
                device_id: res.id,
                reportTime: data.report_time,
                payload: data.payload,
                payload_antares: data.payload_antares,
                reportTimeFlag: moment(data.report_time)
                  .utcOffset("+0700")
                  .format("YYYY-MM-DD HH:mm:ss"),
              };
              if (
                data.typeId == 1 ||
                data.typeId == 2 ||
                data.typeId == 3 ||
                data.typeId == 7
              ) {
                payload.balance =
                  res.prepayment - JSON.parse(data.payload).meter.meterReading;
                payload.meterReading = JSON.parse(
                  data.payload
                ).meter.meterReading;
                if (res.live_last_meter == null) {
                  payload.usage = 0;
                } else {
                  const InputMeter = JSON.parse(data.payload).meter
                    .meterReading;
                  const CurrentMeter = parseFloat(res.live_last_meter);
                  payload.usage = InputMeter - CurrentMeter;
                }
              }
              if (
                data.payload_antares != undefined ||
                data.payload_antares != null
              ) {
                payload.payload_antares = data.payload_antares;
                switch (data.typeId) {
                  case 1:
                    payload.payload_antares = JSON.parse(
                      payload.payload_antares
                    );
                    payload.payload_antares.devInfo.SN =
                      res.iot_detail_gas.meter_id;
                    payload.payload_antares = JSON.stringify(
                      payload.payload_antares
                    );
                    break;

                  default:
                    break;
                }
              }
              payload.to_company_payload = "ok";
              return models.iot_histories
                .create(payload, {
                  transaction: t,
                })
                .then((__responseFinal) => {
                  __responseFinal = JSON.parse(JSON.stringify(__responseFinal));
                  console.log(__responseFinal, "services_core_api_1237.js");
                  if (anomali.length > 0) {
                    models.iots_anomali_history.bulkCreate(anomali);
                  }
                  if (
                    res.iot_area != null &&
                    res.iot_area.decode_id != null &&
                    res.iot_area.url_to_company != null
                  ) {
                    switch (res.iot_area.decode_id) {
                      case 3:
                        if (data.typeId == 1) {
                          let to_gagas = {
                            id: __responseFinal.id,
                            idrefcustomer: res.iot_tenant.handphone,
                            fdate: moment(data.report_time)
                              .utcOffset("+0700")
                              .format("YYYY-MM-DD HH:mm:ss"),
                            fyear: moment(data.report_time)
                              .utcOffset("+0700")
                              .format("YYYY"),
                            fmonth: moment(data.report_time)
                              .utcOffset("+0700")
                              .format("M"),
                            fday: moment(data.report_time)
                              .utcOffset("+0700")
                              .format("D"),
                            fhour: moment(data.report_time)
                              .utcOffset("+0700")
                              .format("H"),
                            fminute: moment(data.report_time)
                              .utcOffset("+0700")
                              .format("m"),
                            fsecond: moment(data.report_time)
                              .utcOffset("+0700")
                              .format("s"),
                            fv:
                              JSON.parse(data.payload).meter.meterReading /
                              1000,
                            fp: 0,
                            fvcs: 1,
                            attribute1: res.merk,
                            attirbute2: res.iot_detail_gas.meter_id,
                            reffcreatedby: "WE",
                            fvu: 0,
                            pinlet: 0,
                          };
                          axios
                            .post(
                              res.iot_area.url_to_company + "/realtime/nonevc",
                              to_gagas,
                              {
                                headers: {
                                  authorization:
                                    "Bearer " + res.iot_area.company_token,
                                },
                              }
                            )
                            .then((response) => {
                              console.log(response.data, "axios");
                              return response.data;
                            })
                            .catch((error) => {
                              if (error.response != undefined) {
                                console.log(error.response.data, "axios error");
                                return error.response.data;
                              } else {
                                console.log(error.message, "axios error");
                                return error.message;
                              }
                            });
                        }
                        break;
                      case 1:
                        console.log(res.iot_area.url_to_company);
                        console.log("to company");
                        if (payload.payload_antares != undefined) {
                          axios
                            .post(
                              res.iot_area.url_to_company,
                              JSON.parse(payload.payload_antares),
                              {
                                headers: {
                                  authorization:
                                    "Bearer " + res.iot_area.company_token,
                                },
                              }
                            )
                            .then((response) => {
                              console.log(response.data, "axios");
                              return response.data;
                            })
                            .catch((error) => {
                              console.log(error.response.data, "axios error");
                              return error.response.data;
                            });
                        } else {
                          break;
                        }
                        break;

                      case 4:
                        console.log("to company gamatechno");
                        axios
                          .post(
                            res.iot_area.url_to_company,
                            JSON.parse(payload.payload),
                            {
                              headers: {
                                authorization:
                                  "Bearer " + res.iot_area.company_token,
                              },
                            }
                          )
                          .then((response) => {
                            console.log(response.data, "axios");
                            return response.data;
                          })
                          .catch((error) => {
                            console.log(error.response.data, "axios error");
                            return error.response.data;
                          });
                        break;

                      case 2:
                        if (JSON.parse(payload.payload).subMeter.length < 1) {
                          return "ok";
                        }
                        let temp_rtu = JSON.parse(
                          payload.payload
                        ).subMeter.filter((valP) => {
                          return (
                            valP.applicationName == res.iot_detail_rtu.model &&
                            valP.serialNumber ==
                              res.iot_detail_rtu.serial_number
                          );
                        })[0].report;

                        const filter_rtu = (rtu, str) => {
                          let data = rtu.filter((valP) => {
                            return valP.name == str;
                          })[0];

                          if (data != undefined) {
                            return data.value;
                          } else {
                            return 0;
                          }
                        };

                        let to_att = {
                          ctlr_id: res.id,
                          auth: "sample string 2",
                          kwh_id: res.id,
                          kwh_type: res.iot_detail_rtu.model,
                          // kwh_type: res.iot_node_type.type_name,
                          type_kwh_code: res.iot_node_type.type_id,
                          va: filter_rtu(temp_rtu, "va"),
                          vb: filter_rtu(temp_rtu, "vb"),
                          vc: filter_rtu(temp_rtu, "vc"),
                          ia: filter_rtu(temp_rtu, "ia"),
                          ib: filter_rtu(temp_rtu, "ib"),
                          ic: filter_rtu(temp_rtu, "ic"),
                          pf: filter_rtu(temp_rtu, "pf"),
                          f: filter_rtu(temp_rtu, "f"),
                          kwh: filter_rtu(temp_rtu, "kwh"),
                          ts: moment(payload.reportTime)
                            .utcOffset("+0700")
                            .format(),
                        };

                        axios
                          .post(res.iot_area.url_to_company, to_att, {
                            timeout: 120000,
                            headers: {
                              authorization:
                                "Bearer " + res.iot_area.company_token,
                            },
                          })
                          .then((response) => {
                            console.log(response, "axios");
                            models.iot_histories
                              .update(
                                {
                                  to_company_response: JSON.stringify(
                                    response.data
                                  ),
                                  to_company_payload: JSON.stringify(to_att),
                                },
                                { where: { id: __responseFinal.id } }
                              )
                              .then(() => {
                                return response.data;
                              });
                          })
                          .catch((error) => {
                            console.log(error.response.data, "axios error");
                            models.iot_histories
                              .update(
                                {
                                  to_company_response: JSON.stringify(
                                    error.response.data
                                  ),
                                  to_company_payload: JSON.stringify(to_att),
                                },
                                { where: { id: __responseFinal.id } }
                              )
                              .then(() => {
                                return error.response.data;
                              });
                          });
                        break;

                      default:
                        break;
                    }
                  } else {
                    console.log("awd");
                    return __responseFinal;
                  }
                });
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: result,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error.message,
          });
        });
    });
  },
};

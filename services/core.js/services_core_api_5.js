const models = require("../../models/index");
const { Op } = require("sequelize");
require("dotenv").config();
const moment = require("moment");
const axios = require("axios");
const axios_reatry = require("axios-retry");
const { admin } = require("../../config/fcm_teknisi");
const { serviceFcm } = require("./services_core_api_fcm");
const { GetNodeLog } = require("../../middlewares/chache");
module.exports = {
  serviceCoreNodeHistoryApi5: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        // get transaction
        transaction = await models.sequelize.transaction();
        let node = GetNodeLog(data.devEUI);
        if (node == null) throw new Error("devEUI not found");
        let live_interval = 0;
        if (node.last_update != null) {
          const endTime = moment(data.report_time);
          const startTime = moment(node.last_update);
          var duration = moment.duration(endTime.diff(startTime));
          live_interval =
            parseInt(duration.asMinutes()) > parseInt(node.setting_interval)
              ? node.setting_interval
              : parseInt(duration.asMinutes());
        }
        let updateNode = {
          interval: node.interval == null ? live_interval : node.interval,
          setting_interval:
            node.setting_interval == null
              ? live_interval
              : node.setting_interval,
          id: node.id,
          last_update: data.report_time,
          previous_update: node.last_update,
          live_interval: live_interval,
          live_previous_meter: node.live_last_meter,
        };
        let battery5 = JSON.parse(data.payload).payload_fields.batteryVoltage;
        battery5 = (
          ((parseFloat(battery5) - 1.8) * 100) /
          (3.65 - 1.8)
        ).toFixed(2);
        updateNode.live_battery = battery5 > 100 ? 100 : battery5;
        updateNode.live_last_meter = JSON.parse(
          data.payload
        ).payload_fields.gasPressure;
        updateNode.live_valve = "N/A";
        updateNode.setting_valve = "N/A";
        await models.iot_nodes.update(updateNode, {
          where: { id: updateNode.id },
          transaction,
        });
        await models.iot_history_pressure_5.create(
          {
            device_id: node.id,
            devEui: node.devEui,
            meter_id: node.meter_id,
            battrey_level: updateNode.live_battery,
            preassure: updateNode.live_last_meter,
            tenantId: node.tenantId,
            internalId: node.internalId,
            reportTime: data.report_time,
            reportTimeFlag: moment(data.report_time)
              .utcOffset("+0700")
              .format("YYYY-MM-DD HH:mm:ss"),
          },
          {
            transaction,
          }
        );
        await transaction.commit();
        resolve({
          responseCode: 200,
          messages: "Successfully Add Log",
        });
      } catch (error) {
        console.log(error);
        if (transaction) await transaction.rollback();
        reject({
          responseCode: 500,
          messages: error.message,
        });
      }
    });
  },
};

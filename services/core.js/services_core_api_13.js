const models = require("../../models/index");
const { Op } = require("sequelize");
require("dotenv").config();
const moment = require("moment");
const axios = require("axios");
const axios_reatry = require("axios-retry");
const { admin } = require("../../config/fcm_teknisi");
const { serviceFcm } = require("./services_core_api_fcm");
const { GetNodeLog } = require("../../middlewares/chache");
module.exports = {
  serviceCoreNodeHistoryApi13: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        let res = await models.iot_nodes.findOne({
          where: { devEui: data.devEUI },
          include: [
            {
              model: models.iot_nodes,
              attributes: [
                "id",
                "sensor_level_offset",
                "areaId",
                "typeId",
                "devEui",
                "last_update",
                "previous_update",
                "live_last_meter",
                "live_previous_meter",
                "is_unsigned",
                "node_link_id",
                "setting_interval",
              ],
            },
            { model: models.iot_internal },
            { model: models.iot_tenant },
            { model: models.iot_area, attributes: { exclude: ["image"] } },
            { model: models.iot_node_type },
            { model: models.iot_detail_rtu },
          ],
        });
        res = JSON.parse(JSON.stringify(res));
        if (res == null || (res.tenantId == null && res.internalId == null)) {
          return null;
        }
        let meter_rtu = JSON.parse(data.payload).subMeter.find((valP) => {
          return valP.applicationName.trim() == res.iot_detail_rtu.model.trim();
        });
        if (meter_rtu == undefined) {
          return null;
        }
        let temp_meter_rtu = meter_rtu.report.find((valP) => {
          console.log(valP.name.toLocaleLowerCase().trim());
          return valP.name.toLocaleLowerCase().trim().includes("waterlevel");
        });
        let flag_distance = false;
        if (temp_meter_rtu == null) {
          temp_meter_rtu = meter_rtu.report.find((valP) => {
            return valP.name
              .toLocaleLowerCase()
              .trim()
              .includes("water distance (m)");
          });
          if (temp_meter_rtu == undefined) {
            return null;
          } else {
            flag_distance = true;
            meter_rtu = temp_meter_rtu;
          }
        } else {
          meter_rtu = temp_meter_rtu;
        }
        let live_interval = 0;
        if (res.last_update != null) {
          const endTime = moment(data.report_time);
          const startTime = moment(res.last_update);
          var duration = moment.duration(endTime.diff(startTime));
          live_interval = duration.asMinutes();
        }
        let updateNode = {
          id: res.id,
          live_last_meter: parseFloat(meter_rtu.value),
          live_previous_meter: res.live_last_meter,
          last_update: data.report_time,
          previous_update: res.last_update,
          live_interval: live_interval,
          live_battery: JSON.parse(data.payload)
            .meter.battery.split("%")
            .join(""),
          live_valve: JSON.parse(data.payload).meter.valve,
        };
        let alrt =
          flag_distance == true
            ? parseFloat(meter_rtu.value)
            : parseFloat(
                (
                  parseFloat(res.sensor_to_botom) -
                  parseFloat(meter_rtu.value) +
                  parseFloat(res.sensor_level_offset)
                ).toFixed(3)
              );
        let payload_water_rtu = {
          sensor_to_botom: parseFloat(res.sensor_to_botom),
          water_surface_to_bottom:
            flag_distance == true
              ? parseFloat(meter_rtu.value)
              : parseFloat(
                  (
                    parseFloat(res.sensor_to_botom) -
                    parseFloat(meter_rtu.value)
                  ).toFixed(3)
                ),
          water_level: parseFloat(meter_rtu.value),
          reportTime: data.report_time,
          reportTimeFlag: moment(data.report_time)
            .utcOffset("+0700")
            .format("YYYY-MM-DD HH:mm:ss"),
          device_id: res.id,
          tenantId: res.tenantId,
          internalId: res.internalId,
          devEui: res.devEui,
          meter_id: res.meter_id,
          alarm_high: alrt >= res.high_alarm ? true : false,
          alarm_low: alrt <= res.low_alarm ? true : false,
        };
        let flag_notif = false;
        let flag_notif_type = "LOW";
        //notif
        if (
          payload_water_rtu.alarm_high == true ||
          payload_water_rtu.alarm_low == true
        ) {
          flag_notif = true;
          flag_notif_type = payload_water_rtu.alarm_high ? "HIGH" : "LOW";
          let temp_update = [];
          let title = `Alarm Node Water Level ${
            payload_water_rtu.high_alarm ? "High" : "LOW"
          }`;
          temp_update.push(
            {
              user_id: 0,
              user_type: "TEKNISI",
              title: title,
              body: ` ${
                res.iot_tenant == null
                  ? res.iot_internal.internal_name
                  : res.iot_tenant.tenant_name
              } - ${res.iot_area.area_name}\n devEui = ${
                res.devEui
              }\n Water Surface To Bottom = ${
                parseFloat(payload_water_rtu.water_surface_to_bottom) +
                parseFloat(res.sensor_level_offset)
              } m\n Water Level = ${
                payload_water_rtu.water_level
              } m\n Sensor To Bottom = ${
                payload_water_rtu.sensor_to_botom
              } m\n `,
              areaId: 0,
              time: moment().format("YYYY-MM-DD HH:mm:ss"),
            },
            {
              user_id: 0,
              user_type: "TEKNISI",
              title: title,
              body: ` ${
                res.iot_tenant == null
                  ? res.iot_internal.internal_name
                  : res.iot_tenant.tenant_name
              } - ${res.iot_area.area_name}\n devEui = ${
                res.devEui
              }\n Water Surface To Bottom = ${
                parseFloat(payload_water_rtu.water_surface_to_bottom) +
                parseFloat(res.sensor_level_offset)
              } m\n Water Level = ${
                payload_water_rtu.water_level
              } m\n Sensor To Bottom = ${
                payload_water_rtu.sensor_to_botom
              } m\n `,
              areaId: res.iot_area.id,
              time: moment().format("YYYY-MM-DD HH:mm:ss"),
            }
          );
          serviceFcm(
            title,
            ` ${
              res.iot_tenant == null
                ? res.iot_internal.internal_name
                : res.iot_tenant.tenant_name
            } - ${res.iot_area.area_name}\n - ${moment().format(
              "YYYY-MM-DD HH:mm:ss"
            )}\n devEui = ${res.devEui}\n Water Surface To Bottom = ${
              parseFloat(payload_water_rtu.water_surface_to_bottom) +
              parseFloat(res.sensor_level_offset)
            } m\n Water Level = ${
              payload_water_rtu.water_level
            } m\n Sensor To Bottom = ${payload_water_rtu.sensor_to_botom} m\n `,
            temp_update,
            res.iot_area.id
          )
            .then((a) => console.log("a"))
            .catch((a) => console.log("a"));
        }
        if (flag_distance) {
          updateNode.field_billing_rtu = "bukan rtu water level";
        } else {
          updateNode.field_billing_rtu = "no";
        }
        await models.iot_nodes.update(updateNode, {
          where: { id: updateNode.id },
        });
        let result = await models.iot_histories_rtu_water_level.create(
          payload_water_rtu
        );
        if (flag_notif) {
          let update_alarm = {
            node_id: res.id,
            areaId: res.areaId,
            devEui: res.devEui,
            update_time: data.report_time,
            threshold:
              flag_notif_type == "LOW" ? res.low_alarm : res.high_alarm,
            threshold_type: flag_notif_type,
            meter_now:
              flag_distance == true
                ? parseFloat(payload_water_rtu.water_surface_to_bottom)
                : parseFloat(payload_water_rtu.water_surface_to_bottom) +
                  parseFloat(res.sensor_level_offset),
            location_name:
              res.iot_tenant == null
                ? res.iot_internal.internal_name
                : res.iot_tenant.tenant_name,
            location_type: res.iot_tenant == null ? "internal" : "tenant",
          };

          let find_log = await models.iot_histories_rtu_water_level.findAll({
            where: {
              device_id: update_alarm.node_id,
              reportTime: {
                [Op.lte]: update_alarm.update_time,
              },
            },
            limit: 5,
            order: [["reportTime", "DESC"]],
          });
          find_log = JSON.parse(JSON.stringify(find_log));
          let log_grafik = [];
          function compare(a, b) {
            if (a.reportTime < b.reportTime) {
              return -1;
            }
            if (a.reportTime > b.reportTime) {
              return 1;
            }
            return 0;
          }
          find_log = find_log.sort(compare);
          find_log.map((val) => {
            log_grafik.push({
              date: val.reportTimeFlag,
              meter:
                flag_distance == true
                  ? parseFloat(val.water_surface_to_bottom)
                  : parseFloat(val.water_surface_to_bottom) +
                    parseFloat(res.sensor_level_offset),
            });
          });
          update_alarm.grafik = JSON.stringify(log_grafik);
          await models.iots_alarm_water_level.create(update_alarm);
        }
        resolve({
          responseCode: 200,
          messages: "result",
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          messages: error.message,
        });
      }
    });
  },
};

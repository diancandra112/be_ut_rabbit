const models = require("../../models/index");
const { Op } = require("sequelize");
require("dotenv").config();
const moment = require("moment");
const axios = require("axios");
const axios_reatry = require("axios-retry");
const { admin } = require("../../config/fcm_teknisi");
const { serviceFcm } = require("./services_core_api_fcm");
const { GetNodeLog } = require("../../middlewares/chache");
module.exports = {
  serviceCoreNodeHistoryApi6: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        let res = await GetNodeLog(data.devEUI);
        if (res == null || (res.tenantId == null && res.internalId == null)) {
          return null;
        }
        let log = JSON.parse(data.payload).meter.exReading;
        let batt = 0;
        try {
          batt = log.find((val) => val.id == 3);
          if (batt == null) {
            batt = 0;
          } else {
            batt = (
              ((parseFloat(batt.value) - 1.8) * 100) /
              (3.65 - 1.8)
            ).toFixed(2);
            batt = batt > 100 ? 100 : batt;
            batt = batt < 0 ? 0 : batt;
          }
        } catch (error) {}
        let data_create = {
          device_id: res.id,
          reporttime: data.report_time,
          reporttimeflag: moment(data.report_time)
            .utcOffset("+0700")
            .format("YYYY-MM-DD HH:mm:ss"),
          tenantid: res.tenantId,
          internalid: res.internalId,
          deveui: data.devEUI,
          meter_id: res.meter_id,
          battrey_level: batt,
        };
        let live_interval = 0;
        if (res.last_update != null) {
          const endTime = moment(data.report_time);
          const startTime = moment(res.last_update);
          var duration = moment.duration(endTime.diff(startTime));
          live_interval = duration.asMinutes();
        }
        let updateNode = {
          id: res.id,
          last_update: data.report_time,
          previous_update: res.last_update,
          live_interval: live_interval,
          live_battery: batt,
        };
        let create_data = await models.iots_history_6.create(data_create);
        let data_ex = [];
        log.map((val) => {
          data_ex.push({
            device_id: res.id,
            log_id: create_data.id,
            name: val.name,
            value: val.value,
          });
        });
        await models.iots_history_6_ex.bulkCreate(data_ex);
        await models.iot_nodes.update(updateNode, {
          where: { id: updateNode.id },
        });
        resolve({
          responseCode: 200,
          messages: create_data,
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          messages: error.message,
        });
      }
      return models.sequelize
        .transaction(async (t) => {})
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: result,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error.message,
          });
        });
    });
  },
};

const models = require("../../models/index");
const { Op } = require("sequelize");
require("dotenv").config();
const moment = require("moment");
const axios = require("axios");
const axios_reatry = require("axios-retry");
const { admin } = require("../../config/fcm_teknisi");
const { serviceFcm } = require("./services_core_api_fcm");
module.exports = {
  serviceCoreNodeHistoryApi35: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        let node = await models.iot_nodes.findOne({
          where: {
            devEui: data.devEUI,
            [Op.or]: {
              tenantId: { [Op.not]: null },
              internalId: { [Op.not]: null },
            },
          },
          attributes: ["id", "devEui", "tenantId", "internalId"],
        });
        node = JSON.parse(JSON.stringify(node));
        let payload = JSON.parse(data.payload);
        let battery = payload.exReading.payload.batteryLevel
          ? payload.exReading.payload.batteryLevel
          : payload.exReading.payload.batteryStatus;

        let data_hist = {
          device_id: node.id,
          latitude: payload.gpsLatitude,
          longitude: payload.gpsLongitude,
          battery: battery,
          report_time: data.report_time,
          report_time_flag: moment(data.report_time)
            .utcOffset("+0700")
            .format("YYYY-MM-DD HH:mm:ss"),
        };
        let update_node = {
          last_update: data.report_time,
          carrunt_latitude: payload.gpsLatitude,
          carrunt_longitude: payload.gpsLongitude,
        };
        if (battery != "CHARGING") {
          update_node.live_battery = battery;
        }
        if (node == null) {
          throw new Error("Invalid Device");
        }
        await models.iot_nodes.update(update_node, {
          where: { id: node.id },
        });
        let log_35 = await models.iot_histori_35.create(data_hist, {});
        resolve({
          responseCode: 200,
          messages: log_35,
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          messages: error.message,
        });
      }
    });
  },
};

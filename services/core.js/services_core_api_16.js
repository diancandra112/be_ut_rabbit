const models = require("../../models/index");
const { Op } = require("sequelize");
require("dotenv").config();
const moment = require("moment");
const axios = require("axios");
const axios_reatry = require("axios-retry");
const { admin } = require("../../config/fcm_teknisi");
const { serviceFcm } = require("./services_core_api_fcm");
module.exports = {
  serviceCoreNodeHistoryApi16: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        let res = await models.iot_nodes.findOne({
          where: {
            devEui: data.devEUI,
            child_app_name: data.child_app_name,
            child_sn: data.child_sn,
          },
          include: [
            { model: models.iot_tenant },
            { model: models.iot_internal },
            {
              model: models.iot_area,
              attributes: { exclude: ["image", "createdAt", "updatedAt"] },
            },
            { model: models.iot_node_type },
            { model: models.iot_detail_rtu },
          ],
        });
        res = JSON.parse(JSON.stringify(res));
        if (res == null || (res.tenantId == null && res.internalId == null)) {
          return null;
        }
        let live_interval = 0;
        if (res.last_update != null) {
          const endTime = moment(data.report_time);
          const startTime = moment(res.last_update);
          var duration = moment.duration(endTime.diff(startTime));
          live_interval = duration.asMinutes();
        }
        let updateNode = {
          id: res.id,
          last_update: data.report_time,
          previous_update: res.last_update,
          live_interval: live_interval,
        };
        let iots_histori_16 = {
          device_id: parseFloat(res.sensor_to_botom),
          reportTime: data.report_time,
          reportTimeFlag: moment(data.report_time)
            .utcOffset("+0700")
            .format("YYYY-MM-DD HH:mm:ss"),
          device_id: res.id,
          tenantId: res.tenantId,
          internalId: res.internalId,
          devEui: res.devEui,
          meter_id: res.meter_id,
          child_app_name: data.child_app_name,
          child_sn: data.child_sn,
          log: JSON.parse(data.payload).report,
        };

        let res_16 = await models.iots_histori_16.create(iots_histori_16);
        res_16 = JSON.parse(JSON.stringify(res_16));
        let log = [];
        JSON.parse(data.payload).report.map((val) => {
          log.push({
            name: val.name,
            value: val.value,
            histori_id: res_16.id,
          });
        });
        await models.iots_type_16_detail.bulkCreate(log);

        await models.iot_nodes.update(updateNode, {
          where: { id: updateNode.id },
        });
        resolve({
          responseCode: 200,
          messages: res_16,
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          messages: error.message,
        });
      }
    });
  },
};

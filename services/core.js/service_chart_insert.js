const models = require("../../models/index");
const { Op } = require("sequelize");
require("dotenv").config();
const moment = require("moment");
const axios = require("axios");
const axios_reatry = require("axios-retry");

module.exports = {
  SvcInsertChart: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_charts_usage
            .findOne({
              where:
                data.type != null
                  ? {
                      nodeId: data.nodeId,
                      type: data.type,
                    }
                  : {
                      nodeId: data.nodeId,
                    },
              order: [["id", "DESC"]],
            })
            .then((result) => {
              result = JSON.parse(JSON.stringify(result));
              if (result == null) {
                data.startMeter = parseFloat(data.meter);
                data.endMeter = parseFloat(data.meter);
                data.usage = parseFloat(data.meter) - parseFloat(data.meter);
                return models.iots_charts_usage.create(data, {
                  transaction: t,
                });
              } else if (
                result != null &&
                result.fdate == data.fdate &&
                result.fhour == data.fhour
              ) {
                data.endMeter = parseFloat(data.meter);
                data.usage =
                  parseFloat(data.meter) - parseFloat(result.startMeter);
                return models.iots_charts_usage.update(data, {
                  where: {
                    nodeId: data.nodeId,
                    typeId: data.typeId,
                    areaId: data.areaId,
                    fdate: data.fdate,
                    fhour: data.fhour,
                  },
                  transaction: t,
                });
              } else if (
                result != null &&
                (result.fdate == data.fdate || result.fdate != data.fdate) &&
                result.fhour != data.fhour
              ) {
                data.startMeter = parseFloat(result.endMeter);
                data.endMeter = parseFloat(data.meter);
                data.usage =
                  parseFloat(data.endMeter) - parseFloat(data.startMeter);
                return models.iots_charts_usage.create(data, {
                  transaction: t,
                });
              }
            });
        })
        .then((result) => {
          resolve("done no error");
        })
        .catch((error) => {
          console.log(error);
          reject("done  error");
        });
    });
  },
};

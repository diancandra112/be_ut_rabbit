const models = require("../../models/index");
const { Op } = require("sequelize");
require("dotenv").config();
const moment = require("moment");
const axios = require("axios");
const axios_reatry = require("axios-retry");
const { admin } = require("../../config/fcm_teknisi");
const { serviceFcm } = require("./services_core_api_fcm");
module.exports = {
  serviceCoreNodeHistoryApi22: (data, node_find) => {
    return new Promise(async (resolve, reject) => {
      try {
        transaction = await models.sequelize.transaction();
        let live_interval = 0;
        if (node_find.last_update != null) {
          const endTime = moment(data.report_time);
          const startTime = moment(node_find.last_update);
          var duration = moment.duration(endTime.diff(startTime));
          live_interval = duration.asMinutes();
        }
        let node_report = JSON.parse(data.payload);
        let updateNode = {
          id: node_find.id,
          last_update: data.report_time,
          previous_update: node_find.last_update,
          live_interval: live_interval,
          live_valve: node_report.meter.valve,
          setting_valve: node_report.meter.valve,
          live_battery: node_report.meter.battery,
        };
        let iot_histori_22 = {
          reportTime: data.report_time,
          reportTimeFlag: moment(data.report_time)
            .utcOffset("+0700")
            .format("YYYY-MM-DD HH:mm:ss"),
          device_id: node_find.id,
          tenantId: node_find.tenantId,
          internalId: node_find.internalId,
          devEui: node_find.devEui,
          meter_id: node_find.meter_id,
        };
        node_report.meter.exReading.map((val) => {
          iot_histori_22[val.name] = val.value;
          updateNode[val.name] = val.value;
        });
        updateNode.live_battery = iot_histori_22.battery || 0;
        await models.iot_nodes.update(updateNode, {
          where: { id: node_find.id },
          transaction,
        });
        let log_22 = await models.iot_histori_22.create(iot_histori_22, {
          transaction,
        });
        await transaction.commit();
        resolve({
          responseCode: 200,
          messages: log_22,
        });
      } catch (error) {
        console.log(error);
        if (transaction) await transaction.rollback();
        reject({
          responseCode: 400,
          messages: error.message,
        });
      }
    });
  },
};

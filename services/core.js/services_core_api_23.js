const models = require("../../models/index");
const { Op } = require("sequelize");
require("dotenv").config();
const moment = require("moment");
const axios = require("axios");
const axios_reatry = require("axios-retry");
const { admin } = require("../../config/fcm_teknisi");
const { serviceFcm } = require("./services_core_api_fcm");
const { GetNodeLog } = require("../../middlewares/chache");
module.exports = {
  serviceCoreNodeHistoryApi23: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        let node = await models.iot_nodes.findOne({
          where: { devEui: data.devEUI },
          attributes: [
            "id",
            "devEui",
            "sensor_level_offset",
            "tenantId",
            "internalId",
            "last_update",
            "live_last_meter",
            "sensor_to_botom",
            "meter_id",
            "high_alarm",
            "low_alarm",
            "areaId",
          ],
          include: [
            {
              model: models.iot_tenant,
              attributes: ["id", "tenant_name"],
            },
            { model: models.iot_internal, attributes: ["id", "internal_name"] },
          ],
        });
        node = JSON.parse(JSON.stringify(node));
        if (
          node == null ||
          (node.tenantId == null && node.internalId == null)
        ) {
          return null;
        }
        let live_interval = 0;
        if (node.last_update != null) {
          const endTime = moment(data.report_time);
          const startTime = moment(node.last_update);
          var duration = moment.duration(endTime.diff(startTime));
          live_interval = duration.asMinutes();
        }
        let data_pyd = JSON.parse(data.payload);
        let meter = data_pyd.meter.exReading.find((val) => val.id == 1);
        let btry = data_pyd.meter.exReading.find((val) => val.id == 3);

        btry = ((parseFloat(btry.value) - 1.8) * 100) / (3.65 - 1.8);
        btry = btry > 100 ? 100 : btry;
        btry = btry < 0 ? 0 : btry;
        meter.value = parseFloat(meter.value) / 1000;
        let updateNode = {
          id: node.id,
          live_last_meter: parseFloat(meter.value),
          live_previous_meter: node.live_last_meter,
          last_update: data.report_time,
          previous_update: node.last_update,
          live_interval: live_interval,
          live_battery: btry,
        };
        let payload_water = {
          sensor_to_botom: parseFloat(node.sensor_to_botom),
          water_depth: parseFloat(meter.value),
          reportTime: data.report_time,
          reportTimeFlag: moment(data.report_time)
            .utcOffset("+0700")
            .format("YYYY-MM-DD HH:mm:ss"),
          device_id: node.id,
          tenantId: node.tenantId,
          internalId: node.internalId,
          devEui: node.devEui,
          meter_id: node.meter_id,
          alarm_high: parseFloat(meter.value) >= node.high_alarm ? true : false,
          alarm_low: parseFloat(meter.value) <= node.low_alarm ? true : false,
        };
        let create_data = await models.iot_history_23.create(payload_water);
        create_data = JSON.parse(JSON.stringify(create_data));
        await models.iot_nodes.update(updateNode, {
          where: { id: updateNode.id },
        });
        if (payload_water.alarm_high || payload_water.alarm_low) {
          let update_alarm = {
            node_id: node.id,
            areaId: node.areaId,
            devEui: node.devEui,
            update_time: data.report_time,
            // threshold: flag_notif_type == "LOW" ? res.low_alarm : res.high_alarm,
            // threshold_type: flag_notif_type,
            meter_now: parseFloat(meter.value),
            location_name:
              node.iot_tenant == null
                ? node.iot_internal.internal_name
                : node.iot_tenant.tenant_name,
            location_type: node.iot_tenant == null ? "internal" : "tenant",
          };
          if (payload_water.alarm_high) {
            update_alarm.threshold = node.high_alarm;
            update_alarm.threshold_type = "HIGH";
          } else if (payload_water.alarm_low) {
            update_alarm.threshold = node.low_alarm;
            update_alarm.threshold_type = "LOW";
          }
          let log_grafik = [
            {
              date: updateNode.last_update,
              meter: updateNode.live_last_meter,
            },
          ];
          let list = await models.iot_history_23.findAll({
            where: {
              device_id: payload_water.device_id,
              reportTime: { [Op.lt]: payload_water.reportTime },
            },
            order: [["reportTime", "DESC"]],
            limit: 5,
            attributes: [
              ["reportTime", "date"],
              ["water_depth", "meter"],
            ],
          });
          list = JSON.parse(JSON.stringify(list));
          log_grafik = [...log_grafik, ...list];
          update_alarm.grafik = JSON.stringify(log_grafik);
          await models.iots_alarm_water_level.create(update_alarm);
        }
        resolve({
          responseCode: 200,
          messages: create_data,
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          messages: error.message,
        });
      }
    });
  },
};

const moment = require("moment");
const models = require("../../models/index");
const { Op } = models.Sequelize;

module.exports = {
  type_7: (data) => {
    return new Promise((resolve, reject) => {
      return models.iot_histories
        .findOne({
          attributes: ["payload"],
          where: { device_id: data.id, tenantId: data.tenantId },
          order: [["reportTime", "ASC"]],
        })
        .then((his_first) => {
          try {
            if (his_first == null) {
              console.log("his_first null", data.id);
              resolve("ok");
              return "null";
            }
            his_first = JSON.parse(his_first.payload);
            let update = {
              id: data.id,
              last_cut_date: moment().format("YYYY-MM-DD"),
            };
            let billing = {
              periode_cut: data.area.iots_cutoffs[0].order,
              minimum_charge: 0,
              no_meter: data.meter_id,
              minimum_charge_total:
                data.iot_tenant.minimum_charge_electricity_ct <= 0
                  ? parseFloat(data.area.minimum_charge_electricity_ct) * 1000
                  : parseFloat(data.iot_tenant.minimum_charge_electricity_ct) *
                    1000,
              billing_type: data.area.billing_charge_type,
              areaId: data.areaId,
              tenantId: data.tenantId,
              nodeId: data.id,
              cut_date: moment()
                .utcOffset("+0700")
                .subtract(1, "day")
                .format("YYYY-MM-DD"),
              periode_billing: moment()
                .utcOffset("+0700")
                .subtract(1, "day")
                .format("YYYY-MM"),
              invoice: data.invoice,
              harga_satuan: data.price,
              ppn: 10,
              biaya_transaksi: 5000,
              biaya_penyesuaian: 0,
              status: "NEW",
              correction_usage: 0,
              denda: 0,
              deveui: data.devEui,
              node_type: "ELECTRICITY CT",
              unit: "kwh",
              typeId: 7,
            };

            if (data.bill.length > 0) {
              billing.start_meter = data.bill[0].end_meter;
              billing.start_date = data.bill[0].end_date;
            } else {
              billing.start_meter = his_first.meter.meterReading;
              billing.start_date = his_first.reportTime;
            }
            return models.iot_histories
              .findOne({
                attributes: ["payload"],
                where: {
                  device_id: data.id,
                  tenantId: data.tenantId,
                  reportTimeFlag: models.sequelize.where(
                    models.sequelize.col("iot_histories.reportTimeFlag"),
                    "<=",
                    data.time
                  ),
                },
                order: [["reportTimeFlag", "DESC"]],
              })
              .then((his_last) => {
                his_last = JSON.parse(JSON.stringify(his_last));
                his_last = JSON.parse(his_last.payload);
                billing.end_meter = his_last.meter.meterReading;
                billing.end_date = his_last.reportTime;
                billing.totalizer = his_last.meter.meterReading;
                billing.usage =
                  parseInt(billing.end_meter) - parseInt(billing.start_meter);

                if (billing.usage < billing.minimum_charge_total) {
                  billing.minimum_charge =
                    billing.minimum_charge_total - billing.usage;
                }
                billing.billing_usage =
                  parseFloat(billing.usage) + billing.minimum_charge;
                return models.iot_nodes
                  .update(update, {
                    where: { id: update.id },
                  })
                  .then((result) => {
                    return models.iots_billing_history
                      .create(billing)
                      .then((result) => {
                        result = JSON.parse(JSON.stringify(result));
                        if (data.flag == 1) {
                          return models.iots_billing_history
                            .update(
                              { log_area_id: result.id },
                              { where: { id: result.id } }
                            )
                            .then((result_1) => {
                              result_1 = JSON.parse(JSON.stringify(result_1));
                              resolve(result_1);
                            })
                            .catch((err) => reject(err.message));
                        } else if (data.flag == 2) {
                          return models.iots_billing_history
                            .findOne({
                              where: {
                                typeId: result.typeId,
                                areaId: result.areaId,
                                billing_type: result.billing_type,
                                log_area_id: { [Op.not]: null },
                              },
                              order: [["id", "DESC"]],
                            })
                            .then((res_find) => {
                              res_find = JSON.parse(JSON.stringify(res_find));
                              return models.iots_billing_history
                                .update(
                                  { log_area_id: res_find.log_area_id },
                                  { where: { id: result.id } }
                                )
                                .then((result_2) => {
                                  result_2 = JSON.parse(
                                    JSON.stringify(result_2)
                                  );
                                  resolve(result_2);
                                })
                                .catch((err) => reject(err.message));
                            })
                            .catch((err) => reject(err.message));
                        } else if (data.flag == 3) {
                          return models.iots_billing_history
                            .update(
                              { log_area_id: result.id },
                              { where: { id: result.id } }
                            )
                            .then(() => {
                              return models.iots_billing_history
                                .findAll({
                                  where: {
                                    log_area_id: result.id,
                                    areaId: result.areaId,
                                    billing_type: result.billing_type,
                                  },
                                  order: [["id", "DESC"]],
                                })
                                .then((res_all) => {
                                  res_all = JSON.parse(JSON.stringify(res_all));
                                  let bill_all = {
                                    is_combine_billing: true,
                                    log_area_id: result.id,
                                    areaId: result.areaId,
                                    cut_date: result.cut_date,
                                    invoice: result.invoice,
                                    ppn: 10,
                                    harga_satuan: result.harga_satuan,
                                    periode_cut: result.periode_cut,
                                    biaya_transaksi: result.biaya_transaksi,
                                    biaya_penyesuaian: "0",
                                    status: "NEW",
                                    correction_usage: "0",
                                    denda: "0",
                                    periode_billing: result.periode_billing,
                                    due_date: null,
                                    node_type: result.node_type,
                                    unit: result.unit,
                                    typeId: result.typeId,
                                    resultErp: null,
                                    payment_method: null,
                                    payment_date: null,
                                    minimum_charge_total:
                                      result.minimum_charge_total,
                                    billing_type: "area",
                                    log: null,
                                    log_flag: true,
                                    no_meter: null,
                                  };
                                  let start_date = null;
                                  let end_date = null;

                                  bill_all.start_meter = 0;
                                  bill_all.end_meter = 0;
                                  res_all.map((value, idx) => {
                                    bill_all.start_meter =
                                      parseFloat(bill_all.start_meter) +
                                      parseFloat(value.start_meter);
                                    bill_all.end_meter =
                                      parseFloat(bill_all.end_meter) +
                                      parseFloat(value.end_meter);
                                    if (start_date == null) {
                                      bill_all.start_date = value.start_date;
                                      start_date = value.start_date;
                                    } else if (start_date > value.start_date) {
                                      bill_all.start_date = value.start_date;
                                      start_date = value.start_date;
                                    }

                                    if (end_date == null) {
                                      bill_all.end_date = value.end_date;
                                      end_date = value.end_date;
                                    } else if (end_date < value.end_date) {
                                      bill_all.end_date = value.end_date;
                                      end_date = value.end_date;
                                    }
                                  });
                                  bill_all.totalizer = bill_all.end_meter;
                                  bill_all.usage =
                                    bill_all.end_meter - bill_all.start_meter;
                                  bill_all.minimum_charge = 0;
                                  if (
                                    bill_all.usage <
                                    bill_all.minimum_charge_total
                                  ) {
                                    bill_all.minimum_charge =
                                      bill_all.minimum_charge_total -
                                      bill_all.usage;
                                  }
                                  bill_all.billing_usage =
                                    parseFloat(bill_all.usage) +
                                    bill_all.minimum_charge;
                                  // result.log_area_id
                                  return models.iots_billing_history
                                    .create(bill_all)
                                    .then((res_create) => {
                                      res_create = JSON.parse(
                                        JSON.stringify(res_create)
                                      );
                                      return models.iots_billing_history
                                        .update(
                                          { log_area_id: res_create.id },
                                          {
                                            where: {
                                              [Op.or]: {
                                                log_area_id: result.id,
                                                id: result.id,
                                              },
                                            },
                                          }
                                        )
                                        .then((result) => {
                                          result = JSON.parse(
                                            JSON.stringify(result)
                                          );
                                          resolve(result);
                                        })
                                        .catch((err) => reject(err.message));
                                    })
                                    .catch((err) => reject(err.message));
                                })
                                .catch((err) => reject(err.message));
                            })
                            .catch((err) => reject(err.message));
                        } else if (data.flag == 0) {
                          return models.iots_billing_history
                            .findOne({
                              where: {
                                typeId: result.typeId,
                                areaId: result.areaId,
                                billing_type: result.billing_type,
                                log_area_id: { [Op.not]: null },
                              },
                              order: [["id", "DESC"]],
                            })
                            .then((res_find) => {
                              res_find = JSON.parse(JSON.stringify(res_find));
                              return models.iots_billing_history
                                .update(
                                  { log_area_id: res_find.log_area_id },
                                  { where: { id: result.id } }
                                )
                                .then(() => {
                                  return models.iots_billing_history
                                    .findAll({
                                      where: {
                                        log_area_id: res_find.log_area_id,
                                        areaId: res_find.areaId,
                                        billing_type: res_find.billing_type,
                                      },
                                      order: [["id", "DESC"]],
                                    })
                                    .then((res_all) => {
                                      res_all = JSON.parse(
                                        JSON.stringify(res_all)
                                      );
                                      let bill_all = {
                                        is_combine_billing: true,
                                        log_area_id: res_find.id,
                                        areaId: result.areaId,
                                        cut_date: result.cut_date,
                                        invoice: result.invoice,
                                        ppn: 10,
                                        harga_satuan: result.harga_satuan,
                                        periode_cut: result.periode_cut,
                                        biaya_transaksi: result.biaya_transaksi,
                                        biaya_penyesuaian: "0",
                                        status: "NEW",
                                        correction_usage: "0",
                                        denda: "0",
                                        periode_billing: result.periode_billing,
                                        due_date: null,
                                        node_type: result.node_type,
                                        unit: result.unit,
                                        typeId: result.typeId,
                                        resultErp: null,
                                        payment_method: null,
                                        payment_date: null,
                                        minimum_charge_total:
                                          result.minimum_charge_total,
                                        billing_type: "area",
                                        log: null,
                                        log_flag: true,
                                        no_meter: null,
                                      };
                                      let start_date = null;
                                      let end_date = null;

                                      bill_all.start_meter = 0;
                                      bill_all.end_meter = 0;
                                      res_all.map((value, idx) => {
                                        bill_all.start_meter =
                                          parseFloat(bill_all.start_meter) +
                                          parseFloat(value.start_meter);
                                        bill_all.end_meter =
                                          parseFloat(bill_all.end_meter) +
                                          parseFloat(value.end_meter);
                                        if (start_date == null) {
                                          bill_all.start_date =
                                            value.start_date;
                                          start_date = value.start_date;
                                        } else if (
                                          start_date > value.start_date
                                        ) {
                                          bill_all.start_date =
                                            value.start_date;
                                          start_date = value.start_date;
                                        }

                                        if (end_date == null) {
                                          bill_all.end_date = value.end_date;
                                          end_date = value.end_date;
                                        } else if (end_date < value.end_date) {
                                          bill_all.end_date = value.end_date;
                                          end_date = value.end_date;
                                        }
                                      });
                                      bill_all.totalizer = bill_all.end_meter;
                                      bill_all.usage =
                                        bill_all.end_meter -
                                        bill_all.start_meter;
                                      bill_all.minimum_charge = 0;
                                      if (
                                        bill_all.usage <
                                        bill_all.minimum_charge_total
                                      ) {
                                        bill_all.minimum_charge =
                                          bill_all.minimum_charge_total -
                                          bill_all.usage;
                                      }
                                      bill_all.billing_usage =
                                        parseFloat(bill_all.usage) +
                                        bill_all.minimum_charge;
                                      // result.log_area_id
                                      return models.iots_billing_history
                                        .create(bill_all)
                                        .then((res_create) => {
                                          res_create = JSON.parse(
                                            JSON.stringify(res_create)
                                          );
                                          return models.iots_billing_history
                                            .update(
                                              { log_area_id: res_create.id },
                                              {
                                                where: {
                                                  [Op.or]: {
                                                    log_area_id:
                                                      res_find.log_area_id,
                                                    id: res_create.id,
                                                  },
                                                },
                                              }
                                            )
                                            .then((result) => {
                                              result = JSON.parse(
                                                JSON.stringify(result)
                                              );
                                              resolve(result);
                                            })
                                            .catch((err) =>
                                              reject(err.message)
                                            );
                                        })
                                        .catch((err) => reject(err.message));
                                    })
                                    .catch((err) => reject(err.message));
                                })
                                .catch((err) => reject(err.message));
                            })
                            .catch((err) => reject(err.message));
                        }
                      })
                      .catch((err) => reject(err.message));
                  })
                  .catch((err) => reject(err.message));
              })
              .catch((err) => reject(err.message));
          } catch (error) {
            console.log(error);
            reject(error.message);
          }
        })
        .catch((err) => {
          console.log(err);
          reject(err.message);
        });
      // .then((result) => {})
      // .catch((err) => reject(err.message));
    });
  },
};

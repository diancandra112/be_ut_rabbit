const moment = require("moment");
const models = require("../../models/index");
const { Svchandle } = require("./service_handle_type");
const { SvcGetPricing1, SvcGetPricing2 } = require("./service_get_pricing_rtu");
const { Op } = models.Sequelize;

module.exports = {
  SvcGetArea1: (data) => {
    return new Promise((resolve, reject) => {
      return models.iot_area
        .findAll({
          attributes: ["id"],
          include: [
            {
              model: models.iots_cutoff,
              attributes: ["id", "areaId", "tanggal_cutoff"],
              required: true,
              where: {
                tanggal_cutoff: moment().utcOffset("+0700").format("DD"),
              },
            },
          ],
        })
        .then((res) => resolve(JSON.parse(JSON.stringify(res))))
        .catch(() => resolve("error"));
    });
  },
  SvcGetAreaRtu2: (data) => {
    return new Promise((resolve, reject) => {
      if (data.pricing_option == 1) {
        SvcGetPricing1(data)
          .then((price) => {
            data.price = price.pricing;
            Svchandle(data)
              .then(() => resolve("OK"))
              .catch(() => resolve("error"));
          })
          .catch(() => reject("error"));
      } else {
        SvcGetPricing2(data)
          .then((price) => {
            data.price = price.pricing;
            Svchandle(data)
              .then(() => resolve("OK"))
              .catch(() => resolve("error"));
          })
          .catch(() => reject("error"));
      }
    });
  },

  SvcGetAreaRtu: (data) => {
    return new Promise((resolve, reject) => {
      return models.iot_area
        .findAll({
          attributes: ["id", "pricing_option", "area_name"],
          include: [
            {
              model: models.iots_cutoff,
              attributes: ["id", "areaId", "tanggal_cutoff"],
              required: true,
              where: {
                tanggal_cutoff: moment().utcOffset("+0700").format("DD"),
                time: {
                  [Op.lte]: moment().utcOffset("+0700").format("HH:mm:ss"),
                },
              },
            },
            {
              model: models.iot_nodes,
              where: {
                tenantId: { [Op.not]: null },
                last_update: { [Op.not]: null },
                device_type_id: 1,
                typeId: 4,
                field_billing_rtu: { [Op.not]: null },
                last_cut_date: {
                  [Op.or]: [
                    null,
                    models.sequelize.where(
                      models.sequelize.fn(
                        "date",
                        models.sequelize.col("iot_nodes.last_cut_date")
                      ),
                      "<",
                      moment().utcOffset("+0700").format("YYYY-MM-DD")
                    ),
                  ],
                },
              },
              include: [
                {
                  model: models.iot_tenant,
                },
                {
                  model: models.iot_detail_rtu,
                },
                {
                  model: models.iots_billing_history,
                  order: [["id", "DESC"]],
                  as: "bill",
                  limit: 1,
                },
              ],
            },
          ],
        })
        .then((area) => {
          area = JSON.parse(JSON.stringify(area));
          console.log(area);
          if (area.length > 0) {
            let device = [];
            area.map((value) => {
              value.iot_nodes.map((value2) => {
                let invoice =
                  "We" +
                  "." +
                  value.area_name +
                  "." +
                  value2.iot_tenant.id +
                  "-INV-" +
                  moment().subtract(1, "day").format("DDMMYY") +
                  "-" +
                  value2.iot_tenant.id +
                  "G";
                // if (value2.bill.length == 0) {
                //   invoice += "-1";
                // } else {
                //   let temp_bill = value2.bill[0].invoice.split("-");
                //   if (
                //     temp_bill[2] !== undefined &&
                //     temp_bill[2].slice(4, 6) ==
                //       moment().utcOffset("+0700").format("YY")
                //   ) {
                //     invoice += "-" + (parseInt(temp_bill[3]) + 1).toString();
                //   } else {
                //     invoice += "-1";
                //   }
                // }

                device.push({
                  invoice: invoice,
                  id: value2.id,
                  model: value2.iot_detail_rtu.model,
                  pricing_option: value.pricing_option,
                  ...value2,
                });
              });
            });
            resolve(device);
          } else {
            resolve(area);
          }
        })
        .catch((error) => {
          console.log(error, "eror");
          reject(error);
        });
    });
  },
};

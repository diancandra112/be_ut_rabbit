const moment = require("moment");
const models = require("../../models/index");
const { Svchandle } = require("./service_handle_type");
const { SvcGetPricing1, SvcGetPricing2 } = require("./service_get_pricing");
const { Op } = models.Sequelize;

module.exports = {
  SvcGetArea1: (data) => {
    return new Promise((resolve, reject) => {
      return models.iot_area
        .findAll({
          attributes: ["id"],
          include: [
            {
              model: models.iots_cutoff,
              attributes: ["id", "areaId", "tanggal_cutoff"],
              required: true,
              where: {
                tanggal_cutoff: moment().utcOffset("+0700").format("DD"),
              },
            },
          ],
        })
        .then((res) => resolve(JSON.parse(JSON.stringify(res))))
        .catch(() => resolve("error"));
    });
  },
  SvcGetArea2: (data) => {
    return new Promise((resolve, reject) => {
      if (data.pricing_option == 1) {
        SvcGetPricing1(data)
          .then((price) => {
            if (price == null) {
              resolve("OK");
            } else {
              data.price = price.pricing;
              Svchandle(data)
                .then(() => resolve("OK"))
                .catch((error) => {
                  console.log(error);
                  resolve(error);
                });
            }
          })
          .catch((error) => {
            console.log(error);
            resolve(error);
          });
      } else {
        SvcGetPricing2(data)
          .then((price) => {
            if (price == null) {
              resolve("OK");
            } else {
              data.price = price.pricing;
              Svchandle(data)
                .then(() => resolve("OK"))
                .catch((error) => {
                  console.log(error);
                  resolve(error);
                });
            }
          })
          .catch((error) => {
            console.log(error);
            resolve(error);
          });
      }
    });
  },
  SvcGetArea3: (data) => {
    return new Promise((resolve, reject) => {
      console.log(data);
      //   resolve("ok");
    });
  },
  SvcGetArea: (data) => {
    return new Promise((resolve, reject) => {
      return models.iot_area
        .findAll({
          where: { billing_charge_type: "area" },
          attributes: [
            "id",
            "pricing_option",
            "area_name",
            "minimum_charge_gas",
            "minimum_charge_water",
            "minimum_charge_electricity_ct",
            "minimum_charge_electricity_non_ct",
            "billing_charge_type",
          ],
          include: [
            {
              model: models.iots_cutoff,
              attributes: ["id", "areaId", "tanggal_cutoff", "time", "order"],
              required: true,
              where: {
                tanggal_cutoff: moment().utcOffset("+0700").format("DD"),
                time: {
                  [Op.lte]: moment().utcOffset("+0700").format("HH:mm:ss"),
                },
              },
            },
            {
              model: models.iot_nodes,
              //   limit: 3,
              where: {
                is_unsigned: { [Op.not]: true },
                tenantId: { [Op.not]: null },
                last_update: { [Op.not]: null },
                device_type_id: 1,
                typeId: data,
                last_cut_date: {
                  [Op.or]: [
                    null,
                    models.sequelize.where(
                      models.sequelize.fn(
                        "date",
                        models.sequelize.col("iot_nodes.last_cut_date")
                      ),
                      "<",
                      moment().utcOffset("+0700").format("YYYY-MM-DD")
                    ),
                  ],
                },
              },
              include: [
                {
                  model: models.iot_tenant,
                },
                {
                  model: models.iots_billing_history,
                  order: [["id", "DESC"]],
                  as: "bill",
                  limit: 1,
                },
              ],
            },
          ],
        })
        .then((area) => {
          area = JSON.parse(JSON.stringify(area));
          if (area.length > 0) {
            let device = [];
            area.map((value) => {
              value.iot_nodes.map((value2, idx) => {
                let invoice =
                  "We" +
                  "." +
                  value.area_name +
                  "." +
                  value2.iot_tenant.id +
                  "-INV-" +
                  moment().subtract(1, "day").format("DDMMYY") +
                  "-" +
                  value2.iot_tenant.id +
                  "G";
                // if (value2.bill.length == 0) {
                //   invoice += "-1";
                // } else {
                //   let temp_bill = value2.bill[0].invoice.split("-");
                //   if (
                //     temp_bill[2] !== undefined &&
                //     temp_bill[2].slice(4, 6) ==
                //       moment().utcOffset("+0700").format("YY")
                //   ) {
                //     invoice += "-" + (parseInt(temp_bill[3]) + 1).toString();
                //   } else {
                //     invoice += "-1";
                //   }
                // }
                let flag = 2;
                if (0 == value.iot_nodes.length - 1) {
                  flag = 3;
                } else if (idx == value.iot_nodes.length - 1) {
                  flag = 0;
                } else if (idx == 0) {
                  flag = 1;
                }

                device.push({
                  flag: flag,
                  time:
                    value.iots_cutoffs.length > 0
                      ? moment().utcOffset("+0700").format("YYYY-MM-DD") +
                        " " +
                        value.iots_cutoffs[0].time
                      : moment().utcOffset("+0700").format("YYYY-MM-DD") +
                        " 00:00:00",
                  area: value,
                  invoice: invoice,
                  id: value2.id,
                  pricing_option: value.pricing_option,
                  ...value2,
                });
              });
            });
            resolve(device);
          } else {
            resolve(area);
          }
        })
        .catch((error) => {
          console.log(error, "eror");
          reject(error);
        });
    });
  },
};

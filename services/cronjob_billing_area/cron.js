const corn = require("node-cron");
const moment = require("moment");
const axios = require("axios");
const models = require("../../models/index");
const { Op } = models.Sequelize;
const {
  SvcGetArea2,
  SvcGetArea,
  SvcGetArea1,
  SvcGetArea3,
} = require("./service_get_area");
const { SvcGetAreaRtu, SvcGetAreaRtu2 } = require("./service_get_area_rtu");

const GenerateBillingV2 = (typeId) => {
  //   const job = corn.schedule("*/30 */2 * * *", () => {
  const job = corn.schedule("*/30 * * * *", () => {
    job.stop();
    console.log("running create billing");
    SvcGetArea(typeId)
      .then((area) => {
        console.log(area, "area");
        area
          .reduce(
            (chain, item) => chain.then(() => SvcGetArea2(item)),
            Promise.resolve()
          )
          .then((s) => {
            console.log(s, "ok");
            job.start();
          })
          .catch((e) => {
            console.log(e, "awdawd");
            job.start();
          });
      })
      .catch((e) => {
        console.log("error");
        job.start();
      });
  });
};

const GenerateBillingRtuV2 = () => {
  //   const job = corn.schedule("*/30 */2 * * *", () => {
  const job = corn.schedule("*/30 * * * *", () => {
    job.stop();
    console.log("running create billing");
    SvcGetAreaRtu()
      .then((area) => {
        console.log(area, "area");
        area
          .reduce(
            (chain, item) => chain.then(() => SvcGetAreaRtu2(item)),
            Promise.resolve()
          )
          .then((s) => {
            console.log(s, "ok");
            job.start();
          })
          .catch((e) => {
            console.log(e, "awdawd");
            job.start();
          });
      })
      .catch((e) => {
        console.log("error");
        job.start();
      });
  });
};

module.exports = {
  all2Area: () => {
    GenerateBillingV2(1);
    GenerateBillingV2(2);
    GenerateBillingV2(3);
    GenerateBillingV2(7);
    // GenerateBillingRtuV2();
  },
};

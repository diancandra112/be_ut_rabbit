const moment = require("moment");
const models = require("../../models/index");
const { Op } = models.Sequelize;

module.exports = {
  type_4: (data) => {
    return new Promise((resolve, reject) => {
      return models.iot_histories
        .findOne({
          attributes: ["payload"],
          where: { device_id: data.id, tenantId: data.tenantId },
          order: [["reportTime", "ASC"]],
        })
        .then((his_first) => {
          try {
            if (his_first == null) {
              console.log("his_first null", data.id);
              resolve("ok");
              return "null";
            }
            his_first = JSON.parse(his_first.payload);
            let update = {
              id: data.id,
              last_cut_date: moment().format("YYYY-MM-DD"),
            };
            let billing = {
              areaId: data.areaId,
              tenantId: data.tenantId,
              nodeId: data.id,
              cut_date: moment()
                .utcOffset("+0700")
                .subtract(1, "day")
                .format("YYYY-MM-DD"),
              periode_billing: moment()
                .utcOffset("+0700")
                .subtract(1, "day")
                .format("YYYY-MM"),
              invoice: data.invoice,
              harga_satuan: data.price,
              ppn: 10,
              biaya_transaksi: 5000,
              biaya_penyesuaian: 0,
              status: "NEW",
              correction_usage: 0,
              denda: 0,
              deveui: data.devEui,
              node_type: "RTU",
              unit:
                data.rtu_pricing_id == 1 || data.rtu_pricing_id == 2
                  ? "kwh"
                  : "m3",
              typeId: 4,
            };
            if (data.bill.length > 0) {
              billing.start_meter = data.bill[0].end_meter;
              billing.start_date = data.bill[0].end_date;
            } else {
              let temp_rtu = his_first.subMeter.filter((valP) => {
                return valP.applicationName == data.model;
              });
              temp_rtu = temp_rtu[0].report.filter((valP) => {
                return valP.name == data.field_billing_rtu;
              });
              billing.start_meter = temp_rtu[0].value;
              billing.start_date = his_first.reportTime;
            }
            return models.iot_histories
              .findOne({
                attributes: ["payload"],
                where: {
                  device_id: data.id,
                  tenantId: data.tenantId,
                  reportTimeFlag: models.sequelize.where(
                    models.sequelize.fn(
                      "date",
                      models.sequelize.col("iot_histories.reportTimeFlag")
                    ),
                    "<",
                    moment().utcOffset("+0700").format("YYYY-MM-DD")
                  ),
                },
                order: [["reportTimeFlag", "DESC"]],
              })
              .then((his_last) => {
                his_last = JSON.parse(JSON.stringify(his_last));
                his_last = JSON.parse(his_last.payload);
                let temp_rtu2 = his_last.subMeter.filter((valP) => {
                  return valP.applicationName == data.model;
                });
                temp_rtu2 = temp_rtu2[0].report.filter((valP) => {
                  return valP.name == data.field_billing_rtu;
                });
                billing.end_meter = temp_rtu2[0].value;
                billing.end_date = his_last.reportTime;
                billing.totalizer = temp_rtu2[0].value;
                billing.usage =
                  parseFloat(billing.end_meter) -
                  parseFloat(billing.start_meter);
                billing.billing_usage = billing.usage;
                return models.iot_nodes
                  .update(update, {
                    where: { id: update.id },
                  })
                  .then((result) => {
                    console.log(billing);
                    return models.iots_billing_history
                      .create(billing)
                      .then((result) => {
                        result = JSON.parse(JSON.stringify(result));
                        resolve(result);
                      })
                      .catch((err) => reject(err.message));
                  })
                  .catch((err) => reject(err.message));
              })
              .catch((err) => reject(err.message));
          } catch (error) {
            console.log(error);
            reject(error.message);
          }
        })
        .catch((err) => {
          console.log(err);
          reject(err.message);
        });
      // .then((result) => {})
      // .catch((err) => reject(err.message));
    });
  },
};

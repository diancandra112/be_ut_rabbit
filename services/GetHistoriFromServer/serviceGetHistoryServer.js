const models = require("../../models/index");
const { weiots } = require("../../config/axios");

require("dotenv").config();
module.exports = {
  count: () => {
    return new Promise((resolve, reject) => {
      models.iot_nodes
        .count()
        .then((result) => {
          const data = [];
          let flag = 1;
          for (let index = 1; index <= result; index++) {
            data.push(flag);
            flag++;
            index += 99;
          }
          resolve(data);
        })
        .catch((err) => resolve(err));
    });
  },
  findAllNode: (data) => {
    return new Promise((resolve, reject) => {
      models.iot_nodes
        .findAll({
          limit: 100,
          offset: (parseInt(data) - 1) * parseInt(100),
          attributes: [["devEui", "node"], "id", "typeId"],
        })
        .then((result1) => {
          result1 = JSON.parse(JSON.stringify(result1));
          weiots({
            method: "POST",
            url: "/client/report/history",
            data: { node: result1 },
          })
            .then((result) => {
              result.data.payload.map((val) => {
                const type = parseInt(val.typeId);
                if (type == 1 || type == 2 || type == 3 || type == 7) {
                  const meter = JSON.parse(val.payload).meter.meterReading;
                  models.iot_histories
                    .findOne({
                      where: { device_id: val.device_id },
                      order: [["id", "DESC"]],
                    })
                    .then((res) => {
                      let usage = 0;
                      if (res != null) {
                        const lastMeter = JSON.parse(res.payload).meter
                          .meterReading;
                        usage = ((meter - lastMeter) / 1000).toFixed(2);
                      } else {
                        usage = usage.toFixed(2);
                      }
                      models.iot_histories.findOrCreate({
                        where: {
                          device_id: val.device_id,
                          reportTime: val.report_time,
                        },
                        defaults: {
                          device_id: val.device_id,
                          payload: val.payload,
                          reportTime: val.report_time,
                          usage: usage,
                        },
                      });
                    });
                } else {
                  models.iot_histories.findOrCreate({
                    where: {
                      device_id: val.device_id,
                      reportTime: val.report_time,
                    },
                    defaults: {
                      device_id: val.device_id,
                      payload: val.payload,
                      reportTime: val.report_time,
                    },
                  });
                }
              });
              resolve("ok");
            })
            .catch((err) => {
              resolve("err");
              console.log(err);
            });
        })
        .catch((err) => reject(err));
    });
  },
  nodeReport: (x, data) => {
    return new Promise((resolve) =>
      setTimeout(() => {
        data = JSON.parse(JSON.stringify(data));
        console.log(
          "========================================================="
        );
        weiots
          .get(process.env.HOST_SERVER + `/get/history?devEUI=${data.devEui}`)
          .then((s) => {
            console.log(data.devEui);
            reportTime = JSON.parse(s.data.node_history.payload).reportTime;
            console.log(
              "+++++++++++++++++++++++++++++++++++++++++++++++++++++++"
            );
            resolve("ok");
            return models.sequelize
              .transaction((t) => {
                return models.iot_histories.findOrCreate({
                  where: {
                    device_id: data.id,
                    reportTime: reportTime,
                  },
                  defaults: {
                    device_id: data.id,
                    payload: s.data.node_history.payload,
                    reportTime: reportTime,
                  },
                  transaction: t,
                });
              })
              .then(() => resolve("ok"))
              .catch((err) => resolve("err"));
          })
          .catch((err) => {
            console.log("errrrrrrr", err);
            resolve("err");
          });
      }, Math.random() * 5000)
    );
  },
};

const models = require("../models/index");
const Op = models.Sequelize.Op;
require("dotenv").config();

module.exports = {
  serviceSetMember: data => {
    return new Promise((resolve, reject) => {
      if (data.name == undefined || data.name.length < 5) {
        reject({
          responseCode: 400,
          messages: "NAME HARUS LEBIH DARI 4 KARAKTER"
        });
        throw new Error();
      } else {
        return models.sequelize
          .transaction(t => {
            return models.iot_member_level.findOrCreate({
              where: data,
              defaults: data,
              transaction: t
            });
          })
          .then(result => {
            resolve({
              responseCode: 200,
              messages: "BERHASIL MENAMBAH MEMBER LEVEL"
            });
          })
          .catch(error => {
            console.log(error);
            reject({
              responseCode: 400,
              messages: "SOMETING ERROR",
              error: JSON.stringify(error)
            });
          });
      }
    });
  },
  serviceGetMember: data => {
    console.log(data);
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction(t => {
          return models.iot_member_level.findAll({
            where: data,
            include: [
              {
                model: models.iot_pricing_postpaid,
                as: "child",
                include: [{ model: models.iot_node_type }]
              }
            ],
            transaction: t
          });
        })
        .then(result => {
          result = JSON.parse(JSON.stringify(result));
          result.map((val, index) => {
            val.child.map((val2, index2) => {
              result[index].child[index2].type_name =
                val2.iot_node_type.type_name;
              // delete result[index].child[index2].iot_node_type;
            });
          });
          resolve({
            responseCode: 200,
            list_member: result
          });
        })
        .catch(error => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: "SOMETING ERROR",
            error: JSON.stringify(error)
          });
        });
    });
  },
  serviceUpdateMember: data => {
    return new Promise((resolve, reject) => {
      if (data.id == undefined) {
        reject({
          responseCode: 400,
          messages: "ID UNDEFINED"
        });
        throw new Error();
      } else {
        return models.sequelize
          .transaction(t => {
            return models.iot_member_level
              .findAll({ where: data })
              .then(res => {
                console.log(res);
                if (res.length != 0) {
                  reject({
                    responseCode: 400,
                    messages: "DUPLICATE MEMBER LEVEL NAME"
                  });
                } else {
                  return models.iot_member_level.update(data, {
                    where: {
                      area_id: data.area_id,
                      id: data.id
                    },
                    transaction: t
                  });
                }
              });
          })
          .then(result => {
            resolve({
              responseCode: 200,
              messages: "BERHASIL UPDATE MEMBER LEVEL"
            });
          })
          .catch(error => {
            console.log(error);
            reject({
              responseCode: 400,
              messages: "SOMETING ERROR",
              error: JSON.stringify(error)
            });
          });
      }
    });
  },
  serviceDeleteMember: data => {
    return new Promise((resolve, reject) => {
      if (data.id == undefined) {
        reject({
          responseCode: 400,
          messages: "ID UNDEFINED"
        });
        throw new Error();
      } else {
        return models.sequelize
          .transaction(t => {
            return models.iot_member_level.destroy({
              where: data,
              transaction: t
            });
          })
          .then(result => {
            resolve({
              responseCode: 200,
              messages: "BERHASIL DELETE MEMBER LEVEL"
            });
          })
          .catch(error => {
            console.log(error);
            reject({
              responseCode: 400,
              messages: "SOMETING ERROR",
              error: JSON.stringify(error)
            });
          });
      }
    });
  }
};

const models = require("../../models/index");
const Op = models.Sequelize.Op;
require("dotenv").config();

module.exports = {
  serviceDeleteFinance: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_finance
            .findOne({
              where: data,
              transaction: t,
            })
            .then((response) => {
              response = JSON.parse(JSON.stringify(response));
              return Promise.all([
                models.iot_users.destroy({
                  where: { id: response.user_id },
                  transaction: t,
                }),
                models.iots_finance.destroy({
                  where: { id: response.id },
                  transaction: t,
                }),
              ]);
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            message: "Successfully Delete Account",
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },

  serviceEditFinanceV2: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        transaction = await models.sequelize.transaction();
        let finance = await models.iots_finance.findOne({
          where: { id: data.id },
        });
        finance = JSON.parse(JSON.stringify(finance));
        await models.iot_users.update(
          {
            email: data.username,
          },
          {
            where: { id: finance.user_id },
            transaction,
          }
        );
        await models.iots_finance.update(data, {
          where: { id: data.id },
          transaction,
        });
        await transaction.commit();
        resolve({
          responseCode: 200,
          message: "account successfully edit",
        });
      } catch (error) {
        console.log(error);
        if (transaction) await transaction.rollback();
        if (error.message.toLocaleLowerCase() == "validation error") {
          reject({
            responseCode: 400,
            response: "Duplicate Username",
          });
        } else {
          reject({
            responseCode: 400,
            response: error.message,
          });
        }
      }
    });
  },
  serviceEditFinance: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_finance
            .findOne({
              where: { id: data.id },
              transaction: t,
            })
            .then((_iots_finance) => {
              return models.iots_finance
                .update(data, {
                  where: { id: data.id },
                  transaction: t,
                })
                .then(() => {
                  return models.iot_users.update(
                    {
                      email: data.username,
                      role_id: 5,
                      area_id: data.area_id,
                    },
                    {
                      where: {
                        email: _iots_finance.username,
                      },
                      transaction: t,
                    }
                  );
                });
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            message: "Successfully Edit Account",
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            message: "Email Duplicate",
            response: err.message,
          });
        });
    });
  },
  serviceViewFinance: (data, area_id) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_finance.findAll({
            where: [data, area_id],
            transaction: t,
          });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            message: "Successfully Get Account",
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },
  serviceRegisterFinance: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_finance
            .findOrCreate({
              where: { email: data.email, area_id: data.area_id },
              defaults: data,
              transaction: t,
            })
            .then(([res, status]) => {
              if (status == false) {
                throw new Error("email already taken");
              } else {
                let user = {
                  email: data.email,
                  password: data.password,
                  role_id: 5,
                  area_id: data.area_id,
                };
                return models.iot_users
                  .findOrCreate({
                    where: { email: user.email },
                    defaults: user,
                    transaction: t,
                  })
                  .then(([res2, status2]) => {
                    if (status2 == false) {
                      throw new Error("email already taken");
                    } else {
                      return models.iots_finance.update(
                        { user_id: res2.id },
                        {
                          where: { id: res.id },
                          transaction: t,
                        }
                      );
                    }
                  });
              }
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            response: result,
            message: "Account successfully created",
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },
  serviceRegisterFinanceV2: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        let user = await models.iot_users.findOne({
          where: { email: data.username, role_id: 5 },
        });
        let finance = await models.iots_finance.findOne({
          where: { username: data.username },
        });
        user = JSON.parse(JSON.stringify(user));
        finance = JSON.parse(JSON.stringify(finance));
        if (user != null || finance != null) {
          return reject({
            responseCode: 403,
            response: "duplicate username",
          });
        }
        let create_user = await models.iot_users.create({
          email: data.username,
          password: data.password,
          role_id: data.role_id,
          area_id: data.area_id,
        });
        data.user_id = create_user.id;
        await models.iots_finance.create(data);
        resolve({
          responseCode: 200,
          message: "Account successfully created",
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          response: error.message,
        });
      }
    });
  },
  servicePwdFinanceV2: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        let finance = await models.iots_finance.findOne({
          where: { id: data.id },
        });
        finance = JSON.parse(JSON.stringify(finance));
        await models.iot_users.update(
          { password: data.password },
          {
            where: { id: finance.user_id },
          }
        );
        resolve({
          responseCode: 200,
          message: "password successfully update",
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          response: error.message,
        });
      }
    });
  },
};

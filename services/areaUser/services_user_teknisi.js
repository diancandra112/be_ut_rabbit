const models = require("../../models/index");
const Op = models.Sequelize.Op;
require("dotenv").config();

module.exports = {
  serviceDeleteTeknisi: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_teknisi
            .findOne({
              where: data,
              transaction: t,
            })
            .then((response) => {
              response = JSON.parse(JSON.stringify(response));
              return Promise.all([
                models.iots_node_type_teknisi.destroy({
                  where: { teknisi_id: response.id },
                  transaction: t,
                }),
                models.iot_users.destroy({
                  where: { email: response.username },
                  transaction: t,
                }),
                models.iots_teknisi.destroy({
                  where: { id: response.id },
                  transaction: t,
                }),
                models.iots_tiketing.destroy({
                  where: { technician_id: response.id },
                  transaction: t,
                }),
              ]);
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            message: "Successfully Delete Account",
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },
  serviceEditStatusTeknisiCompany: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_teknisi.update(data, {
            where: { id: data.id },
            transaction: t,
          });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            message: "Successfully Edit Account",
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },
  serviceEditTeknisiCompany: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_teknisi
            .findOne({
              where: { id: data.id },
              transaction: t,
            })
            .then((_iots_teknisi) => {
              return models.iots_teknisi
                .update(data, {
                  where: { id: data.id },
                  transaction: t,
                })
                .then(() => {
                  return models.iots_node_type_teknisi
                    .destroy({
                      where: { teknisi_id: data.id },
                      transaction: t,
                    })
                    .then(() => {
                      let teknisi_node = [];
                      data.nodeType.map((val) => {
                        teknisi_node.push({
                          teknisi_id: data.id,
                          type_id: val.type_id,
                          type_name: val.type_name,
                        });
                      });
                      return models.iots_node_type_teknisi
                        .bulkCreate(teknisi_node, {
                          where: { teknisi_id: data.id },
                          transaction: t,
                        })
                        .then(() => {
                          let user = {
                            password: data.password,
                          };
                          return models.iot_users.update(user, {
                            where: {
                              id: _iots_teknisi.user_id,
                            },
                            transaction: t,
                          });
                        });
                    });
                });
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            message: "Successfully Edit Account",
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },
  serviceEditTeknisi: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_teknisi
            .findOne({
              where: { id: data.id },
              transaction: t,
            })
            .then((_iots_teknisi) => {
              return models.iots_teknisi
                .update(data, {
                  where: { id: data.id },
                  transaction: t,
                })
                .then(() => {
                  return models.iots_node_type_teknisi
                    .destroy({
                      where: { teknisi_id: data.id },
                      transaction: t,
                    })
                    .then(() => {
                      let teknisi_node = [];
                      data.nodeType.map((val) => {
                        teknisi_node.push({
                          teknisi_id: data.id,
                          type_id: val.type_id,
                          type_name: val.type_name,
                        });
                      });
                      return models.iots_node_type_teknisi
                        .bulkCreate(teknisi_node, {
                          where: { teknisi_id: data.id },
                          transaction: t,
                        })
                        .then(() => {
                          let user = {
                            email: data.username,
                            password: data.password,
                          };
                          return models.iot_users
                            .findOne({
                              where: {
                                email: data.username,
                                role_id: 6,
                                area_id: data.area_id,
                              },
                              transaction: t,
                            })
                            .then((user_iot) => {
                              return models.iot_users.update(user, {
                                where: {
                                  id: user_iot.id,
                                },
                                transaction: t,
                              });
                            });
                        });
                    });
                });
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            message: "Successfully Edit Account",
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },
  serviceViewTeknisi: (data, area_id) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_teknisi.findAll({
            where: [data, area_id],
            include: [{ model: models.iots_node_type_teknisi }],
            transaction: t,
          });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            message: "Successfully Get Account",
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },
  serviceRegisterTeknisi: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_teknisi
            .findOrCreate({
              where: { username: data.username },
              defaults: data,
              transaction: t,
            })
            .then(([res, status]) => {
              if (status == false) {
                throw new Error("username already taken");
              } else {
                let teknisi_node = [];
                data.nodeType.map((val) => {
                  teknisi_node.push({
                    teknisi_id: res.id,
                    type_id: val.type_id,
                    type_name: val.type_name,
                  });
                });
                return models.iots_node_type_teknisi
                  .bulkCreate(teknisi_node, {
                    transaction: t,
                  })
                  .then(() => {
                    let user = {
                      email: data.username,
                      password: data.password,
                      role_id: 6,
                      area_id: data.area_id,
                    };
                    return models.iot_users
                      .findOrCreate({
                        where: { email: user.email },
                        defaults: user,
                        transaction: t,
                      })
                      .then(([res2, status2]) => {
                        if (status2 == false) {
                          throw new Error("username already taken");
                        } else {
                          return models.iots_teknisi.update(
                            { user_id: res2.id },
                            {
                              where: { id: res.id },
                              transaction: t,
                            }
                          );
                        }
                      });
                  });
              }
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            response: result,
            message: "Account successfully created",
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },
  serviceRegisterTeknisiCompany: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_teknisi
            .findOrCreate({
              where: { username: data.username },
              defaults: data,
              transaction: t,
            })
            .then(([res, status]) => {
              if (status == false) {
                throw new Error("username already taken");
              } else {
                let teknisi_node = [];
                data.nodeType.map((val) => {
                  teknisi_node.push({
                    teknisi_id: res.id,
                    type_id: val.type_id,
                    type_name: val.type_name,
                  });
                });
                return models.iots_node_type_teknisi
                  .bulkCreate(teknisi_node, {
                    transaction: t,
                  })
                  .then(() => {
                    let user = {
                      email: data.username,
                      password: data.password,
                      role_id: 8,
                    };
                    return models.iot_users
                      .findOrCreate({
                        where: { email: user.email },
                        defaults: user,
                        transaction: t,
                      })
                      .then(([res2, status2]) => {
                        if (status2 == false) {
                          throw new Error("username already taken");
                        } else {
                          return models.iots_teknisi.update(
                            { user_id: res2.id },
                            {
                              where: { id: res.id },
                              transaction: t,
                            }
                          );
                        }
                      });
                  });
              }
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            response: result,
            message: "Account successfully created",
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },

  servicePwdTeknisi: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        let teknisi = await models.iots_teknisi.findOne({
          where: { id: data.id },
        });
        teknisi = JSON.parse(JSON.stringify(teknisi));
        await models.iot_users.update(
          { password: data.password },
          {
            where: { id: teknisi.user_id },
          }
        );
        resolve({
          responseCode: 200,
          message: "password successfully update",
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          response: error.message,
        });
      }
    });
  },

  serviceEditTeknisiV2: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        transaction = await models.sequelize.transaction();
        let teknisi = await models.iots_teknisi.findOne({
          where: { id: data.id },
          transaction,
        });

        await models.iots_teknisi.update(data, {
          where: { id: data.id },
          transaction,
        });
        await models.iot_users.update(
          {
            email: data.username,
          },
          {
            where: { id: teknisi.user_id },
            transaction,
          }
        );

        await models.iots_node_type_teknisi.destroy({
          where: { teknisi_id: data.id },
          transaction,
        });
        let teknisi_node = [];
        data.nodeType.map((val) => {
          teknisi_node.push({
            teknisi_id: data.id,
            type_id: val.type_id,
            type_name: val.type_name,
          });
        });
        await models.iots_node_type_teknisi.bulkCreate(teknisi_node, {
          where: { teknisi_id: data.id },
          transaction,
        });

        await transaction.commit();
        resolve({
          responseCode: 200,
          message: "account successfully edit",
        });
      } catch (error) {
        console.log(error);
        if (transaction) await transaction.rollback();
        if (error.message.toLocaleLowerCase() == "validation error") {
          reject({
            responseCode: 400,
            response: "Duplicate Username",
          });
        } else {
          reject({
            responseCode: 400,
            response: error.message,
          });
        }
      }
    });
  },

  serviceRegisterTeknisiV2: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        transaction = await models.sequelize.transaction();
        let user = await models.iot_users.create(
          {
            email: data.username,
            password: data.password,
            role_id: data.role_id,
            area_id: data.area_id,
          },
          {
            transaction,
          }
        );
        data.user_id = user.id;
        let teknisi = await models.iots_teknisi.create(data, {
          transaction,
        });
        let teknisi_node = [];
        data.nodeType.map((val) => {
          teknisi_node.push({
            teknisi_id: teknisi.id,
            type_id: val.type_id,
            type_name: val.type_name,
          });
        });
        await models.iots_node_type_teknisi.bulkCreate(teknisi_node, {
          transaction,
        });
        await transaction.commit();
        resolve({
          responseCode: 200,
          message: "account successfully add",
        });
      } catch (error) {
        console.log(error);
        if (transaction) await transaction.rollback();
        if (error.message.toLocaleLowerCase() == "validation error") {
          reject({
            responseCode: 400,
            response: "Duplicate Username",
          });
        } else {
          reject({
            responseCode: 400,
            response: error.message,
          });
        }
      }
    });
  },

  servicePwdTeknisiCompany: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        let teknisi = await models.iots_teknisi.findOne({
          where: { id: data.id },
        });
        teknisi = JSON.parse(JSON.stringify(teknisi));
        await models.iot_users.update(
          { password: data.password },
          {
            where: { id: teknisi.user_id },
          }
        );
        resolve({
          responseCode: 200,
          message: "password successfully update",
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          response: error.message,
        });
      }
    });
  },
};

const models = require("../../models/index");
const Op = models.Sequelize.Op;
require("dotenv").config();

module.exports = {
  serviceDeleteAdmin: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_admin
            .findOne({
              where: data,
              transaction: t,
            })
            .then((response) => {
              response = JSON.parse(JSON.stringify(response));
              return Promise.all([
                models.iot_users.destroy({
                  where: { id: response.user_id },
                  transaction: t,
                }),
                models.iots_admin.destroy({
                  where: { id: response.id },
                  transaction: t,
                }),
              ]);
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            message: "Successfully Delete Account",
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },
  serviceEditAdminV2: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        transaction = await models.sequelize.transaction();
        let admin = await models.iots_admin.findOne({
          where: { id: data.id },
        });
        admin = JSON.parse(JSON.stringify(admin));
        await models.iot_users.update(
          {
            email: data.username,
          },
          {
            where: { id: admin.user_id },
            transaction,
          }
        );
        await models.iots_admin.update(data, {
          where: { id: data.id },
          transaction,
        });
        await transaction.commit();
        resolve({
          responseCode: 200,
          message: "account successfully edit",
        });
      } catch (error) {
        console.log(error);
        if (transaction) await transaction.rollback();
        if (error.message.toLocaleLowerCase() == "validation error") {
          reject({
            responseCode: 400,
            response: "Duplicate Username",
          });
        } else {
          reject({
            responseCode: 400,
            response: error.message,
          });
        }
      }
    });
  },
  serviceEditAdmin: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction(async (t) => {
          let admin = await models.iots_admin.findAll({
            where: { [Op.or]: { id: data.id, email: data.email } },
            transaction: t,
          });
          admin = JSON.parse(JSON.stringify(admin));
          if (admin.length > 1) throw new Error("Email Already Taken");
          let user = await models.iot_users.findAll({
            where: { [Op.or]: { id: admin[0].user_id, email: data.email } },
            transaction: t,
          });
          user = JSON.parse(JSON.stringify(user));
          if (user.length > 1) throw new Error("Email Already Taken");

          let update_admin = await models.iots_admin.update(data, {
            where: { id: data.id },
            transaction: t,
          });
          let update_user = await models.iot_users.update(
            { email: data.email, password: data.password },
            {
              where: { id: admin[0].user_id },
              transaction: t,
            }
          );
          return update_admin;
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            message: "Successfully Edit Account",
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },
  serviceViewAdmin: (data, area_id) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_admin.findAll({
            where: [data, area_id],
            transaction: t,
          });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            message: "Successfully Get Account",
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },
  serviceRegisterAdmin: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_admin
            .findOrCreate({
              where: { username: data.email },
              defaults: data,
              transaction: t,
            })
            .then(([res, status]) => {
              if (status == false) {
                throw new Error("email already taken");
              } else {
                let user = {
                  email: data.email,
                  password: data.password,
                  role_id: 7,
                  area_id: data.area_id,
                };
                return models.iot_users
                  .findOrCreate({
                    where: { email: data.email },
                    defaults: user,
                    transaction: t,
                  })
                  .then(([res_user, status_user]) => {
                    if (status_user == false) {
                      throw new Error("email already taken");
                    } else {
                      return models.iots_admin.update(
                        { user_id: res_user.id },
                        {
                          where: { id: res.id },
                          transaction: t,
                        }
                      );
                    }
                  });
              }
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            response: result,
            message: "Account successfully created",
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },

  serviceRegisterAdminV2: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        transaction = await models.sequelize.transaction();
        let user = await models.iot_users.create(
          {
            email: data.username,
            password: data.password,
            role_id: data.role_id,
            area_id: data.area_id,
          },
          {
            transaction,
          }
        );
        data.user_id = user.id;
        await models.iots_admin.create(data, {
          transaction,
        });
        await transaction.commit();
        resolve({
          responseCode: 200,
          message: "account successfully add",
        });
      } catch (error) {
        console.log(error);
        if (transaction) await transaction.rollback();
        if (error.message.toLocaleLowerCase() == "validation error") {
          reject({
            responseCode: 400,
            response: "Duplicate Username",
          });
        } else {
          reject({
            responseCode: 400,
            response: error.message,
          });
        }
      }
    });
  },

  servicePwdAminV2: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        let admin = await models.iots_admin.findOne({
          where: { id: data.id },
        });
        admin = JSON.parse(JSON.stringify(admin));
        await models.iot_users.update(
          { password: data.password },
          {
            where: { id: admin.user_id },
          }
        );
        resolve({
          responseCode: 200,
          message: "password successfully update",
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 400,
          response: error.message,
        });
      }
    });
  },
};

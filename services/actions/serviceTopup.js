const { weiots } = require("../../config/axios");
const models = require("../../models/index");
module.exports = {
  SvcSetTopup: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return weiots({
            method: "post",
            url: `/action/topup/${data.devEUI}/${data.topup}`,
          });
          // .then((axis) => {
          //   return models.iot_nodes.update(
          //     {
          //       setting_valve:
          //         data.valve == parseInt(data.valve) ? "open" : "close",
          //     },
          //     { where: { devEui: data.devEUI }, transaction: t }
          //   );
          // });
        })
        .then((response) => {
          resolve({
            responseCode: 200,
            message: "Status Akan Berubah Setelah Satu Siklus",
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 200,
            message: err.response.data + " " + err.message,
          });
        });
    });
  },
};

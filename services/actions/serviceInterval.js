const { weiots } = require("../../config/axios");
const models = require("../../models/index");
module.exports = {
  SvcSetInterval: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return weiots({
            method: "post",
            url: `/action/interval/${data.devEUI}/${data.interval}`,
          }).then(() => {
            return models.iot_nodes.update(
              {
                interval: (parseInt(data.interval) / 60).toString(),
                live_interval: (parseInt(data.interval) / 60).toString(),
                setting_interval: (parseInt(data.interval) / 60).toString(),
              },
              { where: { devEui: data.devEUI }, transaction: t }
            );
          });
        })
        .then((response) => {
          resolve({
            responseCode: 200,
            message: "Interval Akan Berubah Setelah Satu Siklus",
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            message: err.response.data + " " + err.message,
          });
        });
    });
  },
};

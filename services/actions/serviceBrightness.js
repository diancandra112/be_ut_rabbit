const { weiots } = require("../../config/axios");
const models = require("../../models/index");
module.exports = {
  // .then((axis) => {
  //   return models.iot_nodes.update(
  //     {
  //       setting_valve: parseInt(data.valve) == 1 ? "open" : "close",
  //     },
  //     { where: { devEui: data.devEUI }, transaction: t }
  //   );
  // });

  SvcSetBrightness: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          ///brightness/:value/:devEui
          return weiots({
            method: "post",
            url: `/action/brightness/${data.value}/${data.devEUI}`,
          });
        })
        .then((response) => {
          resolve({
            responseCode: 200,
            message: "Status Akan Berubah Setelah Satu Siklus",
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 200,
            message: err.response.data + " " + err.message,
          });
        });
    });
  },
};

const { weiots } = require("../../config/axios");
const models = require("../../models/index");
module.exports = {
  SvcSetPulse: (data) => {
    return new Promise(async (resolve, reject) => {
      try {
        let node = await models.iot_nodes.findOne({
          where: { devEui: data.devEUI },
          attributes: ["live_last_meter", "id", "devEui"],
        });
        node = JSON.parse(JSON.stringify(node));
        let count = 0;
        if (!isNaN(parseFloat(node.live_last_meter))) {
          count = parseFloat(node.live_last_meter);
        }
        await weiots({
          method: "post",
          url: `/action/pulse/${data.devEUI}/${data.pulse}/${count}`,
        });
        await models.iot_nodes.update(
          { setting_pulse: data.pulse },
          {
            where: { devEui: data.devEUI },
          }
        );
        return resolve({
          responseCode: 200,
          message: "Status Akan Berubah Setelah Satu Siklus",
        });
      } catch (error) {
        console.log(err);
        reject({
          responseCode: 500,
          message: error.message,
        });
      }
    });
  },
};

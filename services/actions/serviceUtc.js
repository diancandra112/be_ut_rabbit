const { weiots } = require("../../config/axios");
const models = require("../../models/index");
module.exports = {
  SvcSetUtc: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return weiots({
            method: "post",
            url: `/action/utc/${data.devEUI}`,
          });
        })
        .then((response) => {
          resolve({
            responseCode: 200,
            message: "Status Akan Berubah Setelah Satu Siklus",
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 200,
            message: err.response.data + " " + err.message,
          });
        });
    });
  },
};

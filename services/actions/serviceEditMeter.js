const { weiots } = require("../../config/axios");
const models = require("../../models/index");
module.exports = {
  SvcEditMeter: (data, payload, type) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          switch (type) {
            case 1:
              return models.iot_detail_gas
                .update(payload, { where: data })
                .then(() => {
                  return models.iot_nodes.update(payload, {
                    where: { id: data.node_id },
                  });
                });
              break;
            case 2:
              return models.iot_detail_water
                .update(payload, { where: data })
                .then(() => {
                  return models.iot_nodes.update(payload, {
                    where: { id: data.node_id },
                  });
                });
              break;
            case 3:
              return models.iot_detail_electric
                .update(payload, { where: data })
                .then(() => {
                  return models.iot_nodes.update(payload, {
                    where: { id: data.node_id },
                  });
                });
              break;
            case 7:
              return models.iot_detail_electricct
                .update(payload, { where: data })
                .then(() => {
                  return models.iot_nodes.update(payload, {
                    where: { id: data.node_id },
                  });
                });
              break;

            default:
              throw new Error("typeId Not Supported");
          }
        })
        .then((response) => {
          resolve({
            responseCode: 200,
            message: "Sukses Edit Meter",
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            message: err.message,
          });
        });
    });
  },
};

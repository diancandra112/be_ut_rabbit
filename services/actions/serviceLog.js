const models = require("../../models/index");
const { Op } = models.Sequelize;
const moment = require("moment");

module.exports = {
  SvcGetLogActions: (
    data,
    search = "",
    action = "",
    date_start = undefined,
    date_end = undefined,
    page = 1,
    size = 10,
    areaId
  ) => {
    return new Promise((resolve, reject) => {
      delete data.deveui;
      delete data.search;
      delete data.email;
      delete data.action;
      delete data.date_end;
      delete data.date_start;
      delete data.page;
      delete data.size;
      let date = undefined;
      if (date_start != undefined && date_start != "undefined") {
        date = {
          start_date: models.sequelize.where(
            models.sequelize.fn(
              "date",
              models.sequelize.col("iots_log_downlink.time")
            ),
            ">=",
            moment(date_start).format("YYYY-MM-DD")
          ),
          start_end: models.sequelize.where(
            models.sequelize.fn(
              "date",
              models.sequelize.col("iots_log_downlink.time")
            ),
            "<=",
            moment(date_end).format("YYYY-MM-DD")
          ),
        };
      }
      return models.sequelize
        .transaction((t) => {
          return models.iots_log_downlink.findAndCountAll({
            limit: parseInt(size),
            offset: (parseInt(page) - 1) * parseInt(size),
            where: [
              areaId,
              {
                [Op.or]: {
                  deveui: { [Op.substring]: search },
                  email: { [Op.substring]: search },
                },
                action: { [Op.substring]: action },
              },
              data,
              date,
            ],
            include: [
              {
                model: models.iot_nodes,
                attributes: ["id"],
                required: true,
                include: [{ model: models.iot_tenant, required: true }],
              },
            ],
            transaction: t,
          });
        })
        .then((response) => {
          response = JSON.parse(JSON.stringify(response));
          response.rows.map((val, idx) => {
            response.rows[idx].tenant_name =
              val.iot_node.iot_tenant.tenant_name;

            delete response.rows[idx].iot_node;
          });
          resolve({
            responseCode: 200,
            page: page,
            size: size,
            total: response.count,
            result: response.rows,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            message: err.message,
          });
        });
    });
  },
  SvcLogActions: (
    action = "",
    value = 0,
    email = "",
    deveui = "",
    unit = "",
    areaId = 0
  ) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_log_downlink.create(
            {
              time: moment().utcOffset("+0700").format(),
              action: action,
              value: value,
              unit: unit,
              deveui: deveui,
              email: email,
              areaId: areaId,
            },
            { transaction: t }
          );
        })
        .then((response) => {
          resolve({
            responseCode: 200,
            message: "berhasil set log",
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            message: err.message,
          });
        });
    });
  },
};

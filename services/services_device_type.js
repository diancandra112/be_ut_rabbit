const models = require('../models/index')
const jwt = require('jsonwebtoken')
require('dotenv').config()


module.exports={
    serviceGetDeviceType:(data)=>{
        return new Promise((resolve, reject)=>{
            models.iot_device_type.findAll({
                where:data
            })
            .then((result)=>{
                result = JSON.parse(JSON.stringify(result))
                resolve({
                    responseCode:200,
                    list_device_type:result
                })
            })
            .catch((error)=>{
                console.log(error)
            })
        })
    },
}
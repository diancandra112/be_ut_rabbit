const {
  serviceAreaRegister,
  serviceAreaGet,
  serviceAreaProfile,
  serviceAreaProfileErp,
} = require("./services_area");
const { serviceUserLogin } = require("./services_users");
const { serviceCompaniesRegister } = require("./services_company");
const {
  serviceTenantRegister,
  serviceTenantGet,
} = require("./services_tenant");

module.exports = {
  serviceTenantGet,
  serviceAreaProfile,
  serviceAreaGet,
  serviceAreaRegister,
  serviceCompaniesRegister,
  serviceUserLogin,
  serviceTenantRegister,
  serviceAreaProfileErp,
};

const nodemailer = require("nodemailer");
const { Billing } = require("../../emailPayload/tahigan");
const { transporter } = require("../../config/email");
const { midtrans, fasapay } = require("../../config/axios");
const models = require("../../models/index");
const fs = require("fs");
require("dotenv").config();
module.exports = {
  svcUpdateBilling2: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_billing_history.update(data, {
            where: { id: data.id },
            transaction: t,
          });
        })
        .then((result) => resolve(result))
        .catch((err) => {
          console.log(err);
          reject(err.message);
        });
    });
  },
  svcCheckBilling: (data, dataToErp) => {
    return new Promise((resolve, reject) => {
      return (
        models.sequelize
          .transaction((t) => {
            return models.iots_billing_history
              .findOne({
                where: { id: data.billing_id },
                include: [
                  { model: models.iot_nodes },
                  {
                    model: models.iot_area,
                    attributes: {
                      exclude: ["createdAt", "updatedAt"],
                    },
                    include: [
                      { model: models.iot_pricing_postpaid_all },
                      { model: models.iot_pricing_billing_area },
                      { model: models.iots_erp_item },
                    ],
                  },
                  {
                    model: models.iot_tenant,
                    include: [
                      {
                        model: models.iot_member_level,
                        include: [
                          { model: models.iot_pricing_postpaid, as: "child" },
                        ],
                      },
                    ],
                  },
                ],
              })
              .then((res) => {
                res = JSON.parse(JSON.stringify(res));
                if (res.iot_area.is_erp == false) {
                  resolve({
                    responseCode: 200,
                    data: res,
                  });
                } else {
                  if (
                    res.is_combine_billing == true ||
                    res.billing_type == "node"
                  ) {
                    dataToErp.customer_id = res.iot_area.erp_id;
                  } else {
                    if (res.iot_tenant.erp_id == null)
                      throw new Error("customer id to erp is null");

                    dataToErp.customer_id = res.iot_tenant.erp_id;
                  }
                  if (res.is_combine_billing == true) {
                    let temp_post_area =
                      res.iot_area.iot_pricing_billing_areas.filter((valP) => {
                        return valP.typeId == res.typeId;
                      });

                    if (
                      temp_post_area[0].item_id == null ||
                      temp_post_area[0].unit_id == null
                    )
                      throw new Error("unit_id or item_id is null");

                    dataToErp.item_id = temp_post_area[0].item_id;
                    dataToErp.unit_id = temp_post_area[0].unit_id;
                  } else if (res.billing_type == "node") {
                    if (
                      res.iot_area.saas_item_id == null ||
                      res.iot_area.saas_unit_id == null
                    )
                      throw new Error("unit_id or item_id is null");

                    dataToErp.item_id = res.iot_area.saas_item_id;
                    dataToErp.unit_id = res.iot_area.saas_unit_id;
                  } else if (res.iot_area.pricing_option == 1) {
                    let temp_post = [];

                    if (res.typeId == 4) {
                      // res.iot_node.rtu_pricing_id

                      temp_post = res.iot_area.iot_pricing_postpaid_alls.filter(
                        (valP) => {
                          return valP.typeId == res.iot_node.rtu_pricing_id;
                        }
                      );
                    } else {
                      temp_post = res.iot_area.iot_pricing_postpaid_alls.filter(
                        (valP) => {
                          return valP.typeId == res.typeId;
                        }
                      );
                    }
                    console.log(temp_post[0]);
                    if (
                      temp_post[0] == undefined ||
                      temp_post[0].item_id == null ||
                      temp_post[0].unit_id == null
                    )
                      throw new Error("unit_id or item_id is null");

                    dataToErp.item_id = temp_post[0].item_id;
                    dataToErp.unit_id = temp_post[0].unit_id;
                  } else {
                    let temp_post2 = [];

                    if (res.typeId == 4) {
                      // res.iot_node.rtu_pricing_id

                      temp_post2 = res.iot_tenant.iot_member_level.child.filter(
                        (valP) => {
                          return valP.typeId == res.iot_node.rtu_pricing_id;
                        }
                      );
                    } else {
                      temp_post2 = res.iot_tenant.iot_member_level.child.filter(
                        (valP) => {
                          return valP.typeId == res.typeId;
                        }
                      );
                    }
                    if (
                      temp_post2[0] == undefined ||
                      temp_post2[0].item_id == null ||
                      temp_post2[0].unit_id == null
                    )
                      throw new Error("unit_id or item_id is null");

                    dataToErp.item_id = temp_post2[0].item_id;
                    dataToErp.unit_id = temp_post2[0].unit_id;
                  }

                  resolve({
                    responseCode: 200,
                    data: res,
                    dataToErp: dataToErp,
                  });
                }
              });
          })
          // .then((res) => {
          //   resolve({ responseCode: 200, data: res, dataToErp: dataToErp });
          // })
          .catch((err) => {
            console.error(err);
            reject({ responseCode: 400, data: err.message });
          })
      );
    });
  },
  svcUpdateBilling: (data, attachments, attachmentsInv, attachment_log) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_billing_history
            .update(data, {
              where: { id: data.id },
              transaction: t,
            })
            .then(() => {
              let att = [];
              console.log(data, attachments, attachmentsInv, attachment_log);
              attachments.map((val) => {
                att.push({
                  billingId: data.id,
                  file_name: val.filename,
                  file_pdf: val.filename,
                  default: false,
                });
              });
              attachmentsInv.map((val) => {
                att.push({
                  billingId: data.id,
                  file_name: val.filename,
                  file_pdf: val.filename,
                  default: true,
                  status_id: 1,
                  status_name: "INV",
                });
              });
              attachment_log.map((val) => {
                att.push({
                  billingId: data.id,
                  file_name: val.filename,
                  file_pdf: val.filename,
                  default: true,
                  status_id: 2,
                  status_name: "LOG",
                });
              });
              return models.iots_billing_attachment.bulkCreate(att, {
                transaction: t,
              });
            });
        })
        .then((result) => resolve(result.data))
        .catch((err) => {
          console.log(err);
          reject(err.message);
        });
    });
  },
  svcFasapay: (data) => {
    return new Promise((resolve, reject) => {
      fasapay({
        method: "POST",
        url: "/api/va",
        data: data,
      })
        .then((result) => resolve(result.data))
        .catch((err) => {
          console.log(err);
          resolve({ is_error: true, msg: err.response.data });
        });
    });
  },
  svcMidtrans: (data) => {
    return new Promise((resolve, reject) => {
      midtrans({
        method: "POST",
        url: "/core/charge",
        data: data,
      })
        .then((result) => resolve(result.data))
        .catch((err) => {
          console.log(err);
          resolve({ is_error: true, msg: err.response.data });
        });
    });
  },
  svcSendEmailTest: (data, data2) => {
    return new Promise((resolve, reject) => {
      var mailOptions = {
        from: "we@wiraenergi.co.id",
        to: "diancandra112@gmail.com",
        subject: "E-billing periode Maret 2021",
        text: "res",
      };

      transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
          console.log(error);
          reject(error);
        } else {
          resolve(`Email sent: diancandra112@gmail.com` + info.response);
        }
      });
    });
  },
  svcSendEmailBilling: (data, data2, subject, materai) => {
    return new Promise((resolve, reject) => {
      let attachments = [];
      data.attachmentsAll.map((val) => {
        attachments.push({
          filename: val.filename,
          content: fs.createReadStream("./attachment/" + val.filename),
        });
      });
      Billing(data, data2, materai).then((res) => {
        var mailOptions = {
          from: process.env.EMAIL_USER,
          to: data.email_to,
          cc: data.cc,
          subject: subject,
          html: res,
          attachments: attachments,
          headers: {
            "x-priority": "1",
            "x-msmail-priority": "High",
            importance: "high",
          },
        };

        transporter.sendMail(mailOptions, function (error, info) {
          if (error) {
            reject(error);
          } else {
            resolve(`Email sent: ${data.email_to}` + info.response);
          }
        });
      });
    });
  },
  svcBillingUpdate: (data) => {
    return new Promise((resolve, reject) => {
      let attachments = [];
      data.attachments.map((val) => {
        attachments.push({
          filename: val.name,
          content: fs.createReadStream("./pdf/" + val.name),
        });
      });
      Billing(data).then((res) => {
        var transporter = nodemailer.createTransport({
          service: "gmail",
          auth: {
            user: "testbilling@trimagnus.com",
            pass: "anggurzaitun10",
          },
        });

        var mailOptions = {
          from: "smiots7@gmail.com",
          to: "diancandra@trimagnus.com,smiots7@gmail.com",
          cc: "diancandra112@gmail.com",
          subject: "Password Reset",
          html: res,
          attachments: attachments,
        };

        transporter.sendMail(mailOptions, function (error, info) {
          if (error) {
            reject(error);
          } else {
            resolve("Email sent: " + info.response);
          }
        });
      });
    });
  },
};

const models = require("../../models/index");
const { Op } = models.Sequelize;
const moment = require("moment");
require("dotenv").config();

module.exports = {
  svcCreateBillingAll: async (data) => {
    return new Promise(async (resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_area
            .findAll({
              where: { pricing_option: 1 },
              attributes: ["area_name", "id", "username"],
              include: [
                {
                  model: models.iot_companies,
                  as: "list_area",
                },
                {
                  model: models.iot_tenant,
                  as: "list_tenant",
                  include: [
                    {
                      model: models.iots_billing_history,
                      order: [["end_date", "DESC"]],
                    },
                    {
                      model: models.iot_nodes,
                      attributes: ["id", "devEui", "typeId", "tenantId"],
                      where: {
                        device_type_id: 1,
                        typeId: [1, 2, 3, 4, 7],
                        last_cut_date: models.sequelize.where(
                          models.sequelize.fn(
                            "date",
                            models.sequelize.col(
                              "list_tenant->iot_nodes.last_cut_date"
                            )
                          ),
                          "<",
                          moment().format("YYYY-MM-DD")
                        ),
                        tenantId: { [Op.not]: null },
                      },
                      include: [
                        {
                          model: models.iot_histories,
                          limit: 1,
                        },
                        {
                          model: models.iots_billing_history,
                          order: [["end_date", "DESC"]],
                        },
                        {
                          model: models.iot_detail_rtu,
                        },
                      ],
                    },
                  ],
                },
                {
                  model: models.iot_pricing_postpaid_all,
                },
                {
                  model: models.iots_cutoff,
                  as: "cut_off",
                  where: { tanggal_cutoff: moment().format("DD") },
                },
              ],
              transaction: t,
            })
            .then(async (_result) => {
              _result = JSON.parse(JSON.stringify(_result));
              _result = _result.filter((valA, idxA) => {
                return valA.list_tenant.length > 0;
              });
              _result.map((valA, idxA) => {
                let pricing = valA.iot_pricing_postpaid_alls.filter((valP) => {
                  return isNaN(parseFloat(valP.pricing)) == true;
                });
                let newPricing = [];
                pricing.map((val) => {
                  newPricing.push(val.typeId);
                });

                valA.list_tenant.map((valB, idxB) => {
                  newPricing.map((a) => {
                    _result[idxA].list_tenant[
                      idxB
                    ].iot_nodes = valB.iot_nodes.filter((valP) => {
                      return valP.typeId != a;
                    });
                  });
                });
              });

              _result.map((valA, idxA) => {
                valA.list_tenant.map((valB, idxB) => {
                  _result[idxA].list_tenant[
                    idxB
                  ].iot_nodes = valB.iot_nodes.filter((valP) => {
                    return (
                      (valP.typeId != 4 && valP.iot_histories.length > 0) ||
                      (valP.typeId == 4 &&
                        valP.iot_detail_rtu.field_billing != null)
                    );
                  });
                });
              });
              _result.map((valA, idxA) => {
                _result[idxA].list_tenant = valA.list_tenant.filter((valP) => {
                  return valP.iot_nodes.length > 0;
                });
              });
              _result = _result.filter((valP) => {
                return valP.list_tenant.length > 0;
              });
              if (_result.length == 0) {
                return _result;
              } else {
                let rawData = [];
                let rawDataflag = 0;
                let rawDataHistory = [];
                let rawDataHistoryStart = [];
                let rawDataHistoryStartFlag = [];
                let updateNode = [];
                _result.map(async (_valArea, _idxArea) => {
                  _valArea.list_tenant.map(async (_valTenant, _idxTenant) => {
                    let invoice = null;
                    if (_valTenant.iots_billing_history == null) {
                      invoice = `${_valArea.list_area.username}.${
                        _valTenant.username
                      }.${_valTenant.id}-INV-${moment().format("DDMMYY")}-0`;
                    } else {
                      invoice = _valTenant.iots_billing_history.invoice.split(
                        "-"
                      );
                      let lastDate = invoice[invoice.length - 2];
                      let currentDate = moment().format("MMYY");

                      if (lastDate.substr(-4) != currentDate) {
                        invoice = `${_valArea.list_area.username}.${
                          _valTenant.username
                        }.${_valTenant.id}-INV-${moment().format("DDMMYY")}-0`;
                      } else {
                        invoice = invoice.join("-");
                      }
                    }

                    _valTenant.iot_nodes.map((_valNode, _idxNode2) => {
                      let harga_satuan = 0;
                      _valArea.iot_pricing_postpaid_alls.find((val) => {
                        if (val.typeId == _valNode.typeId) {
                          harga_satuan = val.pricing;
                        }
                      });
                      invoice = invoice.split("-");
                      invoice[invoice.length - 1] =
                        parseInt(invoice[invoice.length - 1]) + 1;
                      invoice = invoice.join("-");
                      let rtu = null;
                      let model = null;
                      if (parseInt(_valNode.typeId) == 4) {
                        rtu = _valNode.iot_detail_rtu.field_billing;
                        model = _valNode.iot_detail_rtu.model;
                      }
                      rawData.push({
                        model: model,
                        rtu: rtu,
                        typeId: _valNode.typeId,
                        areaId: _valArea.id,
                        tenantId: _valTenant.id,
                        nodeId: _valNode.id,
                        cut_date: moment()
                          .subtract(1, "day")
                          .format("YYYY-MM-DD"),
                        invoice: invoice,
                        status: "NEW",
                        biaya_transaksi: 5000,
                        biaya_penyesuaian: 0,
                        ppn: 10,
                        harga_satuan: harga_satuan,
                        periode_cut: _valArea.automatic_bill,
                      });
                      rawDataHistory.push(
                        models.iot_histories.findOne({
                          where: {
                            device_id: _valNode.id,
                            col: models.sequelize.where(
                              models.sequelize.fn(
                                "date",
                                models.sequelize.col("iot_histories.reportTime")
                              ),
                              "<=",
                              moment().subtract(1, "day").format("YYYY-MM-DD")
                            ),
                          },
                          order: [["id", "DESC"]],
                          transaction: t,
                        })
                      );
                      if (_valNode.iots_billing_history != null) {
                        rawData[rawDataflag].start_meter =
                          _valNode.iots_billing_history.end_meter;
                        rawData[rawDataflag].start_date =
                          _valNode.iots_billing_history.end_date;
                        rawDataflag++;
                      } else {
                        if (parseInt(_valNode.typeId) == 4) {
                          rawData[rawDataflag].start_meter = JSON.parse(
                            _valNode.iot_histories[0].payload
                          ).subMeter.filter((valP) => {
                            return (
                              valP.applicationName ==
                              _valNode.iot_detail_rtu.model
                            );
                          });
                          rawData[rawDataflag].start_meter = rawData[
                            rawDataflag
                          ].start_meter[0].report.filter((valP) => {
                            return (
                              valP.name == _valNode.iot_detail_rtu.field_billing
                            );
                          });
                          rawData[rawDataflag].start_meter =
                            parseFloat(
                              rawData[rawDataflag].start_meter[0].value
                            ) * 1000;
                          rawData[rawDataflag].start_date =
                            _valNode.iot_histories[0].reportTime;
                          rawDataflag++;
                        } else {
                          rawData[rawDataflag].start_meter = JSON.parse(
                            _valNode.iot_histories[0].payload
                          ).meter.meterReading;
                          rawData[rawDataflag].start_date =
                            _valNode.iot_histories[0].reportTime;
                          rawDataflag++;
                        }
                      }
                    });
                  });
                });
                // return rawData;
                return Promise.all(rawDataHistory).then((_result_end) => {
                  _result_end = JSON.parse(JSON.stringify(_result_end));
                  _result_end = _result_end.filter((val) => {
                    return val != null;
                  });
                  _result_end.map((_valEnd) => {
                    rawData.find((val, idx) => {
                      if (_valEnd.device_id == val.nodeId) {
                        if (parseInt(val.typeId) == 4) {
                          rawData[idx].end_date = _valEnd.reportTime;
                          rawData[idx].end_meter = JSON.parse(
                            _valEnd.payload
                          ).subMeter.filter((valP) => {
                            return valP.applicationName == val.model;
                          });
                          rawData[idx].end_meter = rawData[
                            idx
                          ].end_meter[0].report.filter((valP) => {
                            return valP.name == val.rtu;
                          });
                          rawData[idx].end_meter =
                            parseFloat(rawData[idx].end_meter[0].value) * 1000;
                          rawData[idx].usage =
                            rawData[idx].end_meter - rawData[idx].start_meter;
                          rawData[idx].billing_usage =
                            rawData[idx].end_meter - rawData[idx].start_meter;
                        } else {
                          rawData[idx].end_date = _valEnd.reportTime;
                          rawData[idx].end_meter = JSON.parse(
                            _valEnd.payload
                          ).meter.meterReading;
                          rawData[idx].totalizer = JSON.parse(
                            _valEnd.payload
                          ).meter.meterReading;
                          rawData[idx].usage =
                            rawData[idx].end_meter - rawData[idx].start_meter;
                          rawData[idx].billing_usage =
                            rawData[idx].end_meter - rawData[idx].start_meter;
                        }
                      }
                    });
                  });
                  rawData.map((val) => {
                    if (isNaN(val.usage) == false) {
                      updateNode.push({
                        id: val.nodeId,
                        last_cut_date: moment().format("YYYY-MM-DD"),
                      });
                    }
                  });
                  return models.iots_billing_history
                    .bulkCreate(rawData)
                    .then(() => {
                      return models.iot_nodes.bulkCreate(updateNode, {
                        updateOnDuplicate: ["last_cut_date"],
                        transaction: t,
                      });
                    });
                });
              }
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            message: err.message,
          });
        });
    });
  },
};

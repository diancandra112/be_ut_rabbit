const models = require("../../models/index");
const { Op } = models.Sequelize;
const { midtrans } = require("../../config/axios");
const moment = require("moment");
const { axios_erp, axios_erp2 } = require("../../config/axios");
require("dotenv").config();

const usageV2BAK = (data) => {
  let reportTime = "";
  let report = [];
  let flag = 0;
  data.map((val, idx) => {
    if (reportTime != moment(val.reportTime).format("YYYY-MM-DD")) {
      reportTime = moment(val.reportTime).format("YYYY-MM-DD");
      if (flag == 0) {
        report.push({
          start_date: val.reportTime,
          start_meter: JSON.parse(val.payload).meter.meterReading,
        });
      } else {
        report.push({
          start_date: val.reportTime,
          start_meter: report[flag - 1].end_meter,
        });
      }
      flag++;
    } else {
      const idx = flag - 1;
      report[idx].end_meter = JSON.parse(val.payload).meter.meterReading;
      report[idx].end_date = val.reportTime;
    }
  });

  report.map((val, idx) => {
    report[idx].usage = parseInt(val.end_meter) - parseInt(val.start_meter);
  });
  return report;
};

const usageV2 = (data) => {
  let reportTime = "";
  let before = {};
  let report = [];
  let flag = 0;
  let temp_data = [];
  data.map((val, idx) => {
    if (idx == 0) {
      report.push({
        start_date: val.reportTime,
        start_meter: JSON.parse(val.payload).meter.meterReading,
      });
      before = {
        reportTime: val.reportTime,
        meter: JSON.parse(val.payload).meter.meterReading,
      };
    } else {
      if (
        moment(val.reportTime).utcOffset("+0700").format("YYYY-MM-DD") !==
        moment(before.reportTime).utcOffset("+0700").format("YYYY-MM-DD")
      ) {
        report.push({
          start_date: before.reportTime,
          start_meter: before.meter,
        });
        report[flag].end_meter = before.meter;
        report[flag].end_date = before.reportTime;
        flag++;
        before = {
          reportTime: val.reportTime,
          meter: JSON.parse(val.payload).meter.meterReading,
        };
      } else {
        before = {
          reportTime: val.reportTime,
          meter: JSON.parse(val.payload).meter.meterReading,
        };
      }
    }
    temp_data.push({
      meter: JSON.parse(val.payload).meter.meterReading,
      reportTime: val.reportTime,
    });
  });

  report[report.length - 1].end_meter = temp_data[temp_data.length - 1].meter;
  report[report.length - 1].end_date =
    temp_data[temp_data.length - 1].reportTime;
  report.map((val, idx) => {
    report[idx].usage = parseFloat(val.end_meter) - parseFloat(val.start_meter);
  });
  return report;
};

const usageV2Rtu = (data, model, billing, date_range) => {
  let reportTime = "";
  let before = {};
  let report = [];
  let flag = 0;
  let temp_data = [];

  data.map((val, idx) => {
    let temp_meter = 0;
    let temp_sub = JSON.parse(val.payload).subMeter.filter((valP) => {
      return valP.applicationName == model;
    });
    if (temp_sub.length > 0) {
      temp_sub = temp_sub[0].report.filter((valP) => {
        return valP.name == billing;
      });
      if (temp_sub.length > 0) {
        temp_meter = temp_sub[0].value;
      }
    }
    if (idx == 0) {
      report.push({
        start_date: val.reportTime,
        start_meter: temp_meter * 1000,
      });
      before = { reportTime: val.reportTime, meter: temp_meter };
    } else {
      if (
        moment(val.reportTime).utcOffset("+0700").format("YYYY-MM-DD") !==
        moment(before.reportTime).utcOffset("+0700").format("YYYY-MM-DD")
      ) {
        report.push({
          start_date: before.reportTime,
          start_meter: before.meter * 1000,
        });
        report[flag].end_meter = before.meter * 1000;
        report[flag].end_date = before.reportTime;
        flag++;
        before = { reportTime: val.reportTime, meter: temp_meter };
      } else {
        before = { reportTime: val.reportTime, meter: temp_meter };
      }
    }
    temp_data.push({ meter: temp_meter, reportTime: val.reportTime });
  });
  report[report.length - 1].end_meter =
    temp_data[temp_data.length - 1].meter * 1000;
  report[report.length - 1].end_date =
    temp_data[temp_data.length - 1].reportTime;

  report.map((val, idx) => {
    report[idx].usage = parseFloat(val.end_meter) - parseFloat(val.start_meter);
  });

  return report;
};
const usage = (data) => {
  let reportTime = "";
  let report = [];
  data.map((val, idx) => {
    if (reportTime != moment(val.reportTime).format("MMDD").toString()) {
      if (report.length > 0) {
        report[report.length - 1].end_date = data[idx - 1].reportTime;
        report[report.length - 1].end_meter = JSON.parse(
          data[idx - 1].payload
        ).meter.meterReading;
        report[report.length - 1].usage =
          report[report.length - 1].end_meter -
          report[report.length - 1].start_meter;
      }
      report.push({
        start_date: val.reportTime,
        start_meter: JSON.parse(val.payload).meter.meterReading,
      });
      if (data.length - 1 == idx) {
        if (report[report.length - 1].end_meter == undefined) {
          report[report.length - 1].end_meter =
            report[report.length - 1].start_meter;
          report[report.length - 1].end_date =
            report[report.length - 1].start_date;
          report[report.length - 1].usage =
            report[report.length - 1].end_meter -
            report[report.length - 1].start_meter;
        }
      }
      reportTime = moment(val.reportTime).format("MMDD").toString();
    } else {
      if (idx == data.length - 1) {
        report[report.length - 1].end_date = val.reportTime;
        report[report.length - 1].end_meter = JSON.parse(
          val.payload
        ).meter.meterReading;
        report[report.length - 1].usage =
          report[report.length - 1].end_meter -
          report[report.length - 1].start_meter;
      }
    }
  });
  return report;
};

require("dotenv").config();
module.exports = {
  svcGetCorrection: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {})
        .then((result) => {
          resolve({
            responseCode: 200,
            response: JSON.parse(JSON.stringify(result)),
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response1: JSON.stringify(err),
            response2: "Gagal Update Billing",
          });
        });
    });
  },
  svcGetBillingCorrection: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_correction_history.findAll({
            where: data,
          });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            response: JSON.parse(JSON.stringify(result)),
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response1: JSON.stringify(err),
            response2: "Gagal Update Billing",
          });
        });
    });
  },
  svcBillingCorrection: (data, pesan, denda) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_billing_history.findOne({
            where: { id: data.id },
            transaction: t,
          });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            response: "Berhasil Update Billing",
            response1: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response1: JSON.stringify(err),
            response2: "Gagal Update Billing",
          });
        });
    });
  },
  svcBillingCorrection2: (data, pesan, denda) => {
    return new Promise((resolve, reject) => {
      data.denda = JSON.stringify(data.denda);
      return models.sequelize
        .transaction((t) => {
          return models.iots_billing_history
            .update(data, {
              where: { id: data.id },
              transaction: t,
            })
            .then((result) => {
              return models.iot_correction_history
                .create(pesan, {
                  transaction: t,
                })
                .then(() => {
                  return models.iots_billing_fine.bulkCreate(denda, {
                    updateOnDuplicate: ["used"],
                    transaction: t,
                  });
                });
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            response: "Berhasil Update Billing",
            response1: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response1: JSON.stringify(err),
            response2: "Gagal Update Billing",
          });
        });
    });
  },
  svcGetUsageBillingCount0: (data, date_range) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_histories
            .findAll({
              order: [["reportTime", "ASC"]],
              where: {
                device_id: data.nodeId,
                reportTime: models.sequelize.where(
                  models.sequelize.col("iot_histories.reportTimeFlag"),
                  ">=",
                  moment(data.startDate).format("YYYY-MM-DD HH:mm:ss")
                ),
                reportTime2: models.sequelize.where(
                  models.sequelize.col("iot_histories.reportTimeFlag"),
                  "<=",
                  moment(data.endDate).format("YYYY-MM-DD HH:mm:ss")
                ),
              },
            })
            .then((result) => {
              return models.iot_nodes
                .findOne({
                  where: { id: data.nodeId },
                  attributes: ["id", "field_billing_rtu", "typeId"],
                  include: [
                    {
                      model: models.iot_detail_rtu,
                      attributes: ["node_id", "model"],
                    },
                  ],
                })
                .then((node) => {
                  node = JSON.parse(JSON.stringify(node));

                  if (node.typeId == 4) {
                    let data4 = {
                      log: usageV2Rtu(
                        JSON.parse(JSON.stringify(result)),
                        node.iot_detail_rtu.model,
                        node.field_billing_rtu,
                        date_range
                      ),
                      total: 0,
                    };

                    data4.log.map((val) => {
                      data4.total =
                        parseFloat(data4.total) + parseFloat(val.usage);
                    });
                    return data4;
                  } else {
                    let data = {
                      log: usageV2(JSON.parse(JSON.stringify(result))),
                      total: 0,
                    };

                    data.log.map((val) => {
                      data.total =
                        parseFloat(data.total) + parseFloat(val.usage);
                    });
                    return data;
                  }
                });
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response1: JSON.stringify(err),
            response2: "Gagal Update Billing",
          });
        });
    });
  },
  svcGetUsageBilling: (data, date_range) => {
    return new Promise((resolve, reject) => {
      console.log(data);
      return models.sequelize
        .transaction((t) => {
          return models.iot_histories
            .findAll({
              order: [["reportTime", "ASC"]],
              where: {
                device_id: data.nodeId,
                reportTime: models.sequelize.where(
                  models.sequelize.col("iot_histories.reportTimeFlag"),
                  ">=",
                  moment(data.startDate).format("YYYY-MM-DD HH:mm:ss")
                ),
                reportTime2: models.sequelize.where(
                  models.sequelize.col("iot_histories.reportTimeFlag"),
                  "<=",
                  moment(data.endDate).format("YYYY-MM-DD HH:mm:ss")
                ),
              },
            })
            .then((result) => {
              return models.iot_nodes
                .findOne({
                  where: { id: data.nodeId },
                  attributes: ["id", "field_billing_rtu", "typeId"],
                  include: [
                    {
                      model: models.iot_detail_rtu,
                      attributes: ["node_id", "model"],
                    },
                  ],
                })
                .then((node) => {
                  node = JSON.parse(JSON.stringify(node));
                  if (node.typeId == 4) {
                    let data4 = {
                      log: usageV2Rtu(
                        JSON.parse(JSON.stringify(result)),
                        node.iot_detail_rtu.model,
                        node.field_billing_rtu,
                        date_range
                      ),
                      total: 0,
                    };

                    data4.log.map((val) => {
                      data4.total =
                        parseFloat(data4.total) + parseFloat(val.usage);
                    });
                    return data4;
                  } else {
                    let data = {
                      log: usageV2(JSON.parse(JSON.stringify(result))),
                      total: 0,
                    };

                    data.log.map((val) => {
                      data.total =
                        parseFloat(data.total) + parseFloat(val.usage);
                    });
                    return data;
                  }
                });
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response1: JSON.stringify(err),
            response2: "Gagal Update Billing",
          });
        });
    });
  },
  svcGetBillingClose: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_billing_history.findOne({
            where: { id: data.id },
            attributes: ["payment_date", "payment_method"],
            include: [
              {
                model: models.iots_billing_close_attachments,
                attributes: ["billing_id", "filename", "url"],
              },
            ],
            transaction: t,
          });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            message: "Berhasil Get Billing",
            result: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response1: err.message,
            response2: "Gagal Update Billing",
          });
        });
    });
  },
  svcUpdateBillingClose: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_billing_history
            .update(data, {
              where: { id: data.id },
              transaction: t,
            })
            .then((result) => {
              if (data.files.length == 0) {
                return result;
              } else {
                return models.iots_billing_close_attachments.bulkCreate(
                  data.files,
                  {
                    transaction: t,
                  }
                );
              }
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            response1: "Berhasil Update Billing",
            response2: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response1: err.message,
            response2: "Gagal Update Billing",
          });
        });
    });
  },
  svcUpdateBilling: (data) => {
    return new Promise((resolve, reject) => {
      let invoice = data.invoice;
      let id = data.id;
      delete data.id;
      delete data.invoice;
      return models.sequelize
        .transaction((t) => {
          return models.iots_billing_history.update(data, {
            where: { id: id, invoice: invoice },
            transaction: t,
          });
        })

        .then((result) => {
          resolve({
            responseCode: 200,
            response: "Berhasil Update Billing",
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response1: JSON.stringify(err),
            response2: "Gagal Update Billing",
          });
        });
    });
  },
  svcUpdateBillingCancel: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_billing_history
            .findOne({
              where: { id: data.id, invoice: data.invoice },
              transaction: t,
            })
            .then((find) => {
              find = JSON.parse(JSON.stringify(find));
              return models.iots_billing_history
                .update(
                  { status: data.status, resultErp: null },
                  {
                    where: { id: data.id, invoice: data.invoice },
                    transaction: t,
                  }
                )
                .then(() => {
                  return models.iots_billing_history
                    .update(
                      { log_area_id: null },
                      {
                        where: {
                          id: { [Op.not]: data.id },
                          log_area_id: data.id,
                        },
                        transaction: t,
                      }
                    )
                    .then(() => {
                      if (find.erp_id != null) {
                        let { email_erp, pass_erp, baseUrl, id } = JSON.parse(
                          find.erp_payload
                        );
                        axios_erp({
                          baseURL: baseUrl,
                          url: `/auth/login?email=${email_erp}&password=${pass_erp}`,
                          method: "POST",
                        }).then((res) => {
                          axios_erp({
                            baseURL: baseUrl,
                            url: `/v1/sales_invoices/${id}`,
                            method: "DELETE",
                            headers: {
                              authorization: "Bearer " + res.data.access_token,
                            },
                          });
                        });
                        return "ok";
                      } else {
                        return "ok";
                      }
                    });
                });
            });
        })

        .then((result) => {
          resolve({
            responseCode: 200,
            response: "sukses cancel invoice",
            result: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response1: err.message,
            response2: "Gagal Update Billing",
          });
        });
    });
  },
  svcGetBilling: (
    data,
    sort_key = "cut_date",
    sort_idx = "DESC",
    page = 1,
    page_size = "all",
    search_inv = ""
  ) => {
    return new Promise((resolve, reject) => {
      let periode = {};
      if (
        data.cut_date !== undefined &&
        moment(data.cut_date).format() !== "Invalid date"
      ) {
        periode.cut_date1 = models.sequelize.where(
          models.sequelize.col("iots_billing_history.periode_billing"),
          "=",
          moment(data.cut_date).format("YYYY-MM")
        );
        // periode.cut_date1 = models.sequelize.where(
        //   models.sequelize.fn(
        //     "YEAR",
        //     models.sequelize.col("iots_billing_history.cut_date")
        //   ),
        //   "=",
        //   moment(data.cut_date).format("YYYY")
        // );
        // periode.cut_date2 = models.sequelize.where(
        //   models.sequelize.fn(
        //     "MONTH",
        //     models.sequelize.col("iots_billing_history.cut_date")
        //   ),
        //   "=",
        //   moment(data.cut_date).format("MM")
        // );
      }
      delete data.cut_date;
      return models.sequelize
        .transaction((t) => {
          let pdf = [
            {
              model: models.iots_wa_status,
            },
          ];
          if (data.id != null) {
            pdf.push({
              model: models.iots_billing_attachment,
              limit: 1,
              where: { status_name: "INV" },
              order: [["id", "DESC"]],
            });
          }
          return models.iot_area
            .findOne({
              where: { id: data.areaId },
            })
            .then((res_area) => {
              res_area = JSON.parse(JSON.stringify(res_area));
              return models.iots_billing_history
                .count({
                  where: [
                    data,
                    periode,
                    {
                      // is_combine_billing:
                      //   res_area.billing_charge_type == "area" ? true : false,
                      invoice: { [Op.substring]: search_inv },
                    },
                  ],
                })
                .then((res_count) => {
                  res_count = JSON.parse(JSON.stringify(res_count));
                  if (page_size == "all") {
                    page_size = res_count;
                  }
                  return models.iots_billing_history
                    .findAll({
                      offset: (parseInt(page) - 1) * parseInt(page_size),
                      limit: parseInt(page_size),
                      where: [
                        data,
                        periode,
                        {
                          // is_combine_billing:
                          //   res_area.billing_charge_type == "area"
                          //     ? true
                          //     : false,
                          invoice: { [Op.substring]: search_inv },
                        },
                      ],
                      order: [[sort_key, sort_idx]],
                      attributes: {
                        exclude: [
                          "wa_rimender",
                          "wa_raw",
                          "wa_msgId",
                          "wa_status",
                          "wa_responseCode",
                          "createdAt",
                          "updatedAt",
                        ],
                      },
                      include: [
                        ...pdf,
                        {
                          model: models.iot_tenant,
                        },
                        {
                          model: models.iot_nodes,
                          where: { is_unsigned: false },
                        },
                        {
                          model: models.iot_area,
                          attributes: { exclude: ["createdAt", "updatedAt"] },
                          include: [{ model: models.iots_cutoff }],
                        },
                        // {
                        //   model: models.iot_area,
                        //   // include: [
                        //   //   { model: models.iot_pricing_postpaid_all },
                        //   //   // {
                        //   //   //   model: models.iot_member_level,
                        //   //   //   include: [
                        //   //   //     { model: models.iot_pricing_postpaid, as: "child" },
                        //   //   //   ],
                        //   //   // },
                        //   // ],
                        // },
                      ],
                    })
                    .then((res_find) => {
                      res_find = JSON.parse(JSON.stringify(res_find));
                      if (data.id != undefined) {
                        res_find.map((val, idx) => {
                          console.log(val);
                          let cut = val.iot_area.iots_cutoffs.find((valP) => {
                            return (
                              parseInt(valP.tanggal_cutoff) ==
                              parseInt(
                                moment(val.cut_date).add(1, "day").format("DD")
                              )
                            );
                          });
                          res_find[idx].iot_area.iots_cutoffs = cut
                            ? cut
                            : {
                                id: 320,
                                areaId: 11,
                                tenantId: null,
                                tanggal_cutoff: 1,
                                order: 1,
                                auto_bill_type: 1,
                                status: null,
                                time: "08:00:00",
                                createdAt: "2021-08-02T05:37:51.000Z",
                                updatedAt: "2021-08-02T05:37:51.000Z",
                              };
                          if (val.iot_node != null && val.typeId == 4) {
                            res_find[idx].unit =
                              val.iot_node.field_billing_rtu.match(
                                /\(([^)]+)\)/
                              )
                                ? val.iot_node.field_billing_rtu.match(
                                    /\(([^)]+)\)/
                                  )[1]
                                : null;
                          }
                          if (
                            val.iot_area.is_erp == true &&
                            val.is_combine_billing == true &&
                            val.billing_type == "area"
                          ) {
                            res_find[idx].iot_area.nomor_pelanggan =
                              res_find[idx].iot_area.erp_contract_name == null
                                ? ""
                                : res_find[
                                    idx
                                  ].iot_area.erp_contract_name.split(" - ")[0];
                          } else if (
                            val.iot_area.is_erp == false &&
                            val.is_combine_billing == true &&
                            val.billing_type == "area"
                          ) {
                            res_find[idx].iot_area.nomor_pelanggan =
                              res_find[idx].iot_area.id;
                          } else if (
                            val.iot_area.is_erp == false &&
                            val.is_combine_billing == false &&
                            val.billing_type == "tenant"
                          ) {
                            res_find[idx].iot_tenant.member_id =
                              res_find[idx].iot_tenant.id;
                          } else if (
                            val.iot_area.is_erp == true &&
                            val.is_combine_billing == false &&
                            val.billing_type == "tenant"
                          ) {
                            res_find[idx].iot_tenant.member_id =
                              res_find[idx].iot_tenant.erp_contract_name == null
                                ? ""
                                : res_find[
                                    idx
                                  ].iot_tenant.erp_contract_name.split(
                                    " - "
                                  )[0];
                          }
                        });
                      }
                      return { total: res_count, data: res_find };
                    });
                });
            });
        })
        .then((result) => {
          result = JSON.parse(JSON.stringify(result));
          let total = result.total;
          result = result.data;

          result.map((val, idx) => {
            if (data.id != null) {
              result[idx].iots_billing_attachments.map((valP, idx2) => {
                result[idx].iots_billing_attachments[idx2].file_pdf =
                  process.env.PDF_URL + valP.file_pdf;
              });
            }
            // switch (parseInt(val.iot_area.pricing_option)) {
            // case 1:
            let harga = "";
            // val.iot_area.iot_pricing_postpaid_alls.find((vals) => {
            // console.log(vals.typeId, "==", val.typeId);
            // if (vals.typeId == val.typeId) {
            result[idx].biaya_penyesuaian = parseFloat(
              result[idx].biaya_penyesuaian
            );
            if (val.typeId == 4 || val.billing_type == "node") {
              result[idx].usage = parseFloat(result[idx].usage);
              result[idx].billing_usage = parseFloat(result[idx].billing_usage);
            } else {
              result[idx].usage = parseFloat(result[idx].usage) / 1000;
              result[idx].billing_usage =
                parseFloat(result[idx].billing_usage) / 1000;
              result[idx].minimum_charge =
                parseFloat(result[idx].minimum_charge) / 1000;
              result[idx].minimum_charge_total =
                parseFloat(result[idx].minimum_charge_total) / 1000;
            }
            result[idx].amount = parseInt(
              (
                parseFloat(val.billing_usage) * parseFloat(val.harga_satuan)
              ).toFixed(4)
            );
            let _denda = Array.isArray(JSON.parse(result[idx].denda))
              ? JSON.parse(result[idx].denda)
              : [];
            result[idx].denda = _denda;
            result[idx].materai = result[idx].materai;
            result[idx].amount_pajak = parseInt(
              result[idx].amount * (parseInt(result[idx].ppn) / 100)
            );
            result[idx].erp_inv = result[idx].resultErp;
            result[idx].sub_total =
              result[idx].amount + result[idx].amount_pajak;
            result[idx].total_denda = 0;
            _denda.map((valDenda, idx2) => {
              _denda[idx2].jumlah_denda = parseInt(valDenda.jumlah_denda);
              result[idx].total_denda += parseInt(valDenda.jumlah_denda);
            });
            result[idx].denda = _denda;
            if (val.typeId == 4) {
              result[idx].totalizer = parseFloat(result[idx].totalizer);
              result[idx].start_meter = parseFloat(result[idx].start_meter);
              result[idx].end_meter = parseFloat(result[idx].end_meter);
            } else {
              result[idx].totalizer = parseFloat(result[idx].totalizer) / 1000;
              result[idx].start_meter =
                parseFloat(result[idx].start_meter) / 1000;
              result[idx].end_meter = parseFloat(result[idx].end_meter) / 1000;
            }
            // result[idx].pricing = vals.pricing;
            result[idx].pricing = val.harga_satuan;
            result[idx].total =
              result[idx].sub_total +
              result[idx].total_denda +
              result[idx].materai;
            result[idx].grandtotal =
              result[idx].total +
              result[idx].biaya_penyesuaian +
              result[idx].biaya_transaksi;
            // }
            // });
            //   break;

            // default:
            //   break;
            // }
          });
          resolve({
            responseCode: 200,
            page: page,
            size: page_size,
            total: total,
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: JSON.stringify(err),
          });
        });
    });
  },
  svcGetCountBilling: (data, date_range) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_billing_history
            .findOne({
              where: {
                nodeId: data.nodeId,
                start_date: data.startDate,
              },
              attributes: ["id"],
            })
            .then((result) => {
              result = JSON.parse(JSON.stringify(result));
              return models.iots_billing_history.count({
                where: {
                  id: { [Op.lt]: result.id },
                  nodeId: data.nodeId,
                },
              });
            });
        })
        .then((result) => {
          resolve(result);
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response1: err.message,
            response2: "Gagal Update Billing",
          });
        });
    });
  },
  svcBillingFine: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_billing_history.findOne({
            where: { id: data.id },
            transaction: t,
          });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            response: "Berhasil Update Billing",
            response1: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response1: JSON.stringify(err),
            response2: "Gagal Update Billing",
          });
        });
    });
  },
  svcGetBillingLogArea: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_billing_history.findAll({
            attributes: ["log", "id", "tenantId", "areaId", "no_meter"],
            include: [
              {
                model: models.iot_tenant,
                attributes: ["id", "tenant_name", "address", "member_id"],
              },
            ],
            where: {
              areaId: data.areaId,
              is_combine_billing: false,
              log_area_id: data.log_area_id,
              log: { [Op.not]: null },
            },
          });
        })
        .then((result) => {
          result = JSON.parse(JSON.stringify(result));
          let output = [];
          result.map((val) => {
            if (
              /^[\],:{}\s]*$/.test(
                val.log
                  .replace(/\\["\\\/bfnrtu]/g, "@")
                  .replace(
                    /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,
                    "]"
                  )
                  .replace(/(?:^|:|,)(?:\s*\[)+/g, "")
              )
            ) {
              output.push({
                id: val.id,
                no_meter: val.no_meter,
                tenant_name: val.iot_tenant.tenant_name,
                address: val.iot_tenant.address,
                member_id: val.iot_tenant.member_id,
                tenantId: val.tenantId,
                areaId: val.areaId,
                log: val.log,
              });
            }
          });
          resolve({
            responseCode: 200,
            response: output,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response1: JSON.stringify(err),
            response2: "Gagal Update Billing",
          });
        });
    });
  },
  svcGetBillingAttachCombine: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_billing_history.findAll({
            // attributes: ["log", "id", "tenantId", "areaId", "no_meter"],
            include: [
              {
                model: models.iot_tenant,
                attributes: ["id", "tenant_name", "address", "member_id"],
              },
            ],
            where: {
              areaId: data.areaId,
              is_combine_billing: false,
              log_area_id: data.log_area_id,
            },
          });
        })
        .then((result) => {
          result = JSON.parse(JSON.stringify(result));
          let output = [];
          result.map((val) => {
            output.push({
              customer_name: val.iot_tenant.tenant_name,
              billing_id: val.invoice,
              start_date: val.start_date,
              end_date: val.end_date,
              start_totalizer: [1, 2, 3, 7].includes(val.typeId)
                ? parseFloat(val.start_meter) / 1000
                : parseFloat(val.start_meter),
              end_totalizer: [1, 2, 3, 7].includes(val.typeId)
                ? parseFloat(val.end_meter) / 1000
                : parseFloat(val.end_meter),
              minimum_charge: val.minimum_charge_total,
              usage: (parseFloat(val.usage) / 1000).toFixed(3),
              ajustment_usage: val.correction_usage,
              minimum_charge_usage: val.minimum_charge,
              usage_billing: (parseFloat(val.billing_usage) / 1000).toFixed(3),
              unit: val.unit,
            });
          });
          resolve({
            responseCode: 200,
            response: output,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response1: JSON.stringify(err),
            response2: "Gagal Update Billing",
          });
        });
    });
  },
};

const models = require("../../models/index");
const axios = require("../../config/axios");
const Op = models.Sequelize.Op;
require("dotenv").config();
module.exports = {
  svcGetUsageHistori: (data) => {
    console.log(data);
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_histories.findOne({
            where: {
              device_id: data.nodeId,
              reportTime:
                data.order == undefined
                  ? { [Op.gte]: data.date }
                  : { [Op.lt]: data.date },
            },
            order: [data.order == undefined ? ["id", "ASC"] : ["id", "DESC"]],
          });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            response: result,
          });
        })
        .catch((err) => {
          reject({
            responseCode: 400,
            response: JSON.stringify(err),
          });
        });
    });
  },
  svcGetAutoBill: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_auto_bill_options.findAll({
            transaction: t,
          });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            response: result,
          });
        })
        .catch((err) => {
          reject({
            responseCode: 400,
            response: JSON.stringify(err),
          });
        });
    });
  },
};

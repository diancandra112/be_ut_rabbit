const models = require("../../models/index");
const axios = require("../../config/axios");
const { serviceUploadImages } = require("../service_pdf");

require("dotenv").config();
module.exports = {
  svcSendMail: (data, attachment) => {
    return new Promise((resolve, reject) => {
      try {
        // let attachmentData = [attachment];
        let pdfData = [];
        // console.log(Array.isArray(attachment));
        data.attachment
          .reduce(
            (chain, item) =>
              chain.then(() =>
                serviceUploadImages(item).then((res) => {
                  pdfData.push({
                    file_pdf: res.messages,
                    billingId: data.billingId,
                  });
                })
              ),
            Promise.resolve()
          )
          .then((s) => {
            models.iots_billing_attachment
              .bulkCreate(pdfData)
              .then((s) => console.log(s))
              .catch((e) => console.log(e));
          })
          .catch((e) => console.log(e));
      } catch (error) {
        console.log(error);
      }
      // return models.sequelize
      //   .transaction((t) => {
      //     return models.iots_billing_history.findAll({
      //       where: data,
      //       attributes: { exclude: ["createdAt", "updatedAt"] },
      //       include: [
      //         {
      //           model: models.iot_nodes,
      //           include: [{ model: models.iot_node_type }],
      //         },
      //         {
      //           model: models.iot_tenant,
      //         },
      //       ],
      //     });
      //   })

      //   .then((result) => {
      //     result = JSON.parse(JSON.stringify(result));
      //     result.map((val, idx) => {
      //       result[idx].devEui = val.iot_node.devEui;
      //       result[idx].tenant_name = val.iot_tenant.tenant_name;
      //       result[idx].type_name = val.iot_node.iot_node_type.type_name;
      //       result[idx].satuan = val.iot_node.iot_node_type.satuan;
      //       delete result[idx].iot_node;
      //       delete result[idx].iot_tenant;
      //     });
      //     resolve({
      //       responseCode: 200,
      //       response: result,
      //     });
      //   })
      //   .catch((err) => {
      //     console.log(err);
      //     reject({
      //       responseCode: 400,
      //       response: JSON.stringify(err),
      //     });
      //   });
    });
  },
};

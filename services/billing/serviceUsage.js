const models = require("../../models/index");
const axios = require("../../config/axios");
const moment = require("moment");

require("dotenv").config();
let combine = (names) => names.filter((v, i) => names.indexOf(v) === i);
module.exports = {
  svcGetUsage: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_billing_history.findAll({
            where: data,
            attributes: { exclude: ["createdAt", "updatedAt"] },
            include: [
              // {
              //   model: models.iot_nodes,
              //   include: [{ model: models.iot_node_type }],
              // },
              {
                model: models.iot_tenant,
              },
            ],
          });
        })

        .then((result) => {
          result = JSON.parse(JSON.stringify(result));
          result.map((val, idx) => {
            // result[idx].devEui = val.iot_node.devEui;
            result[idx].devEui = val.deveui;
            result[idx].tenant_name = val.iot_tenant.tenant_name;
            // result[idx].type_name = val.iot_node.iot_node_type.type_name;
            result[idx].type_name = val.node_type;
            // result[idx].satuan = val.iot_node.iot_node_type.satuan;
            result[idx].satuan = val.unit;
            delete result[idx].iot_node;
            delete result[idx].iot_tenant;
          });
          resolve({
            responseCode: 200,
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: JSON.stringify(err),
          });
        });
    });
  },
  svcGetUsageV2: (data, type, dataType) => {
    return new Promise((resolve, reject) => {
      // let filterType = {
      //   cut_date1: models.sequelize.where(
      //     models.sequelize.fn(
      //       "year",
      //       models.sequelize.col("iots_billing_history.cut_date")
      //     ),
      //     "=",
      //     moment(data.periode).format("YYYY")
      //   ),
      //   cut_date2: models.sequelize.where(
      //     models.sequelize.fn(
      //       "month",
      //       models.sequelize.col("iots_billing_history.cut_date")
      //     ),
      //     "=",
      //     moment(data.periode).format("MM")
      //   ),
      // };
      let filterType = {
        periode_billing: models.sequelize.where(
          models.sequelize.col("iots_billing_history.periode_billing"),
          "=",
          moment(data.periode).format("YYYY-MM")
        ),
      };
      delete data.periode;

      return models.sequelize
        .transaction((t) => {
          return models.iots_billing_history.findAll({
            where: [data, filterType],
            // attributes: { exclude: ["createdAt", "updatedAt"] },
            include: [
              {
                model: models.iot_nodes,
                include: [{ model: models.iot_node_type }],
                where: { is_unsigned: false },
                required: true,
              },
              {
                model: models.iot_tenant,
              },
              // {
              //   model: models.iot_internal,
              // },
            ],
            order: [
              ["tenantId", "ASC"],
              ["start_date", "ASC"],
            ],
          });
        })
        .then((result) => {
          try {
            result = JSON.parse(JSON.stringify(result));

            result = result.filter((valP) => {
              return valP.iot_node != null;
            });
            result.map((val, idx) => {
              result[idx].devEui = val.iot_node.devEui;
              result[idx].tenant_name =
                val.iot_tenant != null ? val.iot_tenant.tenant_name : null;
              result[idx].type_name = val.iot_node.iot_node_type.type_name;
              result[idx].satuan = val.iot_node.iot_node_type.satuan;
              // delete result[idx].iot_node;
              delete result[idx].iot_tenant;
            });
            let output_final = [];
            let flag = 0;
            let list_int_room = [];
            if (result.length > 0) {
              result.map((val, idx) => {
                list_int_room.push(val.tenantId);
              });
              list_int_room = combine(list_int_room);
              list_int_room.map((val, idx) => {
                let tempData = result.filter((valP) => {
                  return valP.tenantId == val;
                });
                tempData.map((valTemp, idxTemp) => {
                  if (idxTemp == 0) {
                    if (valTemp.typeId == 4) {
                      valTemp.usage = valTemp.usage * 1000;
                      valTemp.billing_usage = valTemp.billing_usage * 1000;
                    }
                    output_final.push({
                      start_date: moment(valTemp.start_date).format(
                        "YYYY-MM-DD"
                      ),
                      end_date: moment(valTemp.end_date).format("YYYY-MM-DD"),
                      status_user: valTemp.status_user,
                      internalId: valTemp.internalId,
                      areaId: valTemp.areaId,
                      tenantId: valTemp.tenantId,
                      cut_date: valTemp.cut_date,
                      usage: valTemp.usage,
                      iot_internal: valTemp.iot_internal,
                      tenant_name: valTemp.tenant_name,
                      internal_name: valTemp.internal_name,
                      type_name: valTemp.type_name,
                      satuan: valTemp.satuan,
                    });
                  } else {
                    if (valTemp.typeId == 4) {
                      valTemp.usage = valTemp.usage * 1000;
                      valTemp.billing_usage = valTemp.billing_usage * 1000;
                    }
                    output_final[flag].usage =
                      parseFloat(output_final[flag].usage) +
                      parseFloat(valTemp.usage);
                  }
                });
                flag++;
              });
            }
            resolve({
              responseCode: 200,
              // response: output_final,
              result: result,
            });
          } catch (error) {
            console.log(error);
          }
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });

      // .then((result) => {
      // })
    });
  },
};

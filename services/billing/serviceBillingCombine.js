const models = require("../../models/index");
const { Op } = models.Sequelize;
const { midtrans } = require("../../config/axios");
const moment = require("moment");

require("dotenv").config();
module.exports = {
  svcBillingCombine: (data, pesan, denda) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_billing_history
            .findAll({
              where: {
                billing_type: "tenant",
                typeId: { [Op.not]: null },
                areaId: data.areaId,
                periode_billing: data.periode_billing,
                log_area_id: null,
                is_combine_billing: false,
              },
              transaction: t,
            })
            .then((result_billing) => {
              result_billing = JSON.parse(JSON.stringify(result_billing));
              if (result_billing.length == 0) {
                throw new Error("All Billing Already Combine");
              }
              let typeId = [];
              result_billing.map((valType) => {
                typeId.push(valType.typeId);
              });
              typeId = Array.from(new Set(typeId));
              return models.iot_area
                .findOne({
                  where: {
                    id: data.areaId,
                  },
                  include: [{ model: models.iot_pricing_billing_area }],
                  attributes: [
                    "id",
                    "is_ppn_value",
                    "is_ppn",
                    "minimum_charge_gas",
                    "minimum_charge_water",
                    "minimum_charge_electricity_ct",
                    "minimum_charge_electricity_non_ct",
                  ],
                  transaction: t,
                })
                .then((result_area) => {
                  result_area = JSON.parse(JSON.stringify(result_area));
                  if (result_area.iot_pricing_billing_areas.length == 0) {
                    throw new Error("Please Config Pricing billing");
                  }
                  typeId.map((valPr) => {
                    if (valPr == 4) {
                    } else {
                      let cek = result_area.iot_pricing_billing_areas.filter(
                        (valP) => {
                          return valP.typeId == valPr;
                        }
                      );
                      if (cek.length == 0) {
                        throw new Error(
                          `Please Config Pricing billing ${
                            valPr == 1
                              ? "GAS"
                              : valPr == 2
                              ? "WATER"
                              : valPr == 3
                              ? "ELECTRICITY NON CT"
                              : "ELECTRICITY CT"
                          }`
                        );
                      }
                    }
                  });
                  let ppn = 0;
                  if (result_area.is_ppn == true) {
                    ppn = result_area.is_ppn_value;
                  }
                  let billing = [];
                  typeId.map((val) => {
                    let type = result_billing.filter((valP) => {
                      return valP.typeId == val;
                    });
                    let date_start = null;
                    let date_end = null;
                    let temp_bill = null;
                    let node = [];
                    if (type.length > 0) {
                      type.map((valT, idxT) => {
                        node.push(valT.id);
                        if (idxT == 0) {
                          let harga =
                            result_area.iot_pricing_billing_areas.find(
                              (valP) => {
                                return valP.typeId == valT.typeId;
                              }
                            );
                          temp_bill = {
                            areaId: valT.areaId,
                            cut_date: valT.cut_date,
                            start_meter: parseFloat(valT.start_meter),
                            end_meter: parseFloat(valT.end_meter),
                            start_date: valT.start_date,
                            end_date: valT.end_date,
                            totalizer: parseFloat(valT.totalizer),
                            usage: parseFloat(valT.billing_usage),
                            invoice: valT.invoice,
                            ppn: ppn,
                            harga_satuan: harga.pricing,
                            periode_cut: valT.periode_cut,
                            biaya_transaksi: valT.biaya_transaksi,
                            biaya_penyesuaian: parseFloat(
                              valT.biaya_penyesuaian
                            ),
                            status: "NEW",
                            correction_usage: 0,
                            billing_usage: parseFloat(valT.billing_usage),
                            denda: Array.isArray(JSON.parse(valT.denda))
                              ? JSON.parse(valT.denda)
                              : [],
                            periode_billing: valT.periode_billing,
                            node_type: valT.node_type,
                            unit: valT.unit,
                            typeId: valT.typeId,

                            minimum_charge: 0,
                            minimum_charge_total:
                              valT.typeId == 1
                                ? result_area.minimum_charge_gas * 1000
                                : valT.typeId == 2
                                ? result_area.minimum_charge_water * 1000
                                : valT.typeId == 3
                                ? result_area.minimum_charge_electricity_non_ct *
                                  1000
                                : result_area.minimum_charge_electricity_ct *
                                  1000,
                            billing_type: "area",
                            log_flag: true,
                            is_combine_billing: true,
                          };
                        } else {
                          if (Array.isArray(JSON.parse(valT.denda))) {
                            temp_bill.denda = temp_bill.denda.concat(
                              JSON.parse(valT.denda)
                            );
                          }
                          temp_bill.start_meter += parseFloat(valT.start_meter);
                          temp_bill.end_meter += parseFloat(valT.end_meter);
                          temp_bill.totalizer += parseFloat(valT.totalizer);
                          temp_bill.usage += parseFloat(valT.billing_usage);
                          temp_bill.biaya_penyesuaian += parseFloat(
                            valT.biaya_penyesuaian
                          );
                          temp_bill.billing_usage += parseFloat(
                            valT.billing_usage
                          );
                        }
                      });
                      if (Array.isArray(temp_bill.denda)) {
                        temp_bill.denda = JSON.stringify(temp_bill.denda);
                      }
                      if (temp_bill.usage < temp_bill.minimum_charge_total) {
                        temp_bill.minimum_charge =
                          temp_bill.minimum_charge_total - temp_bill.usage;
                      }
                      temp_bill.billing_usage =
                        parseFloat(temp_bill.usage) + temp_bill.minimum_charge;
                      models.iots_billing_history
                        .create(temp_bill)
                        .then((res_bill) => {
                          node.push(res_bill.id);
                          models.iots_billing_history.update(
                            {
                              log_area_id: res_bill.id,
                            },
                            { where: { id: { [Op.in]: node } } }
                          );
                        });
                    }
                  });
                  return "Berhasil Update Billing";
                });
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            message: "Berhasil Update Billing",
            result: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            message: err.message,
          });
        });
    });
  },
};

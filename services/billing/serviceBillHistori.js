const models = require("../../models/index");
const axios = require("../../config/axios");

require("dotenv").config();
module.exports = {
  svcGetLastInv: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_billing_history.findOne({
            order: [["id", "DESC"]],
            transaction: t,
          });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            response: result,
          });
        })
        .then((err) => {
          reject({
            responseCode: 400,
            response: JSON.stringify(err),
          });
        });
    });
  },
  svcGetBillHistory: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_billing_history.findOne({
            where: data,
            order: [["id", "DESC"]],
            transaction: t,
          });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            response: result,
          });
        })
        .then((err) => {
          reject({
            responseCode: 400,
            response: JSON.stringify(err),
          });
        });
    });
  },
};

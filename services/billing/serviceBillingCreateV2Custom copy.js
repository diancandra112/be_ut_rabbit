const models = require("../../models/index");
const { Op } = models.Sequelize;
const moment = require("moment");
require("dotenv").config();

module.exports = {
  svcCreateBillingCustom: async (data) => {
    return new Promise(async (resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_area
            .findAll({
              where: { pricing_option: 2 },
              attributes: ["area_name", "id", "username"],
              include: [
                {
                  model: models.iot_companies,
                  as: "list_area",
                },
                {
                  model: models.iot_tenant,
                  as: "list_tenant",
                  include: [
                    {
                      model: models.iots_billing_history,
                      order: [["id", "DESC"]],
                    },
                    {
                      model: models.iot_nodes,
                      attributes: ["id", "devEui", "typeId", "tenantId"],
                      where: {
                        device_type_id: 1,
                        typeId: [1, 2, 3, 7],
                        last_cut_date: models.sequelize.where(
                          models.sequelize.fn(
                            "date",
                            models.sequelize.col(
                              "list_tenant->iot_nodes.last_cut_date"
                            )
                          ),
                          "<",
                          moment().format("YYYY-MM-DD")
                        ),
                        tenantId: { [Op.not]: null },
                      },
                      include: [
                        {
                          model: models.iots_billing_history,
                        },
                        {
                          model: models.iot_detail_rtu,
                        },
                      ],
                    },
                  ],
                },
                {
                  model: models.iot_member_level,
                  include: [
                    { model: models.iot_pricing_postpaid, as: "child" },
                  ],
                },
                {
                  model: models.iots_cutoff,
                  as: "cut_off",
                  where: { tanggal_cutoff: moment().format("DD") },
                },
              ],
              transaction: t,
            })
            .then(async (_result) => {
              _result = JSON.parse(JSON.stringify(_result));
              if (_result.length == 0) {
                return _result;
              } else {
                let rawData = [];
                let rawDataHistory = [];
                let rawDataHistoryStart = [];
                let rawDataHistoryStartFlag = [];
                let updateNode = [];
                _result.map(async (_valArea, _idxArea) => {
                  _valArea.list_tenant.map(async (_valTenant, _idxTenant) => {
                    let invoice = null;
                    if (_valTenant.iots_billing_history == null) {
                      invoice = `${_valArea.list_area.username}.${
                        _valTenant.username
                      }.${_valTenant.id}-INV-${moment().format("DDMMYY")}-0`;
                    } else {
                      invoice = _valTenant.iots_billing_history.invoice.split(
                        "-"
                      );
                      let lastDate = invoice[invoice.length - 2];
                      let currentDate = moment().format("MMYY");

                      if (lastDate.substr(-4) != currentDate) {
                        invoice = `${_valArea.list_area.username}.${
                          _valTenant.username
                        }.${_valTenant.id}-INV-${moment().format("DDMMYY")}-0`;
                      } else {
                        invoice = invoice.join("-");
                      }
                    }

                    _valTenant.iot_nodes.map((_valNode, _idxNode) => {
                      // updateNode.push({
                      //   id: _valNode.id,
                      //   last_cut_date: moment().format("YYYY-MM-DD"),
                      // });
                      let harga_satuan = 0;
                      _valArea.iot_member_levels.find((val) => {
                        if (val.id == _valTenant.member_level_id) {
                          val.child.find((valChild) => {
                            if (valChild.typeId == _valNode.typeId) {
                              harga_satuan = valChild.pricing;
                            }
                          });
                        }
                      });
                      invoice = invoice.split("-");
                      invoice[invoice.length - 1] =
                        parseInt(invoice[invoice.length - 1]) + 1;
                      invoice = invoice.join("-");
                      let rtu = null;
                      let model = null;
                      if (parseInt(_valNode.typeId) == 4) {
                        rtu = _valNode.iot_detail_rtu.field_billing;
                        model = _valNode.iot_detail_rtu.model;
                      }
                      rawData.push({
                        model: model,
                        rtu: rtu,
                        typeId: _valNode.typeId,
                        areaId: _valArea.id,
                        tenantId: _valTenant.id,
                        nodeId: _valNode.id,
                        cut_date: moment()
                          .subtract(1, "day")
                          .format("YYYY-MM-DD"),
                        invoice: invoice,
                        status: "NEW",
                        biaya_transaksi: 5000,
                        biaya_penyesuaian: 0,
                        ppn: 10,
                        harga_satuan: harga_satuan,
                        periode_cut: _valArea.automatic_bill,
                      });
                      rawDataHistory.push(
                        models.iot_histories.findOne({
                          where: {
                            device_id: _valNode.id,
                            col: models.sequelize.where(
                              models.sequelize.fn(
                                "date",
                                models.sequelize.col("iot_histories.reportTime")
                              ),
                              "<=",
                              moment().subtract(1, "day").format("YYYY-MM-DD")
                            ),
                          },
                          order: [["id", "DESC"]],
                          transaction: t,
                        })
                      );
                      if (_valNode.iots_billing_history != null) {
                        rawData[_idxNode].start_meter =
                          _valNode.iots_billing_history.end_meter;
                        rawData[_idxNode].start_date =
                          _valNode.iots_billing_history.end_date;
                      } else {
                        rawDataHistoryStartFlag.push(_valNode.id);
                        rawDataHistoryStart.push(
                          models.iot_histories.findOne({
                            where: {
                              device_id: _valNode.id,
                            },
                            transaction: t,
                          })
                        );
                      }
                    });
                  });
                });
                return Promise.all(rawDataHistory).then((_result_end) => {
                  _result_end = _result_end.filter((val) => {
                    return val != null;
                  });
                  _result_end.map((_valEnd) => {
                    rawData.find((val, idx) => {
                      if (_valEnd.device_id == val.nodeId) {
                        if (parseInt(val.typeId) == 4) {
                          let billingRtu = JSON.parse(
                            _valEnd.payload
                          ).subMeter.filter((valP) => {
                            return valP.applicationName == val.model;
                          });
                          billingRtu = billingRtu[0].report.filter((valP) => {
                            return valP.name == val.rtu;
                          });
                          let billingRtuValue =
                            parseFloat(billingRtu[0].value) * 1000;
                          rawData[idx].end_date = _valEnd.reportTime;
                          rawData[idx].end_meter = billingRtuValue;
                          rawData[idx].totalizer = billingRtuValue;
                          rawData[idx].usage =
                            rawData[idx].end_meter - rawData[idx].start_meter;
                          rawData[idx].billing_usage =
                            rawData[idx].end_meter - rawData[idx].start_meter;
                        } else {
                          rawData[idx].end_date = _valEnd.reportTime;
                          rawData[idx].end_meter = JSON.parse(
                            _valEnd.payload
                          ).meter.meterReading;
                          rawData[idx].totalizer = JSON.parse(
                            _valEnd.payload
                          ).meter.meterReading;
                          rawData[idx].usage =
                            rawData[idx].end_meter - rawData[idx].start_meter;
                          rawData[idx].billing_usage =
                            rawData[idx].end_meter - rawData[idx].start_meter;
                        }
                      }
                    });
                  });
                  if (rawDataHistoryStartFlag.length == 0) {
                    return models.iots_billing_history.bulkCreate(rawData);
                  } else {
                    return Promise.all(rawDataHistoryStart).then(
                      (_result_start) => {
                        _result_start = _result_start.filter((value) => {
                          return value != null;
                        });
                        _result_start.map((_valStart) => {
                          rawData.find((val, idx) => {
                            if (_valStart.device_id == val.nodeId) {
                              if (parseInt(val.typeId) == 4) {
                                let billingRtus = JSON.parse(
                                  _valStart.payload
                                ).subMeter.filter((valP) => {
                                  return valP.applicationName == val.model;
                                });
                                billingRtus = billingRtus[0].report.filter(
                                  (valP) => {
                                    return valP.name == val.rtu;
                                  }
                                );
                                let billingRtusValue =
                                  parseFloat(billingRtus[0].value) * 1000;
                                rawData[idx].start_date = _valStart.reportTime;
                                rawData[idx].start_meter = billingRtusValue;
                                rawData[idx].usage =
                                  rawData[idx].end_meter -
                                  rawData[idx].start_meter;
                                rawData[idx].billing_usage =
                                  rawData[idx].end_meter -
                                  rawData[idx].start_meter;
                              } else {
                                rawData[idx].start_date = _valStart.reportTime;
                                rawData[idx].start_meter = JSON.parse(
                                  _valStart.payload
                                ).meter.meterReading;
                                rawData[idx].usage =
                                  rawData[idx].end_meter -
                                  rawData[idx].start_meter;
                                rawData[idx].billing_usage =
                                  rawData[idx].end_meter -
                                  rawData[idx].start_meter;
                              }
                            }
                          });
                        });
                        let newRawData = [];
                        let newUpdateNode = [];
                        rawData.map((val) => {
                          if (isNaN(val.usage) == false) {
                            newRawData.push(val);
                            updateNode.push({
                              id: val.nodeId,
                              last_cut_date: moment().format("YYYY-MM-DD"),
                            });
                            // updateNode.find((val2) => {
                            //   if (val2.id == val.nodeId) {
                            //     newUpdateNode.push(val);
                            //   }
                            // });
                          }
                        });
                        return models.iots_billing_history
                          .bulkCreate(newRawData)
                          .then(() => {
                            return models.iot_nodes.bulkCreate(updateNode, {
                              updateOnDuplicate: ["last_cut_date"],
                              transaction: t,
                            });
                          });
                      }
                    );
                  }
                });
              }
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            message: err.message,
          });
        });
    });
  },
};

const models = require("../../models/index");
const { Op } = models.Sequelize;
const axios = require("../../config/axios");
const moment = require("moment");
getDateArray = (first, end) => {
  // eslint-disable-next-line
  let arr = new Array();
  let dt = new Date(first);
  while (dt <= end) {
    arr.push(moment(new Date(dt)).format("YYYY-MM-DD"));
    dt.setDate(dt.getDate() + 1);
  }
  return arr;
};
let combine = (names) => names.filter((v, i) => names.indexOf(v) === i);
require("dotenv").config();
module.exports = {
  svcGetNodeTenant: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_tenant
            .findOne({
              where: { id: data.id },
              include: [
                {
                  model: models.iot_nodes,
                  where: {
                    typeId: { [Op.in]: [1, 2, 3, 4, 7] },
                    device_type_id: 1,
                  },
                  include: [
                    { model: models.iot_node_type },
                    { model: models.iot_detail_rtu },
                  ],
                },
              ],
            })
            .then((_res) => {
              _res = JSON.parse(JSON.stringify(_res));
              if (_res == null) {
                throw new Error("tenant Not Found");
              } else {
                let node = { node: [] };
                _res.iot_nodes.map((value) => {
                  node.node.push(value.id);
                });
                node.result = _res;
                return node;
              }
            });
        })
        .then((result) => {
          resolve(result);
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },
  svcGetNodeInternal: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_internal
            .findOne({
              where: { id: data.id },
              // attributes: { exclude: ["createdAt", "updatedAt"] },
              include: [
                {
                  model: models.iot_nodes,
                  include: [{ model: models.iot_node_type }],
                },
              ],
            })
            .then((_res) => {
              _res = JSON.parse(JSON.stringify(_res));
              if (_res == null) {
                throw new Error("Intenal Not Found");
              } else {
                let node = { node: [] };
                _res.iot_nodes.map((value) => {
                  node.node.push(value.id);
                });
                node.result = _res;
                return node;
              }
            });
        })
        .then((result) => {
          resolve(result);
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },
  svcGetHistoryNode: (
    node_id,
    start,
    end,
    type,
    deveui,
    type_name,
    tenant_name,
    merk,
    field_billing_rtu,
    rtu_model
  ) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.sequelize
            .query(
              `SELECT *
          FROM
            iot_histories a 
          WHERE
            device_id = ${node_id} 
            AND DATE( reportTimeFlag ) <= '${moment(end).format(
              "YYYY-MM-DD"
            )}' AND DATE( reportTimeFlag ) >= '${moment(start)
                .subtract(1, "days")
                .format("YYYY-MM-DD")}' 
            AND (
            reportTimeFlag IN (
            SELECT
              MAX( a.reportTimeFlag ) as max
            FROM
              iot_histories a 
            WHERE
              a.device_id = ${node_id} 
            GROUP BY
            DATE( a.reportTimeFlag )) OR reportTimeFlag IN (
            SELECT
              MIN( a.reportTimeFlag )  as min
            FROM
              iot_histories a 
            WHERE
              a.device_id = ${node_id} 
            GROUP BY
            DATE( a.reportTimeFlag )) 
            
            )
          ORDER BY
          reportTimeFlag ASC`
            )
            .then((result) => {
              let data_before = "";
              let output_final = [];
              let flag = 0;
              if (result[0].length == 0) return result[0];
              let tgl = getDateArray(
                moment(result[0][0].reportTimeFlag).utcOffset("+0700"),
                moment(
                  result[0][result[0].length - 1].reportTimeFlag
                ).utcOffset("+0700")
              );
              tgl.map((val, idx) => {
                let temp = result[0].filter((valP) => {
                  return (
                    moment(val).format("YYYY-MM-DD") ==
                    moment(valP.reportTimeFlag).format("YYYY-MM-DD")
                  );
                });
                if (temp.length > 0) {
                  let start_meter = 0;
                  let end_meter = 0;
                  if (type_name == "RTU") {
                    type = field_billing_rtu.match(/\(([^)]+)\)/)
                      ? field_billing_rtu.match(/\(([^)]+)\)/)[1]
                      : "";
                    start_meter = JSON.parse(temp[0].payload).subMeter.find(
                      (val) => {
                        return val.applicationName == rtu_model;
                      }
                    );
                    start_meter = start_meter.report.find((val) => {
                      return val.name == field_billing_rtu;
                    });
                    start_meter = start_meter.value;

                    end_meter = JSON.parse(
                      temp[temp.length - 1].payload
                    ).subMeter.find((val) => {
                      return val.applicationName == rtu_model;
                    });
                    end_meter = end_meter.report.find((val) => {
                      return val.name == field_billing_rtu;
                    });
                    end_meter = end_meter.value;
                  } else {
                    start_meter = parseFloat(
                      JSON.parse(temp[0].payload).meter.meterReading
                    );
                    start_meter = start_meter / 1000;
                    end_meter = parseFloat(
                      JSON.parse(temp[temp.length - 1].payload).meter
                        .meterReading
                    );
                    end_meter = end_meter / 1000;
                  }

                  output_final.push({
                    unit: type,
                    deveui: deveui,
                    type_name: type_name,
                    tenant_name: tenant_name,
                    device_id: temp[0].device_id,
                    start: temp[0].reportTimeFlag,
                    date: moment(temp[0].reportTimeFlag)
                      .utcOffset("+0700")
                      .format("YYYY-MM-DD"),
                    end: temp[temp.length - 1].reportTimeFlag,
                    start_meter: start_meter,
                    end_meter: end_meter,
                    usage: end_meter - start_meter,
                  });
                }
              });
              return output_final;
            });
        })
        .then((res) => resolve(res))
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: err.message,
          });
        });
    });
  },
};

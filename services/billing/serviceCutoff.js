const models = require("../../models/index");
const axios = require("../../config/axios");

require("dotenv").config();
module.exports = {
  svcGetCutOff: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_cutoff.findAll({ where: data });
        })

        .then((result) => {
          resolve({
            responseCode: 200,
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: JSON.stringify(err),
          });
        });
    });
  },
  svcCreateCutoff: (data, areaId) => {
    return new Promise((resolve, reject) => {
      console.log(data);
      return models.sequelize
        .transaction((t) => {
          return models.iots_cutoff
            .destroy({ where: { areaId: areaId }, transaction: t })
            .then(() => {
              return models.iots_cutoff.bulkCreate(data, { transaction: t });
              // .then(() => {
              //   return models.iot_tenant
              //     .findAll({ where: { area_id: areaId } })
              //     .then((result) => {
              //       result = JSON.parse(JSON.stringify(result));
              //       let raw = [];
              //       data.map((val) => {
              //         result.map((val2, idx) => {
              //           val.tenantId = val2.id;
              //           console.log(val);
              //         });
              //       });
              //       // console.log(data);
              //     });
              // });
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: JSON.stringify(err),
          });
        });
    });
  },
  svcCreateCutoffSaas: (data, areaId) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_cutoff_saas
            .destroy({ where: { areaId: areaId }, transaction: t })
            .then(() => {
              return models.iots_cutoff_saas.bulkCreate(data, {
                transaction: t,
              });
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: JSON.stringify(err),
          });
        });
    });
  },
};

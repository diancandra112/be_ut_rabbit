const { axios_erp, axios_erp2 } = require("../../config/axios");
const FormData = require("form-data");
module.exports = {
  SvcBillingToErp: (
    noted,
    materai,
    ppn,
    data,
    denda,
    admin,
    baseUrl,
    email_erp,
    pass_erp,
    meter_start,
    meter_end,
    period_to,
    period_from,
    iots_erp_item,
    erp_contract_id,
    biaya_admin
  ) => {
    return new Promise((resolve, reject) => {
      axios_erp({
        baseURL: baseUrl,
        url: `/auth/login?email=${email_erp}&password=${pass_erp}`,
        method: "POST",
      })
        .then((res) => {
          data.sub_total = parseInt(data.sub_total.toFixed(4));
          let bodyFormData = new FormData();
          bodyFormData.append("si_date", data.si_date);
          bodyFormData.append("due_date", data.due_date);
          bodyFormData.append("customer_id", data.customer_id);
          bodyFormData.append("term_payment", data.term_payment);
          bodyFormData.append("no_ref", data.no_ref);
          bodyFormData.append("description", noted);
          bodyFormData.append("is_manual", "1");
          bodyFormData.append("disc", "0");
          bodyFormData.append("total_disc", "0");
          bodyFormData.append("ppn", "0");
          bodyFormData.append("contract_id", erp_contract_id);
          bodyFormData.append("period_to", period_to);
          bodyFormData.append("period_from", period_from);
          bodyFormData.append(
            "ppn_value",
            parseInt(parseInt(data.sub_total) * (parseInt(ppn) / 100))
          );
          bodyFormData.append(
            "subtotal",
            parseInt(data.sub_total) +
              materai +
              parseInt(admin) +
              parseInt(denda)
          );
          bodyFormData.append(
            "netto",
            parseInt(data.sub_total) +
              materai +
              parseInt(admin) +
              parseInt(denda) +
              parseInt(parseInt(data.sub_total) * (parseInt(ppn) / 100))
          );

          bodyFormData.append("items[0][meter_start]", meter_start);
          bodyFormData.append("items[0][meter_end]", meter_end);
          bodyFormData.append("items[0][item_id]", data.item_id);
          bodyFormData.append("items[0][unit_id]", data.unit_id);
          bodyFormData.append("items[0][unit_price]", data.unit_price);
          bodyFormData.append("items[0][qty]", data.qty);
          bodyFormData.append("items[0][sub_total]", data.sub_total);
          bodyFormData.append("items[0][disc_item]", "0");
          bodyFormData.append("items[0][disc_item_amount]", "0");
          bodyFormData.append("items[0][total_price]", data.sub_total);

          //materai
          if (materai > 0) {
            bodyFormData.append(
              "items[4][item_id]",
              iots_erp_item.materai_item_id
            );
            bodyFormData.append(
              "items[4][unit_id]",
              iots_erp_item.materai_unit_id
            );
            bodyFormData.append("items[4][unit_price]", materai.toString());
            bodyFormData.append("items[4][qty]", 1);
            bodyFormData.append("items[4][sub_total]", materai.toString());
            bodyFormData.append("items[4][disc_item]", "0");
            bodyFormData.append("items[4][disc_item_amount]", "0");
            bodyFormData.append("items[4][total_price]", materai.toString());
          }

          //denda
          if (parseInt(denda) > 0) {
            bodyFormData.append(
              "items[1][item_id]",
              iots_erp_item.denda_item_id
            );
            bodyFormData.append(
              "items[1][unit_id]",
              iots_erp_item.denda_unit_id
            );
            bodyFormData.append("items[1][unit_price]", parseInt(denda));
            bodyFormData.append("items[1][qty]", 1);
            bodyFormData.append("items[1][sub_total]", parseInt(denda));
            bodyFormData.append("items[1][disc_item]", "0");
            bodyFormData.append("items[1][disc_item_amount]", "0");
            bodyFormData.append("items[1][total_price]", parseInt(denda));
          }

          //admin
          if (admin > 0) {
            bodyFormData.append(
              "items[2][item_id]",
              iots_erp_item.admin_item_id
            );
            bodyFormData.append(
              "items[2][unit_id]",
              iots_erp_item.admin_unit_id
            );
            bodyFormData.append("items[2][unit_price]", admin);
            bodyFormData.append("items[2][qty]", 1);
            bodyFormData.append("items[2][sub_total]", admin);
            bodyFormData.append("items[2][disc_item]", "0");
            bodyFormData.append("items[2][disc_item_amount]", "0");
            bodyFormData.append("items[2][total_price]", admin);
          }

          const formHeaders = bodyFormData.getHeaders();
          axios_erp({
            baseURL: baseUrl,
            url: "/v1/smartmeter/sales_invoices",
            method: "POST",
            headers: {
              authorization: "Bearer " + res.data.access_token,
              ...formHeaders,
            },
            data: bodyFormData,
          })
            .then((res2) => {
              let journal_data = {
                no_ref: res2.data.data.decode,
                journal_type: "SI",
                journal_date: res2.data.data.si_date,
                balance: 0,
                note: null,
                total_debet:
                  parseInt(data.sub_total) +
                  materai +
                  parseInt(admin) +
                  parseInt(denda) +
                  parseInt(parseInt(data.sub_total) * (parseInt(ppn) / 100)),
                total_kredit:
                  parseInt(data.sub_total) +
                  materai +
                  parseInt(admin) +
                  parseInt(denda) +
                  parseInt(parseInt(data.sub_total) * (parseInt(ppn) / 100)),
                detail: [
                  {
                    coa_id: iots_erp_item.piutang_coa_id,
                    coa_name: iots_erp_item.piutang_coa_name,
                    coa_no: iots_erp_item.piutang_coa_no,
                    debet_kredit: "D",
                    note: null,
                    debet_value:
                      parseInt(data.sub_total) +
                      materai +
                      parseInt(admin) +
                      parseInt(denda) +
                      parseInt(
                        parseInt(data.sub_total) * (parseInt(ppn) / 100)
                      ),
                    kredit_value: "0",
                  },
                  {
                    coa_id: iots_erp_item.penjualan_coa_id,
                    coa_name: iots_erp_item.penjualan_coa_name,
                    coa_no: iots_erp_item.penjualan_coa_no,
                    debet_kredit: "K",
                    note: null,
                    debet_value: "0",
                    kredit_value: parseInt(data.sub_total),
                  },
                  {
                    coa_id: iots_erp_item.ppn_coa_id,
                    coa_name: iots_erp_item.ppn_coa_name,
                    coa_no: iots_erp_item.ppn_coa_no,
                    debet_kredit: "K",
                    note: null,
                    debet_value: "0",
                    kredit_value: parseInt(
                      parseInt(data.sub_total) * (parseInt(ppn) / 100)
                    ),
                  },
                  {
                    coa_id: iots_erp_item.materai_coa_id,
                    coa_name: iots_erp_item.materai_coa_name,
                    coa_no: iots_erp_item.materai_coa_no,
                    debet_kredit: "K",
                    note: null,
                    debet_value: "0",
                    kredit_value: materai.toString(),
                  },
                  {
                    coa_id: iots_erp_item.admin_coa_id,
                    coa_name: iots_erp_item.admin_coa_name,
                    coa_no: iots_erp_item.admin_coa_no,
                    debet_kredit: "K",
                    note: null,
                    debet_value: "0",
                    kredit_value: biaya_admin,
                  },
                ],
              };

              if (parseInt(denda) > 0) {
                journal_data.detail.push({
                  coa_id: iots_erp_item.denda_coa_id,
                  coa_name: iots_erp_item.denda_coa_name,
                  coa_no: iots_erp_item.denda_coa_no,
                  debet_kredit: "K",
                  note: null,
                  debet_value: "0",
                  kredit_value: parseInt(denda),
                });
              }
              axios_erp({
                baseURL: baseUrl,
                url: "/v1/smartmeter/journal_sales_invoices",
                method: "POST",
                headers: {
                  authorization: "Bearer " + res.data.access_token,
                },
                data: journal_data,
              })
                .then((res_jurnal) => {
                  res2.data.data.email_erp = email_erp;
                  res2.data.data.pass_erp = pass_erp;
                  res2.data.data.baseUrl = baseUrl;
                  resolve(res2.data.data);
                })
                .catch((err) => {
                  console.log(err);
                  reject(err.response.data);
                });
            })
            .catch((err) => {
              console.log(err);
              reject(err.response.data);
            });
        })
        .catch((err) => {
          console.log(err);
          reject(err.response.data);
        });
    });
  },
};

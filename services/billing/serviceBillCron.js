const models = require("../../models/index");
const moment = require("moment");
const Op = models.Sequelize.Op;
const { midtrans } = require("../../config/axios");
require("dotenv").config();
const { svcGetBilling } = require("./serviceBilling");
const svcNode = (data, member_id, periode_cut) => {
  return new Promise((resolve, reject) => {
    data
      .reduce(
        (chain, item) =>
          chain.then(() => svcNodeBilling(item, member_id, periode_cut)),
        Promise.resolve()
      )
      .then((s) => resolve(s))
      .catch((e) => resolve(e));
  });
};
const svcNodeBilling = (data, tenant, periode_cut) => {
  return new Promise((resolve, reject) => {
    return models.sequelize.transaction((t) => {
      return models.iots_billing_history
        .findOne({
          where: {
            areaId: data.areaId,
            tenantId: data.tenantId,
            nodeId: data.id,
            // status: { [Op.ne]: "CANCEL" },
          },
          order: [["id", "DESC"]],
          attributes: { exclude: ["createdAt", "updatedAt", "tenantId"] },
          transaction: t,
        })
        .then((result) => {
          result = JSON.parse(JSON.stringify(result));
          if (result != null) {
            console.log("new report from last date");
            svcFindNodeHistoryDESC({ nodeId: data.id })
              .then((last_record_raw) => {
                last_record = JSON.parse(last_record_raw.payload).meter
                  .meterReading;
                const report = {
                  areaId: data.areaId,
                  tenantId: data.tenantId,
                  nodeId: data.id,
                  cut_date: moment().format("YYYY-MM-DD"),
                  start_meter: result.totalizer,
                  end_meter: last_record,
                  start_date: result.end_date,
                  end_date: last_record_raw.reportTime,
                  totalizer: last_record,
                  usage: parseFloat(last_record - result.totalizer),
                  ppn: 10,
                  biaya_transaksi: 5000,
                  harga_satuan: data.pricing,
                  periode_cut: periode_cut,
                  status: "NEW",
                };
                svcCreateBilling(report, tenant)
                  .then((res) => {
                    resolve(JSON.stringify(res));
                  })
                  .catch((err) => {
                    reject(err);
                    console.log(err);
                  });
              })
              .catch((err) => {
                reject(JSON.stringify(err));
                console.log(JSON.stringify(err));
              });
          } else {
            console.log("new report from installation date");
            svcFindNodeHistoryASC({
              nodeId: data.id,
              installationDate: data.installationDate,
            })
              .then((first_record_raw) => {
                first_record = JSON.parse(first_record_raw.payload).meter
                  .meterReading;
                svcFindNodeHistoryDESC({ nodeId: data.id })
                  .then((last_record_raw) => {
                    last_record = JSON.parse(last_record_raw.payload).meter
                      .meterReading;
                    const report = {
                      areaId: data.areaId,
                      tenantId: data.tenantId,
                      nodeId: data.id,
                      cut_date: moment().format("YYYY-MM-DD"),
                      start_meter: first_record,
                      end_meter: last_record,
                      start_date: first_record_raw.reportTime,
                      end_date: last_record_raw.reportTime,
                      totalizer: last_record,
                      usage: parseFloat(last_record - first_record),
                      ppn: 10,
                      biaya_transaksi: 5000,
                      harga_satuan: data.pricing,
                      periode_cut: periode_cut,
                      status: "NEW",
                    };
                    svcCreateBilling(report, tenant)
                      .then((res) => {
                        resolve(JSON.stringify(res));
                      })
                      .catch((err) => {
                        reject(err);
                        console.log(err);
                      });
                  })
                  .catch((err) => {
                    reject(JSON.stringify(err));
                    console.log(JSON.stringify(err));
                  });
              })
              .catch((err) => {
                reject(JSON.stringify(err));
                console.log(JSON.stringify(err));
              });
          }
        });
    });
  });
};

const svcMidtrans = (data) => {
  return new Promise((resolve, reject) => {
    midtrans({
      method: "POST",
      url: "/core/charge",
      data: data,
    })
      .then((result) => resolve(result.data))
      .catch((err) => reject(err.response.data));
  });
};

const svcCreateBilling = (data, tenant) => {
  // console.log(data);
  return new Promise((resolve, reject) => {
    let inv = "";
    return models.sequelize
      .transaction((t) => {
        return models.sequelize
          .query(
            `SELECT * FROM iots_billing_histories WHERE areaId = ${
              data.areaId
            } AND tenantId=${data.tenantId} AND nodeId=${
              data.nodeId
            } AND YEAR(cut_date)=${moment().format(
              "YYYY"
            )} AND MONTH(cut_date)=${moment().format(
              "MM"
            )} ORDER BY id DESC LIMIT 1`
            // )} AND status != 'CANCEL' ORDER BY id DESC LIMIT 1`
          )
          .then((result) => {
            result = JSON.parse(JSON.stringify(result));
            result = result[0][0];
            if (result == undefined) {
              inv = `${tenant.member_id}-INV-${moment().format("DDMMYY")}-1`;
            } else {
              inv = result.invoice.split("-");
              inv[inv.length - 1] = parseInt(inv[inv.length - 1]) + 1;
              inv = inv.join("-");
            }
            data.invoice = inv;
            return models.iots_billing_history.findOrCreate({
              where: {
                nodeId: data.nodeId,
                areaId: data.areaId,
                tenantId: data.tenantId,
                cut_date: data.cut_date,
                // status: { [Op.ne]: "CANCEL" },
              },
              defaults: data,
            });
            // .then(([result, status]) => {
            //   if (status == true) {
            //     console.log("test ok", JSON.parse(JSON.stringify(result)));
            //     svcGetBilling({ id: result.id, areaId: result.areaId })
            //       .then((res) => {
            //         res = JSON.parse(JSON.stringify(res));
            //         const dataMid = {
            //           amount: res.response[0].grandtotal,
            //           invoice: result.invoice,
            //           email: tenant.email,
            //           first_name: tenant.tenant_name,
            //           last_name: "",
            //           phone: tenant.handphone,
            //           bank: "bca",
            //         };
            //         svcMidtrans(dataMid)
            //           .then((resmid) => {
            //             return models.iots_billing_history.update(
            //               { virtual_account: resmid.va_numbers[0].va_number },
            //               {
            //                 where: { id: result.id },
            //               }
            //             );
            //           })
            //           .catch((err) => {
            //             reject(err);
            //             console.log(err);
            //           });
            //       })
            //       .catch((err) => {
            //         reject(err);
            //         console.log(err);
            //       });
            //   } else {
            //     resolve("test");
            //   }
            // });
          });
      })
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
        console.log(err);
      });
  });
};

const svcFindNodeHistoryDESC = (data) => {
  return new Promise((resolve, reject) => {
    return models.sequelize
      .transaction((t) => {
        return models.iot_histories.findOne({
          where: {
            device_id: data.nodeId,
            // reportTime: {
            //   [Op.lt]: moment().format("YYYY-MM-DD"),
            // },
            reportTime: models.sequelize.where(
              models.sequelize.fn("date", models.sequelize.col("reportTime")),
              "=",
              moment().format("YYYY-MM-DD")
            ),
          },
          order: [["id", "DESC"]],
        });
      })
      .then((result) => {
        resolve(JSON.parse(JSON.stringify(result)));
      })
      .catch((err) => {
        reject({
          responseCode: 400,
          response: JSON.stringify(err),
        });
      });
  });
};

const svcFindNodeHistoryASC = (data) => {
  return new Promise((resolve, reject) => {
    return models.sequelize
      .transaction((t) => {
        return models.iot_histories.findOne({
          where: {
            device_id: data.nodeId,
            reportTime: {
              [Op.gte]: moment(data.installationDate).format("YYYY-MM-DD"),
            },
          },
          order: [["id", "ASC"]],
        });
      })
      .then((result) => {
        resolve(JSON.parse(JSON.stringify(result)));
      })
      .catch((err) => {
        reject({
          responseCode: 400,
          response: JSON.stringify(err),
        });
      });
  });
};
const pricing = (data) => {
  return new Promise((resolve, reject) => {
    // console.log(data);
    if (data.list_tenant.pricing_option == 1) {
      // console.log(data.list_tenant.iot_pricing_postpaid_alls);
      const all = data.list_tenant.iot_pricing_postpaid_alls;
      // console.log();
      data.iot_nodes.map((val, idx) => {
        all.find((val2) => {
          if (val2.typeId == val.typeId) {
            // console.log(val2);
            data.iot_nodes[idx].pricing = val2.pricing;
          }
        });
      });

      // svcNode(data.iot_nodes, data).then(() => {
      resolve(data);
      // });
    } else {
      const member = data.list_tenant.iot_member_levels;
      data.iot_nodes.map((val2, idx) => {
        member.find((val) => {
          if (val.id == data.member_level) {
            val.child.find((val3) => {
              if (val3.typeId == val2.typeId) {
                data.iot_nodes[idx].pricing = val3.pricing;
              }
            });
          }
        });
      });

      // svcNode(data.iot_nodes, data).then(() => {
      resolve(data);
      // });
    }
  });
};
module.exports = {
  svcGetCutOffV2: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_cutoff.findAll({
            where: data,
            attributes: { exclude: ["createdAt", "updatedAt", "tenantId"] },
            transaction: t,
          });
        })
        .then((result) => {
          resolve(JSON.parse(JSON.stringify(result)));
        })
        .catch((err) => {
          reject(JSON.stringify(err));
        });
    });
  },
  svcGetTenantNode: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_tenant.findAll({
            where: { area_id: data.areaId },
            attributes: [
              "id",
              "area_id",
              "tenant_name",
              "member_id",
              "member_level",
              "email",
              "handphone",
            ],
            include: [
              {
                model: models.iot_nodes,
                required: true,
                where: {
                  device_type_id: 1,
                  [Op.or]: [
                    { typeId: 1 },
                    { typeId: 2 },
                    { typeId: 3 },
                    { typeId: 7 },
                  ],
                },
              },
              {
                model: models.iot_area,
                as: "list_tenant",
                include: [
                  { model: models.iot_pricing_postpaid_options },
                  { model: models.iot_pricing_postpaid_all },
                  {
                    model: models.iot_member_level,
                    include: [
                      { model: models.iot_pricing_postpaid, as: "child" },
                    ],
                  },
                ],
              },
            ],
            transaction: t,
          });
        })
        .then((result) => {
          result = JSON.parse(JSON.stringify(result));
          console.log("======================");
          // console.log(result[0].list_tenant);
          // console.log(data.order);
          if (result.length > 0) {
            result
              .reduce(
                (chain, item) =>
                  // chain.then(() => svcNode(item.iot_nodes, item)),
                  chain.then(() =>
                    pricing(item).then((res) => {
                      // console.log(res);
                      svcNode(res.iot_nodes, res, data.order);
                    })
                  ),
                Promise.resolve()
              )
              .then((s) => resolve(s))
              .catch((e) => resolve(e));
          } else {
            resolve("next");
          }
        })
        .catch((err) => {
          console.log(err);
          reject(JSON.stringify(err));
        });
    });
  },
};

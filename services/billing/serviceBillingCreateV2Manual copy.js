const models = require("../../models/index");
const { Op } = models.Sequelize;
const moment = require("moment");
require("dotenv").config();

module.exports = {
  svcCreateBillingAllManual: (data, area) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_area
            .findOne({
              // where: { id: area.id },
              where: { id: 1 },
              include: [
                {
                  model: models.iot_companies,
                  as: "list_area",
                },
                {
                  model: models.iot_tenant,
                  as: "list_tenant",
                  on: {
                    col1: models.sequelize.where(
                      models.sequelize.col("iot_area.id"),
                      "=",
                      models.sequelize.col("list_tenant.area_id")
                    ),
                    col2: models.sequelize.where(
                      models.sequelize.col("list_tenant.id"),
                      "=",
                      1
                      // data.tenant_id
                    ),
                  },
                  include: [
                    {
                      model: models.iots_billing_history,
                      order: [["id", "DESC"]],
                    },
                    {
                      model: models.iot_nodes,
                      attributes: ["id", "devEui", "typeId", "tenantId"],
                      where: {
                        device_type_id: 1,
                        typeId: [1, 2, 3, 7],
                      },
                      include: [
                        {
                          model: models.iot_histories,
                          limit: 1,
                        },
                        {
                          model: models.iots_billing_history,
                        },
                        {
                          model: models.iot_detail_rtu,
                        },
                      ],
                    },
                  ],
                },
                {
                  model: models.iot_pricing_postpaid_all,
                  on: {
                    col1: models.sequelize.where(
                      models.sequelize.col("iot_area.id"),
                      "=",
                      models.sequelize.col("iot_pricing_postpaid_alls.areaId")
                    ),
                    col2: models.sequelize.where(
                      models.sequelize.col("iot_area.pricing_option"),
                      "=",
                      "1"
                    ),
                  },
                },
                {
                  model: models.iot_member_level,
                  on: {
                    col1: models.sequelize.where(
                      models.sequelize.col("iot_area.id"),
                      "=",
                      models.sequelize.col("iot_member_levels.area_id")
                    ),
                    col2: models.sequelize.where(
                      models.sequelize.col("iot_area.pricing_option"),
                      "=",
                      "2"
                    ),
                  },
                  include: [
                    { model: models.iot_pricing_postpaid, as: "child" },
                  ],
                },
              ],
            })
            .then(async (_result) => {
              _result = JSON.parse(JSON.stringify(_result));
              if (parseInt(_result.pricing_option) == 1) {
                let pricing = _result.iot_pricing_postpaid_alls.filter(
                  (valP) => {
                    return isNaN(parseFloat(valP.pricing)) == true;
                  }
                );
                let newPricing = [];
                pricing.map((val) => {
                  newPricing.push(val.typeId);
                });

                _result.list_tenant.map((valB, idxB) => {
                  newPricing.map((a) => {
                    _result.list_tenant[idxB].iot_nodes = valB.iot_nodes.filter(
                      (valP) => {
                        return valP.typeId != a;
                      }
                    );
                  });
                });
              } else {
              }
              _result.list_tenant.map((valB, idxB) => {
                _result.list_tenant[idxB].iot_nodes = valB.iot_nodes.filter(
                  (valP) => {
                    return (
                      (valP.typeId != 4 && valP.iot_histories.length > 0) ||
                      (valP.typeId == 4 &&
                        valP.iot_detail_rtu.field_billing != null)
                    );
                  }
                );
              });
              _result.list_tenant = _result.list_tenant.filter((valP) => {
                return valP.iot_nodes.length > 0;
              });
              if (_result.list_tenant.length == 0) {
                _result = null;
              }
              if (_result.length == 0) {
                throw new Error("no data");
              } else {
                let rawData = [];
                let rawDataflag = 0;
                let rawDataHistory = [];
                let rawDataHistoryStart = [];
                let rawDataHistoryStartFlag = [];
                let updateNode = [];
              }

              return _result;
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            message: err.message,
          });
        });
    });
  },
};

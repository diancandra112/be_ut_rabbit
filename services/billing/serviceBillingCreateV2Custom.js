const models = require("../../models/index");
const { Op } = models.Sequelize;
const moment = require("moment");
require("dotenv").config();

module.exports = {
  svcCreateBillingCustom: async (data) => {
    return new Promise(async (resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_area
            .findAll({
              where: { pricing_option: 2 },
              include: [
                {
                  model: models.iot_nodes,
                  on: {
                    col1: models.sequelize.where(
                      models.sequelize.col("iot_area.id"),
                      "=",
                      models.sequelize.col("iot_nodes.areaId")
                    ),
                    col2: models.sequelize.where(
                      models.sequelize.col("iot_nodes.typeId"),
                      { [Op.in]: [1, 2, 3, 7] }
                    ),
                    col3: models.sequelize.where(
                      models.sequelize.col("iot_nodes.device_type_id"),
                      "=",
                      1
                    ),
                    col4: models.sequelize.where(
                      models.sequelize.fn(
                        "date",
                        models.sequelize.col("iot_nodes.last_cut_date")
                      ),

                      "<",
                      moment().format("YYYY-MM-DD")
                    ),
                    col5: models.sequelize.where(
                      models.sequelize.col("iot_nodes.tenantId"),
                      "IS NOT",
                      null
                    ),
                  },
                  include: [
                    {
                      model: models.iot_histories,
                      as: "history_start",
                      order: [["reportTimeFlag", "ASC"]],
                      limit: 1,
                    },
                    {
                      model: models.iot_histories,
                      as: "history_end",
                      where: {
                        col1: models.sequelize.where(
                          models.sequelize.fn(
                            "date",
                            models.sequelize.col("iot_histories.reportTimeFlag")
                          ),
                          "<",
                          moment().utcOffset("+0700").format("YYYY-MM-DD")
                        ),
                      },
                      order: [["reportTimeFlag", "DESC"]],
                      limit: 1,
                    },
                    {
                      model: models.iot_tenant,
                      include: [
                        {
                          model: models.iot_member_level,
                          include: [
                            {
                              model: models.iot_pricing_postpaid,
                              as: "child",
                              on: {
                                col1: models.sequelize.where(
                                  models.sequelize.col(
                                    "iot_nodes->iot_tenant->iot_member_level.id"
                                  ),
                                  "=",
                                  models.sequelize.col(
                                    "iot_nodes->iot_tenant->iot_member_level->child.memberId"
                                  )
                                ),
                                col2: models.sequelize.where(
                                  models.sequelize.col("iot_nodes.typeId"),
                                  "=",
                                  models.sequelize.col(
                                    "iot_nodes->iot_tenant->iot_member_level->child.typeId"
                                  )
                                ),
                              },
                            },
                          ],
                        },
                      ],
                    },
                    {
                      model: models.iots_billing_history,
                      as: "last_billing_desc",
                      limit: 1,
                      order: [["id", "DESC"]],
                    },
                  ],
                },
                {
                  model: models.iots_cutoff,
                  as: "cut_off",
                  required: true,
                  on: {
                    col1: models.sequelize.where(
                      models.sequelize.col("iot_area.id"),
                      "=",
                      models.sequelize.col("cut_off.areaId")
                    ),
                    col2: models.sequelize.where(
                      models.sequelize.col("cut_off.tanggal_cutoff"),
                      "=",
                      moment().format("DD")
                    ),
                  },
                },
              ],
            })
            .then((_result) => {
              try {
                _result = JSON.parse(JSON.stringify(_result));
                if (_result.length == 0) {
                  return _result;
                } else {
                  let rawData = [];
                  let updateNode = [];
                  _result.map((valArea, idxArea) => {
                    _result[idxArea].iot_nodes = _result[
                      idxArea
                    ].iot_nodes.filter((valP) => {
                      return valP.iot_tenant.iot_member_level.child.length > 0;
                    });
                  });
                  _result = _result.filter((valP) => {
                    return valP.iot_nodes.length > 0;
                  });

                  _result.map((valArea, idxArea) => {
                    valArea.iot_nodes.map((valNode, idxNode) => {
                      let invoice = "";
                      if (valNode.last_billing_desc.length == 0) {
                        invoice = `${valArea.username}.${
                          valNode.iot_tenant.username
                        }.${valNode.id}-INV-${moment().format("DDMMYY")}-1`;
                      } else {
                        invoice = valNode.last_billing_desc[0].invoice.split(
                          "-"
                        );
                        let lastDate = invoice[invoice.length - 2].substr(-4);
                        let currentDate = moment().format("MMYY");
                        if (lastDate != currentDate) {
                          invoice = `${valArea.username}.${
                            valNode.iot_tenant.username
                          }.${valNode.id}-INV-${moment().format("DDMMYY")}-1`;
                        } else {
                          invoice[invoice.length - 1] =
                            parseInt(invoice[invoice.length - 1]) + 1;
                          invoice = invoice.join("-");
                        }
                      }
                      let start_meter = 0;
                      let start_date = "";
                      let end_meter = 0;
                      let end_date = "";
                      if (valNode.last_billing_desc.length > 0) {
                        start_meter = valNode.last_billing_desc[0].end_meter;
                        start_date = valNode.last_billing_desc[0].end_date;
                        end_meter = JSON.parse(valNode.history_end[0].payload)
                          .meter.meterReading;
                        end_date = valNode.history_end[0].reportTime;
                        totalizer = JSON.parse(valNode.history_end[0].payload)
                          .meter.meterReading;
                      } else {
                        start_meter = JSON.parse(
                          valNode.history_start[0].payload
                        ).meter.meterReading;
                        start_date = valNode.history_start[0].reportTime;
                        end_meter = JSON.parse(valNode.history_end[0].payload)
                          .meter.meterReading;
                        end_date = valNode.history_end[0].reportTime;
                        totalizer = JSON.parse(valNode.history_end[0].payload)
                          .meter.meterReading;
                      }
                      updateNode.push({
                        id: valNode.id,
                        last_cut_date: moment().format("YYYY-MM-DD"),
                      });
                      rawData.push({
                        // model: model,
                        // rtu: rtu,
                        typeId: parseInt(valNode.typeId),
                        deveui: valNode.devEui,
                        node_type:
                          parseInt(valNode.typeId) == 1
                            ? "GAS"
                            : parseInt(valNode.typeId) == 2
                            ? "WATER"
                            : parseInt(valNode.typeId) == 3
                            ? "ELECTRICITY NON CT"
                            : parseInt(valNode.typeId) == 4
                            ? "RTU"
                            : parseInt(valNode.typeId) == 5
                            ? "PRESSURE"
                            : parseInt(valNode.typeId) == 6
                            ? "TEMPERATURE"
                            : "ELECTRICITY CT",
                        unit:
                          parseInt(valNode.typeId) == 3 ||
                          parseInt(valNode.typeId) == 7
                            ? "kwh"
                            : "m3",
                        billing_usage: end_meter - start_meter,
                        usage: end_meter - start_meter,
                        periode_cut: valArea.automatic_bill,
                        start_date: start_date,
                        start_meter: start_meter,
                        end_meter: end_meter,
                        end_date: end_date,
                        totalizer: totalizer,
                        typeId: valNode.typeId,
                        areaId: valArea.id,
                        tenantId: valNode.iot_tenant.id,
                        nodeId: valNode.id,
                        cut_date: moment()
                          .subtract(1, "day")
                          .format("YYYY-MM-DD"),
                        invoice: invoice,
                        status: "NEW",
                        biaya_transaksi: 5000,
                        biaya_penyesuaian: 0,
                        ppn: 10,
                        harga_satuan:
                          valNode.iot_tenant.iot_member_level.child[0].pricing,
                        periode_cut: valArea.automatic_bill,
                      });
                    });
                  });
                  return models.iots_billing_history
                    .bulkCreate(rawData)
                    .then(() => {
                      return models.iot_nodes.bulkCreate(updateNode, {
                        updateOnDuplicate: ["last_cut_date"],
                        transaction: t,
                      });
                    });
                }
              } catch (error) {
                console.log(error);
                return error.message;
              }
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            message: err.message,
          });
        });
    });
  },
};

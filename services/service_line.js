const models = require("../models/index");
module.exports = {
  SvcRegisterLine: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_line
            .findOrCreate({
              where: { line_id: data.line_id, area_id: data.area_id },
              defaults: data,
            })
            .then(([res, status]) => {
              if (status == false) throw new Error("duplicate line_id");
              return "sukses insert data";
            });
        })
        .then((result) => {
          resolve({ responseCode: 200, message: result });
        })
        .catch((error) => {
          console.error(error);
          reject({ responseCode: 400, message: error.message });
        });
    });
  },
  SvcGetLine: (data, area_id) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_line.findAll({
            where: [data, area_id],
          });
        })
        .then((result) => {
          resolve({ responseCode: 200, message: result });
        })
        .catch((error) => {
          console.error(error);
          reject({ responseCode: 400, message: error.message });
        });
    });
  },
  SvcDeleteLine: (id) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_line.destroy({
            where: id,
          });
        })
        .then((result) => {
          resolve({ responseCode: 200, message: "sukses delete data" });
        })
        .catch((error) => {
          console.error(error);
          reject({ responseCode: 400, message: error.message });
        });
    });
  },
  SvcEditLine: (data, id) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_line.update(data, {
            where: id,
          });
        })
        .then((result) => {
          resolve({ responseCode: 200, message: "sukses edit data" });
        })
        .catch((error) => {
          console.error(error);
          reject({ responseCode: 400, message: error.message });
        });
    });
  },
};

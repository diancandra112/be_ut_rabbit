const models = require("../models/index");
const { Op } = models.Sequelize;
const jwt = require("jsonwebtoken");
require("dotenv").config();

module.exports = {
  serviceAddType: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_node_type
            .destroy({
              truncate: true,
            })
            .then(() => {
              return models.iot_node_type.bulkCreate(data);
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: "Berhasil terdaftar",
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error,
          });
        });
    });
  },
  serviceEditType: (dataUser, dataProfile) => {
    return new Promise((resolve, reject) => {});
  },
  serviceGetType: (data) => {
    return new Promise((resolve, reject) => {
      models.iot_node_type
        .findAll({
          where: data,
          order: [["type_id"]],
        })
        .then((result) => {
          console.log(result);
          resolve({
            responseCode: 200,
            list_type: result,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error,
          });
        });
    });
  },
  serviceGetTenantInternal: (data) => {
    return new Promise((resolve, reject) => {
      models.allocation_type
        .findAll({
          where: data,
          order: [["id", "ASC"]],
          include: [{ model: models.iot_node_type }],
        })
        .then((result) => {
          result = JSON.parse(JSON.stringify(result));
          console.log(result);
          let data = [];
          result.map((val, index) => {
            data[index] = {
              type_id: val.iot_node_type.type_id,
              type_name: val.iot_node_type.type_name,
            };
            // console.log(data)
          });

          resolve({
            responseCode: 200,
            list_type: data,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error,
          });
        });
    });
  },
  serviceGetTypePricing: (areaId) => {
    return new Promise((resolve, reject) => {
      return models.iot_nodes
        .findAll({
          where: { areaId: areaId, typeId: { [Op.in]: [1, 2, 3, 7] } },
          attributes: ["typeId"],
        })
        .then((_type) => {
          _type = JSON.parse(JSON.stringify(_type));
          let type = [];
          if (_type.length > 0) {
            _type.map((val, index) => {
              type.push(val.typeId);
            });
          } else {
            type.push(1, 2, 3, 7);
          }
          models.iot_node_type
            .findAll({
              where: { type_id: { [Op.in]: type } },
              order: [["type_id"]],
            })
            .then((result) => {
              resolve({
                responseCode: 200,
                list_type: result,
              });
            })
            .catch((error) => {
              console.log(error);
              reject({
                responseCode: 400,
                messages: error,
              });
            });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error,
          });
        });
    });
  },
};

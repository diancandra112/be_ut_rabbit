const models = require("../models/index");
const axios = require("axios");
require("dotenv").config();
const { Op } = models.Sequelize;
const { midtrans } = require("../config/axios");
module.exports = {
  servicemidtransCallback: (data, invoice, history) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_billing_history
            .update(data, {
              where: {
                invoice: data.invoice,
                status: { [Op.not]: "PAID" },
              },
              transaction: t,
            })
            .then(() => {
              return models.iots_midtrans_callback
                .create(history, {
                  transaction: t,
                })
                .then(() => {
                  return midtrans({
                    method: "POST",
                    url: "/core/cancel",
                    data: { order_id: invoice },
                  })
                    .then((result) => {
                      return "sukses ";
                    })
                    .catch((err) => {
                      console.log(err.response.data);
                      return "sukses ";
                      // throw new Error(err.response.data);
                    });
                });
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: "Berhasil rubah status",
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error.message,
          });
        });
    });
  },
};

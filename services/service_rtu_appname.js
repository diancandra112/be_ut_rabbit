const models = require("../models/index");
const axios = require("axios");
require("dotenv").config();

module.exports = {
  serviceGatewayRegister: (data) => {
    return new Promise((resolve, reject) => {
      axios
        .get(process.env.HOST_SERVER + `/rtu/appname?header=${data}`)
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: result.data.messages,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error,
          });
        });
    });
  },
  serviceRtuEditBilling: (data, update_details) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_detail_rtu
            .update(update_details, {
              where: { node_id: update_details.node_id },
              transaction: t,
            })
            .then(() => {
              return models.iot_nodes.update(data, {
                where: { id: data.id },
                transaction: t,
              });
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            data: result,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error.message,
          });
        });
    });
  },
  serviceBillingDenda: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_billing_fine.findAll({
            where: data,
            transaction: t,
          });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            data: result,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error.message,
          });
        });
    });
  },
};

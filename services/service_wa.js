const models = require("../models/index");
const { wa_token, wa_damp } = require("../config/wa");
module.exports = {
  serviceWaSend: (
    params = [],
    to = [],
    att_arr = [],
    billing_id,
    att_arr1,
    att_arr2
  ) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return wa_damp({
            method: "POST",
            url: "billing_rev1",
            data: {
              to: to,
              token: wa_token,
              param: params,
              header: {
                type: "document",
                data: att_arr1,
                filename:
                  att_arr1.split("1-2")[att_arr1.split("1-2").length - 1],
              },
            },
          }).then((result) => {
            resolve({ responseCode: 200, message: result });
            let data_wa = [];
            result.data.data.map((val_wa) => {
              data_wa.push({
                billing_id: billing_id,
                wa_raw: JSON.stringify(val_wa),
                wa_msgId: val_wa.msgId,
                wa_status: val_wa.status,
                wa_responseCode: result.data.responseCode,
                wa_number: val_wa.to,
                note: "billing",
              });
            });
            models.iots_wa_status.bulkCreate(data_wa);
            models.iots_billing_history
              .update(
                {
                  wa_rimender: JSON.stringify({ params: params, to: to }),
                  wa_raw: JSON.stringify(result.data),
                  wa_msgId: result.data.data[0].msgId,
                  wa_status: result.data.data[0].status,
                  wa_responseCode: result.data.responseCode,
                },
                { where: { id: billing_id } }
              )
              .then(() => {
                setTimeout(() => {
                  wa_damp({
                    method: "POST",
                    url: "billing_attachment",
                    data: {
                      to: to,
                      token: wa_token,
                      param: [
                        att_arr2.split("1-2")[att_arr2.split("1-2").length - 1],
                      ],
                      header: {
                        type: "document",
                        data: att_arr2,
                        filename:
                          att_arr2.split("1-2")[
                            att_arr2.split("1-2").length - 1
                          ],
                      },
                    },
                  }).then(() => {
                    att_arr.map((val) => {
                      wa_damp({
                        method: "POST",
                        url: "billing_attachment",
                        data: {
                          to: to,
                          token: wa_token,
                          param: [
                            val.split("1-2")[val.split("1-2").length - 1],
                          ],
                          header: {
                            type: "document",
                            data: val,
                            filename:
                              val.split("1-2")[val.split("1-2").length - 1],
                          },
                        },
                      });
                    });
                  });
                }, 5000);
              });
          });
        })
        .then((result) => {
          console.log({ responseCode: 200, message: result });
        })
        .catch((error) => {
          console.error(error);
          reject({ responseCode: 400, message: error.message });
        });
    });
  },
};

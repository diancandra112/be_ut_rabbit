const corn = require("node-cron");
const moment = require("moment");
const models = require("../models/index");
const Op = models.Sequelize.Op;
const { svcGetCutOff } = require("../services/billing/serviceCutoff");
const { SvcGagasHourly } = require("../services/gagas/svc_gagas_hourly");
const { SvcGagasDaily } = require("../services/gagas/svc_gagas_daily");
const {
  SvcGatewayCheck,
} = require("../services/dashboard/gateway/service_gateway");
const {
  svcGetAutoBill,
  svcGetUsageHistori,
} = require("../services/billing/serviceAutoBill");
const {
  svcGetCutOffV2,
  svcGetTenantNode,
} = require("../services/billing/serviceBillCron");
const {
  svcGetBillHistory,
  svcGetLastInv,
} = require("../services/billing/serviceBillHistori");
const { setviceAreaList } = require("../services/services_area");
const {
  count,
  findAllNode,
} = require("../services/GetHistoriFromServer/serviceGetHistoryServer");
const {
  svcCreateBillingAll,
} = require("../services/billing/serviceBillingCreateV2");
const {
  svcCreateBillingAllManual,
} = require("../services/billing/serviceBillingCreateV2Manual");
const {
  svcCreateBillingCustom,
} = require("../services/billing/serviceBillingCreateV2Custom");

const GenerateBilling = () => {
  const job = corn.schedule("*/30 3-20 * * *", async () => {
    // const job = corn.schedule("* * * * * *", async () => {
    console.log(`cron job ${moment().format("YYYY-MM-DD HH:mm:ss")}`);
    const tgl = parseInt(moment().format("DD"));
    job.stop();
    svcGetCutOffV2({ tanggal_cutoff: tgl })
      .then((areaList) => {
        if (areaList.length > 0) {
          areaList
            .reduce(
              (chain, item) => chain.then(() => svcGetTenantNode(item)),
              Promise.resolve()
            )
            .then((s) => {
              console.log(s);
              job.start();
            })
            .catch((e) => job.start());
        } else {
          job.start();
        }
      })
      .catch((err) => job.start());
  });
};
const cron_gateway = () => {
  const job = corn.schedule("*/15 * * * *", () => {
    job.stop();
    console.log("running SvcGatewayCheck");
    SvcGatewayCheck()
      .then((e) => {
        console.log(e);
        job.start();
      })
      .catch((e) => {
        console.log(e);
        job.start();
      });
  });
};

const GenerateBillingV2 = () => {
  const job = corn.schedule("*/30 */2 * * *", () => {
    // const job = corn.schedule("* * * * * *", () => {
    job.stop();
    console.log("running create billing");
    // Promise.all([svcCreateBillingAllManual()])a
    Promise.all([svcCreateBillingCustom(), svcCreateBillingAll()])
      .then((e) => {
        console.log("ok");
        job.start();
      })
      .catch((e) => {
        console.log("error");
        job.start();
      });
  });
};

const GagasHourly = () => {
  const job = corn.schedule("40 */1 * * *", () => {
    // const job = corn.schedule("* * * * * *", () => {
    job.stop();
    console.log("running create GagasHourly");
    SvcGagasHourly()
      .then((e) => {
        console.log("ok");
        job.start();
      })
      .catch((e) => {
        console.log("error");
        job.start();
      });
  });
};

const GagasDaily = () => {
  const job = corn.schedule("10 1 * * *", () => {
    // const job = corn.schedule("* * * * * *", () => {
    job.stop();
    console.log("running create GagasHourly");
    SvcGagasDaily()
      .then((e) => {
        console.log("ok");
        job.start();
      })
      .catch((e) => {
        console.log("error");
        job.start();
      });
  });
};

const flush = () => {
  const job = corn.schedule("* * * * *", async () => {
    try {
      console.log("flush");
      job.stop();
      const db = require("../models/index");
      await db.sequelize.query("FLUSH HOSTS");
      let list = await db.sequelize.query("show processlist");
      list = JSON.parse(JSON.stringify(list));
      console.log(list[0].length > 0);
      if (list[0].length > 0) {
        list[0].map((val) => {
          if (val.db == "db_sm_iki" && val.Time >= 250) {
            console.log(val, val.Id, "val");
            db.sequelize.query(`kill ${val.Id}`);
          }
        });
        await db.sequelize.query("FLUSH HOSTS");
      }
      job.start();
    } catch (error) {
      console.log(error);
      job.start();
    }
  });
};

const del_his = () => {
  const job = corn.schedule("* 23 * * *", async () => {
    try {
      console.log("flush");
      job.stop();
      let tgl = moment().subtract(6, "month").format("YYYY-MM-DD");
      let del = await models.iot_histories.destroy({
        where: {
          date: models.sequelize.where(
            models.sequelize.fn("date", models.sequelize.col("createdAt")),
            "<",
            tgl
          ),
        },
      });
      console.log(del);
      // job.start();
    } catch (error) {
      console.log(error);
      // job.start();
    }
  });
};
module.exports = {
  all: () => {
    // flush();
    // del_his();
    // GenerateBillingV2();
    cron_gateway();
    // GagasHourly();
    // GagasDaily();
  },
};

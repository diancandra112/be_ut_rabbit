const models = require("../models/index");
const jwt = require("jsonwebtoken");
const moment = require("moment");
const {
  admin_area,
  finance_area,
  teknisi_area,
  teknisi_company,
  admin_company,
} = require("../config/uac");
require("dotenv").config();

module.exports = {
  serviceUserLogin: (data) => {
    return new Promise((resolve, reject) => {
      models.iot_users
        .findOne({
          where: {
            email: data.email,
            password: data.password,
          },
          attributes: {
            exclude: ["password", "createdAt", "updatedAt"],
          },
          include: [
            { model: models.iot_user_role },
            { model: models.iot_companies },
            { model: models.iot_tenant },
            { model: models.iot_area },
          ],
        })
        .then(async (result) => {
          result = JSON.parse(JSON.stringify(result));
          let dataResult = "";
          if (result != null) {
            if (result.role_id == 5) {
              let area_payload = await models.iot_area.findOne({
                where: { id: result.area_id },
              });
              if (area_payload == null) {
                return reject({
                  responseCode: 401,
                  messages: "Area Not Found",
                });
              }
              let profile = {
                id: result.area_id,
                email: area_payload.email,
                role_id: 2,
                area_id: result.area_id,
                iot_area: area_payload,
                iot_company: null,
                iot_tenant: null,
                role: "AREA",
                real_role: "FINANCE",
              };
              const token = jwt.sign(profile, process.env.SECRET);
              resolve({
                responseCode: 200,
                token: `Bearer ${token}`,
                profile: profile,
                uac: finance_area,
              });
            } else if (result.role_id == 6) {
              let area_payload = await models.iot_area.findOne({
                where: { id: result.area_id },
              });
              if (area_payload == null) {
                return reject({
                  responseCode: 401,
                  messages: "Area Not Found",
                });
              }
              let profile = {
                id: result.area_id,
                role_id: 2,
                email: area_payload.email,
                area_id: result.area_id,
                iot_area: area_payload,
                iot_company: null,
                iot_tenant: null,
                role: "AREA",
                real_role: "TEKNISI",
              };
              const token = jwt.sign(profile, process.env.SECRET);
              resolve({
                responseCode: 200,
                token: `Bearer ${token}`,
                profile: profile,
                uac: finance_area,
              });
            }
            // if (condition) {
            else {
              result.role = result.iot_user_role.role;
              result.real_role = result.iot_user_role.role;
              let uac =
                result.role.trim().toLocaleLowerCase() == "area"
                  ? admin_area
                  : [];
              if (
                result.role.trim().toLocaleLowerCase() == "area" &&
                result.iot_area.saas
              ) {
                uac = admin_area;
                let saas_date = moment(result.iot_area.saas_expired).utcOffset(
                  "0700"
                );
                let now = moment().utcOffset("0700");
                if (saas_date.isSameOrBefore(now)) {
                  return reject({
                    responseCode: 401,
                    messages: "your account is expired",
                  });
                }
              }
              result.id =
                result.iot_company != null
                  ? result.iot_company.id
                  : result.iot_tenant != null
                  ? result.iot_tenant.id
                  : result.iot_area.id;
              delete result.iot_user_role;
              if (result.iot_area != null && result.iot_area != undefined) {
                dataResult = result.iot_area.image;
                delete result.iot_area.image;
              }
              if (
                result.iot_company != null &&
                result.iot_company != undefined
              ) {
                dataResult = result.iot_company.image;
                delete result.iot_company.image;
              }
              const token = jwt.sign(result, process.env.SECRET);
              if (result.iot_area != null && result.iot_area != undefined) {
                result.iot_area.image = dataResult;
              }
              if (
                result.iot_company != null &&
                result.iot_company != undefined
              ) {
                result.iot_company.image = dataResult;
              }
              resolve({
                responseCode: 200,
                token: `Bearer ${token}`,
                profile: result,
                uac: uac,
              });
            }
          } else {
            reject({
              responseCode: 401,
              messages: "Email atau password salah",
            });
          }
        })
        .catch((error) => {
          console.log(error);
        });
    });
  },
  serviceUserLoginRole1: (result) => {
    return new Promise((resolve, reject) => {
      try {
        result = JSON.parse(JSON.stringify(result));
        let dataResult = "";
        result.role = "COMPANY";
        result.real_role = "COMPANY";
        result.id = result.iot_company.id;
        delete result.iot_user_role;
        dataResult = result.iot_company.image;
        delete result.iot_company.image;
        const token = jwt.sign(result, process.env.SECRET);
        result.iot_company.image = dataResult;
        resolve({
          responseCode: 200,
          token: `Bearer ${token}`,
          profile: result,
          uac: admin_company,
        });
      } catch (error) {
        console.log(error);
        return reject({
          responseCode: 500,
          messages: error.message,
        });
      }
    });
  },
  serviceUserLoginRole2: (data) => {
    return new Promise((resolve, reject) => {
      try {
        data = JSON.parse(JSON.stringify(data));
        data.role = "AREA";
        data.real_role = "AREA";
        if (data.iot_area.saas) {
          let saas_date = moment(data.iot_area.saas_expired).utcOffset("0700");
          let now = moment().utcOffset("0700");
          if (saas_date.isSameOrBefore(now)) {
            return reject({
              responseCode: 401,
              messages: "your account is expired",
            });
          }
        }
        data.id = data.iot_area.id;
        delete data.iot_user_role;
        dataResult = data.iot_area.image;
        delete data.iot_area.image;
        const token = jwt.sign(data, process.env.SECRET);
        data.iot_area.image = dataResult;
        resolve({
          responseCode: 200,
          token: `Bearer ${token}`,
          profile: data,
          uac: admin_area,
        });
      } catch (error) {
        console.log(error);
        return reject({
          responseCode: 500,
          messages: error.message,
        });
      }
    });
  },
  serviceUserLoginRole5: (result) => {
    return new Promise(async (resolve, reject) => {
      try {
        result = JSON.parse(JSON.stringify(result));
        if (result.role_id == 5) {
          let area_payload = await models.iot_area.findOne({
            where: { id: result.area_id },
          });
          if (area_payload == null) {
            return reject({
              responseCode: 401,
              messages: "Area Not Found",
            });
          }
          let profile = {
            id: result.area_id,
            email: area_payload.email,
            role_id: 2,
            area_id: result.area_id,
            iot_area: area_payload,
            iot_company: null,
            iot_tenant: null,
            role: "AREA",
            real_role: "FINANCE",
            finance_name: result.iots_finance.fullname,
            finance_id: result.iots_finance.id,
          };
          const token = jwt.sign(profile, process.env.SECRET);
          resolve({
            responseCode: 200,
            token: `Bearer ${token}`,
            profile: profile,
            uac: finance_area,
          });
        }
      } catch (error) {
        console.log(error);
        return reject({
          responseCode: 500,
          messages: error.message,
        });
      }
    });
  },
  serviceUserLoginRole7: (result) => {
    return new Promise(async (resolve, reject) => {
      result = JSON.parse(JSON.stringify(result));
      let dataResult = "";
      let area_payload = await models.iot_area.findOne({
        where: { id: result.area_id },
      });
      if (area_payload == null) {
        return reject({
          responseCode: 401,
          messages: "Area Not Found",
        });
      }
      let profile = {
        id: result.area_id,
        role_id: 2,
        email: area_payload.email,
        area_id: result.area_id,
        iot_area: area_payload,
        iot_company: null,
        iot_tenant: null,
        role: "AREA",
        real_role: "ADMIN",
      };
      const token = jwt.sign(profile, process.env.SECRET);
      resolve({
        responseCode: 200,
        token: `Bearer ${token}`,
        profile: profile,
        uac: admin_area,
      });
    });
  },
  serviceUserLoginRole6: (result) => {
    return new Promise(async (resolve, reject) => {
      result = JSON.parse(JSON.stringify(result));
      let dataResult = "";
      let area_payload = await models.iot_area.findOne({
        where: { id: result.area_id },
      });
      if (area_payload == null) {
        return reject({
          responseCode: 401,
          messages: "Area Not Found",
        });
      }
      let profile = {
        user_id: result.id,
        id: result.area_id,
        role_id: 2,
        email: area_payload.email,
        area_id: result.area_id,
        iot_area: area_payload,
        iot_company: null,
        iot_tenant: null,
        role: "AREA",
        real_role: "TEKNISI",
      };
      const token = jwt.sign(profile, process.env.SECRET);
      resolve({
        responseCode: 200,
        token: `Bearer ${token}`,
        profile: profile,
        uac: finance_area,
      });
    });
  },
  serviceUserLoginRole8: (result) => {
    return new Promise(async (resolve, reject) => {
      result = JSON.parse(JSON.stringify(result));
      let area_payload = await models.iot_companies.findOne();
      if (area_payload == null) {
        return reject({
          responseCode: 401,
          messages: "Area Not Found",
        });
      }
      let profile = {
        user_id: result.id,
        id: area_payload.id,
        role_id: 1,
        email: "area_payload.email",
        area_id: null,
        iot_area: null,
        iot_company: area_payload,
        iot_tenant: null,
        role: "COMPANY",
        real_role: "TEKNISI COMPANY",
      };
      const token = jwt.sign(profile, process.env.SECRET);
      resolve({
        responseCode: 200,
        token: `Bearer ${token}`,
        profile: profile,
        uac: teknisi_company,
      });
    });
  },
};

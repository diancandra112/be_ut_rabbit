const models = require("../models/index");
const Op = models.Sequelize.Op;
const jwt = require("jsonwebtoken");
require("dotenv").config();

module.exports = {
  setviceAreaList: (data, type) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_area.findAll({
            attributes: ["id", "pricing_option"],
            include: [
              {
                model: models.iots_cutoff,
                where: data,
                required: true,
                attributes: {
                  exclude: ["createdAt", "updatedAt", "tenantId", "status"],
                },
              },
              {
                model: models.iot_tenant,
                as: "list_tenant",
                include: [
                  {
                    model: models.iot_nodes,
                    where: { [Op.or]: type },
                    required: true,
                    include: [
                      { model: models.iot_detail_water },
                      { model: models.iot_detail_gas, as: "iot_detail_gas" },
                      { model: models.iot_detail_electric },
                      { model: models.iot_detail_electricct },
                      // { model: models.iot_detail_rtu },
                      // { model: models.iot_detail_temperature },
                      // { model: models.iot_detail_preassure }
                    ],
                  },
                ],
              },
            ],
          });
        })

        .then((result) => {
          result = JSON.parse(JSON.stringify(result));
          // console.log(result[0].list_tenant[0].iot_nodes);
          resolve({
            responseCode: 200,
            response: result,
          });
        })
        .catch((err) => {
          console.log(err);
          reject({
            responseCode: 400,
            response: JSON.stringify(err),
          });
        });
    });
  },
  serviceEditAreaProfile: (data, body) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_area.update(body, {
            where: data,
          });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: "berhasil update area profile",
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 500,
            messages: "internal server error",
            payload: error.message,
          });
        });
    });
  },
  serviceAreaProfileErp: (data) => {
    return new Promise((resolve, reject) => {
      models.iot_area
        .update(data, {
          where: { id: data.id },
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: "berhasil update area profile",
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 500,
            messages: "internal server error",
            payload: error,
          });
        });
    });
  },
  serviceAreaProfile: (data) => {
    return new Promise((resolve, reject) => {
      models.iot_area
        .findOne({
          where: data,
          include: [
            {
              model: models.iots_cutoff,
              attributes: { exclude: ["createdAt", "updatedAt", "id"] },
            },
            {
              model: models.iots_cutoff_saas,
              attributes: { exclude: ["createdAt", "updatedAt", "id"] },
            },
          ],
          attributes: {
            exclude: ["user_id", "createdAt", "updatedAt"],
          },
        })
        .then((result) => {
          result = JSON.parse(JSON.stringify(result));
          if (result == null) {
            resolve({
              responseCode: 404,
              messages: "area tidak di temukan di company ini",
            });
          } else {
            resolve({
              responseCode: 200,
              messages: JSON.parse(JSON.stringify(result)),
            });
          }
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 500,
            messages: "internal server error",
            payload: error,
          });
        });
    });
  },
  serviceAreaGet: () => {
    return new Promise(async (resolve, reject) => {
      try {
        let com = await models.iot_companies.findOne();
        com = JSON.parse(JSON.stringify(com));
        let area = await models.iot_area.findAll({
          order: [["area_name", "ASC"]],
        });
        area = JSON.parse(JSON.stringify(area));
        com.list_area = area;
        resolve({
          responseCode: 200,
          messages: com,
        });
      } catch (error) {
        console.log(error);
        reject({
          responseCode: 500,
          messages: error.message,
          payload: error,
        });
      }
    });
  },
  serviceAreaRegister: (dataUser, dataProfile) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_users
            .findOrCreate({
              where: {
                email: dataUser.email,
              },
              defaults: dataUser,
              transaction: t,
            })
            .then(([res, status]) => {
              res = JSON.parse(JSON.stringify(res));
              if (status == false) {
                throw new Error();
              }
              dataProfile.user_id = res.id;
              dataProfile.pricing_option = 1;
              return models.iot_area
                .create(dataProfile, { transaction: t })
                .then((res_area) => {
                  res_area = JSON.parse(JSON.stringify(res_area));
                  return models.iots_cutoff.create(
                    {
                      areaId: res_area.id,
                      tanggal_cutoff: 1,
                      order: 1,
                      auto_bill_type: 1,
                    },
                    { transaction: t }
                  );
                });
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: "Berhasil terdaftar",
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: "Email telah digunakan",
          });
        });
    });
  },
  serviceEditAlarmPressure: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_area
            .update(data, { where: { id: data.id } })
            .then((area) => {
              models.iot_nodes.update(
                {
                  interval_alarm_pressure: data.interval_pressure,
                  alarm_pressure: data.pressure,
                },
                { where: { typeId: 5, areaId: data.id } }
              );
              models.iot_nodes.update(
                {
                  interval_alarm_pressure: data.interval_water_pressure,
                  alarm_pressure: data.water_pressure,
                },
                { where: { typeId: 8, areaId: data.id } }
              );
              models.iot_nodes.update(
                {
                  interval_alarm_pressure: data.interval_pressure_pt,
                  alarm_pressure: data.pressure_pt,
                },
                { where: { typeId: 9, areaId: data.id } }
              );
              models.iot_nodes.update(
                {
                  interval_alarm_pressure: data.interval_low_pressure,
                  alarm_pressure: data.low_pressure,
                },
                { where: { typeId: 11, areaId: data.id } }
              );
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: "berhasil update area profile",
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 500,
            messages: "internal server error",
            payload: error.message,
          });
        });
    });
  },
  serviceAlarmPressure: (data) => {
    return new Promise((resolve, reject) => {
      models.iot_area
        .findOne({
          where: data,
          attributes: [
            "pressure",
            "water_pressure",
            "pressure_pt",
            "low_pressure",
            "interval_pressure",
            "interval_water_pressure",
            "interval_pressure_pt",
            "interval_low_pressure",
          ],
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: result,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 500,
            messages: "internal server error",
            payload: error,
          });
        });
    });
  },
  serviceSaveErp: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_erp_item
            .destroy({
              where: { areaId: data.areaId },
              transaction: t,
            })
            .then(() => {
              return models.iots_erp_item.create(data, {
                transaction: t,
              });
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: "berhasil update area profile",
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 500,
            messages: "internal server error",
            payload: error.message,
          });
        });
    });
  },
  serviceGetErp: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iots_erp_item.findOne({
            where: data,
            transaction: t,
          });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: "berhasil update area profile",
            data: result,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 500,
            messages: "internal server error",
            payload: error.message,
          });
        });
    });
  },
  serviceTenantPassword: (data) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_tenant
            .findAll({
              where: { [Op.or]: { id: data.id, username: data.username } },
              transaction: t,
            })
            .then((res_tenant) => {
              res_tenant = JSON.parse(JSON.stringify(res_tenant));
              if (res_tenant.length > 1) {
                throw new Error("username already use");
              }
              return models.iot_users
                .findAll({
                  where: {
                    [Op.or]: {
                      id: res_tenant[0].user_id,
                      email: data.username,
                    },
                  },
                  transaction: t,
                })
                .then((res_user) => {
                  res_user = JSON.parse(JSON.stringify(res_user));
                  if (res_user.length > 1) {
                    throw new Error("username already use");
                  }
                  return models.iot_tenant
                    .update(
                      {
                        username: data.username,
                        password: data.password,
                        password_flag: data.password_flag,
                      },
                      {
                        where: { id: data.id },
                        transaction: t,
                      }
                    )
                    .then(() => {
                      return models.iot_users.update(
                        { email: data.username, password: data.password },
                        {
                          where: { id: res_tenant[0].user_id },
                          transaction: t,
                        }
                      );
                    });
                });
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: "berhasil update Tenant Password",
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 500,
            messages: error.message,
          });
        });
    });
  },
  serviceTenantParameters: (data, tenant) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_tenant
            .update(data, {
              where: { id: tenant },
              transaction: t,
            })
            .then(() => {
              return models.iot_nodes.update(data, {
                where: { tenantId: tenant },
                transaction: t,
              });
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: "berhasil update Tenant Password",
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 500,
            messages: "internal server error",
            payload: error.message,
          });
        });
    });
  },
  serviceGetTenantParameters: (tenant) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_tenant.findOne({
            attributes: [
              "id",
              "anomali_usage_start_date",
              "anomali_usage_end_date",
              "minim_balance_gas",
              "minim_balance_water",
              "minimum_charge_gas",
              "minimum_charge_water",
              "minimum_charge_electricity_ct",
              "minimum_charge_electricity_non_ct",
              "is_ppn",
            ],
            where: { id: tenant },
            transaction: t,
          });
        })
        .then((result) => {
          result = JSON.parse(JSON.stringify(result));
          if (result != null) {
            result.minim_balance_gas /= 1000;
            result.minim_balance_water /= 1000;
          }
          resolve({
            responseCode: 200,
            messages: "berhasil update Tenant Password",
            result: result,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 500,
            messages: "internal server error",
            payload: error.message,
          });
        });
    });
  },
};

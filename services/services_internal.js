const models = require("../models/index");
const jwt = require("jsonwebtoken");
require("dotenv").config();

module.exports = {
  serviceInternalEdit: (data) => {
    return new Promise((resolve, reject) => {
      models.iot_internal
        .update(data, {
          where: { id: data.id },
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: "Berhasil Update Internal",
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 500,
            messages: "internal server error",
            payload: error,
          });
        });
    });
  },
  serviceInternalGet: (data) => {
    return new Promise((resolve, reject) => {
      models.iot_internal
        .findAll({
          where: data,
          attributes: { exclude: ["user_id", "createdAt", "updatedAt"] },
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            list_internal: JSON.parse(JSON.stringify(result)),
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 500,
            messages: "internal server error",
            payload: error,
          });
        });
    });
  },
  serviceInternalRegister: (dataProfile, nodeType) => {
    return new Promise((resolve, reject) => {
      return models.sequelize
        .transaction((t) => {
          return models.iot_internal
            .findOrCreate({
              where: {
                internal_name: dataProfile.internal_name,
              },
              defaults: dataProfile,
              transaction: t,
            })
            .then(([res, status]) => {
              res = JSON.parse(JSON.stringify(res));
              nodeType.map((val, index) => {
                nodeType[index].internal_id = res.id;
              });
              if (status == false) {
                throw new Error();
              }
              return models.allocation_type.bulkCreate(nodeType);
            });
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: "Berhasil menambah internal",
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: "nama internal telah digunakan",
          });
        });
    });
  },
};

const models = require("../models/index");
const { Op } = require("sequelize");
require("dotenv").config();

module.exports = {
  serviceHistoryFilter: (start, end, device_id) => {
    return new Promise((resolve, reject) => {
      let current_datetime = new Date("2020-02-23 00:00:00");
      console.log(current_datetime);
      models.iot_histories
        .findAll({
          where: {
            device_id: device_id,
            reportTime: {
              [Op.between]: [start, end],
            },
          },
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: result,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error,
          });
        });
    });
  },
  serviceHistoryRangeFilter: (start, end, device_id) => {
    return new Promise((resolve, reject) => {
      models.iot_histories
        .findAll({
          where: {
            device_id: device_id,
            reportTime: {
              [Op.between]: [start, end],
            },
          },
        })
        .then((result) => {
          resolve({
            responseCode: 200,
            messages: result,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error,
          });
        });
    });
  },
  serviceHistoryNodev2: (data, start, end, device_id, typeId) => {
    return new Promise((resolve, reject) => {
      let child = "";
      if (data.child != undefined) {
        child = data.child;
        delete data.child;
      }
      models.iot_histories
        .findAll({
          where: {
            device_id: device_id,
            reportTime: {
              [Op.between]: [start, end],
            },
          },
          order: [["id", "DESC"]],
          include: [
            {
              model: models.iot_nodes,
              include: [
                { model: models.iot_device_type },
                { model: models.iot_node_type },

                { model: models.iot_detail_water },
                { model: models.iot_detail_gas, as: "iot_detail_gas" },
                { model: models.iot_detail_electric },
                { model: models.iot_detail_rtu },
                { model: models.iot_detail_temperature },
                { model: models.iot_detail_preassure },
              ],
            },
          ],
        })
        .then((result) => {
          result = JSON.parse(JSON.stringify(result));
          console.log(result);
          switch (parseInt(typeId)) {
            case 1:
              console.log("GAS");
              result.map((val, index) => {
                result[index].payload = JSON.parse(val.payload);
                console.log(val);
                const data = {
                  devEui: val.payload.devEUI,
                  update_time: val.payload.reportTime,
                  totalizer: val.payload.meter.meterReading,
                  valve: val.payload.meter.valve,
                  usage: "",
                  balance:
                    parseInt(val.iot_node.iot_detail_gas.balance) -
                    parseInt(val.payload.meter.meterReading),
                  battrey_level: val.payload.meter.battery,
                };
                result[index].payload = data;
              });
              break;
            case 2:
              console.log("WATER");
              result.map((val, index) => {
                result[index].payload = JSON.parse(val.payload);
                const data = {
                  devEui: val.payload.devEUI,
                  update_time: val.payload.reportTime,
                  totalizer: val.payload.meter.meterReading,
                  valve: val.payload.meter.valve,
                  usage: "",
                  balance:
                    parseInt(val.iot_node.iot_detail_water.balance) -
                    parseInt(val.payload.meter.meterReading),
                  battrey_level: val.payload.meter.battery,
                };
                result[index].payload = data;
              });
              break;
            case 3:
              console.log("ELECTRICITY");
              result.map((val, index) => {
                console.log(val);
                result[index].payload = JSON.parse(val.payload);

                const data = {
                  devEui: val.payload.devEUI,
                  update_time: val.payload.reportTime,
                  totalizer: val.payload.meter.meterReading,
                  valve: val.payload.meter.valve,
                  usage: "",
                  balance:
                    parseInt(val.iot_node.iot_detail_electric.balance) -
                    parseInt(val.payload.meter.meterReading),
                  battrey_level: val.payload.meter.battery,
                };
                result[index].payload = data;
              });
              break;
            case 4:
              console.log("RTU");
              result.map((val, index) => {
                result[index].payload = JSON.parse(val.payload);
                let NewData = "";
                val.payload.subMeter.map((val) => {
                  if (val.applicationName == child) {
                    NewData = val;
                  }
                });
                const data = {
                  devEui: val.payload.devEUI,
                  update_time: val.reportTime,
                  raw: NewData.report,
                };
                result[index] = data;
              });
              break;
            case 5:
              console.log("PRESSURE");
              result.map((val, index) => {
                result[index].payload = JSON.parse(val.payload);
                const data = {
                  devEui: val.payload.hardware_serial,
                  update_time: val.reportTime,
                  preassure: val.payload.payload_fields.gasPressure.toFixed(2),
                  battrey_level: val.payload.payload_fields.batteryVoltage,
                };
                result[index] = data;
              });
              break;
            case 9:
              console.log("PRESSURE PT");
              result.map((val, index) => {
                result[index].payload = JSON.parse(val.payload);
                const data = {
                  devEui: val.payload.hardware_serial,
                  update_time: val.reportTime,
                  preassure:
                    parseFloat(
                      val.payload.payload_fields.gasPressure.toFixed(2)
                    ) *
                      2 +
                    10,
                  battrey_level: val.payload.payload_fields.batteryVoltage,
                };
                result[index] = data;
              });
              break;
            case 6:
              console.log("TEMPERATURE");
              result.map((val, index) => {
                result[index].payload = JSON.parse(val.payload);

                const data = {
                  devEui: val.payload.devEUI,
                  update_time: val.payload.reportTime,
                  Temperature: val.payload.meter.exReading[0].value,
                  Humidity: val.payload.meter.exReading[1].value,
                };
                result[index] = data;
              });
              break;

            default:
              break;
          }

          resolve({
            responseCode: 200,
            history_node: result,
          });
        })
        .catch((error) => {
          console.log(error);
          reject({
            responseCode: 400,
            messages: error,
          });
        });
    });
  },
};

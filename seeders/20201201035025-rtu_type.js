"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     */
    await queryInterface.bulkInsert(
      "iot_node_types",
      [
        {
          type_id: 101,
          type_name: "RTU GAS",
          active: false,
          satuan: "M3",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          type_id: 102,
          type_name: "RTU WATER",
          active: false,
          satuan: "M3",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          type_id: 103,
          type_name: "RTU ELECTRICITY NON CT",
          active: false,
          satuan: "Kwh",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          type_id: 104,
          type_name: "RTU ELECTRICITY CT",
          active: false,
          satuan: "Kwh",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     */
    const { Op } = Sequelize;
    await queryInterface.bulkDelete(
      "iot_node_types",
      { type_id: { [Op.in]: [101, 102, 103, 104] } },
      {}
    );
  },
};

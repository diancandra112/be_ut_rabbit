"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "iot_user_roles",
      [
        {
          id: 1,
          role: "COMPANY",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 2,
          role: "AREA",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 3,
          role: "TENANT",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 4,
          role: "INTERNAL",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 5,
          role: "FINANCE",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 6,
          role: "TEKNISI",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 7,
          role: "ADMIN",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 8,
          role: "TEKNISI COMPANY",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      */
    return queryInterface.bulkDelete("iot_user_roles", null, {});
  },
};

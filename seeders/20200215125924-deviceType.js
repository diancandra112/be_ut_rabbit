'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('iot_device_types', [
      {
        name: 'POSTPAID',
        createdAt: new Date,
        updatedAt: new Date 
      },
      {
        name: 'PREPAID',
        createdAt: new Date,
        updatedAt: new Date 
      },
    ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};

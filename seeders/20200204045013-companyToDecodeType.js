"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "iot_decode_types",
      [
        {
          id: 1,
          company_name: "PGN",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 2,
          company_name: "LMP",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 3,
          company_name: "GAGAS",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 4,
          company_name: "GAMATECHNO",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      */
    return queryInterface.bulkDelete("iot_user_roles", null, {});
  },
};

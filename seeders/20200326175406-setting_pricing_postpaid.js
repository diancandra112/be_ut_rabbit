"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "iot_pricing_postpaid_options",
      [
        {
          postpaid_id: "1",
          option_name: "One Price For All Customer",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          postpaid_id: "2",
          option_name: "Setting Price Member Level",
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};

"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "iots_auto_bill_options",
      [
        {
          name: "Monthly 1x",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          name: "Monthly 2x",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          name: "Monthly 3x",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          name: "Monthly 4x",
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};

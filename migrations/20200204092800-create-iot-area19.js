"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iot_areas",
          "saas_30",
          {
            type: Sequelize.STRING(10),
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_areas",
          "saas_7",
          {
            type: Sequelize.STRING(10),
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_areas",
          "saas_3",
          {
            type: Sequelize.STRING(10),
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iot_areas", "saas_30", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_areas", "saas_7", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_areas", "saas_3", {
          transaction: t,
        }),
      ]);
    });
  },
};

'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('iot_detail_preassures', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      last_update: {
        type: Sequelize.DATE
      },
      devEui: {
        type: Sequelize.STRING
      },
      battery_level: {
        type: Sequelize.STRING
      },
      interval: {
        type: Sequelize.INTEGER
      },
      desc: {
        type: Sequelize.STRING
      },
      node_id: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('iot_detail_preassures');
  }
};
"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("iots_billing_fines", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      status: {
        type: Sequelize.STRING,
      },
      deskripsi: {
        type: Sequelize.STRING,
      },
      inv_id: {
        type: Sequelize.INTEGER,
      },
      jumlah_hari: {
        type: Sequelize.INTEGER,
        defaultValue: 0,
      },
      jumlah_denda: {
        type: Sequelize.DOUBLE,
        defaultValue: 0,
      },
      used: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      link_inv_denda: {
        type: Sequelize.INTEGER,
      },
      bln_inv: {
        type: Sequelize.STRING,
      },
      bln_denda: {
        type: Sequelize.STRING,
      },
      nodeId: {
        type: Sequelize.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("iots_billing_fines");
  },
};

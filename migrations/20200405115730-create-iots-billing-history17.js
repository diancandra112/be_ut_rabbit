"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iots_billing_histories",
          "materai",
          {
            type: Sequelize.DOUBLE,
            defaultValue: 10000,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iots_billing_histories", "materai", {
          transaction: t,
        }),
      ]);
    });
  },
};

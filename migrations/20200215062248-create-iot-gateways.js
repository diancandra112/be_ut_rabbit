"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("iot_gateways", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      gateway_name: {
        type: Sequelize.STRING,
      },
      unit_model: {
        type: Sequelize.STRING,
      },
      mac_address: {
        type: Sequelize.STRING,
      },
      power_source: {
        type: Sequelize.STRING,
      },
      description: {
        type: Sequelize.STRING,
      },
      installation_date: {
        type: Sequelize.DATE,
      },
      latitude: {
        type: Sequelize.STRING,
      },
      longitude: {
        type: Sequelize.STRING,
      },
      area_id: {
        type: Sequelize.INTEGER,
      },
      last_update: {
        type: Sequelize.DATE,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("iot_gateways");
  },
};

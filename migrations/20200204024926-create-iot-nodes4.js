"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iot_nodes",
          "start_anomali_date",
          {
            type: Sequelize.DATE,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_nodes",
          "start_anomali_meter",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_nodes",
          "is_anomali",
          {
            type: Sequelize.BOOLEAN,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iot_nodes", "start_anomali_date", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_nodes", "start_anomali_meter", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_nodes", "is_anomali", {
          transaction: t,
        }),
      ]);
    });
  },
};

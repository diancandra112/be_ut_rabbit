"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("iots_tiketings", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      no_tiket: {
        type: Sequelize.STRING,
      },
      type_user: {
        type: Sequelize.STRING,
      },
      ticket_type: {
        type: Sequelize.ENUM("installation", "maintenance"),
      },
      technician_id: {
        type: Sequelize.INTEGER,
      },
      technician_name: {
        type: Sequelize.STRING,
      },
      type_user: {
        type: Sequelize.ENUM("tenant", "internal"),
      },
      user_id: {
        type: Sequelize.INTEGER,
      },
      area_id: {
        type: Sequelize.INTEGER,
      },
      note: {
        type: Sequelize.TEXT,
      },
      ticket_processing_date: {
        type: "TIMESTAMP",
      },
      ticket_close_date: {
        type: "TIMESTAMP",
      },
      ticket_delegation_date: {
        type: "TIMESTAMP",
      },
      status: {
        type: Sequelize.ENUM("new", "onprogress", "close"),
      },
      createdAt: {
        allowNull: false,
        type: "TIMESTAMP",
      },
      updatedAt: {
        allowNull: false,
        type: "TIMESTAMP",
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("iots_tiketings");
  },
};

"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iots_history_gas",
          "max_totalizer",
          {
            type: Sequelize.DOUBLE,
            defaultValue: 9999000,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iots_history_gas", "max_totalizer", {
          transaction: t,
        }),
      ]);
    });
  },
};

"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("iot_histories_rtu_water_levels", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      water_level: {
        type: Sequelize.DOUBLE,
        defaultValue: 0,
      },
      water_surface_to_bottom: {
        type: Sequelize.DOUBLE,
        defaultValue: 0,
      },
      sensor_to_botom: {
        type: Sequelize.DOUBLE,
        defaultValue: 0,
      },
      reportTime: {
        type: Sequelize.STRING,
      },
      reportTimeFlag: {
        type: Sequelize.STRING,
      },
      device_id: {
        type: Sequelize.INTEGER,
      },
      tenantId: {
        type: Sequelize.INTEGER,
      },
      internalId: {
        type: Sequelize.INTEGER,
      },
      devEui: {
        type: Sequelize.STRING,
      },
      meter_id: {
        type: Sequelize.STRING,
      },
      alarm_high: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      alarm_low: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
    await queryInterface.addIndex(
      "iot_histories_rtu_water_levels",
      ["device_id", "reportTime"],
      { unique: true }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("iot_histories_rtu_water_levels");
  },
};

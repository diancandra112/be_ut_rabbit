"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface
      .createTable("iot_histories", {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        device_id: {
          type: Sequelize.STRING,
        },
        usage: {
          type: Sequelize.TEXT,
        },
        payload: {
          type: Sequelize.TEXT,
        },
        reportTime: {
          type: Sequelize.STRING,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
      })
      .then(() =>
        queryInterface.addIndex("iot_histories", ["device_id", "reportTime"], {
          unique: true,
        })
      );
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("iot_histories");
  },
};

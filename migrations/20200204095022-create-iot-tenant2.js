"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.changeColumn(
          "iot_tenants",
          "username",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.changeColumn(
          "iot_tenants",
          "password",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.changeColumn(
          "iot_tenants",
          "username",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.changeColumn(
          "iot_tenants",
          "password",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
      ]);
    });
  },
};

"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iot_nodes",
          "interval_alarm_pressure",
          {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_nodes",
          "last_alarm_pressure",
          {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iot_nodes", "interval_alarm_pressure", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_nodes", "last_alarm_pressure", {
          transaction: t,
        }),
      ]);
    });
  },
};

"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iot_nodes",
          "setting_pulse",
          {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_nodes",
          "live_pulse",
          {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_nodes",
          "start_totalizer",
          {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_nodes",
          "flag_install",
          {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iot_nodes", "setting_pulse", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_nodes", "live_pulse", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_nodes", "start_totalizer", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_nodes", "flag_install", {
          transaction: t,
        }),
      ]);
    });
  },
};

"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iots_billing_histories",
          "minimum_charge",
          {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_billing_histories",
          "minimum_charge_total",
          {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_billing_histories",
          "billing_type",
          {
            type: Sequelize.ENUM("area", "tenant", "node"),
            defaultValue: "tenant",
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn(
          "iots_billing_histories",
          "minimum_charge",
          {
            transaction: t,
          }
        ),
        queryInterface.removeColumn(
          "iots_billing_histories",
          "minimum_charge_total",
          {
            transaction: t,
          }
        ),
        queryInterface.removeColumn("iots_billing_histories", "billing_type", {
          transaction: t,
        }),
      ]);
    });
  },
};

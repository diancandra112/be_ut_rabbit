"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iots_billing_histories",
          "correction_usage",
          {
            type: Sequelize.STRING,
            defaultValue: "0",
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_billing_histories",
          "billing_usage",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn(
          "iots_billing_histories",
          "correction_usage",
          {
            transaction: t,
          }
        ),
        queryInterface.removeColumn("iots_billing_histories", "billing_usage", {
          transaction: t,
        }),
      ]);
    });
  },
};

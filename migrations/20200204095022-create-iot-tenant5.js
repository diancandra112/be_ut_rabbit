"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iot_tenants",
          "minim_balance_gas",
          {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_tenants",
          "minim_balance_water",
          {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_tenants",
          "anomali_usage_start_date",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_tenants",
          "anomali_usage_end_date",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iot_tenants", "minim_balance_gas", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_tenants", "minim_balance_water", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_areas", "anomali_usage_start_date", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_areas", "anomali_usage_end_date", {
          transaction: t,
        }),
      ]);
    });
  },
};

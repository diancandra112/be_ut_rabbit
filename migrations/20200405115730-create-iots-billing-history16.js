"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iots_billing_histories",
          "erp_payload",
          {
            type: Sequelize.TEXT,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_billing_histories",
          "erp_id",
          {
            type: Sequelize.INTEGER,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iots_billing_histories", "erp_payload", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_billing_histories", "erp_id", {
          transaction: t,
        }),
      ]);
    });
  },
};

"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iot_areas",
          "saas_item_id",
          {
            type: Sequelize.INTEGER,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_areas",
          "saas_unit_id",
          {
            type: Sequelize.INTEGER,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_areas",
          "saas_Item_name",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iot_areas", "saas_item_id", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_areas", "saas_unit_id", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_areas", "saas_Item_name", {
          transaction: t,
        }),
      ]);
    });
  },
};

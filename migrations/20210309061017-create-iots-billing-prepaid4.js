"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iots_billing_prepaids",
          "payment_qris",
          {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_billing_prepaids",
          "qris_string",
          {
            type: Sequelize.TEXT,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_billing_prepaids",
          "qris_timeout",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_billing_prepaids",
          "qris_order_id",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_billing_prepaids",
          "qris_nominal",
          {
            type: Sequelize.DOUBLE,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iots_billing_prepaids", "payment_qris", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_billing_prepaids", "qris_string", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_billing_prepaids", "qris_timeout", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_billing_prepaids", "qris_order_id", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_billing_prepaids", "qris_nominal", {
          transaction: t,
        }),
      ]);
    });
  },
};

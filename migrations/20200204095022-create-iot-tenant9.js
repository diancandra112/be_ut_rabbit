"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iot_tenants",
          "company_name",
          {
            type: Sequelize.STRING,
            defaultValue: "",
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_tenants",
          "billing_address",
          {
            type: Sequelize.TEXT,
            defaultValue: "",
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iot_tenants", "company_name", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_tenants", "billing_address", {
          transaction: t,
        }),
      ]);
    });
  },
};

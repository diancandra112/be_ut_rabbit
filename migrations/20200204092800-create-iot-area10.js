"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iot_areas",
          "email_erp",
          {
            type: Sequelize.TEXT,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_areas",
          "pass_erp",
          {
            type: Sequelize.TEXT,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iot_areas", "pass_erp", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_areas", "email_erp", {
          transaction: t,
        }),
      ]);
    });
  },
};

'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('iot_detail_gas', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      last_update: {
        type: Sequelize.DATE
      },
      meter_id: {
        type: Sequelize.STRING
      },
      meter_id: {
        type: Sequelize.STRING
      },
      status_valve: {
        type: Sequelize.INTEGER
      },
      battery_level: {
        type: Sequelize.STRING
      },
      interval: {
        type: Sequelize.INTEGER
      },
      desc: {
        type: Sequelize.STRING
      },
      node_id: {
        type: Sequelize.INTEGER
      },
      billing_type_id: {
        type: Sequelize.INTEGER
      },
      start_totalizer: {
        type: Sequelize.STRING
      },
      balance: {
        type: Sequelize.STRING
      },
      installation_date: {
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('iot_detail_gas');
  }
};
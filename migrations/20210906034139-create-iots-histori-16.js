"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("iots_histori_16s", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      device_id: {
        type: Sequelize.INTEGER,
      },
      reportTime: {
        type: Sequelize.STRING,
      },
      reportTimeFlag: {
        type: Sequelize.STRING,
      },
      tenantId: {
        type: Sequelize.INTEGER,
      },
      internalId: {
        type: Sequelize.INTEGER,
      },
      devEui: {
        type: Sequelize.STRING,
      },
      meter_id: {
        type: Sequelize.STRING,
      },
      child_app_name: {
        type: Sequelize.STRING,
      },
      child_sn: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
    await queryInterface.addIndex(
      "iots_histori_16s",
      ["device_id", "reportTime"],
      { unique: true }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("iots_histori_16s");
  },
};

"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("iots_histori_18s", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      device_id: {
        type: Sequelize.INTEGER,
      },
      reportTime: {
        type: Sequelize.STRING,
      },
      reportTimeFlag: {
        type: Sequelize.STRING,
      },
      tenantId: {
        type: Sequelize.INTEGER,
      },
      internalId: {
        type: Sequelize.INTEGER,
      },
      devEui: {
        type: Sequelize.STRING,
      },
      status: {
        type: Sequelize.TEXT,
      },
      battery: {
        type: Sequelize.DOUBLE,
      },
      board_temp: {
        type: Sequelize.DOUBLE,
      },
      rh: {
        type: Sequelize.DOUBLE,
      },
      iaq: {
        type: Sequelize.DOUBLE,
      },
      iaq_quality: {
        type: Sequelize.STRING,
      },
      environment_temp: {
        type: Sequelize.DOUBLE,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
    await queryInterface.addIndex(
      "iots_histori_18s",
      ["reportTime", "device_id"],
      {
        name: "index",
        unique: true,
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("iots_histori_18s");
  },
};

"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iot_areas",
          "saas",
          {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_areas",
          "saas_expired",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_areas",
          "saas_price",
          {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_areas",
          "saas_cut_date",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iot_areas", "saas", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_areas", "saas_expired", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_areas", "saas_price", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_areas", "saas_cut_date", {
          transaction: t,
        }),
      ]);
    });
  },
};

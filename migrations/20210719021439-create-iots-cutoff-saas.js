"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("iots_cutoff_saas", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      areaId: {
        type: Sequelize.INTEGER,
      },
      tanggal_cutoff: {
        type: Sequelize.INTEGER,
      },
      order: {
        type: Sequelize.INTEGER,
        defaultValue: 1,
      },
      time: {
        type: Sequelize.STRING,
        defaultValue: "00:00:00",
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("iots_cutoff_saas");
  },
};

"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("iots_alarm_water_levels", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      areaId: {
        type: Sequelize.INTEGER,
      },
      node_id: {
        type: Sequelize.INTEGER,
      },
      devEui: {
        type: Sequelize.STRING,
      },
      update_time: {
        type: Sequelize.STRING,
      },
      threshold: {
        type: Sequelize.DOUBLE,
      },
      threshold_type: {
        type: Sequelize.STRING,
      },
      meter_now: {
        type: Sequelize.DOUBLE,
      },
      location_name: {
        type: Sequelize.STRING,
      },
      location_type: {
        type: Sequelize.ENUM("internal", "tenant"),
      },
      grafik: {
        type: Sequelize.TEXT,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("iots_alarm_water_levels");
  },
};

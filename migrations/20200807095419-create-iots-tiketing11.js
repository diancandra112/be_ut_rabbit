"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iots_tiketings",
          "create_by",
          {
            type: Sequelize.STRING,
            defaultValue: "admin",
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_tiketings",
          "create_by_type",
          {
            type: Sequelize.STRING,
            defaultValue: "AREA",
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_tiketings",
          "create_by_id",
          {
            type: Sequelize.INTEGER,
            defaultValue: 0,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iots_tiketings", "create_by", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_tiketings", "create_by_type", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_tiketings", "create_by_id", {
          transaction: t,
        }),
      ]);
    });
  },
};

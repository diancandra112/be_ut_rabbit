"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iot_tenants",
          "erp_contract_name",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_tenants",
          "erp_contract_id",
          {
            type: Sequelize.INTEGER,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iot_tenants", "erp_contract_name", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_tenants", "erp_contract_id", {
          transaction: t,
        }),
      ]);
    });
  },
};

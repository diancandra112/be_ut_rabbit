"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iots_tiketings",
          "note_teknisi",
          {
            type: Sequelize.TEXT,
          },
          { transaction: t }
        ),
        // queryInterface.addColumn(
        //   "iots_tiketings",
        //   "merk",
        //   {
        //     type: Sequelize.STRING,
        //   },
        //   { transaction: t }
        // ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iots_tiketings", "note_teknisi", {
          transaction: t,
        }),
        // queryInterface.removeColumn("iots_tiketings", "merk", {
        //   transaction: t,
        // }),
      ]);
    });
  },
};

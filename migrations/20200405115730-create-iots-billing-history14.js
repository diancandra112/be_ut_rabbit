"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iots_billing_histories",
          "no_meter",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_billing_histories",
          "is_combine_billing",
          {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iots_billing_histories", "no_meter", {
          transaction: t,
        }),
        queryInterface.removeColumn(
          "iots_billing_histories",
          "is_combine_billing",
          {
            transaction: t,
          }
        ),
      ]);
    });
  },
};

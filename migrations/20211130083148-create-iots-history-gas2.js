"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iots_history_gas",
          "valve",
          {
            type: Sequelize.STRING,
            defaultValue: "open",
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_history_gas",
          "battrey_level",
          {
            type: Sequelize.STRING,
            defaultValue: "100",
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iots_history_gas", "valve", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_history_gas", "battrey_level", {
          transaction: t,
        }),
      ]);
    });
  },
};

"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.changeColumn(
          "iots_billing_histories",
          "status",
          {
            type: Sequelize.ENUM("CANCEL", "PAID", "UNPAID", "NEW"),
            defaultValue: "NEW",
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.changeColumn(
          "iots_billing_histories",
          "status",
          {
            type: Sequelize.INTEGER,
          },
          {
            transaction: t,
          }
        ),
      ]);
    });
  },
};

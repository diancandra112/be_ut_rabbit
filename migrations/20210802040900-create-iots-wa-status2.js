"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iots_wa_statuses",
          "note",
          {
            type: Sequelize.STRING,
            defaultValue: "billing",
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iots_wa_statuses", "note", {
          transaction: t,
        }),
      ]);
    });
  },
};

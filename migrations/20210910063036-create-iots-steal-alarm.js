"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("iots_steal_alarms", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      node_id: {
        type: Sequelize.INTEGER,
      },
      areaId: {
        type: Sequelize.INTEGER,
      },
      area_id: {
        type: Sequelize.INTEGER,
      },
      devEui: {
        type: Sequelize.STRING,
      },
      type: {
        type: Sequelize.STRING,
      },
      report_time_start: {
        type: Sequelize.STRING,
      },
      report_time_end: {
        type: Sequelize.STRING,
      },
      date: {
        type: Sequelize.STRING,
      },
      notif: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });

    await queryInterface.addIndex("iots_steal_alarms", ["node_id", "date"], {
      unique: true,
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("iots_steal_alarms");
  },
};

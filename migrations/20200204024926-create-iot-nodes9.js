"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iot_nodes",
          "latitude",
          {
            type: Sequelize.STRING,
            defaultValue: "0",
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_nodes",
          "longitude",
          {
            type: Sequelize.STRING,
            defaultValue: "0",
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_nodes",
          "altitude",
          {
            type: Sequelize.STRING,
            defaultValue: "0",
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iot_nodes", "latitude", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_nodes", "longitude", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_nodes", "altitude", {
          transaction: t,
        }),
      ]);
    });
  },
};

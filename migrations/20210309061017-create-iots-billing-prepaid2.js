"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iots_billing_prepaids",
          "no_order",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_billing_prepaids",
          "va_bca",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_billing_prepaids",
          "va_permata",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_billing_prepaids",
          "satuan_vc",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_billing_prepaids",
          "type_vc",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iots_billing_prepaids", "no_order", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_billing_prepaids", "va_bca", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_billing_prepaids", "va_permata", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_billing_prepaids", "satuan_vc", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_billing_prepaids", "type_vc", {
          transaction: t,
        }),
      ]);
    });
  },
};

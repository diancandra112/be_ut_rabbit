"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iot_pricing_postpaid_alls",
          "item_id",
          {
            type: Sequelize.INTEGER,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_pricing_postpaid_alls",
          "unit_id",
          {
            type: Sequelize.INTEGER,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_pricing_postpaid_alls",
          "Item_name",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iot_pricing_postpaid_alls", "item_id", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_pricing_postpaid_alls", "unit_id", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_pricing_postpaid_alls", "Item_name", {
          transaction: t,
        }),
      ]);
    });
  },
};

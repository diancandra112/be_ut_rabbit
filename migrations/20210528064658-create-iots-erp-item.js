"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("iots_erp_items", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      areaId: {
        type: Sequelize.INTEGER,
      },
      materai_name: {
        type: Sequelize.STRING,
      },
      materai_item_id: {
        type: Sequelize.STRING,
      },
      materai_unit_id: {
        type: Sequelize.STRING,
      },
      denda_name: {
        type: Sequelize.STRING,
      },
      denda_item_id: {
        type: Sequelize.STRING,
      },
      denda_unit_id: {
        type: Sequelize.STRING,
      },
      admin_name: {
        type: Sequelize.STRING,
      },
      admin_item_id: {
        type: Sequelize.STRING,
      },
      admin_unit_id: {
        type: Sequelize.STRING,
      },
      piutang_coa_id: {
        type: Sequelize.STRING,
      },
      piutang_coa_no: {
        type: Sequelize.STRING,
      },
      piutang_coa_name: {
        type: Sequelize.STRING,
      },
      penjualan_coa_id: {
        type: Sequelize.STRING,
      },
      penjualan_coa_no: {
        type: Sequelize.STRING,
      },
      penjualan_coa_name: {
        type: Sequelize.STRING,
      },
      ppn_coa_id: {
        type: Sequelize.STRING,
      },
      ppn_coa_no: {
        type: Sequelize.STRING,
      },
      ppn_coa_name: {
        type: Sequelize.STRING,
      },
      materai_coa_id: {
        type: Sequelize.STRING,
      },
      materai_coa_no: {
        type: Sequelize.STRING,
      },
      materai_coa_name: {
        type: Sequelize.STRING,
      },
      admin_coa_id: {
        type: Sequelize.STRING,
      },
      admin_coa_no: {
        type: Sequelize.STRING,
      },
      admin_coa_name: {
        type: Sequelize.STRING,
      },
      denda_coa_id: {
        type: Sequelize.STRING,
      },
      denda_coa_no: {
        type: Sequelize.STRING,
      },
      denda_coa_name: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("iots_erp_items");
  },
};

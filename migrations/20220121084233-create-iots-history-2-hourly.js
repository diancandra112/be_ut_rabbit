"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("iots_history_2_hourlies", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      nodeId: {
        type: Sequelize.INTEGER,
      },
      areaId: {
        type: Sequelize.INTEGER,
      },
      tenantId: {
        type: Sequelize.INTEGER,
      },
      internalId: {
        type: Sequelize.INTEGER,
      },
      fdate: {
        type: Sequelize.STRING,
      },
      fyear: {
        type: Sequelize.STRING,
      },
      fmonth: {
        type: Sequelize.STRING,
      },
      fday: {
        type: Sequelize.STRING,
      },
      fhour: {
        type: Sequelize.STRING,
      },
      fminute: {
        type: Sequelize.STRING,
      },
      totalizer: {
        type: Sequelize.DOUBLE,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
    await queryInterface.addIndex(
      "iots_history_2_hourlies",
      ["nodeId", "areaId", "tenantId", "internalId", "fdate", "fhour"],
      { name: "index", unique: true }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("iots_history_2_hourlies");
  },
};

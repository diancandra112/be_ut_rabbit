"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iots_cutoffs",
          "time",
          {
            type: Sequelize.STRING,
            defaultValue: "00:00:00",
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iots_cutoffs", "time", {
          transaction: t,
        }),
      ]);
    });
  },
};

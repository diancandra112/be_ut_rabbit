"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("iots_history_gas", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      device_id: {
        type: Sequelize.INTEGER,
      },
      meterReading: {
        type: Sequelize.DOUBLE,
      },
      reportTime: {
        type: Sequelize.STRING,
      },
      reportTimeFlag: {
        type: Sequelize.STRING,
      },
      tenantId: {
        type: Sequelize.INTEGER,
      },
      internalId: {
        type: Sequelize.INTEGER,
      },
      devEui: {
        type: Sequelize.STRING,
      },
      meter_id: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
    await queryInterface.addIndex(
      "iots_history_gas",
      ["reportTime", "device_id"],
      {
        name: "index",
        unique: true,
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("iots_history_gas");
  },
};

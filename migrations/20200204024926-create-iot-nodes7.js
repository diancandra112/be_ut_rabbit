"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iot_nodes",
          "setting_interval",
          {
            type: Sequelize.STRING,
            defaultValue: "0",
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_nodes",
          "setting_valve",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iot_nodes", "setting_interval", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_nodes", "setting_valve", {
          transaction: t,
        }),
      ]);
    });
  },
};

"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iots_billing_histories",
          "log_area_id",
          {
            type: Sequelize.INTEGER,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_billing_histories",
          "log",
          {
            type: Sequelize.TEXT,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_billing_histories",
          "log_flag",
          {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iots_billing_histories", "log_area_id", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_billing_histories", "log", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_billing_histories", "log_flag", {
          transaction: t,
        }),
      ]);
    });
  },
};

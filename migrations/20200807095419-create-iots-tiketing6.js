"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iots_tiketings",
          "gateway_name",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_tiketings",
          "unit_model",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_tiketings",
          "mac_address",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iots_tiketings", "gateway_name", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_tiketings", "unit_model", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_tiketings", "mac_address", {
          transaction: t,
        }),
      ]);
    });
  },
};

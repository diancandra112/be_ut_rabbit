"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iot_histories",
          "internalId",
          {
            type: Sequelize.INTEGER,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_histories",
          "tenantId",
          {
            type: Sequelize.INTEGER,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_histories",
          "devEui",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_histories",
          "meter_id",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iot_histories", "internalId", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_histories", "tenantId", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_histories", "devEui", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_histories", "meter_id", {
          transaction: t,
        }),
      ]);
    });
  },
};

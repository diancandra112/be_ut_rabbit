"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iots_tiketings",
          "name",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_tiketings",
          "email",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_tiketings",
          "phone",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_tiketings",
          "node_type",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_tiketings",
          "user_name",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iots_tiketings", "name", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_tiketings", "email", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_tiketings", "phone", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_tiketings", "user_name", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_tiketings", "node_type", {
          transaction: t,
        }),
      ]);
    });
  },
};

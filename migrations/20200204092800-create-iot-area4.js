"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iot_areas",
          "url_erp",
          {
            type: Sequelize.TEXT,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_areas",
          "is_erp",
          {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iot_areas", "is_erp", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_areas", "url_erp", {
          transaction: t,
        }),
      ]);
    });
  },
};

"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iot_nodes",
          "sensor_to_botom",
          {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_nodes",
          "high_alarm",
          {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_nodes",
          "low_alarm",
          {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iot_nodes", "sensor_to_botom", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_nodes", "high_alarm", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_nodes", "low_alarm", {
          transaction: t,
        }),
      ]);
    });
  },
};

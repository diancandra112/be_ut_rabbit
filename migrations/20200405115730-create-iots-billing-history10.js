"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iots_billing_histories",
          "payment_method",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_billing_histories",
          "payment_date",
          {
            type: Sequelize.DATE,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn(
          "iots_billing_histories",
          "payment_method",
          {
            transaction: t,
          }
        ),
        queryInterface.removeColumn("iots_billing_histories", "payment_date", {
          transaction: t,
        }),
      ]);
    });
  },
};

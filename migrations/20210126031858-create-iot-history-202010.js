'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('iot_history_202010s', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      device_id: {
        type: Sequelize.STRING
      },
      usage: {
        type: Sequelize.STRING
      },
      reporttime: {
        type: Sequelize.STRING
      },
      payload: {
        type: Sequelize.TEXT
      },
      payload_antares: {
        type: Sequelize.TEXT
      },
      reporttimeflag: {
        type: Sequelize.STRING
      },
      to_company_response: {
        type: Sequelize.TEXT
      },
      to_company_payload: {
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('iot_history_202010s');
  }
};
"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iot_nodes",
          "iaq",
          {
            type: Sequelize.DOUBLE,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_nodes",
          "iaq_quality",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iot_nodes", "iaq", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_nodes", "iaq_quality", {
          transaction: t,
        }),
      ]);
    });
  },
};

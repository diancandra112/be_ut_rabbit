"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iot_areas",
          "erp_name",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_areas",
          "erp_id",
          {
            type: Sequelize.INTEGER,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_areas",
          "erp_contract_name",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_areas",
          "erp_contract_id",
          {
            type: Sequelize.INTEGER,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iot_areas", "erp_name", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_areas", "erp_id", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_areas", "erp_contract_name", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_areas", "erp_contract_id", {
          transaction: t,
        }),
      ]);
    });
  },
};

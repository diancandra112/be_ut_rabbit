"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iots_billing_histories",
          "wa_rimender",
          {
            type: Sequelize.TEXT,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_billing_histories",
          "wa_raw",
          {
            type: Sequelize.TEXT,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_billing_histories",
          "wa_msgId",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_billing_histories",
          "wa_status",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_billing_histories",
          "wa_responseCode",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iots_billing_histories", "wa_rimender", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_billing_histories", "wa_raw", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_billing_histories", "wa_msgId", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_billing_histories", "wa_status", {
          transaction: t,
        }),
        queryInterface.removeColumn(
          "iots_billing_histories",
          "wa_responseCode",
          {
            transaction: t,
          }
        ),
      ]);
    });
  },
};

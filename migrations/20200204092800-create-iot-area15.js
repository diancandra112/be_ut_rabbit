"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.changeColumn(
          "iot_areas",
          "phone",
          {
            type: Sequelize.TEXT,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_areas",
          "email_pic",
          {
            type: Sequelize.TEXT,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.changeColumn(
          "iot_areas",
          "phone",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.removeColumn("iot_areas", "email_pic", {
          transaction: t,
        }),
      ]);
    });
  },
};

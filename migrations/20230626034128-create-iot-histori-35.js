"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("iot_histori_35s", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      device_id: {
        type: Sequelize.INTEGER,
      },
      latitude: {
        type: Sequelize.DOUBLE,
      },
      longitude: {
        type: Sequelize.DOUBLE,
      },
      battery: {
        type: Sequelize.STRING,
      },
      report_time: {
        type: Sequelize.STRING,
      },
      report_time_flag: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
    await queryInterface.addIndex(
      "iot_histori_35s",
      ["device_id", "report_time", "report_time_flag"],
      { name: "index", unique: true }
    );
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("iot_histori_35s");
  },
};

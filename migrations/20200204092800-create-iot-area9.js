"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iot_areas",
          "pressure",
          {
            type: Sequelize.DOUBLE,
            defaultValue: 50,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_areas",
          "water_pressure",
          {
            type: Sequelize.DOUBLE,
            defaultValue: 0.5,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_areas",
          "pressure_pt",
          {
            type: Sequelize.DOUBLE,
            defaultValue: 50,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_areas",
          "low_pressure",
          {
            type: Sequelize.DOUBLE,
            defaultValue: 0.3,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iot_areas", "pressure", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_areas", "water_pressure", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_areas", "pressure_pt", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_areas", "low_pressure", {
          transaction: t,
        }),
      ]);
    });
  },
};

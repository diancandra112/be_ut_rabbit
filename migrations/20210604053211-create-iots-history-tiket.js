"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("iots_history_tikets", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      ticket_type: {
        type: Sequelize.STRING,
      },
      create_by: {
        type: Sequelize.STRING,
      },
      delegation_to: {
        type: Sequelize.STRING,
      },
      ticket_delegation_date: {
        type: Sequelize.STRING,
      },
      no_tiket: {
        type: Sequelize.STRING,
      },
      tiket_id: {
        type: Sequelize.INTEGER,
      },
      area_id: {
        type: Sequelize.INTEGER,
      },
      action: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("iots_history_tikets");
  },
};

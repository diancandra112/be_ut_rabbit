"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iots_tiketings",
          "installation_date",
          {
            type: "TIMESTAMP",
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_tiketings",
          "dev_eui",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_tiketings",
          "meter_id",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_tiketings",
          "billing_type",
          {
            type: Sequelize.ENUM("PREPAID", "POSTPAID"),
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_tiketings",
          "status_valve",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_tiketings",
          "battery_level",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_tiketings",
          "interval",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_tiketings",
          "description",
          {
            type: Sequelize.TEXT,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iots_tiketings", "installation_date", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_tiketings", "dev_eui", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_tiketings", "billing_type", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_tiketings", "meter_id", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_tiketings", "status_valve", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_tiketings", "battery_level", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_tiketings", "interval", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_tiketings", "description", {
          transaction: t,
        }),
      ]);
    });
  },
};

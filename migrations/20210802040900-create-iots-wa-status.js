'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('iots_wa_statuses', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      billing_id: {
        type: Sequelize.INTEGER
      },
      wa_raw: {
        type: Sequelize.TEXT
      },
      wa_msgId: {
        type: Sequelize.STRING
      },
      wa_status: {
        type: Sequelize.STRING
      },
      wa_responseCode: {
        type: Sequelize.STRING
      },
      wa_number: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('iots_wa_statuses');
  }
};
"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iot_areas",
          "interval_pressure",
          {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_areas",
          "interval_water_pressure",
          {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_areas",
          "interval_pressure_pt",
          {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_areas",
          "interval_low_pressure",
          {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iot_areas", "interval_pressure", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_areas", "interval_water_pressure", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_areas", "interval_pressure_pt", {
          transaction: t,
        }),
        queryInterface.removeColumn(
          "iot_areas",
          "interval_preinterval_low_pressuressure",
          {
            transaction: t,
          }
        ),
      ]);
    });
  },
};

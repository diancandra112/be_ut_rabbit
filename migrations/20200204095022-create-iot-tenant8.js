"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iot_tenants",
          "is_ppn",
          {
            type: Sequelize.BOOLEAN,
            defaultValue: true,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_tenants",
          "is_ppn_value",
          {
            type: Sequelize.INTEGER,
            defaultValue: 10,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iot_tenants", "is_ppn", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_tenants", "is_ppn_value", {
          transaction: t,
        }),
      ]);
    });
  },
};

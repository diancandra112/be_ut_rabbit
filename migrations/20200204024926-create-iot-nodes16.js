"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iot_nodes",
          "line_prepaid",
          {
            type: Sequelize.DATE,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_nodes",
          "email_anomali",
          {
            type: Sequelize.DATE,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_nodes",
          "email_prepaid",
          {
            type: Sequelize.DATE,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_nodes",
          "email_low_battery",
          {
            type: Sequelize.DATE,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_nodes",
          "email_offline",
          {
            type: Sequelize.DATE,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iot_nodes", "line_prepaid", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_nodes", "email_offline", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_nodes", "email_low_battery", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_nodes", "email_prepaid", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_nodes", "email_anomali", {
          transaction: t,
        }),
      ]);
    });
  },
};

"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("iots_billing_histories", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      areaId: {
        type: Sequelize.INTEGER,
      },
      tenantId: {
        type: Sequelize.INTEGER,
      },
      nodeId: {
        type: Sequelize.INTEGER,
      },
      cut_date: {
        type: Sequelize.STRING,
      },
      start_meter: {
        type: Sequelize.STRING,
      },
      end_meter: {
        type: Sequelize.STRING,
      },

      start_date: {
        type: Sequelize.STRING,
      },
      end_date: {
        type: Sequelize.STRING,
      },
      totalizer: {
        type: Sequelize.STRING,
      },
      usage: {
        type: Sequelize.STRING,
      },
      invoice: {
        type: Sequelize.STRING,
      },
      status: {
        type: Sequelize.STRING,
      },
      biaya_transaksi: {
        type: Sequelize.INTEGER,
      },
      virtual_account: {
        type: Sequelize.STRING,
      },
      biaya_penyesuaian: {
        type: Sequelize.INTEGER,
      },
      ppn: {
        type: Sequelize.INTEGER,
      },
      harga_satuan: {
        type: Sequelize.INTEGER,
      },
      periode_cut: {
        type: Sequelize.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("iots_billing_histories");
  },
};

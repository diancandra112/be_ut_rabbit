"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iot_tenants",
          "minimum_charge_gas",
          {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_tenants",
          "minimum_charge_water",
          {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_tenants",
          "minimum_charge_electricity_ct",
          {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_tenants",
          "minimum_charge_electricity_non_ct",
          {
            type: Sequelize.DOUBLE,
            defaultValue: 0,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iot_tenants", "minimum_charge_gas", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_tenants", "minimum_charge_water", {
          transaction: t,
        }),
        queryInterface.removeColumn(
          "iot_tenants",
          "minimum_charge_electricity_ct",
          {
            transaction: t,
          }
        ),
        queryInterface.removeColumn(
          "iot_tenants",
          "minimum_charge_electricity_non_ct",
          {
            transaction: t,
          }
        ),
      ]);
    });
  },
};

"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iot_areas",
          "anomali_usage_start_date",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_areas",
          "anomali_usage_end_date",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iot_areas", "anomali_usage_start_date", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_areas", "anomali_usage_end_date", {
          transaction: t,
        }),
      ]);
    });
  },
};

"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("iots_billing_prepaids", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      order_date: {
        type: Sequelize.DATE,
      },
      tenant_id: {
        type: Sequelize.INTEGER,
      },
      tenant_name: {
        type: Sequelize.STRING,
      },
      device_id: {
        type: Sequelize.INTEGER,
      },
      device_deveui: {
        type: Sequelize.STRING,
      },
      area_id: {
        type: Sequelize.INTEGER,
      },
      product_id: {
        type: Sequelize.INTEGER,
      },
      product_name: {
        type: Sequelize.STRING,
      },
      product_value: {
        type: Sequelize.STRING,
      },
      product_price: {
        type: Sequelize.INTEGER,
      },
      status: {
        type: Sequelize.ENUM("CANCEL", "PAID", "UNPAID"),
        defaultValue: "UNPAID",
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("iots_billing_prepaids");
  },
};

"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface
      .createTable("iots_history_type_5s", {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        node_id: {
          type: Sequelize.INTEGER,
        },
        devEui: {
          type: Sequelize.STRING,
        },
        battrey_level: {
          type: Sequelize.DOUBLE,
        },
        preassure: {
          type: Sequelize.DOUBLE,
        },
        tenant_id: {
          type: Sequelize.INTEGER,
        },
        internal_id: {
          type: Sequelize.INTEGER,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        update_time: {
          type: Sequelize.STRING,
        },
        reportTimeFlag: {
          type: Sequelize.STRING,
        },
      })
      .then(() =>
        queryInterface.addIndex(
          "iots_history_type_5s",
          ["node_id", "update_time", "reportTimeFlag"],
          { unique: true }
        )
      );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("iots_history_type_5s");
  },
};

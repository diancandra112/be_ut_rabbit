"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("iots_kerusakan_gas", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      node_gas_id: {
        type: Sequelize.INTEGER,
      },
      node_gas_devEui: {
        type: Sequelize.STRING,
      },
      node_gas_last_update: {
        type: Sequelize.STRING,
      },
      node_pressure_id: {
        type: Sequelize.INTEGER,
      },
      areaId: {
        type: Sequelize.INTEGER,
      },
      node_pressure_devEui: {
        type: Sequelize.STRING,
      },
      node_pressure_date_1: {
        type: Sequelize.STRING,
      },
      node_pressure_date_2: {
        type: Sequelize.STRING,
      },
      node_pressure_meter_1: {
        type: Sequelize.DOUBLE,
      },
      node_pressure_meter_2: {
        type: Sequelize.DOUBLE,
      },
      node_gas_totalizer: {
        type: Sequelize.DOUBLE,
      },
      is_notif: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      location_type: {
        type: Sequelize.STRING,
      },
      location: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("iots_kerusakan_gas");
  },
};

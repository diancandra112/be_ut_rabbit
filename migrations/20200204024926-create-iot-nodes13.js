"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iot_nodes",
          "line_anomali",
          {
            type: Sequelize.DATE,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_nodes",
          "line_offline",
          {
            type: Sequelize.DATE,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_nodes",
          "line_low_battery",
          {
            type: Sequelize.DATE,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iot_nodes", "line_anomali", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_nodes", "line_offline", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_nodes", "line_low_battery", {
          transaction: t,
        }),
      ]);
    });
  },
};

'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('iots_to_publics', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      payload: {
        type: Sequelize.TEXT
      },
      method: {
        type: Sequelize.STRING
      },
      auth: {
        type: Sequelize.TEXT
      },
      url: {
        type: Sequelize.TEXT
      },
      status: {
        type: Sequelize.TEXT
      },
      code: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('iots_to_publics');
  }
};
"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iot_nodes",
          "previous_update",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_nodes",
          "live_previous_meter",
          {
            type: Sequelize.STRING,
            defaultValue: "0",
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_nodes",
          "anomali_usage_start_date",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_nodes",
          "anomali_usage_end_date",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_nodes",
          "max_usage",
          {
            type: Sequelize.STRING,
            defaultValue: "0",
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iot_nodes", "previous_update", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_nodes", "live_previous_meter", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_nodes", "anomali_usage_start_date", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_nodes", "anomali_usage_end_date", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_nodes", "max_usage", {
          transaction: t,
        }),
      ]);
    });
  },
};

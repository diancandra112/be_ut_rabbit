"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.changeColumn(
          "iots_billing_histories",
          "biaya_penyesuaian",
          {
            type: Sequelize.STRING,
            defaultValue: "0",
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.changeColumn(
          "iots_billing_histories",
          "biaya_penyesuaian",
          {
            type: Sequelize.INTEGER,
          },
          {
            transaction: t,
          }
        ),
      ]);
    });
  },
};

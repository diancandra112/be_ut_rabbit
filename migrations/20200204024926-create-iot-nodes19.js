"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iot_nodes",
          "live_prepayment",
          {
            type: Sequelize.STRING,
            defaultValue: "0",
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_nodes",
          "live_prepayment_date",
          {
            type: Sequelize.DATE,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iot_nodes", "live_prepayment", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_nodes", "live_prepayment_date", {
          transaction: t,
        }),
      ]);
    });
  },
};

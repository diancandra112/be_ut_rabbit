'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('iot_histori_11s', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      device_id: {
        type: Sequelize.INTEGER
      },
      meterReading: {
        type: Sequelize.DOUBLE
      },
      reportTime: {
        type: Sequelize.STRING
      },
      reportTimeFlag: {
        type: Sequelize.STRING
      },
      tenantId: {
        type: Sequelize.INTEGER
      },
      internalId: {
        type: Sequelize.INTEGER
      },
      devEui: {
        type: Sequelize.STRING
      },
      meter_id: {
        type: Sequelize.STRING
      },
      valve: {
        type: Sequelize.STRING
      },
      battrey_level: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('iot_histori_11s');
  }
};
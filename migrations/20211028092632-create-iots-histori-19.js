"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("iots_histori_19s", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      reportTime: {
        type: Sequelize.STRING,
      },
      reportTimeFlag: {
        type: Sequelize.STRING,
      },
      device_id: {
        type: Sequelize.INTEGER,
      },
      tenantId: {
        type: Sequelize.INTEGER,
      },
      internalId: {
        type: Sequelize.INTEGER,
      },
      devEui: {
        type: Sequelize.STRING,
      },
      meter_id: {
        type: Sequelize.STRING,
      },
      status: {
        type: Sequelize.STRING,
      },
      battery: {
        type: Sequelize.DOUBLE,
      },
      board_temp: {
        type: Sequelize.DOUBLE,
      },
      time: {
        type: Sequelize.DOUBLE,
      },
      count: {
        type: Sequelize.DOUBLE,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
    await queryInterface.addIndex(
      "iots_histori_19s",
      ["reportTime", "device_id"],
      {
        name: "index",
        unique: true,
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("iots_histori_19s");
  },
};

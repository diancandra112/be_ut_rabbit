"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iot_pricing_postpaids",
          "item_id",
          {
            type: Sequelize.INTEGER,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_pricing_postpaids",
          "unit_id",
          {
            type: Sequelize.INTEGER,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iot_pricing_postpaids",
          "Item_name",
          {
            type: Sequelize.STRING,
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iot_pricing_postpaids", "item_id", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_pricing_postpaids", "unit_id", {
          transaction: t,
        }),
        queryInterface.removeColumn("iot_pricing_postpaids", "Item_name", {
          transaction: t,
        }),
      ]);
    });
  },
};

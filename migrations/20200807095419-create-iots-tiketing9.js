"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.addColumn(
          "iots_tiketings",
          "node_id",
          {
            type: Sequelize.INTEGER,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_tiketings",
          "tenant_id",
          {
            type: Sequelize.INTEGER,
          },
          { transaction: t }
        ),
        queryInterface.addColumn(
          "iots_tiketings",
          "internal_id",
          {
            type: Sequelize.INTEGER,
          },
          { transaction: t }
        ),
        queryInterface.changeColumn(
          "iots_tiketings",
          "ticket_type",
          {
            type: Sequelize.ENUM("installation", "maintenance", "unassigned"),
          },
          { transaction: t }
        ),
      ]);
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
        queryInterface.removeColumn("iots_tiketings", "tenant_id", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_tiketings", "internal_id", {
          transaction: t,
        }),
        queryInterface.removeColumn("iots_tiketings", "node_id", {
          transaction: t,
        }),
        queryInterface.changeColumn(
          "iots_tiketings",
          "ticket_type",
          {
            type: Sequelize.ENUM("installation", "maintenance"),
          },
          { transaction: t }
        ),
      ]);
    });
  },
};

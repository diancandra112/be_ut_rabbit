"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("iots_history_waters", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      device_id: {
        type: Sequelize.INTEGER,
      },
      meterreading: {
        type: Sequelize.DOUBLE,
      },
      reporttime: {
        type: Sequelize.STRING,
      },
      reporttimeflag: {
        type: Sequelize.STRING,
      },
      tenantid: {
        type: Sequelize.INTEGER,
      },
      internalid: {
        type: Sequelize.INTEGER,
      },
      deveui: {
        type: Sequelize.STRING,
      },
      meter_id: {
        type: Sequelize.STRING,
      },
      valve: {
        type: Sequelize.STRING,
      },
      battrey_level: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
    await queryInterface.addIndex(
      "iots_history_waters",
      ["reportTime", "device_id"],
      {
        name: "index",
        unique: true,
      }
    );
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("iots_history_waters");
  },
};
